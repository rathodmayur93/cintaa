//
//  CompletedShootModel.swift
//  CINTA
//
//  Created by Mayur on 16/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CompletedShootModel: NSObject {

    var alies_name      = ""
    var shift_time      = ""
    var shoot_date      = ""
    var entry_id        = ""
    var shoot_location  = ""
    var film_tv_name    = ""
    var house_name      = ""
}
