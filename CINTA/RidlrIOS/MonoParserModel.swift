//
//  MonoParserModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MonoParserModel{

    var monoStops = [MonoStops]()
}

class MonoStops{
    
    var stopId      = ""
    var stopName    = ""
    var stopLat     = ""
    var stopLong    = ""
}
