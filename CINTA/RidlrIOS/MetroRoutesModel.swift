//
//  MetroRoutesModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MetroRoutesModel{
    
    var metroRoute = [MetroRoute]()
}

class MetroRoute{

    var routeId             = ""
    var routeShortName      = ""
    var routeLongName       = ""
    var routeDirectitonId   = ""
    var routeSourceId       = ""
    var routeDestinationId  = ""
}
