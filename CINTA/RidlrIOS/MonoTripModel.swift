//
//  MonoTripModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MonoTripModel{
    
    var monoBaseModel = [MonoTripBaseModel]()
}

class MonoTripBaseModel{
    
    var tripId          = ""
    var stopModel       = [MonoStopsId]()
    var monoTiming     = [MonoTimingModel]()
}

class MonoStopsId{
    
    var stopId          = ""
    var pattern         = [MonoPatternModel]()
}

class MonoPatternModel{
    
    var stopInterval    = ""
}

class MonoTimingModel{
    
    var startingTime    = ""
    var calendarId      = ""
}
