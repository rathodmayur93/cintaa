//
//  PenaltyIssueDetectedViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 27/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PenaltyIssueDetectedViewController: UIViewController {

    @IBOutlet var exitStationLabel      : UILabel!
    @IBOutlet var exitByValue           : UILabel!
    
    @IBOutlet var newExitStationLabel   : UILabel!
    @IBOutlet var penaltyFare           : UILabel!
    
    @IBOutlet var penaltyTimeOutLabel   : UILabel!
    @IBOutlet var penaltyTimeFareLabel  : UILabel!
    @IBOutlet var totalPenaltyFareLabel : UILabel!
    
    @IBOutlet var currentStationView    : UIView!
    @IBOutlet var newStationView        : UIView!
    @IBOutlet var proceedBT             : UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setUI()
    }
    
    func setUI(){
        
        currentStationView.layer.borderWidth = 1.0
        currentStationView.layer.borderColor = UIColor.lightGray.cgColor
        
        newStationView.layer.borderWidth     = 1.0
        newStationView.layer.borderColor     = UIColor.lightGray.cgColor
        
        exitStationLabel.text          = penalyList[0].bookingRoute[0].destinationName
        newExitStationLabel.text       = penaltyStation
        
        
        
        if(penalyList[0].itemSubType == "EXIT_STATION_EXCESS_TIME_PENALTY"){
            
            let (h,m,s)                    = uiFun.convertMiliSecondToHourMinute(seconds: Int(penalyList[0].itemAttribute[0].excessTime)!/1000)
            penaltyTimeOutLabel.text       = "\(h)Hr:\(m)Min:\(s)Sec"
            penaltyTimeFareLabel.text      = "₹ \(penalyList[0].totalAmount)"
            penaltyFare.text               = "₹ 0.0"
        }else{
            penaltyFare.text               = "₹ \(penalyList[0].totalAmount)"
            penaltyTimeFareLabel.text      = "₹ 0.0"
        }
        
        totalPenaltyFareLabel.text     = "₹ \(penalyList[0].totalAmount)"
        rechargeAmount                 = "\(penalyList[0].totalAmount)"
        
        // set new selected station by user
        if(metroTokenJourneyType == "STORE_VALUE_PASS"){
            exitStationLabel.text   = penaltyStation
        }
        uiFun.writeData(value: "Penalty", key: "ticketingAgency")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "PenaltyIssueViewController", fromController: self)
    }
    
    
    @IBAction func proceedAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
    }
}
