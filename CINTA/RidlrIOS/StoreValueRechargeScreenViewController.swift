//
//  StoreValueRechargeScreenViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 17/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import GMStepper

var passRecharge = false
class StoreValueRechargeScreenViewController: UIViewController {

    
    @IBOutlet var passIdLabel       : UILabel!
    @IBOutlet var passBalanceLabel  : UILabel!
    @IBOutlet var stepper           : GMStepper!
    @IBOutlet var totalLabel        : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        passIdLabel.text             = uiFun.readData(key: "metroStoreValueMasterQRCode")
        passBalanceLabel.text        = uiFun.readData(key: "storeValuePassBalance")
        totalLabel.text              = "\(stepper.value)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func stepperAction(_ sender: Any) {
        totalLabel.text = "\(stepper.value)"
        
    }
    
    @IBAction func confirmAction(_ sender: Any) {
            passRecharge            = true
            rechargeAmount          = "\(Int(stepper.value))"
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        
    }
    
}
