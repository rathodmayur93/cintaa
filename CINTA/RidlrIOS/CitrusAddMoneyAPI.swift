//
//  CitrusAddMoneyAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 07/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CreditCardValidator

class CitrusAddMoneyAPI: UIViewController {

    var util        = Utillity()
    var constant    = Constants()
    var uiFun       = UiUtillity()
    
    
    func makeAPICall(fromViewController : AddMoneyViewController, monthYear : String, cardNumber :String, amount : String, whichCard : String){
        
        let url = "\(constant.STAGING_BASE_URL)/payment/addMoney"
        print("Adding Money In Citrus Wallet Part 1 \(url)")
        
        util.APICall(apiUrl: url, jsonString: citrusAddMoneyJSONString(monthYear: monthYear, cardNum: cardNumber, amount: amount,whichCard:whichCard )!, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                fromViewController.citrusAddMoneyResponse(response: jsonResponse)
                print("Successfully Adding Money In Citrus Wallet Part 1 \(jsonResponse) ")
                return
            }else {
                print("Adding Money In Citrus Wallet Part 1 Failed....Try Harder Mayur ")
                return
            }
        }
    }

    func citrusAddMoneyJSONString(monthYear : String, cardNum : String, amount : String, whichCard : String) -> String?{
        
        let creditCardValidator  = CreditCardValidator()
        let selectedCardType     = creditCardValidator.type(from: cardNum)
        
        print("Amount to add API \(amount) & card type is \(selectedCardType?.name)")
        let para2:NSMutableDictionary = NSMutableDictionary()
        para2.setValue(monthYear, forKey: "cardExpiry")
        para2.setValue(selectedCardType?.name, forKey: "cardType")
        para2.setValue(uiFun.maskCardNumber(str: cardNum), forKey: "maskedCardNumber")
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue(amount, forKey: "amount")  // Change this value while in production
        para1.setValue("CITRUSPAY", forKey: "paymentGateway")
        para1.setValue(whichCard, forKey: "paymentMode")
        para1.setValue(para2, forKey: "details")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(para1, forKey: "paymentDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("citrus add money json string part 1= \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
}
