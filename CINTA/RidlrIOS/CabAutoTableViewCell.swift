//
//  CabAutoTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 30/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CabAutoTableViewCell: UITableViewCell {

    @IBOutlet var durationLabel: UILabel!
    
    @IBOutlet var distanceMode1Label: UILabel!
    @IBOutlet var mode2Label: UILabel!
    @IBOutlet var mode3Label: UILabel!
    
    @IBOutlet var mode1ImageView: UIImageView!
    @IBOutlet var mode2ImageView: UIImageView!
    @IBOutlet var mode3ImageView: UIImageView!
    
    @IBOutlet var horizontalScrollView: UIScrollView!
   

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
