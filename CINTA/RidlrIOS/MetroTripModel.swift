//
//  MetroTripModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MetroTripModel{

    var metroBaseModel = [MetroTripBaseModel]()
}

class MetroTripBaseModel{

    var tripId          = ""
    var stopModel       = [MetroStopsId]()
    var metroTiming     = [MetroTimingModel]()
}

class MetroStopsId{

    var stopId          = ""
    var pattern         = [MetroPatternModel]()
}

class MetroPatternModel{

    var stopInterval    = ""
}

class MetroTimingModel{

    var startingTime    = ""
    var calendarId      = ""
}
