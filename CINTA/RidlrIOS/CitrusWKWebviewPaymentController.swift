//
//  CitrusWKWebviewPaymentController.swift
//  RidlrIOS
//
//  Created by Mayur on 18/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import WebKit

class CitrusWKWebviewPaymentController: UIViewController, WKScriptMessageHandler {

    @IBOutlet var webviewWK: UIWebView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = NSURL (string: redirectUrl!)
        let requestObj = NSURLRequest(url: url! as URL);
        webviewWK.loadRequest(requestObj as URLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage){
        print("Javascript Response \(message.body)")
        
        if(message.name == "callbackHandler") {
            print("JavaScript is sending a message \(message.body)")
        }
        
    }

}
