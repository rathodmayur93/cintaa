//
//  Constants.swift
//  RidlrIOS
//
//  Created by Mayur on 15/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class Constants{

    let uiFun = UiUtillity()
    
    let app_version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
    let app_version_name    = "\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String)d- DEBUG iOS"
   
    let BASE_URL            = "http://testapi.ridlr.in:8053/"   //"https://payment.ridlr.in/"   //
    let STAGING_BASE_URL    = "http://testapi.ridlr.in:8053/master/v1" //"https://payment.ridlr.in/v1" //
    let PROFILE_STAGING_URL = "http://localhost:8080/"      // Not in use
    let CITRUS_PG           = "https://sandboxadmin.citruspay.com/service/moto/authorize/struct/payment"//"https://admin.citruspay.com/service/moto/authorize/struct/payment"//
    
    let METRO_TOKEN         = "http://testapi.ridlr.in:8080/v1"
    
    let TIMELINE_URL        = "http://Newsfeed.saarath.com/InteractiveTimelineWS/clsTimeLineAPI.asmx/"
    let MAP_DATA            = "https://api.saarath.com/MobileAppsWS/MobileAppWS.asmx/"//"http://ridlr.in/callWebService_Get.php?webMethodName=MapDataAsJSonWap&parameters"//"http://ridlr.in/callWebService_Get.php?webMethodName=MapDataAsJSonWap&"
    
    let MAP_TESTING         = "http://ridlr.in/callWebService_Get.php?webMethodName=MapDataAsJSonWap&parameters=bound1:19.101358319943,72.8490476310253,19.1284046088012,72.8651408851147~bound2:0,0,0,0~zoom:4"
    let CARD_PAYMENT        = "http://testapi.ridlr.in:8053/v1/payment" // Not in use
    
    let newsFeedURL         = "http://Newsfeed.saarath.com/InteractiveTimelineWS/clsTimeLineAPI.asmx/GetTimeLinesForLocation?addparams=FirstPostedOn%7C%2CLastPostedOn%7C&strLocation=Mumbai&strViewType=&strIP=&strGroupId=ridlr&strCustKey=7blgd24n8vy5szwz7blgd24n8vy5szwz&dbLatitude=19.076406&updType=All&dbLongitude=72.874639&strSinceId=&intNoOfRecords=10&updSource=All&accessType=device&passkey=NGQyYWE5YjI1OTNmZDM3MA%3D%3D%0A&groupId=ridlr&custKey=7blgd24n8vy5szwz7blgd24n8vy5szwz&userid=-111"
    
    let ROUTE_SEARCH_PLANNER_URL    = "http://tomcat.ridlr.in:8080/traffline-api/planner"
    
    let ROUTE_SEARCH_DRIVE_URL      = "http://api.saarath.com/MobileAppsWS/MobileAppWebservice.svc/Route/TravelTime"
    
    let TIMETABLE_STAGING_BASE_URL  = "http://tomcat.ridlr.in:8080/traffline-api/" //
    
    let TIMETABLE_BASE_URL          = "http://tomcat.ridlr.in:8080/traffline-api/" // http://testapi.ridlr.in:5410/traffline-api/
    let TIMETABLE_BUS_NUMBER        = "http://elastic.ridlr.in:9100/"
    let TIMETABLE_BUS_STOP          = "http://elastic.ridlr.in:9100/"
    
    let BLOG_URL                    = "http://ridlr.in/blog"
    let OFFER_URL                   = "http://www.ridlr.in/offers"
    
    /********************* APP CONSTANTS *****************************/
    
    let CUST_KEY    = "7blgd24n8vy5szwz7blgd24n8vy5szwz"
    let PASS_KEY    = "NGQyYWE5YjI1OTNmZDM3MA=="
    let ACCESS_TYPE = "device"
    let GROUP_ID    = "ridlr"
    let MAP_ZOOM    = "4"
    
    let noInternetJSON = "{\"status\": \"OK\",\"error\": \"No Internet\"}"
}
