//
//  BestPassModel.swift
//  RidlrIOS
//
//  Created by Mayur on 10/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BestPassModel {
    
    var bestModel = [BESTModel]()
}

class BESTModel{
    
    var status              = ""
    var code                = ""
    var bestData            = [BestData]()
}

class BestData{
    
    var currentDetails      = [Items]()
    var actionDetails       = [Items]()
}

class Items{
    
    var itemType            = ""
    var issuerIdentifier    = ""
    var activeRenewalDate   = ""
    var itemSubType         = ""
    var txnRefNo            = ""
    var discount            = ""
    var activationDate      = ""
    var activationCriteria  = ""
    var passCategory        = ""
    var routeDetailText     = ""
    var pricingDetails      = [PricingDetails]()
    var concession          = ""
    var companyIdentifier   = ""
    var itemAction          = ""
    var itemAmount          = ""
    var validity            = ""
    var afterMigration      = ""
    var expiryDate          = ""
    var issuerTag           = ""
    var routeDetails        = [RouteDetails]()
    var user                = [BestUser]()
}

class BestUser{
    
    var dob                 = ""
    var userImage           = ""
    var gender              = ""
    var name                = ""
    var type                = ""
}

class PricingDetails{
    
    var discount            = ""
    var concession          = ""
    var totalAmount         = ""
    var baseAmount          = ""
    var tax                 = ""
    var surcharge           = [Surcharge]()
}

class Surcharge{
    
    var amount              = ""
    var code                = ""
    var appliedBy           = ""
    var applyLevel          = ""
    var applyValue          = ""
    var applyType           = ""
    var name                = ""
}

class RouteDetails{

    var destinationId           = ""
    var routeNo                 = ""
    var destinationName         = ""
    var nextRoute               = ""
    var routeId                 = ""
    var sourceStageName         = ""
    var sourceAreaId            = ""
    var sourceName              = ""
    var prevRoute               = ""
    var sourceId                = ""
    var destinationStageName    = ""
    var sourceStageId           = ""
    var routeName               = ""
    var destinationStageId      = ""
    var destinationAreaId       = ""

}


