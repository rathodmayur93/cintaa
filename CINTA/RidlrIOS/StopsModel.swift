//
//  StopsModel.swift
//  RidlrIOS
//
//  Created by Mayur on 22/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class StopsModel{

    var stops = [Stops]()
}

class Stops{

    var name = ""
    var lat = ""
    var long = ""
    var agencyId = [AgencyId]()
}

class AgencyId{

    var agencyId = ""
}
