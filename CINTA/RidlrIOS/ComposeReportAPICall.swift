//
//  ComposeReportAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 19/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class ComposeReportAPICall{

    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    var encodedPassKey = ""
    
    func makeAPICall(fromViewController : ComposeViewController, composeText : String, mediaLink : String){
        
        let uuid                = uiFun.readData(key: "UUID")
        let utf8str             = uuid.data(using: String.Encoding.utf8)
        if let base64Encoded    = utf8str?.base64EncodedString()  {
            encodedPassKey      = base64Encoded //encoding uuid
        }
        
        let todaysDate                  :NSDate               = NSDate()
        let dateFormatter               :DateFormatter        = DateFormatter()
        dateFormatter.dateFormat                              = "yyyy-MM-dd HH:mm:ss.SSS"
        let todayDate                                         = dateFormatter.string(from: todaysDate as Date)
        print("Today date is \(todayDate)")
        
        let url = "\(constant.TIMELINE_URL)ComposeTimelineMessage"  // API url
        
        
        // Parameters for api call
        let params : [String:AnyObject] = ["dtmPostedOn":todayDate as AnyObject,
                                           "strUpdateId":"" as AnyObject,
                                           "updSource":"traffapp" as AnyObject,
                                           "strTextUpdate":composeText as AnyObject,
                                           "lngFromUserId":"-111" as AnyObject,
                                           "strProfilePicURL":"" as AnyObject,
                                           "strTwitterHandleOrTraffUsername":uiFun.readData(key: "Username") as AnyObject,
                                           "strLocation":"Mumbai" as AnyObject,
                                           "dbLatitude":"0.0" as AnyObject,
                                           "dbLongitude":"0.0" as AnyObject,
                                           "updType":"1" as AnyObject,
                                           "strSourceUpdateId":"" as AnyObject,
                                           "strEmoticons":"" as AnyObject,
                                           "strMediaLinks" : mediaLink as AnyObject,
                                           "strGroupId" : "g2apps" as AnyObject,
                                           "strCustKey":"f29a3c79212611e3aeb927d5ba248633" as AnyObject,
                                           "strIP":"" as AnyObject,
                                           "passkey" : constant.PASS_KEY as AnyObject,
                                           "addparams":"" as AnyObject]
        
        // Sample request of api
        //let parameterString = "dtmPostedOn=2017-02-07 10:14:49, strUpdateId=, updSource=traffapp, strTextUpdate=Thanks for update, lngFromUserId=-111, strProfilePicURL=, strTwitterHandleOrTraffUsername=mayur, strLocation=Mumbai, dbLatitude=0.0, dbLongitude=0.0, updType=1, strMediaLinks=, strSourceUpdateId=, strEmoticons=, strGroupId=g2apps, strCustKey=f29a3c79212611e3aeb927d5ba248633, strIP=, passkey=YWY3NjE1N2U3NDQ1ZmM3OQ%3D%3D%0A, addparams="//params.stringFromHttpParameters()
        
        util.sendRequest(url: url, parameters: params, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                
                fromViewController.responseFromComposeReport(jsonResponse: jsonResponse)
                print("Successfully Compose Timeline Response Got.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Compose Timeline Failed....Try Harder Mayur ")
                return
            }
            
        })
    }

}
