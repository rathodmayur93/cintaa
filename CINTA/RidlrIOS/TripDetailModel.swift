//
//  TripDetailModel.swift
//  RidlrIOS
//
//  Created by Mayur on 24/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class TripDetailModel{

    var startTime   = ""
    var endTime     = ""
    var stops       = [TripStop]()
}

class TripStop{

    var id              = ""
    var name            = ""
    var arrivaltime     = ""
    var departuretime   = ""
    var lon             = ""
    var lat             = ""
    var platformno      = ""
}
