//
//  CitrusFetchSaveCardDetailAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 07/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CitrusFetchSaveCardDetailAPI {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : AddMoneyViewController){
        
        let randomNum:UInt32 = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String = String(randomNum)
        let timeStamp = Int(NSDate().timeIntervalSince1970)
        
        let url = "\(constant.STAGING_BASE_URL)/pg/CITRUSPAY/add_money?random=\(randomNumString)&current_time=\(timeStamp)"
        print("Fetching save card citrus API \(url)")
        
        util.APICall(apiUrl: url, jsonString: "", requestType: "GET", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.saveCardAPIResponse(response: jsonResponse)
                //print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }

}
