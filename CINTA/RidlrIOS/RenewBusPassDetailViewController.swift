
//
//  RenewBusPassDetailViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 08/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import M13Checkbox


class RenewBusPassDetailViewController: UIViewController {

    var uiFun       = UiUtillity()
    var ridlrColors = RidlrColors()
    
    @IBOutlet var passDetailVIew            : UIView!
    @IBOutlet var timePeriodView            : UIView!
    
    @IBOutlet var passNumberLabel           : UILabel!
    @IBOutlet var nameLabel                 : UILabel!
    @IBOutlet var validTillLabel            : UILabel!
    @IBOutlet var agencyTypeLabel           : UILabel!
    @IBOutlet var monthlyValidity           : UILabel!
    @IBOutlet var quaterlyValidity          : UILabel!
    
    @IBOutlet var monthlyFareLabel          : UILabel!
    @IBOutlet var quaterlyFareLabel         : UILabel!
    
    @IBOutlet var typeLabel                 : UILabel!
    @IBOutlet var typeLabelValue            : UILabel!
    
    @IBOutlet var concessionLabel           : UILabel!
    @IBOutlet var concessionValueLabel      : UILabel!
    
    @IBOutlet var categoryLabel             : UILabel!
    @IBOutlet var categoryValueLabel        : UILabel!
    
    @IBOutlet var viewMoreBT                : UIButton!
    @IBOutlet var renewMyPassBT             : UIButton!
    
    @IBOutlet var monthlyCheckbox           : M13Checkbox!
    @IBOutlet var quarterlyCheckbox         : M13Checkbox!
    
    @IBOutlet var monthlyLabel              : UILabel!
    @IBOutlet var quaterlyLabel             : UILabel!
    
    var selectedPass    = ""
    var passRechargeState                   : String!
    var passDuration                        = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("/***** BUS PASS RECHARGE DURATION SCREEN *******/")
        setUI()
        uiFun.createCleverTapEvent(message: "BUS PASS RECHARGE DURATION SCREEN")
        print("Successfully can read busPassList \(bestPassResponseListAction[0].status)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        timePeriodView.frame.size.height = 70
    }
    
    func setUI(){
        
        renewMyPassBT.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        renewMyPassBT.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        
        passNumberLabel.text    = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].companyIdentifier
        nameLabel.text          = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].user[0].name
        validTillLabel.text     = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].expiryDate
        agencyTypeLabel.text    = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].itemSubType
        
        monthlyFareLabel.text   = "₹ \(bestPassResponseListAction[0].bestData[0].actionDetails[1].pricingDetails[0].totalAmount)"
        if(passDurationType == "Q"){
            quaterlyFareLabel.text  = "₹ \(bestPassResponseListAction[0].bestData[0].actionDetails[0].pricingDetails[0].totalAmount)"
        }else{
            quaterlyFareLabel.isHidden = true
        }
        quarterlyCheckbox.isHidden = false
        
        monthlyValidity.text    = "Valid Till \(bestPassResponseListAction[0].bestData[0].actionDetails[0].expiryDate)"
        if(passDurationType == "Q"){
            quaterlyValidity.text   = "Valid Till \(bestPassResponseListAction[0].bestData[0].actionDetails[1].expiryDate)"
            quaterlyLabel.text = bestPassResponseListAction[0].bestData[0].actionDetails[1].validity
        }else{
            quaterlyValidity.text       = "Not Available"
            quaterlyValidity.isHidden   = true
            quarterlyCheckbox.isHidden  = true
            quaterlyLabel.isHidden      = true
            //timePeriodView.translatesAutoresizingMaskIntoConstraints = true
            UIView.animate(withDuration: 0.5, animations: {
                self.timePeriodView.frame.size.height = 84
            })
            
            
        }
        
        monthlyLabel.text = bestPassResponseListAction[0].bestData[0].actionDetails[0].validity
        
        //let hello = bestPassResponseListAction[0].bestData[0].actionDetails[0].expiryDate
    }
    
    @IBAction func viewMoreBTTapped(_ sender: AnyObject) {
        
    }
    
    @IBAction func renewMyPassBTTapeed(_ sender: AnyObject) {
        
        let defaults = UserDefaults.standard
        let profileStatus = defaults.string(forKey: "ProfileCreation")
        
        if(selectedPass == ""){
            uiFun.showAlert(title: "", message: "Please Select Pass Duration", logMessage: "Please Select Pass Duration", fromController: self)
            return
        }
        if(profileStatus == "done"){
            self.uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
        }
    }
    
    
    @IBAction func monthlyPassTapped(_ sender: AnyObject) {
        
        monthlyCheckbox.enableMorphing = true
        monthlyCheckbox.setCheckState(.checked, animated: true)
        quarterlyCheckbox.setCheckState(.unchecked, animated: true)
        
        passRechargeState = bestPassResponseListAction[0].bestData[0].actionDetails[0].validity
        print("Pass Recharge State \(passRechargeState)")
       
        rechargeAmount = bestPassResponseListAction[0].bestData[0].actionDetails[0].pricingDetails[0].totalAmount
        
        if(passRechargeState == "MONTHLY"){
            passType = 0
        }else if(passRechargeState == "QUARTERLY"){
            passType = 1
        }
       
        passDuration = bestPassResponseListAction[0].bestData[0].actionDetails[0].validity
        selectedPass = "Monthly"
        
    }
    
    @IBAction func quarterlyPassTapped(_ sender: AnyObject) {
        
        quarterlyCheckbox.enableMorphing = true
        quarterlyCheckbox.setCheckState(.checked, animated: true)
        monthlyCheckbox.setCheckState(.unchecked, animated: true)
        
        passRechargeState = bestPassResponseListAction[0].bestData[0].actionDetails[1].validity
        print("Pass Recharge State \(passRechargeState)")
        
        rechargeAmount = bestPassResponseListAction[0].bestData[0].actionDetails[1].pricingDetails[0].totalAmount
        
        if(passRechargeState == "MONTHLY"){
            passType = 0
        }else if(passRechargeState == "QUARTERLY"){
            passType = 1
        }
       
        passDuration = bestPassResponseListAction[0].bestData[0].actionDetails[1].validity
        selectedPass = "Quaterly"
    }
    
    
    
}
