//
//  FeedbackAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 24/05/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FeedbackAPI: UIViewController {

    var util        = Utillity()
    var constant    = Constants()
    var uiFun       = UiUtillity()
    var jsonString  = ""
    
    func makeAPICall(fromViewController : UserFeedbackViewController, deviceId : String, email : String, feedback : String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/recordFeedback" // Feedback API url
        
        jsonString = RouteSearchJSONString(deviceId: deviceId, email: email, feedback: feedback)! // JSON Body for api call
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: false) { result, error in
            
            // Executing api call
            if let jsonResponse = result{
                // Success in api calling
                fromViewController.gotFeedbackResponse(response: jsonResponse)
                print("Successfully Feedback API Call Repsonse \(jsonResponse) ")
                return
                
            }else {
                // Failed while executing  Api Call
                print("Route search api Drive call Failed....Try Harder Mayur ")
                return
            }
        }
    }
    
    func RouteSearchJSONString(deviceId : String, email : String, feedback : String) -> String?{
        
        // Creating Feeback API Json Body
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(deviceId, forKey: "device_id")
        para.setValue(email, forKey: "email")
        para.setValue("IOS User \(feedback)", forKey: "feedback")
        para.setValue("null", forKey: "mobile")
        para.setValue("-9", forKey: "user_id")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string route search = \(jsonString)")
            //uiFun.writeToDatabase(entityValue: jsonString, entityName: "RecentSearch", key: "plannerRecentSearchJSON")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
}
