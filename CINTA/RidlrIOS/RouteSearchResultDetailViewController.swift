//
//  RouteSearchResultViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 06/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps


let uiFun = UiUtillity()
let ridlrColors = RidlrColors()

class RouteSearchResultDetailViewController: UIViewController {
    
    @IBOutlet var backButton            : UIButton!
    @IBOutlet var timeLabel             : UILabel!
    @IBOutlet var minsLabel             : UILabel!
    @IBOutlet var toolbarView           : UIView!
    @IBOutlet var verticalScrollView1   : UIScrollView!
    
    var mapView : GMSMapView = GMSMapView()
    
    var backgroundView                  : UIView = UIView()
    var startingStationView             : UIView = UIView()
    var legsView                        : UIView = UIView()
    var destinationStationView          : UIView = UIView()
    var verticalBlackLine               : UIView = UIView()
    
    var verticalScrollView              : UIScrollView = UIScrollView()
    var verticalStackView               : UIStackView  = UIStackView()
    
    var vLayout                         : UIView        = UIView()
    
    
    let moreIV                          : UIImageView = UIImageView()
    let lessIV                          : UIImageView = UIImageView()
    
    let legMoreIV                       : UIImageView = UIImageView()
    let legLessIV                       : UIImageView = UIImageView()
    
    let endMoreIV                       : UIImageView = UIImageView()
    let endLessIV                       : UIImageView = UIImageView()
    
    var localLegList            = [WalkOnly]()
    
    var showingWalkDirection    = false
    var legCount                = 0
    var legHeight               = 0
    var yCordinateOfLeg         = 0
    var mapViewExpanded         = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // selction of mode
        if(plannerSelectedSegmentIndex == 0){
            localLegList = walkOnlyList
        }else if(plannerSelectedSegmentIndex == 1){
            localLegList = autoCabList
        }
        
        initMap()
        createVerticalScrollView()
        createVerticalStackView()
        startingPointView()
        
        var i       = 0
        var xPos    = 47
        legCount    = localLegList[plannerTripIndex].legs.count
        print("Leg Counts are \(legCount)")
        
        while (i < legCount){
            // creating image for mode
            let mode1IV : UIImageView = UIImageView()
            mode1IV.frame             = CGRect(x: xPos, y: 60, width: 18, height: 18)
            mode1IV.image             = UIImage(named: getModeImageName(modeName: localLegList[plannerTripIndex].legs[i].mode))
            toolbarView.addSubview(mode1IV)
            
            // creating image for mode2
            if(i != legCount - 1){
                let mode2IV : UIImageView = UIImageView()
                mode2IV.frame             = CGRect(x: (mode1IV.frame.origin.x + mode1IV.frame.size.width), y: 60, width: 18, height: 18)
                mode2IV.image             = UIImage(named: "ic_hardware_keyboard_arrow_right")
                toolbarView.addSubview(mode2IV)
                xPos = Int(mode2IV.frame.origin.x) + Int(mode2IV.frame.size.width)
            }
            
            // setting time label text
            timeLabel.text = "\(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].starttimestamp)) - \(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].endtimestamp))"
            minsLabel.text = "\(uiFun.convertTimestampToMinutes(arrivalTime: localLegList[plannerTripIndex].duration))"
            
            i += 1
        }
        
        timeLabel.text = "\(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].starttimestamp)) - \(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].endtimestamp))"
        print("Planner Result Detail Leg start time \(timeLabel.text)")
        
        uiFun.createCleverTapEvent(message: "FIND A ROUTE ROUTE SEARCH RESULT DETAIL SCREEN")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //createVerticalScrollView()
    }
    
    override func viewDidLayoutSubviews() {
        // crating vertical scroll bar
        super.viewDidLayoutSubviews()
        verticalScrollView.frame        = CGRect(x: 0, y: 196, width: self.view.frame.width, height: 1200)
        verticalScrollView.contentSize  = CGSize(width:self.view.frame.width, height: 1800)
        print("Scrollview height is \(legsView.frame.origin.y + legsView.frame.size.height + 80)")
    }
    
    func initMap(){
        
        // initializing map and map camera
        let camera      = GMSCameraPosition.camera(withLatitude: plannerStartingLat, longitude: plannerStartingLong, zoom: 15.0)
        mapView.frame   = CGRect(x: 0, y: 86, width: self.view.frame.width, height: 110)
        mapView         = GMSMapView.map(withFrame: mapView.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        self.view.addSubview(mapView)
        
        // map clickable area view
        let mapClickView : UIView    = UIView()
        mapClickView.frame           = CGRect(x: 0, y: 86, width: self.view.frame.width, height: 110)
        mapClickView.backgroundColor = UIColor.clear
        self.view.addSubview(mapClickView)
        
        // when user clicks on map
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(mapViewClicked))
        mapClickView.isUserInteractionEnabled = true
        mapClickView.addGestureRecognizer(tapGesture)
        
        let path = GMSMutablePath()
       
        // creating marker
        let position        = CLLocationCoordinate2D(latitude: plannerStartingLat, longitude: plannerStartingLong)
        print("Starting and Destination Lat Long \(plannerStartingLat) , \(plannerDestinationLong)")
        let titleStart      = GMSMarker(position: position)
        titleStart.icon     = UIImage(named: "ic_map_flag_start_complete")
        titleStart.opacity  = 0.6
        titleStart.map      = mapView
        path.add(CLLocationCoordinate2D(latitude: plannerStartingLat, longitude: plannerStartingLong))
        
        // assigning legList
        var legList         = [WalkOnly]()
        if(plannerSelectedSegmentIndex == 0){
            legList     =   walkOnlyList
        }else{
            legList     =   autoCabList
        }
        var i = 1
        
        
        while(i < legList[plannerTripIndex].legs.count - 1){
        
            // crating google marker
            let position        = CLLocationCoordinate2D(latitude: Double(legList[plannerTripIndex].legs[i].startingPoint[0].lat)!, longitude: Double(legList[plannerTripIndex].legs[i].startingPoint[0].lon)!)
            let titleStart      = GMSMarker(position: position)
            titleStart.icon     = UIImage(named: getModeImageName(modeName: legList[plannerTripIndex].legs[i].mode))
            titleStart.opacity  = 0.6
            titleStart.map      = mapView
            path.add(CLLocationCoordinate2D(latitude: Double(legList[plannerTripIndex].legs[i].startingPoint[0].lat)!, longitude: Double(legList[plannerTripIndex].legs[i].startingPoint[0].lon)!))
            
            // creating UI for mod
            if(legList[plannerTripIndex].legs[i].intermediatestops.count != 0){
                
                //
                var j = 0
                while (j < legList[plannerTripIndex].legs[i].intermediatestops.count){
                    
                    let position        = CLLocationCoordinate2D(latitude: Double(legList[plannerTripIndex].legs[i].intermediatestops[j].lat)!, longitude: Double(legList[plannerTripIndex].legs[i].intermediatestops[j].lon)!)
                    let titleStart      = GMSMarker(position: position)
                    titleStart.icon     = UIImage(named: getModeImageName(modeName: legList[plannerTripIndex].legs[i].mode))
                    titleStart.opacity  = 0.6
                    
                    path.add(CLLocationCoordinate2D(latitude: Double(legList[plannerTripIndex].legs[i].intermediatestops[j].lat)!, longitude: Double(legList[plannerTripIndex].legs[i].intermediatestops[j].lon)!))
                    
                    j += 1
                }
                
            }
            i += 1
        }
        
        // creating google marker for end position
        let positionEnd   = CLLocationCoordinate2D(latitude: plannerDestinationLat, longitude: plannerDestinationLong)
        let titleEnd      = GMSMarker(position: positionEnd)
        titleEnd.icon     = UIImage(named: "ic_map_flag_start_complete")
        titleEnd.opacity  = 0.6
        titleEnd.map      = mapView
        path.add(CLLocationCoordinate2D(latitude: plannerDestinationLat, longitude: plannerDestinationLong))
        
        
        // creting polylines
        let polyline = GMSPolyline(path: path)
        
        polyline.strokeColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        polyline.strokeWidth = 3.0
        polyline.geodesic    = true
        
        polyline.map = self.mapView
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        
        // back button tap action
        if(mapViewExpanded){
            mapView.frame.size.height       = 110
            verticalScrollView.isHidden     = false
            mapViewExpanded                 = false
        }else{
            uiFun.navigateToScreem(identifierName: "RouteSearchResult", fromController: self)
        }
        
    }
    
    func mapViewClicked(){
        // when user clicks on map
        mapViewExpanded = true
        print("map view gets hit")
        mapView.frame.size.height = 800
        verticalScrollView.isHidden = true
    }
    
    func createVerticalScrollView(){
        
        // creating vertical scroll bar
        verticalScrollView.frame            = CGRect(x: 0, y: 196, width: self.view.frame.width, height: 800)
        verticalScrollView.backgroundColor  = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
        
        verticalScrollView.alwaysBounceVertical             = true
        verticalScrollView.bounces                          = true
        verticalScrollView.bouncesZoom                      = false
        
        verticalScrollView.isUserInteractionEnabled         = true
        
        vLayout                 = VerticalLayout(width: view.frame.width)
        vLayout.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
        verticalScrollView.addSubview(vLayout)
        
        self.view.addSubview(verticalScrollView)
    }
    
    func createVerticalStackView(){
        
        // creating vertical stack view
        verticalStackView.frame         = CGRect(x: 8, y: 8, width: Int(self.view.frame.width - 16), height: Int(self.view.frame.height))
        verticalStackView.axis          = .vertical
        verticalStackView.distribution  = .fill
        verticalStackView.alignment     = .firstBaseline
    }
    
    func startingPointView(){
        
        // creating first view for starting position
        startingStationView.frame           = CGRect(x: 8, y: 8, width: Int(self.view.frame.width - 32), height: 120)
        startingStationView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        
        // start label
        let startLabel : UILabel = UILabel()
        startLabel.startCircularbackground(text: "S")
        startingStationView.addSubview(startLabel)
        verticalSolidBlackLine(xPos: 17, yPos: 36)
        
        // time label
        let timeLabel : UILabel = UILabel()
        timeLabel.timeLabelProperties(text: "\(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].starttimestamp))")
        startingStationView.addSubview(timeLabel)
        
        // start location label
        let startLocation : UILabel = UILabel()
        startLocation.addressLabelProperties(text: myLocation)
        startingStationView.addSubview(startLocation)
        
        // mod 1 image view
        let mode1IV : UIImageView = UIImageView()
        mode1IV.createModeImage(imageName: getModeImageName(modeName: localLegList[plannerTripIndex].legs[0].mode),yPos: 24)
        mode1IV.center.y          = 74
        startingStationView.addSubview(mode1IV)
        
        // mod 1 label
        let mode1Label : UILabel = UILabel()
        mode1Label.modeLabelProperties(text: localLegList[plannerTripIndex].legs[0].mode)
        startingStationView.addSubview(mode1Label)
        
        // mod 1 time label
        let mode1TimeLabel : UILabel = UILabel()
        mode1TimeLabel.modeTimeLabelProperties(text: "\(uiFun.convertTimestampToMinutes(arrivalTime: localLegList[plannerTripIndex].legs[0].duration)) (\(localLegList[plannerTripIndex].legs[0].distance) km)")
        startingStationView.addSubview(mode1TimeLabel)
        
        // more image view on clicking of these view will expand and shows the walk direction
        moreIV.image             = UIImage(named: "ic_indicator_expand_content")
        moreIV.frame             = CGRect(x: Int(self.view.frame.width - 64), y: 72, width: 36, height:36)
        moreIV.isHidden          = true
        startingStationView.addSubview(moreIV)
        
        // less image view on clicking of these view will shrink and hide the walk direction
        lessIV.image             = UIImage(named: "ic_indicator_collapsed_content")
        lessIV.frame             = CGRect(x: Int(self.view.frame.width - 64), y: 72, width: 36, height:36)
        lessIV.isHidden          = true
        startingStationView.addSubview(lessIV)
        
        var w           = 0
        let walkYpos    = 104
        moreIV.isHidden = true
        lessIV.isHidden = false
        
        // showing the walk instruction
        while(w < localLegList[plannerTripIndex].legs[0].walkinstructions.count){
            
            let actualYpos          = walkYpos + (22 * w)
            let walkLabel : UILabel = UILabel()
            walkLabel.walkProperties(text: localLegList[plannerTripIndex].legs[0].walkinstructions[w],yPos: actualYpos)
            startingStationView.addSubview(walkLabel)
            w += 1
        }
        
        let walkInstructionCount                = localLegList[plannerTripIndex].legs[0].walkinstructions.count
        startingStationView.frame.size.height   = CGFloat(120 + (22 * walkInstructionCount))

        // when user clicks on walk show directions
        let tapGesture                  = UITapGestureRecognizer(target: self, action: #selector(walkDirectionView))
        tapGesture.numberOfTapsRequired = 1
        moreIV.isUserInteractionEnabled = true
        moreIV.addGestureRecognizer(tapGesture)
        
        // when user clicks on walk hide directions
        let tapGesture1                  = UITapGestureRecognizer(target: self, action: #selector(hideWalkDirection))
        tapGesture1.numberOfTapsRequired = 1
        lessIV.isUserInteractionEnabled  = true
        lessIV.addGestureRecognizer(tapGesture1)
   
        vLayout.addSubview(startingStationView)
        createLegsView()
    }
    
    func walkDirectionView(){
        // show walk direction function
        var w           = 0
        let walkYpos    = 104
        var i           = 0
        let legCouts    = localLegList[plannerTripIndex].legs.count - 3
        moreIV.isHidden = true
        lessIV.isHidden = false
        
        while(w < localLegList[plannerTripIndex].legs[0].walkinstructions.count){
            
            let actualYpos          = walkYpos + (22 * w)
            let walkLabel : UILabel = UILabel()
            walkLabel.walkProperties(text: localLegList[plannerTripIndex].legs[0].walkinstructions[w],yPos: actualYpos)
            startingStationView.addSubview(walkLabel)
            w += 1
        }
        
        let walkInstructionCount                = localLegList[plannerTripIndex].legs[0].walkinstructions.count
        startingStationView.frame.size.height   = CGFloat(120 + (22 * walkInstructionCount))
        
        
        while (i < (localLegList[plannerTripIndex].legs.count - 2)){
            self.view.viewWithTag(Int("20\(i)")!)?.center.y    = mapView.center.y + startingStationView.frame.size.height + 68 + CGFloat(200 * i)
            i += 1
        }
        destinationStationView.center.y    = mapView.center.y + startingStationView.frame.size.height + 68 + CGFloat(200 * legCouts)
    }
    
    func hideWalkDirection(){
        // hide walk direction function
        moreIV.isHidden = false
        lessIV.isHidden = true
        var i = 0
        startingStationView.frame.size.height   = 102
        
        while (i < (localLegList[plannerTripIndex].legs.count - 2)){
            self.view.viewWithTag(Int("20\(i)")!)?.center.y    = mapView.center.y + startingStationView.frame.size.height + 68 + CGFloat(200 * i)
            i += 1
        }
    }
    
    func createLegsView(){
        
        var i               = 0
        
        while (i < (localLegList[plannerTripIndex].legs.count - 2)){
            
            print("Value of i \(i) and j is \(plannerTripIndex)")
            //createVerticalScrollView()
            /******************** Creating LegView *********************/
            yCordinateOfLeg = Int(startingStationView.frame.size.height) + 16 + (200 * i)
            let stopsCount              = localLegList[plannerTripIndex].legs[i + 1].intermediatestops.count
            print("Leg \(i) counts are \(stopsCount)")
            
            let emptyView                 = VerticalLayout(width: view.frame.width)
            emptyView.backgroundColor     = UIColor.white
            emptyView.tag                 = Int("20\(i)")!
            
            print("View Tag \(emptyView.tag)")
            legsView.frame           = CGRect(x: 8, y: yCordinateOfLeg, width: Int(self.view.frame.width - 16), height: 200)
            legsView.tag             = i
            legsView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
            
            let legStartTime : UILabel  = UILabel()
            legStartTime.frame          = CGRect(x: 42, y: 8, width: 160, height: 18)
            legStartTime.normlaLabelProperties(text: "\(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].legs[i + 1].starttimestamp))", fontSize: 12)
            legStartTime.textColor  = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            emptyView.addSubview(legStartTime)
            
            
            let mode1View               = HorizontalLayout(height: 18)
            mode1View.backgroundColor   = UIColor.white

            /***************** BOARD AT ******************/
            
            let legImageView : UIImageView = UIImageView()
            legImageView.createArrowImage(imageName: "right_arrow", yPos: -32)
            mode1View.addSubview(legImageView)
            
            verticalSolidBlackLine(xPos: 17, yPos: 36)
            
            let boardAt : UILabel = UILabel()
            boardAt.frame       = CGRect(x: 8, y: 0, width: 92, height:18)
            boardAt.normlaLabelProperties(text: "Board at", fontSize: 12)
            boardAt.textColor   = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            mode1View.addSubview(boardAt)
            emptyView.addSubview(mode1View)
            
            let stationName : UILabel = UILabel()
            stationName.frame       = CGRect(x: 40, y: 0, width: 160, height:18)
            stationName.normlaLabelProperties(text: "\(localLegList[plannerTripIndex].legs[i + 1].startingPoint[0].name)", fontSize: 12)
            stationName.textColor   = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            emptyView.addSubview(stationName)
            
            verticalSolidBlackLine(xPos: 17, yPos: 152)
            
            /******************** MODE DETAIL ********************/
            
            let mode2View               = HorizontalLayout(height: 18)
            mode2View.backgroundColor   = UIColor.white

            
            let leg1ImageView : UIImageView = UIImageView()
            leg1ImageView.createModeImage(imageName: getModeImageName(modeName: localLegList[plannerTripIndex].legs[i + 1].mode), yPos: 16)
            mode2View.addSubview(leg1ImageView)
            
            let leg1Label : UILabel = UILabel()
            leg1Label.frame = CGRect(x: 8, y: 16, width: (self.legsView.frame.width - 64), height: 18)
            if(localLegList[plannerTripIndex].legs[i + 1].mode == "BUS"){
                leg1Label.normlaLabelProperties(text: "\(localLegList[plannerTripIndex].legs[i + 1].routeShortName)", fontSize: 14)
            }
            else{
                leg1Label.normlaLabelProperties(text: "\(localLegList[plannerTripIndex].legs[i + 1].routeLongName)", fontSize: 14)
            }
            mode2View.addSubview(leg1Label)
            emptyView.addSubview(mode2View)
            
            verticalSolidBlackLine(xPos: 17, yPos: 36)
            
            let agencyName : UILabel = UILabel()
            agencyName.frame        = CGRect(x: 42, y: 16, width: 92, height:18)
            agencyName.normlaLabelProperties(text: localLegList[plannerTripIndex].legs[i + 1].mode,fontSize: 10)
            agencyName.textColor    = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            emptyView.addSubview(agencyName)
            
            let stopsView : UIView      = UIView()
            stopsView.frame             = CGRect(x: 42, y: 0, width: self.view.frame.size.width - 64, height: 1)
            stopsView.backgroundColor   = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
            emptyView.addSubview(stopsView)
            
            let viewMore                = HorizontalLayout(height: 36)
            viewMore.backgroundColor    = UIColor.white
            
            let stopsLabelHeading : UILabel = UILabel()
            stopsLabelHeading.frame         = CGRect(x: 42, y: 0, width: 80, height: 18)
            stopsLabelHeading.text          = "\(localLegList[plannerTripIndex].legs[i + 1].intermediatestops.count) stops \(uiFun.convertTimestampToMinutes(arrivalTime: localLegList[plannerTripIndex].legs[i + 1].duration))"
            stopsLabelHeading.font          = UIFont.systemFont(ofSize: 12.0)
            stopsLabelHeading.textColor     = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            stopsLabelHeading.sizeToFit()
            viewMore.addSubview(stopsLabelHeading)
            
                legMoreIV.image             = UIImage(named: "ic_indicator_expand_content")
                legMoreIV.frame             = CGRect(x: 32, y: 0, width: 30, height:24)
                legMoreIV.isHidden          = true
                viewMore.addSubview(legMoreIV)
                
                legLessIV.image             = UIImage(named: "ic_indicator_collapsed_content")
                legLessIV.frame             = CGRect(x: 32, y: 0, width: 30, height:36)
                legLessIV.isHidden          = true
                viewMore.addSubview(legLessIV)
            
            emptyView.addSubview(viewMore)
            
            /******************** INTERMEDIATE STOPS COUNT AND LIST **************************/
            
            var w                           = 0
            let modeYpos                    = 124
            var actualYpos                  = 0
            while(w < localLegList[plannerTripIndex].legs[i + 1].intermediatestops.count){
                
                actualYpos              = modeYpos + (22 * w)
                let stops : UILabel     = UILabel()
                stops.isHidden          = false
                stops.tag               = Int("22\(w)")!
                
                stops.stopListProperties(text: localLegList[plannerTripIndex].legs[i + 1].intermediatestops[w].name,yPos: actualYpos)
                emptyView.addSubview(stops)
                
                let stopSeparatorView : UIView      = UIView()
                stopSeparatorView.frame             = CGRect(x: 42, y: 0, width: Int(self.view.frame.size.width - 64), height: 1)
                stopSeparatorView.backgroundColor   = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
                stopSeparatorView.isHidden          = false
                emptyView.addSubview(stopSeparatorView)
                w += 1
            }
            
            /******************** GET OFF AT **************************/
            
            let mode3View               = HorizontalLayout(height: 18)
            mode3View.backgroundColor   = UIColor.white
            
            let getOffImageView : UIImageView = UIImageView()
            getOffImageView.tag               = Int("60\(i)")!
            getOffImageView.createArrowImage(imageName: "right_arrow", yPos: 72)
            getOffImageView.center.y          = 16
            mode3View.addSubview(getOffImageView)
            
            let getOffAt : UILabel = UILabel()
            getOffAt.tag           = Int("30\(i)")!
            getOffAt.frame = CGRect(x: 8, y: 8, width: 92, height:18)
            getOffAt.normlaLabelProperties(text: "Get off at", fontSize: 12)
            getOffAt.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            mode3View.addSubview(getOffAt)
            emptyView.addSubview(mode3View)
            
            let dropStation : UILabel = UILabel()
            dropStation.tag           = Int("40\(i)")!
            dropStation.frame = CGRect(x: 40, y: 8, width: 160, height:18)
            dropStation.normlaLabelProperties(text: "\(localLegList[plannerTripIndex].legs[i + 1].endPoint[0].name)", fontSize: 12)
            dropStation.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
            emptyView.addSubview(dropStation)
            
            let separatorView : UIView = UIView()
            separatorView.tag          = Int("50\(i)")!
            separatorView.frame        = CGRect(x: 0, y: 8, width: Int(self.view.frame.width - 16), height: 8)
            separatorView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
            emptyView.addSubview(separatorView)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(legShowStopsTapped(sender:)))
            tapGesture.numberOfTapsRequired = 1
            legMoreIV.isUserInteractionEnabled = true
            legMoreIV.addGestureRecognizer(tapGesture)
            vLayout.addSubview(emptyView)
            i += 1
            
        }
        
        endPointView()
    }
    
    func legShowStopsTapped(sender : UITapGestureRecognizer){
        // when user clicks on the stop in legs view
        var w           = 0
        let modeYpos    = 124
        var i           = 2
        
        let view        = sender.view
        
        print("Selected IMage Sender Id \(view?.tag)")
        
        
        legMoreIV.isHidden = true
        legLessIV.isHidden = false
        
        while(w < localLegList[plannerTripIndex].legs[1].intermediatestops.count){
            
            let actualYpos          = modeYpos + (22 * w)
            let stops : UILabel     = UILabel()
            stops.stopListProperties(text: localLegList[plannerTripIndex].legs[1].intermediatestops[w].name,yPos: actualYpos)
            self.view.viewWithTag(201)?.addSubview(stops)
            
            let stopSeparatorView : UIView      = UIView()
            stopSeparatorView.frame             = CGRect(x: 42, y: actualYpos + 16, width: Int(self.view.frame.size.width - 64), height: 1)
            stopSeparatorView.backgroundColor   = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
            self.view.viewWithTag(201)?.addSubview(stopSeparatorView)
            w += 1
        }
        
        let stopsCount                                  = localLegList[plannerTripIndex].legs[i].intermediatestops.count
        self.view.viewWithTag(201)?.frame.size.height   = CGFloat(120 + (8 * stopsCount))
        self.view.viewWithTag(301)?.center.y            = CGFloat(136 + (4 * stopsCount))
        self.view.viewWithTag(401)?.center.y            = CGFloat(154 + (4 * stopsCount))
        self.view.viewWithTag(501)?.center.y            = CGFloat(190 + (4 * stopsCount))
        self.view.viewWithTag(601)?.center.y            = CGFloat(136 + (4 * stopsCount))
        destinationStationView.frame.origin.y           = mapView.center.y + startingStationView.frame.size.height + (self.view.viewWithTag(201)?.center.y)!
        
        
        while (i < (localLegList[plannerTripIndex].legs.count - 2)){
            destinationStationView.frame.origin.y    = mapView.center.y + startingStationView.frame.size.height + (self.view.viewWithTag(201)?.frame.size.height)! + CGFloat(200 * i)
            
            i += 1
        }
        
    }
    
    func endPointView(){
        
        // craeting destination or end point view
        destinationStationView = VerticalLayout(width: self.view.frame.width)
        destinationStationView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        
        
        let walkHorizontalView      = HorizontalLayout(height: 36)
        let mode1IV : UIImageView   = UIImageView()
        mode1IV.frame               = CGRect(x: 8, y: 8, width: 24, height: 24)
        mode1IV.image               = UIImage(named: getModeImageName(modeName: localLegList[plannerTripIndex].legs[legCount].mode))
        walkHorizontalView.addSubview(mode1IV)
    
        let walkVerticalView        = VerticalLayout(width: self.view.frame.size.width)
        let mode1Label : UILabel    = UILabel()
        mode1Label.frame            = CGRect(x: 0, y: 0, width: 120, height: 18)
        mode1Label.normlaLabelProperties(text: localLegList[plannerTripIndex].legs[0].mode, fontSize: 14)
        walkVerticalView.addSubview(mode1Label)
        
        // creating destination time label
        let mode1TimeLabel : UILabel    = UILabel()
        mode1TimeLabel.frame            = CGRect(x: 0, y: 0, width: 120, height: 18)
        mode1TimeLabel.normlaLabelProperties(text: "\(uiFun.convertTimestampToMinutes(arrivalTime: localLegList[plannerTripIndex].legs[legCount].duration))",fontSize: 10)
        mode1TimeLabel.textColor        = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
        walkVerticalView.addSubview(mode1TimeLabel)
        walkHorizontalView.addSubview(walkVerticalView)
        destinationStationView.addSubview(walkHorizontalView)
        
        var w               = 0
        let endVerticalView = VerticalLayout(width: self.view.frame.size.width)
        var actualYpos      = 0
        endMoreIV.isHidden  = true
        endLessIV.isHidden  = false
        legCount            = localLegList[plannerTripIndex].legs.count
        while(w < localLegList[plannerTripIndex].legs[legCount - 1].walkinstructions.count){
            if(w == 0 ){
                actualYpos = 8
            }else{
                actualYpos = 0
            }
    
            let walkLabel : UILabel = UILabel()
            walkLabel.walkProperties(text: localLegList[plannerTripIndex].legs[self.legCount - 1].walkinstructions[w],yPos: actualYpos)
            endVerticalView.addSubview(walkLabel)
            w += 1
        }
        
        let horizontalView      = HorizontalLayout(height: 36)
        
        // creating destination start label
        let startLabel : UILabel = UILabel()
        startLabel.startCircularbackground(text: "E")
        startLabel.tag   = 803
        startLabel.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        horizontalView.addSubview(startLabel)
        
        let endDestinationView  = VerticalLayout(width: self.view.frame.size.width)
        
        // creating destination label
        let destinationLabel : UILabel  = UILabel()
        destinationLabel.addressLabelProperties(text: "\(plannerDestinationStation)")
        destinationLabel.frame          = CGRect(x: 0, y: 0, width: 220, height: 12)
        endDestinationView.addSubview(destinationLabel)
        
        // creating destination time label
        let desinationTimeLabel : UILabel   = UILabel()
        desinationTimeLabel.frame           = CGRect(x: 8, y: 0, width: 120, height: 18)
        desinationTimeLabel.normlaLabelProperties(text: "\(uiFun.convertTimestampToHourMinute(arrivalTime: localLegList[plannerTripIndex].endtimestamp))", fontSize: 10)
        desinationTimeLabel.textColor       = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
        desinationTimeLabel.tag             = 801
        endDestinationView.addSubview(desinationTimeLabel)
        
        // adding all views into horizontal view
        horizontalView.addSubview(endDestinationView)
        endVerticalView.addSubview(horizontalView)
        destinationStationView.addSubview(endVerticalView)
        
        legCount    = localLegList[plannerTripIndex].legs.count
        
        // destination image view
        if(localLegList[plannerTripIndex].legs[legCount - 1].walkinstructions.count != 0){
            endMoreIV.image             = UIImage(named: "ic_indicator_expand_content")
            endMoreIV.frame             = CGRect(x: Int(self.view.frame.width - 64), y: 8, width: 36, height:36)
            destinationStationView.addSubview(endMoreIV)
            
            endLessIV.image             = UIImage(named: "ic_indicator_collapsed_content")
            endLessIV.frame             = CGRect(x: Int(self.view.frame.width - 64), y: 8, width: 36, height:36)
            endLessIV.isHidden          = true
            destinationStationView.addSubview(endLessIV)
        }
        
        // destination walk direction show
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(walkDirectionViewEnd))
        tapGesture.numberOfTapsRequired = 1
        endMoreIV.isUserInteractionEnabled = true
        endMoreIV.addGestureRecognizer(tapGesture)
        
        // destination walk direction hide action
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(hideWalkDirectionEnd))
        tapGesture1.numberOfTapsRequired = 1
        endLessIV.isUserInteractionEnabled = true
        endLessIV.addGestureRecognizer(tapGesture1)
        
        vLayout.addSubview(destinationStationView)
    }
    
    func walkDirectionViewEnd(){
        // show walk direction for destination view
        var w           = 0
        let walkYpos    = 104
        
        endMoreIV.isHidden = true
        endLessIV.isHidden = false
        
        while(w < localLegList[plannerTripIndex].legs[legCount - 1].walkinstructions.count){
            
            let actualYpos          = walkYpos + (22 * w)
            let walkLabel : UILabel = UILabel()
            walkLabel.walkProperties(text: localLegList[plannerTripIndex].legs[self.legCount - 1].walkinstructions[w],yPos: actualYpos)
            destinationStationView.addSubview(walkLabel)
            w += 1
        }
        
        let walkInstructionCount                            = localLegList[plannerTripIndex].legs[0].walkinstructions.count
        destinationStationView.viewWithTag(801)?.center.y   = CGFloat(68 + (18 * walkInstructionCount))
        destinationStationView.viewWithTag(802)?.center.y   = CGFloat(54 + (18 * walkInstructionCount))
        destinationStationView.viewWithTag(803)?.center.y   = CGFloat(54 + (18 * walkInstructionCount))
        destinationStationView.frame.size.height            = CGFloat(120 + (22 * walkInstructionCount))
    }
    
    func hideWalkDirectionEnd(){
    }
    
    func verticalSolidBlackLine(xPos : Int, yPos : Int){
        
        verticalBlackLine.frame = CGRect(x: xPos, y: yPos, width: 3, height: 26)
        verticalBlackLine.backgroundColor = UIColor.black
        startingStationView.addSubview(verticalBlackLine)
    }
    
    func getModeImageName(modeName : String) -> String{
        
        if(modeName == "WALK"){
            return "ic_indicator_mode_walk_holo"
        }
        if(modeName == "RAIL"){
            return "ic_indicator_mode_train_holo"
        }
        if(modeName == "BUS"){
            return "ic_indicator_mode_bus_holo"
        }
        if(modeName == "AUTO"){
            return "ic_indicator_mode_rickshaw_holo"
        }
        if(modeName == "METRO"){
            return "ic_indicator_mode_metro_holo"
        }else{
            return ""
        }
    }
}



extension UILabel{
    
    
    func normlaLabelProperties(text : String, fontSize : Int){
        
        self.text               = text
        self.textAlignment      = .left
        self.font               = UIFont.boldSystemFont(ofSize: CGFloat(fontSize))
        self.textColor          = UIColor.black
        
    }
    func timeLabelProperties(text : String){
        
        self.frame          = CGRect(x: 42, y: 16, width: 190, height:18)
        self.text           = text
        self.textAlignment  = .left
        self.font           = UIFont.boldSystemFont(ofSize: 12)
        self.textColor      = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
    }
    
    func addressLabelProperties(text : String){
        
        self.frame          = CGRect(x: 42, y: 34, width: 220, height:18)
        self.text           = text
        self.textAlignment  = .left
        self.font           = UIFont.boldSystemFont(ofSize: 12)
        self.textColor      = UIColor.black
    }
    
    func modeLabelProperties(text: String){
        
        self.frame              = CGRect(x: 42, y: 68, width: 190, height:18)
        self.text               = text
        self.textAlignment      = .left
        self.font               = UIFont.boldSystemFont(ofSize: 14)
        self.textColor          = UIColor.black
    }
    
    func modeTimeLabelProperties(text : String){
        
        self.frame              = CGRect(x: 42, y: 84, width: 190, height:18)
        self.text               = text
        self.textAlignment      = .left
        self.font               = UIFont.boldSystemFont(ofSize: 12)
        self.textColor          = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
    }
    
    func startCircularbackground(text:String){
        
        self.frame                  = CGRect(x: 8, y: 16, width: 24, height: 24)
        self.text                   = text
        self.textAlignment          = .center
        self.font                   = UIFont.boldSystemFont(ofSize: 12)
        self.textColor              = UIColor.white
        self.layer.cornerRadius     = 12.0
        self.layer.backgroundColor  = UIColor.black.cgColor
    }
    
    func walkProperties(text : String, yPos : Int){
        
        self.frame                  = CGRect(x: 42, y: yPos, width: 220, height: 18)
        self.text                   = text
        self.textAlignment          = .left
        self.font                   = UIFont.boldSystemFont(ofSize: 12)
        self.textColor              = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
    }
    
    func stopListProperties(text : String, yPos : Int){
        
        self.frame                  = CGRect(x: 42, y: 0, width: 260, height: 18)
        self.text                   = text
        self.textAlignment          = .left
        self.font                   = UIFont.boldSystemFont(ofSize: 12)
        self.textColor              = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
    }
}

extension UIImageView{
    
    func createModeImage(imageName : String, yPos : Int){
        
        self.frame                  = CGRect(x: 8, y: 16, width: 24, height: 24)
        self.image                  = UIImage(named: imageName)
    }
    
    func createArrowImage(imageName : String, yPos : Int){
        self.frame                  = CGRect(x: 8, y: 0, width: 24, height: 24)
        self.image                  = UIImage(named: imageName)
        self.layer.cornerRadius     = 12
        self.layer.backgroundColor  = UIColor.black.cgColor
    }
}

extension UIView{
    
    func createEmptyView(yCord : Int, width : Int){
        
        self.frame              = CGRect(x: 8, y: yCord, width: width, height: 400)
        self.backgroundColor    = UIColor.white
    }
}

