//
//  RecentSearch+CoreDataProperties.swift
//  
//
//  Created by Mayur on 22/05/17.
//
//

import Foundation
import CoreData


extension RecentSearch {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RecentSearch> {
        return NSFetchRequest<RecentSearch>(entityName: "RecentSearch");
    }

    @NSManaged public var agencyId: String?
    @NSManaged public var mode0: String?
    @NSManaged public var mode1: String?
    @NSManaged public var mode2: String?
    @NSManaged public var mode3: String?
    @NSManaged public var mode11: String?
    @NSManaged public var planner1: String?
    @NSManaged public var plannerRecentSearchJSON: String?
    @NSManaged public var recentSearchDestinationLat: String?
    @NSManaged public var recentSearchDestinationLong: String?
    @NSManaged public var recentSearchStartLat: String?
    @NSManaged public var recentSearchStartLong: String?
    @NSManaged public var routeId: String?
    @NSManaged public var stopId: String?

}
