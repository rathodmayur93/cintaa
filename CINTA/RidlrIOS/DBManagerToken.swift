//
//  DBManagerToken.swift
//  RidlrIOS
//
//  Created by Mayur on 31/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class DBManagerToken: NSObject {

    let field_BookingItemId         = "bookingItemId"
    let field_Status                = "status"
    let field_QrCodeId              = "qrCodeId"
    let field_Type                  = "type"
    let field_QrCode                = "qrCode"
    let field_Expiry                = "expiry"
    let field_itemSubType           = "itemSubType"
    
    let field_sourceName          = "sourceName"
    let field_destinationName     = "destinationName"
    let field_sourceId            = "sourceId"
    let field_destinationId       = "destinationId"
    
    
    var pathToDatabase  : String!
    var database        : FMDatabase!
    static let shared   : DBManagerToken     = DBManagerToken()
    let databaseFileName                     = "databaseToken.sqlite"
    
    
    override init() {
        super.init()
        
        let documentsDirectory = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createTokenTableQuery = "create table if not exists tokenData (\(field_QrCodeId) varchar primary key not null, \(field_BookingItemId) varchar not null, \(field_Status) varchar not null, \(field_Type) varchar not null, \(field_QrCode) varchar, \(field_Expiry) varchar not null, \(field_itemSubType) varchar not null, \(field_sourceName) varchar not null,\(field_destinationName) varchar not null,\(field_sourceId) varchar not null,\(field_destinationId) varchar not null)"
                    
                    do {
                        try database.executeUpdate(createTokenTableQuery, values: nil)
                        created = true
                    }
                    catch {
                        print("Could not create table.")
                        print(error.localizedDescription)
                    }
                    
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        
        return false
    }
    
    
    func insertTokenData(bookingItemList : [MetroTokenJSONModel]){
        
        if openDatabase(){
            
            var query = ""
            
            for i in 0..<bookingItemList.count{
                for j in 0..<bookingItemList[i].newMetroToken.count{
                
                    let status           = bookingItemList[i].newMetroToken[j].status
                    let qrCodeId         = bookingItemList[i].newMetroToken[j].qrCodeId
                    let type             = bookingItemList[i].newMetroToken[j].type
                    let qrCode           = bookingItemList[i].newMetroToken[j].qrCode
                    let expiry           = bookingItemList[i].newMetroToken[j].expiry
                    let bookingItemId    = bookingItemList[i].companyRefId
                    let itemSubType      = bookingItemList[i].itemSubType
                    
                    let sourceName          = bookingItemList[i].routeDetail[0].sourceName
                    let destinationName     = bookingItemList[i].routeDetail[0].destinationName
                    let sourceId            = bookingItemList[i].routeDetail[0].sourceId
                    let destinationId       = bookingItemList[i].routeDetail[0].destinationId
                
                    query += "insert into tokenData (\(field_QrCodeId), \(field_BookingItemId), \(field_Status), \(field_Type), \(field_QrCode), \(field_Expiry), \(field_itemSubType), \(field_sourceName), \(field_destinationName), \(field_sourceId), \(field_destinationId)) values ('\(qrCodeId)', '\(bookingItemId)', '\(status)', '\(type)', '\(qrCode)', '\(expiry)', '\(itemSubType)', '\(sourceName)', '\(destinationName)', '\(sourceId)', '\(destinationId)');"
                    
                }
            }
            
            print("Token Insert Query Is \(query)")
            
            if !database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }
            
            database.close()
        }
    }
    
    
    func fetchBookingItems(itemSubType : String) -> [MetroTokenJSONModel]{
        
        var bookingItems = [MetroTokenJSONModel]()
        
        if openDatabase() {
            let query = "select * from tokenData where \(field_itemSubType) = '\(itemSubType)'"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let metroTokenModel                       = MetroTokenJSONModel()
                    let newMetroToken                         = NewMetroTokenModel()
                    
                    metroTokenModel.bookingItemId             = results.string(forColumn: field_BookingItemId)
                    metroTokenModel.itemSubType               = results.string(forColumn: field_itemSubType)
                    
                    newMetroToken.status                      = results.string(forColumn: field_Status)
                    newMetroToken.qrCodeId                    = results.string(forColumn: field_QrCodeId)
                    newMetroToken.type                        = results.string(forColumn: field_Type)
                    newMetroToken.qrCode                      = results.string(forColumn: field_QrCode)
                    newMetroToken.expiry                      = results.string(forColumn: field_Expiry)
                    
                    newMetroToken.sourceName                  = results.string(forColumn: field_sourceName)
                    newMetroToken.destinationName             = results.string(forColumn: field_destinationName)
                    newMetroToken.sourceId                    = results.string(forColumn: field_sourceId)
                    newMetroToken.destinationId               = results.string(forColumn: field_destinationId)
                    
                    
                    metroTokenModel.newMetroToken.append(newMetroToken)
                    bookingItems.append(metroTokenModel)
                    
                    print("QRCode Id or Slave ID \(results.string(forColumn: field_QrCodeId))")
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return bookingItems
    }
    
    func updateTokenData(withID qrCodeId: String, newQRcodeId: String, qrCode: String, status : String, type : String, expiry : String, source : String, destination : String, sourceId : String, destinationId : String, itemSubType : String) {
        if openDatabase() {
            print("Old QR Code Id \(qrCodeId) & newQRCodeId \(newQRcodeId)")
            let query = "update tokenData set \(field_Status)=?, \(field_Type)=?, \(field_QrCodeId)=?, \(field_QrCode)=?, \(field_Expiry)=?, \(field_sourceName)=?, \(field_destinationName)=?, \(field_sourceId)=?, \(field_destinationId)=? where \(field_QrCodeId)=?"
            
            do {
                try database.executeUpdate(query, values: [status, type, newQRcodeId, qrCode, expiry, source, destination, sourceId, destinationId, qrCodeId])
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
    }
    
    
    func databaseStatus() -> Bool{
    
        var results : FMResultSet!
        if openDatabase() {
            let query = "select * from tokenData"
            
            do {
                print(database)
                results = try database.executeQuery(query, values: nil)
                print("number of rows in database \(results.int(forColumnIndex: 0))")
            
            }
            catch {
                print(error.localizedDescription)
                
            }
            
            database.close()
        }
        if(results == nil){
            return false
        }else{
            return true
        }
    }
    
    func fetchBookingId(qrCodeId : String) -> String{
    
        var results : FMResultSet!
        var bookingIdValue = ""
        if openDatabase() {
            let query = "select \(field_BookingItemId) from tokenData where \(field_QrCodeId)= '\(qrCodeId)'"
            
            do {
                print(database)
                results = try database.executeQuery(query, values: nil)
                print("booking item of qrCode is \(results.string(forColumn: field_BookingItemId))")
                
                bookingIdValue = results.string(forColumn: field_BookingItemId)
                print("Fetching bookingItemId \(bookingIdValue) where qrCodeId is \(qrCodeId)")
                
            }
            catch {
                print(error.localizedDescription)
                
            }
            
            database.close()
        }
        
        return bookingIdValue
    }
    
    func deleteBookingItem(qrCodeId : String) -> Bool{
        
        var deleted = false
        
        if openDatabase() {
            let query = "delete from tokenData where \(field_QrCodeId)= '\(qrCodeId)'"
            
            database.executeStatements(query)
            deleted = true
            
            database.close()
        }
        
        return deleted
    }
    
}
