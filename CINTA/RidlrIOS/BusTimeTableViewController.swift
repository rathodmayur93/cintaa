//
//  BusTimeTableViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 01/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SWSegmentedControl

class BusTimeTableViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let sc = SWSegmentedControl(items: ["A", "B", "C"])
        sc.frame = CGRect(x: 166, y: 323, width: 300, height: 44)
        sc.selectedSegmentIndex = 2 // default to 0
        sc.setSelectedSegmentIndex(1, animated: true)
        sc.titleColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        sc.indicatorColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
