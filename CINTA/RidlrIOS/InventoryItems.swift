//
//  InventoryItems.swift
//  RidlrIOS
//
//  Created by Mayur on 11/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class InventoryItems {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(itemSubtype : String, identifier  :String, item : String, fromViewController : MetroHomeScreenViewController){
        
        let randomNum:UInt32            = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String      = String(randomNum)
        let timeStamp                   = Int(NSDate().timeIntervalSince1970)

        
        let url = "\(constant.METRO_TOKEN)/inventory/item?random=\(randomNumString)&company=RELIANCE_METRO&itemSubType=\(itemSubtype)&identifier=\(identifier)&item=\(item)&current_time=\(timeStamp)"
        
        print("Metro Token Inventory Items \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                fromViewController.gotInventoryResponse(response: jsonResponse)
                print("Successfully Token Inventory fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Status API Metro Token failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }

}
