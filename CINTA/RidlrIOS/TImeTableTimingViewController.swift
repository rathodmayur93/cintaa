//
//  TImeTableTimingViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 23/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var timetableTripsValues            = [TripModel]()
var sortedTimetableTripValues       = [TripValues]()

var BusTripValues                   = [BusTripListModel]()


var busTimetableTripValues          = [TripModel]()
var commonTripValues                = [TripModel]()
var trainTimetableTripValues        = [TripModel]()
var metroTimetableTripValues        = [TripModel]()
var monoTimetableTripValues         = [TripModel]()

var timetabelTimingSortedList       = [String]()
var legTimetabelTimingSortedList    = [String]()
var legTimetabelTimingSortedList2   = [String]()
var busTimetableTripList            = [String]()
var busCacheTripValues              = [String]()

var tripDetailIndex                 : Int?
var doneWithTripDetail              = false
var timeChangeValue                 = ""
var hopeViewVisibility              = false
var tableViewHeight                 = 81
var agencyIdValueSelected           = ""
var routeIdValueSelected            = ""
var tripType                        = ""
var hopeCount   : Int?              = nil
var hopeStationName1                = ""
var hopeStationName2                = ""
var hopSelected                     = false
var hop2Selected                    = false
var hopIndex                        :Int?
var tableViewScrollIndex            = 0

class TImeTableTimingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate {

    
    @IBOutlet var closeButtton          : UIButton!
    @IBOutlet var nowButton             : UIButton!
    @IBOutlet var changeButton          : UIButton!
    
    @IBOutlet var startingStationLabel  : UILabel!
    @IBOutlet var destinationLabel      : UILabel!
    @IBOutlet var timingLabel           : UILabel!
    
    @IBOutlet var timeTableTimingTable  : UITableView!
    var arrival                         = ""
    let uiFun                           = UiUtillity()
    let ridlrColors                     = RidlrColors()
    
    var hour                            : Int?
    var minutes                         : Int?
    
    var rowHeight                       = 80
    var distanceBetweenTwoCell          = 0
    

    var indicator                       = UIActivityIndicatorView()
    var alert: UIAlertView              = UIAlertView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setUI()
        fetchTripListCache()
        print("Common Trip Values Count Code Optimization \(commonTripValues.count)")
        
        uiFun.createCleverTapEvent(message: "TIMETABLE MODE RESULT SCREEN")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    
        // Check for agency value
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
                return busTimetableTripList.count       // List for bus number and name
            }else if(busSegmentSelectedIndex == 1){
                return commonTripValues.count           // List for bus stop name
            }
        }else{
            return commonTripValues.count               // For Train, Metro and Mono
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        // Intializing table view cell
        let cell         = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TImeTableTimingViewCell
        cell.tag         = indexPath.row
        print("Index Path of table view in timing view controller \(indexPath.row)")
        
        if(tableViewScrollIndex == indexPath.row){
            cell.currentCellBand.isHidden = false
        }else{
            cell.currentCellBand.isHidden = true
        }
        
            /**************** BUS TIMETABLE TIMING **************/
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
                
            let timeArray           = busTimetableTripList[indexPath.row].components(separatedBy: ":")
            cell.hope1View.isHidden = true
            cell.hopeTime.isHidden  = true
            cell.hope2View.isHidden = true
            
                if(Int(timeArray[0])! > 12){
                    let hour = Int(timeArray[0])! - 12
                    cell.timeLabel.text = "\(hour):\(timeArray[1]) pm"      // Setting
                    cell.startsFromStationLabel.text = ""
                    cell.destionStationNameLabel.text = ""
                    cell.agencyName.text = ""
                    cell.hopeView.isHidden = true
                    rowHeight = 42
                }
                else{
                    cell.timeLabel.text = "\(timeArray[0]):\(timeArray[1]) am"
                    cell.startsFromStationLabel.text = ""
                    cell.destionStationNameLabel.text = ""
                    cell.agencyName.text = ""
                    cell.hopeView.isHidden = true
                    rowHeight = 42
                }
        }else{
        
        /**************** TRAIN, METRO and MONO TIMETABLE TIMING **************/
          
    if(commonTripValues.count != 0){
        let findTimeFromLlist   = commonTripValues[indexPath.row].tripValues[0].sStartTime
        let separateDateTime    = findTimeFromLlist.components(separatedBy: " ")
        let timeArray           = separateDateTime[1].components(separatedBy: ":")
        
                    
        if(Int(timeArray[0])! > 12){
            let hour = Int(timeArray[0])! - 12
            cell.timeLabel.text = "\(hour):\(timeArray[1]) pm"
        }else{
            cell.timeLabel.text = "\(timeArray[0]):\(timeArray[1]) am"
        }
        
        let modeOfTrain         =   commonTripValues[indexPath.row].tripValues[0].sRouteId[0].routeLongName
        let slowFastArray       =   modeOfTrain.components(separatedBy: "-")
       
        print("timetable mode id \(timetableModeId)")
         /**************** SETTING STARTING AND DESTINATION LABEL **************/
        
        if(timetableModeId == "2"){
            cell.destionStationNameLabel.text = "\(commonTripValues[indexPath.row].tripValues[0].sRouteId[0].routeDestinationName)-\(slowFastArray[1])"
        }else{
            let destinationStop               = commonTripValues[indexPath.row].tripValues[0].sRouteId[0].routeLongName
            let destinationName               = destinationStop.components(separatedBy: "To ")
            cell.destionStationNameLabel.text = destinationName[1]
        }
        if(busSegmentSelectedIndex == 1 && travelModeSelectedIndex == 1){
            cell.destionStationNameLabel.text = "\(commonTripValues[indexPath.row].tripValues[0].sRouteId[0].routeShortName)"
        }
                    
        cell.startsFromStationLabel.text = "Starts from \(commonTripValues[indexPath.row].tripValues[0].sRouteId[0].routeSourceName)"
        cell.agencyName.text             = "\(commonTripValues[indexPath.row].tripValues[0].sRouteId[0].agency)"
        
        hopeCount                        = Int(commonTripValues[indexPath.row].tripValues[0].hopCountTotal) // Hop Count
        print("Hop Count is \(hopeCount!)")
        
        if(hopeCount == 0){
            // Hop count is 0
            cell.hope1View.isHidden = true
            cell.hope2View.isHidden = true
            rowHeight               = 72
        }
        if(hopeCount! >= 1){
            // hop count is 1
            cell.hope1View.isHidden = false
            
            let findTimeFromLlistLeg1   = commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sStartTime
            let separateDateTimeLeg1    = findTimeFromLlistLeg1.components(separatedBy: " ")
            let legTimeArray            = separateDateTimeLeg1[1].components(separatedBy: ":")
            
            if(Int(legTimeArray[0])! > 12){
                let hour            = Int(legTimeArray[0])! - 12
                cell.hopeTime.text  = "\(hour):\(legTimeArray[1]) pm"
            
            }else{
                cell.hopeTime.text  = "\(legTimeArray[0]):\(legTimeArray[1]) am"
            }
            
            cell.changeAtHopeNameLabel.text = "change at \(commonTripValues[indexPath.row].tripValues[0].sTripDestName)"
            hopeStationName1                = commonTripValues[indexPath.row].tripValues[0].sTripDestName
            
            if(busSegmentSelectedIndex == 1 && travelModeSelectedIndex == 1){
                cell.hopeStationName.text           = "\(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeShortName)"
            }
            else{
                    let destinationStop             =   commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeLongName
                    let hopDestinationName          = destinationStop.components(separatedBy: "To ")
                    cell.hopeStationName.text       = hopDestinationName[1]
            }
            
            cell.hopeStartsFrom.text                = "Starts from \(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeSourceName)"
           
            cell.hopeAgency.text                    = "\(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].agency)"
            
            rowHeight               = 170
            cell.hope2View.isHidden = true
        }
        
        if(hopeCount == 2){
            // Hop count is 2
            cell.hope2View.isHidden     = false
            
            let findTimeFromLlistLeg2   = commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sStartTime
            let separateDateTimeLeg2    = findTimeFromLlistLeg2.components(separatedBy: " ")
            let legTimeArray2           = separateDateTimeLeg2[1].components(separatedBy: ":")
            
            if(Int(legTimeArray2[0])! > 12){
                let hour                = Int(legTimeArray2[0])! - 12
                cell.hopeTime2.text     = "\(hour):\(legTimeArray2[1]) pm"
            
            }else{
                cell.hopeTime2.text     = "\(legTimeArray2[0]):\(legTimeArray2[1]) am"
            }
            
            cell.changeAtHopeNameLabel2.text    = "change at \(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sTripDestName)"
            hopeStationName2                    = commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sTripDestName
            // Check for bus
            if(busSegmentSelectedIndex == 1 && travelModeSelectedIndex == 1){
                cell.hopeStationName.text       = "\(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeShortName)"
            }
            else{
                
                let destinationStop              =   commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeLongName
                let hopDestinationName           = destinationStop.components(separatedBy: "To ")
                cell.hopeStationName2.text       = hopDestinationName[1]
            }
            
            cell.hopeStartsFrom2.text        = "Starts from \(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeSourceName)"
            
            cell.hopeAgency2.text            = "\(commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].agency)"
            
            rowHeight  = 280
        }
        
            // Tap action on hop 1
            let tapGesture1                     = UITapGestureRecognizer(target: self, action: #selector(hope1Selected))
            tapGesture1.cancelsTouchesInView    = false
            tapGesture1.numberOfTapsRequired    = 1
            cell.hope1View.addGestureRecognizer(tapGesture1)
            cell.hope1View.isUserInteractionEnabled = true
        
            // Tap action on hop 2
            let tapGesture2                     = UITapGestureRecognizer(target: self, action: #selector(hope2Selected))
            tapGesture2.cancelsTouchesInView    = false
            tapGesture2.numberOfTapsRequired    = 1
            cell.hope2View.addGestureRecognizer(tapGesture2)
            cell.hope2View.isUserInteractionEnabled = true
            
        }
    }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // Creating layout for each row
        var whiteRoundedView : UIView       = UIView()
        cell.contentView.backgroundColor    = uiFun.hexStringToUIColor(hex: ridlrColors.newsFeed_bg_color)
        
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
             whiteRoundedView  = UIView(frame: CGRect(x: 0, y: 1, width: self.view.frame.width, height: CGFloat(Float(rowHeight))))
        }else{
             whiteRoundedView  = UIView(frame: CGRect(x: 0, y: 8, width: self.view.frame.width, height: CGFloat(Float(rowHeight))))
        }
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("Cell Selecterd Index \(indexPath.row)")
        print("Hop Selection \(hopSelected)")
        
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
            hopeCount = 0
      
        }else{
            hopeCount        = Int(commonTripValues[indexPath.row].tripValues[0].hopCountTotal)
            hopeStationName1 = commonTripValues[indexPath.row].tripValues[0].sTripDestName
            hopeStationName2 = commonTripValues[indexPath.row].tripValues[0].nextEarliestTripLegModel[0].sTripDestName
            print("Selected Cell hop Count is \(hopeCount)")
        
        }
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
           if(tripType == "up"){
                tripId = BusTripValues[0].up[indexPath.row].id
            
           }else if(tripType == "down"){
                tripId = BusTripValues[0].down[indexPath.row].id
            }
        }else{
            routeIdValueSelected  = commonTripValues[indexPath.row].tripValues[0].sRouteId[0].routeId
            agencyIdValueSelected = commonTripValues[indexPath.row].tripValues[0].agency[0].agencyId
            print("Route ID Value is \(routeIdValueSelected) and agency id \(agencyIdValueSelected)")
        }
        tripDetailIndex = indexPath.row
        uiFun.navigateToScreem(identifierName: "TimetableMapViewController", fromController: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return CGFloat(Float(rowHeight))
    }
    
    func hope1Selected(){
        // When user clicks on hop 1
        hopSelected           = true
        print("Hope Selection In Its Own Method \(hopSelected)")
    }
    
    func hope2Selected(){
        // When user clicks on hop 2
         hop2Selected = true
         print("Hope Selection 2 In Its Own Method \(hop2Selected)")
    }
    
    func fetchTripListCache(){
    
        timeTableTimingTable.reloadData()
        // Storing trip result into temp variable for better UX
        switch travelModeSelectedIndex {
        case 0:
            if(trainTimetableTripValues.count == 0){
                distanceBetweenTwoCell = 8
                FetchBusRouteTripDetail()
            }else{
                commonTripValues = trainTimetableTripValues
                ScrollTableview()
                
            }
            break
        case 1:
            
            switch busSegmentSelectedIndex {
            case 0:
                if(busCacheTripValues.count == 0){
                    distanceBetweenTwoCell = 2
                    FetchBusTimetableAPICall()
                }else{
                    busTimetableTripList = busCacheTripValues
                    ScrollAccordingCurrentTime()
                }
                break
            case 1:
                if(busTimetableTripValues.count == 0){
                    distanceBetweenTwoCell = 8
                    FetchBusRouteTripDetail()
                }else{
                    commonTripValues = busTimetableTripValues
                    ScrollTableview()
                }
                
                break
            default:
                break
            }
        case 2:
            if(metroTimetableTripValues.count == 0){
                distanceBetweenTwoCell = 8
                FetchBusRouteTripDetail()
            }else{
                commonTripValues = metroTimetableTripValues
                ScrollTableview()
            }
            break
        case 3:
            if(monoTimetableTripValues.count == 0){
                distanceBetweenTwoCell = 8
                FetchBusRouteTripDetail()
            }else{
                commonTripValues = monoTimetableTripValues
                ScrollTableview()
            }
            break
        default:
            break
        }
    }
    
    func nowTimeValues(){
    
        // Fetching current date and time
        let date            = NSDate()
        let calendar        = NSCalendar.current
        
        var minuteString    = ""
        
        hour                = calendar.component(.hour, from: date as Date)
        minutes             = calendar.component(.minute  , from: date as Date)
    
        if(String(describing : hour!).characters.count == 1){
            hour = Int("0\(hour!)")
        }
        if(String(describing : minutes!).characters.count == 1){
            minuteString = "0\(minutes!)"
        }else{
            minuteString = "\(minutes!)"
        }
        
        if(hour! > 12){
            let actualHour   = Int(hour!) - 12
            timingLabel.text = "Today, \(actualHour):\(minuteString) PM"
        }else{
            timingLabel.text = "Today, \(hour!):\(minuteString) AM"
        }
    }
    
    func setUI(){
        
        
        activityIndicator()
        
        startingStationLabel.text       = startingStationName
        destinationLabel.text           = destinationStationName
        
        // Now Button background UI
        nowButton.layer.cornerRadius    = 5
        nowButton.backgroundColor       = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        nowButton.layer.borderWidth     = 1
        nowButton.layer.borderColor     = UIColor.white.cgColor
        nowButton.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor), for: .normal)
        
        // Change Button background UI
        changeButton.layer.cornerRadius = 5
        changeButton.backgroundColor    = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        changeButton.layer.borderWidth  = 1
        changeButton.layer.borderColor  = UIColor.white.cgColor
        changeButton.setTitleColor(UIColor.white, for: .normal)
        
        if(timeChange){
            // If time is change by clicking on CHange button
            var timeChangeMinutesString     = ""
            print("Time has been change in TimeTableTimingViewcontroller")
            nowButton.layer.cornerRadius    = 5
            nowButton.backgroundColor       = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
            nowButton.layer.borderWidth     = 1
            nowButton.layer.borderColor     = UIColor.white.cgColor
            nowButton.setTitleColor(UIColor.white, for: .normal)
            
            changeButton.layer.cornerRadius = 5
            changeButton.backgroundColor    = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
            changeButton.layer.borderWidth  = 1
            changeButton.layer.borderColor  = UIColor.white.cgColor
            changeButton.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor), for: .normal)
            
            
            if(String(describing : timeChangeMinute!).characters.count == 1){
                timeChangeMinutesString = "0\(timeChangeMinute!)"
            
            }else{
                timeChangeMinutesString = "\(timeChangeMinute!)"
            }
            timingLabel.text            = "\(timeChangeDate), \(timingLabelValue!):\(timeChangeMinutesString) \(timeChangeMode!)"
            
            timeTableTimingTable.reloadData()
        }else{
            nowTimeValues()
        }
    }
    
    func activityIndicator() {
        // Loader
        indicator.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func FetchBusTimetableAPICall(){
        // Fetching Bus Trip List API Call
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
        
        let date1                 = Date()
        let calendar1             = Calendar.current
        let components1           = calendar1.dateComponents([.year, .month, .day], from: date1)
        
        let year                  =  components1.year!
        let month                 = components1.month!
        let day                   = components1.day!
        
        let actualMonth     = uiFun.getTwoDigitTime(time: String(describing: month))
        let actualDay       = uiFun.getTwoDigitTime(time: String(describing: day))
        
        arrival             = "\(year)\(actualMonth)\(actualDay)"
        
        let busTimetable    = BusTripListAPI()
        busTimetable.makeAPICall(fromViewController: self, agencyId: agencyId, routeId: routeId, stopId: stopId, date: arrival)
    }
    
    func FetchModeTimetableAPICall(){
        // Fetching Agency Trip List
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
        
        let fetchTrips = FetchModeTimetableAPI()
        fetchTrips.makeAPICall(fromViewController: self, jsonString: TimetableTripJSONBody(), cityId: cityIdFromData, modeId: timetableModeId)
    }
    
    func FetchModeTripResponse(response : JSON){
        // Processing json response of FetchModeTimetableAPICall
        processTripJSON(response: response)
        
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
    }
    
    func FetchBusTripList(response : JSON){
        // Processing json response of FetchModeTimetableAPICall
        print("Got Bus Trip List Result")
        processBusTripJSON(response: response)
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
    }
    
    func FetchBusRouteTripDetail(){
        // Fetching Bus Route Trip List
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
        let busTripRouteAPI = FetchBusRouteDetailAPICall()
        busTripRouteAPI.makeAPICall(fromViewController: self, jsonString: TimetableTripJSONBody(), cityId: cityIdFromData, modeId: timetableModeId)
    }
    
    func FetchBusRouteTripResponse(response : JSON){
        // Processing json response of FetchBusRouteTripDetail
        self.processTripJSON(response: response)
        
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        print("Got Trip Response \(response)")
        
    }
    
    func TimetableTripJSONBody() -> String{
        // JSON String for trip detail api call
        let date            = NSDate()
        let calendar        = NSCalendar.current
        let hour            = calendar.component(.hour, from: date as Date)
        let minutes         = calendar.component(.minute, from: date as Date)
        let seconds         = calendar.component(.second, from: date as Date)
        
        let date1           = Date()
        let components1     = calendar.dateComponents([.year, .month, .day], from: date1)
        
        let year            = components1.year!
        let month           = components1.month!
        let day             = components1.day!
        
        if(timeChange){
            arrival = timeChangeValue
        }else{
            let actualMonth   = uiFun.getTwoDigitTime(time: String(describing: month))
            let actualDay     = uiFun.getTwoDigitTime(time: String(describing: day))
            let actualHour    = uiFun.getTwoDigitTime(time: String(describing: hour))
            let actualMinutes = uiFun.getTwoDigitTime(time: String(describing: minutes))
            
            arrival = "\(year)\(actualMonth)\(actualDay) \(actualHour):\(actualMinutes):\(seconds)"
        }
        
        let hotStopSequenceArray : NSMutableArray       = NSMutableArray()
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(arrival, forKey: "arrivaltime")
        para.setValue(arrival, forKey: "departuretime")
        para.setValue(destinationLabel.text!, forKey: "destinationstop")
        para.setValue(3, forKey: "max_inter_agency_hop")
        para.setValue(1, forKey: "max_intra_agency_hop")
        para.setValue(hotStopSequenceArray, forKey: "selectedHopStopSequence")
        para.setValue(startingStationLabel.text!, forKey: "sourcestop")
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("timetable trips json string = \(jsonString)") // json string
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return ""
        }
    }
    
    func processTripJSON(response : JSON){
        // Processing json response into model
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        
        if(String(describing: response["status"]) == "4000"){
            uiFun.showAlert(title: "", message: "System Error", logMessage: "APi Call Failed", fromController: self)
            return
        }
        
        let tripsArray      = response["trips"]
        let responseString  = String(describing: tripsArray)

        if let data         = responseString.data(using: .utf8){
            do {
                let tripDic = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                print("Keys for json \(tripDic?.keys)")
                
                for keys in (tripDic?.keys)!{
                
                    //print("For loop for keys lets do it \(keys)")
                    
                        let tripObject  = tripsArray[keys]
                        
                        let tripModel       = TripModel()
                        let tripValues      = TripValues()
                        
                        tripValues.calendarID            = String(describing: tripObject.arrayValue[0]["calendarID"])
                        tripValues.dCost                 = String(describing: tripObject.arrayValue[0]["dCost"])
                        tripValues.hopCountTotal         = String(describing: tripObject.arrayValue[0]["hopCountTotal"])
                        tripValues.nextEarliestTripLeg   = String(describing: tripObject.arrayValue[0]["nextEarliestTripLeg"])
                        tripValues.patternId             = String(describing: tripObject.arrayValue[0]["patternId"])
                        tripValues.pfSeqId               = String(describing: tripObject.arrayValue[0]["pfSeqId"])
                        tripValues.sRouteStartTimeStr    = String(describing: tripObject.arrayValue[0]["sRouteStartTimeStr"])
                        tripValues.sStartTime            = String(describing: tripObject.arrayValue[0]["sStartTime"])
                        tripValues.sTripDestName         = String(describing: tripObject.arrayValue[0]["sTripDestName"])
                        tripValues.sTripDestination      = String(describing: tripObject.arrayValue[0]["sTripDestination"])
                        tripValues.sTripFinalDestination = String(describing: tripObject.arrayValue[0]["sTripFinalDestination"])
                        tripValues.sTripSource           = String(describing: tripObject.arrayValue[0]["sTripSource"])
                        tripValues.srcPfNo               = String(describing: tripObject.arrayValue[0]["srcPfNo"])
                        tripValues.totalTripCost         = String(describing: tripObject.arrayValue[0]["totalTripCost"])
                        tripValues.tripFinalDest         = String(describing: tripObject.arrayValue[0]["tripFinalDest"])
                        tripValues.sTripDestName         = String(describing: tripObject.arrayValue[0]["sTripDestName"])
                    
                        let agenciesJSON                 = tripObject.arrayValue[0]["agency"]
                        let tripAgency                   = TripAgency()
                        
                        tripAgency.agencyColor           = String(describing: agenciesJSON["agencyColor"])
                        tripAgency.agencyId              = String(describing: agenciesJSON["agencyId"])
                        tripAgency.agencyLongName        = String(describing: agenciesJSON["agencyLongName"])
                        tripAgency.agencyShortName       = String(describing: agenciesJSON["agencyShortName"])
                        
                        let routeIdJSON = tripObject.arrayValue[0]["sRouteId"]
                        let routeId1 = RouteId()
                        
                        routeId1.agency                  = String(describing: routeIdJSON["agency"])
                        routeId1.routeDestination        = String(describing: routeIdJSON["routeDestination"])
                        routeId1.routeDestinationName    = String(describing: routeIdJSON["routeDestinationName"])
                        routeId1.routeDirection          = String(describing: routeIdJSON["routeDirection"])
                        routeId1.routeId                 = String(describing: routeIdJSON["routeId"])
                        routeId1.routeLongName           = String(describing: routeIdJSON["routeLongName"])
                        routeId1.routeShortName          = String(describing: routeIdJSON["routeShortName"])
                        routeId1.routeSource             = String(describing: routeIdJSON["routeSource"])
                        routeId1.routeSourceName         = String(describing: routeIdJSON["routeSourceName"])
                        routeId                          = String(describing: routeIdJSON["routeId"])
                    
                        let routeStartTimeJSON           = tripObject.arrayValue[0]["sRouteStartTime"]
                        let timeJSON                     = routeStartTimeJSON["time"]
                    
                        let routeStartTime               = RouteStartTime()
                        
                        routeStartTime.date              = String(describing: timeJSON["date"])
                        routeStartTime.day               = String(describing: timeJSON["day"])
                    
                        routeStartTime.month             = String(describing: timeJSON["month"])
                        routeStartTime.seconds           = String(describing: timeJSON["seconds"])
                        routeStartTime.time              = String(describing: timeJSON["time"])
                        routeStartTime.timezoneOffset    = String(describing:timeJSON["timezoneOffset"])
                    
                        let nextEarliestTripLeg          = tripObject.arrayValue[0]["nextEarliestTripLeg"]
                        let nextEarlyLegModel            = NextEarliestTripLeg()
                    
                        nextEarlyLegModel.calendarID            = String(describing: nextEarliestTripLeg["calendarID"])
                        nextEarlyLegModel.dCost                 = String(describing: nextEarliestTripLeg["dCost"])
                        nextEarlyLegModel.hopCountTotal         = String(describing: nextEarliestTripLeg["hopCountTotal"])
                        nextEarlyLegModel.nextEarliestTripLeg   = String(describing: nextEarliestTripLeg["nextEarliestTripLeg"])
                        nextEarlyLegModel.patternId             = String(describing: nextEarliestTripLeg["patternId"])
                        nextEarlyLegModel.pfSeqId               = String(describing: nextEarliestTripLeg["pfSeqId"])
                        nextEarlyLegModel.sRouteStartTimeStr    = String(describing: nextEarliestTripLeg["sRouteStartTimeStr"])
                        nextEarlyLegModel.sStartTime            = String(describing: nextEarliestTripLeg["sStartTime"])
                        nextEarlyLegModel.sTripDestName         = String(describing: nextEarliestTripLeg["sTripDestName"])
                        nextEarlyLegModel.sTripDestination      = String(describing: nextEarliestTripLeg["sTripDestination"])
                        nextEarlyLegModel.sTripFinalDestination = String(describing: nextEarliestTripLeg["sTripFinalDestination"])
                        nextEarlyLegModel.sTripSource           = String(describing: nextEarliestTripLeg["sTripSource"])
                        nextEarlyLegModel.srcPfNo               = String(describing: nextEarliestTripLeg["srcPfNo"])
                        nextEarlyLegModel.totalTripCost         = String(describing: nextEarliestTripLeg["totalTripCost"])
                        nextEarlyLegModel.tripFinalDest         = String(describing: nextEarliestTripLeg["tripFinalDest"])
                        nextEarlyLegModel.sTripDestName         = String(describing: nextEarliestTripLeg["sTripDestName"])
                    
                        let legAgencyJSON                       = nextEarliestTripLeg["agency"]
                        let legAgencyModel                      = LegAgency()
                    
                        legAgencyModel.agencyColor              = String(describing: legAgencyJSON["agencyColor"])
                        legAgencyModel.agencyId                 = String(describing: legAgencyJSON["agencyId"])
                        legAgencyModel.agencyLongName           = String(describing: legAgencyJSON["agencyLongName"])
                        legAgencyModel.agencyShortName          = String(describing: legAgencyJSON["agencyShortName"])
                    
                        let legRouteIdJSON                      = nextEarliestTripLeg["sRouteId"]
                        let legRouteId                          = LegRouteId()
                    
                        legRouteId.agency                       = String(describing: legRouteIdJSON["agency"])
                        legRouteId.routeDestination             = String(describing: legRouteIdJSON["routeDestination"])
                        legRouteId.routeDestinationName         = String(describing: legRouteIdJSON["routeDestinationName"])
                        legRouteId.routeDirection               = String(describing: legRouteIdJSON["routeDirection"])
                        legRouteId.routeId                      = String(describing: legRouteIdJSON["routeId"])
                        legRouteId.routeLongName                = String(describing: legRouteIdJSON["routeLongName"])
                        legRouteId.routeShortName               = String(describing: legRouteIdJSON["routeShortName"])
                        legRouteId.routeSource                  = String(describing: legRouteIdJSON["routeSource"])
                        legRouteId.routeSourceName              = String(describing: legRouteIdJSON["routeSourceName"])
                    
                        let legRouteTimeJSON                    = nextEarliestTripLeg["sRouteStartTime"]
                        let legTimeJSON                         = legRouteTimeJSON["time"]
                        let legRouteTime                        = LegRouteStartTime()
                    
                        legRouteTime.date                       = String(describing: legTimeJSON["date"])
                        legRouteTime.day                        = String(describing: legTimeJSON["day"])
                        legRouteTime.hours                      = String(describing: legTimeJSON["hours"])
                        legRouteTime.minutes                    = String(describing: legTimeJSON["minutes"])
                        legRouteTime.month                      = String(describing: legTimeJSON["month"])
                        legRouteTime.seconds                    = String(describing: legTimeJSON["seconds"])
                        legRouteTime.time                       = String(describing: legTimeJSON["time"])
                        legRouteTime.timezoneOffset             = String(describing: legTimeJSON["timezoneOffset"])
                    
                        let nextEarliestTripLeg2                = nextEarliestTripLeg["nextEarliestTripLeg"]
                        let nextEarlyLegModel2                  = NextEarliestTripLeg2()
                    
                        nextEarlyLegModel2.calendarID            = String(describing: nextEarliestTripLeg2["calendarID"])
                        nextEarlyLegModel2.dCost                 = String(describing: nextEarliestTripLeg2["dCost"])
                        nextEarlyLegModel2.hopCountTotal         = String(describing: nextEarliestTripLeg2["hopCountTotal"])
                        nextEarlyLegModel2.nextEarliestTripLeg   = String(describing: nextEarliestTripLeg2["nextEarliestTripLeg"])
                        nextEarlyLegModel2.patternId             = String(describing: nextEarliestTripLeg2["patternId"])
                        nextEarlyLegModel2.pfSeqId               = String(describing: nextEarliestTripLeg2["pfSeqId"])
                        nextEarlyLegModel2.sRouteStartTimeStr    = String(describing: nextEarliestTripLeg2["sRouteStartTimeStr"])
                        nextEarlyLegModel2.sStartTime            = String(describing: nextEarliestTripLeg2["sStartTime"])
                        nextEarlyLegModel2.sTripDestName         = String(describing: nextEarliestTripLeg2["sTripDestName"])
                        nextEarlyLegModel2.sTripDestination      = String(describing: nextEarliestTripLeg2["sTripDestination"])
                        nextEarlyLegModel2.sTripFinalDestination = String(describing: nextEarliestTripLeg2["sTripFinalDestination"])
                        nextEarlyLegModel2.sTripSource           = String(describing: nextEarliestTripLeg2["sTripSource"])
                        nextEarlyLegModel2.srcPfNo               = String(describing: nextEarliestTripLeg2["srcPfNo"])
                        nextEarlyLegModel2.totalTripCost         = String(describing: nextEarliestTripLeg2["totalTripCost"])
                        nextEarlyLegModel2.tripFinalDest         = String(describing: nextEarliestTripLeg2["tripFinalDest"])
                        nextEarlyLegModel2.sTripDestName         = String(describing: nextEarliestTripLeg2["sTripDestName"])
                    
                        let legAgencyJSON2                  = nextEarliestTripLeg2["agency"]
                        let legAgencyModel2                 = LegAgency2()
                    
                        legAgencyModel2.agencyColor         = String(describing: legAgencyJSON2["agencyColor"])
                        legAgencyModel2.agencyId            = String(describing: legAgencyJSON2["agencyId"])
                        legAgencyModel2.agencyLongName      = String(describing: legAgencyJSON2["agencyLongName"])
                        legAgencyModel2.agencyShortName     = String(describing: legAgencyJSON2["agencyShortName"])
                    
                        let legRouteIdJSON2                 = nextEarliestTripLeg2["sRouteId"]
                        let legRouteId2                     = LegRouteId2()
                    
                        legRouteId2.agency                  = String(describing: legRouteIdJSON2["agency"])
                        legRouteId2.routeDestination        = String(describing: legRouteIdJSON2["routeDestination"])
                        legRouteId2.routeDestinationName    = String(describing: legRouteIdJSON2["routeDestinationName"])
                        legRouteId2.routeDirection          = String(describing: legRouteIdJSON2["routeDirection"])
                        legRouteId2.routeId                 = String(describing: legRouteIdJSON2["routeId"])
                        legRouteId2.routeLongName           = String(describing: legRouteIdJSON2["routeLongName"])
                        legRouteId2.routeShortName          = String(describing: legRouteIdJSON2["routeShortName"])
                        legRouteId2.routeSource             = String(describing: legRouteIdJSON2["routeSource"])
                        legRouteId2.routeSourceName         = String(describing: legRouteIdJSON2["routeSourceName"])
                    
                        let legRouteTimeJSON2               = nextEarliestTripLeg2["sRouteStartTime"]
                        let legTimeJSON2                    = legRouteTimeJSON2["time"]
                        let legRouteTime2                   = LegRouteStartTime2()
                    
                        legRouteTime2.date                  = String(describing: legTimeJSON2["date"])
                        legRouteTime2.day                   = String(describing: legTimeJSON2["day"])
                        legRouteTime2.month                 = String(describing: legTimeJSON2["month"])
                        legRouteTime2.seconds               = String(describing: legTimeJSON2["seconds"])
                        legRouteTime2.time                  = String(describing: legTimeJSON2["time"])
                        legRouteTime2.timezoneOffset        = String(describing: legTimeJSON2["timezoneOffset"])
                    
                        let hour    = String(describing: timeJSON["hours"])
                        let minutes = String(describing: timeJSON["minutes"])
                    
                        if(hour.characters.count == 1){
                            routeStartTime.hours = "0\(String(describing: timeJSON["hours"]))"
                        }else{
                            routeStartTime.hours = "\(String(describing: timeJSON["hours"]))"
                        }
                    
                        if(minutes.characters.count == 1){
                            routeStartTime.minutes = "0\(String(describing: timeJSON["minutes"]))"
                        }else{
                            routeStartTime.minutes = "\(String(describing: timeJSON["minutes"]))"
                        }
                        /**************** HOP TIME *********/
                    
                        let hopHour    = String(describing: legTimeJSON["hours"])
                        let hopMinutes = String(describing: legTimeJSON["minutes"])
                    
                        if(hopHour.characters.count == 1){
                            legRouteTime.hours = "0\(String(describing: legTimeJSON["hours"]))"
                        }else{
                            legRouteTime.hours = "\(String(describing: legTimeJSON["hours"]))"
                        }
                    
                        if(hopMinutes.characters.count == 1){
                            legRouteTime.minutes = "0\(String(describing: legTimeJSON["minutes"]))"
                        }else{
                            legRouteTime.minutes = "\(String(describing: legTimeJSON["minutes"]))"
                        }
                    
                        /**************** HOP TIME 2 *********/
                    
                        let hopHour2    = String(describing: legTimeJSON2["hours"])
                        let hopMinutes2 = String(describing: legTimeJSON2["minutes"])
                    
                        if(hopHour2.characters.count == 1){
                            legRouteTime2.hours = "0\(String(describing: legTimeJSON2["hours"]))"
                        }else{
                            legRouteTime2.hours = "\(String(describing: legTimeJSON2["hours"]))"
                        }
                    
                        if(hopMinutes2.characters.count == 1){
                            legRouteTime2.minutes = "0\(String(describing: legTimeJSON2["minutes"]))"
                        }else{
                            legRouteTime2.minutes = "\(String(describing: legTimeJSON2["minutes"]))"
                        }
                    
                        /****************STORING DATA INTO MODEL *********/

                        nextEarlyLegModel2.legAgency.append(legAgencyModel2)
                        nextEarlyLegModel2.sRouteId.append(legRouteId2)
                        nextEarlyLegModel2.sRouteStartTime.append(legRouteTime2)
                    
                        nextEarlyLegModel.legAgency.append(legAgencyModel)
                        nextEarlyLegModel.sRouteId.append(legRouteId)
                        nextEarlyLegModel.sRouteStartTime.append(legRouteTime)
                        nextEarlyLegModel.nextEarliestTripLegModel.append(nextEarlyLegModel2)

                        tripValues.agency.append(tripAgency)
                        tripValues.sRouteId.append(routeId1)
                        tripValues.sRouteStartTime.append(routeStartTime)
                        tripValues.nextEarliestTripLegModel.append(nextEarlyLegModel)
                        
                        tripModel.tripValues.append(tripValues)
                        sortedTimetableTripValues.append(tripValues)
                        commonTripValues.append(tripModel)
                }
                
                
                commonTripValues = commonTripValues.sorted { $0.tripValues[0].sStartTime < $1.tripValues[0].sStartTime }
                if(commonTripValues.count == 0){
                    
                    print("Common Trip Values \(commonTripValues.count)")
                    alert = UIAlertView(title: "Notification", message: "Sorry, we couldn't find any trips for this timing", delegate: nil, cancelButtonTitle: "Cancel");
                    
                    alert.delegate  = self
                    alert.show();
                    return
                }
                saveTripListForCache()      // saving response for better UX
                timeTableTimingTable.reloadData()
                ScrollTableview()
                
               
                
            } catch {
                print(error.localizedDescription)
        }
      }
    }
    
    func saveTripListForCache(){
        // Saving trip list data into temp variable for making UX smoother
        switch travelModeSelectedIndex {
        case 0:
            trainTimetableTripValues    = commonTripValues
            break
        case 1:
            if(busSegmentSelectedIndex == 1 ){
                busTimetableTripValues  = commonTripValues
            }
            break
        case 2:
            metroTimetableTripValues    = commonTripValues
            break
        case 3:
            monoTimetableTripValues     = commonTripValues
            break
        default:
            break
        }
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        // Showing aler box
        let buttonTitle = alertView.buttonTitle(at: buttonIndex)
        print("\(buttonTitle) pressed")
        if buttonTitle == "Go Back" {
            uiFun.navigateToScreem(identifierName: "TimeTableViewController", fromController: self)
        }
    }
    
    func processBusTripJSON(response : JSON){
        // Process bus trip api response
        BusTripValues.removeAll()
        busTimetableTripList.removeAll()
        
        if(String(describing: response["statusmessage"]) == "TRIP_NOT_FOUND"){
            uiFun.showAlert(title: "", message: "Trip Not Found", logMessage: "Trip Not Found", fromController: self)
            return
        }
        let tripJSON = response["trip"]
        
        if(tripJSON.count == 0){
        
            uiFun.showAlert(title: "", message: "Ooops...Your requested trip not found!", logMessage: "Bus Number Trip Not Found", fromController: self)
            return
        }
        
        let busTripModel = BusTripListModel()
        
        busTripModel.routeid             = String(describing: tripJSON.arrayValue[0]["routeid"])
        busTripModel.longname            = String(describing: tripJSON.arrayValue[0]["longname"])
        busTripModel.shortname           = String(describing: tripJSON.arrayValue[0]["shortname"])
        busTripModel.modeid              = String(describing: tripJSON.arrayValue[0]["modeid"])
        busTripModel.modename            = String(describing: tripJSON.arrayValue[0]["modename"])
        busTripModel.hoponcount          = String(describing: tripJSON.arrayValue[0]["hoponcount"])
        busTripModel.tripcountondate     = String(describing: tripJSON.arrayValue[0]["tripcountondate"])
        busTripModel.report_notification = String(describing: tripJSON.arrayValue[0]["report_notification"])
        
        let sourceStopJSON               = tripJSON.arrayValue[0]["sourcestop"]
        let sourceStopModel              = SourceStop()
        
        sourceStopModel.id               = String(describing: sourceStopJSON["id"])
        sourceStopModel.name             = String(describing: sourceStopJSON["name"])
        sourceStopModel.lon              = String(describing: sourceStopJSON["lon"])
        sourceStopModel.lat              = String(describing: sourceStopJSON["lat"])
        busTripModel.sourcestop.append(sourceStopModel)
        
        let destinationStopJSON         = tripJSON.arrayValue[0]["destinationstop"]
        let destinationStopModel        = DestinationStop()
        
        destinationStopModel.id         = String(describing: destinationStopJSON["id"])
        destinationStopModel.name       = String(describing: destinationStopJSON["name"])
        destinationStopModel.lon        = String(describing: destinationStopJSON["lon"])
        destinationStopModel.lat        = String(describing: destinationStopJSON["lat"])
        busTripModel.destinationstop.append(destinationStopModel)
        
        let tripUp                      = tripJSON.arrayValue[0]["up"]
        print("Trip Up Count \(tripUp.count)")
        
        var i = 0
        while(i < tripUp.count){
        
            let tripUpModel             = TripUp()
            
            tripType                    = "up"
            tripUpModel.id              = String(describing: tripUp.arrayValue[i]["id"])
            tripUpModel.starttime       = String(describing: tripUp.arrayValue[i]["starttime"])
            tripUpModel.startdatetime   = String(describing: tripUp.arrayValue[i]["startdatetime"])
            tripUpModel.stoptime        = String(describing: tripUp.arrayValue[i]["stoptime"])
            tripUpModel.stoptdateime    = String(describing: tripUp.arrayValue[i]["stoptdateime"])
            tripUpModel.endtime         = String(describing: tripUp.arrayValue[i]["endtime"])
            tripUpModel.enddatetime     = String(describing: tripUp.arrayValue[i]["enddatetime"])
            tripUpModel.calender        = String(describing: tripUp.arrayValue[i]["calender"])
            
            busTripModel.up.append(tripUpModel)
            busTimetableTripList.append(String(describing: tripUp.arrayValue[i]["startdatetime"]))
            
            i += 1
        }
        
        let tripDown                    = tripJSON.arrayValue[0]["down"]
        print("Trip Down Count \(tripDown.count)")
        
        var j = 0
        while(j < tripDown.count){
            
            let tripDownModel             = TripDown()
            
            tripType                      = "down"
            tripDownModel.id              = String(describing: tripDown.arrayValue[i]["id"])
            tripDownModel.starttime       = String(describing: tripDown.arrayValue[i]["starttime"])
            tripDownModel.startdatetime   = String(describing: tripDown.arrayValue[i]["startdatetime"])
            tripDownModel.stoptime        = String(describing: tripDown.arrayValue[i]["stoptime"])
            tripDownModel.stoptdateime    = String(describing: tripDown.arrayValue[i]["stoptdateime"])
            tripDownModel.endtime         = String(describing: tripDown.arrayValue[i]["endtime"])
            tripDownModel.enddatetime     = String(describing: tripDown.arrayValue[i]["enddatetime"])
            tripDownModel.calender        = String(describing: tripDown.arrayValue[i]["calender"])
            
            busTripModel.down.append(tripDownModel)
            busTimetableTripList.append(String(describing: tripDown.arrayValue[j]["startdatetime"]))
            j += 1
        }
        
        BusTripValues.append(busTripModel)              // Storing response into list
        busCacheTripValues = busTimetableTripList
        
        timeTableTimingTable.reloadData()
        ScrollAccordingCurrentTime()
    }
    
    func ScrollTableview(){
        // Scroll the table depending on the time user have selected
        var index : Int?
        var i = 0
        while(i < commonTripValues.count){
            
            let findTimeFromLlist   = commonTripValues[i].tripValues[0].sStartTime
            let separateDateTime    = findTimeFromLlist.components(separatedBy: " ")
            let timeArray           = separateDateTime[1].components(separatedBy: ":")
            if(timeChange){
                timeChange  = false
                hour        = timeChangehour
                minutes     = timeChangeMinute
            }
            if(Int(timeArray[0]) == hour){
                if(Int(timeArray[1])! > minutes!){
                    index = i
                    break
                }
            }
            i += 1
        }
        
        if(index == nil){
            
            i = 0
            while(i < commonTripValues.count){
                
                let findTimeFromLlist   = commonTripValues[i].tripValues[0].sStartTime
                let separateDateTime    = findTimeFromLlist.components(separatedBy: " ")
                let timeArray           = separateDateTime[1].components(separatedBy: ":")
                if(timeChange){
                    
                    timeChange  = false
                    hour        = timeChangehour            // Changed time : hour
                    minutes     = timeChangeMinute          // Changed time : minute
                }
                if(Int(timeArray[0]) == hour!+1){
                    minutes = 0
                    if(Int(timeArray[1])! > minutes!){
                        index = i           // finding index where to scroll
                        break
                    }
                }
                i += 1
            }
        }
        // if index is nil increase the hour by one
        let indexpath : NSIndexPath?
        if(index == nil){
            tableViewScrollIndex    = 0
            indexpath               = NSIndexPath(row: 0, section: 0)
            print("Value of index \(index)")
        }else{
            tableViewScrollIndex    = index!
            indexpath               = NSIndexPath(row: index!, section: 0)
            print("Value of index \(index!)")
        }
        
        let visibleRowIndexPath = timeTableTimingTable.indexPathsForVisibleRows

        for index in visibleRowIndexPath!{
        
            if(index.row == tableViewScrollIndex){
                return
            }else{
                timeTableTimingTable.scrollToRow(at: indexpath as! IndexPath, at: .top, animated: false)
            }
        }
    }
    
    func ScrollAccordingCurrentTime(){
        // Scroll the table depending on the current time.
        var index : Int?
        var i = 0
        while(i < busTimetableTripList.count){
            
            let timeArray = busTimetableTripList[i].components(separatedBy: ":")
            if(timeChange){
                timeChange  = false
                hour        = timeChangehour        // Changed time :hour
                minutes     = timeChangeMinute      // Changed time :minute
            }
            if(Int(timeArray[0]) == hour){
                if(Int(timeArray[1])! > minutes!){
                    index = i                       // finding index where to scroll
                    break
                }
            }
            i += 1
        }
        
        // if index is nil increas the hour by one
        if(index == nil){
            i = 0
            while(i < busTimetableTripList.count){
                
                let timeArray   = busTimetableTripList[i].components(separatedBy: ":")
                if(timeChange){
                    timeChange  = false
                    hour        = timeChangehour
                    minutes     = timeChangeMinute
                }
                if(Int(timeArray[0]) == hour!+1){
                    minutes = 0
                    if(Int(timeArray[1])! > minutes!){
                        index = i
                        break
                    }
                }
                i += 1
            }
        }
        
        let indexpath : NSIndexPath?
        if(index == nil){
            indexpath   = NSIndexPath(row: 0, section: 0)
            print("Value of index \(index)")
        }else{
            indexpath   = NSIndexPath(row: index!, section: 0)
            print("Value of index \(index)")
        }
        
        timeTableTimingTable.scrollToRow(at: indexpath as! IndexPath, at: .top, animated: true)
        let visibleRowIndexPath = timeTableTimingTable.indexPathsForVisibleRows
        
        for index in visibleRowIndexPath!{
            
            if(index.row == tableViewScrollIndex){
                timeTableTimingTable.scrollToRow(at: indexpath as! IndexPath, at: .top, animated: false)
                return
           
            }else{
                timeTableTimingTable.scrollToRow(at: indexpath as! IndexPath, at: .top, animated: false)
            }
        }
    }

    func SortTimetable(){
        // Sorting timetable according to time
        var i = 0
        timetabelTimingSortedList.removeAll()
        while(i < commonTripValues.count){
    
            timetabelTimingSortedList.append("\(commonTripValues[i].tripValues[0].sRouteStartTime[0].hours):\(commonTripValues[i].tripValues[0].sRouteStartTime[0].minutes)")
            i += 1
        }
        timetabelTimingSortedList.sort()
        timeTableTimingTable.reloadData()
        
    }
    
    func SortLegTimeTable(){
        // Sorting Legs timetable according to time
        var i = 0
        while(i < commonTripValues.count){
            
            legTimetabelTimingSortedList.append("\(commonTripValues[i].tripValues[0].nextEarliestTripLegModel[0].sRouteStartTime[0].hours):\(commonTripValues[i].tripValues[0].nextEarliestTripLegModel[0].sRouteStartTime[0].minutes)")
            i += 1
        }
        legTimetabelTimingSortedList.sort()
    }
    
    func SortLegTimeTable2(){
        // Sorting Legs timetable 2 according to time
        var i = 0
        
        while(i < commonTripValues.count){
            
            legTimetabelTimingSortedList2.append("\(commonTripValues[i].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteStartTime[0].hours):\(commonTripValues[i].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteStartTime[0].minutes)")
            i += 1
        }
        legTimetabelTimingSortedList2.sort()
        timeTableTimingTable.reloadData()
    }
    
    @IBAction func closeButtonTapped(_ sender: AnyObject) {
        
        // When user clicks on back button clear all list and set starting and destination as default text
        doneWithTripDetail = true
        timetabelTimingSortedList.removeAll()
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
                startingStationName        = "Search For Bus Number"
                destinationStationName     = "Search Bus Stop"
            }else if(busSegmentSelectedIndex == 1){
                startingStationName        = "Start Bus Stop"
                destinationStationName     = "Destination Bus Stop"
            }
        }else{
            startingStationName            = "Starting Station"
            destinationStationName         = "Destination Station"
        }
        
        /******* Clear all list to avoid getting wrong trip list ****/
        
        trainTimetableTripValues.removeAll()
        busTimetableTripValues.removeAll()
        metroTimetableTripValues.removeAll()
        monoTimetableTripValues.removeAll()
        commonTripValues.removeAll()        // Parent of all trip list
        busCacheTripValues.removeAll()      // Bus Trip
        
        uiFun.navigateToScreem(identifierName: "TimeTableViewController", fromController: self)
    }
    @IBAction func nowButonTapped(_ sender: AnyObject) {
        nowButtonTappedEvent()
    }
    
    func nowButtonTappedEvent(){
        // when user clicks on the now button
        nowTimeValues()
        timeTableTimingTable.reloadData()
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
                ScrollAccordingCurrentTime()
            }else if(busSegmentSelectedIndex == 1){
                ScrollTableview()
                commonTripValues = busTimetableTripValues
            }
        }else{
            ScrollTableview()
        }
        
        // Now button UI changes to active state
        nowButton.layer.cornerRadius = 5
        nowButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        nowButton.layer.borderWidth = 1
        nowButton.layer.borderColor = UIColor.white.cgColor
        nowButton.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor), for: .normal)
        
        changeButton.layer.cornerRadius = 5
        changeButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        changeButton.layer.borderWidth = 1
        changeButton.layer.borderColor = UIColor.white.cgColor
        changeButton.setTitleColor(UIColor.white, for: .normal)
    }
   
    @IBAction func changeButtonTapped(_ sender: AnyObject) {
        
    }
}

extension UITableViewCell {
    
    var indexPath: IndexPath? {
        return (superview as? UITableView)?.indexPath(for: self)
    }
}
