//
//  ComposeViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 18/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import KCFloatingActionButton
import SwiftyJSON
import CoreLocation
import AssetsLibrary

class ComposeViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextViewDelegate,CLLocationManagerDelegate {

    
    let uiFun = UiUtillity()
    let ridlrColors = RidlrColors()
    
    var locationManager = CLLocationManager()
    @IBOutlet var usernameLabel: UILabel!
    
    @IBOutlet var composeTextView: UITextView!
    
    @IBOutlet var image1: UIImageView!
    @IBOutlet var image2: UIImageView!
    @IBOutlet var image3: UIImageView!
    
    var image1Flag          = false
    var image2Flag          = false
    var image3Flag          = false
    
    var latitute            = ""
    var longitude           = ""
    
    var mediaLink           = ""
    var selectImageFrom     = ""
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createFAB()
        composeTextView.delegate            = self
        
        composeTextView.text                = "Start typing to report something on the road"
        composeTextView.textColor           = UIColor.lightGray
        
        // Requesting location from user
        locationManager.delegate            = self
        locationManager.desiredAccuracy     = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        let defaults                        = UserDefaults.standard
        let timelineLogin                   = defaults.string(forKey: "TimelineLogin")!
        if(timelineLogin != "Nope"){
            let username                    = uiFun.readData(key: "Username")
            usernameLabel.text              = username
        }
        
        uiFun.createCleverTapEvent(message: "COMPOSE TIMLINE NEWSFEED SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        // Back button tap action
        locationManager.stopUpdatingLocation()
        if(fromTrafficPage){
            fromTrafficPage = false
            uiFun.navigateToScreem(identifierName: "TrafficNearYouViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "NewsFeedViewController", fromController: self)
        }
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView){
    
        composeTextView.text        = ""                // Compose Text set to blank
        composeTextView.textColor   = UIColor.black
    }
    
    func createFAB(){
        
        // Creating floating action button to upload pic from camera or gallery
        let fab = KCFloatingActionButton()
        fab.buttonColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        fab.addItem("From Gallery", icon: UIImage(named: "ic_indicator_gallery")!, handler: { item in
            self.fromGallery()
            fab.close()
        })
        fab.addItem("Take Picture", icon: UIImage(named: "ic_indicator_camera")!, handler: { item in
            self.openCameraButton()
            fab.close()
        })

        self.view.addSubview(fab)
    }
    
    func fromGallery(){
        // Uploading pic from gallery
        selectImage(from: UIImagePickerControllerSourceType.photoLibrary)
        selectImageFrom = "gallery"
    }
    
    func selectImage(from source: UIImagePickerControllerSourceType){
        // Selected image source type
        let imagePickerController           = UIImagePickerController()
        imagePickerController.delegate      = self
        imagePickerController.sourceType    = source
        imagePickerController.allowsEditing = false
        
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    func openCameraButton() {
        // Upload pic from the camera
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker             = UIImagePickerController()
            imagePicker.delegate        = self
            imagePicker.sourceType      = UIImagePickerControllerSourceType.camera;
            selectImageFrom             = "camera"
            imagePicker.allowsEditing   = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            // If iPhone doesnt have camera :-(
            uiFun.showAlert(title: "No Camera", message: "This device doesn't have camera", logMessage: "NO camera in device", fromController: self)
        }
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        self.dismiss(animated: true, completion: { () -> Void in
            // Selected image data
            let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
            if(self.selectImageFrom == "gallery"){
                let imageURL          = info[UIImagePickerControllerReferenceURL] as! NSURL // Selected Image path
                print("News Feed Image Url For newsfedd \(imageURL)")
                let imageName         = imageURL.lastPathComponent          // Selected image name
                print("News FeedImage Name For newsfedd \(imageName)")
            }
            
            let compressImageData     = chosenImage.resized(withPercentage: 0.1)    // compressing image data
            let imageData             = UIImagePNGRepresentation(compressImageData!)//(compressImageData!, 1.0)//(chosenImage, 0.0)
            self.image1.contentMode   = .scaleAspectFit                             // Scalling image
            
            print("News Feed Image data \(imageData)")
            let strBase64             = imageData?.base64EncodedString()            // Converting image into base64
            let base64Encoded         = strBase64?.addingPercentEncodingForURLQueryValue()  // encoding base64
            
            /***************** TIMESTAMP *******************/
            var arrival             = ""
            
            let date                = NSDate()
            let calendar            = NSCalendar.current
            let hour                = calendar.component(.hour, from: date as Date)
            let minutes             = calendar.component(.minute, from: date as Date)
            let seconds             = calendar.component(.second, from: date as Date)
            
            let date1               = Date()
            let components1         = calendar.dateComponents([.year, .month, .day], from: date1)
            
            let year                = components1.year!
            let month               = components1.month!
            let day                 = components1.day!
            
            if(timeChange){
                arrival = plannerChangeDateTime
            }else{
                let actualMonth   = self.uiFun.getTwoDigitTime(time: String(describing: month))
                let actualDay     = self.uiFun.getTwoDigitTime(time: String(describing: day))
                let actualHour    = self.uiFun.getTwoDigitTime(time: String(describing: hour))
                let actualMinutes = self.uiFun.getTwoDigitTime(time: String(describing: minutes))
                let actualSeconds = self.uiFun.getTwoDigitTime(time: String(describing: seconds))
                
                arrival           = "\(year)\(actualMonth)\(actualDay)\(actualHour)\(actualMinutes)\(actualSeconds)"
            }
            let imageName123      = "RIDLR_\(UUID().uuidString)_\(arrival)" // image name which will pass in api
            let imagePath         = self.getDocumentsDirectory().appendingPathComponent(imageName123)
            print("Name of selected image using camera is \(imagePath)")
            
            let uploadPic         = UploadPicAPICall()
            
            uploadPic.makeAPICall(fromViewController: self, loginName: "Mayur", imageName: "mayur_image", base64String: base64Encoded!, lat:self.latitute ,long:self.longitude )
            
            // check for taken image where to display i.e in slot1, slot2 & slot3
            if(!self.image1Flag){
                self.image1.image = chosenImage
                self.image1Flag = true
                return
            }else if(!self.image2Flag){
            
                self.image2.image = chosenImage
                self.image2Flag = true
                return
            }
            else if(!self.image3Flag){
            
                self.image3.image = chosenImage
                self.image3Flag = true
                return
            }
        })
        
    }
    
    func getDocumentsDirectory() -> URL {
        // Fetching path of document
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    @IBAction func postButtonTapped(_ sender: AnyObject) {
        // Make an api call when user clicks on post button
        let checkTimelineLogin = uiFun.readData(key: "TimelineLogin")
        if(checkTimelineLogin == "Nope"){
            guestUserLogin() // if user is not login show the alert box
        }else{
            ComposeTimelineAPICall()    // api call for post
        }
    }
    
    func ComposeTimelineAPICall(){
        // API call for post
        let composeAPI = ComposeReportAPICall()
        composeAPI.makeAPICall(fromViewController: self, composeText: composeTextView.text, mediaLink: mediaLink)
    }
    
    func guestUserLogin(){
        
        // if user is not login show the alert box
        var alertController:UIAlertController?
        alertController = UIAlertController(title: "User Detail",
                                            message: "You are required to create a guest name before posting a message",
                                            preferredStyle: .alert)
        
        alertController!.addTextField(
            configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Enter Username"
        })
        
        let action = UIAlertAction(title: "Ok",
                                   style: UIAlertActionStyle.default,
                                   handler: {[weak self]
                                    (paramAction:UIAlertAction!) in
                                    if let textFields = alertController?.textFields{
                                        let theTextFields = textFields as [UITextField]
                                        let enteredText = theTextFields[0].text
                                        self?.uiFun.writeData(value: enteredText!, key: "Username")
                                        self?.uiFun.writeData(value: "Done", key: "TimelineLogin")
                                        self?.ComposeTimelineAPICall()
                                        
                                    }
        })
        alertController?.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default,handler: nil))
        
        alertController?.addAction(action)
        self.present(alertController!,
                     animated: true,
                     completion: nil)
    }
    
    func responseFromComposeReport(jsonResponse : JSON){
    
        // Compose timeline api response
        locationManager.stopUpdatingLocation()
        let status = String(describing: jsonResponse["codedesc"])
        if(status == "Success"){
            // Post sent successfully
            uiFun.navigateToScreem(identifierName: "NewsFeedViewController", fromController: self)
        }
    }
    
    func responseFromUploadPicApi(jsonResponse : JSON){
        // Uploading pic to the server api call response
        print("Got Uploading Image News Feed ")
        
        if(String(describing: jsonResponse["value"]) == ""){
            uiFun.showAlert(title: "", message: "Oops...Image Uploading Fail!", logMessage: "Image Uploading Fail Compose Timeline", fromController: self)
        }else{
            mediaLink = String(describing: jsonResponse["value"])
            print("Media Link Of NewsFeed \(mediaLink)")
            
           uiFun.loadImageFromURL(url: mediaLink) { (result, error) in
                
                if result != nil{
                    if let mediaLinkData    = UIImage(data: result!){
                        self.image1.image   = mediaLinkData
                        print("Media Link Set Successfully On Response")
                        return
                    }
                }else {
                    print("Image Set Failed....Try Harder Mayur ")
                    
                    return
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Fetching user location
        let userLocation: CLLocation = locations[0]
        
        latitute  = String(userLocation.coordinate.latitude)
        longitude = String(userLocation.coordinate.longitude)

        uiFun.writeData(value: String(describing :latitute), key: "lat")
        uiFun.writeData(value: String(describing :longitude), key: "long")
        
        
        
    }
}

extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize  = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize  = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toHeight height: CGFloat) -> UIImage? {
        let canvasSize  = CGSize(width: CGFloat(ceil(height/size.width * size.height)), height: height)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }

}
