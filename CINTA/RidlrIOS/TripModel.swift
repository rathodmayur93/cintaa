//
//  TripModel.swift
//  RidlrIOS
//
//  Created by Mayur on 23/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class TripModel{
    
    var tripValues = [TripValues]()
}

class TripValues{

    var calendarID              = ""
    var dCost                   = ""
    var hopCountTotal           = ""
    var nextEarliestTripLeg     = ""
    var patternId               = ""
    var pfSeqId                 = ""
    var sRouteStartTimeStr      = ""
    var sStartTime              = ""
    var sTripDestName           = ""
    var sTripDestination        = ""
    var sTripFinalDestination   = ""
    var sTripSource             = ""
    var sTripSrcName            = ""
    var srcPfNo                 = ""
    var totalTripCost           = ""
    var tripFinalDest           = ""
    
    var agency                  = [TripAgency]()
    var sRouteId                = [RouteId]()
    var sRouteStartTime         = [RouteStartTime]()
    var nextEarliestTripLegModel = [NextEarliestTripLeg]()
}

class TripAgency{

    var agencyColor             = ""
    var agencyId                = ""
    var agencyLongName          = ""
    var agencyShortName         = ""
}

class RouteId{

    var agency                  = ""
    var routeDestination        = ""
    var routeDestinationName    = ""
    var routeDirection          = ""
    var routeId                 = ""
    var routeLongName           = ""
    var routeShortName          = ""
    var routeSource             = ""
    var routeSourceName         = ""
    
}

class RouteStartTime{

    var date                    = ""
    var day                     = ""
    var hours                   = ""
    var minutes                 = ""
    var month                   = ""
    var seconds                 = ""
    var time                    = ""
    var timezoneOffset          = ""
}

class NextEarliestTripLeg{

    var calendarID              = ""
    var dCost                   = ""
    var hopCountTotal           = ""
    var nextEarliestTripLeg     = ""
    var patternId               = ""
    var pfSeqId                 = ""
    var sRouteStartTimeStr      = ""
    var sStartTime              = ""
    var sTripDestName           = ""
    var sTripDestination        = ""
    var sTripFinalDestination   = ""
    var sTripSource             = ""
    var sTripSrcName            = ""
    var srcPfNo                 = ""
    var totalTripCost           = ""
    var tripFinalDest           = ""
    
    var legAgency               = [LegAgency]()
    var sRouteId                = [LegRouteId]()
    var sRouteStartTime         = [LegRouteStartTime]()
    var nextEarliestTripLegModel     = [NextEarliestTripLeg2]()
}

class LegAgency{

    var agencyColor             = ""
    var agencyId                = ""
    var agencyLongName          = ""
    var agencyShortName         = ""
}

class LegRouteId{

    var agency                  = ""
    var routeDestination        = ""
    var routeDestinationName    = ""
    var routeDirection          = ""
    var routeId                 = ""
    var routeLongName           = ""
    var routeShortName          = ""
    var routeSource             = ""
    var routeSourceName         = ""
}

class LegRouteStartTime{
    
    var date                    = ""
    var day                     = ""
    var hours                   = ""
    var minutes                 = ""
    var month                   = ""
    var seconds                 = ""
    var time                    = ""
    var timezoneOffset          = ""
}

class NextEarliestTripLeg2{
    
    var calendarID              = ""
    var dCost                   = ""
    var hopCountTotal           = ""
    var nextEarliestTripLeg     = ""
    var patternId               = ""
    var pfSeqId                 = ""
    var sRouteStartTimeStr      = ""
    var sStartTime              = ""
    var sTripDestName           = ""
    var sTripDestination        = ""
    var sTripFinalDestination   = ""
    var sTripSource             = ""
    var sTripSrcName            = ""
    var srcPfNo                 = ""
    var totalTripCost           = ""
    var tripFinalDest           = ""
    
    var legAgency               = [LegAgency2]()
    var sRouteId                = [LegRouteId2]()
    var sRouteStartTime         = [LegRouteStartTime2]()
}

class LegAgency2{
    
    var agencyColor             = ""
    var agencyId                = ""
    var agencyLongName          = ""
    var agencyShortName         = ""
}

class LegRouteId2{
    
    var agency                  = ""
    var routeDestination        = ""
    var routeDestinationName    = ""
    var routeDirection          = ""
    var routeId                 = ""
    var routeLongName           = ""
    var routeShortName          = ""
    var routeSource             = ""
    var routeSourceName         = ""
}

class LegRouteStartTime2{
    
    var date                    = ""
    var day                     = ""
    var hours                   = ""
    var minutes                 = ""
    var month                   = ""
    var seconds                 = ""
    var time                    = ""
    var timezoneOffset          = ""
}










