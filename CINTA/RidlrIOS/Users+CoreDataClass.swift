//
//  Users+CoreDataClass.swift
//  
//
//  Created by Mayur on 22/05/17.
//
//

import Foundation
import CoreData


public class Users: NSManagedObject {

    static let entityName = "Users"
}
