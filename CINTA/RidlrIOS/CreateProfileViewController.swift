
//
//  CreateProfileViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 09/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreData
import M13Checkbox
import SwiftyJSON
import DLRadioButton


var profileType                     = ""
var otpEmailId                      = ""
var otpMobile                       = ""
var otpWallet                       = ""

class CreateProfileViewController: UIViewController {

    var uiFun                       = UiUtillity()
    var ridlrColors                 = RidlrColors()
    var constant                    = Constants()
    var uiUtility                   = UiUtillity()
    
    @IBOutlet var emailIdTF         : UITextField!
    @IBOutlet var mobileNumberTF    : UITextField!
    
    @IBOutlet var walletCB          : M13Checkbox!
    @IBOutlet var proceedBT         : UIButton!
    var navBar                      : UINavigationController!
    var selectedWallet              : String!
    
    /************ WALLET UI **************/
    
    @IBOutlet var walletUI              : UIView!
    @IBOutlet var citrusProfileRadio    : DLRadioButton!
    @IBOutlet var mobikwikProfileRadio  : DLRadioButton!
    @IBOutlet var freechargeProfileRadio: DLRadioButton!
    @IBOutlet var walletSwitch          : UISwitch!
    @IBOutlet var includeWallet         : UILabel!
    
    @IBOutlet var citrusWalletLabel     : UILabel!
    @IBOutlet var mobikwikWalletLabel   : UILabel!
    @IBOutlet var freechargeWalletLabel : UILabel!
    @IBOutlet var termsCondition        : UITextView!
    @IBOutlet var agreeTermsBT          : UIButton!
    
    @IBOutlet var citrusLogo            : UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        uiFun.createCleverTapEvent(message: "PROFILE NAME NUMBER SCREEN")
        
        citrusProfileRadio.isSelected   = true
        walletSwitch.isSelected         = true
        selectedWallet                  = "CITRUSPAY"
        
        // Tap Gesture On Citrus Wallet Label Function
        let tapGestureCitrus = UITapGestureRecognizer(target: self, action: #selector(citrusWalletLabelTapped))
        tapGestureCitrus.numberOfTapsRequired = 1
        citrusWalletLabel.isUserInteractionEnabled = true
        citrusWalletLabel.addGestureRecognizer(tapGestureCitrus)
        
        // Tap Gesture On Mobikwik Wallet Label Function
        let tapGestureMobikwik = UITapGestureRecognizer(target: self, action: #selector(mobikwikWalletLabelTapped))
        tapGestureMobikwik.numberOfTapsRequired = 1
        mobikwikWalletLabel.isUserInteractionEnabled = true
        mobikwikWalletLabel.addGestureRecognizer(tapGestureMobikwik)
        
        // Tap Gesture On Freecharge Wallet Label Function
        let tapGestureFreecharge = UITapGestureRecognizer(target: self, action: #selector(freechargeWalletLabelTapped))
        tapGestureFreecharge.numberOfTapsRequired = 1
        freechargeWalletLabel.isUserInteractionEnabled = true
        freechargeWalletLabel.addGestureRecognizer(tapGestureFreecharge)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("/***** PROFILE NAME NUMBER SCREEN *******/")
    }
    

    func setUI(){
        
        mobileNumberTF.keyboardType = UIKeyboardType.numberPad // Number pad keyboard
        proceedBT.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        proceedBT.backgroundColor   = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        
        emailIdTF.keyboardType      = UIKeyboardType.emailAddress   // Email Add Keyboard
        mobileNumberTF.keyboardType = UIKeyboardType.numberPad      // Number pad keyboard
        
        walletCB.checkState         = .checked                      // Citrus Wallet radio checked
        
        mobileNumberTF.addTarget(self, action: #selector(checkMobileNumber), for: UIControlEvents.editingChanged)
        
        // You must set the formatting of the link manually
        let linkAttributes = [
            NSLinkAttributeName: NSURL(string: "http://ridlr.in/privacy")!,
            NSForegroundColorAttributeName: UIColor.blue
            ] as [String : Any]
        
        let attributedString = NSMutableAttributedString(string: "I agreed to the Private Policy and Terms & Condition")
        // Set the 'click here' substring to be the link
        attributedString.setAttributes(linkAttributes, range: NSMakeRange(5, 10))
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // On clicking out side of keyboard the keyboard will get disappear
        self.view.endEditing(true)
        
    }
    
    @IBAction func checkBoxTapped(_ sender: Any) {
    
    }
    
    @IBAction func agreedTermsBTAction(_ sender: Any) {
        
    }
    
    
    func checkMobileNumber(){
        // Mobile Number Length Check
        if((mobileNumberTF.text?.characters.count)! > 10){
            mobileNumberTF.deleteBackward()
        }
    }

    @IBAction func citrusProfileRadioActionEvent(_ sender: Any) {
        // Citrus Radio Button Tap Action
        citrusWalletLabelTapped()
    }
    
    @IBAction func mobikwikProfileRadioActionEvent(_ sender: Any) {
        // Mobikwik Radio Button Tap Action
        mobikwikWalletLabelTapped()
    }
    
    @IBAction func freechargeProfileRadioActionEvent(_ sender: Any) {
        // Freecharge Radio Button Tap Action
        freechargeWalletLabelTapped()
    }
    
    @IBAction func walletSwithButtonAction(_ sender: Any) {
        // Wallet Switch Action
        if(walletSwitch.isOn){
            // Show Wallets
            walletUI.isHidden   = false
        }else{
            // Hide Wallets
            walletUI.isHidden   = true
        }
    }
    
    
    @IBAction func walletInfoAction(_ sender: Any) {
        print("Radio Working Fine")
    }
    
    
    func citrusWalletLabelTapped(){
        // Citrus Wallet Label & Radio Button Tap Action
        mobikwikProfileRadio.isSelected     = false
        freechargeProfileRadio.isSelected   = false
        citrusProfileRadio.isSelected       = true
        selectedWallet                      = "CITRUSPAY"
        print("CITRUSPAY")
    }
    
    func mobikwikWalletLabelTapped(){
        // Mobikwik Wallet Label & Radio Button Tap Action
        citrusProfileRadio.isSelected       = false
        freechargeProfileRadio.isSelected   = false
        mobikwikProfileRadio.isSelected     = true
        selectedWallet                      = "MOBIKWIK"
        print("MOBIKWIK")
    }
    
    func freechargeWalletLabelTapped(){
        // Freecharge Wallet Label & Radio Button Tap Action
        citrusProfileRadio.isSelected       = false
        mobikwikProfileRadio.isSelected     = false
        freechargeProfileRadio.isSelected   = true
        selectedWallet                      = "FREECHARGE"
        print("FREECHARGE")
    }
    
    
    @IBAction func proceedBTTapped(_ sender: AnyObject) {
        
        // Check for validtion of email id
        if(isValidEmail(testStr: emailIdTF.text!)){
            // Check for validation of mobile number
            if(validateMobile(value: mobileNumberTF.text!)){
                
                uiFun.showIndicatorLoader()
                
                let profile: Dictionary<String, AnyObject> = [
                    "Identity": uiFun.readData(key: "UUID") as AnyObject,                   // String or number
                    "Email": emailIdTF.text! as AnyObject,                                  // Email address of the user
                    "Phone": mobileNumberTF.text! as AnyObject,                             // Phone (with the country code, starting with +)
                ]
                
                CleverTap.sharedInstance()?.profilePush(profile)
        
                if(walletSwitch.isOn){
                    // Create profile using wallet
                    authMethodAPICall()
                    uiFun.createCleverTapEvent(message: "Profile type selected on Submit \(selectedWallet)")
                }else{
                    // Create profile without wallet
                    profileCreationWithoutWalletAPICall()
                    uiFun.createCleverTapEvent(message: "Profile type selected on Submit Ridlr")
                }
        
                let appDelegate =  UIApplication.shared.delegate as! AppDelegate
                let context     = appDelegate.persistentContainer.viewContext
                
                /************ UPDATING CORE DATA ***********/
                
                let city = NSFetchRequest<NSFetchRequestResult>(entityName: "Users") //Fetching entity user from core data
                city.returnsObjectsAsFaults = false

                do {
                    
                    let results =  try context.fetch(city) // Fetching city from entity user core data
                    if results.count > 1{
                        print("Saved Data \(results[0]) \(results[1])")
                        
                        for result in results as! [NSManagedObject]{
                            print("Updating Core Data")
                            result.setValue(emailIdTF.text, forKey: "userEmail")        // Setting user email id into core data
                            result.setValue(mobileNumberTF.text, forKey: "mobileNumber") // Setting mobile number into core data
                        }
                    }
                        
                    else{
                        let user = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
                        user.setValue(emailIdTF.text, forKey: "userEmail")
                        user.setValue(mobileNumberTF.text, forKey: "mobileNumber")
                        print("Core Data is Empty Inserting Into Core Data Entity Name User")
                    }
                }
                    
                catch {
                    print("There is an error in fetching entity from core data")
                }
                
                /************ FINISH UPDATING CORE DATA *****************/
                
                otpEmailId = emailIdTF.text!        // Set User email id to global variable.
                otpMobile  = mobileNumberTF.text!   // Set User mobile number to global variable.
                if(walletSwitch.isOn){
                    otpWallet  = selectedWallet
                }
        
                do {
                    try context.save()
                        print("Saved Successfully")
                    }
                catch {
                    print("There is an error")
                }
        
            }else{
                uiFun.showAlert(title: "", message: "Ooopss.. Looks Like You Entered Wrong Mobile Number", logMessage: "Invalid Mobile Number Profile Creation", fromController: self)
            }
        }else{
            uiFun.showAlert(title: "", message: "Ooopss.. Looks Like You Entered Wrong Email Id", logMessage: "Invalid Email Id Profile Creation", fromController: self)
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        // back button action
        if(profileBackNavigation != ""){
            uiFun.navigateToScreem(identifierName: profileBackNavigation, fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        }
    }
    
    func profileCreationWithoutWalletAPICall(){
    
        // Profile creation without any wallet otp will be received from RIDLR
        let util        = Utillity()
        profileType     = "profile"
        
        let apiUrl = "\(constant.STAGING_BASE_URL)/user/auth_methods" // API Call For Reqeusting OTP
        util.APICall(apiUrl: apiUrl, jsonString: jsonStringWithoutWalletAPI(email: String(describing: emailIdTF.text!), mobile: String(describing: mobileNumberTF.text!))!, requestType: "POST", header: false) { (result, error) in
           
            print("Successfully called api call without wallet \(result)")
            let jsonResponse = result!
            self.uiFun.hideIndicatorLoader()
            print("JSON Response Code \(String(describing: jsonResponse["code"]))")
            if(String(describing: jsonResponse["code"]) == "5000"){
                // Success
                self.uiFun.createCleverTapEvent(message: "Authentication method Ridlr : OTP")
                self.uiFun.navigateToScreem(identifierName: "ProfileOTPViewController", fromController: self)
            }else{
                self.uiFun.hideIndicatorLoader()
                self.uiFun.showExceptionDialog(jsonResponse: jsonResponse, fromViewController: self)
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // Validating Email Address
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validateMobile(value: String) -> Bool {
        // Validating mobile number
        if(value.characters.count != 10){
            return false
        }else{
            return true
        }
    }
    
    func authMethodAPICall(){
    
        // Profile creation without any wallet otp will be received from Respective Wallet
        profileType = "wallet"
        do {
            // Creating json string for wallet profile creation
            let jsonString = checkEmailAndNumber(email: String(describing: emailIdTF.text!), mobile: String(describing: mobileNumberTF.text!), payment: selectedWallet)
            print("JOSN BODY REQUEST \(jsonString)")
            
            // create post request
            let url = NSURL(string: "\(constant.STAGING_BASE_URL)/payment/auth_methods")! // API Call For Reqeusting OTP
            print("Auth Method URL Payment \(url)")
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonString?.data(using: String.Encoding.utf8, allowLossyConversion: true)
            print("BODY REQUEST" + String(describing: request.httpBody))
            
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                if error != nil{
                    print("Certificate Error -> \(error!)")
                    self.uiFun.hideIndicatorLoader()
                    if(error.debugDescription.contains("timed out")){
                        self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                    }else if(error.debugDescription.contains("connection was lost")){
                        self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                    }else if(error.debugDescription.contains("appears to be offline")){
                       self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                    }
                    
                    return
                }else{
                    
                    if let urlContent = data{
                        
                        do {
                            
                            let jsonResult      = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                            
                            let jsonResponse    = JSON(jsonResult)
                            print("JSON RESPONSE \(jsonResponse)")
                            
                        if(JSON(jsonResult["status"]!) == "OK"){
                            
                            self.uiFun.hideIndicatorLoader()
                            
                            print("JSON OBJECT VALUE \(jsonResult["data"]!!)")
                            
                            let jsonData = JSON(jsonResult["data"]!!)
                            
                            // Setting data into the parmanent storage
                            let defaults = UserDefaults.standard
                            defaults.set(String(describing: jsonData["pgPreAuthData"]), forKey: "pgPreAuthData")
                            defaults.set(String(describing: self.emailIdTF.text!), forKey: "userEmailId")
                            defaults.set(String(describing: self.mobileNumberTF.text!), forKey: "userMobileNo")
                            
                            
                            print("Jugaad of  mobile \(jsonData["mobile"])")
                            
                            if let description1 = ((jsonResult["data"] as? NSArray)?[0] as? NSDictionary)?["mobile"] as? String{
                                print("JSON Response Mobile \(description1)")
                            }
                            
                            /**************** NAVIGATE TO OTP SCREEN**************/
                            
                            self.uiFun.createCleverTapEvent(message: "Authentication method Citrus:OTP")
                            self.uiFun.navigateToScreem(identifierName: "ProfileOTPViewController", fromController: self)
                            
                            if(JSON(jsonResult["status"]!) == "OK"){
                                print("authMethodAPICall Success")
                            }
                        }else {
                            
                            let jsonError = JSON(jsonResult["error"]!!)
                            self.uiFun.hideIndicatorLoader()
                            
                            
                            if(String(describing: jsonError["errorCodeText"]) == "" || String(describing: jsonError["errorCodeText"]) == "null"){
                                // Fetching errorCodeText and if its null parse response to "showExceptionDialog" method
                                    self.uiFun.showExceptionDialog(jsonResponse: jsonResponse, fromViewController: self)
                            }else{
                                // Show erroCodeText into alert box
                                self.uiFun.showAlert(title: "Error", message: String(describing: jsonError["errorCodeText"]), logMessage: "authMethodAPICall()", fromController: self)
                            }
                        }
                            
                        }
                        catch{
                            // Failed To Convert JSON
                            print("Failed To Convert JSON")
                            self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "authMethodAPICall()", fromController: self)
                        }
                      
                    }
                }
            }
            
            task.resume()
            
        }

    }
    
    func checkEmailAndNumber(email: String, mobile: String, payment:String) -> String?{
        // Creating json string for wallet otp request
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(email, forKey: "walletEmail")
        para.setValue(mobile, forKey: "walletMobile")
        para.setValue(payment, forKey: "paymentGateway")
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    func jsonStringWithoutWalletAPI(email : String, mobile: String) -> String?{
        // Creating json string for otp request (Without Wallet)
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(email, forKey: "contactEmail")
        para.setValue(mobile, forKey: "contactNumber")
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    func userProfileAuthMethod(){
        
        do {
            if let jsonString = userProfileAuthData(){
                print("JSON BODY REQUEST \(jsonString)")
                
                // create post request
                let url             = NSURL(string: "\(constant.STAGING_BASE_URL)/user/auth_methods")!
                print("Auth Method URL User \(url)")
                let request         = NSMutableURLRequest(url: url as URL)
                request.httpMethod  = "POST"
                
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true)
                print("BODY REQUEST" + String(describing: request.httpBody))
                
                
                let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                    if error != nil{
                        // error in calling api
                        print("Certificate Error -> \(error!)")
                        self.uiFun.hideIndicatorLoader()
                        if(error.debugDescription.contains("timed out")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("connection was lost")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("appears to be offline")){
                            self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                        }
                        return
                    }else{
                        
                        if let urlContent = data{
                            
                            do {
                                // API call response
                                let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                                
                                print("Citrus add money json result \(jsonResult)")
                                
                                if(JSON(jsonResult["status"]!) == "OK"){
                                    // Success response
                                    print("JSON RESPONSE userProfileAuthMethod \(jsonResult)")
                                    print("JSON OBJECT VALUE userProfileAuthMethod \(jsonResult["data"]!!)")
                                    
                                    let jsonData = JSON(jsonResult["data"]!!)
                                    print("User Profile Auth Method Data \(jsonData)")
                                    
                                }else{
                                    // Fail Response
                                    self.uiFun.showExceptionDialog(jsonResponse: JSON(jsonResult), fromViewController: self)
                                }
                                
                            }
                            catch{
                                print("Failed To Convert JSON")
                                // By default error if fail to convert response into json
                                self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "userProfileAuthMethod()", fromController: self)
                            }
                        }
                    }
                }
                
                task.resume()
                
            }
        }
    }
    
    func userProfileAuthData() -> String?{
        
        // JSON String for OTP Request for wallets
        let defaults = UserDefaults.standard
        let pgAuthData = defaults.string(forKey: "pgAuthData")
        defaults.setValue(String(describing: emailIdTF.text), forKey: "contactEmail")
        defaults.setValue(String(describing: mobileNumberTF.text), forKey: "contactNumber")
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue("CITRUSPAY", forKey: "paymentGateway")
        para1.setValue(pgAuthData, forKey: "pgAuthData")
    
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(String(describing: emailIdTF.text), forKey: "contactEmail")
        para.setValue(String(describing: mobileNumberTF.text), forKey: "contactNumber")
        para.setValue(para1, forKey: "walletInfo")
        
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string ultimate = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
}

extension CreateProfileViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return (navigationController?.viewControllers.count)! > 1 ? true : false
    }
}

