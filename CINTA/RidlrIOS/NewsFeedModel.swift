//
//  NewsFeedModel.swift
//  RidlrIOS
//
//  Created by Mayur on 12/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class NewsFeedModel {

    var alertMessageSent        = ""
    var sourceUpdateId          = ""
    var modifiedDate            = ""
    var updateId                = ""
    var source                  = ""
    var location                = ""
    var likesCount              = ""
    var textUpdate              = ""
    var username                = ""
    var updateType              = ""
    var postedOn                = ""
    var repliesCount            = ""
    var fromUser                = ""
    var profileImg              = ""
    var likeArrayFromUser       = ""
    var likeArrayUsername       = ""
    var likeArrayActivityTime   = ""
    var mediaLink               = ""
    var isMyOwnPost             = false
}
