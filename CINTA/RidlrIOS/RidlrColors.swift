//
//  Colors.swift
//  RidlrIOS
//
//  Created by Mayur on 26/10/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class RidlrColors: UIViewController {

    var ridlrBrandColor         = "#00BECC"
    var ridlrBrandDarkColor     = "#00ACC1"
    var ridlrTextColor          = "#333333"
    
    var whiteColor              = "#ffffff"
    var greyColor               = "#4D072126"
    var lighGreyColor           = "#EDEFF0"
    var color_gray_90           = "#1A072126"
    var color_gray_50           = "#80072126"
    var color_gray_20           = "#CC072126"
    var light_grey_50           = "#ECECEC"
    
    var error_color             = "#D32F2F"
    var success_color           = "008040"
    var newsFeed_bg_color       = "#EBEBF1"
    var ridlr_brand_color_light = "#00C8D6"
    
    var col_white_alpha30       = "#4CFFFFFF"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
