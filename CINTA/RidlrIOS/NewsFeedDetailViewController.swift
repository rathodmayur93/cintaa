//
//  NewsFeedDetailViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 14/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class NewsFeedDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet var userProfileIV: UIImageView!
    @IBOutlet var emptyStateIV: UIImageView!
    
    @IBOutlet var emptyStateLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
   
    @IBOutlet var contentTV: UITextView!
    @IBOutlet var replyUsefulSegment: UISegmentedControl!
    
    @IBOutlet var likeUsefulContentTableView: UITableView!
    
    var difference : Date = Date()
    
    let uiFun = UiUtillity()
    let ridlrColors = RidlrColors()
    let constant = Constants()
    let util = Utillity()
    
    var replyList = [NewsFeedReplyModel]()
    
    
    var segmentSelected = "reply"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        getReplyForSpecificUpdate()
        showReply()
        uiFun.createCleverTapEvent(message: "NEWS FEED DETAIL SCREEN")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        // back button tap action
        uiFun.navigateToScreem(identifierName: "NewsFeedViewController", fromController: self)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // return the reply and like count
        if(segmentSelected == "reply"){
            return replyList.count
        }else if(segmentSelected == "useful"){
            return Int(newsFeedList[newsFeedIndex].likesCount)!
        }
        return 0
    }
        
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        // Intializing the cell
        let cell = likeUsefulContentTableView.dequeueReusableCell(withIdentifier: "Cell") as! NewsFeedreplyTableViewCell
        
        if(segmentSelected == "reply"){
            // Reply Segment is selected
            cell.updateContent.isHidden         = false
            cell.likeCount.isHidden             = false
            cell.likeButton.isHidden            = false
            cell.deleteButton.isHidden          = false
            
            cell.username.text                  = replyList[indexPath.row].username
            cell.updateContent.text             = replyList[indexPath.row].textUpdate
            cell.likeCount.text                 = replyList[indexPath.row].likeCount
            
            let stringToDate                    = uiFun.convertStringToDate(dateString: replyList[indexPath.row].postedOn)
            let timeInterval                    = difference.offset(from: stringToDate)
            print("Date difference is \(timeInterval)")
            
            cell.time.text                      = timeInterval
        
            if(replyList[indexPath.row].profileImage != ""){
            
                let imageUrl                    = replyList[indexPath.row].profileImage
            
                uiFun.loadImageFromURL(url: imageUrl) { (result, error) in
                    // Load media from its url
                    if result != nil{
                        if let imageDataFromURl                     = UIImage(data: result!){
                            cell.profileImage.image                 = imageDataFromURl
                            cell.profileImage.layer.cornerRadius    = cell.profileImage.frame.size.width/2
                            cell.profileImage.clipsToBounds         = true
                            return
                        }
                    }else {
                        // Failed to load media from its url
                        cell.profileImage.image                     = UIImage(named: "ic_indicator_default_user_active")
                        print("Image Set Failed....Try Harder Mayur ")
                        return
                    }
                }
            }
            return cell
        }else if(segmentSelected == "useful"){
            // Useful Segment is Selected
            cell.username.text          = newsFeedList[newsFeedIndex].likeArrayUsername
            
            let stringToDate            = uiFun.convertStringToDate(dateString: newsFeedList[newsFeedIndex].likeArrayActivityTime)
            let timeInterval            = difference.offset(from: stringToDate)
            print("Date difference is \(timeInterval)")
            
            cell.time.text              = timeInterval
            cell.profileImage.image     = UIImage(named: "ic_indicator_default_user_active")
            
            cell.updateContent.isHidden = true
            cell.likeCount.isHidden     = true
            cell.likeButton.isHidden    = true
            cell.deleteButton.isHidden  = true
            
            return cell
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // heigh of the table
        return 163.0
    }
    
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        // UI Layout of the cell
        cell.contentView.backgroundColor        = uiFun.hexStringToUIColor(hex: ridlrColors.newsFeed_bg_color)
        let whiteRoundedView : UIView           = UIView(frame: CGRect(x: 0, y:8, width: self.view.frame.width, height: 205))
        whiteRoundedView.layer.backgroundColor  = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
    }
    
    
    
    func setUI(){
    
        print("Index row is \(newsFeedIndex!)")
        usernameLabel.text = newsFeedList[newsFeedIndex].username
        
        if(newsFeedList[newsFeedIndex].modifiedDate != "null"){
            let stringToDate = uiFun.convertStringToDate(dateString: newsFeedList[newsFeedIndex].modifiedDate)
            let timeInterval = difference.offset(from: stringToDate)
            print("Date difference is \(timeInterval)")
            
            timeLabel.text   = timeInterval     // Set the modified date
            
        }else{
            
            let stringToDate = uiFun.convertStringToDate(dateString: newsFeedList[newsFeedIndex].postedOn)
            let timeInterval = difference.offset(from: stringToDate)
            print("Date difference is \(timeInterval)")
            
            timeLabel.text = timeInterval       // set the datea
        }
        
        print("Like User are \(newsFeedList[newsFeedIndex].likeArrayUsername)")
        contentTV.text  = newsFeedList[newsFeedIndex].textUpdate        // Liked user content
        let likeCount   = Int(newsFeedList[newsFeedIndex].likesCount)   // Likes count
        
        if(likeCount == 0){
            likeUsefulContentTableView.isHidden = true                  // Hide the like table
        }
        
        /************** LOAD IMAGE ****************/
        
        let imageUrl = newsFeedList[newsFeedIndex].profileImg
        
        if(imageUrl != "null" || imageUrl != ""){
            // load image from url
            uiFun.loadImageFromURL(url: imageUrl) { (result, error) in
            
            if result != nil{
                if let imageDataFromURl                     = UIImage(data: result!){
                    self.userProfileIV.image                = imageDataFromURl
                    self.userProfileIV.layer.cornerRadius   = self.userProfileIV.frame.size.width/2
                    self.userProfileIV.clipsToBounds        = true
                    return
                }
            }else {
                self.userProfileIV.image = UIImage(named: "ic_indicator_default_user_active")
                print("Image Set Failed....Try Harder Mayur ")
                return
            }
        }
      }
        
        likeUsefulContentTableView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        likeUsefulContentTableView.separatorStyle  = UITableViewCellSeparatorStyle.none
    }
    
   
    @IBAction func replyUsefulSegmentAction(_ sender: AnyObject) {
        
        // Which segment user have selected
        switch replyUsefulSegment.selectedSegmentIndex {
        case 0:
            showReply()
            segmentSelected = "reply"
            break
        case 1:
            let likeCount = Int(newsFeedList[newsFeedIndex].likesCount)
            showUseful()
            segmentSelected = "useful"
            if(likeCount != 0){
                likeUsefulContentTableView.reloadData()
            }
        default:
            break
        }
    }
    
    
    func showReply(){
    
        // Show what users have replied on the selected post
        let replyCount = Int(newsFeedList[newsFeedIndex].repliesCount)
        if(replyCount == 0){
            // If no user have replied on the selected post
            likeUsefulContentTableView.isHidden     = true
            emptyStateIV.image                      = UIImage(named: "ic_indicator_empty_reply")
            emptyStateLabel.text                    = "Be the first one to reply"
        
        }else{
            // If user have replied on the selected post show replies in table view
            likeUsefulContentTableView.isHidden     = false
            emptyStateIV.isHidden                   = true
            emptyStateLabel.isHidden                = true
        }
    }
    
    func showUseful(){
        
        let likeCount = Int(newsFeedList[newsFeedIndex].likesCount)
        print("Like Count news feed detail \(likeCount)")
        if(likeCount == 0){
            // If no user have liked on the selected post
            likeUsefulContentTableView.isHidden = true
            emptyStateIV.isHidden = false
            emptyStateLabel.isHidden = false
            emptyStateIV.image = UIImage(named: "ic_indicator_empty_usefull")
            emptyStateLabel.text = "Be the first one to find this useful"
        }else{
            // If user have liked on the selected post show likes in table view
            likeUsefulContentTableView.isHidden = false
            emptyStateIV.isHidden = true
            emptyStateLabel.isHidden = true
        }
    }
    
    func getReplyForSpecificUpdate(){
    
        // Fetch Users replies on particular post
        let url = "\(constant.TIMELINE_URL)GetTimelineRepliesForSpecificUpdates" // API Url
        
        // Parameters needed
        let params : [String:AnyObject] = ["strIP":"" as AnyObject, "dbUpdateId":newsFeedList[newsFeedIndex].updateId as AnyObject, "strActivity":"2" as AnyObject, "strCustKey":constant.CUST_KEY as AnyObject, "passkey":constant.PASS_KEY as AnyObject,"addparams":"" as AnyObject, "accessType":"device" as AnyObject, "strGroupId":"ridlr" as AnyObject, "userid":"-111" as AnyObject, "strSinceId":"" as AnyObject, "intNoOfRecords":"10" as AnyObject]
      
           util.sendRequest(url: url, parameters: params) { (result, error) in
            
            if let jsonResponse = result{
                // Got the json response
                let values = jsonResponse["value"]
                let event = values["Event"]
                
                print("Reply event is \(event)")
                
                if(event != "null"){
                
                    var i = 0
                    
                    while i < event.count{
                        // Storing the json response into the model
                        let newsFeedReplyModel = NewsFeedReplyModel()   // Model
                        
                        newsFeedReplyModel.fromUser         = String(describing : event.arrayValue[i]["FromUser"])
                        newsFeedReplyModel.likeCount        = String(describing: event.arrayValue[i]["LikesCount"])
                        newsFeedReplyModel.location         = String(describing: event.arrayValue[i]["Location"])
                        newsFeedReplyModel.postedOn         = String(describing: event.arrayValue[i]["PostedOn"])
                        newsFeedReplyModel.profileImage     = String(describing: event.arrayValue[i]["ProfileImg"])
                        newsFeedReplyModel.textUpdate       = String(describing: event.arrayValue[i]["TextUpdate"])
                        newsFeedReplyModel.updateId         = String(describing: event.arrayValue[i]["UpdateId"])
                        newsFeedReplyModel.updateType       = String(describing: event.arrayValue[i]["UpdateType"])
                        newsFeedReplyModel.username         = String(describing: event.arrayValue[i]["Username"])
                        newsFeedReplyModel.isMyOwnPost      = String(describing: event.arrayValue[i]["isMyOwnPost"])
                        
                        self.replyList.append(newsFeedReplyModel)
                        i += 1
                    }
                    self.likeUsefulContentTableView.reloadData()
                }
            }
            
            if error != nil{
                print("Error From News Feed \(error)")
            }
        }
    }
}







