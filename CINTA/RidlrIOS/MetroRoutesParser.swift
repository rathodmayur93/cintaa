//
//  MetroRoutesParser.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var metroRouteList = [MetroRoutesModel]()
class MetroRoutesParser{
    
    let uiFun = UiUtillity()
    func parseMetroRoutesCSVToList(){
        
        let queueName = DispatchQueue(label: "routes.metro.processing")
        
        queueName.async {
            
            let csvContent = self.uiFun.readFromCSV(fileName: "MUM Metro_routes")
            let csvNewLineContent = csvContent.components(separatedBy: .newlines)
            let delimiter = ","
            
            let lines:[String] = csvNewLineContent
            
            
            
            
            for line in lines {
                
                let metroRouteParseModel = MetroRoutesModel()
                
                let metroRoutes       = MetroRoute()
                var metroData         = line.components(separatedBy: delimiter)
                
                if(metroData[0] != ""){
                    
                    metroRoutes.routeId = metroData[0]
                    metroRoutes.routeShortName = metroData[1]
                    metroRoutes.routeLongName = metroData[2]
                    metroRoutes.routeDirectitonId = metroData[3]
                    metroRoutes.routeSourceId = metroData[4]
                    metroRoutes.routeDestinationId = metroData[5]
                    
                    metroRouteParseModel.metroRoute.append(metroRoutes)
                    metroRouteList.append(metroRouteParseModel)
                }
                
            }
        }
    }
}
