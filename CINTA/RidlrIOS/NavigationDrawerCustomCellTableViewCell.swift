//
//  NavigationDrawerCustomCellTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 25/10/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class NavigationDrawerCustomCellTableViewCell: UITableViewCell {

    @IBOutlet var drawerIcon: UIImageView!
    @IBOutlet var drawerLabel: UILabel!
    
    @IBOutlet var agenciesTrain: UILabel!
    @IBOutlet var subAgenciesTrain: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
