//
//  SelectBusAgencyViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class SelectBusAgencyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let cityParser            = CityParser()
    var busAgencies           = [Agency]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        busAgencies = cityParser.getAgencies(modeName: "BUS")   // fetching agencies name from csv file
        uiFun.createCleverTapEvent(message:"SELECT BUS AGENCY SCREEN")
    }
    @IBAction func backButton(_ sender: Any) {
        // when user clicks on back button set starting and destination text to default
        if(travelModeSelectedIndex == 1){
            
            if(busSegmentSelectedIndex == 0){
                startingStationName     = "Search For Bus Number"
                destinationStationName  = "Search Bus Stop"
            }else if(busSegmentSelectedIndex == 1){
                startingStationName        = "Start Bus Stop"
                destinationStationName     = "Destination Bus Stop"
            }
        }else{
            startingStationName     = "Starting Station"
            destinationStationName  = "Destination Station"
        }
        uiFun.navigateToScreem(identifierName: "TimeTableViewController", fromController: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // table view row count
        return busAgencies.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // initializing cell and displaying text
        let cell             = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        cell.textLabel?.text = busAgencies[indexPath.row].agencyLongName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // When user clicks on particular row
        if(travelModeSelectedIndex == 1){
            
            if(busSegmentSelectedIndex == 0){
                startingStationName         = "Search For Bus Number"
                destinationStationName      = "Search Bus Stop"
            
            }else if(busSegmentSelectedIndex == 1){
            
                startingStationName        = "Start Bus Stop"
                destinationStationName     = "Destination Bus Stop"
            }
        }else{
            startingStationName     = "Starting Station"
            destinationStationName  = "Destination Station"
        }
        
        busAgency = busAgencies[indexPath.row].agencyShortName      // Fetchnig agency short name
        agencyId  = busAgencies[indexPath.row].agencyId             // id of selected agency
        uiFun.navigateToScreem(identifierName: "TimeTableViewController", fromController: self)
    }
}
