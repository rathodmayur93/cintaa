//
//  FetchBusNumberModel.swift
//  RidlrIOS
//
//  Created by Mayur on 08/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusNumberModel{
    
    var index   = ""
    var type    = ""
    var id      = ""
    var score   = ""
    var sort    = ""
    var fields  = [FieldModel]()
}

class FieldModel{

    var full_long_name          = ""
    var source_stop_name        = ""
    var route_long_name         = ""
    var custom_route_id         = ""
    var agency_id               = ""
    var destination_stop_id     = ""
    var route_short_name        = ""
    var source_stop_id          = ""
    var destination_stop_name   = ""
}
