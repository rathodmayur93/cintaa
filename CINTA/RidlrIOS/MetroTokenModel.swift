//
//  MetroTokenModel.swift
//  RidlrIOS
//
//  Created by Mayur on 28/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MetroTokenModel : NSObject, NSCoding{
    
    var bookingItemId       = ""
    var itemType            = ""
    var itemSubType         = ""
    var userType            = ""
    var userDob             = ""
   
    var userName            = ""
    var activationDate      = ""
    var expiryDate          = ""
    var activeRenewalDate   = ""
    var validityCriteria    = ""
   
    var validity            = ""
    var issuerIdentifier    = ""
    var issuerTag           = ""
    var companyItemId       = ""
    var fare                = [Fare]()
    
    var routeDetail         = [Route]()
    var newMetroToken       = [NewMetroTokenModel]()
    
    init(bookingItemId : String, itemType : String, itemSubType : String, userType : String, userDob : String, userName : String, activationDate : String, expiryDate : String, activeRenewalDate : String, validityCriteria : String, validity : String, issuerIdentifier : String, issuerTag : String, companyItemId : String, fare : [Fare], routeDetail : [Route], newMetroToken : [NewMetroTokenModel])
    {
        
        self.bookingItemId      = bookingItemId
        self.itemType           = itemType
        self.itemSubType        = itemSubType
        self.userType           = userType
        self.userDob            = userDob
       
        self.userName           = userName
        self.activationDate     = activationDate
        self.expiryDate         = expiryDate
        self.activeRenewalDate  = activeRenewalDate
        self.validityCriteria   = validityCriteria
        
        self.validity           = validity
        self.issuerIdentifier   = issuerIdentifier
        self.issuerTag          = issuerTag
        self.companyItemId      = companyItemId
        self.fare               = fare
        self.routeDetail        = routeDetail
        self.newMetroToken      = newMetroToken
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder : NSCoder){
        
        guard     let bookingItemId     = decoder.decodeObject(forKey: "bookingItemId")     as? String,
        let itemType                    = decoder.decodeObject(forKey: "itemType")          as? String,
        let itemSubType                 = decoder.decodeObject(forKey: "itemSubType")       as? String,
        let userType                    = decoder.decodeObject(forKey: "userType")          as? String,
        let userDob                     = decoder.decodeObject(forKey: "userDob")           as? String,
        let userName                    = decoder.decodeObject(forKey: "userName")          as? String,
        let activationDate              = decoder.decodeObject(forKey: "activationDate")    as? String,
        let expiryDate                  = decoder.decodeObject(forKey: "expiryDate")        as? String,
        let activeRenewalDate           = decoder.decodeObject(forKey: "activeRenewalDate") as? String,
        let validityCriteria            = decoder.decodeObject(forKey: "validityCriteria")  as? String,
        let validity                    = decoder.decodeObject(forKey: "validity")          as? String,
        let issuerIdentifier            = decoder.decodeObject(forKey: "issuerIdentifier")  as? String,
        let issuerTag                   = decoder.decodeObject(forKey: "issuerTag")         as? String,
        let companyItemId               = decoder.decodeObject(forKey: "companyItemId")     as? String,
        let fare                        = decoder.decodeObject(forKey: "fare")              as? [Fare],
        let routeDetail                 = decoder.decodeObject(forKey: "routeDetail")       as? [Route],
        let newMetroToken               = decoder.decodeObject(forKey: "newMetroToken")     as? [NewMetroTokenModel]
        else{return nil}
        
        self.init(
            bookingItemId       : bookingItemId,
            itemType            : itemType,
            itemSubType         : itemSubType,
            userType            : userType,
            userDob             : userDob,
            
            userName            : userName,
            activationDate      : activationDate,
            expiryDate          : expiryDate,
            activeRenewalDate   : activeRenewalDate,
            validityCriteria    : validityCriteria,
            
            validity            : validity,
            issuerIdentifier    : issuerIdentifier,
            issuerTag           : issuerTag,
            companyItemId       : companyItemId,
            fare                : fare,
           
            routeDetail         : routeDetail,
            newMetroToken       : newMetroToken
        )
    }
    
    func encode(with coder: NSCoder) {
        
        coder.encode(self.bookingItemId, forKey: "bookingItemId")
        coder.encode(self.itemType, forKey: "itemType")
        coder.encode(self.itemSubType, forKey: "itemSubType")
        coder.encode(self.userType, forKey: "userType")
        coder.encode(self.userDob, forKey: "userDob")
        
        coder.encode(self.userName, forKey: "userName")
        coder.encode(self.activationDate, forKey: "activationDate")
        coder.encode(self.expiryDate, forKey: "expiryDate")
        coder.encode(self.activeRenewalDate, forKey: "activeRenewalDate")
        coder.encode(self.validityCriteria, forKey: "validityCriteria")
        
        coder.encode(self.validity, forKey : "validity")
        coder.encode(self.issuerIdentifier, forKey: "issuerIdentifier")
        coder.encode(self.issuerTag, forKey: "issuerTag")
        coder.encode(self.companyItemId, forKey: "companyItemId")
        coder.encode(self.fare, forKey: "fare")
        coder.encode(self.routeDetail, forKey: "routeDetail")
        coder.encode(self.newMetroToken, forKey: "newMetroToken")
    }
}

class MetroTokenJSONModel {

    var bookingItemId       = ""
    var itemType            = ""
    var itemSubType         = ""
    var userType            = ""
    var userDob             = ""
    
    var userName            = ""
    var activationDate      = ""
    var expiryDate          = ""
    var activeRenewalDate   = ""
    var validityCriteria    = ""
    
    var validity            = ""
    var issuerIdentifier    = ""
    var issuerTag           = ""
    var companyItemId       = ""
    var bookingNo           = ""
    
    var companyRefId        = "" // Master QR Code
    var refTxnId            = "" // Old QR Code Id Or Slave Id
    var trips               = ""
    var entryScanTime       = ""
    
    var fare                = [Fare]()
    var routeDetail         = [Route]()
    var newMetroToken       = [NewMetroTokenModel]()
}


class BookingItemsListOffline : NSObject, NSCoding{

    var bookingItemListOffline = [MetroTokenJSONModel]()
    var companyReferenceId     = ""
    var totalAmount            = ""
    
    init(bookingItemListOffline : [MetroTokenJSONModel], companyReferenceId : String, totalAmount : String){
        self.bookingItemListOffline = bookingItemListOffline
        self.companyReferenceId     = companyReferenceId
        self.totalAmount            = totalAmount
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder decoder: NSCoder) {
        
        guard let bookingItemListOffline = decoder.decodeObject(forKey : "bookingItemListOffline")  as? [MetroTokenJSONModel],
        let companyReferenceId           = decoder.decodeObject(forKey : "companyReferenceId")      as? String,
        let totalAmount                  = decoder.decodeObject(forKey: "totalAmount")              as? String
            
            else {
            return nil
        }
        
        self.init(bookingItemListOffline : bookingItemListOffline, companyReferenceId : companyReferenceId, totalAmount : totalAmount)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.bookingItemListOffline, forKey: "bookingItemListOffline")
        coder.encode(self.companyReferenceId, forKey: "companyReferenceId")
        coder.encode(self.totalAmount, forKey: "totalAmount")
    }
}

class Fare{
    
    var baseAmount          = ""
    var totalAmount         = ""
    var totalSurcharge      = ""
    var totalTax            = ""
    var totalDiscount       = ""
    var totalConcession     = ""
    
}

class Route{

    var bookingItemRouteId      = ""
    var sourceId                = ""
    var sourceName              = ""
    var sourceStageId           = ""
    var destinationId           = ""
    var destinationStageId      = ""
    var destinationName         = ""
    var routeId                 = ""
    var routeName               = ""
    var sourceStageName         = ""
    var destinationStageName    = ""
}

class NewMetroTokenModel{

    var status              = ""
    var qrCodeId            = ""       // Slave Master ID
    var type                = ""
    var qrCode              = ""
    var expiry              = ""
    
    var bookingItemRouteId      = ""
    var sourceId                = ""
    var sourceName              = ""
    var sourceStageId           = ""
    var destinationId           = ""
    var destinationStageId      = ""
    var destinationName         = ""
    var routeId                 = ""
    var routeName               = ""
    var sourceStageName         = ""
    var destinationStageName    = ""
}


