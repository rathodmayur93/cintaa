//
//  TripPassReceiptViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 04/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import Darwin
import BigInt
import SwiftyJSON

class TripPassReceiptViewController: UIViewController {

    
    @IBOutlet var masterView        : UIView!
    @IBOutlet var routeDetailView   : UIView!
    
    @IBOutlet var masterQRCodeLabel : UILabel!
    @IBOutlet var destinationLabel  : UILabel!
    @IBOutlet var sourceLabel       : UILabel!
    @IBOutlet var expiryLabe        : UILabel!
    @IBOutlet var tripLabel         : UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        masterView.layer.borderColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_50).cgColor
        masterView.layer.borderWidth = 1.0
        
        routeDetailView.layer.borderWidth = 1.0
        routeDetailView.layer.borderColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_20).cgColor
        
        masterQRCodeLabel.text      = uiFun.readData(key: "tripMasterQRCode")           //tripInfoList[0].masterQRCode
        sourceLabel.text            = uiFun.readData(key: "tripPassSourceName")         //tripInfoList[0].originStation
        destinationLabel.text       = uiFun.readData(key: "tripPassDestinationeName")   //tripInfoList[0].destinationStation
        tripLabel.text              = uiFun.readData(key: "tripPassRemainingTrip")      //tripInfoList[0].trips
        
        //print("Trip Pass Master QR Code \(tripInfoList[0].masterQRCode)")
    }
    
    func createBookingNo() -> String?{
    
        let timestamp                = Int(NSDate().timeIntervalSince1970 * 1000)
        
        let userId                  = uiFun.readData(key: "userId")
        let maxAllowedCharacter     = 10
        let maxAllowedBookingNo     = 16
        let userIdCharacters        = userId.characters.count
        let allowedRandom           = maxAllowedCharacter - userIdCharacters
        let random                  = drand48()
        let randomGeneration        = UInt64(round(random * pow(10, Double(allowedRandom)) * 9) + pow(10, Double(allowedRandom)))
        let mergedChar              = "\(randomGeneration)\(userId)\(timestamp)"
        print(mergedChar)
        let mergedCharINT           = BigInt(mergedChar)
        let base36                  = mergedCharINT?.toBase(b: 36)
        print("Base 36 String \(base36)")
        let leftCharcter            = maxAllowedBookingNo - (base36?.characters.count)!
        print("Booking no charcter left \(leftCharcter)")
        
        return base36!
    }
    
    @IBAction func startNewTripBTAction(_ sender: Any) {
        
        uiFun.showIndicatorLoader()
        
        let bookingNo       = uiFun.readData(key: "tripBookingId")
        let compantRefid    = uiFun.readData(key: "tripMasterQRCode")
        let itemId          = createBookingNo()
        
        if(tripLabel.text == "0"){
            uiFun.navigateToScreem(identifierName: "MetroTripViewController", fromController: self)
        }else{
            let tripStartAPI    = TripStartAPI()
            tripStartAPI.makeAPICall(bookingNo: bookingNo, companyItemId: compantRefid, companyReferenceId: itemId!, fromViewController: self)
        }
    }
    
    func gotResponseFromTripStart(response : JSON){
    
        uiFun.hideIndicatorLoader()
        print("JSON Response Of Trip Pass Start \(response)")
        if(String(describing: response["code"]) == "5000"){
            
            let processResponseToDatabase   = MetroJSONResponses()
            let tripPassResponse            = processResponseToDatabase.processMetroJSONResponse(response: response)
        
            uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
        }else{
            let errorJSON       = response["error"]
            uiFun.showAlert(title: "", message: String(describing : errorJSON["errorCodeText"]), logMessage: "Start Trip Pass Error", fromController: self)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    
    
}
