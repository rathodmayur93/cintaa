//
//  PenaltyIssueViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 27/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var penaltyStation      = "Select Station"
var penaltyStationId    = ""

var penalyList          = [PenaltyModel]()
class PenaltyIssueViewController: UIViewController {

    
    @IBOutlet var toolbarHeading            : UILabel!
    @IBOutlet var selectStationLabel        : UILabel!
    
    @IBOutlet var exitDetailView            : UIView!
    @IBOutlet var entryStationLabel         : UILabel!
    @IBOutlet var entryTimeLabel            : UILabel!
    @IBOutlet var destinationStationLabel   : UILabel!
    @IBOutlet var destinationTimeLabel      : UILabel!
    @IBOutlet var stationVIew               : UIView!
    @IBOutlet var continueBT                : UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    func setUI(){
    
        exitDetailView.layer.borderWidth    = 1.0
        exitDetailView.layer.borderColor    = UIColor.lightGray.cgColor
        
        exitDetailView.isHidden             = true
        
        // destination Station Click
        let tapGesture1                             = UITapGestureRecognizer(target: self, action: #selector(destinationStationAction))
        tapGesture1.numberOfTapsRequired            = 1
        selectStationLabel.isUserInteractionEnabled = true
        selectStationLabel.addGestureRecognizer(tapGesture1)
        
        selectStationLabel.text     = penaltyStation
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(penaltyStation != "Select Station"){
            selectStationLabel.textColor = UIColor.black
        }
        
        if(penaltyStation != "Select Station"){
            uiFun.showIndicatorLoader()
            let penaltyAPI = PenaltyAPICallViewController()
            penaltyAPI.makeAPICall(stationId: penaltyStationId, itemSubtype: tokenSubType, identifier: tokenIdentifier, fromViewController: self)
        }
        
        continueBT.isEnabled        = false
        continueBT.backgroundColor  =   UIColor.gray
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        penaltyStation      = "Select Station"
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        uiFun.navigateToScreem(identifierName: "PenaltyIssueDetectedViewController", fromController: self)
    }
    
    func destinationStationAction(){
        metroFromWhere = "penalty"
        uiFun.navigateToScreem(identifierName: "MetroStationsViewController", fromController: self)
        isOriginStationSelected = false
    }
    
    func updateUI(){
    
        entryStationLabel.text          = penalyList[0].bookingRoute[0].sourceName
        destinationStationLabel.text    = penalyList[0].bookingRoute[0].destinationName
        
        if(metroTokenJourneyType == "STORE_VALUE_PASS"){
            destinationStationLabel.text   = penaltyStation
        }
        
        let entryScanTime               = String(describing: penalyList[0].itemAttribute[0].entryScanTime)
        
        if(entryScanTime != "null"){
            
            let timestampDate                 = NSDate(timeIntervalSince1970: Double(entryScanTime)!/1000)
            let dayTimePeriodFormatter        = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
            entryTimeLabel.text               = dayTimePeriodFormatter.string(from: timestampDate as Date)
        }
        

    }

    func gotPenaltyResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        penalyList.removeAll()
        
        if(String(describing: response["code"]) == "5000"){
            
            exitDetailView.isHidden         = false
            continueBT.isEnabled            = true
            
            let dataJSON            = response["data"]
            let penaltyModel        = PenaltyModel()
            
            for i in 0..<dataJSON.count{
            
                penaltyModel.bookingItemId  = String(describing: dataJSON.arrayValue[dataJSON.count - 1]["bookingItemId"])
                penaltyModel.itemSubType    = String(describing: dataJSON.arrayValue[0]["itemSubType"])
                penaltyModel.itemType       = String(describing: dataJSON.arrayValue[0]["itemType"])
                penaltyModel.itemSubType1   = String(describing: dataJSON.arrayValue[dataJSON.count - 1]["itemSubType"])
                penaltyModel.totalAmount    = String(describing: dataJSON.arrayValue[dataJSON.count - 1]["totalAmount"])
            
            
                let bookingRouteJSON        = dataJSON.arrayValue[0]["bookingItemRoutesList"]
                let bookingRoute            = BookingRoute()
            
                bookingRoute.bookingItemRouteId     = String(describing: bookingRouteJSON.arrayValue[0]["routeId"])
                bookingRoute.routeName              = String(describing: bookingRouteJSON.arrayValue[0]["routeName"])
                bookingRoute.sourceStageName        = String(describing: bookingRouteJSON.arrayValue[0]["sourceStageName"])
                bookingRoute.destinationName        = String(describing: bookingRouteJSON.arrayValue[0]["destinationName"])
                bookingRoute.bookingItemRouteId     = String(describing: bookingRouteJSON.arrayValue[0]["bookingItemRouteId"])
                bookingRoute.sourceId               = String(describing: bookingRouteJSON.arrayValue[0]["sourceId"])
                bookingRoute.sourceStageId          = String(describing: bookingRouteJSON.arrayValue[0]["sourceStageId"])
                bookingRoute.destinationId          = String(describing: bookingRouteJSON.arrayValue[0]["destinationId"])
                bookingRoute.sourceName             = String(describing: bookingRouteJSON.arrayValue[0]["sourceName"])
                bookingRoute.destinationStageName   = String(describing: bookingRouteJSON.arrayValue[0]["destinationStageName"])
            
                let itemAttributeJSON               = dataJSON.arrayValue[0]["itemAttribute"]
                let itemAttribute                   = ItemAttribute()
            
                itemAttribute.masterQrCodeId        = String(describing: itemAttributeJSON["masterQrCodeId"])
                itemAttribute.originalTokenType     = String(describing: itemAttributeJSON["originalTokenType"])
                itemAttribute.refTxnId              = String(describing: itemAttributeJSON["refTxnId"])
                itemAttribute.entryScanTime         = String(describing: itemAttributeJSON["entryScanTime"])
                itemAttribute.excessTime            = String(describing: itemAttributeJSON["excessTime"])
            
                penaltyModel.bookingRoute.append(bookingRoute)
                penaltyModel.itemAttribute.append(itemAttribute)
            
                penalyList.append(penaltyModel)
            }
            
            updateUI()
            
        }else{
        
            let errorJson = response["error"]
            uiFun.showAlert(title: "", message: String(describing:errorJson["errorCodeText"]), logMessage: "No Penalty Found", fromController: self)
        }
        
    }
    
}
