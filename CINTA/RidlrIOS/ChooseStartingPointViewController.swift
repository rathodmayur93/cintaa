//
//  ChooseStartingPointViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 26/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class ChooseStartingPointViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    let uiFun       = UiUtillity()
    let ridlrColors = RidlrColors()
    
    @IBOutlet var currentLocationLabel: UILabel!
    
    var mapView     : GMSMapView             = GMSMapView()
    var lat         : CLLocationDegrees      = CLLocationDegrees()
    var long        : CLLocationDegrees      = CLLocationDegrees()
    var marker      : GMSMarker              = GMSMarker()
    var camera      : GMSCameraPosition      = GMSCameraPosition()
    var userPosition: CLLocationCoordinate2D = CLLocationCoordinate2D()
    let geocoder                    = GMSGeocoder()
    let locationManager             = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Requesting for user location
        locationManager.delegate    = self
        
        uiFun.createCleverTapEvent(message: "FIND A ROUTE CHOOSE LOCATION USING GOOGLE MAP SCREEN")
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            print("Authorized")
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            let alertController = UIAlertController (title: "Location Setting", message: "Please provide location access.", preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                        redirectingPage = "ChooseStartingPointViewController"
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (UIAlertAction) in
                self.uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
            })
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
            
            break
        }
    }
    
    func initMap(){
    
        print("Locatin from splash to route search \(uiFun.readData(key: "lat"))")
        
        // set the map camera
        camera  = GMSCameraPosition.camera(withLatitude: Double(uiFun.readData(key: "lat"))!, longitude: Double(uiFun.readData(key: "long"))!, zoom: 13.0)
        let mapViewFrame            = CGRect(x: 0, y: 105, width: self.view.frame.width, height: self.view.frame.height - 105)
        mapView                     = GMSMapView.map(withFrame: mapViewFrame, camera: camera)
        mapView.isMyLocationEnabled = true
        self.view.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        let position = CLLocationCoordinate2D(latitude: Double(uiFun.readData(key: "lat"))!, longitude: Double(uiFun.readData(key: "long"))!)
        marker                  = GMSMarker(position: position)
        marker.icon             = UIImage(named: "ic_indicator_map_pin_favourite")
        marker.appearAnimation  = kGMSMarkerAnimationPop;
        marker.map              = self.mapView
        
        //reverse geo-coding
        geocoder.reverseGeocodeCoordinate(userPosition) { (response, error) in
            print("Response of Google Map is \(response) \(self.userPosition)")
        }
    }
    
    public func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker){
        print("Marker position changed")
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition){
        // When map camera changes this method will get invoked
        marker.position = mapView.camera.target
        print("Route Search Lcoation Changed")
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        // When dragging of the map is complete this method will get invoked
        if(plannerFromWhere == "start"){
            
            plannerStartingLat  = marker.position.latitude
            plannerStartingLong = marker.position.longitude
        }else if(plannerFromWhere == "end"){
            plannerDestinationLat  = marker.position.latitude
            plannerDestinationLong = marker.position.longitude
        }
        
        geocoder.reverseGeocodeCoordinate(position.target) { (response, error) in
            // GOogle reverse geo-coding using lat long
            let jsonResponse = response?.results()
            self.currentLocationLabel.text = jsonResponse?[0].lines?[0]
            print("result of google geocoding \(jsonResponse)")
            print("Response of Google Map is \(response?.results()) \(position.target)")
            
            if(plannerFromWhere == "start"){
                if let subLocalityStarting = (jsonResponse?[0].subLocality){
                    
                    let findStartingSubLocality = subLocalityStarting.components(separatedBy: ",")
                    plannerStartingSubLocality  = findStartingSubLocality[findStartingSubLocality.count - 1]
                    print("SubLocality Starting Station \(plannerStartingSubLocality)")
                }
            }else if (plannerFromWhere == "end"){
                if let subLocalityDestination = (jsonResponse?[0].subLocality){
                    
                    let findDestinationSublocality = subLocalityDestination.components(separatedBy: ",")
                    plannerDestinationSubLocality  = findDestinationSublocality[findDestinationSublocality.count - 1]
                    print("Sublocality Destination Station \(plannerDestinationSubLocality)")
                }
            }
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        // When user location gets changed
        lat  = locValue.latitude
        long = locValue.longitude
        
        
        userPosition = locValue
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        locationManager.stopUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
        
        initMap()   // Initializing map
        mapView.delegate            = self
    }
    
    
    @IBAction func backButtonTappedAction(_ sender: AnyObject) {
        // When user taps on back button
        uiFun.navigateToScreem(identifierName: "LocationViewController", fromController: self)
        
        mapView.isMyLocationEnabled     = false
        locationManager.stopUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
    }
    
    @IBAction func doneButtonAction(_ sender: AnyObject) {
        // When user taps on done button
        if(plannerFromWhere == "start"){
            myLocation                     = self.currentLocationLabel.text!
            plannerStartingStationPlaceId   = "null"
        }else if(plannerFromWhere == "end"){
            
            navigateToPlannerResult        = true
            plannerDestinationStation      = self.currentLocationLabel.text!
             plannerDestinationPlaceId       = "null"
        }
        
        uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
        
        mapView.isMyLocationEnabled     = false
        locationManager.stopUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
    }
    
    
    
    func reverseGeocoding(latitude: CLLocationDegrees, longitude: CLLocationDegrees){
        // Reverse Geo-Coding converting lat lng to address
        var locationAddress = ""
        print("map location lat \(latitude) and long \(longitude)")
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            
            if error != nil{
                print(error!)
            }
            else{
                
                if let address = placemarks?[0]{
                    print(address)
                    
                    var subThoroughFare = ""
                    var thoroughFare = ""
                    var subLocality  = ""
                    var subAdmin = ""
                    var postalCode = ""
                    var country = ""
                    
                    if address.subThoroughfare != nil{
                        subThoroughFare = address.subThoroughfare!
                    }
                    
                    if address.thoroughfare != nil{
                        thoroughFare = address.thoroughfare!
                    }
                    
                    if address.subLocality != nil{
                        subLocality = address.subLocality!
                    }
                    
                    if address.subAdministrativeArea != nil{
                        subAdmin = address.subAdministrativeArea!
                    }
                    if address.postalCode != nil{
                        postalCode = address.postalCode!
                    }
                    
                    if address.country != nil {
                        country = address.country!
                    }
                    
                    var markerLocation = ""
                
                    markerLocation = subThoroughFare + "," + thoroughFare + "," + subLocality + "," + subAdmin + "," + postalCode + "," + country
                    
                    locationAddress = markerLocation
                    
                    self.currentLocationLabel.text = locationAddress
                    print("Location Address \(locationAddress)")
                    
                    print(subThoroughFare + "\n" + thoroughFare + "\n" + subLocality + "\n" + subAdmin + "\n" + postalCode + "\n" + country)
                }
            }
            
        }
    }
}
