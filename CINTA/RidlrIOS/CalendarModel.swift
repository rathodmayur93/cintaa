//
//  CalendarModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CalendarModel{

    var calendarData = [CalendarData]()
}

class CalendarData{

    var calendarId      = ""
    var calendarDays    = ""
    var calenarUnknown  = ""
    var monday          = ""
    var tuesday         = ""
    var wednesday       = ""
    var thursday        = ""
    var friday          = ""
    var saturday        = ""
    var sunday          = ""
}

