//
//  Users+CoreDataProperties.swift
//  
//
//  Created by Mayur on 22/05/17.
//
//

import Foundation
import CoreData


extension Users {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Users> {
        return NSFetchRequest<Users>(entityName: "Users");
    }

    @NSManaged public var city: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var userEmail: String?

}
