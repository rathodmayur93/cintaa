//
//  MetroParser.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var metroStopList = [MetroParserModel]()
class MetroParser {

    let uiFun = UiUtillity()
    
    func parseMetroStopsCSVToList(){
    
        let queueName = DispatchQueue(label: "stops.metro.processing")
        
        queueName.async {
            
            let csvContent          = self.uiFun.readFromCSV(fileName: "MUM Metro_stops")
            let csvNewLineContent   = csvContent.components(separatedBy: .newlines)
            let delimiter           = ","
            
            let lines:[String] = csvNewLineContent
            
            for line in lines {
                
                let metroStopsParserModel   = MetroParserModel()
                let metroStopsmodel         = MetroStops()
                var metroData               = line.components(separatedBy: delimiter)
                
                if(metroData[0] != ""){
                    
                    metroStopsmodel.stopId      = metroData[0]
                    metroStopsmodel.stopName    = metroData[1]
                    metroStopsmodel.stopLat     = metroData[2]
                    metroStopsmodel.stopLong    = metroData[3]
                
                    metroStopsParserModel.metroStops.append(metroStopsmodel)
                    metroStopList.append(metroStopsParserModel)
                }
                
                
            }
        }
    }

}
