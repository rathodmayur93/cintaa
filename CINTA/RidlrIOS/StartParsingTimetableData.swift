//
//  StartParsingTimetableData.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class StartParsingTimetableData{
    
    func StartSync() {
    
        let cityName = uiFun.readFromDatabase(entityName: "Users", key: "city")
        print("Selected City Name Is \(cityName)")
        let passingCSVData = CityParser()
        passingCSVData.demoReadFromCSV(cityName: cityName)
        
        let calendarParsing = CalendarParse()
        calendarParsing.demoReadFromCSV()
    }
}
