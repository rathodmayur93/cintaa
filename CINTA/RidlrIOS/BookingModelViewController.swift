//
//  BookingModelViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 20/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BookingModelViewController{
    
    var bookingId           = ""
    var companyStatus       = ""
    var bookingDateTime     = ""
    var totalAmount         = ""
    var companyItemId       = ""
    var company             = ""
    var paymentMethod       = ""
    var itemType            = ""
    var paidVia             = ""
    var timestamp           = ""
    var itemSubtype         = ""
}
