//
//  BESTPassRechargeSuccessViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 16/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

var lastPassRecharged = false
class BESTPassRechargeSuccessViewController: UIViewController {

    
    @IBOutlet var busPassNumberLabel        : UILabel!
    @IBOutlet var payableAmount             : UILabel!
    @IBOutlet var bookingIdLabel            : UILabel!
    @IBOutlet var rechargeTypeLabel         : UILabel!
    @IBOutlet var renewalDate               : UILabel!
    @IBOutlet var paidViaLabel              : UILabel!
    @IBOutlet var passRechargeStatus        : UILabel!
    @IBOutlet var bookingIdValueLabel       : UILabel!
    @IBOutlet var amountTextLabel           : UILabel!
    
    @IBOutlet var successIV                 : UIImageView!
    @IBOutlet var containerView             : UIView!
    @IBOutlet var whatsNext                 : UIView!
    
    let uiFun = UiUtillity()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        print("/***** BUS PASS RECHARGE SUCCESS SCREEN *******/")
        
        uiFun.createCleverTapEvent(message: "BUS PASS RECHARGE SUCCESS SCREEN")
        uiFun.createCleverTapEvent(message: "BEST Pass Renewal Success")
        
        let bestCardNumber      = uiFun.readData(key: "bestPassNumber") //  Fetching BEST card number
        busPassNumberLabel.text = bestCardNumber
        
        containerView.isHidden  = false
        whatsNext.isHidden      = false
        
        let date                = Date()
        let calendar            = Calendar.current
        let components          = calendar.dateComponents([.year, .month, .day], from: date)
        
        // Fetchig Year, Month and Day
        let year                = components.year
        let month               = components.month
        let day                 = components.day
        
        renewalDate.text = "\(day!)-\(month!)-\(year!)"
        
        // Check for decide whether the user navigated from profile booking history or from payment page
        if(showBookingReceipt){
            
            switch passType {
            case 0:
                rechargeTypeLabel.text = "Monthly"
            case 1:
                rechargeTypeLabel.text = "Quaterly"
            default:
                break
            }
            if(bookingList[receiptIndex].companyStatus == "SUCCESS"){
              
                // Recharge Successful
                bookingIdLabel.text               = bookingList[receiptIndex].bookingId         // Fetching booking id
                payableAmount.text                = "₹ \(bookingList[receiptIndex].totalAmount)" // Fetching total amount of recharge
                paidViaLabel.text                 = "Paid via \(bookingList[receiptIndex].paidVia)" // Wallet Type
                busPassNumberLabel.text           = bookingList[receiptIndex].companyItemId     // BEST pass number
                
                // Converting timestamp into date format
                let timestampDate                 = NSDate(timeIntervalSince1970: Double(bookingList[receiptIndex].timestamp)!/1000)
                let dayTimePeriodFormatter        = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
                let dateString                    = dayTimePeriodFormatter.string(from: timestampDate as Date)
                renewalDate.text                  = dateString
            }else{
                
                // Recharge Fail
                successIV.image                   = UIImage(named: "ic_fail")
                passRechargeStatus.text           = "Recharge Failed"
                containerView.isHidden            = true
                whatsNext.isHidden                = true
                busPassNumberLabel.text           = bookingList[receiptIndex].companyItemId
                paidViaLabel.text                 = "Paid via \(bookingList[receiptIndex].paidVia)"
                
                // Converting timestamp into date format
                let timestampDate                 = NSDate(timeIntervalSince1970: Double(bookingList[receiptIndex].timestamp)!/1000)
                let dayTimePeriodFormatter        = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
                let dateString                    = dayTimePeriodFormatter.string(from: timestampDate as Date)
                renewalDate.text                  = dateString
            }
        } else{
            // From Payment Page
            
            switch passType {
            case 0:
                rechargeTypeLabel.text = "Monthly"
            case 1:
                rechargeTypeLabel.text = "Quaterly"
            default:
                break
            }
            
            if(paymentResponseListWebview[0].status == "OK" || paymentResponseListWebview[0].status == "SUCCESS"){
                
                // Recharge Successful
                uiFun.createCleverTapEvent(message: "BEST Pass Renewal Response Pass Number \(paymentResponseListWebview[0].data[0].paymentDetails[0].cardNumber)")
    
                print("Status \(paymentResponseListWebview[0].status)")
                print("Status \(paymentResponseListWebview[0].data[0].order_no)")
                print("Status \(paymentResponseListWebview[0].data[0].paymentDetails[0].bookingId)")
 
                busPassNumberLabel.text     = paymentResponseListWebview[0].data[0].paymentDetails[0].cardNumber // BEST pass number
                passRechargeStatus.text     = "Recharge Successful"
                bookingIdLabel.text         = paymentResponseListWebview[0].data[0].paymentDetails[0].bookingId  // Fetching booking id
                payableAmount.text          = "₹ \(paymentResponseListWebview[0].data[0].paymentDetails[0].amount)" // Fetching total amount
                paidViaLabel.text           = paidVia
              
//                if(paymentResponseListWebview[0].data[0].paymentDetails.count == 1){
//                    paidViaLabel.text           = "Paid via \(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentMode) (\(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentGateway))" // Wallet Type
//                }else if(paymentResponseListWebview[0].data[0].paymentDetails.count == 2){
//                    paidViaLabel.text           = "Paid via \(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentMode) & \(paymentResponseListWebview[0].data[0].paymentDetails[1].paymentMode)" // Wallet Type
//                }
                
                
            }else{
            
                // Recharge Fail
                successIV.image             = UIImage(named: "ic_fail")
                passRechargeStatus.text     = "Recharge Failed"
                busPassNumberLabel.text     = paymentResponseListWebview[0].data[0].paymentDetails[0].cardNumber
                bookingIdLabel.text         = paymentResponseListWebview[0].data[0].paymentDetails[0].bookingId
                payableAmount.text          = "₹ \(paymentResponseListWebview[0].data[0].paymentDetails[0].amount)"
                paidViaLabel.text           = "Paid via \(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentMethod)"
                containerView.isHidden  = true
                whatsNext.isHidden      = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonTapped(_ sender: AnyObject) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
    
    @IBAction func closeButtonTapped(_ sender: AnyObject) {
        // "showBookingReceipt" decide where to navigate on back button tap
        if(showBookingReceipt){
            showBookingReceipt = false
            uiFun.navigateToScreem(identifierName: "BookingsViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        }
    }

}
