//
//  ProcessTripPassResponse.swift
//  RidlrIOS
//
//  Created by Mayur on 04/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var tripInfoList    = [TripPassModel]()
class ProcessTripPassResponse {

    
    func processTripResponse(respnse : JSON){

        let dataJSON                    = respnse["data"]
        let bookingDetailJSON           = dataJSON["bookingDetails"]
        let bookingPaymentDetail        = bookingDetailJSON.arrayValue[bookingDetailJSON.count - 1]["bookingPaymentDetailsList"]
        
        let tripModel                   = TripPassModel()
        tripModel.bookingId             = String(describing: bookingDetailJSON.arrayValue[bookingDetailJSON.count - 1]["bookingId"])
        
        tripModel.amount                = String(describing :bookingPaymentDetail.arrayValue[0]["paymentAmount"])
        tripModel.paymnetMethod         = String(describing :bookingPaymentDetail.arrayValue[0]["paymentMethod"])
        tripModel.paymentGateway        = String(describing :bookingPaymentDetail.arrayValue[0]["paymentGateway"])
        
        
        let bookingItemList             = bookingDetailJSON.arrayValue[bookingDetailJSON.count - 1]["bookingItemsList"]
        let routeList                   = bookingItemList.arrayValue[0]["bookingItemRoutesList"]
        
        tripModel.originStation         = String(describing: routeList.arrayValue[0]["sourceName"])
        tripModel.destinationStation    = String(describing: routeList.arrayValue[0]["destinationName"])
        tripModel.expiryDate            = String(describing: bookingItemList.arrayValue[0]["expiryDate"])
        
        let itemSubType                 = String(describing : bookingItemList.arrayValue[0]["itemSubType"])

        let itemAttribute               = bookingItemList.arrayValue[0]["itemAttribute"]
        tripModel.trips                 = String(describing: itemAttribute["trips"])
        tripModel.masterQRCode          = String(describing: itemAttribute["masterQrCodeId"])
        tripModel.balance               = String(describing: itemAttribute["balance"])
        tripModel.itemSubType           = itemSubType
      
        if(itemSubType == "TRIP_PASS"){
            uiFun.writeData(value: "yes", key: "metroTripStatus")
            uiFun.writeData(value: String(describing: bookingDetailJSON.arrayValue[bookingDetailJSON.count - 1]["bookingId"]), key: "tripBookingId")
            uiFun.writeData(value: String(describing: itemAttribute["masterQrCodeId"]), key: "tripMasterQRCode")
            uiFun.writeData(value: String(describing: routeList.arrayValue[0]["sourceName"]), key: "tripPassSourceName")
            uiFun.writeData(value: String(describing: routeList.arrayValue[0]["destinationName"]), key: "tripPassDestinationeName")
            uiFun.writeData(value: String(describing: itemAttribute["trips"]), key: "tripPassRemainingTrip")
            uiFun.writeData(value: String(describing: bookingItemList.arrayValue[0]["expiryDate"]), key: "tripPassExpiry")
            
        }else{
            uiFun.writeData(value: "yes", key: "metroStoreValuePass")
            uiFun.writeData(value: String(describing: itemAttribute["masterQrCodeId"]), key: "metroStoreValueMasterQRCode")
            uiFun.writeData(value: String(describing: bookingDetailJSON.arrayValue[bookingDetailJSON.count - 1]["bookingId"]), key: "storeValueBookingId")
            uiFun.writeData(value:  String(describing: itemAttribute["balance"]), key: "storeValuePassBalance")
            uiFun.writeData(value: String(describing: bookingItemList.arrayValue[0]["expiryDate"]), key: "storeValueExpiry")
        }
        
        tripInfoList.append(tripModel)
    }
}
