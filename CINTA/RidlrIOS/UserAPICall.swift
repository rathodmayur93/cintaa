//
//  UserAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 01/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class UserAPICall {

    let util = Utillity()
    func makeAPICall(fromViewController : ProfileOTPViewController){
        
        let constant = Constants()
        let url = "\(constant.STAGING_BASE_URL)/user"
        
        print("Users API Call URL \(url)")
        util.APICall(apiUrl: url, jsonString: "", requestType: "GET", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.gotUserAPIResponse(response: jsonResponse)
                print("Successfully User API Call Done.... \(jsonResponse) ")
                return
            }else {
                print("User API Call Failed....Try Harder Mayur ")
                return
            }
        }
    }

    
}
