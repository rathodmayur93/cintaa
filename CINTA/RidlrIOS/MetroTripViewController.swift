//
//  MetroTripViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 17/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit


var tripOriginStation                   = "Origin Station"
var tripDestinationStation              = "Destination Station"

var tripOriginStationAgencyId           = 1
var tripDestinationStationAgencyId      = 1

var isTripOriginStationSelected         = false

class MetroTripViewController: UIViewController {

    @IBOutlet var originStationLabel        : UILabel!
    @IBOutlet var destinationStationLabel   : UILabel!
    @IBOutlet var totalAmountLabel          : UILabel!
   
    @IBOutlet var confirmBT                 : UIButton!
    
    var tripTotalFare = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        // Origin Station Click
        let tapGesture                                  = UITapGestureRecognizer(target: self, action: #selector(originStationAction))
        tapGesture.numberOfTapsRequired                 = 1
        originStationLabel.isUserInteractionEnabled     = true
        originStationLabel.addGestureRecognizer(tapGesture)
        
        // destination Station Click
        let tapGesture1                             = UITapGestureRecognizer(target: self, action: #selector(destinationStationAction))
        tapGesture1.numberOfTapsRequired                    = 1
        destinationStationLabel.isUserInteractionEnabled    = true
        destinationStationLabel.addGestureRecognizer(tapGesture1)
        
        // check for setting station name
        originStationLabel.text         = tripOriginStation
        destinationStationLabel.text    = tripDestinationStation
        
        // change the text color when station gets selected
        if(tripOriginStation != "Origin Station"){
            originStationLabel.textColor           = UIColor.black
        }
        // change the text color when station gets selected
        if(tripDestinationStation != "Destination Station"){
            destinationStationLabel.textColor      = UIColor.black
        }
        
        // change the confirm detail button background color
        if(tripOriginStation != "Origin Station" && tripDestinationStation != "Destination Station"){
            confirmBT.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        }
        
        getFareOfJourney()
    }
    
    
    
    func originStationAction(){
        isTripOriginStationSelected     = true
        uiFun.navigateToScreem(identifierName: "MetroStationsViewController", fromController: self)
        
    }
    
    func destinationStationAction(){
        isTripOriginStationSelected     = false
        uiFun.navigateToScreem(identifierName: "MetroStationsViewController", fromController: self)
    }
    
    func resetStations(){
        // this will reset starting and destination station
        tripOriginStation           = "Origin Station"
        tripDestinationStation      = "Destination Station"
        
    }
    
    func getFareOfJourney(){
        
        if(tripOriginStation != "Origin Station" && tripDestinationStation != "Destination Station"){
            
            let fetchFare           = MetroFetchTokenFare()
            let fare                = fetchFare.tripFare[tripOriginStationAgencyId - 1][tripDestinationStationAgencyId - 1]
            totalAmountLabel.text   = "₹ \(fare)"
            tripTotalFare           = "\(fare)"
            print("Metro Trip Fare \(fare)")
            
        }
    }
    
    @IBAction func confirmBTAction(_ sender: Any) {
        
        let profileStatus       = uiFun.readData(key: "ProfileCreation")
        rechargeAmount          = tripTotalFare
        print("Metro Trip Recharge Amount is \(rechargeAmount)")
        uiFun.writeData(value: "metroTrip", key: "ticketingAgency")
     
        if(profileStatus == "done"){
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }else{
            uiFun.createCleverTapEvent(message: "Profile creation touchpoints Metro Trip Pass")
            profileBackNavigation = "MetroHomeScreenViewController"
            uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        resetStations()
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    
    
}
