//
//  CityParserModel.swift
//  RidlrIOS
//
//  Created by Mayur on 10/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CityParserModel{
    
    var cityModel = [CityModel]()
}
class CityModel{
    
    var cityId = ""
    var cityName = ""
    var minLat = ""
    var maxLat = ""
    var minLong = ""
    var maxLong = ""
    var centerLat = ""
    var centerLong = ""
    var cityBound = ""
    var mode = [Mode]()
    
}

class Mode{

    var modeId = ""
    var modeName = ""
    var modePriority = ""
    var agencies = [Agency]()
}

class Agency {
    
    var agencyId = ""
    var agencyLongName = ""
    var agencyShortName = ""
    var agencyColor = ""

}
