//
//  RouteSearchModel.swift
//  RidlrIOS
//
//  Created by Mayur on 02/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class RouteSearchModel {

    var plan  = [Plan]()
}

class Plan{

    var walkOnly = [WalkOnly]()
    var autoCab = [WalkOnly]()
}

class WalkOnly{

    var localTransport = ""
    var duration       = ""
    var starttimestamp = ""
    var endtimestamp   = ""
    var legs           = [Legs]()
    var walkDuration   = ""
    
    
}
class Legs {

    var mode                = ""
    var starttimestamp      = ""
    var endtimestamp        = ""
    var duration            = ""
    var routeShortName      = ""
    var routeLongName       = ""
    var agencyId            = ""
    var routeid             = ""
    var distance            = ""
    var legGeometry         = [LegGeometry]()
    var startingPoint       = [StartingPoint]()
    var endPoint            = [EndPoint]()
    var intermediatestops   = [IntermediateStops]()
    var walkinstructions    = [String]()
    
}

class LegGeometry{

    var points = ""
    var length = ""
}

class StartingPoint{

    var name = ""
    var lat = ""
    var lon = ""
    var stopid = ""
    
}

class EndPoint{

    var name = ""
    var lat = ""
    var lon = ""
    var stopid = ""
}

class IntermediateStops{

    var name = ""
    var lat = ""
    var lon = ""
}

