//
//  MonoRoutesModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MonoRoutesModel{
    
    var monoRoute = [MonoRoute]()
}

class MonoRoute{
    
    var routeId             = ""
    var routeShortName      = ""
    var routeLongName       = ""
    var routeDirectitonId   = ""
    var routeSourceId       = ""
    var routeDestinationId  = ""
}
