//
//  CardPaymentModel.swift
//  RidlrIOS
//
//  Created by Mayur on 15/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CardPaymentModel{
    
    var cardData                    = [CardPaymentData]()
}
class CardPaymentData{

    var order_no                    = ""
    var isPaymentFinal              = ""
    var paymentDetails              = [PaymentDetails]()
    var bookingDetails              = [BookingDetails]()
}
class PaymentDetails{

    var amount                      = ""
    var paymentGateway              = ""
    var notifyUrl                   = ""
    var paymentMode                 = ""
    var responseType                = ""
    var message                     = ""
    var merchantAccessKey           = ""
    var returnUrl                   = ""
    var signature                   = ""
    var transactionId               = ""
}
class BookingDetails{

    var unpaidAmount                = ""
    var bookingDateTime             = ""
    var deviceId                    = ""
    var company                     = ""
    var bookingId                   = ""
    var paymentStatus               = ""
    var action                      = ""
    var companyReferenceId          = ""
    var totalTax                    = ""
    var paymentStatusMessage        = ""
    var bookingTicketValidationList = ""
    var companyStatusMessage        = ""
    var totalAmount                 = ""
    var totalDiscount               = ""
    var baseAmount                  = ""
    var companyStatus               = ""
    var totalSurcharge              = ""
    var bookingItemsList            = [BookingItemsList]()
    var bookingPaymentDetailsList   = [BookingPaymentDetailsList]()
    var clientBooking               = [ClientBooking]()
}

class BookingItemsList{

    var itemType                    = ""
    var companyItemId               = ""
    var bookingItemId               = ""
    var issuerIdentifier            = ""
    var itemSubType                 = ""
    var bookingItemRoutesList       = ""
    var userType                    = ""
    var userDob                     = ""
    var activationDate              = ""
    var validityCriteria            = ""
    var totalTax                    = ""
    var totalConcession             = ""
    var validity                    = ""
    var totalAmount                 = ""
    var userName                    = ""
    var expiryDate                  = ""
    var totalDiscount               = ""
    var baseAmount                  = ""
    var totalSurcharge              = ""
    var issuerTag                   = ""
}

class BookingPaymentDetailsList{

    var cardType                    = ""
    var paymentGateway              = ""
    var bankCode                    = ""
    var cardToken                   = ""
    var maskedCardNumber            = ""
    var bookingPaymentId            = ""
    var paymentMethod               = ""
    var paymentStatus               = ""
    var paymentStatusMessage        = ""
    var paymentAmount               = ""
    var transactionRefId            = ""
}
class ClientBooking{

    var email                       = ""
    var mobile                      = ""
    var clientRefId                 = ""
}

