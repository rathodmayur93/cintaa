//
//  FetchBusStopsAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 08/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusStopsAPI{
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TimetableStationViewController, agencyId : String, routeId : String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/stop/agencyid/\(agencyId)/routeid/\(routeId)/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Fetch Stops URL \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.FetchBusStopResponse(response: jsonResponse)
                print("Successfully Timetable BUS Stops fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable BUS Stops fetching failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
}
