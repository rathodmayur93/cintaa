//
//  NewsFeedreplyTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 15/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class NewsFeedreplyTableViewCell: UITableViewCell {

    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var username: UILabel!
    @IBOutlet var time: UILabel!
    @IBOutlet var likeCount: UILabel!
    
    @IBOutlet var updateContent: UITextView!
    
    @IBOutlet var likeButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func likeButtonTapped(_ sender: AnyObject) {
        
    }
    
    @IBAction func deleteButtonTapped(_ sender: AnyObject) {
        
    }
    
}
