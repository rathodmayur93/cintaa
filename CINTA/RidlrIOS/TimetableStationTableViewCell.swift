//
//  TimetableStationTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 22/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class TimetableStationTableViewCell: UITableViewCell {

    @IBOutlet var stationName: UILabel!
    @IBOutlet var agencyName: UILabel!
    @IBOutlet var from: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
