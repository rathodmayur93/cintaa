//
//  OfferViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 24/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class OfferViewController: UIViewController {

    @IBOutlet var webView: UIWebView!
    var url              : NSURL    = NSURL()
    let constant                    = Constants()
    @IBOutlet var headingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if(fromBlog){
            url         = NSURL(string: constant.BLOG_URL)! // BLOG Url
            fromBlog            = false
            headingLabel.text   = "BLOG"    // Toolbar Heading Text
            print("From Blog")
            uiFun.createCleverTapEvent(message: "Blog Webview Screen")
        }else{
            url = NSURL(string: constant.OFFER_URL)!    // Offer Url
            print("From Offer")
            headingLabel.text = "OFFERS"    // Toolbar Heading Text
            uiFun.createCleverTapEvent(message: "Offers WebView Screen")
        }
        
        
        let requestObj = NSURLRequest(url: url as URL);
        webView.loadRequest(requestObj as URLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
}
