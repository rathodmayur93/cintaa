//
//  PaymentRechargeAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 03/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class PaymentRechargeAPICall {
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    

    func makeAPICall(fromViewController : PaymentViewController){
        uiFun.showIndicatorLoader()
        let randomNum:UInt32 = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String = String(randomNum)
        let timeStamp = Int(NSDate().timeIntervalSince1970)
        
        //STAGING let url = "http://testapi.ridlr.in:8080/master/v1/payment/options?\(randomNumString)&current_time=\(timeStamp)"
        let url = "\(constant.STAGING_BASE_URL)/payment/options?\(randomNumString)&current_time=\(timeStamp)" // Payment API Call
        
        print("Payment Option Fetcing Balance URL \(url)")
        util.APICall(apiUrl: url, jsonString: "", requestType: "GET", header: true){result, error in
            
            if let jsonResponse = result{
                fromViewController.gotResponseFromArhitecture(response: jsonResponse)
                print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
            }else {
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
        }
    }
}
