//
//  FetchBusStopsFromAtoBAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusStopsFromAtoBAPI{
    
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TimetableStationViewController, searchString : String, agencyId : String){
        
        let cityName = uiFun.readFromDatabase(entityName: "Users", key: "city")
        let url = "\(constant.TIMETABLE_BUS_STOP)\(cityName.lowercased())/stop/_search"
        
        print("Timetable Bus Number Fetching URL \(url)")
        
        util.APICall(apiUrl: url, jsonString: fetchBusNumberJSON(searchString: searchString)!, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.FetchBusStopResponseFromAtoB(response: jsonResponse)
                print("Successfully Bus Stops A to B Fetching Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Fetching Bus Number Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    
    func fetchBusNumberJSON(searchString : String) -> String?{
        
        let sortArray:NSMutableArray = NSMutableArray()
        let sortDic:NSMutableDictionary = NSMutableDictionary()
        sortDic.setValue("asc", forKey: "order")
        
        let wildCardDic:NSMutableDictionary = NSMutableDictionary()
        wildCardDic.setValue("*\(searchString.lowercased())*", forKey: "l_stop_name_index")
        
        let wildCard : NSMutableDictionary = NSMutableDictionary()
        wildCard.setValue(wildCardDic, forKey: "wildcard")
        
        let wildCardDic1:NSMutableDictionary = NSMutableDictionary()
        wildCardDic1.setValue(agencyId, forKey: "l_agency_id")
        
        let wildCard1 : NSMutableDictionary = NSMutableDictionary()
        wildCard1.setValue(wildCardDic1, forKey: "wildcard")
        
        
        let mustArray:NSMutableArray = NSMutableArray()
        mustArray.add(wildCard)
        mustArray.add(wildCard1)
        
        let boolDic:NSMutableDictionary = NSMutableDictionary()
        boolDic.setValue(mustArray, forKey: "must")
        
        let queryDic:NSMutableDictionary = NSMutableDictionary()
        queryDic.setValue(boolDic, forKey: "bool")
        
        let sort:NSMutableDictionary = NSMutableDictionary()
        sort.setValue(sortDic, forKey: "route_short_name")
        sortArray.add(sort)
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(queryDic, forKey: "query")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("fetch bus stops a to b json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

}
