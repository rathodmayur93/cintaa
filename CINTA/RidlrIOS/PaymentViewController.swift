//
//  PaymentViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 10/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import M13Checkbox
import SwiftyJSON
import DLRadioButton
import SwiftLuhn
import CreditCardValidator

var amountToAddValue            = ""
var passType                    = 0
var passRechargeBookingId       = ""
var passRechargePaymentDetail   = ""
var passRechargeTimeStamp       = ""
var isRidlrCBChecked            = true

var paymentResponseList         = [CardPaymentModel]()
var paidVia                     = ""
var metroTokenResposneList      = [MetroTokenJSONModel]()

class PaymentViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    var uiFun = UiUtillity()
    var ridlrColors = RidlrColors()
    
    @IBOutlet var passNumberView        : UIView!
    @IBOutlet var passDetailTF          : UITextView!
    
    @IBOutlet var ridlrPointsCB         : M13Checkbox!
    @IBOutlet var payAndProceed         : UIButton!
    
    @IBOutlet var ridlrPointsView       : UIView!
    @IBOutlet var cardUIView            : UIView!
    
    @IBOutlet var walletBalanceLabel    : UILabel!
    @IBOutlet var amountToAddLabel      : UILabel!
    @IBOutlet var citrusBalace          : UILabel!
    @IBOutlet var rcashLabel            : UILabel!
    @IBOutlet var payableAmount         : UILabel!
    @IBOutlet var payableAmountLabel    : UILabel!
    @IBOutlet var passNumber            : UILabel!
    
    @IBOutlet var paymentTypeSegment    : UISegmentedControl!
    @IBOutlet var citrusLogo            : UIImageView!
    @IBOutlet var citrusSeparator       : UIView!
    
    @IBOutlet var loader                : UIActivityIndicatorView!
    
    var passCardNumber                  : String?
    var amountToPay                     : Double?
    var ridlrPoints                     : Double?
    var citrusWalletBalance             : Double?
    var mobikwikWalletBalance           : Double?
    var freechargeWalletBalance         : Double?
    var extraAmountToAdd                : Int?
    var scrollView                      : UIScrollView = UIScrollView()
    
    /************** NETBANKING UI ****************/
    
    @IBOutlet var bankPicker            : UIPickerView!
    var bankLabel                       : UILabel = UILabel()
    var bankArrayList                   = [BankModel]()
    let constant                        = Constants()
    let defaults                        = UserDefaults.standard
    var netBankingIssueCodePay          = ""
    
    /************** CARD UI ****************/
    @IBOutlet var saveCardB             : DLRadioButton!
    @IBOutlet var creditCardRB          : DLRadioButton!
    @IBOutlet var debitCardRB           : DLRadioButton!
    
    @IBOutlet var cvvTF                 : UITextField!
    @IBOutlet var monthYearTF           : UITextField!
    @IBOutlet var caardNumberTF         : UITextField!
    
    @IBOutlet var cardImage             : UIImageView!
    
    /************** FREECHARGE UI ***************/
    @IBOutlet var freechargeView        : UIView!
    @IBOutlet var freechargeLabel       : UILabel!
    @IBOutlet var freechargeRadio       : DLRadioButton!
    var isFreechargeConfigured          = false
    
    /************** MOBIKWIK UI ***************/
    @IBOutlet var mobikwikView          : UIView!
    @IBOutlet var mobikwikLabel         : UILabel!
    @IBOutlet var mobikwikRadio         : DLRadioButton!
    var isMobikwikConfigured            = false
    
    /************* CITRUS UI ****************/
    @IBOutlet var citrusView            : UIView!
    @IBOutlet var citrusLabel           : UILabel!
    @IBOutlet var citrusRadio           : DLRadioButton!
    var isCitrusConfigured              = false
    
    /************** MOBIKWIK UI 1***************/
    @IBOutlet var mobikwikView1          : UIView!
    @IBOutlet var mobikwikLabel1         : UILabel!
    @IBOutlet var mobikwikRadio1         : DLRadioButton!
    var isMobikwikConfigured1            : Bool!
    
    
    var whichCardSelected               = ""
    var transcationMode                 = "Wallet"
    var slectedCardType                 : CreditCardValidationType!
    var selectedWallet                  = ""
    
    var year                            : Int!
    var month                           : Int!
    
    var task                            : URLSessionDataTask!
    var initiator                       = ""
    
    var isConnectedToInternet           = true
    var showLoader                      = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("/***** PAYMENT SCREEN *******/")
        
        setUI()
        
        bankPicker.delegate     = self
        print("Internet connection \(uiFun.isConnectedToNetwork())")
        
        // Response is in function gotResponseFromArhitecture()
        let apiCall             = PaymentRechargeAPICall()
        apiCall.makeAPICall(fromViewController: self)
        
        self.view.addSubview(payableAmountLabel)
        
        caardNumberTF.delegate  = self
        monthYearTF.delegate    = self
        isFromProfileWallet     = false
        
        //Notification center for keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        // When User Taps On Citrus Wallet
        let tapGestureCitrus                        = UITapGestureRecognizer(target: self, action: #selector(citrusWalletLabelTapped))
        tapGestureCitrus.numberOfTapsRequired       = 1
        citrusLabel.isUserInteractionEnabled        = true
        citrusLabel.addGestureRecognizer(tapGestureCitrus)
        
        // When User Taps On Mobikwik Wallet
        let tapGestureMobikwik                      = UITapGestureRecognizer(target: self, action: #selector(mobikwikWalletLabelTapped))
        tapGestureMobikwik.numberOfTapsRequired     = 1
        mobikwikLabel.isUserInteractionEnabled      = true
        mobikwikLabel.addGestureRecognizer(tapGestureMobikwik)
        
        // When User Taps On Mobikwik Wallet
        let tapGestureMobikwik1                      = UITapGestureRecognizer(target: self, action: #selector(mobikwikWalletLabelTapped))
        tapGestureMobikwik1.numberOfTouchesRequired  = 1
        tapGestureMobikwik1.numberOfTapsRequired     = 1
        mobikwikLabel1.isUserInteractionEnabled      = true
        mobikwikLabel1.addGestureRecognizer(tapGestureMobikwik1)
        
        // When User Taps On Freecharge Wallet
        let tapGestureFreecharge                    = UITapGestureRecognizer(target: self, action: #selector(freechargeWalletLabelTapped))
        tapGestureFreecharge.numberOfTapsRequired   = 1
        freechargeLabel.isUserInteractionEnabled    = true
        freechargeLabel.addGestureRecognizer(tapGestureFreecharge)
        
        
        uiFun.createCleverTapEvent(message: "PAYMENT SCREEN")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        if(showLoader){
            uiFun.showIndicatorLoader()
        }else{
            uiFun.hideIndicatorLoader()
        }
        let date        = Date()
        let calendar    = Calendar.current
        let components  = calendar.dateComponents([.year, .month, .day], from: date)
        
        // Fetching Current Month and Year
        year            =  components.year
        month           = components.month
        
    }
    
    override func viewDidLayoutSubviews() {
        
        let agencyName = defaults.string(forKey: "ticketingAgency")! // Fetching agency name i.e either Metro or BEST
        
        if(agencyName == "Metro"){
            initiator = "Metro Recharge"
            uiFun.createCleverTapEvent(message: "Payment flow initiator Metro Recharge")
            UpdateUIForMetro()
        
        }else if(agencyName == "MetroToken"){
            initiator  = "MetroToken"
            uiFun.createCleverTapEvent(message: "Payment flow initiator Metro Token")
            //UpdateUIForMetro()
        }
        else{
            initiator = "BEST Pass"
            uiFun.createCleverTapEvent(message: "Payment flow initiator BEST Pass")
        }
    }
    
    func keyboardWillShow(sender: NSNotification) {
        // When Keyboard is open UI should move up so user can enter card detail
        let agencyName = defaults.string(forKey: "ticketingAgency")! // Fetching agency name i.e either Metro or BEST
        if let keyboardSize = (sender.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                if(agencyName != "Metro"){
                    self.view.frame.origin.y = keyboardSize.height
                }
            }
        }
    }
    
    func keyboardWillHide(sender: NSNotification) {
        // Hide Keyboard
        self.view.frame.origin.y += 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func setUI(){
        
        showCitrusWallet()
        
        cardUIView.isHidden         = true
        bankLabel.isHidden          = true
        bankPicker.isHidden         = true
        isRidlrCBChecked            = true
        loader.isHidden             = true
        
        // Keyboard Type
        caardNumberTF.keyboardType  = UIKeyboardType.numberPad
        monthYearTF.keyboardType    = UIKeyboardType.numberPad
        cvvTF.keyboardType          = UIKeyboardType.numberPad
        
        let ridlrToken              = uiFun.readData(key: "ridlrToken")         // Fetching Ridlr Token
        let agencyName              = uiFun.readData(key: "ticketingAgency")    // Fetching Ticketing Agency
        
        // Intialzing the Loader
        loader.layer.cornerRadius           = 10
        loader.activityIndicatorViewStyle   = .whiteLarge
        loader.backgroundColor              = uiFun.hexStringToUIColor(hex: ridlrColors.ridlr_brand_color_light)
        
        print("Ridlr Token Is \(ridlrToken)")
        print("Agency Name is \(agencyName)")
        
        loader.startAnimating()
        
        ridlrPointsCB.checkState    = .unchecked
        
        if(agencyName == "Metro"){
            
            payableAmount.text       = rechargeAmount                           // Recharge Amount
            passNumber.text          = "Card : \(metroCardNumber)"              // Metro Card Number
            passDetailTF.text        = "Recharge Amount : \(rechargeAmount)"
            
            // Intializing payableAmountLabel
            payableAmountLabel.frame = CGRect(x: 240, y: 60, width: 120, height: 36)
            ridlrPointsView.isHidden = true
            
        }else if(agencyName == "Best"){
            
            // Fetching BEST pass number
            passNumber.text     = "Pass Number : \(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].companyIdentifier)"
            passDetailTF.text   = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].itemSubType
      
        } else if(agencyName == "MetroToken"){
            payableAmount.text       = rechargeAmount                           // Recharge Amount
            passNumber.text          = "\(tokenOriginStationName)    >   \(tokenDestinationStationName)"
            passDetailTF.text        = "Versova - Ghatkopar \(tokenNumberOfPassengers) ticket"
            //ridlrPointsView.isHidden = true
            mobikwikView.isHidden      = true
        } else if(agencyName == "metroTrip"){
            
            payableAmount.text       = rechargeAmount
            passNumber.text          = "\(tripOriginStation)    >   \(tripDestinationStation)"
            passDetailTF.text        = "Versova - Ghatkopar \(tokenNumberOfPassengers) ticket"
        
        } else if(agencyName == "Penalty"){
            payableAmount.text       = rechargeAmount
            passNumber.text          = "\(penalyList[0].bookingRoute[0].sourceName)    >   \(penaltyStation)"
            passDetailTF.text        = "Versova - Ghatkopar"
        } else if(agencyName == "STORE_VALUE_PASS"){
            payableAmount.text       = rechargeAmount
            //passNumber.text          = "\(penalyList[0].bookingRoute[0].sourceName)    >   \(penaltyStation)"
            passDetailTF.text        = "Versova - Ghatkopar"
        }
        
        payAndProceed.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        payAndProceed.backgroundColor       = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor) // Proceed Background Color
        
        passDetailTF.backgroundColor        = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        passNumberView.backgroundColor      = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        // Blue Border Around Ridlr Points
        ridlrPointsView.layer.cornerRadius  = 5
        ridlrPointsView.layer.borderWidth   = 1
        ridlrPointsView.layer.borderColor   = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor).cgColor
        
        // Net banking select bank label
        bankLabel.frame                     = CGRect(x: 16, y: 400, width: self.view.frame.width, height: 18)
        bankLabel.text                      = "Select Bank"
        bankLabel.font                      = UIFont.boldSystemFont(ofSize: 16.0)
        bankLabel.translatesAutoresizingMaskIntoConstraints = true
        bankLabel.textAlignment             = .center
        self.view.addSubview(bankLabel)
        
        let horizontalConstraint    = NSLayoutConstraint(item: bankLabel, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint      = NSLayoutConstraint(item: bankLabel, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0)
        let widthConstraint         = NSLayoutConstraint(item: bankLabel, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
        let heightConstraint        = NSLayoutConstraint(item: bankLabel, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 100)
        
        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        caardNumberTF.addTarget(self, action: #selector(checkCardNumber), for: UIControlEvents.editingChanged)
        monthYearTF.addTarget(self, action: #selector(checkYearMonth), for: UIControlEvents.editingChanged)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /************ DETECTING KEYBOARD BACKPRESS EVENT *****************/
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let  char       = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            
            // If blank space occured while pressing backpress it shoudl remove two characters. Card Number
            if(caardNumberTF.isFirstResponder){
                if(range.location % 5 == 0 && (textField.text?.characters.count)! > 1){
                    let index  = textField.text?.characters.index((textField.text?.startIndex)!, offsetBy: (range.location - 1))
                    print("Text Field Index \(textField.text?[index!])")
                
                    if(textField.text?[index!] == " "){
                        textField.text?.remove(at: (textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location))!)
                    }
                }
            }
            
            // Expiry Date Logic while pressing back space
            if(monthYearTF.isFirstResponder){
                if((textField.text?.characters.count)! > 1){
                
                    let index  = textField.text?.characters.index((textField.text?.startIndex)!, offsetBy: (range.location))
                    // Inserting "/" while entering month and year
                    if(textField.text?[index!] == "/"){
                        print("Text Field Index 1201 \(textField.text?[index!])")
                        textField.text?.remove(at: (textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location - 1))!)
                        print("In Expiry Date")
                    }
                }
            }
        }
        return true
    }
    
    /************************** BANK LIST PICKER VIEW METHODS****************************/
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print("Picker Data \(bankArrayList.count)")
        return bankArrayList.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Picker View \(bankArrayList[row].bankName)")
        return bankArrayList[row].bankName
        
    }
    
    // The number of columns of data
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // When user select bank from picker view
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(bankArrayList.count != 0){
            bankLabel.text          = bankArrayList[row].bankName
            netBankingIssueCodePay  = bankArrayList[row].issuerCode
        }
        self.view.endEditing(true)
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        
        var label = view as! UILabel!
        
        if label == nil {
            label = UILabel()
        }
        
        bankLabel.text          = bankArrayList[0].bankName     // Setting bank name from bankArrayList
        netBankingIssueCodePay  = bankArrayList[0].issuerCode   // Assign Selected Bank Issuer Code
        
        let data                = bankArrayList[row].bankName
        let title               = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0, weight: UIFontWeightRegular)])
        label?.attributedText   = title
        label?.textColor        = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor) // Text Color In Picker View
        label?.textAlignment    = .center
        return label!
    }
    /************************** CARD VIEW METHODS ****************************/
    
    @IBAction func saveCardRBAction(_ sender: AnyObject) {
        
        creditCardRB.isSelected = false
        debitCardRB.isSelected  = false
        whichCardSelected       = "Save"
    }
    
    @IBAction func creditCardRBAction(_ sender: AnyObject) {
        
        saveCardB.isSelected        = false
        debitCardRB.isSelected      = false
        passRechargePaymentDetail   = "CREDIT_CARD"
        whichCardSelected           = "CREDIT_CARD"
    }
    
    @IBAction func debitCardRBAction(_ sender: AnyObject) {
    
        creditCardRB.isSelected     = false
        saveCardB.isSelected        = false
        passRechargePaymentDetail   = "DEBIT_CARD"
        whichCardSelected           = "DEBIT_CARD"
    }
    
    // When Citrus Wallet Tapepd
    func citrusWalletLabelTapped(){
        
        if(isCitrusConfigured){
            mobikwikRadio1.isSelected    = false
            freechargeRadio.isSelected   = false
            citrusRadio.isSelected       = true
            selectedWallet               = "CITRUSPAY"
            UpdateRequiredAmountWallet(paymentGateway: "CITRUSPAY")
            print("CITRUSPAY")
        }else{
            addingWhichWallet = "CITRUSPAY"
            uiFun.navigateToScreem(identifierName: "AddWalletViewController", fromController: self)
        }
    }
    // When Mobikwik Wallet Tapepd
    func mobikwikWalletLabelTapped(){
        
        if(isMobikwikConfigured){
            citrusRadio.isSelected       = false
            freechargeRadio.isSelected   = false
            mobikwikRadio1.isSelected    = true
            selectedWallet               = "MOBIKWIK"
            UpdateRequiredAmountWallet(paymentGateway: "MOBIKWIK")
            print("MOBIKWIK")
        }else{
            addingWhichWallet            = "MOBIKWIK"
            uiFun.navigateToScreem(identifierName: "AddWalletViewController", fromController: self)
        }
    }
    
    // When Freecharge Wallet Tapepd
    func freechargeWalletLabelTapped(){
    
        if(isFreechargeConfigured){
            citrusRadio.isSelected        = false
            mobikwikRadio1.isSelected     = false
            freechargeRadio.isSelected    = true
            selectedWallet                = "FREECHARGE"
            UpdateRequiredAmountWallet(paymentGateway: "FREECHARGE")
            print("FREECHARGE")
        }else{
            addingWhichWallet             = "FREECHARGE"
            uiFun.navigateToScreem(identifierName: "AddWalletViewController", fromController: self)
        }
    }
    
    
    func UpdateRequiredAmountWallet(paymentGateway : String){
        
        
        switch paymentGateway {
            case "CITRUSPAY":
                
                if(Int(citrusWalletBalance!) >= Int(payableAmount.text!)!){
                    // If citrus balance is greater than payable amount set button title to proceed to pay
                    payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
                }else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
                    // If citrus balance is lesser than payable amount set button title to required to amount to pay
                    let amountToAdd     = Float(payableAmount.text!)! - Float(citrusWalletBalance!)
                    amountToAddValue    = "\(amountToAdd)"
                    print("Amount to add \(amountToAdd)")
                    payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)
            }
            break
        case "MOBIKWIK":
            if(Int(mobikwikWalletBalance!) >= Int(payableAmount.text!)!){
                // If mobikwik balance is greater than payable amount set button title to proceed to pay
                payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            }else if(Int(mobikwikWalletBalance!) < Int(payableAmount.text!)!){
                // If mobikwik balance is lesser than payable amount set button title to required to amount to pay
                let amountToAdd = Float(payableAmount.text!)! - Float(mobikwikWalletBalance!)
                amountToAddValue    = "\(amountToAdd)"
                print("Amount to add \(amountToAdd)")
                payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)

            }
            break
        case "FREECHARGE":
            if(Int(freechargeWalletBalance!) >= Int(payableAmount.text!)!){
                // If freecharge balance is greater than payable amount set button title to proceed to pay
                payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            }else if(Int(freechargeWalletBalance!) < Int(payableAmount.text!)!){
                // If freecharge balance is lesser than payable amount set button title to required to amount to pay
                let amountToAdd = Float(payableAmount.text!)! - Float(freechargeWalletBalance!)
                amountToAddValue    = "\(amountToAdd)"
                print("Amount to add \(amountToAdd)")
                payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)
            }
            break
            
        default:
            break
        }
        
    }

    
    
    /************************** CARD VIEW METHODS ENDS ****************************/
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        
        let agency = uiFun.readData(key: "ticketingAgency") // Fetching Tickeing Agency i.e Either Metro or BEST
        
        if(agency == "Metro"){
            uiFun.navigateToScreem(identifierName: "MetroCardViewController", fromController: self)
        }else if(agency == "Best"){
            uiFun.navigateToScreem(identifierName: "RenewBusPassNewViewController", fromController: self)
        }else if(agency == "MetroToken"){
            uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
        }else if(agency == "metroTrip"){
            uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
        }else if(agency == "Penalty"){
            uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
        }else if(agency == "STORE_VALUE_PASS"){
            uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
        }
    }
    
    @IBAction func ridlrCBTapped(_ sender: AnyObject) {
        
        if(ridlrPoints == 0.0){
            ridlrPointsCB.setCheckState(.unchecked, animated: true)
            uiFun.showAlert(title: "", message: "Ridlr point balance is not enough", logMessage: "", fromController: self)
            return
        }
        
        if(ridlrPoints == nil){
            uiFun.showAlert(title: "", message: "Ridlr point balance is not enough", logMessage: "", fromController: self)
            return
        }
        // Ridlr Point urf Rcash Radio Button Tap Action
        if(ridlrPointsCB.checkState == .checked){
            
            uiFun.createCleverTapEvent(message: "Check Ridlr Points")
            let selectedSegment = paymentTypeSegment.selectedSegmentIndex
            
            switch selectedSegment {
            case 0:
                //Wallet
                updatingUI()
                isRidlrCBChecked = true
                break
            case 1:
                // Card Payment
                cardUpdateUI()
                isRidlrCBChecked = true
                break
            case 2:
                // Net Banking
                cardUpdateUI()
                isRidlrCBChecked = true
                break
            default:
                break
            }
        }else{
            uiFun.createCleverTapEvent(message: "Unchecked Ridlr Points ")
            isRidlrCBChecked    = false
            payableAmount.text  = rechargeAmount
            if(paymentTypeSegment.selectedSegmentIndex < 3){
                // Wallet Segment Selected
                if(paymentTypeSegment.selectedSegmentIndex == 0){
                    showCitrusLayout()
                }
                paymentTypeSegment.selectedSegmentIndex = 0
                
                if(Int(citrusWalletBalance!) >= Int(payableAmount.text!)!){
                    payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
                }
                else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
                
                    let amountToAdd = Float(payableAmount.text!)! - Float(citrusWalletBalance!)
                    print("Amount to add \(amountToAdd)")
                    amountToAddValue = "\(amountToAdd)"
                    payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)
                }
            }
        }
        
    }
    
    
    func UpdateUIForMetro(){
        
        // In Metro There Is No Option like Rcash and Mobikwik Wallet so we have to hide those things for METRO agency
        uiFun.hideIndicatorLoader()
        print("Updating Metro UI")
        
        // Move Payment UI to upwards
        payableAmountLabel.center.y  = 175
        payableAmount.center.y       = 175
      
        cardUIView.center.y          = 360
        paymentTypeSegment.center.y  = 221
        
        citrusView.center.y          = 300
        freechargeView.center.y      = 360
        
        bankLabel.center.y           = 340
        bankPicker.center.y          = 440
        
        mobikwikView.isHidden        = true // Mobikwik Hidden
        
    }
    
    @IBAction func paymentTypeSegmentAction(_ sender: AnyObject) {
        
        let selectedSegment = paymentTypeSegment.selectedSegmentIndex
        
        switch selectedSegment {
        case 0:
            
            // Wallet UI Visible
            showCitrusWallet()
            cardUIView.isHidden     = true
            bankLabel.isHidden      = true
            bankPicker.isHidden     = true
            transcationMode         = "Wallet"
            
            if(isConnectedToInternet){
                updateWalletUI()
            }
            
            // Hide Pay and Proceed if not single wallet configured or availabel
            
            if(!isCitrusConfigured && !isMobikwikConfigured && !isFreechargeConfigured){
                
                if(Int(ridlrPoints!) < Int(rechargeAmount)!){
                    payAndProceed.isHidden = true
                }else{
                    payAndProceed.isHidden = false
                }
                
            }else{
                payAndProceed.isHidden      = false
                citrusRadio.isHidden        = !isCitrusConfigured
                mobikwikRadio.isHidden      = !isMobikwikConfigured
                
                freechargeRadio.isHidden    = !isFreechargeConfigured
            }
            
            let agencyName = defaults.string(forKey: "ticketingAgency")!
            if(agencyName == "Metro"){
                initiator = "Metro Recharge"
                UpdateUIForMetro()
            }else if(agencyName == "MetroToken"){
                mobikwikView.isHidden = true
            }else{
                initiator = "BEST Pass"
            }
            
            uiFun.createCleverTapEvent(message: "tap on wallet")
            break
        case 1:
            // Card UI is visible
            hideCitrusWallet()
            
            payAndProceed.isHidden  = false
            cardUIView.isHidden     = false
            citrusView.isHidden     = true
            bankLabel.isHidden      = true
            bankPicker.isHidden     = true
            transcationMode         = "Card"
            payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            uiFun.createCleverTapEvent(message: "tap on wallet")
            break
        case 2:
            // Net banking UI is visible
            hideCitrusWallet()
            
            payAndProceed.isHidden  = false
            cardUIView.isHidden     = true
            bankLabel.isHidden      = false
            bankPicker.isHidden     = false
            transcationMode         = "NetBanking"
            payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            uiFun.createCleverTapEvent(message: "tap on NB")
            break
        default:
            break
        }
    }
    
    
    @IBAction func payAndProceed(_ sender: AnyObject) {
        
        // User Tap On Proceed To Pay Button
        let selectedSegment = paymentTypeSegment.selectedSegmentIndex
        print("Selected Segment \(selectedSegment)")
        
        switch selectedSegment {
            case 0:
                // Payment Through Wallet
                walletPayment()
                break
            case 1:
                // Payment Through Card
                let isValidCardNumber = caardNumberTF.text?.replacingOccurrences(of: " ", with: "").isValidCardNumber()
                let cardType          = caardNumberTF.text?.replacingOccurrences(of: " ", with: "").cardType()
                if(isValidCardNumber)!{
                    if checkCardExpiry() {
                        print("Selected Card Type is \(cardType)")
                        PaymentAPICall()
                    }else{
                        uiFun.hideIndicatorLoader()
                    }
                }else{
                    uiFun.showAlert(title: "", message: "Invalid Card Number", logMessage: "User Entered Invalid Card Number", fromController: self)
                }
                break
                    
            case 2:
                //Paymnet Through Net banking
                uiFun.showIndicatorLoader()
                PaymentAPICall()
                break
            default:
                break
            }
    }
    
    func checkCardExpiry() -> Bool{
        
        
        let splitExpiry         = monthYearTF.text?.components(separatedBy: "/")
        let month               = splitExpiry?[0]
        let year                = splitExpiry?[1]
        
        if(monthYearTF.text?.contains("//"))!{
            uiFun.showAlert(title: "", message: "Please Enter Valid Expiry Month", logMessage: "Invalid Expiry Month", fromController: self)
            return false
        }else if(year?.characters.count != 4){
            uiFun.showAlert(title: "", message: "Please Enter Valid Expiry Year In YYYY Format", logMessage: "Invalid Expiry Year", fromController: self)
            
            return false
        }else if(month?.characters.count == 1){
            uiFun.showAlert(title: "", message: "Please Enter Valid Expiry Month in MM Format", logMessage: "Invalid Expiry Month", fromController: self)
            return false
        }else{
            return true
        }
        
    }
    
    func walletPayment(){
        
        if(ridlrPointsCB.checkState == .checked){
            
            if(Int(payableAmount.text!)! == 0){
                PaymentAPICall() // This Block Will Executed For BEST, when Whole Payable Amount is deducted from Rcash.
                return
            }
        }
        
        switch selectedWallet {
            case "CITRUSPAY":
                if(Float(Float(payableAmount.text!)! - Float(citrusWalletBalance!)) > 0.0){
                    citrusAddMoney()  // If Citrus Doesnt have payable amount than will add money into citrus
                }else{
                    PaymentAPICall()  // If Citrus have payable amount than will do payment
                }
            break
            case "MOBIKWIK":
                if(Float(Float(payableAmount.text!)! - Float(mobikwikWalletBalance!)) > 0.0){
                    uiFun.showIndicatorLoader()
                    // If Mobikwik Doesnt have payable amount than will add money into mobikwik
                    let mobiWalletAPICall = MobiFreeWalletAPICall()
                    mobiWalletAPICall.makeAPICall(fromViewController: self, amount: amountToAddValue, paymentaGateway: "MOBIKWIK")
                }else{
                    // If Mobikwik have payable amount than will do payment
                    PaymentAPICall()
                }
                break
            case "FREECHARGE":
                uiFun.showIndicatorLoader()
                if(Float(Float(payableAmount.text!)! - Float(freechargeWalletBalance!)) > 0.0){
                    // If Freecharge Doesnt have payable amount than will add money into freecharge
                    let mobiWalletAPICall = MobiFreeWalletAPICall()
                    mobiWalletAPICall.makeAPICall(fromViewController: self, amount: amountToAddValue, paymentaGateway: "FREECHARGE")
                }else{
                    // If Freecharge have payable amount than will do payment
                    PaymentAPICall()
                }
                break
            default:
                break
            }
        }
    
    func cardPayment(){
        // Card Payment API Call
        let cardPaymentAPICall = CardPaymentAPICall()
        cardPaymentAPICall.makeAPICall(fromViewController: self, jsonString: cardPaymentRequestBody()!)
    }
    
    func checkCardNumber(){
        // It wont allow user to enter card number more than 16 digits
        if((caardNumberTF.text?.characters.count)! > 19){
            caardNumberTF.deleteBackward()
        }
        // This check adds the blank space i.e " " after every 4 characters
        if(((caardNumberTF.text?.characters.count)! % 5) == 0 && (caardNumberTF.text?.characters.count)! > 4){
            caardNumberTF.text?.insert(" ", at: (caardNumberTF.text?.index((caardNumberTF.text?.startIndex)!, offsetBy: (caardNumberTF.text?.characters.count)! - 1))!)
        }
        
        
        let creditCardValidator = CreditCardValidator()  // Third Party Lib for validating  debit and credit card
        
        if(creditCardValidator.validate(string: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)){
            
            // THis gives the card type from card number i.e VISA, MASTER etc etc..
            slectedCardType = creditCardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)
            //print("Selected Credit Card Validator \(slectedCardType.name)")
            
            // Setting respective card image into text field
            switch slectedCardType.name {
            case "Amex":
                cardImage.image = UIImage(named: "ic_card_amex")
                break
            case "Visa":
                cardImage.image = UIImage(named: "ic_card_visa")
                break
            case "MasterCard":
                cardImage.image = UIImage(named: "ic_card_master")
                break
            case "Discover":
                cardImage.image = UIImage(named: "ic_card_discover")
                break
            case "DinersClub":
                cardImage.image = UIImage(named: "ic_card_dinners")
                break
            case "JCB":
                cardImage.image = UIImage(named: "ic_card_jcb")
                break
            case "Maestro":
                cardImage.image = UIImage(named: "ic_card_maestro")
                break;
            default:
                break
            }
        }
    }
    
    func checkYearMonth(){
        
        var currentLength = monthYearTF.text?.characters.count
        
        
        //It wont allow user to enter month year more than 7 digits
        if((monthYearTF.text?.characters.count)! > 7){
            monthYearTF.deleteBackward()
        }
        
        // Adds "/" after entering month in card payment
        if(monthYearTF.text?.characters.count == 2){
            monthYearTF.text?.append("/")
        }
        
    }
    
    func validateCardNumber(cardNumber : String) -> Bool{
        
        // Validate the card number fromm its digits
        print("Selected Card \(cardNumber)")
        if(slectedCardType.name == "Amex"){
            if(cardNumber.characters.count == 19){
                return true
            }else{return false}
        }else{
            if(cardNumber.characters.count == 16){
                return true
            }else{return false}
        }
    }
    
    func validateCVV(cvv : String) -> Bool{
        
        // Validatinf CVV number of card
        if(cvv.isEmpty){
            return false
        }
        
        if(slectedCardType.name == "Amex"){
            if(cvv.characters.count == 4){
                return true
            }else{
                return false
            }
        }else{
            
            if(cvv.characters.count == 3){
                return true
            }else{
                return false
            }
        }
    }
    
    func validateExpMonth(expMonth : String, expYear : String) -> Bool{
        
        // Validate the exp month and year of card
        if(expMonth.isEmpty){
            return false
        }
        
        if(Int(expMonth)! > 1 && Int(expMonth)! <= 12){
            if(expMonth.isEmpty || expMonth.isEmpty){
                return false
            }
            if(Int(expYear)! >= year){
                if(Int(expMonth)! >= month){
                    return true
                }else{return false}
            }else{return false}
        }else{return false}
    }
    
    
    func wholeCardValidator(cardNumber : String, expYearMonth : String, cvv : String) -> Bool{
        
        // Show error correspoding to wrong input data
        var expMonthValidate = ""
        var expYearValidate  = ""
        
        if(!expYearMonth.isEmpty){
             expMonthValidate                   = expYearMonth.components(separatedBy: "/")[0]
             expYearValidate                    = expYearMonth.components(separatedBy: "/")[1]
        }
        
        print("")
        let cardValidatorResp       = validateCardNumber(cardNumber: cardNumber.replacingOccurrences(of: " ", with: ""))
        let expDateValidatorResp    = validateExpMonth(expMonth: expMonthValidate, expYear: expYearValidate)
        let cvvValidator            = validateCVV(cvv: cvv)
        
        if(!cardValidatorResp){
            uiFun.showAlert(title: "", message: "Invalid Card Number", logMessage: "Invlaid Card Number", fromController: self)
        }
        if(!expDateValidatorResp){
            uiFun.showAlert(title: "", message: "Invalid Expiry Date", logMessage: "Invalid Exp Date", fromController: self)
        }
        if(!cvvValidator){
            uiFun.showAlert(title: "", message: "Invalid CVV", logMessage: "Invalid CVV", fromController: self)
        }
        
        if(cardValidatorResp && expDateValidatorResp && cvvValidator){
            return true
        }else{
            return false
        }
    }

    
    func cardPaymentRequestBody() -> String?{
        
        // Creating json string for Card Payment
        let bookingItemListArray:NSMutableArray      = NSMutableArray()
        
        
        let bookingPaymentDetailArray:NSMutableArray = NSMutableArray()

        let itemArray:NSMutableArray                 = NSMutableArray()
        let itemDic: NSMutableDictionary             = NSMutableDictionary()
        
        let pricingDetailArray : NSMutableArray      = NSMutableArray()
        let pricingDetailList : NSMutableDictionary  = NSMutableDictionary()
        
        let routeDetailsArray : NSMutableArray       = NSMutableArray()
        
        itemDic.setValue("2017-01-16", forKey: "activationDate")
        itemDic.setValue(metroCardNumber, forKey: "companyIdentifier")
        itemDic.setValue("RECHARGE", forKey: "itemAction")
        itemDic.setValue(Int(rechargeAmount), forKey: "itemAmount")
        itemDic.setValue("SMART_CARD", forKey: "itemType")
        itemDic.setValue(0, forKey: "validityInDays")
        
        pricingDetailList.setValue(Int(rechargeAmount), forKey: "baseAmount")
        pricingDetailList.setValue(Int(rechargeAmount), forKey: "totalAmount")
        pricingDetailArray.add(pricingDetailList)
        
        itemDic.setValue(pricingDetailList, forKey: "pricingDetails")
        itemDic.setValue(routeDetailsArray, forKey: "routeDetails")
        
        itemArray.add(itemDic)
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue("RECHARGE", forKey: "action")
        para1.setValue("\(rechargeAmount).0", forKey: "amount")
        para1.setValue("RELIANCE_METRO", forKey: "company")
        para1.setValue("SMART_CARD", forKey: "itemType")
        para1.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
        para1.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
        para1.setObject(itemArray, forKey: "items" as NSCopying)
        para1.setValue(pricingDetailList, forKey: "pricingDetails")
        
        
        let paymentDetailsArray:NSMutableArray       = NSMutableArray()
        let paymentDetailsList1: NSMutableDictionary = NSMutableDictionary()
        let paymentDetailsList2: NSMutableDictionary = NSMutableDictionary()
 
        if(ridlrPointsCB.checkState == .checked){
            
            let passAmount = Int(rechargeAmount)
            var remainigAmount = 0
            
            if(passAmount! > Int(ridlrPoints!)){
                
                remainigAmount = passAmount! - Int(ridlrPoints!)
                paymentDetailsList2.setValue("\(Int(ridlrPoints!)).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                
                paymentDetailsArray.add(paymentDetailsList2)
                
                if(remainigAmount <= Int(citrusWalletBalance!)){
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList1)
                }
                else{
                    
                    payAndProceed.setTitle("ADD RS. \(remainigAmount - Int(citrusWalletBalance!)) AND PAY", for: .normal)
                }
                
            }else{
                
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                
                paymentDetailsArray.add(paymentDetailsList2)
            }
            
            
        }
        let bookingDetailArray : NSMutableArray = NSMutableArray()
        bookingDetailArray.add(para1)
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(para1, forKey: "bookingDetails")
        para.setValue(paymentDetailsArray, forKey: "paymentDetails")
       
        let jsonData: NSData
        do{
            jsonData        = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString  = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string metro json request body = \(jsonString)") // JSON String for card payment request
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

    @IBAction func ridlrCashTapped(_ sender: AnyObject) {
        
        ridlrPointsCB.enableMorphing = true
        print("Ridlr cash checkbox \(ridlrPointsCB.checkedValue)")
        
        if(ridlrPointsCB.isSelected){
            
            print("Ridlr cash selected")
        }
        
        let agencyName              = uiFun.readData(key: "ticketingAgency")
        if(agencyName.contains("Metro")){
            mobikwikView.isHidden   = true
        }

    }
    
    func PaymentAPICall(){
        uiFun.showIndicatorLoader()
        
        let defaults    = UserDefaults.standard
        let agencyName  = defaults.string(forKey: "ticketingAgency")!
        
        var url : NSURL!
        if(agencyName == "MetroToken" || agencyName == "metroTrip" || agencyName == "Penalty" || agencyName == "STORE_VALUE_PASS") {
            url = NSURL(string: "\(constant.STAGING_BASE_URL)/payment") //url = NSURL(string: "\(constant.STAGING_BASE_URL)/payment")!//// payment option api call for metro token
        }else{
            url = NSURL(string: "\(constant.STAGING_BASE_URL)/payment")! // Payment Option API call
        }
        print("Paymnet option api call url \(url)")
        
        // Fetching Metro Card Number or BEST Pass Number by making sure that initiator of this payment is not metro token
        if(agencyName == "Best" || agencyName == "Metro"){
            let PassCardNumber = "Pass Number :\(defaults.string(forKey: "bestPassNumber")!)"
            print("BEST Recharge Pass Number \(PassCardNumber)!")
        }
        
        let ridlrToken = defaults.string(forKey: "ridlrToken")!         // Fetching rildr token
           // Ferching Ticketing Agency i.e Either Metro or BEST
        print("Ridlr Token Add Money \(ridlrToken)")
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"         // POST Request
        
        // Headers for API call
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(ridlrToken, forHTTPHeaderField: "ridlr-token")
        request.setValue("A*S)(CR@!T^ST%%C**!l@N#T$I", forHTTPHeaderField: "apk_key")
        request.setValue("1479196020161", forHTTPHeaderField: "X-SESSION-ID")
        request.setValue(constant.app_version_name, forHTTPHeaderField: "app_version_name")
        request.setValue("iOS", forHTTPHeaderField: "Platform")
        
        switch agencyName {
        case "Best":
            
            if(Int(rcashLabel.text!)! <= 0 && ridlrPointsCB.checkState == .checked){
                // If user doesnt have enough Rcash
                uiFun.showAlert(title: "", message: "Insufficient Ridlr Points", logMessage: "Insufficient Ridlr Points", fromController: self)
                return
            }
            
            let paymentJSONString   = paymentRequestBodyBEST()    // JSON String for BEST Pass payment
            request.httpBody        = paymentJSONString?.data(using: String.Encoding.utf8, allowLossyConversion: true) // API call Body
            print("BODY REQUEST" + String(describing: request.httpBody))
            break
            
        case "Metro":
            
            let metroRachargeJSONString = paymentRequestBody()      // JSON String for Metro Card Payment
            request.httpBody            = metroRachargeJSONString?.data(using: String.Encoding.utf8, allowLossyConversion: true) // API call Body
            print("BODY REQUEST" + String(describing: request.httpBody))
            
            uiFun.createCleverTapEvent(message: "User tap on Pay Now")
            break
            
        case "MetroToken":
            
            if(Int(rcashLabel.text!)! <= 0 && ridlrPointsCB.checkState == .checked){
                // If user doesnt have enough Rcash
                uiFun.showAlert(title: "", message: "Insufficient Ridlr Points", logMessage: "Insufficient Ridlr Points", fromController: self)
                return
            }
            
            let getMetroTokenJsonBody   = MetroTokenJSONBody()  // JSON String for Metro Token
            request.httpBody    = getMetroTokenJsonBody.data(using: String.Encoding.utf8, allowLossyConversion: true) // API call Body
            print("TOKEN BODY REQUEST" + String(describing: request.httpBody))
            break
            
        case "metroTrip":
            
            if(Int(rcashLabel.text!)! <= 0 && ridlrPointsCB.checkState == .checked){
                // If user doesnt have enough Rcash
                uiFun.showAlert(title: "", message: "Insufficient Ridlr Points", logMessage: "Insufficient Ridlr Points", fromController: self)
                return
            }
            
            let getMetroTripJsonBody = metroTripJSON(itemAction: "RENEW", itemSubType: "TRIP_PASS")  // JSON String for Metro Token
            request.httpBody        = getMetroTripJsonBody.data(using: String.Encoding.utf8, allowLossyConversion: true) // API call Body
            print("TRIP JSON BODY REQUEST" + String(describing: request.httpBody))
            break
        
        case "Penalty":
            
            if(Int(rcashLabel.text!)! <= 0 && ridlrPointsCB.checkState == .checked){
                // If user doesnt have enough Rcash
                uiFun.showAlert(title: "", message: "Insufficient Ridlr Points", logMessage: "Insufficient Ridlr Points", fromController: self)
                return
            }
            
            let getMetroTripJsonBody = MetroPenaltyJSON()  // JSON String for Metro Token
            request.httpBody         = getMetroTripJsonBody.data(using: String.Encoding.utf8, allowLossyConversion: true) // API call Body
            print("TRIP JSON BODY REQUEST" + String(describing: request.httpBody))
            break
        case "STORE_VALUE_PASS":
            
            if(Int(rcashLabel.text!)! <= 0 && ridlrPointsCB.checkState == .checked){
                // If user doesnt have enough Rcash
                uiFun.showAlert(title: "", message: "Insufficient Ridlr Points", logMessage: "Insufficient Ridlr Points", fromController: self)
                return
            }
            
            let getMetroStoreValueJsonBody  = metroTripJSON(itemAction: "RECHARGE", itemSubType: "STORE_VALUE_PASS")  // JSON String for Metro Token
            request.httpBody                = getMetroStoreValueJsonBody.data(using: String.Encoding.utf8, allowLossyConversion: true) // API call Body
            print("TRIP JSON BODY REQUEST" + String(describing: request.httpBody))
            break
            
        default:
            break
        }
        
        // Disable caching
        let config                = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        config.urlCache           = nil
        
        // Making api call now
        task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
            if error != nil{
                self.uiFun.hideIndicatorLoader()
                print("Certificate Error -> \(error!)")
                // if api call is unsuccessful than show alert depending on the error
                if(error.debugDescription.contains("timed out")){
                    self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Payment API Error", fromController: self)
                }else if(error.debugDescription.contains("connection was lost")){
                    self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Payment API Error", fromController: self)
                }else if(error.debugDescription.contains("appears to be offline")){
                    self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Payment API Error", fromController: self)
                }else if(error.debugDescription.contains("not connect to")){
                    self.uiFun.showAlert(title: "", message: "Could not connect to the server", logMessage: "Payment API Error", fromController: self)
                }

                
                return
            }else{
                
                if let urlContent = data{
                    
                    do {
                        
                        // API Response
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        self.loader.stopAnimating()
                        
                        let jsonRechargeResponse = JSON(jsonResult)
                        print("Payment API Call Response \(jsonRechargeResponse)")
                        
                        if(jsonRechargeResponse["code"] == 5000){
                            
                            // JSON Resonse Success
                            self.uiFun.createCleverTapEvent(message: "BEST Pass Renewal Response Success")
                            self.processPaymentResult(jsonResult: jsonRechargeResponse)
                        
                        }else{
                            // JSON Response Fail
                            self.uiFun.hideIndicatorLoader()
                            let jsonError = jsonRechargeResponse["error"]   // Fetching JSON Error
                            
                            if(jsonError["errorCodeText"] == "" || jsonError["errorCodeText"] == "null"){
                                // If errorCodeText is not present parse the response to "showExceptionDialog"
                                self.uiFun.showExceptionDialog(jsonResponse: jsonRechargeResponse, fromViewController: self)
                            
                            }else{
                                // Show alert if there is an error in json resposne
                                self.uiFun.showAlert(title: "Error", message: String(describing: jsonError["errorCodeText"]), logMessage: "Recharge Failed", fromController: self)
                                self.uiFun.createCleverTapEvent(message: "BEST Pass Renewal Response failed \(jsonError["errorCodeText"])")
                                print("Recharge Failed Please Try Again")
                            }
                        }
                        
                    }
                    catch{
                        // Unable to convert response into json format
                        print("Failed To Convert JSON")
                        self.uiFun.hideIndicatorLoader()
                        self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "Failed TO Convert JSON", fromController: self)
                        
                    }
                }
            }
        }
        
        task.resume()
        
    }
    
    func processPaymentResult(jsonResult : JSON){
        
        // Processing payment response depending on the payment type i.e Wallet, Card and Net banking
        switch transcationMode {
        case "Wallet":
            print("Processing Wallet Payment Response")
            walletResonse(jsonRechargeResponse: jsonResult)
            break
        case "Card":
            cardPaymentResponse(jsonRechargeResponse: jsonResult)
            break
        case "NetBanking":
            netBankingResponse(jsonRechargeResponse: jsonResult)
            break
        default:
            
            break
        }
        
    }
    
    @IBAction func citrusRadioActionEvent(_ sender: Any) {
        
        mobikwikRadio.isSelected    = false
        freechargeRadio.isSelected  = false
        citrusRadio.isSelected      = true
        selectedWallet              = "CITRUSPAY"
    }
    
    @IBAction func freechargeRadioActionEvent(_ sender: Any) {
        
        mobikwikRadio.isSelected    = false
        freechargeRadio.isSelected  = true
        citrusRadio.isSelected      = false
        selectedWallet              = "FREECHARGE"
    }
    
    @IBAction func mobikwikRadioActionEvent(_ sender: Any) {
        
        mobikwikRadio.isSelected    = true
        freechargeRadio.isSelected  = false
        citrusRadio.isSelected      = false
        selectedWallet              = "MOBIKWIK"
    }
    
    
    
    func walletResonse(jsonRechargeResponse : JSON){
        
        print("Renew Pass Option Response \(jsonRechargeResponse)")
        print("Recharge Successful")
        // Processing wallet json response
        let passRechargeData        = jsonRechargeResponse["data"]
        let passBookingDetail       = passRechargeData["bookingDetails"]
        
        processWalletResponse(jsonWalletResponse: jsonRechargeResponse) // Storing response into the model
        
        passRechargeBookingId       = String(describing: passBookingDetail["bookingId"])
        
        let passRechargePayment     = passRechargeData["paymentDetails"]
        passRechargePaymentDetail   = String(describing: passRechargePayment["paymentGateway"])
        
        let defaults        = UserDefaults.standard
        let agencyName      = defaults.string(forKey: "ticketingAgency")!
        
        if(agencyName == "Best"){
            self.uiFun.navigateToScreem(identifierName: "BESTPassRechargeSuccessViewController", fromController: self)
        }else if(agencyName == "Metro"){
            self.uiFun.navigateToScreem(identifierName: "MetroRechargeSuccessViewController", fromController: self)
        }else if(agencyName == "MetroToken"){
            uiFun.hideIndicatorLoader()
            self.uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
        }else if(agencyName == "Penalty"){
            uiFun.hideIndicatorLoader()
            self.uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
        }else if(agencyName == "metroTrip"){
            uiFun.hideIndicatorLoader()
            self.uiFun.navigateToScreem(identifierName: "TripPassReceiptViewController", fromController: self)
        }else if(agencyName == "STORE_VALUE_PASS"){
            uiFun.hideIndicatorLoader()
            self.uiFun.navigateToScreem(identifierName: "StoreValueReceiptViewController", fromController: self)
        }
    }
    
    
    func processWalletResponse(jsonWalletResponse : JSON){
        
        paymentResponseListWebview.removeAll()
        print("Wallet JSON \(jsonWalletResponse)")
        let agencyName              = uiFun.readData(key: "ticketingAgency")
        
        let data                    = jsonWalletResponse["data"]
        let paymentDetailJSON       = data["paymentDetails"]
        let bookingDetailJSON       = data["bookingDetails"]
        
        
        let walletModel             = PaymentReesponseModel()
        
        walletModel.status          = String(describing : jsonWalletResponse["status"])
        print("Wallet Status \(String(describing : paymentDetailJSON["status"]))")
        
        let walletData              = PaymentDataWebView()
        walletData.order_no         = String(describing : data["orderNo"])
        print("Wallet Order No \(String(describing : data["orderNo"]))")
        
        let walletDetail            = PaymentDetailsResponseWebView()
    
        let bookingPaymentDetailsList = bookingDetailJSON.arrayValue[0]["bookingPaymentDetailsList"]
        
        
        walletDetail.paymentGateway  = String(describing : bookingDetailJSON["paymentGateway"]) // Wallet Name i.e Citrus, Mobi, Freecharge
        walletDetail.paymentMode     = String(describing: paymentDetailJSON["paymentMode"])
        walletDetail.bookingId       = String(describing : data["orderNo"])
        
       
        
        if(agencyName == "Metro"){
            // Card Number
            let bookingItemJSON         = bookingDetailJSON.arrayValue[0]["bookingItemsList"]
            walletDetail.cardNumber     = String(describing : bookingItemJSON.arrayValue[0]["companyItemId"])
            walletDetail.amount         = String(describing : paymentDetailJSON["amount"]) // Recharge Amount
        }
        
        if(agencyName == "MetroToken"){
            
            uiFun.writeData(value: jsonWalletResponse.rawString()!, key: "tokenJSONResponse")
            
            let processingMetroJSONResponse = MetroJSONResponses()
            metroTokenResposneList          = processingMetroJSONResponse.processMetroJSONResponse(response: jsonWalletResponse)
            
        }
        
        if(agencyName == "metroTrip" || agencyName == "STORE_VALUE_PASS"){
            
            uiFun.writeData(value: jsonWalletResponse.rawString()!, key: "metroTripResponse")
            
            DBManager.shared.createDatabase()
            DBManagerToken.shared.createDatabase()
            
            let processMetroTrip = ProcessTripPassResponse()
            processMetroTrip.processTripResponse(respnse: jsonWalletResponse)
            
        }
        
        if(agencyName == "Penalty"){
        
            let processingMetroJSONResponse = MetroJSONResponses()
            metroTokenResposneList          = processingMetroJSONResponse.processMetroJSONResponse(response: jsonWalletResponse)
        }
        
        if(agencyName == "Best"){
        
            let paymentDetailJSON       =  bookingDetailJSON.arrayValue[0]["bookingPaymentDetailsList"]
        
            if(paymentDetailJSON.count == 1){
                paidVia             = "Paid via \(String(describing: paymentDetailJSON.arrayValue[0]["paymentGateway"]))" // Wallet Type
            }else if(paymentDetailJSON.count == 2){
                paidVia             = "Paid via \(String(describing: paymentDetailJSON.arrayValue[0]["paymentGateway"])) & \(String(describing: paymentDetailJSON.arrayValue[1]["paymentGateway"]))" // Wallet Type
            }

            
            var i = 0
            while(i < paymentDetailJSON.count){
          
               // walletDetail.amount         = String(describing:paymentDetailJSON["totalAmount"])
                walletDetail.paymentGateway = String(describing: paymentDetailJSON.arrayValue[i]["paymentGateway"])
               // walletDetail.paymentMode    = String(describing: paymentDetailJSON["paymentMode"])
                
                i += 1
                
            }
            let itemJSON                = bookingDetailJSON.arrayValue[0]["bookingItemsList"]
            walletDetail.cardNumber     = String(describing : itemJSON.arrayValue[0]["companyItemId"])
            walletDetail.amount         = String(describing: itemJSON.arrayValue[0]["totalAmount"])
            
        }
        
        print("Wallet REcharge Amount \(String(describing : paymentDetailJSON["amount"]))")
        print("Paid Via Wallet \(String(describing : paymentDetailJSON["paymentMode"]))")
        //print("Wallet Card Number \(String(describing : bookingDetailJSON["items"].arrayValue[0]["companyIdentifier"]))")
        
        walletData.paymentDetails.append(walletDetail)
        walletModel.data.append(walletData)
        paymentResponseListWebview.append(walletModel)
    }
    
    func cardPaymentResponse(jsonRechargeResponse : JSON){
        
        // Processing card payment json response
        processPaymentResponseToModel(jsonRechargeResponse: jsonRechargeResponse)
    }
    
    func netBankingResponse(jsonRechargeResponse : JSON){
    
        // Processign net banking payment response
        let jsonData        = jsonRechargeResponse["data"]
        let paymentDetails  = jsonData["paymentDetails"]
        
        redirectUrl         = String(describing : paymentDetails["redirectUrl"])
        
        uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
        uiFun.createCleverTapEvent(message: "Payment response \(initiator) Success")
        
    }
    
    func processPaymentResponseToModel(jsonRechargeResponse : JSON){
        // Storing payment respone into the respective models
        paymentResponseList.removeAll()                 // Removinf previous data from the list
        let paymentRechargeModel = CardPaymentModel()   // Intializing the model
        
        let jsonData = jsonRechargeResponse["data"]     // Fetching data from json response
        print("Credir Card Payment Response \(jsonRechargeResponse)")
        
        let cardPaymentData             = CardPaymentData()
        cardPaymentData.order_no        = String(describing: jsonData["order_no"])
        cardPaymentData.isPaymentFinal  = String(describing: jsonData["isPaymentFinal"])
        
        let paymentDetails              = jsonData["paymentDetails"]
        
        let paymentDetail               = PaymentDetails()
        
        paymentDetail.amount            = String(describing: paymentDetails["amount"])
        paymentDetail.paymentGateway    = String(describing: paymentDetails["paymentGateway"])
        paymentDetail.notifyUrl         = String(describing: paymentDetails["notifyUrl"])
        paymentDetail.paymentMode       = String(describing: paymentDetails["paymentMode"])
        paymentDetail.responseType      = String(describing: paymentDetails["responseType"])
        paymentDetail.message           = String(describing: paymentDetails["message"])
        paymentDetail.merchantAccessKey = String(describing: paymentDetails["merchantAccessKey"])
        paymentDetail.returnUrl         = String(describing: paymentDetails["returnUrl"])
        paymentDetail.signature         = String(describing: paymentDetails["signature"])
        paymentDetail.transactionId     = String(describing: paymentDetails["transactionId"])
        
        if(jsonData["isPaymentFinal"]).boolValue{
        
        let bookingDetails = jsonData["bookingDetails"]
        
        let bookingDetail = BookingDetails()
        
        bookingDetail.unpaidAmount         = String(describing: bookingDetails["unpaidAmount"])
        bookingDetail.bookingDateTime      = String(describing: bookingDetails["bookingDateTime"])
        bookingDetail.deviceId             = String(describing: bookingDetails["deviceId"])
        bookingDetail.company              = String(describing: bookingDetails["company"])
        bookingDetail.bookingId            = String(describing: bookingDetails["bookingId"])
        bookingDetail.paymentStatus        = String(describing: bookingDetails["paymentStatus"])
        bookingDetail.action               = String(describing: bookingDetails["action"])
        bookingDetail.companyReferenceId   = String(describing: bookingDetails["companyReferenceId"])
        bookingDetail.totalTax             = String(describing: bookingDetails["totalTax"])
        bookingDetail.paymentStatusMessage = String(describing: bookingDetails["paymentStatusMessage"])
        bookingDetail.bookingTicketValidationList = String(describing: bookingDetails["bookingTicketValidationList"])
        bookingDetail.companyStatusMessage = String(describing: bookingDetails["companyStatusMessage"])
        bookingDetail.totalAmount          = String(describing: bookingDetails["totalAmount"])
        bookingDetail.totalDiscount        = String(describing: bookingDetails["totalDiscount"])
        bookingDetail.baseAmount           = String(describing: bookingDetails["baseAmount"])
        bookingDetail.companyStatus        = String(describing: bookingDetails["companyStatus"])
        bookingDetail.totalSurcharge       = String(describing: bookingDetails["totalSurcharge"])
        
        let bookingItemsList = bookingDetails["bookingItemsList"].arrayValue[0]
        
        let bookingItemsListModel = BookingItemsList()
        
        bookingItemsListModel.itemType              = String(describing: bookingItemsList["itemType"])
        bookingItemsListModel.companyItemId         = String(describing: bookingItemsList["companyItemId"])
        bookingItemsListModel.bookingItemId         = String(describing: bookingItemsList["bookingItemId"])
        bookingItemsListModel.issuerIdentifier      = String(describing: bookingItemsList["issuerIdentifier"])
        bookingItemsListModel.itemSubType           = String(describing: bookingItemsList["itemSubType"])
        bookingItemsListModel.bookingItemRoutesList = String(describing: bookingItemsList["bookingItemRoutesList"])
        bookingItemsListModel.userType              = String(describing: bookingItemsList["userType"])
        bookingItemsListModel.userDob               = String(describing: bookingItemsList["userDob"])
        bookingItemsListModel.activationDate        = String(describing: bookingItemsList["activationDate"])
        bookingItemsListModel.validityCriteria      = String(describing: bookingItemsList["validityCriteria"])
        bookingItemsListModel.totalTax              = String(describing: bookingItemsList["totalTax"])
        bookingItemsListModel.totalConcession       = String(describing: bookingItemsList["totalConcession"])
        bookingItemsListModel.validity              = String(describing: bookingItemsList["validity"])
        bookingItemsListModel.totalAmount           = String(describing: bookingItemsList["totalAmount"])
        bookingItemsListModel.userName              = String(describing: bookingItemsList["userName"])
        bookingItemsListModel.expiryDate            = String(describing: bookingItemsList["expiryDate"])
        bookingItemsListModel.totalDiscount         = String(describing: bookingItemsList["totalDiscount"])
        bookingItemsListModel.baseAmount            = String(describing: bookingItemsList["baseAmount"])
        bookingItemsListModel.totalSurcharge        = String(describing: bookingItemsList["totalSurcharge"])
        bookingItemsListModel.issuerTag             = String(describing: bookingItemsList["issuerTag"])
        uiFun.createCleverTapEvent(message: "BEST Pass Renewal Response Expiry Date \(String(describing: bookingItemsList["expiryDate"]))")
        
        let bookingPaymentDetailsList = bookingDetails["bookingPaymentDetailsList"].arrayValue[0]
        
        let paymentDetailList = BookingPaymentDetailsList()
        
        paymentDetailList.cardType             = String(describing: bookingPaymentDetailsList["cardType"])
        paymentDetailList.paymentGateway       = String(describing: bookingPaymentDetailsList["paymentGateway"])
        paymentDetailList.bankCode             = String(describing: bookingPaymentDetailsList["bankCode"])
        paymentDetailList.cardToken            = String(describing: bookingPaymentDetailsList["cardToken"])
        paymentDetailList.maskedCardNumber     = String(describing: bookingPaymentDetailsList["maskedCardNumber"])
        paymentDetailList.bookingPaymentId     = String(describing: bookingPaymentDetailsList["bookingPaymentId"])
        paymentDetailList.paymentMethod        = String(describing: bookingPaymentDetailsList["paymentMethod"])
        paymentDetailList.paymentStatus        = String(describing: bookingPaymentDetailsList["paymentStatus"])
        paymentDetailList.paymentStatusMessage = String(describing: bookingPaymentDetailsList["paymentStatusMessage"])
        paymentDetailList.paymentAmount        = String(describing: bookingPaymentDetailsList["paymentAmount"])
        paymentDetailList.transactionRefId     = String(describing: bookingPaymentDetailsList["transactionRefId"])
            
        paymentDetail.paymentMode              = String(describing: bookingPaymentDetailsList["paymentMethod"])
        paymentDetail.paymentGateway           = String(describing: bookingPaymentDetailsList["paymentGateway"])
        paidVia         = "\(String(describing: bookingPaymentDetailsList["paymentMethod"]))"
        
        let clientBooking = bookingDetails["clientBooking"]
        
        let clientBookingModel = ClientBooking()
        
        clientBookingModel.email       = String(describing: clientBooking["email"])
        clientBookingModel.mobile      = String(describing: clientBooking["mobile"])
        clientBookingModel.clientRefId = String(describing: clientBooking["clientRefId"])
            
        bookingDetail.clientBooking.append(clientBookingModel)
        bookingDetail.bookingItemsList.append(bookingItemsListModel)
        bookingDetail.bookingPaymentDetailsList.append(paymentDetailList)
        
        cardPaymentData.bookingDetails.append(bookingDetail)
        
        }
        
        
        cardPaymentData.paymentDetails.append(paymentDetail)
        
        paymentRechargeModel.cardData.append(cardPaymentData)
        
        paymentResponseList.append(paymentRechargeModel) // Payment Response List
        
        citrusPaymentGateway() // After successfully calling payment api call another api for citrus payment gateway webview
        
    }
    
    func citrusPaymentGateway(){
        
        do {
            
            uiFun.showIndicatorLoader()
            
            // Creating json string for citrus payment gateway
            if let jsonString       = citrusPGJSONString(){
                print("CITRUS PG JSON BODY REQUEST \(jsonString)")
                
                // create post request
                let url             = NSURL(string: "\(constant.CITRUS_PG)")!
                print("citrus PG Gateway url \(url)")
                
                let defaults        = UserDefaults.standard
                let ridlrToken      = defaults.string(forKey: "ridlrToken") // Fetching ridlr token
                print("ridlr token citrus add money \(ridlrToken!)")
                
                // Creating post request
                let request         = NSMutableURLRequest(url: url as URL)
                request.httpMethod  = "POST"
                
                // Setting api json body
                request.httpBody    = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true)
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                print("BODY REQUEST" + String(describing: request.httpBody))
                
                // Making an api call
                let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                    if error != nil{
                        print("Certificate Error -> \(error!)")
                        self.uiFun.hideIndicatorLoader()
                        if(error.debugDescription.contains("timed out")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("connection was lost")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("appears to be offline")){
                            self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                        }
                        return
                    }else{
                        
                        if let urlContent = data{
                            
                            do {
                                // API Response
                                let jsonResult      = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                                
                                print("Citrus payment gateway api response \(jsonResult)")
                            
                                let jsonResponse    = JSON(jsonResult)
                                
                                redirectUrl         = String(describing: jsonResponse["redirectUrl"])
                                if(redirectUrl != ""){
                                    // Got the redirect URL which we will open in webview to complete transaction
                                    print("Citrus PG Redirect URL \(jsonResponse["redirectUrl"])")
                                    self.uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
                                    self.uiFun.createCleverTapEvent(message: "Payment response \(self.initiator) Success")
                                }else{
                                    self.uiFun.hideIndicatorLoader()
                                    self.uiFun.showAlert(title: "", message: String(describing: jsonResponse["txMsg"]), logMessage: "Citrus error", fromController: self)
                                    self.uiFun.createCleverTapEvent(message: "Payment response \(self.initiator) failed \(jsonResponse)")
                                }
                            }
                            catch{
                                print("Failed To Convert JSON")
                                self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "citrusPaymentGateway()", fromController: self)
                            }
                        }
                    }
                }
                
                task.resume()
                
            }
        }
    }
    
    func citrusPGJSONString() -> String?{
        
        
        // Creating json string for citrus payment gateway
        var selectedCardType = ""
        let cardValidator = CreditCardValidator()
        let cardType      = cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)
        
        if(whichCardSelected == "CREDIT_CARD"){
            selectedCardType = "credit"
        }else{
            selectedCardType = "debit"
        }
        let amount:NSMutableDictionary = NSMutableDictionary()
        amount.setValue("INR", forKey: "currency")
        amount.setValue("\(payableAmount.text!).0", forKey: "value") //\(amountToAddLabel.text).0
        
        let paymentMode:NSMutableDictionary = NSMutableDictionary()
        paymentMode.setValue(cvvTF.text, forKey: "cvv")
        paymentMode.setValue(monthYearTF.text, forKey: "expiry")
        paymentMode.setValue("Baahubali", forKey: "holder")
        paymentMode.setValue(caardNumberTF.text?.replacingOccurrences(of: " ", with: ""), forKey: "number")
        paymentMode.setValue(cardType?.name, forKey: "scheme")
        paymentMode.setValue(selectedCardType, forKey: "type")
        
        let paymentToken:NSMutableDictionary = NSMutableDictionary()
        paymentToken.setValue(paymentMode, forKey: "paymentMode")
        paymentToken.setValue("paymentOptionToken", forKey: "type")
        
        let address:NSMutableDictionary = NSMutableDictionary()
        address.setValue("Mumbai", forKey: "city")
        address.setValue("India", forKey: "country")
        address.setValue("Maharashtra", forKey: "state")
        address.setValue("streetone", forKey: "street1")
        address.setValue("streettwo", forKey: "street2")
        address.setValue("400052", forKey: "zip")
        
        let userDetail:NSMutableDictionary = NSMutableDictionary()
        userDetail.setValue(address, forKey: "address")
        userDetail.setValue(uiFun.readData(key: "userEmailId"), forKey: "email")
        userDetail.setValue("Tester", forKey: "firstName")
        userDetail.setValue("Citrus", forKey: "lastName")
        userDetail.setValue(uiFun.readData(key: "userMobileNo"), forKey: "mobileNo")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(amount, forKey: "amount")
        para.setValue(paymentResponseList[0].cardData[0].paymentDetails[0].merchantAccessKey, forKey: "merchantAccessKey")
        para.setValue(paymentResponseList[0].cardData[0].paymentDetails[0].transactionId, forKey: "merchantTxnId")
        para.setValue(paymentResponseList[0].cardData[0].paymentDetails[0].notifyUrl, forKey: "notifyUrl")
        para.setValue(paymentToken, forKey: "paymentToken")
        para.setValue("MSDKG", forKey: "requestOrigin")
        para.setValue(paymentResponseList[0].cardData[0].paymentDetails[0].signature, forKey: "requestSignature")
        para.setValue(paymentResponseList[0].cardData[0].paymentDetails[0].returnUrl, forKey: "returnUrl")
        para.setValue(userDetail, forKey: "userDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("citrus add money json string = \(jsonString)") // json string for citrus payment gateway
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    
    func paymentRequestBody() -> String?{
        
        // Metro JSON String
        let date            = Date()
        let calendar        = Calendar.current
        let components      = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year            =  components.year
        let month           = components.month
        let day             = components.day
        
        let dateMonthYear   = "\(day!)-\(month!)-\(year!)"
        
        let bookingItemListArray:NSMutableArray         = NSMutableArray()

        let bookingPaymentDetailArray:NSMutableArray    = NSMutableArray()
       
        let itemArray:NSMutableArray                    = NSMutableArray()
        let itemDic: NSMutableDictionary                = NSMutableDictionary()
        
        let pricingDetailArray : NSMutableArray         = NSMutableArray()
        let pricingDetailList : NSMutableDictionary     = NSMutableDictionary()
        
        let routeDetailsArray : NSMutableArray = NSMutableArray()
        
        //itemDic.setValue("DURATION", forKey: "activationCriteria")
        itemDic.setValue(dateMonthYear, forKey: "activationDate")
        itemDic.setValue(metroCardNumber, forKey: "companyIdentifier")
        itemDic.setValue("RECHARGE", forKey: "itemAction")
        itemDic.setValue(Int(rechargeAmount), forKey: "itemAmount")
        itemDic.setValue("SMART_CARD", forKey: "itemType")
        itemDic.setValue(0, forKey: "validityInDays")
        
        pricingDetailList.setValue(Int(rechargeAmount), forKey: "baseAmount")
        pricingDetailList.setValue(Int(rechargeAmount), forKey: "totalAmount")
        pricingDetailArray.add(pricingDetailList)
        
        itemDic.setValue(pricingDetailList, forKey: "pricingDetails")
        itemDic.setValue(routeDetailsArray, forKey: "routeDetails")
        
        itemArray.add(itemDic)
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue("RECHARGE", forKey: "action")
        para1.setValue("\(rechargeAmount).0", forKey: "amount")
        para1.setValue("RELIANCE_METRO", forKey: "company")
        para1.setValue("SMART_CARD", forKey: "itemType")
        para1.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
        para1.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
        para1.setObject(itemArray, forKey: "items" as NSCopying)
        para1.setValue(pricingDetailList, forKey: "pricingDetails")
        
        
        let paymentDetailsArray:NSMutableArray = NSMutableArray()
        let paymentDetailsList2: NSMutableDictionary = NSMutableDictionary()
        
        print("Transactoin mode is \(transcationMode)")
            // Metro Payment Using Wallet, Card and Net Banking
            switch transcationMode {
            case "Wallet":
                print("Payment Using Wallet")
                if(Int(rechargeAmount)! > Int(citrusWalletBalance!)){
                    let remainingValue = Int(rechargeAmount)! - Int(citrusWalletBalance!)
                    payAndProceed.setTitle("ADD RS> \(remainingValue) AND PAY", for: .normal)
                }else{
                    
                    paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                    paymentDetailsList2.setValue("\(selectedWallet)", forKey: "paymentGateway")
                    paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList2)
                }
                break
            case "Card":
                if(whichCardSelected == ""){
                    // If User haven't selected any card i.e debit or credit alert box will be shown
                    uiFun.showAlert(title: "Error", message: "Please Select Card Type", logMessage: "Error in card selection", fromController: self)
                }else{
                    
                    let cardValidator = CreditCardValidator()
                    print("Card Typo Is \(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name)")
                    
                    if(wholeCardValidator(cardNumber: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!, expYearMonth: monthYearTF.text!, cvv: cvvTF.text!)){
                        let cardDetail:NSMutableDictionary = NSMutableDictionary()
                        cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                        cardDetail.setValue(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name, forKey: "cardType")
                        cardDetail.setValue(uiFun.maskCardNumber(str: caardNumberTF.text!), forKey: "maskedCardNumber")
                    
                        paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                        paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                        paymentDetailsList2.setValue(whichCardSelected, forKey: "paymentMode")
                        paymentDetailsList2.setValue(cardDetail, forKey: "details")
                        paymentDetailsArray.add(paymentDetailsList2)
                    }
                }
                break
            case "NetBanking":
                
                let bankDetail:NSMutableDictionary = NSMutableDictionary()
                bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                paymentDetailsList2.setValue("NET_BANKING", forKey: "paymentMode")
                paymentDetailsList2.setValue(bankDetail, forKey: "details")
                paymentDetailsArray.add(paymentDetailsList2)
                break
            default:
                break
            }
     
        let bookingDetail : NSMutableArray  = NSMutableArray()
        bookingDetail.add(para1)
        
        let para:NSMutableDictionary        = NSMutableDictionary()
        para.setValue(bookingDetail, forKey: "bookingDetails")
        para.setValue(paymentDetailsArray, forKey: "paymentDetails")
    
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string metro json request body = \(jsonString)") // JSON string for metro payment
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
        
        
    }
    
    func paymentRequestBodyBEST() -> String?{
        
        // BEST Payment Json String
        let bookingItemListArray            :NSMutableArray             = NSMutableArray()
        
        let bookingPaymentDetailArray       :NSMutableArray             = NSMutableArray()
        
        let itemArray                       :NSMutableArray             = NSMutableArray()
        let itemDic                         : NSMutableDictionary       = NSMutableDictionary()
        
        let pricingDetailArray              : NSMutableArray            = NSMutableArray()
        let pricingDetailList               : NSMutableDictionary       = NSMutableDictionary()
        
        let surchargeDetailArray            : NSMutableArray            = NSMutableArray()
        let surchargeDetailList             : NSMutableDictionary       = NSMutableDictionary()
        
        let routeDetailsArray               : NSMutableArray            = NSMutableArray()
        let routeDic                        : NSMutableDictionary       = NSMutableDictionary()
        
        let userArray                       : NSMutableArray            = NSMutableArray()
        let userDetail                      : NSMutableDictionary       = NSMutableDictionary()
        
        let customerDetail                  : NSMutableArray            = NSMutableArray()
        
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].activationCriteria, forKey: "activationCriteria")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].activationDate, forKey: "activationDate")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].companyIdentifier, forKey: "companyIdentifier")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].concession, forKey: "concession")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].itemAction, forKey: "itemAction")
        itemDic.setValue(Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].itemAmount), forKey: "itemAmount")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].itemSubType, forKey: "itemSubType")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].itemType, forKey: "itemType")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].passCategory, forKey: "passCategory")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].validity, forKey: "validity")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].expiryDate, forKey: "expiryDate")
        itemDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].activeRenewalDate, forKey: "activeRenewalDate")
        itemDic.setValue(0, forKey: "validityInDays")
        
        if(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetailText != "null"){
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].destinationId, forKey: "destinationId")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].routeNo, forKey: "routeNo")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].destinationName, forKey: "destinationName")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].routeId, forKey: "routeId")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].sourceStageName, forKey: "sourceStageName")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].sourceAreaId, forKey: "sourceAreaId")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].sourceName, forKey: "sourceName")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].sourceId, forKey: "sourceId")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].destinationStageName, forKey: "destinationStageName")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].sourceStageId, forKey: "sourceStageId")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].routeName, forKey: "routeName")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].destinationStageId, forKey: "destinationStageId")
            routeDic.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].routeDetails[0].destinationAreaId, forKey: "destinationAreaId")
            
            routeDetailsArray.add(routeDic)
            
        }
        pricingDetailList.setValue(Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].baseAmount), forKey: "baseAmount")
    
        pricingDetailList.setValue(Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].totalAmount), forKey: "totalAmount")
        
        surchargeDetailList.setValue(Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].surcharge[0].amount), forKey: "amount")
        surchargeDetailList.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].surcharge[0].appliedBy, forKey: "appliedBy")
        surchargeDetailList.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].surcharge[0].applyLevel, forKey: "applyLevel")
        surchargeDetailList.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].surcharge[0].applyType, forKey: "applyType")
        surchargeDetailList.setValue(0, forKey: "applyValue")
        surchargeDetailList.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].surcharge[0].name, forKey: "name")
        surchargeDetailArray.add(surchargeDetailList)
        
        pricingDetailList.setValue(surchargeDetailArray, forKey: "surcharge")
        pricingDetailArray.add(pricingDetailList)
        
        userDetail.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].user[0].dob, forKey: "dob")
        userDetail.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].user[0].name, forKey: "name")
        userDetail.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].user[0].type, forKey: "type")
        userDetail.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].user[0].gender, forKey: "gender")
        userDetail.setValue(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].user[0].userImage, forKey: "userImage")
        userArray.add(userDetail)
        
        itemDic.setValue(userDetail, forKey: "user")
        itemDic.setValue(pricingDetailList, forKey: "pricingDetails")
        itemDic.setValue(routeDetailsArray, forKey: "routeDetails")
        
        itemArray.add(itemDic)
        
        let priceDetailList : NSMutableDictionary = NSMutableDictionary()
        
        priceDetailList.setValue(Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].totalAmount), forKey: "baseAmount")
        priceDetailList.setValue(Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].totalAmount), forKey: "totalAmount")
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        let para1Array : NSMutableArray = NSMutableArray()
        
        para1.setValue("RENEW", forKey: "action")
        para1.setValue("\(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].totalAmount).0", forKey: "amount")
        para1.setValue("BEST", forKey: "company")
        para1.setValue("PASS", forKey: "itemType")
        para1.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
        para1.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
        para1.setObject(customerDetail, forKey: "customerDetails" as NSCopying)
        para1.setObject(itemArray, forKey: "items" as NSCopying)
        para1.setValue(priceDetailList, forKey: "pricingDetails")
        para1Array.add(para1)
        
        let paymentDetailsArray:NSMutableArray = NSMutableArray()
        let paymentDetailsList1: NSMutableDictionary = NSMutableDictionary()
        let paymentDetailsList2: NSMutableDictionary = NSMutableDictionary()
        
        if(ridlrPointsCB.checkState == .checked){
            // Rcash Is Selected
            let passAmount = Int(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].totalAmount)
            var remainigAmount = 0
            
            if(passAmount! > Int(ridlrPoints!)){
                
                remainigAmount = passAmount! - Int(ridlrPoints!)
                paymentDetailsList2.setValue("\(Int(ridlrPoints!)).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                paymentDetailsArray.add(paymentDetailsList2)
                
            switch transcationMode {
                // BEST Payment through wallet (Combination Payment)
                case "Wallet" :
                    
                    if(remainigAmount <= Int(citrusWalletBalance!)){
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue(selectedWallet, forKey: "paymentGateway")
                    paymentDetailsList1.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay Citrus Wallet")
                        
                }
                else{
                    
                    payAndProceed.setTitle("ADD RS. \(remainigAmount - Int(citrusWalletBalance!)) AND PAY", for: .normal)
                }
                break
                    
                case "Card":
                    
                    // BEST Payment through Card (Combination Payment)
                    let cardDetail:NSMutableDictionary = NSMutableDictionary()
                    cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                    cardDetail.setValue(uiFun.detectCardType(cardNumber: caardNumberTF.text!, fromViewController: self), forKey: "cardType")
                    cardDetail.setValue(uiFun.maskCardNumber(str: caardNumberTF.text!), forKey: "maskedCardNumber")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue(whichCardSelected, forKey: "paymentMode")
                    paymentDetailsList1.setValue(cardDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay \(whichCardSelected)")
                    break
                    
                case "NetBanking":
                    
                    // BEST payment through Net Banking (Combination Payment)
                    let bankDetail:NSMutableDictionary = NSMutableDictionary()
                    bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue("NET_BANKING", forKey: "paymentMode")
                    paymentDetailsList1.setValue(bankDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay Net Banking")
                    break
                    
                default :
                    break
            }
               
        }else{
                // Payment Through Rcash
                paymentDetailsList2.setValue("\(bestPassResponseListCurrent[0].bestData[0].actionDetails[passType].pricingDetails[0].totalAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                
                paymentDetailsArray.add(paymentDetailsList2)
            }
            
            
        }else{
            // Rcash is not selected
            print("Transactoin mode is \(transcationMode)")
            switch transcationMode {
            case "Wallet":
                // Payment through wallet
                if(Int(rechargeAmount)! > Int(citrusWalletBalance!)){
                    
                    let remainingValue = Int(rechargeAmount)! - Int(citrusWalletBalance!)
                    payAndProceed.setTitle("ADD RS> \(remainingValue) AND PAY", for: .normal)
                }else{
                    
                    paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                    paymentDetailsList2.setValue(selectedWallet, forKey: "paymentGateway")
                    paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList2)
                    uiFun.createCleverTapEvent(message: "Proceed to pay Citrus Wallet")
                }
                break
            case "Card":
                
                // Payment Through Card
                if(whichCardSelected == ""){
                    
                    uiFun.showAlert(title: "Error", message: "Please Select Card Type", logMessage: "Error in card selection", fromController: self)
                }else{
                    
                    let cardDetail:NSMutableDictionary = NSMutableDictionary()
                    cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                    cardDetail.setValue(uiFun.detectCardType(cardNumber: caardNumberTF.text!, fromViewController: self), forKey: "cardType")
                    cardDetail.setValue(uiFun.maskCardNumber(str: caardNumberTF.text!), forKey: "maskedCardNumber")
                    
                    paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                    paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList2.setValue(whichCardSelected, forKey: "paymentMode")
                    paymentDetailsList2.setValue(cardDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList2)
                    uiFun.createCleverTapEvent(message: "Proceed to pay \(whichCardSelected)")
                }
                break
            case "NetBanking":
                
                // Payment through Net Banking
                let bankDetail:NSMutableDictionary = NSMutableDictionary()
                bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                paymentDetailsList2.setValue("NET_BANKING", forKey: "paymentMode")
                paymentDetailsList2.setValue(bankDetail, forKey: "details")
                paymentDetailsArray.add(paymentDetailsList2)
                uiFun.createCleverTapEvent(message: "Proceed to pay Net Banking")
                break
                
            default:
                break
            }
        }
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(para1Array, forKey: "bookingDetails")
        para.setValue(paymentDetailsArray, forKey: "paymentDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string best json request body = \(jsonString)")
            return jsonString // json string for payment gateway
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    func MetroTokenJSONBody() -> String{
            
            // Metro TOKEN JSON String
            let date            = Date()
            let calendar        = Calendar.current
            let components      = calendar.dateComponents([.year, .month, .day], from: date)
            
            let year            = components.year
            let month           = components.month
            let day             = components.day
        
            let actualMonth     = uiFun.getTwoDigitTime(time: "\(month!)")
            let actualDay       = uiFun.getTwoDigitTime(time: "\(day!)")
            let dateMonthYear   = "\(year!)-\(actualMonth)-\(actualDay)"
            
            let bookingItemListArray     :NSMutableArray          = NSMutableArray()
            let bookingPaymentDetailArray:NSMutableArray          = NSMutableArray()
            let customerDetailArray      :NSMutableArray          = NSMutableArray()
        
            let itemArray:NSMutableArray                          = NSMutableArray()
            let itemDic: NSMutableDictionary                      = NSMutableDictionary()
        
            let pricingDetailArray      : NSMutableArray          = NSMutableArray()
            let pricingDetailList       : NSMutableDictionary     = NSMutableDictionary()
            let userDetailArray         : NSMutableDictionary     = NSMutableDictionary()
            
            let routeDetailsArray       : NSMutableArray          = NSMutableArray()
            let routeDetailDictionary   : NSMutableDictionary     = NSMutableDictionary()
        
            //for loop for multiple token request
            var i = 0
            for i in 0..<tokenNumberOfPassengers{
                
                itemDic.setValue(dateMonthYear, forKey: "activationDate")
                itemDic.setValue("ISSUE", forKey: "itemAction")
                itemDic.setValue(Int(rechargeAmount)!/tokenNumberOfPassengers, forKey: "itemAmount")
                itemDic.setValue("TICKET", forKey: "itemType")
                itemDic.setValue(metroTokenJourneyType, forKey: "itemSubType")
                itemDic.setValue("DAILY", forKey: "validity")
                itemDic.setValue(0, forKey: "validityInDays")
            
                userDetailArray.setValue("ADULT", forKey: "type")
            
                pricingDetailList.setValue(Int(rechargeAmount)!/tokenNumberOfPassengers, forKey: "baseAmount")
                pricingDetailList.setValue(Int(rechargeAmount)!/tokenNumberOfPassengers, forKey: "totalAmount")
                pricingDetailArray.add(pricingDetailList)
        
                routeDetailDictionary.setValue(tokenDestinationAgencyId, forKey: "destinationId")
                routeDetailDictionary.setValue(tokenDestinationStationName, forKey: "destinationName")
                routeDetailDictionary.setValue(0, forKey: "destinationStageId")
                routeDetailDictionary.setValue("1", forKey: "routeId")
                routeDetailDictionary.setValue("Versova - Ghatkopar", forKey: "routeName")
                routeDetailDictionary.setValue(tokenOriginStationAgencyId, forKey: "sourceId")
                routeDetailDictionary.setValue(tokenOriginStationName, forKey: "sourceName")
                routeDetailDictionary.setValue(0, forKey: "sourceStageId")
                routeDetailsArray.add(routeDetailDictionary)
        
                itemDic.setValue(pricingDetailList, forKey: "pricingDetails")
                itemDic.setValue(routeDetailsArray, forKey: "routeDetails")
                itemDic.setValue(userDetailArray, forKey: "user")
            
                itemArray.add(itemDic)
            }
        
            let pricingDetailList1       : NSMutableDictionary     = NSMutableDictionary()
            pricingDetailList1.setValue(Int(rechargeAmount)!, forKey: "baseAmount")
            pricingDetailList1.setValue(Int(rechargeAmount)!, forKey: "totalAmount")
            pricingDetailArray.add(pricingDetailList1)
        
            let para1:NSMutableDictionary = NSMutableDictionary()
            para1.setValue("ISSUE", forKey: "action")
            para1.setValue("\(rechargeAmount).0", forKey: "amount")
            para1.setValue("RELIANCE_METRO", forKey: "company")
            para1.setValue("TICKET", forKey: "itemType")
            para1.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
            para1.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
            para1.setObject(customerDetailArray, forKey: "customerDetails" as NSCopying)
            para1.setObject(itemArray, forKey: "items" as NSCopying)
            para1.setValue(pricingDetailList1, forKey: "pricingDetails")
            
            
            let paymentDetailsArray:NSMutableArray       = NSMutableArray()
            let paymentDetailsList1: NSMutableDictionary = NSMutableDictionary()
            let paymentDetailsList2: NSMutableDictionary = NSMutableDictionary()
        
        if(ridlrPointsCB.checkState == .checked){
            // Rcash Is Selected
            let passAmount      = Int(rechargeAmount)
            var remainigAmount  = 0
            
            if(passAmount! > Int(ridlrPoints!)){
                
                remainigAmount = passAmount! - Int(ridlrPoints!)
                paymentDetailsList2.setValue("\(Int(ridlrPoints!)).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                paymentDetailsArray.add(paymentDetailsList2)
                
                switch transcationMode {
                // BEST Payment through wallet (Combination Payment)
                case "Wallet" :
                    
                    if(remainigAmount <= Int(citrusWalletBalance!)){
                        
                        paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                        paymentDetailsList1.setValue(selectedWallet, forKey: "paymentGateway")
                        paymentDetailsList1.setValue("WALLET", forKey: "paymentMode")
                        paymentDetailsArray.add(paymentDetailsList1)
                        uiFun.createCleverTapEvent(message: "Proceed to pay Citrus Wallet")
                        
                    }
                    else{
                        
                        payAndProceed.setTitle("ADD RS. \(remainigAmount - Int(citrusWalletBalance!)) AND PAY", for: .normal)
                    }
                    break
                    
                case "Card":
                    // BEST Payment through Card (Combination Payment)
                    let cardDetail:NSMutableDictionary = NSMutableDictionary()
                    cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                    cardDetail.setValue(uiFun.detectCardType(cardNumber: caardNumberTF.text!, fromViewController: self), forKey: "cardType")
                    cardDetail.setValue(uiFun.maskCardNumber(str: caardNumberTF.text!), forKey: "maskedCardNumber")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue(whichCardSelected, forKey: "paymentMode")
                    paymentDetailsList1.setValue(cardDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay \(whichCardSelected)")
                    break
                    
                case "NetBanking":
                    
                    // BEST payment through Net Banking (Combination Payment)
                    let bankDetail:NSMutableDictionary = NSMutableDictionary()
                    bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue("NET_BANKING", forKey: "paymentMode")
                    paymentDetailsList1.setValue(bankDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay Net Banking")
                    break
                    
                default :
                    break
                }
                
            }else{
                // Payment Through Rcash
                paymentDetailsList2.setValue(rechargeAmount, forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                
                paymentDetailsArray.add(paymentDetailsList2)
            }
            
            
        }else{
            print("Transactoin mode is \(transcationMode)")
            // Metro Payment Using Wallet, Card and Net Banking
            switch transcationMode {
            case "Wallet":
                print("Payment Using Wallet")
                if(Int(rechargeAmount)! > Int(citrusWalletBalance!)){
                    let remainingValue = Int(rechargeAmount)! - Int(citrusWalletBalance!)
                    payAndProceed.setTitle("ADD RS> \(remainingValue) AND PAY", for: .normal)
                }else{
                    
                    paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                    paymentDetailsList2.setValue("\(selectedWallet)", forKey: "paymentGateway")
                    paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList2)
                }
                break
           
            case "Card":
                if(whichCardSelected == ""){
                    // If User haven't selected any card i.e debit or credit alert box will be shown
                    uiFun.showAlert(title: "Error", message: "Please Select Card Type", logMessage: "Error in card selection", fromController: self)
                }else{
                    
                    let cardValidator = CreditCardValidator()
                    print("Card Type Is \(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name)")
                    
                    if(wholeCardValidator(cardNumber: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!, expYearMonth: monthYearTF.text!, cvv: cvvTF.text!)){
                        let cardDetail:NSMutableDictionary = NSMutableDictionary()
                        cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                        cardDetail.setValue(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name, forKey: "cardType")
                        cardDetail.setValue(caardNumberTF.text?.replacingOccurrences(of: " ", with: ""), forKey: "maskedCardNumber")
                        
                        paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                        paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                        paymentDetailsList2.setValue(whichCardSelected, forKey: "paymentMode")
                        paymentDetailsList2.setValue(cardDetail, forKey: "details")
                        paymentDetailsArray.add(paymentDetailsList2)
                    }
                }
                break
           
            case "NetBanking":
                
                let bankDetail:NSMutableDictionary = NSMutableDictionary()
                bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                paymentDetailsList2.setValue("NET_BANKING", forKey: "paymentMode")
                paymentDetailsList2.setValue(bankDetail, forKey: "details")
                paymentDetailsArray.add(paymentDetailsList2)
                break
            default:
                break
            }
        }
        
            let bookingDetail : NSMutableArray  = NSMutableArray()
            bookingDetail.add(para1)
            
            let para:NSMutableDictionary        = NSMutableDictionary()
            para.setValue(bookingDetail, forKey: "bookingDetails")
            para.setValue(paymentDetailsArray, forKey: "paymentDetails")
            
            let jsonData: NSData
            do{
                jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
                let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
                print("json array string metro token json request body = \(jsonString)") // JSON string for metro payment
                return jsonString
                
            } catch _ {
                print ("UH OOO")
                return ""
        }
    }
    
    
    func MetroPenaltyJSON() -> String{
        
        // Metro TOKEN JSON String
        let date            = Date()
        let calendar        = Calendar.current
        let components      = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year            = components.year
        let month           = components.month
        let day             = components.day
        
        let actualMonth     = uiFun.getTwoDigitTime(time: "\(month!)")
        let actualDay       = uiFun.getTwoDigitTime(time: "\(day!)")
        let dateMonthYear   = "\(year!)-\(actualMonth)-\(actualDay)"
        
        let bookingItemListArray     :NSMutableArray          = NSMutableArray()
        let bookingPaymentDetailArray:NSMutableArray          = NSMutableArray()
        let customerDetailArray      :NSMutableArray          = NSMutableArray()
        
        let itemArray:NSMutableArray                          = NSMutableArray()
        let itemDic: NSMutableDictionary                      = NSMutableDictionary()
        
        let itemDic1      : NSMutableDictionary               = NSMutableDictionary()
        
        let pricingDetailArray      : NSMutableArray          = NSMutableArray()
        let pricingDetailList       : NSMutableDictionary     = NSMutableDictionary()
        
        
        let pricingDetailArray1      : NSMutableArray          = NSMutableArray()
        let pricingDetailList1       : NSMutableDictionary     = NSMutableDictionary()
        
        let routeDetailsArray       : NSMutableArray          = NSMutableArray()
        let routeDetailDictionary   : NSMutableDictionary     = NSMutableDictionary()
    
        /************* items array at index 0 ******************/
        itemDic.setValue(dateMonthYear, forKey: "activationDate")
        itemDic.setValue("ISSUE", forKey: "itemAction")
        itemDic.setValue(0, forKey: "itemAmount")
        itemDic.setValue(penalyList[0].itemType, forKey: "itemType")
        itemDic.setValue(penalyList[0].itemSubType, forKey: "itemSubType")
        itemDic.setValue(penalyList[0].itemAttribute[0].refTxnId, forKey: "refTxnId")
        itemDic.setValue("DAILY", forKey: "validity")
        itemDic.setValue(0, forKey: "validityInDays")
        
        pricingDetailList.setValue(0, forKey: "baseAmount")
        pricingDetailList.setValue(0, forKey: "totalAmount")
        pricingDetailArray.add(pricingDetailList)
            
        routeDetailDictionary.setValue(Int(penaltyStationId), forKey: "destinationId")
        routeDetailDictionary.setValue(penaltyStation, forKey: "destinationName")
        routeDetailDictionary.setValue(0, forKey: "destinationStageId")
        routeDetailDictionary.setValue("1", forKey: "routeId")
        routeDetailDictionary.setValue("Versova - Ghatkopar", forKey: "routeName")
        routeDetailDictionary.setValue(uiFun.readData(key: "penalyuSourceId"), forKey: "sourceId")
        routeDetailDictionary.setValue(penalyList[0].bookingRoute[0].sourceName, forKey: "sourceName")
        routeDetailDictionary.setValue(0, forKey: "sourceStageId")
        routeDetailsArray.add(routeDetailDictionary)
            
        itemDic.setValue(pricingDetailList, forKey: "pricingDetails")
        itemDic.setValue(routeDetailsArray, forKey: "routeDetails")
        
        itemArray.add(itemDic)

        /************* items array at index 1 ******************/
        
        itemDic1.setValue(dateMonthYear, forKey: "activationDate")
        itemDic1.setValue("ISSUE", forKey: "itemAction")
        itemDic1.setValue(Int(rechargeAmount), forKey: "itemAmount")
        itemDic1.setValue(penalyList[0].itemType, forKey: "itemType")
        itemDic1.setValue(penalyList[0].itemSubType1, forKey: "itemSubType")
        itemDic1.setValue(penalyList[0].itemAttribute[0].refTxnId, forKey: "refTxnId")
        itemDic1.setValue("DAILY", forKey: "validity")
        itemDic1.setValue(0, forKey: "validityInDays")
        
        pricingDetailList1.setValue(Int(rechargeAmount), forKey: "baseAmount")
        pricingDetailList1.setValue(Int(rechargeAmount), forKey: "totalAmount")
        pricingDetailArray1.add(pricingDetailList)
        
        itemDic1.setValue(pricingDetailList1, forKey: "pricingDetails")
        itemDic1.setValue(routeDetailsArray, forKey: "routeDetails")
        
        
        itemArray.add(itemDic1)
        /******* Actual json body ********/
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue("ISSUE", forKey: "action")
        para1.setValue("\(rechargeAmount).0", forKey: "amount")
        para1.setValue("RELIANCE_METRO", forKey: "company")
        para1.setValue(penalyList[0].itemType, forKey: "itemType")
        para1.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
        para1.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
        para1.setObject(customerDetailArray, forKey: "customerDetails" as NSCopying)
        para1.setObject(itemArray, forKey: "items" as NSCopying)
        
        para1.setValue(pricingDetailList1, forKey: "pricingDetails")
        
        
        let paymentDetailsArray:NSMutableArray       = NSMutableArray()
        let paymentDetailsList1: NSMutableDictionary = NSMutableDictionary()
        let paymentDetailsList2: NSMutableDictionary = NSMutableDictionary()
        
        if(ridlrPointsCB.checkState == .checked){
            // Rcash Is Selected
            let passAmount      = Int(rechargeAmount)
            var remainigAmount  = 0
            
            if(passAmount! > Int(ridlrPoints!)){
                
                remainigAmount = passAmount! - Int(ridlrPoints!)
                paymentDetailsList2.setValue("\(Int(ridlrPoints!)).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                paymentDetailsArray.add(paymentDetailsList2)
                
                switch transcationMode {
                // BEST Payment through wallet (Combination Payment)
                case "Wallet" :
                    
                    if(remainigAmount <= Int(citrusWalletBalance!)){
                        
                        paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                        paymentDetailsList1.setValue(selectedWallet, forKey: "paymentGateway")
                        paymentDetailsList1.setValue("WALLET", forKey: "paymentMode")
                        paymentDetailsArray.add(paymentDetailsList1)
                        uiFun.createCleverTapEvent(message: "Proceed to pay Citrus Wallet")
                        
                    }
                    else{
                        
                        payAndProceed.setTitle("ADD RS. \(remainigAmount - Int(citrusWalletBalance!)) AND PAY", for: .normal)
                    }
                    break
                    
                case "Card":
                    // BEST Payment through Card (Combination Payment)
                    let cardDetail:NSMutableDictionary = NSMutableDictionary()
                    cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                    cardDetail.setValue(uiFun.detectCardType(cardNumber: caardNumberTF.text!, fromViewController: self), forKey: "cardType")
                    cardDetail.setValue(caardNumberTF.text?.replacingOccurrences(of: " ", with: ""), forKey: "maskedCardNumber")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue(whichCardSelected, forKey: "paymentMode")
                    paymentDetailsList1.setValue(cardDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay \(whichCardSelected)")
                    break
                    
                case "NetBanking":
                    
                    // BEST payment through Net Banking (Combination Payment)
                    let bankDetail:NSMutableDictionary = NSMutableDictionary()
                    bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue("NET_BANKING", forKey: "paymentMode")
                    paymentDetailsList1.setValue(bankDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay Net Banking")
                    break
                    
                default :
                    break
                }
                
            }else{
                // Payment Through Rcash
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                
                paymentDetailsArray.add(paymentDetailsList2)
            }
            
            
        }else{
            print("Transactoin mode is \(transcationMode)")
            // Metro Payment Using Wallet, Card and Net Banking
            switch transcationMode {
            case "Wallet":
                print("Payment Using Wallet")
                if(Int(rechargeAmount)! > Int(citrusWalletBalance!)){
                    let remainingValue = Int(rechargeAmount)! - Int(citrusWalletBalance!)
                    payAndProceed.setTitle("ADD RS> \(remainingValue) AND PAY", for: .normal)
                }else{
                    
                    paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                    paymentDetailsList2.setValue("\(selectedWallet)", forKey: "paymentGateway")
                    paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList2)
                }
                break
                
            case "Card":
                if(whichCardSelected == ""){
                    // If User haven't selected any card i.e debit or credit alert box will be shown
                    uiFun.showAlert(title: "Error", message: "Please Select Card Type", logMessage: "Error in card selection", fromController: self)
                }else{
                    
                    let cardValidator = CreditCardValidator()
                    print("Card Type Is \(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name)")
                    
                    if(wholeCardValidator(cardNumber: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!, expYearMonth: monthYearTF.text!, cvv: cvvTF.text!)){
                        let cardDetail:NSMutableDictionary = NSMutableDictionary()
                        cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                        cardDetail.setValue(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name, forKey: "cardType")
                        cardDetail.setValue(caardNumberTF.text?.replacingOccurrences(of: " ", with: ""), forKey: "maskedCardNumber")
                        
                        paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                        paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                        paymentDetailsList2.setValue(whichCardSelected, forKey: "paymentMode")
                        paymentDetailsList2.setValue(cardDetail, forKey: "details")
                        paymentDetailsArray.add(paymentDetailsList2)
                    }
                }
                break
                
            case "NetBanking":
                
                let bankDetail:NSMutableDictionary = NSMutableDictionary()
                bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                paymentDetailsList2.setValue("NET_BANKING", forKey: "paymentMode")
                paymentDetailsList2.setValue(bankDetail, forKey: "details")
                paymentDetailsArray.add(paymentDetailsList2)
                break
            default:
                break
            }
        }
        
        let bookingDetail : NSMutableArray  = NSMutableArray()
        bookingDetail.add(para1)
        
        let para:NSMutableDictionary        = NSMutableDictionary()
        para.setValue(bookingDetail, forKey: "bookingDetails")
        para.setValue(paymentDetailsArray, forKey: "paymentDetails")
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string metro penalty json request body = \(jsonString)") // JSON string for metro payment
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return ""
        }
    }
    
    func metroTripJSON(itemAction : String, itemSubType : String) -> String{
    
        // Metro TOKEN JSON String
        let date            = Date()
        let calendar        = Calendar.current
        let components      = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year            = components.year
        let month           = components.month
        let day             = components.day
        
        let actualMonth     = uiFun.getTwoDigitTime(time: "\(month!)")
        let actualDay       = uiFun.getTwoDigitTime(time: "\(day!)")
        let dateMonthYear   = "\(year!)-\(actualMonth)-\(actualDay)"
        
        let bookingItemListArray     :NSMutableArray          = NSMutableArray()
        let bookingPaymentDetailArray:NSMutableArray          = NSMutableArray()
        let customerDetailArray      :NSMutableArray          = NSMutableArray()
        
        let itemArray   : NSMutableArray                      = NSMutableArray()
        let itemDic     : NSMutableDictionary                 = NSMutableDictionary()
        
        let itemArray0    : NSMutableArray                    = NSMutableArray()
        let itemDic0      : NSMutableDictionary               = NSMutableDictionary()
        
        let pricingDetailArray      : NSMutableArray          = NSMutableArray()
        let pricingDetailList       : NSMutableDictionary     = NSMutableDictionary()
        let userDetailArray         : NSMutableDictionary     = NSMutableDictionary()
        
        let routeDetailsArray       : NSMutableArray          = NSMutableArray()
        let routeDetailDictionary   : NSMutableDictionary     = NSMutableDictionary()
        
        let routeDetailArray0       : NSMutableArray          = NSMutableArray()
    
        let pricingDetailArray0     : NSMutableArray          = NSMutableArray()
        let pricingDetailList0      : NSMutableDictionary     = NSMutableDictionary()
        
        /********* Pricing Detail Array ************/
        pricingDetailList0.setValue(0, forKey: "baseAmount")
        pricingDetailList0.setValue(0, forKey: "totalAmount")
        pricingDetailArray0.add(pricingDetailList)
        
        /********* Booking detail array 0 **************/
        
        itemDic0.setValue(uiFun.readFromDatabase(entityName: "Users", key: "userEmail"), forKey: "email")
        itemDic0.setValue(uiFun.readFromDatabase(entityName: "Users", key: "mobileNumber"), forKey: "mobile")
        itemDic0.setValue(dateMonthYear, forKey: "activationDate")
        itemDic0.setValue("ISSUE", forKey: "itemAction")
        itemDic0.setValue(0, forKey: "itemAmount")
        itemDic0.setValue(itemSubType, forKey: "itemSubType")
        itemDic0.setValue("E_ID_CARD", forKey: "itemType")
        itemDic0.setValue(routeDetailArray0, forKey: "routeDetails")
        itemDic0.setValue(0, forKey: "validityInDays")
        itemDic0.setValue(pricingDetailList0, forKey: "pricingDetails")
        itemArray0.add(itemDic0)
        
        /********* Booking detail array 1 **************/
        itemDic.setValue(dateMonthYear, forKey: "activationDate")
        itemDic.setValue(itemAction, forKey: "itemAction")
        itemDic.setValue(Int(rechargeAmount), forKey: "itemAmount")
        itemDic.setValue("PASS", forKey: "itemType")
        itemDic.setValue(metroTokenJourneyType, forKey: "itemSubType")
        itemDic.setValue("DAILY", forKey: "validity")
        itemDic.setValue(0, forKey: "validityInDays")
        
        if(isFromRechargePass){
            isFromRechargePass = false
            itemDic.setValue(uiFun.readData(key: "metroStoreValueMasterQRCode"), forKey: "companyIdentifier")
        }
        
        userDetailArray.setValue("ADULT", forKey: "type")
        
        pricingDetailList.setValue(Int(rechargeAmount), forKey: "baseAmount")
        pricingDetailList.setValue(Int(rechargeAmount), forKey: "totalAmount")
        pricingDetailArray.add(pricingDetailList)
        
        /********  Booking detail array at index 0 ***********/
        
            
        routeDetailDictionary.setValue(tripDestinationStationAgencyId, forKey: "destinationId")
        routeDetailDictionary.setValue(tripDestinationStation, forKey: "destinationName")
        routeDetailDictionary.setValue(0, forKey: "destinationStageId")
        routeDetailDictionary.setValue("1", forKey: "routeId")
        routeDetailDictionary.setValue("Versova - Ghatkopar", forKey: "routeName")
        routeDetailDictionary.setValue(tripOriginStationAgencyId, forKey: "sourceId")
        routeDetailDictionary.setValue(tripOriginStation, forKey: "sourceName")
        routeDetailDictionary.setValue(0, forKey: "sourceStageId")
        routeDetailsArray.add(routeDetailDictionary)
            
        itemDic.setValue(pricingDetailList, forKey: "pricingDetails")
        itemDic.setValue(routeDetailsArray, forKey: "routeDetails")
        itemDic.setValue(userDetailArray, forKey: "user")
            
        itemArray.add(itemDic)
        
        let para0 : NSMutableDictionary = NSMutableDictionary()
        para0.setValue("ISSUE", forKey: "action")
        para0.setValue("0", forKey: "amount")
        para0.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
        para0.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
        para0.setValue("RELIANCE_METRO", forKey: "company")
        para0.setValue("E_ID_CARD", forKey: "itemType")
        para0.setObject(itemArray0, forKey: "items" as NSCopying)
        para0.setObject(customerDetailArray, forKey: "customerDetails" as NSCopying)
        para0.setValue(pricingDetailList0, forKey: "pricingDetails")
        
        let para1:NSMutableDictionary   = NSMutableDictionary()
        para1.setValue(itemAction, forKey: "action")
        para1.setValue("\(rechargeAmount).0", forKey: "amount")
        para1.setValue("RELIANCE_METRO", forKey: "company")
        para1.setValue("PASS", forKey: "itemType")
        para1.setObject(bookingItemListArray, forKey: "bookingItemsList" as NSCopying)
        para1.setObject(bookingPaymentDetailArray, forKey: "bookingPaymentDetailsList" as NSCopying)
        para1.setObject(customerDetailArray, forKey: "customerDetails" as NSCopying)
        para1.setObject(itemArray, forKey: "items" as NSCopying)
        para1.setValue(pricingDetailList, forKey: "pricingDetails")
        
        let paymentDetailsArray:NSMutableArray       = NSMutableArray()
        let paymentDetailsList1: NSMutableDictionary = NSMutableDictionary()
        let paymentDetailsList2: NSMutableDictionary = NSMutableDictionary()
        
        if(ridlrPointsCB.checkState == .checked){
            // Rcash Is Selected
            let passAmount      = Int(rechargeAmount)
            var remainigAmount  = 0
            
            if(passAmount! > Int(ridlrPoints!)){
                
                remainigAmount = passAmount! - Int(ridlrPoints!)
                paymentDetailsList2.setValue("\(Int(ridlrPoints!)).0", forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                paymentDetailsArray.add(paymentDetailsList2)
                
                switch transcationMode {
                // BEST Payment through wallet (Combination Payment)
                case "Wallet" :
                    
                    if(remainigAmount <= Int(citrusWalletBalance!)){
                        
                        paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                        paymentDetailsList1.setValue(selectedWallet, forKey: "paymentGateway")
                        paymentDetailsList1.setValue("WALLET", forKey: "paymentMode")
                        paymentDetailsArray.add(paymentDetailsList1)
                        uiFun.createCleverTapEvent(message: "Proceed to pay Citrus Wallet")
                        
                    }
                    else{
                        
                        payAndProceed.setTitle("ADD RS. \(remainigAmount - Int(citrusWalletBalance!)) AND PAY", for: .normal)
                    }
                    break
                    
                case "Card":
                    // BEST Payment through Card (Combination Payment)
                    let cardDetail:NSMutableDictionary = NSMutableDictionary()
                    cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                    cardDetail.setValue(uiFun.detectCardType(cardNumber: caardNumberTF.text!, fromViewController: self), forKey: "cardType")
                    cardDetail.setValue(caardNumberTF.text?.replacingOccurrences(of: " ", with: ""), forKey: "maskedCardNumber")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue(whichCardSelected, forKey: "paymentMode")
                    paymentDetailsList1.setValue(cardDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay \(whichCardSelected)")
                    break
                    
                case "NetBanking":
                    
                    // BEST payment through Net Banking (Combination Payment)
                    let bankDetail:NSMutableDictionary = NSMutableDictionary()
                    bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                    
                    paymentDetailsList1.setValue("\(remainigAmount).0", forKey: "amount")
                    paymentDetailsList1.setValue("CITRUSPAY", forKey: "paymentGateway")
                    paymentDetailsList1.setValue("NET_BANKING", forKey: "paymentMode")
                    paymentDetailsList1.setValue(bankDetail, forKey: "details")
                    paymentDetailsArray.add(paymentDetailsList1)
                    uiFun.createCleverTapEvent(message: "Proceed to pay Net Banking")
                    break
                    
                default :
                    break
                }
                
            }else{
                // Payment Through Rcash
                paymentDetailsList2.setValue(rechargeAmount, forKey: "amount")
                paymentDetailsList2.setValue("RIDLR_CASH", forKey: "paymentGateway")
                paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                
                paymentDetailsArray.add(paymentDetailsList2)
            }
            
            
        }else{
            print("Transactoin mode is \(transcationMode)")
            // Metro Payment Using Wallet, Card and Net Banking
            switch transcationMode {
            case "Wallet":
                print("Payment Using Wallet")
                if(Int(rechargeAmount)! > Int(citrusWalletBalance!)){
                    let remainingValue = Int(rechargeAmount)! - Int(citrusWalletBalance!)
                    payAndProceed.setTitle("ADD RS> \(remainingValue) AND PAY", for: .normal)
                }else{
                    
                    paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                    paymentDetailsList2.setValue("\(selectedWallet)", forKey: "paymentGateway")
                    paymentDetailsList2.setValue("WALLET", forKey: "paymentMode")
                    paymentDetailsArray.add(paymentDetailsList2)
                }
                break
                
            case "Card":
                if(whichCardSelected == ""){
                    // If User haven't selected any card i.e debit or credit alert box will be shown
                    uiFun.showAlert(title: "Error", message: "Please Select Card Type", logMessage: "Error in card selection", fromController: self)
                }else{
                    
                    let cardValidator = CreditCardValidator()
                    print("Card Type Is \(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name)")
                    
                    if(wholeCardValidator(cardNumber: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!, expYearMonth: monthYearTF.text!, cvv: cvvTF.text!)){
                        let cardDetail:NSMutableDictionary = NSMutableDictionary()
                        cardDetail.setValue(monthYearTF.text, forKey: "cardExpiry")
                        cardDetail.setValue(cardValidator.type(from: (caardNumberTF.text?.replacingOccurrences(of: " ", with: ""))!)?.name, forKey: "cardType")
                        cardDetail.setValue(caardNumberTF.text?.replacingOccurrences(of: " ", with: ""), forKey: "maskedCardNumber")
                        
                        paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                        paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                        paymentDetailsList2.setValue(whichCardSelected, forKey: "paymentMode")
                        paymentDetailsList2.setValue(cardDetail, forKey: "details")
                        paymentDetailsArray.add(paymentDetailsList2)
                    }
                }
                break
                
            case "NetBanking":
                
                let bankDetail:NSMutableDictionary = NSMutableDictionary()
                bankDetail.setValue(netBankingIssueCodePay, forKey: "issuerCode")
                
                paymentDetailsList2.setValue("\(rechargeAmount).0", forKey: "amount")
                paymentDetailsList2.setValue("CITRUSPAY", forKey: "paymentGateway")
                paymentDetailsList2.setValue("NET_BANKING", forKey: "paymentMode")
                paymentDetailsList2.setValue(bankDetail, forKey: "details")
                paymentDetailsArray.add(paymentDetailsList2)
                break
            default:
                break
            }
        }
        
        let bookingDetail : NSMutableArray  = NSMutableArray()
        
        if(!passRecharge){
            bookingDetail.add(para0)
            passRecharge    = false
        }
        bookingDetail.add(para1)
        
        
        let para:NSMutableDictionary        = NSMutableDictionary()
        para.setValue(bookingDetail, forKey: "bookingDetails")
        para.setValue(paymentDetailsArray, forKey: "paymentDetails")
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string metro trip json request body = \(jsonString)") // JSON string for metro payment
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return ""
        }
    
    }
    
    func citrusAddMoney(){
        // If wallet doesnt have enough balance will navigate to AddMoney view controller
        uiFun.navigateToScreem(identifierName: "AddMoneyViewController", fromController: self)
    }
    
    
    func disablePayAndProceed(){
    
        payAndProceed.isEnabled                 = false
        payAndProceed.isUserInteractionEnabled  = false
        payAndProceed.backgroundColor           = uiFun.hexStringToUIColor(hex: ridlrColors.greyColor)
        isConnectedToInternet                   = false
    }
    
    func enablePayAndProceed(){
    
        payAndProceed.isEnabled         = true
        payAndProceed.backgroundColor   = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
    }
    
    func gotResponseFromArhitecture(response : JSON){
        
        // Response of the wallet balance, bank list name, and save cards
        uiFun.hideIndicatorLoader()
        showLoader  = false
        let agencyName              = uiFun.readData(key: "ticketingAgency")
        
        if(String(describing: response["status"]) == "OK" || String(describing: response["status"]) == "PHE"){
            uiFun.hideIndicatorLoader()
            loader.isHidden = true
            loader.stopAnimating()
            enablePayAndProceed()
            
            print("JSON RESPONSE Payment Response \(response)")
            print("JSON OBJECT VALUE Payment Option \(response["data"])")
            
            let jsonData = response["data"]
            print("Payment Reponse \(jsonData)")
            
            
            /************ CITRUS WALLET CHECK ************/
            let jsonPaymentOption = jsonData["paymentOptions"].arrayValue[0]
            isCitrusConfigured    = NSString(string: String(describing: jsonPaymentOption["configured"])).boolValue
            print("Citrus Config \(isCitrusConfigured)")
            
            if(isCitrusConfigured){
                if(jsonPaymentOption["details"] != nil){
                    // Citrus Wallet Added
                    let citrusBalance       = String(describing: jsonPaymentOption["details"]["balance"]) // Citrus Balance
                    print("jsonPaymentOption \(citrusBalance)")
                    citrusBalace.text       = "Citrus (Rs \(citrusBalance))"
                    citrusWalletBalance     = Double(citrusBalance)
                    citrusRadio.isSelected  = true
                    selectedWallet          = "CITRUSPAY"    // By default when payment is made through wallet it will use citrus wallet
                    
                    print("Citrus balance is equal to \(citrusWalletBalance!)")
                }else{
                    // Citrus Wallet Not Added
                    uiFun.hideIndicatorLoader()
                    citrusBalace.text       = "Citrus (Currently Unavailable)"
                    citrusWalletBalance     = 0.0
                    citrusRadio.isHidden    = true
                    
                    //uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
                }
            }else{
                uiFun.hideIndicatorLoader()
                citrusBalace.text       = "Citrus (Add Wallet)"
                citrusRadio.isHidden    = true
                citrusWalletBalance     = 0.0
            }
            
            /************ MOBIKWIK WALLET CHECK ************/
            
            let jsonPaymentOptionMobi = jsonData["paymentOptions"].arrayValue[6]
            isMobikwikConfigured      = NSString(string: String(describing: jsonPaymentOptionMobi["configured"])).boolValue
            
            if(isMobikwikConfigured){
                if(jsonPaymentOptionMobi["details"] != nil){
                    // Mobikwik Wallet Added
                    let mobikwikBalance      = String(describing: jsonPaymentOptionMobi["details"]["balance"]) // Mobikwik Balance
                    print("jsonPaymentOption \(mobikwikBalance)")
                    mobikwikLabel1.text      = "Mobikwik (Rs \(mobikwikBalance))"
                    mobikwikWalletBalance    = Double(mobikwikBalance)
                    mobikwikRadio1.isHidden  = false
                    print("Mobikwik balance is equal to \(mobikwikWalletBalance!)")
                }else{
                    // Mobikwik Wallet Not Added
                    uiFun.hideIndicatorLoader()
                    mobikwikLabel1.text         = "Mobikwik (Currently Unavailable)"
                    mobikwikRadio1.isHidden     = true
                    mobikwikWalletBalance       = 0.0
                    
                }
            }else{
                uiFun.hideIndicatorLoader()
                mobikwikLabel1.text      = "Mobikwik (Add Wallet)"
                mobikwikRadio1.isHidden   = true
                mobikwikWalletBalance   = 0.0
            }
            
            /************ FREECHARGE WALLET CHECK ************/
            
            let jsonPaymentOptionFree   = jsonData["paymentOptions"].arrayValue[5]
            isFreechargeConfigured      = NSString(string: String(describing: jsonPaymentOptionFree["configured"])).boolValue
            
            if(isFreechargeConfigured){
                if(jsonPaymentOptionFree["details"] != nil){
                    // Freecharge Wallet Added
                    let freechargeBalance   = String(describing: jsonPaymentOptionFree["details"]["balance"]) // Freecharge Balance
                    print("jsonPaymentOption \(freechargeBalance)")
                    freechargeLabel.text    = "Freecharge (Rs \(freechargeBalance))"
                    freechargeWalletBalance = Double(freechargeBalance)
                    isFreechargeConfigured  = true
                    freechargeRadio.isHidden = false
                    print("Freecharge balance is equal to \(freechargeWalletBalance!)")
                }else{
                    // Freecharge Wallet Not Added
                    uiFun.hideIndicatorLoader()
                    freechargeLabel.text        = "Freecharge (Currently Unavailabel)"
                    freechargeWalletBalance     = 0.0
                    freechargeRadio.isHidden    = true
                    //uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
                    
                }
            }else{
            
                uiFun.hideIndicatorLoader()
                freechargeLabel.text        = "Freecharge (Add Wallet)"
                freechargeRadio.isHidden    = true
                freechargeWalletBalance     = 0.0
            }
            
            
        
            let bankDetail      = jsonData["paymentOptions"].arrayValue[2] // Bank Detail Array
            let bankDetailArray = bankDetail["details"]
            let bankArray       = bankDetailArray["banks"]
            
            var i = 0
            
            while(i < bankArray.count){
                
                let bankModel           = BankModel()
                
                bankModel.bankName      = String(describing: bankArray.arrayValue[i]["bankName"])
                bankModel.issuerCode    = String(describing: bankArray.arrayValue[i]["issuerCode"])
                bankArrayList.append(bankModel)
                i += 1
            }
            
            bankPicker.reloadAllComponents()
            
            let ridlrCash = jsonData["paymentOptions"].arrayValue[7] // change if staging url is change since it excluded of freecharge
            print("Ridlr Cash \(ridlrCash)")
            
            if(ridlrCash["details"] != nil){
                let rcash = ridlrCash["details"]["balance"]     // ridlr cash balance
                print("RCASH AMOUNT IS \(rcash)")
                
                rcashLabel.text = String(describing: ridlrCash["details"]["balance"]) //100
                ridlrPoints     = Double(String(describing: ridlrCash["details"]["balance"]))! //100.0//
        
                print("Ridlr points are equal to \(ridlrPoints)")
            }else{
                
                // Unabel to fetch ridlr cash
                uiFun.hideIndicatorLoader()
                rcashLabel.text = "0"
                
                ridlrPoints = 0
                print("Ridlr points are equal to \(ridlrPoints)")
            }
            
            // Hide Pay and Proceed if not single wallet configured or availabel
            
            if(!isCitrusConfigured && !isMobikwikConfigured && !isFreechargeConfigured){
                
                if(Int(ridlrPoints!) < Int(rechargeAmount)!){
                    payAndProceed.isHidden = true
                }else{
                    payAndProceed.isHidden = false
                }
                
            }else{
                payAndProceed.isHidden      = false
                citrusRadio.isHidden        = !isCitrusConfigured
                mobikwikRadio.isHidden      = !isMobikwikConfigured
                
                freechargeRadio.isHidden    = !isFreechargeConfigured
            }
            
            /***************** UPDATING UI ********************/
            
            if(isRidlrCBChecked && agencyName == "Best"){
                updatingUI()
            }else if(agencyName == "Metro"){
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.UpdateUIForMetro()
                })
                //
                let amountToAdd     = Int(rechargeAmount)! - Int(citrusWalletBalance!) // Required Amount To Complete transcation
                amountToAddValue    = "\(amountToAdd).0"
                print("Amount to add is \(amountToAdd)")
                
                if(Int(citrusWalletBalance!) >= Int(payableAmount.text!)!){
                    // Wallet having sufficient amount to pay
                    payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
                
                }else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
                    // Wallet doesnt have sufficient amount to pay
                    payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)
                }
            }else{
                
                isRidlrCBChecked             = true
                ridlrPointsCB.checkState     = .unchecked
               
                let citrusRounfValueofWallet = Int(citrusWalletBalance!)
               
                if(citrusRounfValueofWallet >= Int(payableAmount.text!)!){
                    payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
                
                }else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
                    
                    let amountToAdd     = Int(rechargeAmount)! - Int(citrusWalletBalance!) // Required Amount To Complete transcation
                    amountToAddValue    = "\(amountToAdd).0"
                    print("Amount to add is \(amountToAdd)")
                    
                    let amountToAddInWallet  = Int(payableAmount.text!)! - Int(citrusWalletBalance!)
                    payAndProceed.setTitle("ADD RS. \(amountToAddInWallet) AND PAY", for: .normal)

                }
            }
//            if(fromCitrusWebView){
//                print("From Citrus Web View \(fromCitrusWebView)")
//                //This method gets called after load money into citrus.
//                if(payableAmount.text == "0"){
//                    PaymentAPICall() // after load money want to call make payment uncomment this api call
//                    fromCitrusWebView = false
//                }
//            }
        }
        else{
            
            loader.isHidden = true
            loader.stopAnimating()
            
            // disabling pay and process button if the network connection unavailable
            disablePayAndProceed()
            
            uiFun.hideIndicatorLoader()
            let jsonError = response["error"]
            print(response)
            print("No internet connection \(response["status"])")
            
            // Fetching api error from json response
            if(String(describing: response["errorCodeText"]) == "" || String(describing: response["errorCodeText"]) == "null"){
                // if errorCodeText is null or blanck parse the api response to "showExceptionDialog" method
                uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
            }else{
                // Show api error in alert box
                uiFun.showAlert(title: "Error", message: String(describing: jsonError["errorCodeText"]), logMessage: "Recharge Failed", fromController: self)
            }
        }
        uiFun.hideIndicatorLoader()
    }
    
    func cardUpdateUI(){
        // Showing payable amount after deducting amount from ridlr cash
       // updatingUI()
        
        if(Int(rechargeAmount)! <= Int(rcashLabel.text!)!){
            
            // if user have enough rcash for payment, hide wallet, card and net banking UI
            hideCitrusLayout()
            cardUIView.isHidden     = true
            bankLabel.isHidden      = true
            bankPicker.isHidden     = true
            
            if(ridlrPoints == 0.0){
                ridlrPointsCB.setCheckState(.unchecked, animated: true)
            }else{
                ridlrPointsCB.setCheckState(.checked, animated: true)
            }
            
            payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            payableAmount.text = "0"
            
            mobikwikView.isHidden   = true
            
        }else if(Int(rechargeAmount)! > Int(rcashLabel.text!)!){
            
            // if user doesn't have enough rcash for payment, show wallet, card and net banking UI
            if(ridlrPoints == 0.0){
                ridlrPointsCB.setCheckState(.unchecked, animated: true)
            }else{
                ridlrPointsCB.checkState = .checked
            }
            
            showCitrusLayout()
            payableAmount.text       = String(Int(rechargeAmount)! - Int(rcashLabel.text!)!)
            let amountToAdd          = Int(payableAmount.text!)! - Int(citrusWalletBalance!)
            
            if(Int(citrusWalletBalance!) >= Int(payableAmount.text!)!){
                
                payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            }else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
                
                amountToAddValue = "\(Int(payableAmount.text!)! - Int(citrusWalletBalance!)).0"
                payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)
            }
        }
    }
    
    func updatingUI(){
        
        if(Int(rechargeAmount)! <= Int(rcashLabel.text!)!){
            
            // if user have enough rcash for payment, hide wallet, card and net banking UI
            hideCitrusLayout()
            cardUIView.isHidden     = true
            bankLabel.isHidden      = true
            bankPicker.isHidden     = true
            
            if(ridlrPoints == 0.0){
                ridlrPointsCB.setCheckState(.unchecked, animated: true)
            }else{
                ridlrPointsCB.setCheckState(.checked, animated: true)
            }
            
            payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            payableAmount.text = "0"
            
            mobikwikView.isHidden   = true
            
        }else if(Int(rechargeAmount)! > Int(rcashLabel.text!)!){
            
            // if user doesn't have enough rcash for payment, show wallet, card and net banking UI
            if(ridlrPoints == 0.0){
                ridlrPointsCB.setCheckState(.unchecked, animated: true)
            }else{
                ridlrPointsCB.checkState = .checked
            }
            
            showCitrusLayout()
            payableAmount.text       = String(Int(rechargeAmount)! - Int(rcashLabel.text!)!)
            let amountToAdd          = Int(payableAmount.text!)! - Int(citrusWalletBalance!)
            
            if(Int(citrusWalletBalance!) >= Int(payableAmount.text!)!){
                
                payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
            }else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
                
                amountToAddValue = "\(Int(payableAmount.text!)! - Int(citrusWalletBalance!)).0"
                payAndProceed.setTitle("ADD RS. \(amountToAdd) AND PAY", for: .normal)
            }
        }
    }
    
    func updateWalletUI(){
        if(String(describing :citrusWalletBalance) != ""){
            if(Int(citrusWalletBalance!) >= Int(payableAmount.text!)!){
                payAndProceed.setTitle("PROCEED TO PAY", for: .normal)
        
        }else if(Int(citrusWalletBalance!) < Int(payableAmount.text!)!){
            
                let amountToAddWallet = Int(payableAmount.text!)! - Int(citrusWalletBalance!)
                payAndProceed.setTitle("ADD RS. \(amountToAddWallet) AND PAY", for: .normal)
            }
        }
    }
    
    func NetBankingResponse(response : JSON){
        // Net banking payment respone
        uiFun.hideIndicatorLoader()
        print("Net banking response \(response)")
        if(response["status"] == "OK"){
            
            let netBankingData    = response["data"]
            // Open this redirectURL in webview to complete transaction
            redirectUrl           = String(describing: netBankingData["redirectUrl"])
            
            print("Citrus Add Money Redirect URL\(redirectUrl)")
            self.uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
            uiFun.createCleverTapEvent(message: "Payment response \(initiator) Success")
            
        }else{
            self.uiFun.hideIndicatorLoader()
            
            let jsonError   = response["error"]
            
            // Show Error in alert box from api response
            self.uiFun.showAlert(title: "Error", message: String(describing: jsonError["errorCodeText"]), logMessage: "Recharge Failed", fromController: self)
            uiFun.createCleverTapEvent(message: "Payment response \(initiator) failed \(response)")
        }
        
    }
    
    func MobiFreeWalletResponse(response : JSON){
    
        // Mobikwik & Freecharge Response
        uiFun.hideIndicatorLoader()
        if(String(describing: response["status"]) == "OK"){
            // Success Response
            let jsonData    = response["data"]
            // Open this redirectURL in webview to complete transaction
            redirectUrl     = String(describing: jsonData["redirectUrl"])
            uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
        }else{
            
            uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
        }
    }
    
    
    
    func hideCitrusLayout(){
        
        paymentTypeSegment.isHidden = true
        citrusLogo.isHidden         = true
        citrusBalace.isHidden       = true
        citrusSeparator.isHidden    = true
        citrusRadio.isHidden        = true
        
        freechargeView.isHidden     = true
        mobikwikView.isHidden       = true
    }
    
    func showCitrusLayout(){
        
        paymentTypeSegment.isHidden = false
        citrusLogo.isHidden         = false
        citrusBalace.isHidden       = false
        citrusSeparator.isHidden    = true
        citrusRadio.isHidden        = false
        
        freechargeView.isHidden     = false
        mobikwikView.isHidden       = false
        citrusView.isHidden         = false
    }
    
    func showCitrusWallet(){
        
        citrusLogo.isHidden         = false
        citrusBalace.isHidden       = false
        citrusSeparator.isHidden    = true
        citrusRadio.isHidden        = false
        
        citrusView.isHidden         = false
        mobikwikView.isHidden       = false
        freechargeView.isHidden     = false
    }
    
    func hideCitrusWallet(){
        
        citrusLogo.isHidden         = true
        citrusBalace.isHidden       = true
        freechargeView.isHidden     = true
        citrusRadio.isHidden        = true
        
        citrusSeparator.isHidden    = true
        mobikwikView.isHidden       = true
    }
}

