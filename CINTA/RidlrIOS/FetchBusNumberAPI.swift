//
//  FetchBusNumberAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 08/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusNumberAPI: UIViewController {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TimetableStationViewController, searchString : String){
        
        let cityName = uiFun.readFromDatabase(entityName: "Users", key: "city")
        let url = "\(constant.TIMETABLE_BUS_NUMBER)\(cityName.lowercased())/bus/_search?size=20"
        
        print("Timetable Bus Number Fetching URL \(url)")
        
        util.APICall(apiUrl: url, jsonString: fetchBusNumberJSON(searchString: searchString)!, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.FetchBusNumberResponse(response: jsonResponse)
                print("Successfully Bus Number Fetching Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Fetching Bus Number Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    
    func fetchBusNumberJSON(searchString : String) -> String?{
        
        let fieldsArray:NSMutableArray = NSMutableArray()
        fieldsArray.add("agency_id")
        fieldsArray.add("custom_route_id")
        fieldsArray.add("destination_stop_id")
        fieldsArray.add("destination_stop_name")
        fieldsArray.add("full_long_name")
        fieldsArray.add("route_long_name")
        fieldsArray.add("route_short_name")
        fieldsArray.add("source_stop_id")
        fieldsArray.add("source_stop_name")
        fieldsArray.add("$change")
        fieldsArray.add("serialVersionUID")
        
        let sortArray:NSMutableArray = NSMutableArray()
        let sortDic:NSMutableDictionary = NSMutableDictionary()
        sortDic.setValue("asc", forKey: "order")
        
        let wildCardDic:NSMutableDictionary = NSMutableDictionary()
        wildCardDic.setValue("*\(searchString)*", forKey: "l_route_short_name")
        
        let wildCard : NSMutableDictionary = NSMutableDictionary()
        wildCard.setValue(wildCardDic, forKey: "wildcard")
        
        let mustArray:NSMutableArray = NSMutableArray()
        mustArray.add(wildCard)
        
        let boolDic:NSMutableDictionary = NSMutableDictionary()
        boolDic.setValue(mustArray, forKey: "must")
        
        let queryDic:NSMutableDictionary = NSMutableDictionary()
        queryDic.setValue(boolDic, forKey: "bool")
        
        let sort:NSMutableDictionary = NSMutableDictionary()
        sort.setValue(sortDic, forKey: "route_short_name")
        sortArray.add(sort)
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(fieldsArray, forKey: "fields")
        para.setValue(sortArray, forKey: "sort")
        para.setValue(queryDic, forKey: "query")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("fetch bus number json string = \(jsonString)")   // json string
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
}
