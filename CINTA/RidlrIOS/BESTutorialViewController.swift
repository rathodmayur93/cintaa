//
//  BESTutorialViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 27/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BESTutorialViewController: UIViewController {

    @IBOutlet var closeIV: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // When user click on BEST pass for the first time this screen will be shown to user
        
        uiFun.createCleverTapEvent(message: "BEST Tutorial Screen")
        self.view.backgroundColor   = UIColor.clear
        let blurEffect              = UIBlurEffect(style: .light)
        let blurEffectView          = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame        = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "RenewBusPassViewController", fromController: self)
    }
}
