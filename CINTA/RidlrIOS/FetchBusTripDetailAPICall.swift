//
//  FetchBusTripDetailAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusTripDetailAPICall{
    
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TimetableMapViewController, jsonString : String, agencyId : String, routeId : String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/trip/agencyid/\(agencyId)/routeid/\(routeId)/tripid/-9/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Trips Detail Fetching URL \(url)")
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.FetchTripDetailResponse(response: jsonResponse)
                print("Successfully Timetable Trips Details Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable Trips Details Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
}
