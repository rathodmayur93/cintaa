//
//  LocationViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 26/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation

class LocationViewController: UIViewController, CLLocationManagerDelegate{

    @IBOutlet var myLocationLabel           : UILabel!
    @IBOutlet var chooseLocationOnMapLabel  : UILabel!
    
    @IBOutlet var recentSearchLabel         : UILabel!
    
    var resultsViewController               : GMSAutocompleteResultsViewController?
    var searchController                    : UISearchController?
    var resultView                          : UITextView?
    var backButtonView                      : UIButton?
    
    let geocoder                = GMSGeocoder()
    var locationManager         = CLLocationManager()
    let uiFun                   = UiUtillity()
    let ridlrColors             = RidlrColors()
    var starSubLocallity        = ""
    var destinationSubLocality  = ""
    
        
    @IBOutlet var backButton: UIButton!
    override func viewDidLoad() {
        
        super.viewDidLoad()

        /*********** applying google location search bar funcionlities ***************/
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController                        = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater  = resultsViewController
        
        // craeting search bar
        let subView = UIView(frame: CGRect(x: 36.0, y: 20.0, width: (self.view.frame.width - 36), height: 45.0))
        navigationController?.navigationBar.isTranslucent       = false
        searchController?.hidesNavigationBarDuringPresentation  = false
        
        // creating back button
        backButtonView?.frame = CGRect(x: 8, y: 8, width: 36, height: 36)
        let btnImage          = UIImage(named: "ic_indicator_go_back_holo")
        backButtonView?.setImage(btnImage, for: .normal)
        
        // This makes the view area include the nav bar even though it is opaque.
        // Adjust the view placement down.
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .top
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        setUI()
        
        uiFun.createCleverTapEvent(message: "FIND A ROUTE SELECT LOCATION USING MAP OR SEARCH BAR SCREEN")
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
        
        /*************  SEARCH BUTTON ON TOP ****************/
        backButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        searchController?.searchBar.barTintColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        searchController?.searchBar.tintColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        searchController?.searchBar.placeholder = "Enter a location in Mumbai"
        searchController?.searchBar.barStyle = UIBarStyle.default
        
        if(searchController?.searchBar.isSearchResultsButtonSelected)!{
            print("Search bar is selected...!!!")
        }
        
        // When user clicks on the chooseLocationOnMapLabel
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectLocationOnMap))
        tapGesture.numberOfTapsRequired = 1
        chooseLocationOnMapLabel.isUserInteractionEnabled = true
        chooseLocationOnMapLabel.addGestureRecognizer(tapGesture)
        
        // When user clicks on the myLocationLabel
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(selectMyLocation))
        tapGesture1.numberOfTapsRequired = 1
        myLocationLabel.isUserInteractionEnabled = true
        myLocationLabel.addGestureRecognizer(tapGesture1)
        
        // When user clicks on the backButtonView
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(backButtonAction))
        tapGesture2.numberOfTapsRequired = 1
        backButtonView?.isUserInteractionEnabled = true
        backButtonView?.addGestureRecognizer(tapGesture2)
        
        // requesting for location
        locationManager.delegate            = self
        locationManager.desiredAccuracy     = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // fetching location response
        let userLocation: CLLocation = locations[0]
        
        let latitude    = userLocation.coordinate.latitude
        let longitude = userLocation.coordinate.longitude
        
        // Storing data into the parmanent storage
        uiFun.writeData(value: String(describing :latitude), key: "lat")
        uiFun.writeData(value: String(describing :longitude), key: "long")
        
        // google reverse geo-coding json response
        geocoder.reverseGeocodeCoordinate(userLocation.coordinate) { (response, error) in
            let jsonResponse            = response?.results()
            self.myLocationLabel.text   = jsonResponse?[0].lines?[0]
        }
        
        plannerStartingLat      = latitude
        plannerStartingLong     = longitude
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = false

    }
    
    func selectMyLocation(){
        // when user clicks on the  my location label
        if(plannerFromWhere == "start"){
            myLocation                      = myLocationLabel.text!
            plannerStartingStationPlaceId   = "null"
            let findSublocality             = myLocation.components(separatedBy: ",")
            plannerStartingSubLocality      = findSublocality[findSublocality.count - 1]
            
        }else if(plannerFromWhere == "end"){
            plannerDestinationStation = myLocationLabel.text!
            
            let findDestinationSublocality  = plannerDestinationStation.components(separatedBy: ",")
            plannerDestinationSubLocality   = findDestinationSublocality[findDestinationSublocality.count - 1]
            plannerDestinationPlaceId       = "null"
            navigateToPlannerResult         = true
        }
        
        uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
    }
    
    func selectLocationOnMap(){
        // when user click on select location from map it will open map ChooseStartingPointViewController
        uiFun.navigateToScreem(identifierName: "ChooseStartingPointViewController", fromController: self)
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        // when user clicks on the back button
        uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
    }
    
    func backButtonAction(){
        uiFun.navigateToScreem(identifierName: "FindRouteViewController", fromController: self)
    }
    
    
}
// Handle the user's selection.
extension LocationViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place Sublocality : \(place.placeID)")
        
        
        if(plannerFromWhere == "start"){
            myLocation                      = place.formattedAddress!
            plannerStartingSubLocality      = place.formattedAddress!
            plannerStartingStationPlaceId   = place.placeID
            plannerStartingLat              = place.coordinate.latitude
            plannerStartingLong             = place.coordinate.longitude
            
            print("Starting lat long \(plannerStartingLat) \(plannerStartingLong)")
            
        }else if(plannerFromWhere == "end"){
            navigateToPlannerResult         = true
            plannerDestinationStation       = place.formattedAddress!
            plannerDestinationSubLocality   = place.formattedAddress!
            plannerDestinationPlaceId       = place.placeID
            plannerDestinationLat           = place.coordinate.latitude
            plannerDestinationLong          = place.coordinate.longitude
            
            print("Destinatio lat long \(plannerStartingLat) \(plannerStartingLong)")
        }
        
        uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
