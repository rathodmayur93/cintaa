//
//  CalendarParse.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var calendarDataList = [CalendarModel]()
class CalendarParse{
    
    let uiFun = UiUtillity()
    
    func demoReadFromCSV(){
        
        let queueName = DispatchQueue(label: "data.calendar.processing")
        
        queueName.async {
            let csvContent = self.uiFun.readFromCSV(fileName: "splashCalendarData")
            let csvNewLineContent = csvContent.components(separatedBy: .newlines)
            let delimiter = ","
            
            let lines:[String] = csvNewLineContent
            
            for line in lines {
                
                let calendarModel       = CalendarModel()
                var calendarData        = line.components(separatedBy: delimiter)
                
                if(calendarData[0] != ""){
                    
                    let calendarDataModel = CalendarData()
                    
                    calendarDataModel.calendarId        = calendarData[0]
                    calendarDataModel.calendarDays      = calendarData[1]
                    calendarDataModel.calenarUnknown    = calendarData[2]
                    calendarDataModel.monday            = calendarData[3]
                    calendarDataModel.tuesday           = calendarData[4]
                    calendarDataModel.wednesday         = calendarData[5]
                    calendarDataModel.thursday          = calendarData[6]
                    calendarDataModel.friday            = calendarData[7]
                    calendarDataModel.saturday          = calendarData[8]
                    calendarDataModel.sunday            = calendarData[9]
                    
                    calendarModel.calendarData.append(calendarDataModel)
                    calendarDataList.append(calendarModel)
                    
                }
            }
        }
    }
}
