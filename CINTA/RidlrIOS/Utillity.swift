//
//  Utillity.swift
//  RidlrIOS
//
//  Created by Mayur on 03/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class Utillity: UIViewController, URLSessionDelegate{

    let constant        = Constants()
    let uiFun           = UiUtillity()
    var ridlrToken      = ""
    var errorText       = "Unidentified error"
    
    let container: UIView = UIView()
    var task    : URLSessionDataTask!
    
    override func viewDidLoad() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    
    func APICall(apiUrl:String, jsonString: String, requestType:String, header: Bool, completionHandler: @escaping (JSON?, NSError?) -> Void ){
        // post data api ca;;
        if(uiFun.isConnectedToNetwork()){
            let url = NSURL(string: apiUrl)!        // api url
            print("internet connection")
            if((apiUrl.range(of: "traffline-api")) != nil || (apiUrl.range(of: "Newsfeed.saarath.com") != nil)){
        
            }else{
                ridlrToken = uiFun.readData(key: "ridlrToken")
                print("Ridlr Token Add Money \(ridlrToken)")
            }
        
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = requestType
        
            if(header){
                // header for apu call
                request.setValue(ridlrToken, forHTTPHeaderField: "ridlr-token")
                request.setValue("A*S)(CR@!T^ST%%C**!l@N#T$I", forHTTPHeaderField: "apk_key")
                request.setValue("1479196020161", forHTTPHeaderField: "X-SESSION-ID")
                request.setValue(constant.app_version_name, forHTTPHeaderField: "app_version_name")
                request.setValue("iOS", forHTTPHeaderField: "Platform")
            }
        
            if((apiUrl.range(of: "Route/TravelTime")) != nil){
                //request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.setValue(constant.ACCESS_TYPE, forHTTPHeaderField: "accessType")
                request.setValue("ridlr", forHTTPHeaderField: "groupId")
                request.setValue(constant.CUST_KEY, forHTTPHeaderField: "custKey")
                request.setValue("MDAwMDAwMDAwMDAwMDAw", forHTTPHeaderField: "passKey")
                request.setValue("iOS", forHTTPHeaderField: "Platform")
            }else{
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            }
        
            if(requestType == "GET"){
            }
            else if(requestType == "POST"){
        
                request.httpBody = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true)
                print("BODY REQUEST" + String(describing: request.httpBody))
            }
        
            task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error != nil{
                // api calling fail errors
                if(error.debugDescription.contains("timed out")){
                    let timeouJSON = self.SendCompletionHandler(errorMessage: "Request Timed Out")
                    completionHandler(JSON(timeouJSON), nil)
                }else if(error.debugDescription.contains("connection was lost")){
                    let timeouJSON = self.SendCompletionHandler(errorMessage: "The network connection was lost.")
                    completionHandler(JSON(timeouJSON), nil)
                }else if(error.debugDescription.contains("appears to be offline")){
                    let timeouJSON = self.SendCompletionHandler(errorMessage: "The Internet connection appears to be offline.")
                    completionHandler(JSON(timeouJSON), nil)
                }else if(error.debugDescription.contains("not connect to")){
                    self.uiFun.showAlert(title: "", message: "Could not connect to the server", logMessage: "Payment API Error", fromController: self)
                }
                
                print("Error Debug \(error.debugDescription)")
                //self.uiFun.hideIndicatorLoader()
                print("Certificate Error -> \(error!)")
                return
            }else{
                
                if let urlContent = data{
                    do {
                        // response of api
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        print("Yo Yo \(jsonResult)")
                        print("Yo Yo \(JSON(jsonResult["code"]))")
                        
                        DispatchQueue.main.sync(execute: {
                            
                            if(JSON(jsonResult["status"]) == "OK"){
                                // parse the response to respective api calling function
                                if(error.debugDescription.contains("timed out")){
                                    let timeouJSON = self.SendCompletionHandler(errorMessage: "Request Timed Out")
                                    completionHandler(timeouJSON, nil)
                                }
                                
                                completionHandler(JSON(jsonResult), nil)
                                
                            }else if (JSON(jsonResult["status"]) == "5000"){
                                completionHandler(JSON(jsonResult), nil)
                            }
                            else if((apiUrl.range(of: "traffline-api")) != nil){
                                completionHandler(JSON(jsonResult), nil)
                            }else if((apiUrl.range(of: "ridlr.in")) != nil){
                                completionHandler(JSON(jsonResult), nil)
                            }else if(apiUrl.range(of: "Route/TravelTime") != nil){
                                completionHandler(JSON(jsonResult), nil)
                            }else if(JSON(jsonResult["pgRespCode"]) == "125" || JSON(jsonResult["pgRespCode"]) == "111"){
                                let errorMesasge = JSON(jsonResult["txMsg"]!!)
                                let errorJson = self.SendCompletionHandler(errorMessage: String(describing : errorMesasge))
                                completionHandler(errorJson, nil)
                            }else{
                                    let statusCode      = String(describing : JSON(jsonResult["status"]))
                                
                                    let ridlrErrors     = RidlrException()
                                    self.errorText      = ridlrErrors.RidlrError(errorCode: Int(statusCode)!)
                                    let jsonString      = self.SendCompletionHandler(errorMessage: self.errorText)
                                    completionHandler(jsonString, nil)
                                
                                    var jsonResponse = JSON(jsonResult)
                                    let jsonError = jsonResponse["error"]
                                    if(JSON(jsonError["errorCodeText"]) == "CITRUSPAY-SAVED_CARD Unable to connect PG, " ){
                                        completionHandler(JSON(jsonResult), nil)
                                    }
                                completionHandler(JSON(jsonResult), nil)
                                return
                            }
                        })
                        
                    }
                    catch{
                        print("Failed To Convert JSON")
                    
                        let jsonString = self.SendCompletionHandler(errorMessage: RidlrError.JSON_FAIL.rawValue)
                        completionHandler(jsonString, nil)
                    }
                }
            }
           
        }
        
            task.resume()
        }else{
            
            
            let jsonString = SendCompletionHandler(errorMessage: "No Internet Connection")
            completionHandler(jsonString, nil)
        }
        
    }
    
    func SendCompletionHandler(errorMessage : String) -> JSON{
        // if some error occured it will send the json response with error
        errorText                               = errorMessage
    
        let para      : NSMutableDictionary     = NSMutableDictionary()
        let errorDic  : NSMutableDictionary     = NSMutableDictionary()
        
        errorDic.setValue(errorText, forKey: "errorCodeText")
        
        para.setValue("fail", forKey: "status")
        para.setValue(errorText, forKey: "errorCodeText")
        
        let jsonData: NSData
        do{
            jsonData       = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString      = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            let jsonParseString = JSON(jsonString)
            print("no internet json string = \(jsonParseString)")
            return jsonParseString
        } catch _ {
            print ("UH OOO")
            
            }
        return nil
    }
    
    /**************************** GET REQUEST API CALL *****************************/
    func GetAPICall(apiUrl:String, completionHandler: @escaping (JSON?, NSError?) -> Void ){
        
        if(uiFun.isConnectedToNetwork()){
            let url = NSURL(string: apiUrl)!            /// api call
        
            // fetching ridlr token from parmanent storage
            if UserDefaults.standard.object(forKey: "ridlrToken") != nil{
                let ridlrToken = uiFun.readData(key: "ridlrToken")
                print("Ridlr Token Add Money \(ridlrToken)")
            }
        
        
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "GET"
                // headers
                request.setValue("device", forHTTPHeaderField: "accessType")
                request.setValue("g2apps", forHTTPHeaderField: "groupId")
                request.setValue("f29a3c79212611e3aeb927d5ba248633", forHTTPHeaderField: "custKey")
                request.setValue("NjI2RTQyMDEtMDk0RC00MUM5LTlDM0YtMEQwRTEyRDg1Rjg1fDFGMTBENDg1LTQ5NUQtNEQxOC1CMTlELTcxQjEyM0FFRDE3OA==", forHTTPHeaderField: "passKey")
                request.setValue("iOS", forHTTPHeaderField: "Platform")
        
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
                if error != nil{
                    // api calling errors
                    if(error.debugDescription.contains("timed out")){
                        let timeouJSON = self.SendCompletionHandler(errorMessage: "Request Timed Out")
                        completionHandler(timeouJSON, nil)
                    }else if(error.debugDescription.contains("connection was lost")){
                        let timeouJSON = self.SendCompletionHandler(errorMessage: "The network connection was lost.")
                        completionHandler(timeouJSON, nil)
                    }else if(error.debugDescription.contains("appears to be offline")){
                        let timeouJSON = self.SendCompletionHandler(errorMessage: "The Internet connection appears to be offline.")
                        completionHandler(timeouJSON, nil)
                    }else if(error.debugDescription.contains("not connect to")){
                        self.uiFun.showAlert(title: "", message: "Could not connect to the server", logMessage: "Payment API Error", fromController: self)
                    }
                    
                    print("Certificate Error -> \(error!)")
                    return
                }else{
                
                    if let urlContent = data{
                    
                        do {
                            // api response
                            let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                                print("Yo Yo \(jsonResult)")
                                DispatchQueue.main.sync(execute: {
                                print("Completion Of Timetable Stops")
                                completionHandler(JSON(jsonResult), nil)
                            
                                })
                        
                            }
                        catch{
                            print("Failed To Convert JSON")
                            
                            let jsonString = self.SendCompletionHandler(errorMessage: RidlrError.JSON_FAIL.rawValue)
                            completionHandler(jsonString, nil)
                        }
                    }
                }
            
            }
        task.resume()
        }else{
            // if there is no json response
            let jsonString = SendCompletionHandler(errorMessage: "No Internet Connection")
            completionHandler(jsonString, nil)
        }
        
    }
    
    /***************************** GET REQUEST WITH URL ENCODING WITH PARAMS ********************/
    
    func sendRequest(url: String, parameters: [String : AnyObject], completionHandler: @escaping (JSON?, NSError?) -> Void) {
        
        if(uiFun.isConnectedToNetwork()){
            let parameterString = parameters.stringFromHttpParameters()
            let requestURL = URL(string:"\(url)?\(parameterString)")!       // api url
            
            // fetching ridlr-token from parmanent storage
            ridlrToken = uiFun.readData(key: "ridlrToken")
        
            print("NewsFeed Specific Update Reply URL \(requestURL)")
        
            var request = URLRequest(url: requestURL)
            request.httpMethod = "GET"
            request.setValue(ridlrToken, forHTTPHeaderField: "ridlr-token")
        
            let task1 = URLSession.shared.dataTask(with: request) { (data, response, error) in
                // api calling errors
                if error != nil{
                    if(error.debugDescription.contains("timed out")){
                        let timeouJSON = self.SendCompletionHandler(errorMessage: "Request Timed Out")
                        completionHandler(timeouJSON, nil)
                    }else if(error.debugDescription.contains("connection was lost")){
                        let timeouJSON = self.SendCompletionHandler(errorMessage: "The network connection was lost.")
                        completionHandler(timeouJSON, nil)
                    }else if(error.debugDescription.contains("appears to be offline")){
                        let timeouJSON = self.SendCompletionHandler(errorMessage: "The Internet connection appears to be offline.")
                        completionHandler(timeouJSON, nil)
                    }
                    
                    print("Certificate Error -> \(error!)")
                    return
                }else{
                    // api json response
                    if let urlContent = data{
                    
                        do {
                        
                            let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                            print("Timeline Specific Update Upload Pic Reply \(jsonResult)")
                            
                            DispatchQueue.main.sync(execute: {
                                completionHandler(JSON(jsonResult), nil)
                            })
                        }
                    catch{
                        print("Failed To Convert JSON")
                        let jsonString = self.SendCompletionHandler(errorMessage: RidlrError.JSON_FAIL.rawValue)
                        completionHandler(jsonString, nil)
                    }
                }
            }
        }
        task1.resume()
        }else{
            let jsonString = SendCompletionHandler(errorMessage: "No Internet Connection")
            completionHandler(jsonString, nil)
        }
    }



func uploadTimlinePicRequest(url: String, parameters: [String : AnyObject], completionHandler: @escaping (JSON?, NSError?) -> Void) {
    // news feed upload pic url
    if(uiFun.isConnectedToNetwork()){
        let parameterString = parameters.stringFromHttpParameters()
        let requestURL = URL(string:"\(url)?\(parameterString)")!
    
        var request = URLRequest(url: requestURL)
        request.httpMethod = "POST"
    
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.setValue("iOS", forHTTPHeaderField: "Platform")
    
        let task1 = URLSession.shared.dataTask(with: request) { (data, response, error) in
        
            if error != nil{
                // api calling errors
                if(error.debugDescription.contains("timed out")){
                    let timeouJSON = self.SendCompletionHandler(errorMessage: "Request Timed Out")
                    completionHandler(timeouJSON, nil)
                }else if(error.debugDescription.contains("connection was lost")){
                    let timeouJSON = self.SendCompletionHandler(errorMessage: "The network connection was lost.")
                    completionHandler(timeouJSON, nil)
                }else if(error.debugDescription.contains("appears to be offline")){
                    let timeouJSON = self.SendCompletionHandler(errorMessage: "The Internet connection appears to be offline.")
                    completionHandler(timeouJSON, nil)
                }else if(error.debugDescription.contains("not connect to")){
                    self.uiFun.showAlert(title: "", message: "Could not connect to the server", logMessage: "Payment API Error", fromController: self)
                }
                
                print("Certificate Error -> \(error!)")
                return
            }else{
                if let urlContent = data{
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        print("Timeline Specific Update Upload Pic Reply \(jsonResult)")
                        DispatchQueue.main.sync(execute: {
                            completionHandler(JSON(jsonResult), nil)
                        })
                    
                    }
                catch{
                    let jsonString = self.SendCompletionHandler(errorMessage: RidlrError.JSON_FAIL.rawValue)
                    completionHandler(jsonString, nil)
                    print("Failed To Convert JSON")
                }
            }
        }
        
    }
    task1.resume()
    }else{
        let jsonString = SendCompletionHandler(errorMessage: "No Internet Connection")
        completionHandler(jsonString, nil)
    }
  }
}

extension UITextField {
    func setPreferences(cornerRadius : Int, color : UIColor, borderWidth : Int) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = CGFloat(borderWidth)
        // etc.
    }
}

