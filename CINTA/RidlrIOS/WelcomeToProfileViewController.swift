//
//  WelcomeToProfileViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 09/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class WelcomeToProfileViewController: UIViewController, UIGestureRecognizerDelegate{

    var uiFun = UiUtillity()
    var ridlrColors = RidlrColors()
    
    @IBOutlet var positiveResponseBT: UIButton!
    @IBOutlet var textFieldContent: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("/***** PROFILE WELCOME SCREEN *******/")
        setUI()
        navigationController?.interactivePopGestureRecognizer?.delegate         = self
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled   = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate    = nil
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate    = self
        self.navigationController?.navigationBar.isHidden                       = true
        
        uiFun.createCleverTapEvent(message: "PROFILE WELCOME SCREEN")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.delegate    = self
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        self.view.backgroundColor           = UIColor.clear
        positiveResponseBT.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        positiveResponseBT.backgroundColor  = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        
        let blurEffect          = UIBlurEffect(style: .extraLight)
        let blurEffectView      = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame    = self.view.frame
        
        self.view.insertSubview(blurEffectView, at: 0)

    }
  
    @IBAction func PositiveResponseAction(_ sender: AnyObject) {
        // Create profile button action
        if let createProfileViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreateProfileViewController") as? CreateProfileViewController {
            if let navigator = navigationController {
                navigator.pushViewController(createProfileViewController, animated: true)
            }
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: AnyObject) {
        // cross button (on top left corner) action
        if(profileBackNavigation != ""){
            uiFun.navigateToScreem(identifierName: profileBackNavigation, fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        }
    }
}
