//
//  BusStopListModel.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BusStopListModel{

    var routeId         = ""
    var shortname       = ""
    var longname        = ""
    var directionid     = ""
    var directionlabel  = ""
    var stop            = [BusStops]()

}

class BusStops{

    var id   = ""
    var name = ""
    var lon  = ""
    var lat  = ""
}
