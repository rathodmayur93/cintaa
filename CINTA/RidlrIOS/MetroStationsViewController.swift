//
//  MetroStationsViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 05/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var metroTokenStopList           = [StopsModel]()

var metroTokenStopNameList       = [String]()
class MetroStationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet var searchBar         : UISearchBar!
    @IBOutlet var startStopLabel    : UILabel!
    @IBOutlet var table             : UITableView!
    @IBOutlet var headerLabel       : UILabel!
    
    var stationNamesArray           = [String]()
    var tempShowArray               = [String]()
    var filterStationNames:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate  = self
        // api call commented
//        if(metroStopList.count == 0){
//            uiFun.showIndicatorLoader()
//            
//            // removing previous stored data from list
//            metroTokenStopList.removeAll()
//            tempShowArray.removeAll()
//            
//            // making an api call to fetch stop name
//            let metroStopsApi = FetchMetroStops()
//            metroStopsApi.makeAPICall(fromViewController: self)
//       
//        }else{
//            tempShowArray = metroTokenStopNameList
//        }
        
        tempShowArray = ["Versova", "D N Nagar", "Azad Nagar", "Andheri", "Western Express Highway", "Chakala", "Airport Road"
            , "Marol Naka", "Sakinaka", "Ashalfa", "Jagruti Nagar", "Ghatkopar"]
        
        stationNamesArray = tempShowArray
        
        //changing the haeder value
        if(isOriginStationSelected){
            headerLabel.text    = "Destination Station"
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    /*********** ASSIGNING TABLE PROPERTIES **************/
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return tempShowArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell                = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text    = tempShowArray[indexPath.row];
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    
        switch metroFromWhere {
        case "token":
            if(isOriginStationSelected){
                tokenOriginStationName      = tempShowArray[indexPath.row]
                tokenOriginStationAgencyId  = indexPath.row + 1 //metroTokenStopList[0].stops[indexPath.row].agencyId[0].agencyId
                uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
                print("Origin Station name \(tokenOriginStationName) and agency id \(tokenOriginStationAgencyId)")
            }else{
                tokenDestinationStationName = tempShowArray[indexPath.row]
                tokenDestinationAgencyId    = indexPath.row + 1 //metroTokenStopList[0].stops[indexPath.row].agencyId[0].agencyId
                uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
                print("Destination Station name \(tokenDestinationStationName) and agency id \(tokenDestinationAgencyId)")
            }
            break
       
        case "trip":
            if(isTripOriginStationSelected){
                tripOriginStation           = tempShowArray[indexPath.row]
                tripOriginStationAgencyId   = indexPath.row + 1
                uiFun.navigateToScreem(identifierName: "MetroTripViewController", fromController: self)
                print("Trip Origin Station name \(tripOriginStation) and agency id \(tripOriginStationAgencyId)")
            }else{
                
                tripDestinationStation          = tempShowArray[indexPath.row]
                tripDestinationStationAgencyId  = indexPath.row + 1
                uiFun.navigateToScreem(identifierName: "MetroTripViewController", fromController: self)
                print("Trip Destination Station name \(tripDestinationStation) and agency id \(tripDestinationStationAgencyId)")
            }
            break
        case "penalty":
            penaltyStation      = tempShowArray[indexPath.row]
            penaltyStationId    = "\(indexPath.row + 1)"
            uiFun.navigateToScreem(identifierName: "PenaltyIssueViewController", fromController: self)
            print("Trip Penalty Station is \(penaltyStation)")
            break
       
        default:
            break
        }
    }
    
    // When user search something in search bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterStationNames = searchText.isEmpty ? stationNamesArray : stationNamesArray.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        print("Filter station names \(filterStationNames)")
        self.tempShowArray = filterStationNames
        table.reloadData()  // reloading tableview with filtered data
    }
    
    // Fetch Metro Stop API Response
    func gotMetroStops(response : JSON){
        uiFun.hideIndicatorLoader()
        processStopModel(response: response)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        switch metroFromWhere {
        case "token":
            uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
            break
        case "trip":
            uiFun.navigateToScreem(identifierName: "MetroTripViewController", fromController: self)
            break
        case "penalty":
            uiFun.navigateToScreem(identifierName: "PenaltyIssueViewController", fromController: self)
            break
        default:
            break
        }
    }
    
    
    
    func processStopModel(response : JSON){
        // Processing json response into model
       
        var i               = 0
        let stopsJSON       = response["stop"]
        print("Metro Token Stop JSON Count is \(stopsJSON.count)")
        
        let stopModel       = StopsModel()
        
        while(i < stopsJSON.count){
            
            let stops       = Stops()
            
            stops.name      = String(describing: stopsJSON.arrayValue[i]["name"])
            stops.lat       = String(describing: stopsJSON.arrayValue[i]["lat"])
            stops.long      = String(describing: stopsJSON.arrayValue[i]["lon"])
            
            stationNamesArray.append(String(describing: stopsJSON.arrayValue[i]["name"]))
            tempShowArray   = stationNamesArray
            
            var j = 0
            let agencyJSON  = stopsJSON.arrayValue[i]["agencyid"]
            
            while(j < agencyJSON.count){
                
                let agencyId        = AgencyId()
                agencyId.agencyId   = String(describing: agencyJSON.arrayValue[j])
                stops.agencyId.append(agencyId)
                j += 1
                
            }
            
            stopModel.stops.append(stops)
            metroTokenStopList.append(stopModel)     // StopList arrayList
            i += 1
        }
        metroTokenStopNameList  = tempShowArray // Saving stop list into temp varibale so we can use the list without making an api call
        table.reloadData()                      // Reloading table view with stop list
        
    }
}
