//
//  CardPaymentAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 14/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CardPaymentAPICall {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()

    func makeAPICall(fromViewController : PaymentViewController, jsonString : String){
        
        let url = "\(constant.STAGING_BASE_URL)/payment"    // Card Payment API URL
        print("Payment Option Fetcing Balance URL \(url)")
        
        util.APICall(apiUrl: url, jsonString: "", requestType: "GET", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.gotResponseFromArhitecture(response: jsonResponse)
                print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
                
            }else {
                
                
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }

}
