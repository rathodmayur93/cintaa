//
//  MobiFreeWalletAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 06/06/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MobiFreeWalletAPICall {

    var util        = Utillity()
    var constant    = Constants()
    var uiFun       = UiUtillity()
    
    
    func makeAPICall(fromViewController : PaymentViewController, amount : String, paymentaGateway : String){
        
        // Payment Using Mobikwik and freecharge wallet api call
        let url = "\(constant.STAGING_BASE_URL)/payment/addMoney" //"\(constant.STAGING_BASE_URL)/payment/addMoney"
        print("Adding Money In \(paymentaGateway) Wallet Part 1 \(url)")
        
        util.APICall(apiUrl: url, jsonString: citrusAddMoneyJSONString(amount: amount, paymentGateway: paymentaGateway)!, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                fromViewController.MobiFreeWalletResponse(response: jsonResponse)
                print("Successfully Adding Money In \(paymentaGateway) Wallet Part 1 \(jsonResponse) ")
                return
            }else {
                print("Adding Money In \(paymentaGateway) Wallet Part 1 Failed....Try Harder Mayur ")
                return
            }
        }
    }
    
    func makeAPICallProfile(fromViewController : AddMoneyIntoWalletViewController, amount : String, paymentGateway : String){
        // Add Money Into Mobikwika and freecharge api call
        let url = "\(constant.STAGING_BASE_URL)/payment/addMoney" //"\(constant.STAGING_BASE_URL)/payment/addMoney"
        print("Adding Money In \(paymentGateway) Wallet \(url)")
        
        util.APICall(apiUrl: url, jsonString: citrusAddMoneyJSONString(amount: amount, paymentGateway: paymentGateway)!, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                fromViewController.MobiFreeWalletResponse(response: jsonResponse)
                print("Successfully Adding Money In \(paymentGateway) Wallet Part 1 \(jsonResponse) ")
                return
            }else {
                print("Adding Money In \(paymentGateway) Wallet Part 1 Failed....Try Harder Mayur ")
                return
            }
        }
    }

    func citrusAddMoneyJSONString(amount : String, paymentGateway : String) -> String?{
     
        // JSON body for  mobikwik and freecharge wallet payment
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue(amount, forKey: "amount")
        para1.setValue(paymentGateway, forKey: "paymentGateway") // Either Freecharge or mobikwik
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(para1, forKey: "paymentDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("\(paymentGateway) add money json string part 1= \(jsonString)") // JSoN string for wallet payment
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

}
