//
//  FetchBusNumberMapApiCall.swift
//  RidlrIOS
//
//  Created by Mayur on 10/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusNumberMapApiCall{
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TimetableMapViewController, agencyId : String, routeId : String, tripId : String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/trip/agencyid/\(agencyId)/routeid/\(routeId)/tripid/\(tripId)/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Bus Map Fetch Stops URL \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.BusNumberMapAPIResponse(response: jsonResponse)
                print("Successfully Timetable BUS Mop Data fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable BUS Trip fetching failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }

}
