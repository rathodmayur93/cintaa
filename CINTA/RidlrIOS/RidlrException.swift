//
//  RidlrException.swift
//  RidlrIOS
//
//  Created by Mayur on 10/05/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit


enum RidlrError : String{

    case JSON_FAIL = "Error"
}
class RidlrException {

    /**
     * This enum contains all the error codes sent using response generation.
     * Error naming convention
     * <p>5000 - Success</p>
     * <p>5001 - 5999 Business errors aka ridlr errors</p>
     * <p>6001 - 6999 Payment Gateway Errors</p>
     * <p>7001 onwards Agency series -
     * <ul>
     * <li>7001 to 7099 BEST</li>
     * <li>7101 to 7299 Metro</li>
     * <li>7201 to 7399 Nmmt</li>
     * </ul></p>
     * <p><i>Note - All soft errors will start from one and will go on INCREAMENTING.
     * All hard errors will start at last number and will go on DECREMENTING.
     * </i></p>
     */
    
    
    func RidlrError (errorCode : Int) ->String{
    
        if(errorCode < 6000){
            let backendError = BackendError(errorCode: errorCode)
            return backendError
        }else if(errorCode > 6000 && errorCode < 7000){
            let frontendError = PaymentGatewayError(erroCode: errorCode)
            return frontendError
        }else if(errorCode > 6000){
            let agencyError = PaymentGatewayError(erroCode: errorCode)
            return agencyError
        }
        return "Unidentified error"
    }
    
    
    func PaymentGatewayError(erroCode : Int) -> String{
    
        switch erroCode {
        case 6001:
            return "invalid_credentials"
        case 6002:
            return "account_locked"
        case 6003:
            return "invalid_access_token"
        case 6004:
            return "invalid_refresh_token"
        case 6005:
            return "Generic error"
        case 6006:
            return "Invalid grant"
        case 6007:
            return "return template error"
        case 6008:
            return "Invalid amount"
        case 6009:
            return "Invalid payment gateway"
        case 6010:
            return "Invalid payment mode"
        case 6011:
            return "Failed to generate signature"
        case 6012:
            return "Payment failed"
        case 6013:
            return "Payment Failed due to low balance"
        case 6014:
            return "Signature mismatch"
        case 6015:
            return "PG response doesn't have required key"
        case 6016:
            return "PG option is not supported"
        case 6017:
            return "Selected PG is not configured for user"
        case 6998:
            return "Request key cannot be generated for add money ExcessTransactionLimit"
        case 6999:
            return "unable_to_connect_to_pg"
        default:
            return "Unidentified Error"
        }
    }
    
        
    func AgencyError(errorCode : Int) -> String{
        /*Agency exception*/
        switch errorCode {
        case 7001:
            return "Invalid card number"
        case 7002:
            return "Recharge amount is not valid"
        case 7003:
            return "Validation pending"
        case 7004:
            return "Previous reharge is pending"
        case 7005:
            return "Max recharge amount reached"
        case 7006:
            return "Ageny not found"
        case 7008:
            return "Some error occurred while connecting to agency"
        case 7009:
            return "Item validation with company failed"
        case 7010:
            return "Invalid action against company item"
        case 7011:
            return "Pass cannot be renewed"
        case 7012:
            return "Card cannot be recharged"
        case 7014:
            return "Hi, Your smart card @companyIdentifier is Active and can be renewed only after @date"
        case 7013:
            return "Your last renewal is pending. Kindly wait for the same to get through"
        case 7998:
            return "Agency response is not in expected format"
        case 7999:
            return "Unable to connection to the agency"
        default:
            return "Unidentified error"
        }
    }
    func BackendError(errorCode : Int) -> String{
    
        switch errorCode {
            //Backend Errors
        case 5001:
            return "Bad Request."
        case 5002:
            return "Missing Parameters."
        case 5008:
            return "User not found"
        case 5009:
            return "Operation for given company and item is not supported"
        case 5010:
            return "Invalid parameters"
        case 5011:
            return "Different types of item are not supported against same booking"
        case 5012:
            return "More than one item are not supported against given category"
        case 5020:
            return "Invalid additional data"
        case 5021:
            return "Invalid route details"
        case 5022:
            return "Different types of action are not supported against same booking"
        case 5023:
            return "Invalid parameter booking amount"
        case 5024:
            return "Mismatch in payment details"
        case 5025:
            return "Invalid Booking no"
        case 5026:
            return "Booking already processed"
        case 5027:
            return "Booking's company transaction id is duplicate. Please generate new transaction"
        case 5028:
            return "Incorrect mobile number"
        case 5029:
                return "Invalid otp"
        case 5030:
            return "Otp not sent"
        case 5031:
            return "Successful booking present in the system"
        case 5032:
            return "No successful payment present for this booking"
        case 5999:
            return "Execution failed"
        case 5034:
            return "Your current device is not authorized to use this Pass. Kindly contact customer support"
        case 5035:
            return "Sync data update to date "
        default:
            return "Unidentified Error"

    }
   }
}
