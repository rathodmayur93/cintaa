//
//  BESTPageViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 26/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BESTPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    var pageControl : UIPageControl = UIPageControl()
    // Tutorial Screen Page View COntroller Parent Class
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newPageNameViewController(pageName: "BEST_SUCCESS_01"),    // 1st Tutorial Screen View Controller
                self.newPageNameViewController(pageName: "BEST_SUCCESS_02"),    // 2nd Tutorial Screen View Controller
                self.newPageNameViewController(pageName: "BEST_SUCCESS_03"),    // 3rd Tutorial Screen View Controller
                self.newPageNameViewController(pageName: "BEST_SUCCESS_04")]    // 4th Tutorial Screen View Controller
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        dataSource = self
        
        // Initializing First Screen of Tutorial Page View Controller
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        // Page Control Initializing and its properties
        pageControl = UIPageControl.appearance(whenContainedInInstancesOf: [BESTPageViewController.self])
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.black
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func newPageNameViewController(pageName: String) -> UIViewController {
        // Return the View Controllre depending on the pageName
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(pageName)")
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1 // Previous index of page view controller
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1     // Next index of page view controller
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
    public func presentationCount(for pageViewController: UIPageViewController) -> Int{
        return orderedViewControllers.count // Total Page count of page view controller
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int{
        
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = orderedViewControllers.index(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
}
