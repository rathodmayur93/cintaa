//
//  MonoRoutesParser.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var monoRouteList = [MonoRoutesModel]()
class MonoRoutesParser {

    let uiFun = UiUtillity()
    func parseMonoRoutesCSVToList(){
        
        let queueName = DispatchQueue(label: "routes.mono.processing")
        
        queueName.async {
            
            let csvContent = self.uiFun.readFromCSV(fileName: "MUM Mono_routes")
            let csvNewLineContent = csvContent.components(separatedBy: .newlines)
            let delimiter = ","
            
            let lines:[String] = csvNewLineContent
            
            
            
            
            for line in lines {
                
                let monoRouteParseModel = MonoRoutesModel()
                
                let monoRoutes       = MonoRoute()
                var monoData         = line.components(separatedBy: delimiter)
                
                if(monoData[0] != ""){
                    
                    monoRoutes.routeId = monoData[0]
                    monoRoutes.routeShortName = monoData[1]
                    monoRoutes.routeLongName = monoData[2]
                    monoRoutes.routeDirectitonId = monoData[3]
                    monoRoutes.routeSourceId = monoData[4]
                    monoRoutes.routeDestinationId = monoData[5]
                    
                    monoRouteParseModel.monoRoute.append(monoRoutes)
                    monoRouteList.append(monoRouteParseModel)
                    
                }
                
            }
        }
    }

}
