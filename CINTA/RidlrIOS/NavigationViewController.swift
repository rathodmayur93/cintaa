//
//  NavigationViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 25/10/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreData

var changeCityFrom = ""
var fromBlog       = false

class NavigationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let uiFun       = UiUtillity()
    let ridlrColors = RidlrColors()
    
    @IBOutlet var tableView         : UITableView!
    @IBOutlet var currentCityLabel  : UILabel!
    var navigationDrawerLabel       = [String]()
    var navgationDrawerIcon         = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigation Bar
        let nav                     = self.navigationController?.navigationBar
        nav?.barStyle               = UIBarStyle.blackOpaque
        nav?.barTintColor           = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        nav?.tintColor              = UIColor.blue
        nav?.titleTextAttributes    = [NSForegroundColorAttributeName : uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)]
       
        // TableView Style And Background Color
        tableView.separatorStyle    = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor   = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
   
        
        let cityName                = uiFun.readFromDatabase(entityName: "Users", key: "city") // Fetching City Name From Core Data
        let profileStatus           = uiFun.readData(key: "ProfileCreation")    // Status Of Profile Creation
        
        if(cityName.lowercased() == "mumbai"){
            if(profileStatus == "done"){
                
                navigationDrawerLabel = ["Your Profile","Home", "Ridlr Blog", "Feedback", "Rate The App"]
                navgationDrawerIcon   = ["ic_indicator_default_user_40","ic_indicator_home","ic_cast_connected_black_24dp", "ic_assignment_black_24dp", "ic_indicator_favourites"]
            }else{
        
                navigationDrawerLabel = ["Profile Creation","Home", "Ridlr Blog", "Feedback", "Rate The App"]
                navgationDrawerIcon   = ["ic_indicator_default_user_40","ic_indicator_home", "ic_cast_connected_black_24dp", "ic_assignment_black_24dp", "ic_indicator_favourites"]
            }
        }else{
            
            navigationDrawerLabel = ["Home", "Ridlr Blog", "Feedback", "Rate The App"]
            navgationDrawerIcon   = ["ic_indicator_home", "ic_cast_connected_black_24dp", "ic_assignment_black_24dp", "ic_indicator_favourites"]
        
        }
        currentCityLabel.text = cityName
        
        uiFun.createCleverTapEvent(message: "Navgation Drawer Open")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return navigationDrawerLabel.count      // Number of rows in table view
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        let cell                    = tableView.dequeueReusableCell(withIdentifier: "Cell") as! NavigationDrawerCustomCellTableViewCell
        cell.drawerIcon.image       = UIImage(named: navgationDrawerIcon[indexPath.row])  // Navigation Drawer Row Icon
        cell.drawerLabel.text       = navigationDrawerLabel[indexPath.row]                // Navigation Drawer Row Label Text
        cell.drawerLabel.textColor  = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor) //Navigation Drawer Row Label Text Color
        cell.backgroundColor        = uiFun.hexStringToUIColor(hex: "#00BECC")             // Navigation Drawer Row Label Background Color
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cityName = uiFun.readFromDatabase(entityName: "Users", key: "city")  // Fetching City From Database
        
        if(cityName.lowercased() == "mumbai"){
            let profileStatus = uiFun.readData(key: "ProfileCreation") // Fetching Profile Creation Status
            switch indexPath.row {
                case 0:
                    fromUserProfile = true
                    if(profileStatus == "done"){
                        // If Profile is created navigate to Profile Screen
                        uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
                        uiFun.createCleverTapEvent(message: "Tap on My Profile")
                    }else{
                        // If Profile Is Not Selected Navigate To The Create Profile Screen
                        profileBackNavigation = "ViewController"
                        uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
                    }
                break
            case 1:
                uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
                break
            case 2:
                fromBlog    = true
                uiFun.navigateToScreem(identifierName: "OfferViewController", fromController: self)
                break
            case 3:
                uiFun.navigateToScreem(identifierName: "UserFeedbackViewController", fromController: self)
                break
            default:
                break
            }
        }else{
        
            switch indexPath.row {
            case 0:
                uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
                break
            case 1:
                fromBlog    = true
                uiFun.navigateToScreem(identifierName: "OfferViewController", fromController: self)
                break
            case 2:
                uiFun.navigateToScreem(identifierName: "UserFeedbackViewController", fromController: self)
                break
            default:
                break
            }
        }
        
        
    }
    @IBOutlet var chnageCityTapped: UIButton!

    @IBAction func changeCityTapped(_ sender: Any) {
        
        // When City Change Tapped From The Navigation Drawer Navigate To City List Screen
        changeCityFrom  = "navigation"
        uiFun.navigateToScreem(identifierName: "CityListViewController", fromController: self)
    }

}
