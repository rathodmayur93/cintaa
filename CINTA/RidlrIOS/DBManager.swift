//
//  DBManger.swift
//  RidlrIOS
//
//  Created by Mayur on 31/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class DBManager: NSObject {

    let field_BookingItemId       = "bookingItemId"
    let field_ItemSubType         = "itemSubType"
    let field_ActivationDate      = "activationDate"
    let field_ExpiryDate          = "expiryDate"
    let field_TotalAmount         = "totalAmount"
    let field_MasterQRCode        = "masterQRCode"
    
    let field_sourceName          = "sourceName"
    let field_destinationName     = "destinationName"
    let field_sourceId            = "sourceId"
    let field_destinationId       = "destinationId"
    
    var pathToDatabase  : String!
    var database        : FMDatabase!
    static let shared   : DBManager     = DBManager()
    let databaseFileName                = "database.sqlite"
    
    
    
    override init() {
        super.init()
        
        let documentsDirectory  = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase          = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createBookingTableQuery = "create table if not exists bookingItemList (\(field_BookingItemId) varchar primary key not null, \(field_ItemSubType) varchar not null, \(field_ActivationDate) varcahr not null, \(field_ExpiryDate) varchar not null, \(field_TotalAmount) varchar, \(field_MasterQRCode) varchar not null,\(field_sourceName) varchar not null,\(field_destinationName) varchar not null,\(field_sourceId) varchar not null,\(field_destinationId) varchar not null)"
                    
                    do {
                        try database.executeUpdate(createBookingTableQuery, values: nil)
                        created = true
                    }
                    catch {
                        print("Could not create table.")
                        print(error.localizedDescription)
                    }
                    
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        return false
    }
    
    
    func insertBookingDataData(bookingItemList : [MetroTokenJSONModel]){
    
        if openDatabase(){
        
            var query = ""
            
            for var i in 0..<bookingItemList.count{
            
                let bookingItemId   = bookingItemList[i].bookingItemId
                let itemSubType     = bookingItemList[i].itemSubType
                let activationDate  = bookingItemList[i].activationDate
                let expiryDate      = bookingItemList[i].expiryDate
                let totalAmount     = bookingItemList[i].fare[0].totalAmount
                let masterQRCode    = bookingItemList[i].companyRefId
                
                let journeyType     = uiFun.readData(key: "ticketingAgency")
                
                let sourceName          = bookingItemList[i].routeDetail[0].sourceName
                let destinationName     = bookingItemList[i].routeDetail[0].destinationName
                let sourceId            = bookingItemList[i].routeDetail[0].sourceId
                let destinationId       = bookingItemList[i].routeDetail[0].destinationId
                    
                query += "insert into bookingItemList (\(field_BookingItemId), \(field_ItemSubType), \(field_ActivationDate), \(field_ExpiryDate), \(field_TotalAmount), \(field_MasterQRCode), \(field_sourceName), \(field_destinationName), \(field_sourceId), \(field_destinationId)) values ('\(bookingItemId)', '\(itemSubType)', '\(activationDate)', '\(expiryDate)', '\(totalAmount)', '\(masterQRCode)', '\(sourceName)', '\(destinationName)', '\(sourceId)', '\(destinationId)');"
                print("Insert Booking Id \(bookingItemId)")
            }
            
            if !database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }
            
            database.close()
        }
    }
    
    
    func fetchBookingItems(itemSubType : String) -> [MetroTokenJSONModel]{
    
        var bookingItems = [MetroTokenJSONModel]()
        
        if openDatabase() {
            let query = "select * from bookingItemList where \(field_ItemSubType) = '\(itemSubType)'"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let metroTokenModel                     = MetroTokenJSONModel()
                    let fare                                = Fare()
                    let routeDetail                         = Route()
                    
                    metroTokenModel.bookingItemId           = results.string(forColumn: field_BookingItemId)
                    metroTokenModel.itemSubType             = results.string(forColumn: field_ItemSubType)
                    metroTokenModel.activationDate          = results.string(forColumn: field_ActivationDate)
                    metroTokenModel.expiryDate              = results.string(forColumn: field_ExpiryDate)
                    metroTokenModel.companyRefId            = results.string(forColumn: field_MasterQRCode)
                    
                    fare.totalAmount                        = results.string(forColumn: field_TotalAmount)
                    
                    routeDetail.sourceName                  = results.string(forColumn: field_sourceName)
                    routeDetail.destinationName             = results.string(forColumn: field_destinationName)
                    routeDetail.sourceId                    = results.string(forColumn: field_sourceId)
                    routeDetail.destinationId               = results.string(forColumn: field_destinationId)
                    
                    metroTokenModel.routeDetail.append(routeDetail)
                    metroTokenModel.fare.append(fare)
                    
                    bookingItems.append(metroTokenModel)
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return bookingItems
    }
    
    func updateBookingData(withID masterQRCode: String, source: String, destination: String, sourceId : String, destinationId : String, activationDate : String, expiryDate : String, totalAmount : String, itemSubType : String) {
        if openDatabase() {
            
            let query = "update bookingItemList set \(field_sourceName)=?, \(field_destinationName)=?, \(field_sourceId)=?, \(field_destinationId)=?, \(field_ActivationDate)=?, \(field_ExpiryDate)=?, \(field_TotalAmount)=?, \(field_ItemSubType)=? where \(field_MasterQRCode)=?"
            
            do {
                try database.executeUpdate(query, values: [source, destination, sourceId, destinationId, activationDate,expiryDate,totalAmount,itemSubType, masterQRCode])
                print("Item SubType is \(itemSubType)")
                print("Updating booking id \(masterQRCode)")
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
    }
    
    func databaseStatus() -> Bool{
        
        var results : FMResultSet!
        if openDatabase() {
            let query = "select * from bookingItemList"
            
            do {
                print(database)
                results = try database.executeQuery(query, values: nil)
                
                
            }
            catch {
                print(error.localizedDescription)
                
            }
            
            database.close()
        }
        if(results == nil){
            return false
        }else{
            return true
        }
    }
    
    
    func deleteBookingItem(bookingItemId : String) -> Bool{
    
        var deleted = false
        
        if openDatabase() {
            let query = "delete from bookingItemList where \(field_MasterQRCode) = '\(bookingItemId)'"
            
            database.executeStatements(query)
            deleted = true
            database.close()
        }
        
        return deleted
    }
}
