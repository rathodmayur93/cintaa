//
//  FetchModeTimetableAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 23/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchModeTimetableAPI {
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TImeTableTimingViewController, jsonString : String, cityId : String, modeId : String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/trip/schedule/cityid/\(cityIdFromData)/modeid/\(modeId)/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Trips Fetching URL \(url)")
        print("Timetable Trip JSON Body \(jsonString)")
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.FetchModeTripResponse(response: jsonResponse)
                print("Successfully Timetable Trips Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable Trips Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }

}
