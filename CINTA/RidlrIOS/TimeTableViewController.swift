//
//  TimeTableViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 01/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit


var startingStationName    = "Starting Station"
var destinationStationName = "Destination Station"
var busAgency              = "Select Bus Agency"
var fromWhere              = ""
var timetableModeId        = ""
class TimeTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let uiFun           = UiUtillity()
    let ridlrColors     = RidlrColors()
    
    
    @IBOutlet var locationProviderView              : UIView!
    @IBOutlet var recentSearchesView                : UIView!
    @IBOutlet var startingDestinationView           : UIView!
   
    @IBOutlet var busAgencySeparator                : UIView!
    @IBOutlet var locationSeparator                 : UIView!
    
    @IBOutlet var trainAgenciesTableView            : UITableView!
    
    @IBOutlet var travelModeSegment                 : UISegmentedControl!
    @IBOutlet var busSegmentControl                 : UISegmentedControl!
    
    @IBOutlet var fromToIV                          : UIImageView!
    @IBOutlet var busAgencyIV                       : UIImageView!
    
    @IBOutlet var startingStationLabel              : UILabel!
    @IBOutlet var destinationStationLabel           : UILabel!
    @IBOutlet var selectBusAgencyLabel              : UILabel!
    @IBOutlet var recentSearchLabel                 : UILabel!
    
    @IBOutlet var backButton                        : UIButton!
    
    var transportatioinMode                         : String = ""
    
    var trainAgencies       = [Agency]()
    var busAgencies         = [Agency]()
    var metroAgencies       = [Agency]()
    var monorailAgencies    = [Agency]()
    
    let cityParser          = CityParser()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Setting Tableview separator color and background color
        trainAgenciesTableView.separatorStyle   = UITableViewCellSeparatorStyle.none
        trainAgenciesTableView.backgroundColor  = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        
        let backImage                           = UIImage(named: "ic_indicator_go_back_holo")
        backButton.setImage(backImage, for: .normal)
        backButton.setTitle("", for: .normal)
        
        startingDestinationLableAction()
        
        startingStationLabel.text               = startingStationName       // Starting station name
        destinationStationLabel.text            = destinationStationName    // destination station name
        selectBusAgencyLabel.text               = busAgency                 // Bus agency name e.g BEST,NMMT etc etc
        travelModeSegment.selectedSegmentIndex  = travelModeSelectedIndex   // Index of travel mode e.g Train, Bus, Metro and mono
        busSegmentControl.selectedSegmentIndex  = busSegmentSelectedIndex   // Index of bus mode e.g From Bus Stop or From A to B
        
        recentSearchLabel.lineBreakMode         = NSLineBreakMode.byWordWrapping    // Setting label width to word wrap
        recentSearchLabel.numberOfLines         = 3
        
        if(travelModeSelectedIndex == 0){
            timetableModeId                     = cityParser.getModeId(modeName: "RAIL")    // Setting mode id
            print("Mode Id \(timetableModeId)")
        }

        // Bus Is Selected
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
                //Using Bus Number
                busAgencySelected()
            }else if(busSegmentSelectedIndex == 1){
                // From A to B
                busAgencyFromSourceToDestionation()
            }
        }else{
            newSetUI()
        }
        checkStationNames() // Check for starting and destination station should be different
        uiFun.createCleverTapEvent(message: "TIMETABLE MAIN SCREEN")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        travelModeSegment.selectedSegmentIndex = travelModeSelectedIndex
        
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
                // Updatin UI for Bus search using bus number
                busAgencySelected()
            }else if(busSegmentSelectedIndex == 1){
                // Updating UI for starting and destinatio stop
                busAgencyFromSourceToDestionation()
            }
        }else{
            // common UI for Train, Metro and Mono
            self.newSetUI()
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{

        switch travelModeSegment.selectedSegmentIndex {
        case 0:
            // Fetching agencies name of respective agency
            trainAgencies = cityParser.getAgencies(modeName: "RAIL")
            return trainAgencies.count
        case 1:
            busAgencies = cityParser.getAgencies(modeName: "BUS")
            return busAgencies.count
        case 2:
            metroAgencies = cityParser.getAgencies(modeName: "METRO")
            return metroAgencies.count
        case 3:
            monorailAgencies = cityParser.getAgencies(modeName: "MONO")
            return monorailAgencies.count
        default:
            return 0
        }
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        // Initializing the cell
        let cell            = tableView.dequeueReusableCell(withIdentifier: "Cell") as! NavigationDrawerCustomCellTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if(travelModeSegment.selectedSegmentIndex == 0){
            // train agencies
            cell.agenciesTrain.text     = trainAgencies[indexPath.row].agencyLongName
            cell.subAgenciesTrain.text  = trainAgencies[indexPath.row].agencyShortName
        }
        else if (travelModeSegment.selectedSegmentIndex == 1){
            // bus agencies
            cell.agenciesTrain.text     = busAgencies[indexPath.row].agencyLongName
            cell.subAgenciesTrain.text  = busAgencies[indexPath.row].agencyShortName
        }
        else if(travelModeSegment.selectedSegmentIndex == 2){
            // metro agencies
            cell.agenciesTrain.text     = metroAgencies[indexPath.row].agencyLongName
            cell.subAgenciesTrain.text  = metroAgencies[indexPath.row].agencyShortName
        }
        else if(travelModeSegment.selectedSegmentIndex == 3){
            // mono agencies
            cell.agenciesTrain.text     = monorailAgencies[indexPath.row].agencyLongName
            cell.subAgenciesTrain.text  = monorailAgencies[indexPath.row].agencyShortName
        }
        
        cell.backgroundColor            = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        return cell
    }
    
    func setUI(){
        
        //We are not using this function anywhere its in code just for refernce
        fromToIV.isHidden = false
        busAgencyIV.isHidden = true
        busSegmentControl.isHidden = true
        selectBusAgencyLabel.isHidden = true
        
        

        startingStationLabel.translatesAutoresizingMaskIntoConstraints = true
        
        locationProviderView.frame = CGRect(x: 0, y: -10, width: self.view.frame.width, height: 217)
        recentSearchesView.frame = CGRect(x: 0, y: 205, width: self.view.frame.width, height: 85)
        trainAgenciesTableView.frame = CGRect(x: 0, y: 298, width: self.view.frame.width, height: 265+104) //104 is y co-ordinate defference between the train layout and bus layout
        
        startingDestinationView.frame = CGRect(x: 0, y: 104, width: self.view.frame.width, height: 143)
        startingStationLabel.frame = CGRect(x: 64, y: 8, width: self.view.frame.width, height: 36)
        destinationStationLabel.frame = CGRect(x: 64, y: 61, width: self.view.frame.width, height: 36)
        locationSeparator.frame = CGRect(x: 64, y: 56, width: self.view.frame.width, height: 1)
        
        
        let font = UIFont.systemFont(ofSize: 10)
        busSegmentControl.setTitleTextAttributes([NSFontAttributeName: font],for: .normal)
        busSegmentControl.selectedSegmentIndex = 0
        
        selectBusAgencyLabel.text = busAgency
    }
    
    func newSetUI(){
        
        // common function for Train, Metro and Mono (Updating UI)
        recentSearchesView.translatesAutoresizingMaskIntoConstraints = true
        
        fromToIV.isHidden               = false // imageview hidden

        // Hidding bus ui and showing train ui
        selectBusAgencyLabel.isHidden   = true
        busAgencyIV.isHidden            = true
        busSegmentControl.isHidden      = true
        busAgencySeparator.isHidden     = true
        // starting and destination separator
        locationSeparator.frame         = CGRect(x: 48, y: 60, width: self.view.frame.width - 16, height: 1)
        
        startingDestinationView.frame   = CGRect(x: 8, y: 112, width: Int(self.view.frame.width - 16), height: 112)
        recentSearchesView.frame        = CGRect(x: 0, y: 309, width: Int(self.view.frame.width), height: 84)
        trainAgenciesTableView.frame    = CGRect(x: 0, y: 400, width: Int(self.view.frame.width), height: Int(self.view.frame.height))
        
        /********* Animate Starting Destination View **************/
        startingDestinationView.center = CGPoint(x: startingDestinationView.center.x, y: startingDestinationView.center.y)
        UIView.animate(withDuration: 0.2) {
            self.startingDestinationView.center = CGPoint(x: self.startingDestinationView.center.x, y: self.startingDestinationView.center.y - 104)
        }
        
        /********* Animate Recent Search View **************/
        recentSearchesView.center = CGPoint(x: recentSearchesView.center.x, y: recentSearchesView.center.y)
        UIView.animate(withDuration: 0.2) {
            self.recentSearchesView.center = CGPoint(x: self.recentSearchesView.center.x, y: self.recentSearchesView.center.y - 95)
        }
        
        /********* Animate Table View **************/
        trainAgenciesTableView.center = CGPoint(x: trainAgenciesTableView.center.x, y: trainAgenciesTableView.center.y)
        UIView.animate(withDuration: 0.2) {
            self.trainAgenciesTableView.center = CGPoint(x: self.trainAgenciesTableView.center.x, y: self.trainAgenciesTableView.center.y - 102)
        }
            // Fetching recent search value from database
            let recentSearch = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode\(travelModeSelectedIndex)")
            if(recentSearch != ""){
                // if recent seach value present in the database
                recentSearchLabel.text = recentSearch
            }else{
                // if recent search value is not present
                recentSearchLabel.text = "Your Recent Search Appears Here"
            }
        
        if(doneWithTripDetail){
            
            doneWithTripDetail = false
            if(travelModeSelectedIndex == 1){
            
                if(busSegmentSelectedIndex == 0){
                    startingStationLabel.text     = "Search For Bus Number"
                    destinationStationLabel.text  = "Search Bus Stop"
                
                }else if(busSegmentSelectedIndex == 1){
                    startingStationLabel.text     = "Start Bus Stop"
                    destinationStationLabel.text  = "Destination Bus Stop"
                }
            }else{
                startingStationLabel.text         = "Starting Station"
                destinationStationLabel.text      = "Destination Station"
            }
        }
    
        if(startingStationLabel.text != "Starting Station" && travelModeSelectedIndex != 1){
            // If starting station is selcted set text color to white
            startingStationLabel.textColor      = UIColor.white
        }else{
            // If starting station is not selcted set text color to gray
            startingStationLabel.textColor      = UIColor.gray
            destinationStationLabel.textColor   = UIColor.gray
        }
        selectBusAgencyLabel.text               = busAgency
    }
    
    func ResetLabels(){
            // Reset starting and destination text
            if(travelModeSelectedIndex == 1){
                
                if(busSegmentSelectedIndex == 0){
                    startingStationName     = "Search For Bus Number"
                    destinationStationName  = "Search Bus Stop"
                }else if(busSegmentSelectedIndex == 1){
                    startingStationName     = "Start Bus Stop"
                    destinationStationName  = "Destination ySeBus Stop"
                }
            }else{
                startingStationName         = "Starting Station"
                destinationStationName      = "Destination Station"
            }
        }
    
    func busAgencySelected(){
    
        // When user select bus agency (Using Bus Number)
        recentSearchesView.translatesAutoresizingMaskIntoConstraints = true
        
        fromToIV.isHidden               = true
        busSegmentControl.isHidden      = false
        
        selectBusAgencyLabel.isHidden   = true
        busAgencyIV.isHidden            = true
        busAgencySeparator.isHidden     = true
        
        startingDestinationView.frame   = CGRect(x: 8, y: 124, width: Int(self.view.frame.width - 16), height: 112)
        recentSearchesView.frame        = CGRect(x: 0, y: 309, width: Int(self.view.frame.width), height: 84)
        trainAgenciesTableView.frame    = CGRect(x: 0, y: 400, width: Int(self.view.frame.width), height: Int(self.view.frame.height))
        
        locationSeparator.frame         = CGRect(x: 8, y: 130, width: self.view.frame.width - 16, height: 1)
        
        /********* Animate Starting Destination View **************/
        startingDestinationView.center          = CGPoint(x: startingDestinationView.center.x, y: startingDestinationView.center.y)
        UIView.animate(withDuration: 0.2) {
            self.startingDestinationView.center = CGPoint(x: self.startingDestinationView.center.x - 34, y: self.startingDestinationView.center.y - 92)
        }
        locationSeparator.center            = CGPoint(x: locationSeparator.center.x, y: locationSeparator.center.y)
        UIView.animate(withDuration: 0.2) {
            self.locationSeparator.center   = CGPoint(x: self.locationSeparator.center.x + 16, y: self.locationSeparator.center.y - 72)
        }
        
        /********* Animate Recent Search View **************/
        recentSearchesView.center           = CGPoint(x: recentSearchesView.center.x, y: recentSearchesView.center.y)
        UIView.animate(withDuration: 0.2) {
            self.recentSearchesView.center  = CGPoint(x: self.recentSearchesView.center.x, y: self.recentSearchesView.center.y - 82)
        }
        
        /********* Animate Table View **************/
        trainAgenciesTableView.center           = CGPoint(x: trainAgenciesTableView.center.x, y: trainAgenciesTableView.center.y)
        UIView.animate(withDuration: 0.2) {
            self.trainAgenciesTableView.center  = CGPoint(x: self.trainAgenciesTableView.center.x, y: self.trainAgenciesTableView.center.y - 96)
        }
        
        let recentSearch = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode1")
        if(recentSearch != ""){
            recentSearchLabel.text = recentSearch
        }else{
            recentSearchLabel.text = "Your Recent Search Appears Here"
        }
        
        if(travelModeSelectedIndex == 1 && startingStationLabel.text?.lowercased() != "search for bus number" && busSegmentSelectedIndex == 0){
            startingStationLabel.textColor      = UIColor.white
        }else{
            startingStationLabel.textColor      = UIColor.gray
            destinationStationLabel.textColor   = UIColor.gray
        }
        
    }
    
    func busAgencyFromSourceToDestionation(){
        
        // When user select bus agency (Using Starting and Destination Bus Number)
        
        // Showing UI for Bus
        fromToIV.isHidden               = false
        selectBusAgencyLabel.isHidden   = false
        busAgencySeparator.isHidden     = false
        busAgencyIV.isHidden            = false
        busSegmentControl.isHidden      = false
        
        // Positioning the starting, destination, separator and recent search
        startingDestinationView.frame   = CGRect(x: 8, y: 106, width: Int(self.view.frame.width - 16), height: 112)
        recentSearchesView.frame        = CGRect(x: 0, y: 309, width: Int(self.view.frame.width), height: 84)
        trainAgenciesTableView.frame    = CGRect(x: 0, y: 400, width: Int(self.view.frame.width), height: Int(self.view.frame.height))
        
        locationSeparator.frame         = CGRect(x: 48, y: 60, width: self.view.frame.width - 16, height: 1)
        
        let recentSearch                = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode11")
        if(recentSearch != ""){
            recentSearchLabel.text      = recentSearch
        }else{
            recentSearchLabel.text      = "Your Recent Search Appears Here"
        }
        
        if(travelModeSelectedIndex == 1 && startingStationLabel.text != "Start Bus Stop" && busSegmentSelectedIndex == 1){
            
            // Setting text color to white if start bus stop is selected
            startingStationLabel.textColor      = UIColor.white
        }else{
            // Setting text color to gray if start bus stop is not selected
            startingStationLabel.textColor      = UIColor.gray
            destinationStationLabel.textColor   = UIColor.gray
        }
    }
    
   
    @IBAction func backButtonTapped(_ sender: Any) {
        // When user taps on back button tap action
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        showPlanner = true
    }
    
    
    @IBAction func travelModeSegmentChanged(_ sender: UISegmentedControl) {
        
        switch travelModeSegment.selectedSegmentIndex {
        case 0:
            print("Travel Mode Is Train")
            trainAgenciesTableView.reloadData() // reload agency table view
            
            travelModeSelectedIndex     = 0
            timetableModeId             = cityParser.getModeId(modeName: "RAIL")    // Fetching mode id of the agency
            print("Mode Id \(timetableModeId)")
            
            if(timetableModeId == ""){
                // show alert box if timetableModeId is not selected
                 uiFun.showAlert(title: "", message: "Oops...This mode of transportation is not available in your city.", logMessage: "Mode Of Transportation is not supported", fromController: self)
            }
            startingStationLabel.text       = "Starting Station"
            destinationStationLabel.text    = "Destination Station"
            
            // Fetching recentSearch from database
            let recentSearch                = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode0")
            
            if(recentSearch != ""){
                // Set recent search
                recentSearchLabel.text      = recentSearch
            }else{
                // If recent search is not present
                recentSearchLabel.text      = "Your Recent Search Appears Here"
            }
            newSetUI()  // Updating UI
            break
        case 1:
            print("Travel Mode is Bus")
            
            trainAgenciesTableView.reloadData() // reload agency table view
           
            timetableModeId                         = cityParser.getModeId(modeName: "BUS") // fetching bus mode id
            print("Mode Id \(timetableModeId)")
            travelModeSelectedIndex                 = 1
            busSegmentControl.selectedSegmentIndex  = busSegmentSelectedIndex
            
            if(timetableModeId == ""){
                uiFun.showAlert(title: "", message: "Oops..This mode of transportation is not available in your city.", logMessage: "Mode Of Transportation is not supported", fromController: self)
            }
            
            if(busSegmentSelectedIndex == 0){

                busSegmentSelectedIndex                 = 0
                busSegmentControl.selectedSegmentIndex  = 0
                
                startingStationLabel.text               = "Search for bus number"
                destinationStationLabel.text            = "Select bus stop"
                busAgencySelected()
                
                // Fetching recentSearch from database
                let recentSearch                        = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode1")
              
                if(recentSearch != ""){
                    // set recent search
                    recentSearchLabel.text              = recentSearch
                }else{
                    // If recent search is not present
                    recentSearchLabel.text              = "Your Recent Search Appears Here"
                }
            }else if(busSegmentSelectedIndex == 1){
            
                busSegmentControl.selectedSegmentIndex  = 1
                
                busSegmentSelectedIndex                 = 1
                startingStationLabel.text               = "Start Bus Stop"
                destinationStationLabel.text            = "Destination Bus Stop"
                busAgencyFromSourceToDestionation()
                
                // Fetching recentSearch from database
                let recentSearch = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode11")
                
                if(recentSearch != ""){
                    // set recent search
                    recentSearchLabel.text = recentSearch
                }else{
                    // If recent search is not present
                    recentSearchLabel.text = "Your Recent Search Appears Here"
                }
            }
         break
            
        case 2:
            print("Travel Mode is Metro")
            trainAgenciesTableView.reloadData()
            travelModeSelectedIndex             = 2
            timetableModeId                     = cityParser.getModeId(modeName: "METRO")
            print("Mode Id \(timetableModeId)")
            
            if(timetableModeId == ""){
                uiFun.showAlert(title: "", message: "Oops..This mode of transportation is not available in your city.", logMessage: "Mode Of Transportation is not supported", fromController: self)
            }
            
            // Fetching recentSearch from database
            let recentSearch                    = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode2")
            
            if(recentSearch != ""){
                // set recent search
                recentSearchLabel.text = recentSearch
            }else{
                // If recent search is not present
                recentSearchLabel.text          = "Your Recent Search Appears Here"
            }
            
            startingStationLabel.text           = "Starting Station"
            destinationStationLabel.text        = "Destination Station"
            newSetUI()  // Updating UI
            break
        case 3:
            print("Travel Mode is Monorail")
            trainAgenciesTableView.reloadData()
            travelModeSelectedIndex         = 3
            
            timetableModeId                 = cityParser.getModeId(modeName: "MONORAIL")
            if(timetableModeId == ""){
                uiFun.showAlert(title: "", message: "Oops..This mode of transportation is not available in your city.", logMessage: "Mode Of Transportation is not supported", fromController: self)
            }
            
            print("Mode Id \(timetableModeId)")
            startingStationLabel.text       = "Starting Station"
            destinationStationLabel.text    = "Destination Station"
            
            // Fetching recentSearch from database
            let recentSearch = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode3")
            
            if(recentSearch != ""){
                // set recent search
                recentSearchLabel.text      = recentSearch
            }else{
                // If recent search is not present
                recentSearchLabel.text      = "Your Recent Search Appears Here"
            }
            newSetUI()  // Updating UI
            break
        default:
            print("Please Select Travel Mode")
        }
    }
    
    @IBAction func busSegmentControlAction(_ sender: AnyObject) {
        
        // If bus segment is selected there will two more segment named "BY BUS NUMBER OR NAME" and "FROM A TO B"
        switch busSegmentControl.selectedSegmentIndex {
        case 0:
            busAgencySelected()
            busSegmentSelectedIndex         = 0
            
            startingStationLabel.text       = "Search for bus number"
            destinationStationLabel.text    = "Select bus stop"
            
            // Fetching recentSearch from database
            let recentSearch = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode1")
            
            if(recentSearch != ""){
                // set recent search
                recentSearchLabel.text      = recentSearch
            }else{
                // If recent search is not present
                recentSearchLabel.text      = "Your Recent Search Appears Here"
            }

           break
            
        case 1:
            busAgencyFromSourceToDestionation()
            busSegmentSelectedIndex         = 1
            
            startingStationLabel.text       = "Start Bus Stop"
            destinationStationLabel.text    = "Destination Bus Stop"
            
            // Fetching recentSearch from database
            let recentSearch                = uiFun.readFromDatabase(entityName: "RecentSearch", key: "mode11")
            if(recentSearch != ""){
                // set recent search
                recentSearchLabel.text      = recentSearch
            }else{
                // If recent search is not present
                recentSearchLabel.text      = "Your Recent Search Appears Here"
            }
            break
        default:
            break
            
        }
    }
    
    func startingDestinationLableAction(){
        // Starting station label tap action
        let tapGesture                                = UITapGestureRecognizer(target: self, action: #selector(startingStationTapped))
        tapGesture.numberOfTapsRequired               = 1
        startingStationLabel.isUserInteractionEnabled = true
        startingStationLabel.addGestureRecognizer(tapGesture)
        
        // Destination label tap action
        let tapGestureDestination                   = UITapGestureRecognizer(target: self, action: #selector(destinationStationTapped))
        tapGesture.numberOfTapsRequired             = 1
        destinationStationLabel.isUserInteractionEnabled = true
        destinationStationLabel.addGestureRecognizer(tapGestureDestination)
        
        // Recent search tap action
        let tapGestureRecentSearch                  = UITapGestureRecognizer(target: self, action: #selector(recentSearchTapped))
        tapGesture.numberOfTapsRequired             = 1
        recentSearchesView.isUserInteractionEnabled = true
        recentSearchesView.addGestureRecognizer(tapGestureRecentSearch)
        
        // Bus agency tap action
        let tapGestureBusAgency                       = UITapGestureRecognizer(target: self, action: #selector(busAgencyTapped))
        tapGesture.numberOfTapsRequired               = 1
        selectBusAgencyLabel.isUserInteractionEnabled = true
        selectBusAgencyLabel.addGestureRecognizer(tapGestureBusAgency)
        
    }
    
    func busAgencyTapped(){
        // When user taps on bus agency navigate to "SelectBusAgencyViewController"
        uiFun.navigateToScreem(identifierName: "SelectBusAgencyViewController", fromController: self)
    }
    
    func startingStationTapped(){
        // When user taps on the starting station label
        fromWhere = "start"
        uiFun.navigateToScreem(identifierName: "TimetableStationViewController", fromController: self)
    }
    
    func destinationStationTapped(){
        // When user taps on the destination station label
        fromWhere = "end"
        uiFun.navigateToScreem(identifierName: "TimetableStationViewController", fromController: self)
    }
    
    func recentSearchTapped(){
        // When user taps on the Recent search label
        if(recentSearchLabel.text?.contains("Search Appears Here"))!{
            uiFun.showAlert(title: "", message: "Ooops...No Recent Routes Found!", logMessage: "No recent routes found", fromController: self)
            return
        }else{
        let recentSearchArray           = recentSearchLabel.text?.components(separatedBy: " to ")
        
        startingStationName             = (recentSearchArray?[0])!
        destinationStationName          = (recentSearchArray?[1])!
        
        if(travelModeSelectedIndex == 1){
            // Fetching route id, agency id and stopId from the database
            routeId     = uiFun.readFromDatabase(entityName: "RecentSearch", key: "routeId")
            agencyId    = uiFun.readFromDatabase(entityName: "RecentSearch", key: "agencyId")
            stopId      = uiFun.readFromDatabase(entityName: "RecentSearch", key: "stopId")
        }
        uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
       }
    }
    
    func checkStationNames(){
    
        // Check station name and destination name if both are selected navigate to "TImeTableTimingViewController"
        if(startingStationLabel.text != "Starting Station"){
            
            if(startingStationLabel.text == destinationStationLabel.text){
                startingStationLabel.text       = "Starting Station"
                destinationStationLabel.text    = "Destination Station"
                uiFun.showAlert(title: "Error", message: "Ooops... Starting and destination can't be same", logMessage: "Timetaable Starting and destination same", fromController: self)
            }
        
            if(travelModeSelectedIndex != 1 && destinationStationLabel.text != "Destination Station"){
            
                switch travelModeSelectedIndex {
                case 0:
                    uiFun.writeToDatabase(entityValue: "\(startingStationLabel.text!) to \(destinationStationLabel.text!)", entityName: "RecentSearch", key: "mode0")
                    break
                case 2:
                    uiFun.writeToDatabase(entityValue: "\(startingStationLabel.text!) to \(destinationStationLabel.text!)", entityName: "RecentSearch", key: "mode2")
                    break
                case 3:
                    uiFun.writeToDatabase(entityValue: "\(startingStationLabel.text!) to \(destinationStationLabel.text!)", entityName: "RecentSearch", key: "mode3")
                    break
                    
                default:
                    break
                    
                }
                
                if(navigateToTripResult){
                    navigateToTripResult = false
                    uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
                }
            }
            
            if(travelModeSelectedIndex == 1){
                if(busSegmentSelectedIndex == 0 && destinationStationLabel.text != "Search Bus Stop"){
                    uiFun.writeToDatabase(entityValue: "\(startingStationLabel.text!) to \(destinationStationLabel.text!)", entityName: "RecentSearch", key: "mode1")
                
                    if(navigateToTripResult){
                        navigateToTripResult = false
                        uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
                    }
                }
            }
            
            if(travelModeSelectedIndex == 1){
            
                if(busSegmentSelectedIndex == 1 && destinationStationLabel.text != "Destination Bus Stop"){
                    uiFun.writeToDatabase(entityValue: "\(startingStationLabel.text!) to \(destinationStationLabel.text!)", entityName: "RecentSearch", key: "mode11")
                    
                    if(navigateToTripResult){
                        navigateToTripResult = false
                        uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
                    }
                }
            }
            
        }
    }
}
