//
//  UserFeedbackViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 07/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class UserFeedbackViewController: UIViewController {

    @IBOutlet var emailTF: UITextField!
    @IBOutlet var feedbackTF: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Email Text Field Border Width And Color
        emailTF.layer.borderWidth    = 1
        emailTF.layer.borderColor    = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor).cgColor
        
        // Feedback Text Field Border Width And Color
        feedbackTF.layer.borderWidth = 1
        feedbackTF.layer.borderColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor).cgColor
    
        uiFun.createCleverTapEvent(message: "FEEDBACK SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self) // When user taps on back button from toolbar
    }
    
    @IBAction func submitAction(_ sender: Any) {
        
        // When user clicks on submit button it validate the email and feedback, if succeed makes an api call
        
        if(emailTF.text! == "" || !isValidEmail(testStr: emailTF.text!)){
            uiFun.showAlert(title: "", message: "Please Enter Valid Email Address", logMessage: "Feedback Please Enter Email Id", fromController: self)
        
        }else if(feedbackTF.text == ""){
            uiFun.showAlert(title: "", message: "Please Enter Feedback", logMessage: "Please Enter Feedback", fromController: self)
        
        }else{
            uiFun.showIndicatorLoader()
            let feedbackAPI = FeedbackAPI() // API call for submittin feedback
            feedbackAPI.makeAPICall(fromViewController: self, deviceId: uiFun.readData(key: "UUID"), email: emailTF.text!, feedback: feedbackTF.text)
        }
        
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // Checks whether email address is valid or not
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func gotFeedbackResponse(response : JSON){
        
        // API response
        uiFun.hideIndicatorLoader()
        let status = String(describing:response["status"])
        if(status == "5000"){
            // Success
            uiFun.showAlert(title: "", message: "Feedback Sent Successfuly", logMessage: "Feedback Sent Success", fromController: self)
            emailTF.text    = ""
            feedbackTF.text = ""
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as UIViewController
            self.present(vc, animated: true, completion: nil)
        }else{
            // Fail
            uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
        }
    }

}
