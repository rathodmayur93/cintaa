//
//  ResendOtpApiCall.swift
//  RidlrIOS
//
//  Created by Mayur on 11/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class ResendOtpApiCall {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()

    func makeAPICall(fromViewController : ProfileOTPViewController, emailId : String, paymentGateway : String, walletEmail : String, walletMobile : String){
        
        var url = ""
        if(paymentGateway != ""){
            url = "\(constant.STAGING_BASE_URL)/payment/auth_methods" // Wallet authentication url
        }else{
            url = "\(constant.STAGING_BASE_URL)/user/auth_methods"    // Without wallet authentication url
        }
        
       util.APICall(apiUrl: url, jsonString: resendOTPJson(email: emailId, paymentGateway: paymentGateway, walletEmail: walletEmail, walletMobile: walletMobile)!, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                // API response success
                fromViewController.resendOTPResponse(jsonResult : jsonResponse)
                print("Successfully Resending OTP Done.... \(jsonResponse) ")
                return
                
            }else {
                // API Response fail
                print("Resending OTP Failed....Try Harder Mayur ")
                return
            }
            
        }
    }
    
    
    func resendOTPJson(email : String, paymentGateway : String, walletEmail : String, walletMobile : String) -> String?{
        
        let para:NSMutableDictionary = NSMutableDictionary()
        // Creating json string for api call
        if(paymentGateway != ""){
            para.setValue(email, forKey: "email")
            para.setValue(paymentGateway, forKey: "paymentGateway")
            para.setValue(walletEmail, forKey: "walletEmail")
            para.setValue(walletMobile, forKey: "walletMobile")
        }else{
            para.setValue(email, forKey: "contactEmail")
            para.setValue(walletMobile, forKey: "contactNumber")
        }
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("fetch bus number json string = \(jsonString)") // Json string
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

    
}
