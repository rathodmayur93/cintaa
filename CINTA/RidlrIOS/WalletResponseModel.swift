//
//  WalletResponseModel.swift
//  RidlrIOS
//
//  Created by Mayur on 08/05/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class WalletResponseModel {

    var status              = ""
    var amount              = ""
    var paymentMode         = ""
    var orderNo             = ""
    var companyIdentifier   = ""
}
