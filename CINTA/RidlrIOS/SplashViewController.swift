//
//  SplashViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 26/10/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation
import SwiftyJSON

class SplashViewController: UIViewController, CLLocationManagerDelegate, UIAlertViewDelegate
{
    
    let uiFun               = UiUtillity()
    let ridlrColors         = RidlrColors()
    var locationManager     = CLLocationManager()
    
    var timer               = Timer()
    var time                = 0
    
    var alertTest           = UIAlertView()
    
    
    @IBOutlet var rildrLabel            : UILabel!
    @IBOutlet var ridlrDescriptionLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nav                     = self.navigationController?.navigationBar
        nav?.isHidden               = true
        nav?.barTintColor           = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        nav?.tintColor              = UIColor.blue
        nav?.titleTextAttributes    = [NSForegroundColorAttributeName : uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)]
    
        ridlrDescriptionLabel.lineBreakMode = .byWordWrapping
        ridlrDescriptionLabel.numberOfLines = 0
        
        alertTest.delegate          = self   //set the delegate of alertView
        
        //Requesting For Location Permission
        locationManager.delegate                            = self
        locationManager.desiredAccuracy                     = kCLLocationAccuracyThreeKilometers
//      locationManager.requestWhenInUseAuthorization()
//      locationManager.startUpdatingLocation()
        
        uiFun.createCleverTapEvent(message: "Splash Screen")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkInternetConnection()
    }

    func checkInternetConnection(){
        
        // check for internet connection
        if(uiFun.isConnectedToNetwork()){
            // Intializing Timer
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(SplashViewController.goToScreen), userInfo: nil, repeats: true)
        }else{
            // show no internet dialog
            alertTest.message = "No internet connection. Please Check!"
            alertTest.addButton(withTitle: "Ok")
            alertTest.addButton(withTitle: "Cancel")
            alertTest.title = "No Internet"
            alertTest.show()
            
        }
        
    }
    public func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int){
    
        switch buttonIndex {
        case 0:
            // Opening setting page of phone
            guard let settingsUrl = URL(string: "App-Prefs:root=WIFI") else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                redirectingPage = "SplashViewController"
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
            break
        case 1:
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            break
        default:
            break
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // Fetching Lat Lng Of User
        let userLocation: CLLocation    = locations[0]
        let latitude                    = userLocation.coordinate.latitude
        let longitude                   = userLocation.coordinate.longitude
        
        uiFun.writeData(value: String(describing :latitude), key: "lat")
        uiFun.writeData(value: String(describing :longitude), key: "long")
        
        reverseGeocoding(latitude: latitude, longitude: longitude)
        
        locationManager.stopUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
    }
    
    func reverseGeocoding(latitude: CLLocationDegrees, longitude: CLLocationDegrees){
        // Reverse Geo-Coding converting lat lng to address
        var locationAddress     = ""
        print("map location lat \(latitude) and long \(longitude)")
        let location            = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            
            if error != nil{
                print(error!)
            }
            else{
                
                if let address = placemarks?[0]{
                    print(address)
                    
                    var subThoroughFare     = ""
                    var thoroughFare        = ""
                    var subLocality         = ""
                    var subAdmin            = ""
                    var postalCode          = ""
                    var country             = ""
                    
                    if address.subThoroughfare != nil{
                        subThoroughFare = address.subThoroughfare!
                    }
                    
                    if address.thoroughfare != nil{
                        thoroughFare = address.thoroughfare!
                    }
                    
                    if address.subLocality != nil{
                        subLocality = address.subLocality!
                    }
                    
                    if address.subAdministrativeArea != nil{
                        subAdmin = address.subAdministrativeArea!
                    }
                    if address.postalCode != nil{
                        postalCode = address.postalCode!
                    }
                    
                    if address.country != nil {
                        country = address.country!
                    }
                    
                    var markerLocation = ""
                                        markerLocation  = subThoroughFare + "," + thoroughFare + "," + subLocality + "," + subAdmin + "," + postalCode + "," + country
                    
                    locationAddress = markerLocation
                    print("Location Address \(locationAddress)")
                    
                    print(subThoroughFare + "\n" + thoroughFare + "\n" + subLocality + "\n" + subAdmin + "\n" + postalCode + "\n" + country)
                }
            }
            
        }
    }
    
    func goToScreen(){
        
        time += 1
        
        if time == 3{
            // Code Will Executed When Timer Reach to 3sec
            timer.invalidate()
        
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context     = appDelegate.persistentContainer.viewContext
        
            let city        = NSFetchRequest<NSFetchRequestResult>(entityName: "Users") // Requesting City Object From Core Data
            city.returnsObjectsAsFaults = false
        
        do {
            let results =  try context.fetch(city) // Fetching City Object From Core Data
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    if let cityName = result.value(forKey: "city") as? String{
                        print(cityName)
                        uiFun.writeData(value: cityName, key: "City") // Storing Data to Permanent Storage
                    }
                }
                // If City Is Present In Core Data Will Navigate To Homepage
                uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
                
                
            }
            else{
                // If City Is Not Present In Core Data Will Navigate To CityList Screen
                print("City is not selected yet 2")
                uiFun.writeData(value: "yes", key: "first_time")    // Storing Data to Permanent Storage
                uiFun.writeData(value: "yes", key: "best_tutorial") // Storing Data to Permanent Storage
                self.performSegue(withIdentifier: "toCityListViewController", sender: nil)
                
                locationManager.stopUpdatingLocation()
            }
        }
        catch {
            print("There is an error in fetching city from core data")
        }
      }
    }
}
