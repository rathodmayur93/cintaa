//
//  MetroHomeScreenViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 04/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var tokenFlow   = ""
var tokenArray  = [QRCodeOffline]()

var metroFromWhere  =   ""
let convertedList   = [MetroTokenJSONModel]()

class MetroHomeScreenViewController: UIViewController {

    
    @IBOutlet var metroCardRechargeView : UIView!
    @IBOutlet var metroTokenView        : UIView!
    @IBOutlet var metroTripPassView     : UIView!
    @IBOutlet var metroStoredValuePass  : UIView!
    
    var journeyType = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        
        tokenOriginStationName          = "Origin Station"              // origin station name
        tokenDestinationStationName     = "Destination Station"
        penaltyStation                  = "Select Station"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        // Creating Background Blur
        self.view.backgroundColor   = UIColor.clear
        let blurEffect              = UIBlurEffect(style: .extraLight)
        let blurEffectView          = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame        = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        
        // Creating tap action for card recharge view
        let tapGesture                                  = UITapGestureRecognizer(target: self, action: #selector(tapOnCardRechargeView))
        tapGesture.numberOfTapsRequired                 = 1
        metroCardRechargeView.isUserInteractionEnabled  = true
        metroCardRechargeView.addGestureRecognizer(tapGesture)
        
        // Creating tap action for metro token view
        let tapGesture1                                 = UITapGestureRecognizer(target: self, action: #selector(tapMetroTokenView))
        tapGesture1.numberOfTapsRequired                = 1
        metroTokenView.isUserInteractionEnabled         = true
        metroTokenView.addGestureRecognizer(tapGesture1)
        
        // Creating tap action for metro trip pass view
        let tapGesture2                                 = UITapGestureRecognizer(target: self, action: #selector(tapOnMetroTripPass))
        tapGesture2.numberOfTapsRequired                = 1
        metroTripPassView.isUserInteractionEnabled      = true
        metroTripPassView.addGestureRecognizer(tapGesture2)
        
        // Creating tap action for metro stored value view
        let tapGesture3                                    = UITapGestureRecognizer(target: self, action: #selector(tapOnStoredValuePass))
        tapGesture3.numberOfTapsRequired                   = 1
        metroStoredValuePass.isUserInteractionEnabled      = true
        metroStoredValuePass.addGestureRecognizer(tapGesture3)
    }
    
    func tapOnCardRechargeView(){
        uiFun.writeData(value: "Metro", key: "ticketingAgency")
        uiFun.navigateToScreem(identifierName: "MetroCardViewController", fromController: self)
    }
    
    func tapMetroTokenView(){

        tokenOriginStationName          = "Origin Station"              // origin station name
        tokenDestinationStationName     = "Destination Station"
        
        uiFun.writeData(value: "MetroToken", key: "ticketingAgency")
        
        metroFromWhere                  = "token"
        metroTokenJourneyType           = uiFun.readData(key: "fetchingJourneyType")
        let metroTokenString            = uiFun.readData(key: "companyReferenceId")
        journeyType                     = uiFun.readData(key: "journeyType")
        print("Metro Token Database Value \(metroTokenString)")

        // if token identifier is present in data then call inventory items to check the token status
        if(metroTokenString != ""){
            
            let metroTokenList  = DBManagerToken.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
            print("Token Count \(metroTokenList.count)")
            
            // check if metroTokenList count is zero navigate to buy new token screen
            if(metroTokenList.count == 0){
                uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
            }
            
            // making an api call for each slave qr code
            for i in 0..<metroTokenList.count{
                
                var itemType    = ""
                // making an api call for inventory items
                print("QRCOde ID is \(metroTokenList[i].newMetroToken[0].qrCodeId)")
                uiFun.showIndicatorLoader()
                let inventoryItems  = InventoryItems()
                
                if(metroTokenList[i].newMetroToken[0].type == "PENALTY"){
                    itemType    = "PENALTY"
                }else{
                    itemType    = "TICKET"
                }
                
                inventoryItems.makeAPICall(itemSubtype: journeyType, identifier: metroTokenList[i].newMetroToken[0].qrCodeId,item: itemType, fromViewController: self)
            }
        
        }else{
            
            checkProfileStatus(navigateToVC: "MetroGetTokenViewController")
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
    func tapOnMetroTripPass(){
    
         uiFun.writeData(value: "metroTrip", key: "ticketingAgency")
         metroFromWhere             = "trip"
         metroTokenJourneyType      = "TRIP_PASS"
         var slaveQRCode            = ""
        //check whether user have trip pass or not
         let metroTokenList          = DBManagerToken.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
         let isTripPassConfigured    = uiFun.readData(key: "metroTripStatus")
         if(metroTokenList.count != 0){
            slaveQRCode             = metroTokenList[0].newMetroToken[0].qrCodeId//uiFun.readData(key: "tripPassSlaveQRCode")
         }else{
            slaveQRCode = ""
         }
        
        
        if(slaveQRCode != ""){
            
            uiFun.showIndicatorLoader()
            
            var itemType    = ""
            
            if(metroTokenList[0].newMetroToken[0].type == "PENALTY"){
                itemType    = "PENALTY"
            }else{
                itemType    = "PASS"
            }
            
            let inventoryItems          = InventoryItems()
            inventoryItems.makeAPICall(itemSubtype: "TRIP_PASS", identifier: slaveQRCode,item: itemType, fromViewController: self)
        
        }else{
            
            // is user have trip pass directly show him the pass screen
            if(isTripPassConfigured == "yes"){
                uiFun.navigateToScreem(identifierName: "TripPassReceiptViewController", fromController: self) // TripPassReceiptViewController
            }else{
                //uiFun.navigateToScreem(identifierName: "MetroTripViewController", fromController: self)
                checkProfileStatus(navigateToVC: "MetroTripViewController")
            }
            
        }
    }
    
    func tapOnStoredValuePass(){
        
        uiFun.writeData(value: "STORE_VALUE_PASS", key: "ticketingAgency")
        metroTokenJourneyType               = "STORE_VALUE_PASS"
        var slaveQRCode                     = ""
        
        let metroTokenList                  = DBManagerToken.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
        let isStoreValuePassConfigured      = uiFun.readData(key: "metroStoreValuePass")
                
        if(metroTokenList.count != 0){
            slaveQRCode             = metroTokenList[0].newMetroToken[0].qrCodeId//uiFun.readData(key: "tripPassSlaveQRCode")
        }else{
            slaveQRCode = ""
        }
        
        if(slaveQRCode != ""){
            
            uiFun.showIndicatorLoader()
            
            var itemType    = ""
            
            if(metroTokenList[0].newMetroToken[0].type == "PENALTY"){
                itemType    = "PENALTY"
            }else{
                itemType    = "PASS"
            }
            
            let inventoryItems          = InventoryItems()
            inventoryItems.makeAPICall(itemSubtype: "STORE_VALUE_PASS", identifier: slaveQRCode,item: itemType, fromViewController: self)

        }else{
            
            if(isStoreValuePassConfigured == "yes"){
                uiFun.navigateToScreem(identifierName: "StoreValueReceiptViewController", fromController: self)
            }else{
                //uiFun.navigateToScreem(identifierName: "MetroStoredValueViewController", fromController: self)
                checkProfileStatus(navigateToVC: "MetroStoredValueViewController")
            }
            
        }
    }
    
    func checkProfileStatus(navigateToVC : String){
    
        let profileStatus   = uiFun.readData(key: "ProfileCreation")
        
        if(profileStatus == "done"){
            uiFun.navigateToScreem(identifierName: navigateToVC, fromController: self)
        }else{
            uiFun.createCleverTapEvent(message: "Profile creation touchpoints Metro Token")
            profileBackNavigation = "MetroHomeScreenViewController"
            uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
        }
    }
    
    func gotInventoryResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        if(String(describing: response["code"]) == "5000"){
            print("Got the inventory item response \(response)")
            // navigate to token screen since user already have token
        
            paymentResponseListWebview.removeAll() // clearing the list so other api response shouldn't get piled up
            
            let paymentResponseModel    = PaymentReesponseModel()
            let paymentDataWebView      = PaymentDataWebView()
            let bookingDetailResponse   = BookingDetailResponse()
            var masterQRCode            = ""
        
            let data        = response["data"]              // token information on the response received
            
            // storing response offline for trip pass use
            let offlineData            = NSKeyedArchiver.archivedData(withRootObject: tokenArray)
            UserDefaults.standard.set(offlineData, forKey: "tokensOffline")
     
            // adding model information into gloabal list
            paymentDataWebView.bookingDetails.append(bookingDetailResponse)
            paymentResponseModel.data.append(paymentDataWebView)
            paymentResponseListWebview.append(paymentResponseModel)
            
            for i in 0..<data.count{
                
                let tokensJSON  = data.arrayValue[i]["tokens"]          // qrcode data
                let itemAttr    = data.arrayValue[i]["itemAttribute"]
                    
                    for j in 0..<tokensJSON.count{
                        // storing qrcode data into model
                        if(String(describing: tokensJSON.arrayValue[j]["status"]) != "COMPLETED" && String(describing: tokensJSON.arrayValue[j]["status"]) != "EXPIRED"){
                            
                            let routeJSON                       = data.arrayValue[0]["bookingItemRoutesList"]
                            // storing journey information into global variables
                            tokenDestinationStationName         = String(describing: routeJSON.arrayValue[0]["destinationName"])
                            tokenOriginStationName              = String(describing: routeJSON.arrayValue[0]["sourceName"])
                            bookingDetailResponse.itemSubType   = String(describing :data.arrayValue[0]["itemSubType"])
                           // metroTokenJourneyType               = String(describing :data.arrayValue[0]["itemSubType"])
                            rechargeAmount                      = String(describing :data.arrayValue[0]["totalAmount"])
        
                            processInventoryResponse(response: response)
                            //uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
                        }
                        else{
                            
                            masterQRCode        = String(describing: itemAttr["masterQrCodeId"])
                            let qrCodeID        = String(describing: tokensJSON.arrayValue[j]["qrCodeId"])
                            
                            //let isDeleted       = DBManager.shared.deleteBookingItem(bookingItemId: masterQRCode)
                            let isTokenDeleted  = DBManagerToken.shared.deleteBookingItem(qrCodeId: qrCodeID)
                            print("Token With Slave Id \(qrCodeID) has been deleted \(isTokenDeleted))")
                            
                            if(String(describing :data.arrayValue[0]["itemSubType"]) == "STORE_VALUE_PASS"){
                            
                                let data                        = response["data"]
                                let itemAttribute               = data.arrayValue[0]["itemAttribute"]
                                let balance                     = String(describing: itemAttribute["balance"])
                                uiFun.writeData(value: balance, key: "storeValuePassBalance")
                                
                                if(Double(balance)! < 35.0){
                                    uiFun.navigateToScreem(identifierName: "StoreValueRechargeScreenViewController", fromController: self)
                                }
                            }
                            
                            
                            //metroTokenJourneyType = String(describing :data.arrayValue[0]["itemSubType"])
                            
                            //navigateToQRCodeScreen(response: response, result: "success")
                            
                        }
                    }
                
                }
            
                    let metroTokenList  = DBManagerToken.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
                    print("Metro Token List Count, \(metroTokenList.count)")
                    if(metroTokenList.count > 0){
                        uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
                    }else{
                        DBManager.shared.deleteBookingItem(bookingItemId: masterQRCode)
                        navigateToQRCodeScreen(response: response, result: "success")
                        //navigateToQRCodeScreen(response: response, result: "fail")
                    }
        
            }else{
            
                uiFun.hideIndicatorLoader()
                navigateToQRCodeScreen(response: response, result: "fail")
            
                let errorJSON = response["error"]
                let errorText = String(describing:errorJSON["errorCodeText"])
                uiFun.showAlert(title: "", message: errorText, logMessage: "Token Inventory Items Response", fromController: self)
        }
    }
    
    func navigateToQRCodeScreen(response : JSON, result : String){
    
       // metroTokenJourneyType = uiFun.readData(key: "journeyType")
        
        var navigateToScreen    = ""
        
        if(result == "success"){
            navigateToScreen = "MetroGetTokenViewController"
        }else{
            navigateToScreen = "TokenQRCodeViewController"
        }
        
        switch metroTokenJourneyType {
        case "SINGLE_JOURNEY":
            uiFun.navigateToScreem(identifierName: navigateToScreen, fromController: self)
            break
        case "RETURN_JOURNEY":
            uiFun.navigateToScreem(identifierName: navigateToScreen, fromController: self)
        case "TRIP_PASS":
            //processInventoryResponse(response: response)
            uiFun.writeData(value: "", key: "tripPassSlaveQRCode")
            uiFun.navigateToScreem(identifierName: "TripPassReceiptViewController", fromController: self)
            break
        case "STORE_VALUE_PASS":
            //processInventoryResponse(response: response)
            uiFun.writeData(value: "", key: "storeValueSlaveQRCode")
            uiFun.navigateToScreem(identifierName: "StoreValueReceiptViewController", fromController: self) // change it to MetroStoredValueViewController
        case "TOKEN":
            uiFun.navigateToScreem(identifierName: navigateToScreen, fromController: self)
            break
        default:
            break
        }
    }
    
    func processInventoryResponse(response : JSON){
    
        
        let data                        = response["data"]
        let route                       = data.arrayValue[0]["bookingItemRoutesList"]
        let itemAttribute               = data.arrayValue[0]["itemAttribute"]
        
        let tripModel                   = TripPassModel()
        
        tripModel.amount                = String(describing: data.arrayValue[0]["totalAmount"])
        tripModel.expiryDate            = String(describing: data.arrayValue[0]["expiryDate"])
        tripModel.bookingId             = String(describing: data.arrayValue[0]["bookingItemId"])
        tripModel.itemSubType           = String(describing: data.arrayValue[0]["itemSubType"])
        
        tripModel.originStation         = String(describing: route.arrayValue[0]["sourceName"])
        tripModel.destinationStation    = String(describing: route.arrayValue[0]["destinationName"])
        
        tripModel.masterQRCode          = String(describing: itemAttribute["masterQrCodeId"])
        tripModel.trips                 = String(describing: itemAttribute["trips"])
        
        let itemSubType                 = String(describing: data.arrayValue[0]["itemSubType"])
        
        if(itemSubType == "TRIP_PASS"){
            uiFun.writeData(value: String(describing: itemAttribute["trips"]), key: "tripPassRemainingTrip")
        }else if(itemSubType == "STORE_VALUE_PASS"){
            uiFun.writeData(value: String(describing: itemAttribute["balance"]), key: "storeValuePassBalance")
        }
        
        tripInfoList.append(tripModel)
        
    }
    
    @IBAction func newToken(_ sender: Any) {
        tokenOriginStationName      = "Origin Station"
        tokenDestinationStationName = "Destination Station"
        metroFromWhere              = "token"
        uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
    }
    
}
