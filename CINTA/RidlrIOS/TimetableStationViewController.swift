//
//  TimetableStationViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 22/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var travelModeSelectedIndex     = 0
var busSegmentSelectedIndex     = 0
var agencyId                    = ""
var routeId                     = ""
var stopId                      = ""
var tripId                      = ""
var timetableStopList           = [StopsModel]()
var BusNumberList               = [FetchBusNumberModel]()
var BusStopNameList             = [BusStops]()
var fromAtoBstopsBus            = [SourceBusStops]()

var trainStopList               = [String]()
var metroStopList1              = [String]()
var monoStopList1               = [String]()

var navigateToTripResult        = false

class TimetableStationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var stationNamesArray             = [String]()
    var tempShowArray                 = [String]()
    var trainTimeTable                = [String]()
    var filterStationNames : [String] = []
    var indicator                     = UIActivityIndicatorView()
    
    let uiFun                         = UiUtillity()
    let ridlrColors                   = RidlrColors()
    
    @IBOutlet var searchBar: UISearchBar!
    var stationName             = ""
    var searchString            = ""
    var rowHeight               = 85
    

    @IBOutlet var stationNameList   : UITableView!
    @IBOutlet var backButton        : UIButton!
    @IBOutlet var headingLabel      : UILabel!
    
    let cityParser                  = CityParser()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        stationNameList.delegate        = self
        searchBar.delegate              = self
        backButton.backgroundColor      = uiFun.hexStringToUIColor(hex: ridlrColors.ridlr_brand_color_light)
        setUI()
        fetchStopListFromCache()
        if(travelModeSelectedIndex != 1){
            
        }else if(fromWhere == "end"){
            // Fetch Selected Agency Stop List
            FetchBusStopAPICall()
        }
        
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
            // By default search text for bus number
            FetchBusNumberAPICall(inputText: "1")
        }
        if(busSegmentSelectedIndex == 1){
            // by default search text for stop name
            FetchBusStopFromAtoBAPICall(inputText: "A")
        }
        uiFun.createCleverTapEvent(message: "TIMETABLE STOP LIST SCREEN")
    }
    
    func fetchStopListFromCache(){
        // Code Optimization if stop list is already contains the stops than no need to make an api call
        switch travelModeSelectedIndex {
        case 0:
            if(trainStopList.count == 0){
                FetchStopsAPICall()     // api call
            }else{
                tempShowArray       = trainStopList   // stop list
                stationNamesArray   = tempShowArray
            }
            break
        case 2:
            if(metroStopList.count == 0){
                FetchStopsAPICall()
            }else{
                tempShowArray = metroStopList1
            }
            break
        case 3:
            if(monoStopList.count == 0){
                FetchStopsAPICall()
            }else{
                tempShowArray = monoStopList1
            }
            break
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
      
        // count of number of rows in table view
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
                if(fromWhere == "start"){
                    if(travelModeSelectedIndex == 1){
                        
                        if(busSegmentSelectedIndex == 0){
                            startingStationName     = "Search For Bus Number"
                            destinationStationName  = "Search Bus Stop"
                        }else if(busSegmentSelectedIndex == 1){
                            startingStationName         = "Start Bus Stop"
                            destinationStationName      = "Destination Bus Stop"
                        }
                    }else{
                        startingStationName     = "Starting Station"
                        destinationStationName  = "Destination Station"
                    }
                    
                    return BusNumberList.count
                    
                    
                }else if(fromWhere == "end"){
                    return BusStopNameList.count
                }
            }else if(busSegmentSelectedIndex == 1){
                return fromAtoBstopsBus.count
            }
        }
        
        return tempShowArray.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        // Initilizing table view
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TimetableStationTableViewCell
        if(travelModeSelectedIndex == 1){
            if(busSegmentSelectedIndex == 0){
            
                if(fromWhere == "start"){
                    // Selecting Starting Station name
                    if(BusNumberList[indexPath.row].fields[0].source_stop_name == ""){
           
                        let stationNames = BusNumberList[indexPath.row].fields[0].full_long_name // Stop name
                        let stationArray = stationNames.components(separatedBy: "To") // Separating station name
                
                        cell.stationName.text = BusNumberList[indexPath.row].fields[0].route_short_name // Station name
                        cell.agencyName.text  = "\(stationArray[0])"
                        cell.from.text        = "from \(stationArray[1])"   // source station name
                
                    }else{
                        
                        cell.stationName.text = BusNumberList[indexPath.row].fields[0].route_short_name     // Stop name
                        cell.agencyName.text  = "\(BusNumberList[indexPath.row].fields[0].source_stop_name)" // Agency name
                        cell.from.text        = "from \(BusNumberList[indexPath.row].fields[0].destination_stop_name)" // Destination name
                    }
                }else if(fromWhere == "end"){
                    // Selecting destination name
                    cell.stationName.text   = BusStopNameList[indexPath.row].name
                    cell.agencyName.text    = ""
                    cell.from.text          = ""
                }
            }else if(busSegmentSelectedIndex == 1){
                rowHeight             = 60
                cell.stationName.text = fromAtoBstopsBus[indexPath.row].l_stop_name
                cell.agencyName.text  = fromAtoBstopsBus[indexPath.row].l_stop_area
                cell.from.text        = ""
            }
        }else{
            rowHeight                 = 52
            cell.stationName.text     = tempShowArray[indexPath.row]//timetableStopList[0].stops[indexPath.row].name
            cell.agencyName.isHidden  = true//filterStationNames[indexPath.row]
            cell.from.isHidden        = true
        }
        cell.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Selected row or user taps on any one row in tableview
        if(travelModeSelectedIndex == 1){
            timetableStopList.removeAll()
            if(busSegmentSelectedIndex == 0){
                if(fromWhere == "start"){
                
                    agencyId = BusNumberList[indexPath.row].fields[0].agency_id
                    routeId  = BusNumberList[indexPath.row].fields[0].custom_route_id
                    uiFun.writeToDatabase(entityValue: routeId, entityName: "RecentSearch", key: "routeId")
                    uiFun.writeToDatabase(entityValue: agencyId, entityName: "RecentSearch", key: "agencyId")
                    if(BusNumberList[indexPath.row].fields[0].source_stop_name == ""){
                    
                        let stationNames    = BusNumberList[indexPath.row].fields[0].full_long_name
                        let stationArray    = stationNames.components(separatedBy: "To")
                        startingStationName = "\(BusNumberList[indexPath.row].fields[0].route_short_name) To \(stationArray[0])"
                    }else{
                        startingStationName = "\(BusNumberList[indexPath.row].fields[0].route_short_name) To \(BusNumberList[indexPath.row].fields[0].source_stop_name)"
                    }
                }else if(fromWhere == "end"){
                    navigateToTripResult   = true
                    destinationStationName = BusStopNameList[indexPath.row].name
                    stopId                 = BusStopNameList[indexPath.row].id
                    uiFun.writeToDatabase(entityValue: stopId, entityName: "RecentSearch", key: "stopId")
                }
            }else if(busSegmentSelectedIndex == 1){
            
                agencyId = fromAtoBstopsBus[indexPath.row].l_agency_id
                stopId  = fromAtoBstopsBus[indexPath.row].l_stop_id
            
                if(fromWhere == "start"){
                    startingStationName = "\(fromAtoBstopsBus[indexPath.row].l_stop_name)-\(fromAtoBstopsBus[indexPath.row].l_stop_area)"
                }else if(fromWhere == "end"){
                    navigateToTripResult = true
                    destinationStationName = "\(fromAtoBstopsBus[indexPath.row].l_stop_name)-\(fromAtoBstopsBus[indexPath.row].l_stop_area)"
            }
        }
        }else{
            if(fromWhere == "start"){
                startingStationName = tempShowArray[indexPath.row]
                
                if(travelModeSelectedIndex == 1){
                    
                    if(busSegmentSelectedIndex == 0){
                        destinationStationName  = "Search Bus Stop"
                    }else if(busSegmentSelectedIndex == 1){
                        destinationStationName      = "Destination Bus Stop"
                    }
                }else{
                    destinationStationName  = "Destination Station"
                }

            }else if(fromWhere == "end"){
                navigateToTripResult = true
                destinationStationName = tempShowArray[indexPath.row]
                
            }
        }
        uiFun.navigateToScreem(identifierName: "TimeTableViewController", fromController: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return CGFloat(Float(rowHeight))
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
            BusNumberList.removeAll()
            BusStopNameList.removeAll()
            FetchBusNumberAPICall(inputText: searchText)
        }
        if(busSegmentSelectedIndex == 1){
            fromAtoBstopsBus.removeAll()
            FetchBusStopFromAtoBAPICall(inputText: searchText)
            
        }
        filterStationNames = searchText.isEmpty ? stationNamesArray : stationNamesArray.filter({(dataString: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return dataString.range(of: searchText, options: .caseInsensitive) != nil
        })
        print("Filter station names \(filterStationNames)")
        self.tempShowArray = filterStationNames
        stationNameList.reloadData()
    }
    
    func activityIndicator() {
        // Loder Creation
        indicator.frame                      = CGRect(x: 0, y: 0, width: 60, height: 60)
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        indicator.color                      = UIColor.black
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func setUI(){
        
        activityIndicator()
        stationNameList.rowHeight          = UITableViewAutomaticDimension
        stationNameList.estimatedRowHeight = 85                 // tableview row height
        searchBar.tintColor                = UIColor.gray
        
        // Setting starting and destination label text respective of the agencies
        switch travelModeSelectedIndex {
        case 0:
            headingLabel.text = "All Train Stops"
            searchBar.placeholder = "Type Your Station Name Here"
            break
        case 1:
            headingLabel.text = "All Bus"
            if(busSegmentSelectedIndex == 0){
                if(fromWhere == "end"){
                    searchBar.placeholder = "Search For Bus Stop"
                }else{
                    searchBar.placeholder = "Search For Bus Number"
                }
            }else{
                searchBar.placeholder = "Search For Bus Stop"
            }
            break
        case 2:
            headingLabel.text = "All Metro Station"
            searchBar.placeholder = "Type Your Station Name Here"
            break
        case 3:
            headingLabel.text = "All Mono Station"
            searchBar.placeholder = "Type Your Station Name Here"
            break
        default:
            break
        }
    }
    
    func FetchStopsAPICall(){
        // Api call for fetching stop list of train, metro and mono
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear

        let fetchStopsAPI         = FetchStopsAPI()
        fetchStopsAPI.makeAPICall(fromViewController: self, cityId: cityIdFromData, modeId: timetableModeId)
    }
    
    func FetchBusNumberAPICall(inputText : String){
        // API call for fetching bus number list
        let fetchBusNumberAPI = FetchBusNumberAPI()
        fetchBusNumberAPI.makeAPICall(fromViewController: self, searchString: inputText)
    }
    
    func FetchBusStopFromAtoBAPICall(inputText : String){
        // Search bus stop name using text api call
        let busStopAtoBAPI = FetchBusStopsFromAtoBAPI()
        busStopAtoBAPI.makeAPICall(fromViewController: self, searchString: inputText, agencyId: agencyId)
    }
    
    func FetchBusStopAPICall(){
        // API call for fetching bus stop name list
        let fetchBusStop = FetchBusStopsAPI()
        fetchBusStop.makeAPICall(fromViewController: self, agencyId: agencyId, routeId: routeId)
    }
    
    func FetchBusStopResponseFromAtoB(response : JSON){
        
        print("bus stop response from a to b \(response)")
        fetchBusStopFromAtoB(response: response)
    }
    
    func FetchStopAPIResponse(response : JSON){
        // Stop List API Response
        uiFun.hideIndicatorLoader()
        if(String(describing: response["statusmessage"]) == "SUCCESS"){
            processStopModel(response: response)    // Processing api response
        }else{
            // JSON response contains failure show the alert dialog
            uiFun.showAlert(title: "", message: "There is some problem in connecting. Please Try Again", logMessage: "Failed in Fetching timetable stops", fromController: self)
        }
        
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
    }
    
    func FetchBusNumberResponse(response : JSON){
        // Bus Number Stops Response
        print("Got Bus Number response \(response)")
        processBusStopResult(response: response)
    }
    
    func FetchBusStopResponse(response : JSON){
        // Bus stop names response
        processBusStopResultJSON(response : response)
    }
    
    func processStopModel(response : JSON){
        // Processing json response into model
        if(travelModeSelectedIndex == 1){
            timetableStopList.removeAll()
        }
        var i               = 0
        let stopsJSON       = response["stop"]
        print("Timetable Stop JSON Count is \(stopsJSON.count)")
        
        let stopModel       = StopsModel()
        
        while(i < stopsJSON.count){
        
            let stops       = Stops()
            
            stops.name      = String(describing: stopsJSON.arrayValue[i]["name"])
            stops.lat       = String(describing: stopsJSON.arrayValue[i]["lat"])
            stops.long      = String(describing: stopsJSON.arrayValue[i]["lon"])
            
            stationNamesArray.append(String(describing: stopsJSON.arrayValue[i]["name"]))
            tempShowArray   = stationNamesArray
            
            var j = 0
            let agencyJSON  = stopsJSON.arrayValue[i]["agencyid"]
            
            while(j < agencyJSON.count){
            
                let agencyId        = AgencyId()
                agencyId.agencyId   = String(describing: agencyJSON.arrayValue[j])
                stops.agencyId.append(agencyId)
                j += 1
                
            }
           
            stopModel.stops.append(stops)
            timetableStopList.append(stopModel)     // StopList arrayList
            i += 1
        }
        saveStopListForCache()         // Saving stop list into temp varibale so we can use the list without making an api call
        stationNameList.reloadData()   // Reloading table view with stop list
        
       animateTable()                  // this will show listview with animation from bottom to top
    }
    
    
    func saveStopListForCache(){
        // Saving stop list into temp array
        switch travelModeSelectedIndex {
            case 0:
                trainStopList   = tempShowArray
                break
            case 2:
                metroStopList1  = tempShowArray
                break
            case 3:
                monoStopList1   = tempShowArray
                break
            default:
                break
        }
    }
    
    func processBusStopResult(response : JSON){
        // Processing but stop result into model
        let hitsJSON1   = response["hits"]
        let hitsJSON    = hitsJSON1["hits"]
        print("Bus Number JSON Count \(hitsJSON.count)")
        
        var i           = 0
        while(i < hitsJSON.count){
            
            let sortJSON            = hitsJSON.arrayValue[i]["sort"]
            let busNumberModel      = FetchBusNumberModel()
            
            busNumberModel.index    = String(describing: hitsJSON.arrayValue[i]["_index"])
            busNumberModel.type     = String(describing: hitsJSON.arrayValue[i]["_type"])
            busNumberModel.id       = String(describing: hitsJSON.arrayValue[i]["_id"])
            busNumberModel.score    = String(describing: hitsJSON.arrayValue[i]["_score"])
            busNumberModel.sort     = String(describing: sortJSON[0])
            
            let fieldJSON           = hitsJSON.arrayValue[i]["fields"]
            let responseString      = String(describing: fieldJSON)
           
            let fieldsModel         = FieldModel()
            
            if let data             = responseString.data(using: .utf8){
                do {
                    let tripDic     = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                    print("Keys for json \(tripDic?.keys)")
                    
                    for keys in (tripDic?.keys)!{
                        
                        let keyString = keys
                        print("Timetable Fetch Bus Number \(keyString)")
                        
                        // some of the values will be there in json response, so checking which are present.
                        switch keyString {
                        case "full_long_name":
                            fieldsModel.full_long_name     = String(describing: fieldJSON["full_long_name"].arrayValue[0])
                            break
                        case "source_stop_name":
                            fieldsModel.source_stop_name   = String(describing: fieldJSON["source_stop_name"].arrayValue[0])
                            break
                        case "route_long_name":
                            fieldsModel.route_long_name    = String(describing: fieldJSON["route_long_name"].arrayValue[0])
                            break
                        case "custom_route_id":
                            fieldsModel.custom_route_id    = String(describing: fieldJSON["custom_route_id"].arrayValue[0])
                            break
                        case "agency_id":
                            fieldsModel.agency_id          = String(describing: fieldJSON["agency_id"].arrayValue[0])
                            break
                        case "destination_stop_id":
                            fieldsModel.destination_stop_id = String(describing: fieldJSON["destination_stop_id"].arrayValue[0])
                            break
                        case "route_short_name":
                            fieldsModel.route_short_name   = String(describing: fieldJSON["route_short_name"].arrayValue[0])
                            break
                        case "source_stop_id":
                            fieldsModel.source_stop_id     = String(describing: fieldJSON["source_stop_id"].arrayValue[0])
                            break
                        case "destination_stop_name":
                            fieldsModel.destination_stop_name = String(describing: fieldJSON["destination_stop_name"].arrayValue[0])
                            break
                        default:
                            break
                        }
                        
                    }
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            busNumberModel.fields.append(fieldsModel)
            BusNumberList.append(busNumberModel)
            i += 1
        }
        stationNameList.reloadData()
    }
    
    func processBusStopResultJSON(response : JSON){
        // Processing json response into model
        let routeJSON = response["route"]
        
        let busStopModel = BusStopListModel()
        
        busStopModel.routeId        = String(describing: routeJSON["routeid"])
        busStopModel.shortname      = String(describing: routeJSON["shortname"])
        busStopModel.longname       = String(describing: routeJSON["longname"])
        busStopModel.directionid    = String(describing: routeJSON["directionid"])
        busStopModel.directionlabel = String(describing: routeJSON["directionlabel"])
        
        let stopsJSON = response["stop"]
        
        var i = 0
        
        while(i < stopsJSON.count){
        
            let busStopName = BusStops()
            
            busStopName.id   = String(describing: stopsJSON.arrayValue[i]["id"])
            busStopName.name = String(describing: stopsJSON.arrayValue[i]["name"])
            busStopName.lon  = String(describing: stopsJSON.arrayValue[i]["lon"])
            busStopName.lat  = String(describing: stopsJSON.arrayValue[i]["lat"])
            BusStopNameList.append(busStopName)
            
            i += 1
        }
        
        
        stationNameList.reloadData()
        
    }
    
    func animateTable() {
        // showing stop list with animation
        stationNameList.reloadData()
        
        let cells = stationNameList.visibleCells
        let tableHeight: CGFloat = stationNameList.bounds.size.height
        
        for i in cells {
            let cell: TimetableStationTableViewCell = i as! TimetableStationTableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: TimetableStationTableViewCell = a as! TimetableStationTableViewCell
            
            UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.allowAnimatedContent, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    func fetchBusStopFromAtoB(response : JSON){
        // processing json response into model
        let hits = response["hits"]
        let hitsJSON = hits["hits"]
        
        var i = 0
        
        let busStopAtoBModel        = BusStopFromAtoBModel()
        while(i < hitsJSON.count){
            
            
            busStopAtoBModel._index         = String(describing : hitsJSON.arrayValue[i]["_index"])
            busStopAtoBModel._type          = String(describing : hitsJSON.arrayValue[i]["_type"])
            busStopAtoBModel._id            = String(describing : hitsJSON.arrayValue[i]["_id"])
            busStopAtoBModel._score         = String(describing : hitsJSON.arrayValue[i]["_score"])
            
            let sourceModel                 = SourceBusStops()
            let sourceJSON                  = hitsJSON.arrayValue[i]["_source"]
            
            sourceModel.l_agency_id         = String(describing : sourceJSON["l_agency_id"])
            sourceModel.l_custom_stop_id    = String(describing : sourceJSON["l_custom_stop_id"])
            sourceModel.l_stop_id           = String(describing : sourceJSON["l_stop_id"])
            sourceModel.l_stop_name         = String(describing : sourceJSON["l_stop_name"])
            sourceModel.l_stop_lat          = String(describing : sourceJSON["l_stop_lat"])
            sourceModel.l_stop_lon          = String(describing : sourceJSON["l_stop_lon"])
            sourceModel.l_stop_code         = String(describing : sourceJSON["l_stop_code"])
            sourceModel.l_stop_area         = String(describing : sourceJSON["l_stop_area"])
            sourceModel.l_stop_name_index   = String(describing : sourceJSON["l_stop_name_index"])
            
            busStopAtoBModel.source.append(sourceModel)
            fromAtoBstopsBus.append(sourceModel)
            i += 1
        }
        stationNameList.reloadData()
    }
    
    

    @IBAction func backButtonTapped(_ sender: AnyObject) {
        // When user press back button 
        if(travelModeSelectedIndex == 1){
            timetableStopList.removeAll()
            
            if(busSegmentSelectedIndex == 0){
                startingStationName        = "Search For Bus Number"
                destinationStationName     = "Search Bus Stop"
            }else if(busSegmentSelectedIndex == 1){
                startingStationName        = "Start Bus Stop"
                destinationStationName     = "Destination Bus Stop"
            }
        }else{
            startingStationName            = "Starting Station"
            destinationStationName         = "Destination Station"
        }
        uiFun.navigateToScreem(identifierName: "TimeTableViewController", fromController: self)
        
    }
}
