//
//  FindARouteViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 03/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

// Global variable
var myLocation                  = "My Location"
var plannerDestinationStation   = "Destination"
var plannerFromWhere            = ""

var plannerStartingLat          : CLLocationDegrees = CLLocationDegrees()
var plannerStartingLong         : CLLocationDegrees = CLLocationDegrees()
var plannerDestinationLat       : CLLocationDegrees = CLLocationDegrees()
var plannerDestinationLong      : CLLocationDegrees = CLLocationDegrees()

var navigateToPlannerResult                         = false

var plannerStartingSubLocality                      = ""
var plannerDestinationSubLocality                   = ""

var plannerStartingStationPlaceId                   = "null"
var plannerDestinationPlaceId                       = "null"

var recentSearchJSON                                = ""
var doRecentSearch                                  = false

class FindARouteViewController: UIViewController, CLLocationManagerDelegate {

    
    @IBOutlet var myLocationLabel           : UILabel!
    @IBOutlet var destinationLabel          : UILabel!
    
    @IBOutlet var recentSearchLabel         : UILabel!
    
    @IBOutlet var setHomeLocationLabel      : UILabel!
    @IBOutlet var setOfficeLocationLabel    : UILabel!
    
    @IBOutlet var addNewFavBT               : UIButton!
    
    var locationManager = CLLocationManager()
    
    
    let uiFun           = UiUtillity()
    let ridlrColors     = RidlrColors()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        
        // fetching recent seaerch from the core data
        let recentSearch = uiFun.readFromDatabase(entityName: "RecentSearch", key: "planner1")
        if(recentSearch != ""){
            recentSearchLabel.text      = recentSearch
        }else{
            recentSearchLabel.text      = "Your Recent Search Appears Here"
        }
        
        // tap action on recent search label
        let tapGestureRecentSearch = UITapGestureRecognizer(target: self, action: #selector(recentSearchTapped))
        recentSearchLabel.isUserInteractionEnabled = true
        recentSearchLabel.addGestureRecognizer(tapGestureRecentSearch)
        
        saveRecentSearchToDatabase()
        uiFun.createCleverTapEvent(message: "FIND A ROUTE SOURCE & DESTINATION SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addNewFavBTTapped(_ sender: AnyObject) {
    
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        locationManager.stopUpdatingLocation()
        // when user taps on back button check whether navigate to traffic near you screen to homepage
        if(fromTrafficPage){
            fromTrafficPage = false
            uiFun.navigateToScreem(identifierName: "TrafficNearYouViewController", fromController: self)
        }else{
            showPlanner = true
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        }
    }
    
    func recentSearchTapped(){
        // recent search tap function
        recentSearchJSON = uiFun.readFromDatabase(entityName: "RecentSearch", key: "plannerRecentSearchJSON")
        print("Planner recentSearchJSON \(recentSearchJSON)")
        
        if(recentSearchLabel.text?.contains("Your Recent"))!{
            uiFun.showAlert(title: "", message: "Oops..! There is no recent route", logMessage: "NO Recent search recentSearchTapped()", fromController: self)
            return
        }
        // starting and destination location name from database
        let startEndTripArray       = recentSearchLabel.text?.components(separatedBy: "to")
        myLocation                  = (startEndTripArray?[0])!
        plannerDestinationStation   = (startEndTripArray?[1])!
        
        // fetching data from the core data
        plannerStartingLat          = Double(uiFun.readFromDatabase(entityName: "RecentSearch", key: "recentSearchStartLat"))!
        plannerStartingLong         = Double(uiFun.readFromDatabase(entityName: "RecentSearch", key: "recentSearchStartLong"))!
        
        plannerDestinationLat       = Double(uiFun.readFromDatabase(entityName: "RecentSearch", key: "recentSearchDestinationLat"))!
        plannerDestinationLong      = Double(uiFun.readFromDatabase(entityName: "RecentSearch", key: "recentSearchDestinationLong"))!
        
        doRecentSearch              = true
        uiFun.navigateToScreem(identifierName: "RouteSearchResult", fromController: self)   // navigate to result screen
    }
    
    func setUI(){
        // tap action on myLocationLabel
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectMyLocation))
        tapGesture.numberOfTapsRequired             = 1
        myLocationLabel.isUserInteractionEnabled    = true
        myLocationLabel.addGestureRecognizer(tapGesture)
        
        // tap action on the destination label
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(selectDestinationLocation))
        tapGesture.numberOfTapsRequired             = 1
        destinationLabel.isUserInteractionEnabled   = true
        destinationLabel.addGestureRecognizer(tapGesture1)
        
        
        myLocationLabel.text                        = myLocation
        destinationLabel.text                       = plannerDestinationStation
        
        doRecentSearch                              = false
        
        // check for whether destination station is selected than navigate to route search result screen
        if(navigateToPlannerResult){
            
            if(myLocation == "My Location"){
                uiFun.showAlert(title: "", message: "Please Select My Location", logMessage: "routeSearch", fromController: self)
                return
            }
            
            uiFun.navigateToScreem(identifierName: "RouteSearchResult", fromController: self)
        }
        
        
    }
    
    func selectMyLocation(){
        // When user clicks on the my location navigate to the LocationViewController
        plannerFromWhere = "start"
        self.uiFun.navigateToScreem(identifierName: "LocationViewController", fromController: self)
    }
    
    func selectDestinationLocation(){
        // When user clicks on the destination station navigate to the LocationViewController
        plannerFromWhere = "end"
        self.uiFun.navigateToScreem(identifierName: "LocationViewController", fromController: self)
        
    }
    
    func saveRecentSearchToDatabase(){
        // saving recent search data into core data
        if(plannerDestinationStation != "Destination"){
            
            uiFun.writeToDatabase(entityValue: "\(myLocation) to \(plannerDestinationStation)", entityName: "RecentSearch", key: "planner1")
            
            uiFun.writeToDatabase(entityValue: String(plannerStartingLat), entityName: "RecentSearch", key: "recentSearchStartLat")
            uiFun.writeToDatabase(entityValue: String(plannerStartingLong), entityName: "RecentSearch", key: "recentSearchStartLong")
            uiFun.writeToDatabase(entityValue: String(plannerDestinationLat), entityName: "RecentSearch", key: "recentSearchDestinationLat")
            uiFun.writeToDatabase(entityValue: String(plannerDestinationLong), entityName: "RecentSearch", key: "recentSearchDestinationLong")
        }
    }
}

