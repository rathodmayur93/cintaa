//
//  TimeTableMapViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 26/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class TimeTableMapViewCell: UITableViewCell {

    @IBOutlet var stopImageView: UIImageView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var stationLabel: UILabel!
    @IBOutlet var staionView: UIView!
    @IBOutlet var stationViewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        staionView.layer.cornerRadius = staionView.frame.size.width/2
        staionView.backgroundColor = UIColor.black
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
