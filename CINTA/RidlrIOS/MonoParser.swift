//
//  MonoParser.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var monoStopList = [MonoParserModel]()
class MonoParser: UIViewController {

    let uiFun = UiUtillity()
    
    func parseMonoStopsCSVToList(){
        
        let queueName = DispatchQueue(label: "stops.mono.processing")
        
        queueName.async {
            
            let csvContent          = self.uiFun.readFromCSV(fileName: "MUM Mono_stops")
            let csvNewLineContent   = csvContent.components(separatedBy: .newlines)
            let delimiter           = ","
            
            let lines:[String] = csvNewLineContent
            
            
            
            
            for line in lines {
                
                let monoStopsParserModel = MonoParserModel()
                
                let monoStopsmodel     = MonoStops()
                var monoData           = line.components(separatedBy: delimiter)
                
                if(monoData[0] != ""){
                    
                    monoStopsmodel.stopId      = monoData[0]
                    monoStopsmodel.stopName    = monoData[1]
                    monoStopsmodel.stopLat     = monoData[2]
                    monoStopsmodel.stopLong    = monoData[3]
                    
                    monoStopsParserModel.monoStops.append(monoStopsmodel)
                    monoStopList.append(monoStopsParserModel)
                }
                
            }
        }
    }
}
