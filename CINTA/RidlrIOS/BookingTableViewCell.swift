//
//  BookingTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 20/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BookingTableViewCell: UITableViewCell {

    @IBOutlet var bookingType       : UILabel!
    @IBOutlet var bookingCardNumber : UILabel!
    @IBOutlet var bookingTime       : UILabel!
    @IBOutlet var bookingAmount     : UILabel!
    @IBOutlet var bookingStatus     : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
