//
//  NewsFeedReplyModel.swift
//  RidlrIOS
//
//  Created by Mayur on 15/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class NewsFeedReplyModel{

    
    var fromUser            = ""
    var likeCount           = ""
    var location            = ""
    var postedOn            = ""
    var profileImage        = ""
    var textUpdate          = ""
    var updateId            = ""
    var updateType          = ""
    var username            = ""
    var isMyOwnPost         = ""
}
