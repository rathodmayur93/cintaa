//
//  MapDataAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 19/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class MapDataAPI {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    func makeAPICall(fromViewController : TrafficNearYouViewController, zoomLevel : Int, minLat : Double, minLong :Double, maxLat : Double, maxLong : Double){
        
        
        
        let url = "http://api.saarath.com/MobileAppsWS/MobileAppWebservice.svc/MapData?"
        
        let params : [String:AnyObject] = ["bound2":"0,0,0,0" as AnyObject,
                                           "zoom":String(describing:zoomLevel) as AnyObject,
                                           "parentBoxes":"0" as AnyObject,
                                           "bound":"\(minLat),\(minLong),\(maxLat),\(maxLong)" as AnyObject,
                                           "custKey":constant.CUST_KEY as AnyObject,
                                           "passkey":constant.PASS_KEY as AnyObject,
                                           "groupId":"ridlr" as AnyObject,
                                           "accessType":"device" as AnyObject,
                                            "userid":"-111" as AnyObject]
        
        let parameterString = params.stringFromHttpParameters()
        let mapDataUrl = "\(url)\(parameterString)"//"http://ridlr.in/callWebService_Get.php?webMethodName=MapDataAsJSonWap&parameters=bound1:\(minLat),\(minLong),\(maxLat),\(maxLong)~bound2:0,0,0,0~zoom:4"//
        
       
        print("map data url \(mapDataUrl)")
        
        util.GetAPICall(apiUrl: mapDataUrl, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.responseOfMapData(response: jsonResponse)
                print("Successfully MAP DATA Response Got.... \(jsonResponse) ")
                return
                
            }else {
                
                print("MAP DATA Response Failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
}
