//
//  AddMoneyViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 11/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import DLRadioButton
import CreditCardValidator

var redirectUrl : String?
var paymentType                     = "card"

class AddMoneyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{

    var uiFun       = UiUtillity()
    var ridlrColors = RidlrColors()
    
    @IBOutlet var cardNumber    : UITextField!
    @IBOutlet var monthYearTF   : UITextField!
    @IBOutlet var cvvTF         : UITextField!
    
    @IBOutlet var cardNumberLabel       : UILabel!
    @IBOutlet var cardExpiryDateLabel   : UILabel!
    @IBOutlet var cvvLabl               : UILabel!
    @IBOutlet var saveCardForFutureLabel: UILabel!
    
    
    @IBOutlet var proceedPaymentBT      : UIButton!
    @IBOutlet var loader                : UIActivityIndicatorView!
    
    @IBOutlet var amountToAddLabel      : UILabel!
        
    @IBOutlet var debitCB               : DLRadioButton!
    @IBOutlet var creditCB              : DLRadioButton!
    @IBOutlet var saveCardCB            : DLRadioButton!
    @IBOutlet var saveMyCardCB          : DLRadioButton!
    
    @IBOutlet var debitLable            : UILabel!
    @IBOutlet var creditLabel           : UILabel!
    @IBOutlet var saveCardlabel         : UILabel!
    @IBOutlet var selectBankPicker      : UIPickerView!
    @IBOutlet var cardImageView         : UIImageView!
    
    @IBOutlet var paymentModeSegment    : UISegmentedControl!
    @IBOutlet var saveCardTV            : UITableView!
    
    let defaults                        = UserDefaults.standard
    let constant                        = Constants()
    
    var notifyUrl                       : String?
    var returnUrl                       : String?
    var signature                       : String?
    var transactionId                   : String?
    var emailId                         : String?
    var merchantAccessKey               : String?
    var whichCard                       : String?
    
    var netBankingIssueCode             : String?
    
    var saveCardNumberArray             = [SaveCardDataClass]()
    var netBankingBankArray             = [NetBankingModel]()
    
    var netBankingLabel     : UILabel   = UILabel()
    var netBankingSeparator : UIView    = UIView()
    
    var slectedCardType                 : CreditCardValidationType!
    var isLoaderVisible                 = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("/***** WALLET ADD MONEY SCREEN *******/")
        
        paymentType         = "card"
        let fetchSaveCard   = CitrusFetchSaveCardDetailAPI()    // Api call for fetching save cards and banks name
        fetchSaveCard.makeAPICall(fromViewController: self)   // Response is in saveCardAPIResponse() method
        setUI()
        netBankingUI()
        
        self.selectBankPicker.delegate      = self
        self.selectBankPicker.dataSource    = self
        
        cardNumber.delegate                 = self
        monthYearTF.delegate                = self
        uiFun.createCleverTapEvent(message: "CITRUS WALLET ADD MONEY SCREEN")
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if(isLoaderVisible){
            uiFun.showIndicatorLoader() // Showing loader
        }
    }
    
    func setUI(){
    
        // Proceed Payment Button Title and background color
        proceedPaymentBT.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        proceedPaymentBT.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        
        amountToAddLabel.text            = amountToAddValue
        
        // Each TextField Keyboard Type
        cardNumber.keyboardType          = UIKeyboardType.numberPad
        monthYearTF.keyboardType         = UIKeyboardType.numberPad
        cvvTF.keyboardType               = UIKeyboardType.numberPad
        saveCardTV.isHidden              = true
        netBankingLabel.isHidden         = true
        netBankingSeparator.isHidden     = true
        
        selectBankPicker.isHidden        = true
        
        // When user type anything "checkCardNumber" and "checkYearMonth" method gets executed.
        cardNumber.addTarget(self, action: #selector(checkCardNumber), for: UIControlEvents.editingChanged)
        monthYearTF.addTarget(self, action: #selector(checkYearMonth), for: UIControlEvents.editingChanged)
    }
    
    /************ DETECTING KEYBOARD BACKPRESS EVENT *****************/
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let  char       = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            
            // If blank space occured while pressing backpress it shoudl remove two characters. Card Number
            if(cardNumber.isFirstResponder){
                if(range.location % 5 == 0 && (textField.text?.characters.count)! > 1){
                    let index  = textField.text?.characters.index((textField.text?.startIndex)!, offsetBy: (range.location - 1))
                    print("Text Field Index \(textField.text?[index!])")
                
                    if(textField.text?[index!] == " "){
                        textField.text?.remove(at: (textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location))!)
                    }
                }
            }
            
            // Expiry Date Logic while pressing back space
            if(monthYearTF.isFirstResponder){
                if((textField.text?.characters.count)! > 1){
                
                    let index  = textField.text?.characters.index((textField.text?.startIndex)!, offsetBy: (range.location))
                    // Inserting "/" while entering month and year
                    if(textField.text?[index!] == "/"){
                        print("Text Field Index 1201 \(textField.text?[index!])")
                        textField.text?.remove(at: (textField.text?.index((textField.text?.startIndex)!, offsetBy: range.location - 1))!)
                        print("In Expiry Date")
                    }
                }
            }
        }
        return true
    }
    
    func netBankingUI(){
    
        // Netbankng selecet bank label position, color, alignment and text
        netBankingLabel.frame       = CGRect(x: 16, y: 300, width: Int(self.view.frame.width - 32), height: 30)
        netBankingLabel.textColor   = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrTextColor)
        netBankingLabel.textAlignment = NSTextAlignment.center
        netBankingLabel.text        = "Select Bank"
        self.view.addSubview(netBankingLabel)
        
        // When user taps on net banking label
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectBankLabelAction))
        netBankingLabel.addGestureRecognizer(tap)
        tap.delegate = self
        
        // Thin solid line which separates the label and bank picker
        netBankingSeparator.frame = CGRect(x: 16, y: 324, width: self.view.frame.width - 32, height: 1)
        netBankingSeparator.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
        self.view.addSubview(netBankingSeparator)
    }
    
    
    func selectBankLabelAction(){
        // Shows the bank picker.
        print("Select Bank Label Tapped")
        
    }
    
    
    func checkCardNumber(){
        // Validate the credit or debit card number
        if((cardNumber.text?.characters.count)! > 19){
            cardNumber.deleteBackward()
        }
        if(((cardNumber.text?.characters.count)! % 5) == 0 && (cardNumber.text?.characters.count)! > 4){
            cardNumber.text?.insert(" ", at: (cardNumber.text?.index((cardNumber.text?.startIndex)!, offsetBy: (cardNumber.text?.characters.count)! - 1))!)
            
            
        }
        let creditCardValidator = CreditCardValidator() // Third party lib for card validation
        
        if(creditCardValidator.validate(string: (cardNumber.text?.replacingOccurrences(of: " ", with: ""))!)){
            
            // Fetch the card type i.e VISA, MASTER etc etc and show the respective image on card text field
            slectedCardType = creditCardValidator.type(from: (cardNumber.text?.replacingOccurrences(of: " ", with: ""))!)
            print("Selected Credit Card Validator \(slectedCardType.name)")
            
            switch slectedCardType.name {
            case "Amex":
                cardImageView.image = UIImage(named: "ic_card_amex")
                break
            case "Visa":
                cardImageView.image = UIImage(named: "ic_card_visa")
                break
            case "MCRD":
                cardImageView.image = UIImage(named: "ic_card_master")
                break
            case "Discover":
                cardImageView.image = UIImage(named: "ic_card_discover")
                break
            case "DINERS":
                cardImageView.image = UIImage(named: "ic_card_dinners")
                break
            case "JCB":
                cardImageView.image = UIImage(named: "ic_card_jcb")
                break
            case "mtro":
                cardImageView.image = UIImage(named: "ic_card_maestro")
                break;
            default:
                break
            }
        }
    }
    
    func checkYearMonth(){
        // Validating Expiry Date of Credit or Debit Card
        if((monthYearTF.text?.characters.count)! > 7){
            monthYearTF.deleteBackward()
        }
        
        if(monthYearTF.text?.characters.count == 2){
            monthYearTF.text?.append("/")
        }
        
    }
    /************************** BANK LIST PICKER VIEW METHODS****************************/
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print("Picker Data \(netBankingBankArray.count)")
        return netBankingBankArray.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        print("Picker View \(netBankingBankArray[row].bankName)")
        return netBankingBankArray[row].bankName
        
    }
    
    // The number of columns of data
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
       
        netBankingLabel.text = netBankingBankArray[row].bankName
        netBankingIssueCode  = netBankingBankArray[row].issuerCode
        self.view.endEditing(true)
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
    
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        let data                = netBankingBankArray[row].bankName    // Showing the bank name from the netbankingArray
        let title               = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0, weight: UIFontWeightRegular)])
        label?.attributedText   = title
        label?.textColor        = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        label?.textAlignment    = .center
        return label!
    }
    
    
    /********************* SAVE CARD TABLE VIEW FUNCTIONS ************************/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return saveCardNumberArray.count // Count of save cards
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell                    = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") // Intializing the cell
        cell.selectionStyle         = UITableViewCellSelectionStyle.blue
        let cardNumber              = saveCardNumberArray[indexPath.row]
        cell.textLabel?.text        = cardNumber.saveCardNumber
        cell.textLabel?.textColor   = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(saveCardNumberArray[indexPath.row].token)
        
        let alert = UIAlertController(title: "Confirmation", message: "Please Enter CVV Number of card \(saveCardNumberArray[indexPath.row].saveCardNumber)", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = "CVV"
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let cvvText = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if(cvvText?.text != nil){
                
                let saveCardPayment = CitrusSaveCardAPI()
                saveCardPayment.makeAPICall(fromViewController: self, token: self.saveCardNumberArray[indexPath.row].token, cvv: (cvvText?.text)!, amount: self.amountToAddLabel.text!) // Response is in saveCardPaymentResponse() function
            }
            print("Text field: \(cvvText?.text)")
            
            }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (_) in })
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func proceedPaymentAction(_ sender: AnyObject) {
        uiFun.showIndicatorLoader()
        let citrusAddMoney = CitrusAddMoneyAPI()
        if(paymentType == "card"){
       
            if(whichCard != nil){
                
                if(checkCardExpiry()){
                    //API Response is in citrusAddMoneyResponse() this function
                    citrusAddMoney.makeAPICall(fromViewController: self, monthYear: monthYearTF.text!, cardNumber: cardNumber.text!.replacingOccurrences(of: " ", with: ""), amount: amountToAddLabel.text!, whichCard: whichCard!)
                }else{
                    uiFun.hideIndicatorLoader()
                }
        }else{
            uiFun.hideIndicatorLoader()
            uiFun.showAlert(title: "Error", message: "Please Select Card Type", logMessage: "Card type is not selected", fromController: self)
            }
        }else if(paymentType == "netBanking"){
        
            // Response is in NetBankingResponse()
            
            if(netBankingLabel.text != "Select Bank"){
                let netBanking = CitrusNetBankingAPI()
                netBanking.makeAPICall(fromViewController: self, issuerCode: netBankingIssueCode!, amount: amountToAddLabel.text!)
            }else{
                uiFun.hideIndicatorLoader()
                uiFun.showAlert(title: "", message: "Please select bank", logMessage: "please select bank", fromController: self)
            }
        }
    }
    
    
    func checkCardExpiry() -> Bool{
        
        let splitExpiry         = monthYearTF.text?.components(separatedBy: "/")
        let month               = splitExpiry?[0]
        let year                = splitExpiry?[1]
        
        if(monthYearTF.text?.contains("//"))!{
            uiFun.showAlert(title: "", message: "Please Enter Valid Expiry Month", logMessage: "Invalid Expiry Month", fromController: self)
            return false
        }else if(year?.characters.count != 4){
            uiFun.showAlert(title: "", message: "Please Enter Valid Expiry Year In YYYY Format", logMessage: "Invalid Expiry Year", fromController: self)
            
            return false
        }else if(month?.characters.count == 1){
             uiFun.showAlert(title: "", message: "Please Enter Valid Expiry Month in MM Format", logMessage: "Invalid Expiry Month", fromController: self)
            return false
        }else{
            return true
        }
        
    }
    
    @IBAction func debitCBTapped(_ sender: AnyObject) {
        showCardLayout()
        creditCB.isSelected     = false
        saveCardCB.isSelected   = false
        whichCard               = "DEBIT_CARD"
    }
    
    @IBAction func creditCBapped(_ sender: AnyObject) {
        showCardLayout()
        debitCB.isSelected      = false
        saveCardCB.isSelected   = false
        whichCard               = "CREDIT_CARD"
    }
    
    @IBAction func saveCardCBTapped(_ sender: AnyObject) {
        hideCardLayout()
        saveCardTV.isHidden     = false
        debitCB.isSelected      = false
        creditCB.isSelected     = false
        
        saveCardTV.separatorStyle = UITableViewCellSeparatorStyle.none
        UITableView_Auto_Height()
    }
    
    @IBAction func saveMyCardCBTapped(_ sender: AnyObject) {
        
    }

    @IBAction func backButton(_ sender: Any) {
        // Checks whether navigate to profile add money screen or payment page
        if(isFromProfileWallet){
            isFromProfileWallet = false
            uiFun.navigateToScreem(identifierName: "AddMoneyIntoWalletViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }
    }
    
    func UITableView_Auto_Height()
    {
        // Sets height of the table view row
        if(saveCardTV.contentSize.height < saveCardTV.frame.height){
            var frame: CGRect   = saveCardTV.frame;
            frame.size.height   = saveCardTV.contentSize.height;
            saveCardTV.frame    = frame;
        }
    }
    
    @IBAction func paymentModeSegmentAction(_ sender: AnyObject) {
        
        switch paymentModeSegment.selectedSegmentIndex {
        case 0:
            showEverything()
            netBankingLabel.isHidden        = true
            netBankingSeparator.isHidden    = true
            selectBankPicker.isHidden       = true
            paymentType = "card"
            break
        case 1:
            hideEverything()
            netBankingLabel.isHidden     = false
            netBankingSeparator.isHidden = false
            paymentType = "netBanking"
            selectBankPicker.isHidden    = false
            break
        default:
            break
        }
    }
    
    
    
    func citrusPaymentGateway(){
    // Api call is in this class since for creating json string need too many data from this class and parsing that much to data is not good idea. so api call is in this class!
        
        do {
            // Crating json string for api call
            if let jsonString = citrusPGJSONString(){
                
                print("CITRUS PG JSON BODY REQUEST \(jsonString)")
                // create post request
                let url = NSURL(string: "\(constant.CITRUS_PG)")! // URL for citrus payment gateway
                print("citrus PG Gateway url \(url)")
                
                let defaults    = UserDefaults.standard
                let ridlrToken  = defaults.string(forKey: "ridlrToken") // Fetching ridlr token from parmanent data storage
                print("ridlr token citrus add money \(ridlrToken!)")
                
                let request         = NSMutableURLRequest(url: url as URL) // Creating URL Request
                request.httpMethod  = "POST"
                
                // insert json data to the request
                request.httpBody = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true)
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                print("BODY REQUEST" + String(describing: request.httpBody))
                
                
                let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                    if error != nil{
                        // Error while calling api
                        self.uiFun.hideIndicatorLoader()
                        print("Certificate Error -> \(error!)")
                        // if api call is unsuccessful than show alert depending on the error
                        if(error.debugDescription.contains("timed out")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("connection was lost")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("appears to be offline")){
                            self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                        }
                        print("Certificate Error -> \(error!)")
                        return
                    }else{
                        
                        if let urlContent = data{
                            // API call successfull
                            do {
                                self.uiFun.hideIndicatorLoader()
                                let jsonResult  = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                                
                                print("Citrus payment gateway api response \(jsonResult)")
                                
                                let jsonResponse = JSON(jsonResult)
                                
                                redirectUrl      = String(describing: jsonResponse["redirectUrl"]) // Fetching redirect url
                                if(redirectUrl != ""){
                                    
                                    // Redirecting to the webview for payment gateway
                                    print("Citrus PG Redirect URL \(jsonResponse["redirectUrl"])")
                                    self.uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
                                    self.uiFun.hideIndicatorLoader()
                                }else{
                                    // Show Error in alert
                                    self.uiFun.showAlert(title: "", message: String(describing: jsonResponse["txMsg"]), logMessage: "citrusPaymentGateway()", fromController: self)
                                }
                            }
                            catch{
                                // Failed to convert json
                                self.uiFun.hideIndicatorLoader()
                                self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "citrusPaymentGateway()", fromController: self)
                                print("Failed To Convert JSON")
                            }
                        }
                    }
                }
                task.resume()
            }
        }
    }
    
    func citrusPGJSONString() -> String?{
        
        // Citrus Payment Gateway JSON Body
        var paymentCardType = ""
        
        if(whichCard == "CREDIT_CARD"){
            paymentCardType = "credit"
        }else{
            paymentCardType = "debit"
        }
        
        
        let amount:NSMutableDictionary = NSMutableDictionary()
        amount.setValue("INR", forKey: "currency")
        amount.setValue("\(amountToAddLabel.text!)", forKey: "value") //\(amountToAddLabel.text).0
        
        print("Amount to add in citrus \(String(describing: Float(amountToAddLabel.text!)!))")
        
        let paymentMode:NSMutableDictionary = NSMutableDictionary()
        paymentMode.setValue(cvvTF.text, forKey: "cvv")
        paymentMode.setValue(monthYearTF.text, forKey: "expiry")
        paymentMode.setValue("", forKey: "holder")
        paymentMode.setValue(cardNumber.text?.replacingOccurrences(of: " ", with: ""), forKey: "number")
        paymentMode.setValue("MCRD", forKey: "scheme")
        paymentMode.setValue(paymentCardType, forKey: "type")
        
        let paymentToken:NSMutableDictionary = NSMutableDictionary()
        paymentToken.setValue(paymentMode, forKey: "paymentMode")
        paymentToken.setValue("paymentOptionToken", forKey: "type")
        
        let address:NSMutableDictionary = NSMutableDictionary()
        address.setValue("Mumbai", forKey: "city")
        address.setValue("India", forKey: "country")
        address.setValue("Maharashtra", forKey: "state")
        address.setValue("streetone", forKey: "street1")
        address.setValue("streettwo", forKey: "street2")
        address.setValue("400052", forKey: "zip")
        
        let userDetail:NSMutableDictionary = NSMutableDictionary()
        userDetail.setValue(address, forKey: "address")
        userDetail.setValue(uiFun.readData(key: "userEmailId"), forKey: "email")
        userDetail.setValue("Tester", forKey: "firstName")
        userDetail.setValue("Citrus", forKey: "lastName")
        userDetail.setValue(uiFun.readData(key: "userMobileNo"), forKey: "mobileNo")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(amount, forKey: "amount")
        para.setValue(self.merchantAccessKey, forKey: "merchantAccessKey")
        para.setValue(self.transactionId, forKey: "merchantTxnId")
        para.setValue(self.notifyUrl, forKey: "notifyUrl")
        para.setValue(paymentToken, forKey: "paymentToken")
        para.setValue("MSDKG", forKey: "requestOrigin")
        para.setValue(self.signature, forKey: "requestSignature")
        para.setValue(self.returnUrl, forKey: "returnUrl")
        para.setValue(userDetail, forKey: "userDetails")
        
        uiFun.writeData(value: self.merchantAccessKey!, key: "merchantAccessKey")
        uiFun.writeData(value: self.transactionId!, key: "merchantTxnId")
        uiFun.writeData(value: self.notifyUrl!, key: "notifyUrl")
        uiFun.writeData(value: self.signature!, key: "requestSignature")
        uiFun.writeData(value: self.returnUrl!, key: "returnUrl")
        
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            // Creating String from Dictionary
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("citrus add money json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    
    /*********************** API RESPONSE FUNCTIONS ********************************/
    func saveCardAPIResponse(response:JSON){
        
        isLoaderVisible     = false
        uiFun.hideIndicatorLoader()
        print("Architecture Fetch Save Card API Response \(response)")
        if(response["status"] == "OK"){
            self.loader.isHidden = true
            
            let saveCardData            = response["data"]
            let saveCardPaymentOption   = saveCardData["paymentOptions"].arrayValue[4]
            let saveCardDetail          = saveCardPaymentOption["details"]
            let saveCards               = saveCardDetail["savedCards"]
            
            let netBankingPaymentOption = saveCardData["paymentOptions"].arrayValue[2]
            let netBankingDetail        = netBankingPaymentOption["details"]
            let banks                   = netBankingDetail["banks"]
            
            print("Save card payment option \(saveCards) and length is \(saveCards.count)")
            print("Net Banking payment option \(banks) and length is \(banks.count)")
            
            var i = 0
            
            while i < saveCards.count{
                
                let saveCardObj = SaveCardDataClass()
                saveCardObj.saveCardNumber = String(describing: saveCards.arrayValue[i]["number"])
                saveCardObj.token = String(describing: saveCards.arrayValue[i]["token"])
                saveCardNumberArray.append(saveCardObj)
                i += 1
            }
            saveCardTV.reloadData()
            
            
            while i < banks.count {
            
                let netBankObj = NetBankingModel()
                netBankObj.bankName = String(describing: banks.arrayValue[i]["bankName"])
                netBankObj.issuerCode = String(describing: banks.arrayValue[i]["issuerCode"])
                netBankingBankArray.append(netBankObj)
                print("Bank Name : \(netBankObj.bankName)")
                i += 1
            }
            selectBankPicker.reloadAllComponents()
            print("Duao me yaad rakhna \(netBankingBankArray)")
            
            
            
        
        }
        else
        {
            self.loader.isHidden = true
            if(String(describing :response["errorCodeText"]) == "" || String(describing :response["errorCodeText"]) == "null"){
                uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
            }else{
                self.uiFun.showAlert(title: "Error", message: String(describing: response["errorCodeText"]), logMessage: "saveCardAPIResponse()", fromController: self)
            }
        }
        
        
    }
    
    func citrusAddMoneyResponse(response:JSON) {
        
        print("Citrus Add Money Response \(response)")
        
        if(response["status"] == "OK"){
            print("Citrus Add Money Response Data \(response["data"])")
            let jsonData = response["data"]
            print("User Profile Auth Method Data \(jsonData)")
            
            self.notifyUrl = String(describing: jsonData["notifyUrl"])
            self.returnUrl = String(describing: jsonData["returnUrl"])
            self.signature = String(describing: jsonData["signature"])
            self.transactionId = String(describing: jsonData["transactionId"])
            self.merchantAccessKey = String(describing: jsonData["merchantAccessKey"])
            print("Citrus key values are \(self.notifyUrl)! \(self.returnUrl)! \(self.signature)! \(self.transactionId) \(self.merchantAccessKey)")
            
            citrusPaymentGateway()
        }else{
            uiFun.hideIndicatorLoader()
            if(String(describing :response["errorCodeText"]) == "" || String(describing :response["errorCodeText"]) == "null"){
                uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
            }else{
                self.uiFun.showAlert(title: "Error", message: String(describing: response["errorCodeText"]), logMessage: "citrusAddMoneyResponse()", fromController: self)
            }
           
        }
        //uiFun.hideIndicatorLoader()
    }
    
    func citrusPGResponse(response : JSON){
        print("Citrus Payment Gateway response \(response)")
    }
    
    func saveCardPaymentResponse(response : JSON){
        print("Save card paymnet response \(response)")
    }
    
    func NetBankingResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        print("Net banking response \(response)")
        if(response["status"] == "OK"){
        
            let netBankingData = response["data"]
            redirectUrl = String(describing: netBankingData["redirectUrl"])
            print("Citrus Add Money Redirect URL\(redirectUrl)")
            self.uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
            
            
        }else{
            self.loader.isHidden = true
            if(response["errorCodeText"] == "" || response["errorCodeText"] == "null"){
                uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
            }else{
                self.uiFun.showAlert(title: "Error", message: String(describing: response["errorCodeText"]), logMessage: "NetBankingResponse()", fromController: self)
            }
        }
        
    }
    
    
    /*********************** COMMON FUNCTIONS ********************************/
    
    
    func hideCardLayout(){
    
        cardNumber.isHidden = true
        cvvTF.isHidden = true
        monthYearTF.isHidden = true
        
        cardNumberLabel.isHidden = true
        cardExpiryDateLabel.isHidden = true
        cvvLabl.isHidden = true
        saveCardForFutureLabel.isHidden = true
        
        saveMyCardCB.isHidden = true
        
    }
    
    func showCardLayout(){
        
        cardNumber.isHidden = false
        cvvTF.isHidden = false
        monthYearTF.isHidden = false
        
        cardNumberLabel.isHidden = false
        cardExpiryDateLabel.isHidden = false
        cvvLabl.isHidden = false
        saveCardTV.isHidden = true
        
    }
    
    func hideEverything(){
    
        hideCardLayout()
        debitCB.isHidden        = true
        creditCB.isHidden       = true
        saveCardCB.isHidden     = true
        
        debitLable.isHidden     = true
        creditLabel.isHidden    = true
        saveCardlabel.isHidden  = true
        cardImageView.isHidden  = true
    }
    
    func showEverything(){
    
        showCardLayout()
        debitCB.isHidden        = false
        creditCB.isHidden       = false
        //saveCardCB.isHidden   = false
        
        debitLable.isHidden     = false
        creditLabel.isHidden    = false
        cardImageView.isHidden  = false
    }
    
}
