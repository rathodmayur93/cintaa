//
//  BusTripListModel.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BusTripListModel{
    
    var routeid             = ""
    var longname            = ""
    var shortname           = ""
    var modeid              = ""
    var modename            = ""
    var hoponcount          = ""
    var report_notification = ""
    var tripcountondate     = ""
    
    var sourcestop          = [SourceStop]()
    var destinationstop     = [DestinationStop]()
    var up                  = [TripUp]()
    var down                = [TripDown]()
    
}

class SourceStop {

    var id      = ""
    var name    = ""
    var lon     = ""
    var lat     = ""
}

class DestinationStop {
    
    var id      = ""
    var name    = ""
    var lon     = ""
    var lat     = ""
}

class TripUp{

    var id              = ""
    var starttime       = ""
    var startdatetime   = ""
    var stoptime        = ""
    var stoptdateime    = ""
    var endtime         = ""
    var enddatetime     = ""
    var calender        = ""
}

class TripDown{
    
    var id              = ""
    var starttime       = ""
    var startdatetime   = ""
    var stoptime        = ""
    var stoptdateime    = ""
    var endtime         = ""
    var enddatetime     = ""
    var calender        = ""
}
