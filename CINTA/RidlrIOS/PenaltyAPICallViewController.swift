//
//  PenaltyAPICallViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 02/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PenaltyAPICallViewController {

    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(stationId : String, itemSubtype : String, identifier  :String, fromViewController : PenaltyIssueViewController){
        
        let randomNum:UInt32            = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String      = String(randomNum)
        let timeStamp                   = Int(NSDate().timeIntervalSince1970)
        
        
        let url = "\(constant.METRO_TOKEN)/inventory/penalty?station=\(stationId)&random=\(randomNumString)&company=RELIANCE_METRO&itemSubType=\(itemSubtype)&identifier=\(identifier)&item=PENALTY&current_time=\(timeStamp)"
        
        print("Metro Token Penalty Items \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                fromViewController.gotPenaltyResponse(response: jsonResponse)
                print("Successfully Token Penalty fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable Stops fetching failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
    
}
