//
//  FetchMetroStops.swift
//  RidlrIOS
//
//  Created by Mayur on 05/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchMetroStops {

    
    //http://tomcat.ridlr.in:8080/traffline-api/list/stop/cityid/1/modeid/1/userid/-9/deviceid/90fcd69416b44ac6
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : MetroStationsViewController){
        
        let url = "\(constant.TIMETABLE_STAGING_BASE_URL)list/stop/cityid/1/modeid/1/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Fetch Stops URL \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                print("Reached Success Response")
                fromViewController.gotMetroStops(response: jsonResponse)
                print("Successfully Timetable Stops fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable Stops fetching failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
}
