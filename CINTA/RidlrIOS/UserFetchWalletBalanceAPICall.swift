//
//  UserFetchWalletBalanceAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 09/05/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class UserFetchWalletBalanceAPICall {

    let util = Utillity()
    func makeAPICall(fromViewController : UserProfileViewController){
        
        let randomNum:UInt32 = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String = String(randomNum)
        let timeStamp = Int(NSDate().timeIntervalSince1970)
        
        let constant = Constants()
        let url = "\(constant.STAGING_BASE_URL)/payment/options?random=\(randomNumString)&current_time=\(timeStamp)"
        
        print("Payment Option Fetcing Balance URL \(url)")
        util.APICall(apiUrl: url, jsonString: "", requestType: "GET", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.gotResponseFromPaymentAPI(response: jsonResponse)
                print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
            }else {
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
        }
    }
}
