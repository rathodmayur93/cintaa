//
//  VerticalLayout.swift
//  DynamicView
//
//  Created by Mayur on 28/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class VerticalLayout: UIView {
    
    var yOffsets: [CGFloat] = []
    
    init(width: CGFloat) {
        super.init(frame: CGRect(x: 8, y: 6, width: (width - 32), height: 800))
        print("Vertical Layout Initialize")
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        // Vertical dynamic layout code for UIView
        var height: CGFloat = 0
        
        for i in 0..<subviews.count {
            let view = subviews[i] as UIView
            view.layoutSubviews()
            height += yOffsets[i]
            view.frame.origin.y = height
            height += view.frame.height
            print("Vertical Layout Subview \(i) and yOffset \(yOffsets[i])")
        }
        
        self.frame.size.height = height
        
    }
    
    override func addSubview(_ view: UIView) {
        
        yOffsets.append(view.frame.origin.y)
        print("Vertical Layout yOffSet \(view.frame.origin.y)")
        super.addSubview(view)
    }
    
    
    func removeAll() {
        
        for view in subviews {
            view.removeFromSuperview()
        }
        yOffsets.removeAll(keepingCapacity: false)
        
    }
    
}
