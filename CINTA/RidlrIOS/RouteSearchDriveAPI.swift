//
//  RouteSearchDriveAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 04/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class RouteSearchDriveAPI {

    var util        = Utillity()
    var constant    = Constants()
    var uiFun       = UiUtillity()
    var jsonString  = ""
    
    func makeAPICall(fromViewController : RouteSearchResult){
        
        let url = constant.ROUTE_SEARCH_DRIVE_URL
        
        jsonString = RouteSearchJSONString()!
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: false) { result, error in
            
            if let jsonResponse = result{
                
                fromViewController.routeSearchDriveAPIResponse(jsonResult: jsonResponse)
                print("Successfully Route seatch Drive API Call Repsonse \(jsonResponse) ")
                return
                
            }else {
                
                print("Route search api Drive call Failed....Try Harder Mayur ")
                return
            }
        }
    }
    
    func RouteSearchJSONString() -> String?{
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue("\(plannerDestinationLat), \(plannerDestinationLong)", forKey: "destinationLatLng")
        para.setValue(plannerDestinationStation, forKey: "destinationName")
        para.setValue("\(plannerStartingLat), \(plannerStartingLong)", forKey: "sourceLatLng")
        para.setValue(myLocation, forKey: "sourceName")
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string route search = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

    

}
