//
//  ViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 25/10/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

var fromUserProfile = false
class ViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var toolbarLabel              : UILabel!
    @IBOutlet var bookRechargeBT            : UIButton!
   
    @IBOutlet var metroRechargeBT           : UIButton!
    @IBOutlet var bussPassRchargeBT         : UIButton!
    @IBOutlet var timetableBT               : UIButton!
    @IBOutlet var findRouteBT               : UIButton!
    @IBOutlet var trafficNearYouBT          : UIButton!
    @IBOutlet var nearByStopsBT             : UIButton!
    @IBOutlet var offerButton               : UIButton!
   
    @IBOutlet var metroRechargeLabel        : UILabel!
    @IBOutlet var bussPassRechargeLabel     : UILabel!
    @IBOutlet var timetableLabel            : UILabel!
    @IBOutlet var findRouteLabel            : UILabel!
    @IBOutlet var trafficNearYouLabel       : UILabel!
    @IBOutlet var nearByStopsLabel          : UILabel!
    @IBOutlet var bookRechargeLabel         : UILabel!
    @IBOutlet var planYourTravelLabel       : UILabel!
    
    @IBOutlet var tab_indicator_view        : UIView!
    @IBOutlet var containerView             : UIView!
    
    @IBOutlet var bookRechargeIV            : UIImageView!
    @IBOutlet var plannerIV                 : UIImageView!
    
    var tabIndicatorFlag                    : Bool = false;
    var cityParserScan                             = true
    
    var window                              : UIWindow?
    var centerContainer                     : RESideMenu?

    var uiFun                               = UiUtillity()
    var ridlrColors                         = RidlrColors()
    
    let bookRechargeBackgroundLayer         = CAShapeLayer()
    let travelPlannerBackgroundLayer        = CAShapeLayer()
    
    let trafficNearYouLabelManual           : UILabel = UILabel()
    
    let defaults                            = UserDefaults.standard
    var cityName                            = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        cityName        = uiFun.readFromDatabase(entityName: "Users", key: "city")
        redirectingPage = ""
        
        let firsName = uiFun.readFromDatabase(entityName: "Users", key: "userEmail")
        print("Check it out \(firsName)")
        
        let uuid = UIDevice.current.identifierForVendor!.uuidString
        uiFun.writeData(value: uuid, key: "UUID")
        print("UUID \(uuid)")
        
        
        // MARK:- Unique device identification : Apple May be Reejct This....
        let uniqueId        = KeychainManager()
        let fetchUniqueId   = String(describing : uniqueId.keychain_valueForKey("deviceId"))
        print("Fetch UDID \(fetchUniqueId)")
        if(fetchUniqueId == "nil"){
            
            let deviceId    = uniqueId.getDeviceIdentifierFromKeychain()
            print("UDID \(deviceId)")
            uniqueId.keychain_setObject(deviceId as AnyObject, forKey: "deviceId")
        }
        
        if(cityParserList.count == 0){
            print("Sync CSV File Into List")
            let syncTimetableData = StartParsingTimetableData()
            syncTimetableData.StartSync()
        }
        
        /********** SHORTCUT ITEMS Or 3D Touch On App *************/
        
        let shortcut = UIApplicationShortcutItem(type: "com.ridlrios.metrorecharge", localizedTitle: "Metro Recharge", localizedSubtitle: "Recharge Metro Card", icon: UIApplicationShortcutIcon(type: .add), userInfo: nil)
        UIApplication.shared.shortcutItems = [shortcut]
        
        let shortcut1 = UIApplicationShortcutItem(type: "com.ridlrios.metrorecharge", localizedTitle: "BEST Recharge", localizedSubtitle: "Recharge BEST Recharge", icon: UIApplicationShortcutIcon(type: .add), userInfo: nil)
        UIApplication.shared.shortcutItems = [shortcut1]
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideContainerView), name: NSNotification.Name(rawValue: "callForAlert"), object: nil)
        uiFun.createCleverTapEvent(message: "Main Screen")
    }
    
    override func viewDidLayoutSubviews() {
        
        //Show Only Planner Except Mumbai.
        if(showPlanner || cityName.lowercased() != "mumbai"){
            if(cityName.lowercased() == "mumbai"){
                swipeGesture()
            }
            showPlanner = false
            hideBookRecharge()
            showPlanYourTravel()
            TravelPlannerTapped()
            if(!tabIndicatorFlag){
                
                tabIndicatorFlag = true
                tab_indicator_view.center = CGPoint(x: tab_indicator_view.center.x, y: tab_indicator_view.center.y)
                UIView.animate(withDuration: 0.3) {
                    self.tab_indicator_view.center = CGPoint(x: self.planYourTravelLabel.center.x - 28, y: self.tab_indicator_view.center.y)
                }
            }
        }else{
            
            //Show Recharge Screen For Mumbai City.
            swipeGesture()
            showBookRecharge()
            hidePlanYourTravel()
            showRechargeScreen()
        }
        
        setUI()
        
        if(cityName.lowercased() != "mumbai"){
            
            // Change UI For Only Planner Screen.
            hideBookRecharge()
            bookRechargeIV.isHidden         = true
            bookRechargeLabel.isHidden      = true
            offerButton.isHidden            = true
            
            planYourTravelLabel.center.x    = self.view.frame.width/2   //CGPoint(x: self.view.frame.width/2, y: 101)
            plannerIV.center.x              = self.view.frame.width/2   //CGPoint(x: self.view.frame.width/2, y: 101)
            tab_indicator_view.center.x     = self.view.frame.width/2   //CGPoint(x: self.view.frame.width/2, y: 101)
        }
        
        // Show What's New Dialog When User Opens The App For The First Time.
        let isFirstTime = uiFun.readData(key: "first_time")
        if(isFirstTime == "yes"){
            // Creating Database for storing all the values of pass and ticket
            DBManager.shared.createDatabase()
            DBManagerToken.shared.createDatabase()

            containerView.isHidden = false
        }else{
            containerView.isHidden = true
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        
        if(cityName.lowercased() != "mumbai"){
            hideBookRecharge()
            bookRechargeIV.isHidden         = true
            bookRechargeLabel.isHidden      = true
            
                // Animating Planner Logo and Text To Center
                self.planYourTravelLabel.center.x    = self.view.frame.width/2  //CGPoint(x: self.view.frame.width/2, y: 101)
                self.plannerIV.center.x              = self.view.frame.width/2  //CGPoint(x: self.view.frame.width/2, y: 101)
                self.tab_indicator_view.center.x     = self.view.frame.width/2  //CGPoint(x: self.view.frame.width/2, y: 101)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func swipeGesture(){
        
            // Intializing Swipe Gesture
            let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swiped(gesture:)))
            swipeRight.direction = UISwipeGestureRecognizerDirection.right
            self.view.addGestureRecognizer(swipeRight)
        
            let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(ViewController.swiped(gesture:)))
            swipeLeft.direction = UISwipeGestureRecognizerDirection.left
            self.view.addGestureRecognizer(swipeLeft)
    }
    
    func NavigationDrawerFunction(){
        
            // Show Navigation Drawer When Taps On Hamburger Menu
            self.window = UIWindow.init(frame: UIScreen.main.bounds)
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
            let mainViewController       = mainStoryboard.instantiateViewController(withIdentifier: "ViewController")
            let navigationViewController = mainStoryboard.instantiateViewController(withIdentifier: "NavigationViewController")
    
            centerContainer = RESideMenu(contentViewController: mainViewController, leftMenuViewController: navigationViewController, rightMenuViewController: nil)
        
            centerContainer?.panGestureEnabled           = true
            centerContainer?.menuPreferredStatusBarStyle = UIStatusBarStyle(rawValue: 1)!
            centerContainer?.contentViewShadowEnabled    = true
            centerContainer?.contentViewShadowColor      = UIColor.black
            centerContainer?.contentViewShadowRadius     = 12
            centerContainer?.contentViewShadowOpacity    = 0.6
            centerContainer?.fadeMenuView                = false
            centerContainer?.presentLeftMenuViewController()
                
            window?.rootViewController                   = centerContainer
            window?.makeKeyAndVisible()
    }
    
    func swiped(gesture : UISwipeGestureRecognizer){
    
        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
        
            // Cases When User Swipe Left Or Right
            switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.left:
                
                    TravelPlannerTapped()
                    break
                case UISwipeGestureRecognizerDirection.right:
                
                    BookRechargeTapped()
                    break
                default:
                    print("Default")
                
            }
            
        }
        
    }
    
    ///Whats new dialog button pressed
    func hideContainerView(){
        uiFun.writeData(value: "no", key: "first_time")
        containerView.isHidden = true
    }
    
    func setUI(){
        
        // NavigationBar
        let nav                     = self.navigationController?.navigationBar
        nav?.isHidden               = true
        nav?.barTintColor           = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        nav?.tintColor              = UIColor.blue
        nav?.titleTextAttributes    = [NSForegroundColorAttributeName : uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)]
        
        // Toolbar Heading Text Color
        toolbarLabel.textColor      = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        
        // Creating Traffic Near You Label
        trafficNearYouLabelManual.frame     = CGRect(x: 36, y: 461, width: 166, height: 15)
        trafficNearYouLabelManual.text      = "TRAFFIC NEAR YOU"
        trafficNearYouLabelManual.font      = UIFont.systemFont(ofSize: 12.0, weight: UIFontWeightMedium)
        trafficNearYouLabelManual.textColor = UIColor.black
        self.view.addSubview(trafficNearYouLabelManual)
        
        // Making View Corner Circular
        metroRechargeBT.layer.cornerRadius   = 3
        bussPassRchargeBT.layer.cornerRadius = 3
        
        timetableBT.layer.cornerRadius       = 3
        findRouteBT.layer.cornerRadius       = 3
        trafficNearYouBT.layer.cornerRadius  = 3
        nearByStopsBT.layer.cornerRadius     = 3
        
        /********** CIRCULAR BACKGROUND *************/
    
        let rechargeTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BookRechargeTapped))
        bookRechargeIV.addGestureRecognizer(rechargeTap)
        bookRechargeIV.isUserInteractionEnabled = true
        rechargeTap.delegate = self
        
        let plannerTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TravelPlannerTapped))
        plannerIV.addGestureRecognizer(plannerTap)
        plannerIV.isUserInteractionEnabled = true
        plannerTap.delegate = self
    }
    
    func BookRechargeTapped(){
    
        // Book Recharge Tap Function
        if(tabIndicatorFlag){
            
            tabIndicatorFlag = false
            tab_indicator_view.center = CGPoint(x: tab_indicator_view.center.x, y: tab_indicator_view.center.y)
            UIView.animate(withDuration: 0.3) {
                print("Book Recharge Label X Co-Ord \(self.bookRechargeLabel.center.x)")
                self.tab_indicator_view.center = CGPoint(x: self.bookRechargeLabel.center.x, y: self.tab_indicator_view.center.y)
            }
        }
        showBookRecharge()
        hidePlanYourTravel()
        showRechargeScreen()
    }
    
    func TravelPlannerTapped(){

        // Plan Your Travel Button Tapped
        if(!tabIndicatorFlag){
            
            tabIndicatorFlag = true
            tab_indicator_view.center = CGPoint(x: tab_indicator_view.center.x, y: tab_indicator_view.center.y)
            UIView.animate(withDuration: 0.3) {
                print("Plan Your Travel Label X Co-Ord \(self.planYourTravelLabel.center.x)")
                self.tab_indicator_view.center = CGPoint(x: self.planYourTravelLabel.center.x, y: self.tab_indicator_view.center.y)
            }
        }
        
        hideBookRecharge()
        showPlannerScreen()
        showTravelScreen()
    }
    
    func showRechargeScreen(){
    
        /********** CIRCULAR BACKGROUND *************/
        
        bookRechargeIV.layer.borderWidth        = 1
        bookRechargeIV.layer.backgroundColor    = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandDarkColor).cgColor
        bookRechargeIV.layer.borderColor        = UIColor.white.cgColor
        bookRechargeIV.layer.cornerRadius       = bookRechargeIV.frame.size.width/2
        bookRechargeIV.clipsToBounds            = true
        
        plannerIV.layer.borderWidth             = 1
        plannerIV.backgroundColor               = UIColor.clear
        plannerIV.layer.borderColor             = UIColor.white.cgColor
        plannerIV.layer.cornerRadius            = bookRechargeIV.frame.size.width/2
        plannerIV.clipsToBounds                 = true
    }
    
    func showTravelScreen(){
        
        /********** CIRCULAR BACKGROUND *************/
        
        bookRechargeIV.layer.borderWidth    = 1
        bookRechargeIV.backgroundColor      = UIColor.clear
        bookRechargeIV.layer.borderColor    = UIColor.white.cgColor
        bookRechargeIV.layer.cornerRadius   = bookRechargeIV.frame.size.width/2
        bookRechargeIV.clipsToBounds        = true
        
        plannerIV.layer.borderWidth         = 1
        plannerIV.layer.borderColor         = UIColor.white.cgColor
        plannerIV.layer.backgroundColor     = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandDarkColor).cgColor
        plannerIV.layer.cornerRadius        = bookRechargeIV.frame.size.width/2
        plannerIV.clipsToBounds             = true
    }

    @IBAction func metroRechargeTapped(_ sender: AnyObject) {
    
        print("Metro Recharge Pressed")
        defaults.set("Metro", forKey: "ticketingAgency") // Set Metro as Ticketing Agency
        fromUserProfile = false
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)//MetroCardViewController
    }
    
    @IBAction func bussPassRechargeTapped(_ sender: AnyObject) {
        
        print("Buss Pass Recharge Pressed")
        fromUserProfile = false
        defaults.set("Best", forKey: "ticketingAgency") // Set BEST as Ticketing Agency
        
        let isFirstTime = uiFun.readData(key: "best_tutorial") // Check Whether User Clicking On Best Pass For The First Time
        if(isFirstTime == "yes"){
            uiFun.writeData(value: "no", key: "best_tutorial")
            uiFun.navigateToScreem(identifierName: "BESTutorialViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "RenewBusPassViewController", fromController: self)
        }
    }
    
    @IBAction func timeTableBTTapped(_ sender: AnyObject) {
        print("Timetable Button Is Pressed")
    }
    
    @IBAction func findRouteTapped(_ sender: AnyObject) {
        print("Find Route Button Is Pressed")
    }
    
    @IBAction func trafficNearYouTapped(_ sender: AnyObject) {
        print("Traffic Near You Button Is Pressed")
    }
    
    @IBAction func nearByStopsTapped(_ sender: AnyObject) {
        print("Near By Stops Button Is Pressed")
        uiFun.navigateToScreem(identifierName: "NewsFeedViewController", fromController: self)
    }
    
    
    
    @IBAction func hamburgerMenuTapped(_ sender: AnyObject) {
        
        NavigationDrawerFunction()
    }
    
    func hidePlanYourTravel(){
    
        timetableBT.isHidden        = true
        timetableLabel.isHidden     = true
        
        findRouteBT.isHidden        = true
        findRouteLabel.isHidden     = true
        
        trafficNearYouBT.isHidden           = true
        trafficNearYouLabelManual.isHidden  = true
        
        nearByStopsBT.isHidden      = true
        nearByStopsLabel.isHidden   = true
    }
    
    func showPlanYourTravel(){
    
        timetableBT.isHidden        = false
        timetableLabel.isHidden     = false
        
        findRouteBT.isHidden        = false
        findRouteLabel.isHidden     = false
        
        trafficNearYouBT.isHidden           = false
        trafficNearYouLabelManual.isHidden  = false
        
        nearByStopsBT.isHidden      = false
        nearByStopsLabel.isHidden   = false
    }
    
    func showPlannerScreen(){
        
        timetableBT.isHidden        = false
        timetableLabel.isHidden     = false

        findRouteBT.isHidden        = false
        findRouteLabel.isHidden     = false

        trafficNearYouBT.isHidden           = false
        trafficNearYouLabelManual.isHidden  = false

        nearByStopsBT.isHidden      = false
        nearByStopsLabel.isHidden   = false
        
    }
    
    func hideBookRecharge(){
    
        metroRechargeBT.isHidden        = true
        metroRechargeLabel.isHidden     = true
        
        bussPassRchargeBT.isHidden      = true
        bussPassRechargeLabel.isHidden  = true
    }
    
    func showBookRecharge(){
    
        metroRechargeBT.isHidden        = false
        metroRechargeLabel.isHidden     = false
        
        bussPassRchargeBT.isHidden      = false
        bussPassRechargeLabel.isHidden  = false
    }
    
    func insertNewObject(){
        uiFun.navigateToScreem(identifierName: "MetroCardViewController", fromController: self)
    }
    
}

