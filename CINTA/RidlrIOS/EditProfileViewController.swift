//
//  EditProfileViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 18/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON

class EditProfileViewController: UIViewController {

    @IBOutlet var backbutton    : UIButton!
    @IBOutlet var emailIdTF     : UITextField!
    @IBOutlet var updateButton  : UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        uiFun.createCleverTapEvent(message: "EDIT PROFILE SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
        // Setting update button background color
        updateButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        // back button tap action
        uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
    }

    @IBAction func updateButtonTapped(_ sender: Any) {
        if(isValidEmail(testStr: emailIdTF.text!)){
        
            /***************** API CALL ********************/
            print("Update Profile API Call")
            let constant = Constants()
            let url = "\(constant.STAGING_BASE_URL)/user"
            let params : [String:AnyObject] = ["contactEmail":emailIdTF.text as AnyObject]
            
            //let url = "\(constant.newsFeedURL)"
            print("News Feed url \(url)")
            let util = Utillity()
            
            util.sendRequest(url: url, parameters: params, completionHandler: { (result, error) in
                if let jsonResponse = result{
                   
                    print("Successfully Update Profile Response Got.... \(jsonResponse) ")
                    /****************** API RESPONSE *******************/
                    if(String(describing:jsonResponse["code"]) == "5000"){
                       
                        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
                        let context = appDelegate.persistentContainer.viewContext
                        
                        uiFun.writeToDatabase(entityValue: self.emailIdTF.text!, entityName: "Users", key: "userEmail")
                        do {
                            try context.save()
                            print("Saved Data Successfully")
                            uiFun.createCleverTapEvent(message: "Update email")
                            uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
                        }catch {
                            print("There is an error in saving data")
                            // Default error if something went wrong
                            uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "updateButtonTapped()", fromController: self)
                        }

                    }else{
                        uiFun.hideIndicatorLoader()
                        uiFun.showExceptionDialog(jsonResponse: jsonResponse, fromViewController: self)
                    }
                    return
                }else {
                    print("Update Profile Response Failed....Try Harder Mayur ")
                    uiFun.hideIndicatorLoader()
                    return
                }
            })
        }else{
            uiFun.showAlert(title: "", message: "Invalid Email", logMessage: "Update Profile Invalid Eail", fromController: self)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // Validation of email address
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
