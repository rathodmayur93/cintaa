//
//  MetroCardViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 11/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import Crashlytics

var rechargeAmount          = ""
var metroCardNumber         = ""
var profileBackNavigation   = ""

class MetroCardViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet var metroAmountLable      : UILabel!
    @IBOutlet var stepper               : UIStepper!
    @IBOutlet var recahargeNow          : UIButton!
    @IBOutlet var pasNumberTF           : UITextField!
    @IBOutlet var metroCardBalance      : UITextView!
    @IBOutlet var backButton            : NSLayoutConstraint!
    @IBOutlet var walletButton          : UIImageView!
    
    var shouldProceed               = true
    var rechargeAmountAllowed       = 3000
    var currentBalance              = 0
    var rechargeNowButtonPressed    = false
    
    var uiFun       = UiUtillity()
    var ridlrColors = RidlrColors()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pasNumberTF.keyboardType        = UIKeyboardType.numberPad // When User Clicks On text Field Number Pad Will Open On Keyboard
        recahargeNow.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        recahargeNow.backgroundColor    = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        metroCardBalance.isHidden       = true                // Initially We will hide the card balance or expiry date of pass
        
        // When user clicks on wallet icon on metro recharge screen "walletIconTapped" function will execute
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(walletIconTapped))
        tapGesture.numberOfTapsRequired         = 1
        walletButton.isUserInteractionEnabled   = true
        walletButton.addGestureRecognizer(tapGesture)
        
        uiFun.createCleverTapEvent(message: "Metro Enter Card Number Screen")
        uiFun.createCleverTapEvent(message: "Tap on Metro")
    }
    
    @IBAction func textFieldDidChange(_ sender: AnyObject)
    {
        if(pasNumberTF.text?.characters.count == 2){
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func stepperAction(_ sender: AnyObject) {
        metroAmountLable.text = "\(Int(stepper.value))"
        
        if(Int(metroAmountLable.text!)! > rechargeAmountAllowed){
            metroCardBalance.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.error_color)
            metroCardBalance.text = "You are allowed to recharge a Max of ₹\(rechargeAmountAllowed)"
            shouldProceed = false
        }else{
            metroCardBalance.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
            metroCardBalance.text = "Current balance of the card is ₹ \(currentBalance)"
            shouldProceed = true
        }
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
    func walletIconTapped(){
        // This function makes an api call to fetch the balance of the metro card
        if(pasNumberTF.text == ""){
            uiFun.showAlert(title: "", message: "Please enter card number", logMessage: "Enter Metro Card Number", fromController: self)
            metroCardBalance.text = "Please enter card number"
            metroCardBalance.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.error_color)
        }else{
            uiFun.showIndicatorLoader()
            let fetchCardBalance = CheckCardBalance()
            fetchCardBalance.makeAPICall(fromViewController: self, cardNumber: pasNumberTF.text!)
        }
    }
    
    @IBAction func rechargeNowButtonTapped(_ sender: AnyObject) {
        
        // When user click on submit button it makes an api call to validate the card and navigate to payment screen.
        if(pasNumberTF.text == ""){
            
            let alertController = UIAlertController(title: "Error", message:
                "Please Enter Correct Metro Card Number", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            print("Metro Card Number Validation Error")
        }else{
            
            rechargeNowButtonPressed = true
            
            rechargeAmount  = metroAmountLable.text!
            metroCardNumber = pasNumberTF.text!
            
            uiFun.showIndicatorLoader()
            // api Call
            let fetchCardBalance = CheckCardBalance()
            fetchCardBalance.makeAPICall(fromViewController: self, cardNumber: pasNumberTF.text!)
            
            let defaults = UserDefaults.standard
            //  Store the enter card number to permanent storage for future use
            defaults.setValue(String(describing: pasNumberTF.text!), forKey: "bestPassNumber")
        }
    }
    
    func cardBalanceAPIResponse(response : JSON){
    
        // API response
        let profileStatus   = uiFun.readData(key: "ProfileCreation") //  Fetching data from permanent storage regarding profile status
        
        uiFun.hideIndicatorLoader()
        print("Card Balance API Call response \(response)")
        metroCardBalance.isHidden = false
       
        if(String(describing: response["code"]) == "200"){
            
            currentBalance            = Int(String(describing:(response["productBalance"])))! // Current balance of the metro card
            rechargeAmountAllowed     = 3000 - currentBalance  // Max Recharge Allowed Amount
            
            // Check for max allowed recharge
            if(Int(metroAmountLable.text!)! > rechargeAmountAllowed){
                metroCardBalance.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.error_color)
                metroCardBalance.text = "You are allowed to recharge a Max of ₹\(rechargeAmountAllowed)"
                shouldProceed = false // Variable Decides Whether Screen should navigate to next screen or not
            }else{
                
                metroCardBalance.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
                metroCardBalance.text = "Current balance of the card is ₹ \(currentBalance)"
                shouldProceed = true
                
                // This check decides whether the api response is from submit button or image wallet icon button
                if(rechargeNowButtonPressed){
                    rechargeNowButtonPressed = false
                    // This Check define whether Payment page or Profile Creation Page should appear.
                    if(profileStatus == "done"){
                        if(shouldProceed){
                            self.uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
                        }else{
                            return
                        }
                    }else{
                        uiFun.createCleverTapEvent(message: "Profile creation touchpoints Metro Recharge")
                        profileBackNavigation = "MetroCardViewController"
                        uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
                    }
                }
            }
        }
        else{
            uiFun.hideIndicatorLoader()
            
            if(String(describing: response).contains("offline")){
                //uiFun.handleApiCallError(response: response, fromController: self)
                uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
            }else{
                metroCardBalance.text = "\(response["message"])"
                print("Error message \(response["code"])")
                metroCardBalance.textColor = uiFun.hexStringToUIColor(hex: ridlrColors.error_color)
                shouldProceed = false
                // uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self) // This method parse the  api response and decides which error should be thrown.
            }
            
        }
        
    }
    
}
