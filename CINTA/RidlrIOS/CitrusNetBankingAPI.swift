//
//  CitrusNetBankingAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 09/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CitrusNetBankingAPI {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    func makeAPICall(fromViewController : AddMoneyViewController, issuerCode : String, amount : String){
        
        let url = "\(constant.STAGING_BASE_URL)/payment/addMoney"
        print("Citrus net banking payment gateway url \(url)")
        
        let jsonRequstBody = saveCardPaymentJSONString(issuerCode: issuerCode, amount: amount)
        
        util.APICall(apiUrl: url, jsonString: jsonRequstBody!, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.NetBankingResponse(response: jsonResponse)
                print("Successfully Citrus Net Banking Payment Gateway Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Citrus Net Banking Payment Gateway Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    func makeAPICallPayment(fromViewController : PaymentViewController, issuerCode : String, amount : String){
        
        let url = "\(constant.STAGING_BASE_URL)/payment/addMoney"
        print("Citrus net banking payment gateway url \(url)")
        
        let jsonRequstBody = saveCardPaymentJSONString(issuerCode: issuerCode, amount: amount)
        
        util.APICall(apiUrl: url, jsonString: jsonRequstBody!, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.NetBankingResponse(response: jsonResponse)
                print("Successfully Citrus Net Banking Payment Gateway Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Citrus Net Banking Payment Gateway Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    func saveCardPaymentJSONString(issuerCode : String, amount : String) -> String?{
        
        let para2:NSMutableDictionary = NSMutableDictionary()
        para2.setValue(issuerCode, forKey: "issuerCode")
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue("CITRUSPAY", forKey: "paymentGateway")
        para1.setValue(amount, forKey: "amount")
        para1.setValue("NET_BANKING", forKey: "paymentMode")
        para1.setValue(para2, forKey: "details")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(para1, forKey: "paymentDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("Add Money Net Banking JSON \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }


}
