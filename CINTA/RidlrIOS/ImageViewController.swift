//
//  ImageViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 06/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet var imageView         : UIImageView!
    @IBOutlet var backgroundView    : UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Making background blur
        let blurEffect          = UIBlurEffect(style: .extraLight)
        let blurEffectView      = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame    = backgroundView.frame
        
        self.view.insertSubview(blurEffectView, at: 0)
        
        // Load image from url
        uiFun.loadImageFromURL(url: mediaLinkToLoad) { (result, error) in
            if result != nil{
                if let mediaLinkData                    = UIImage(data: result!){
                    self.imageView.image                = mediaLinkData
                    self.imageView.frame.size.width     = 375.0
                    self.imageView.frame.size.height    = 262.0
                    print("Media Link In ImageView Controller \(mediaLinkToLoad)")
                    print("Image view Media Link Set Successfully")
                    return
                }
            }else {
                // If image loading is fail
                uiFun.showAlert(title: "", message: "Ooops... Unable load image!", logMessage: "Failed To Load Image In News Feed", fromController: self)
                return
            }

        }
        uiFun.createCleverTapEvent(message: "NEWS FEED IMAGE VIEW SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonAction(_ sender: Any) {
        
        uiFun.navigateToScreem(identifierName: "NewsFeedViewController", fromController: self)
    }

}
