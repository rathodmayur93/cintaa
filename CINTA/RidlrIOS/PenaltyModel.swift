//
//  PenaltyModel.swift
//  RidlrIOS
//
//  Created by Mayur on 03/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PenaltyModel {
    
    var itemType        = ""
    var bookingItemId   = ""
    var itemSubType     = ""
    var itemSubType1    = ""
    var totalAmount     = ""
    var bookingRoute    = [BookingRoute]()
    var itemAttribute   = [ItemAttribute]()

}

class BookingRoute{
    
    var bookingItemRouteId      = ""
    var sourceId                = ""
    var sourceName              = ""
    var sourceStageId           = ""
    var destinationId           = ""
    var destinationStageId      = ""
    var destinationName         = ""
    var routeId                 = ""
    var routeName               = ""
    var sourceStageName         = ""
    var destinationStageName    = ""
    
}

class ItemAttribute{

    var originalTokenType       = ""
    var masterQrCodeId          = ""
    var refTxnId                = ""
    var entryScanTime           = ""
    var excessTime              = ""
    
}
