//
//  TimetableMapViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 26/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import GooglePlaces
import CoreData

var timetableTripDetailValuesList   = [TripStop]()
var busTripDetailList               = [BusTripDetailModel]()
var busTripStopListDetail           = [BusStopsDetail]()

class TimetableMapViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    var mapView : GMSMapView = GMSMapView()
    
    @IBOutlet var stationNameLabel  : UILabel!
    @IBOutlet var startFromLabel    : UILabel!
    @IBOutlet var agencyName        : UILabel!
    @IBOutlet var busNumberLabel    : UILabel!
    
    let startingMarker              : UIView     = UIView()
    let markerText                  : UILabel    = UILabel()
    var indicator                                = UIActivityIndicatorView()
    
    @IBOutlet var tripDetailTableView: UITableView!
    
    let uiFun                                   = UiUtillity()
    let ridlrColors                             = RidlrColors()
    
    var lati                                    = ""
    var long                                    = ""
    var greaterIndex                            = 0
    var lowerIndex                              = 0
    
    var blueLoweIndex           : Int?
    var blueGreaterIndex        : Int?
    var busBlueIndex            : Int?
    
    let mapPath                                 = GMSMutablePath()
    
    var tableOriginalCenter     : CGPoint!
    var trayDownOffset          : CGFloat!
    var trayUp                  : CGPoint!
    var trayDown                : CGPoint!
    var tableViewHeight         : CGFloat!

    var mapViewCenter           : CGPoint!
    var mapViewHeight           : CGFloat!
    var isMapExpanded           = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        timetableTripDetailValuesList.removeAll()
        busTripStopListDetail.removeAll()
        
        // when user clicks on map how much map height should change
        trayDown = CGPoint(x: tripDetailTableView.center.x, y: tripDetailTableView.center.y + 200)
        trayUp   = CGPoint(x: tripDetailTableView.center.x, y: tripDetailTableView.center.y - 52)
        
        uiFun.createCleverTapEvent(message: "TIMETABLE MAP VIEW SCREEN")
        
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    
    func swipeDown(gesture : UISwipeGestureRecognizer){
        // when user swipes on tableview
        tripDetailTableView.center = CGPoint(x: tripDetailTableView.center.x, y: tripDetailTableView.center.y)
        UIView.animate(withDuration: 0.3) {
            self.tripDetailTableView.center = CGPoint(x: self.tripDetailTableView.center.x, y: self.tripDetailTableView.center.y + 230)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Count of number of rows in table view
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
            return busTripStopListDetail.count
        }else{
            return timetableTripDetailValuesList.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        // Initializing cell
        let cell            = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TimeTableMapViewCell

        // Setting row selection style to none
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.tag            = indexPath.row
        
        // Check for bus agency having BY BUS NUMBER OR NAME mode selected
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
            
                cell.stationLabel.text = busTripStopListDetail[indexPath.row].name
            
                let arrivalTime        = busTripStopListDetail[indexPath.row].arrivaltime
                // Fetching time using NSIndexRanage
                let hourIndex          = arrivalTime.index(arrivalTime.startIndex, offsetBy:2)
                let hourString         = arrivalTime.substring(to: hourIndex)
            
                let startMinute        = arrivalTime.index(arrivalTime.startIndex, offsetBy: 2)
                let endMinute          = arrivalTime.index(arrivalTime.endIndex, offsetBy: -5)
                let minuteRange        = startMinute..<endMinute
            
                let minuteString       = arrivalTime.substring(with: minuteRange)
            
                let ampmArray          = arrivalTime.components(separatedBy: " ")
                let timePeriod         = ampmArray[1]
            
                // setting time values
                cell.timeLabel.text             = "\(hourString):\(minuteString) \(timePeriod)"
                cell.staionView.clipsToBounds   = true
                cell.stationViewLabel.textColor = UIColor.white
            
                // Setting S and E in round corners
                if(indexPath.row == 0){
                    cell.stationViewLabel.text = "S"
                }else if(indexPath.row == busTripStopListDetail.count - 1){
                    cell.stationViewLabel.text = "E"
                }else if(indexPath.row > 0){
                    cell.stationViewLabel.text = "\(indexPath.row + 1)"
                }
            
            // setting circles color
            if(indexPath.row >= busBlueIndex!){
                cell.stationLabel.textColor           = UIColor.black
                cell.stationViewLabel.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
            }else{
                cell.stationLabel.textColor           = UIColor.gray
                cell.stationViewLabel.backgroundColor = UIColor.black
            }
            
        }else{
        
            cell.stationLabel.text = timetableTripDetailValuesList[indexPath.row].name
            cell.stationLabel.textColor = UIColor.gray
            print("Index path of table view map activity \(indexPath.row) \(timetableTripDetailValuesList[indexPath.row].name)")
            
        
            let arrivalTime        = timetableTripDetailValuesList[indexPath.row].arrivaltime
            
            // Fetching time values using NSIndexRange
            let hourIndex          = arrivalTime.index(arrivalTime.startIndex, offsetBy:2)
            let hourString         = arrivalTime.substring(to: hourIndex)
            
            let startMinute        = arrivalTime.index(arrivalTime.startIndex, offsetBy: 2)
            let endMinute          = arrivalTime.index(arrivalTime.endIndex, offsetBy: -5)
            let minuteRange        = startMinute..<endMinute
            
            let minuteString       = arrivalTime.substring(with: minuteRange)
            
            let ampmArray          = arrivalTime.components(separatedBy: " ")
            let timePeriod         = ampmArray[1]
            
            cell.timeLabel.text    = "\(hourString):\(minuteString) \(timePeriod)"  // setting time values
            cell.staionView.clipsToBounds   = true
            cell.stationViewLabel.textColor = UIColor.white
            
            // setting S, E and number of stop in circle
            if(cell.tag == 0){
                cell.stationViewLabel.text = "S"
            }else if(cell.tag == timetableTripDetailValuesList.count - 1){
                cell.stationViewLabel.text = "E"
            }else if(cell.tag > 0){
                cell.stationViewLabel.text = "\(indexPath.row + 1)"
            }
            
            // setting circle color to black and blue
            if(indexPath.row >= blueLoweIndex! && indexPath.row <= blueGreaterIndex!){
                cell.stationViewLabel.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
                cell.stationLabel.textColor           = UIColor.black
            }else{
                cell.stationViewLabel.backgroundColor = UIColor.black
                cell.stationLabel.textColor           = UIColor.gray
            }
        }
        return cell
    }
    
    func initMap(){
        
        // Initializing the map
        let camera                  = GMSCameraPosition.camera(withLatitude: Double(lati)!, longitude: Double(long)!, zoom: 13.0)
    
        mapView.frame               = CGRect(x: 0, y: 97, width: self.view.frame.width, height: 172)
        mapView                     = GMSMapView.map(withFrame: mapView.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        
        let bounds                  = GMSCoordinateBounds(path: mapPath)
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 0.0))
        
        self.view.addSubview(mapView)
        
        let mapClickView : UIView    = UIView()
        mapClickView.frame           = CGRect(x: 0, y: 97, width: self.view.frame.width, height: 172)
        mapClickView.backgroundColor = UIColor.clear
        self.view.addSubview(mapClickView)  // adding map view
        
        let tapGesture  = UITapGestureRecognizer(target: self, action: #selector(mapViewTapped))
        mapClickView.isUserInteractionEnabled = true
        mapClickView.addGestureRecognizer(tapGesture)
        
        let path        = GMSMutablePath()
        
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 0){
            var i = 0
            while (i < (busTripStopListDetail.count - 1)) {
                
                // getting lat long from list
                let lati    = busTripStopListDetail[i].lat
                let long    = busTripStopListDetail[i].lon
                
                let lati1   = busTripStopListDetail[i+1].lat
                let long1   = busTripStopListDetail[i+1].lon
                
                path.add(CLLocationCoordinate2D(latitude: Double(lati)!, longitude: Double(long)!))
                path.add(CLLocationCoordinate2D(latitude: Double(lati1)!, longitude: Double(long1)!))
                let polyline = GMSPolyline(path: path)
                
                // setting polylines
                polyline.strokeColor = UIColor.black
                polyline.strokeWidth = 2.0
                polyline.geodesic    = true
                
                polyline.map         = self.mapView
                
                i += 1
            }
            
            /************** SOURCE STATION MARKER *****************/
            
            let startCordLat    = busTripStopListDetail[0].lat
            let startCordLon    = busTripStopListDetail[0].lon
            
            let position        = CLLocationCoordinate2D(latitude: Double(startCordLat)!, longitude: Double(startCordLon)!)
            let titleStart      = GMSMarker(position: position)
            titleStart.title    = busTripStopListDetail[0].name
            titleStart.icon     = UIImage(named: "ic_map_flag_start_complete")
            titleStart.opacity  = 0.6
            titleStart.map      = mapView
            
            let circleCenter    = CLLocationCoordinate2D(latitude: Double(startCordLat)!, longitude: Double(startCordLon)!)
            let circ            = GMSCircle(position: circleCenter, radius: 20)
            circ.strokeWidth    = 10
            
            circ.map            = mapView
            
            /************** END STATION MARKER *****************/
            let startCordLatEnd = busTripStopListDetail[(busTripStopListDetail.count - 1)].lat
            let startCordLonEnd = busTripStopListDetail[(busTripStopListDetail.count - 1)].lon
            
            let positionEnd     = CLLocationCoordinate2D(latitude: Double(startCordLatEnd)!, longitude: Double(startCordLonEnd)!)
            let titleEnd        = GMSMarker(position: positionEnd)
            titleEnd.title      = busTripStopListDetail[(busTripStopListDetail.count - 1)].name
            titleEnd.icon       = UIImage(named: "ic_map_flag_end_complete")
            titleEnd.map        = mapView
            titleEnd.opacity    = 0.6
            
            
            /************** FETCHING STATION INDEX *****************/
            
            let startingTripIndex     = 0 //findBusIndex(stationName: startingStationName)
            let destinationTripIndex  = findBusIndex(stationName: destinationStationName)
            print("Starting and ending index are \(startingTripIndex) \(destinationTripIndex)")
            
            if(startingTripIndex > destinationTripIndex){
                greaterIndex = startingTripIndex
                lowerIndex   = destinationTripIndex
            }else{
                greaterIndex = destinationTripIndex
                lowerIndex   = startingTripIndex
            }
            
            /************ ALL STOP MARKER *********/
            
            var j = 0
            
            while(j < busTripStopListDetail.count){
                
                let startLat        = busTripStopListDetail[j].lat
                let startLon        = busTripStopListDetail[j].lon
                
                let circleCenterEnd = CLLocationCoordinate2D(latitude: Double(startLat)!, longitude: Double(startLon)!)
                let circEnd         = GMSCircle(position: circleCenterEnd, radius: 3)
                circEnd.strokeWidth = 10
                circEnd.fillColor   = UIColor.blue
                circEnd.title       = busTripStopListDetail[i].name
                circEnd.map         = mapView
                
                j += 1
            }
            
            /************** STARTING STATION MARKER *****************/
            
            let startingStationCordLat      = busTripStopListDetail[startingTripIndex].lat
            let startingStationCordLon      = busTripStopListDetail[startingTripIndex].lon
            
            let startingStationPosition     = CLLocationCoordinate2D(latitude: Double(startingStationCordLat)!, longitude: Double(startingStationCordLon)!)
            let startingStationTitle        = GMSMarker(position: startingStationPosition)
            startingStationTitle.title      = busTripStopListDetail[startingTripIndex].name
            startingStationTitle.icon       = UIImage(named: "ic_map_flag_start_complete")
            startingStationTitle.map        = mapView
            
            /************** DESTINATION STATION MARKER *****************/
            
            let destinationStationCordLat      = busTripStopListDetail[destinationTripIndex].lat
            let destinationStationCordLon      = busTripStopListDetail[destinationTripIndex].lon
            
            let destinationStationPosition     = CLLocationCoordinate2D(latitude: Double(destinationStationCordLat)!, longitude: Double(destinationStationCordLon)!)
            let destinationStationTitle        = GMSMarker(position: destinationStationPosition)
            destinationStationTitle.title      = busTripStopListDetail[destinationTripIndex].name
            destinationStationTitle.icon       = UIImage(named: "ic_map_flag_start")
            destinationStationTitle.map        = mapView
            
        }else{
            
            var i = 0
            while (i < (timetableTripDetailValuesList.count - 1)) {
                
                let lati    = timetableTripDetailValuesList[i].lat
                let long    = timetableTripDetailValuesList[i].lon
                
                let lati1   = timetableTripDetailValuesList[i+1].lat
                let long1   = timetableTripDetailValuesList[i+1].lon
                
                path.add(CLLocationCoordinate2D(latitude: Double(lati)!, longitude: Double(long)!))
                path.add(CLLocationCoordinate2D(latitude: Double(lati1)!, longitude: Double(long1)!))
                let polyline         = GMSPolyline(path: path)
                
                polyline.strokeColor = UIColor.black
                polyline.strokeWidth = 2.0
                polyline.geodesic    = true
                
                polyline.map         = self.mapView
                
                i += 1
            }
            
            /************** SOURCE STATION MARKER *****************/
            let startCordLat    = timetableTripDetailValuesList[0].lat
            let startCordLon    = timetableTripDetailValuesList[0].lon
            
            let position        = CLLocationCoordinate2D(latitude: Double(startCordLat)!, longitude: Double(startCordLon)!)
            let titleStart      = GMSMarker(position: position)
            titleStart.title    = timetableTripDetailValuesList[0].name
            titleStart.icon     = UIImage(named: "ic_map_flag_start_complete")
            titleStart.opacity  = 0.6
            titleStart.map      = mapView
            
            let circleCenter    = CLLocationCoordinate2D(latitude: Double(startCordLat)!, longitude: Double(startCordLon)!)
            let circ            = GMSCircle(position: circleCenter, radius: 20)
            circ.strokeWidth    = 10
            
            circ.map = mapView
            
            /**************** STOPS MARKER *************/
            
            var k = 0
            while(k < timetableTripDetailValuesList.count){
            
                let lati        = timetableTripDetailValuesList[k].lat
                let long        = timetableTripDetailValuesList[k].lon
                
                let position    = CLLocationCoordinate2D(latitude: Double(lati)!, longitude: Double(long)!)
                let stops       = GMSMarker(position: position)
                stops.title     = "London"
                stops.icon      = GMSMarker.markerImage(with: .black)
                k += 1
            }
            
            /****************** ALL STOP MARKER ***************/
            
            var l = 0
            while(l < timetableTripDetailValuesList.count){
                
                let startLat        = timetableTripDetailValuesList[l].lat
                let startLon        = timetableTripDetailValuesList[l].lon
                
                let circleCenterEnd = CLLocationCoordinate2D(latitude: Double(startLat)!, longitude: Double(startLon)!)
                let circEnd         = GMSCircle(position: circleCenterEnd, radius: 3)
                circEnd.strokeWidth = 10
                circEnd.fillColor   = UIColor.blue
                circEnd.title       = timetableTripDetailValuesList[l].name
                circEnd.map         = mapView
                
                l += 1
            }

            
            /************** END STATION MARKER *****************/
            let startCordLatEnd = timetableTripDetailValuesList[(timetableTripDetailValuesList.count - 1)].lat
            let startCordLonEnd = timetableTripDetailValuesList[(timetableTripDetailValuesList.count - 1)].lon
            
            let positionEnd     = CLLocationCoordinate2D(latitude: Double(startCordLatEnd)!, longitude: Double(startCordLonEnd)!)
            let titleEnd        = GMSMarker(position: positionEnd)
            titleEnd.title      = timetableTripDetailValuesList[(timetableTripDetailValuesList.count - 1)].name
            titleEnd.icon       = UIImage(named: "ic_map_flag_end_complete")
            titleEnd.map        = mapView
            titleEnd.opacity    = 0.6
            let circEnd         = GMSCircle(position: circleCenter, radius: 20)
            circEnd.strokeWidth = 10
            
            /************** FETCHING STATION INDEX *****************/
            var destinationTripIndex : Int?
            var startingTripIndex    : Int?
            if(hopeCount == 2){
                if(hopSelected){
                    
                    startingTripIndex     = findIndex(stationName: hopeStationName1)
                    destinationTripIndex  = findIndex(stationName: hopeStationName2)
                }else if(hop2Selected){
            
                    startingTripIndex     = findIndex(stationName: hopeStationName2)
                    destinationTripIndex  = findIndex(stationName: destinationStationName)
                
                }else{
                    
                    startingTripIndex     = findIndex(stationName: startingStationName)
                    destinationTripIndex  = findIndex(stationName: hopeStationName1)
                    
                    
                }
            }
            
            if(hopeCount == 1){
            
                if(hopSelected){
                    
                    startingTripIndex     = findIndex(stationName: hopeStationName1)
                    destinationTripIndex  = findIndex(stationName: destinationStationName)
               }else{
                   
                    startingTripIndex     = findIndex(stationName: startingStationName)
                    destinationTripIndex  = findIndex(stationName: hopeStationName1)
                }
                
            }
            
            if(hopeCount == 0){
            
                startingTripIndex     = findIndex(stationName: startingStationName)
                destinationTripIndex  = findIndex(stationName: destinationStationName)
            }
            print("Starting and ending index are \(startingTripIndex!) \(destinationTripIndex!)")
            
            
            
            if(startingTripIndex! > destinationTripIndex!){
                greaterIndex = startingTripIndex!
                lowerIndex   = destinationTripIndex!

            }else{
                greaterIndex = destinationTripIndex!
                lowerIndex   = startingTripIndex!
            }
            
            /************** STARTING STATION MARKER *****************/
            
            let startingStationCordLat      = timetableTripDetailValuesList[startingTripIndex!].lat
            let startingStationCordLon      = timetableTripDetailValuesList[startingTripIndex!].lon
            
            let startingStationPosition     = CLLocationCoordinate2D(latitude: Double(startingStationCordLat)!, longitude: Double(startingStationCordLon)!)
            let startingStationTitle        = GMSMarker(position: startingStationPosition)
            startingStationTitle.title      = timetableTripDetailValuesList[startingTripIndex!].name
            startingStationTitle.icon       = UIImage(named: "ic_map_flag_start")
            startingStationTitle.map        = mapView
            
            let circleCenterStarting        = CLLocationCoordinate2D(latitude: Double(startingStationCordLat)!, longitude: Double(startingStationCordLon)!)
            let circEndStart                = GMSCircle(position: circleCenterStarting, radius: 20)
            circEndStart.strokeWidth = 10
            
            /************** DESTINATION STATION MARKER *****************/
            
            let destinationStationCordLat      = timetableTripDetailValuesList[destinationTripIndex!].lat
            let destinationStationCordLon      = timetableTripDetailValuesList[destinationTripIndex!].lon
            
            let destinationStationPosition     = CLLocationCoordinate2D(latitude: Double(destinationStationCordLat)!, longitude: Double(destinationStationCordLon)!)
            let destinationStationTitle        = GMSMarker(position: destinationStationPosition)
            destinationStationTitle.title      = timetableTripDetailValuesList[destinationTripIndex!].name
            destinationStationTitle.icon       = UIImage(named: "ic_map_flag_end")
            destinationStationTitle.map        = mapView
            
        }
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        // When user taps on back button
        if(isMapExpanded){
            isMapExpanded                           = false
            mapView.frame.size.height               = 172
            tripDetailTableView.center.y            = 270
            tripDetailTableView.frame.size.height   = self.view.frame.size.height
            setUI()
        }else{
            hopSelected     = false
            hop2Selected    = false
            uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
        }
    }
    
    func mapViewTapped(){
        // when map is tapped change the height of map frame
        isMapExpanded                   = true
        mapView.frame.size.height       = 500
        mapView.animate(toZoom: 13.0)
        tripDetailTableView.center.y    = 500
        print("Map View Tapped")
    }
    
    
    func setUI(){
        
        activityIndicator()
        if(travelModeSelectedIndex == 1){
            // making an api call for fetching stops
            if(busSegmentSelectedIndex == 1){
                stationNameLabel.text = startingStationName
                startFromLabel.text   = destinationStationName
                agencyName.text       = ""
                FetchBusTripDetailAPI()
                
            }else if(busSegmentSelectedIndex == 0){
                
                stationNameLabel.text = startingStationName
                startFromLabel.text   = destinationStationName
                agencyName.text       = ""
                FetchBusNumberMapAPICall()
                
            }
        }else{
            stationNameLabel.text   = "\(commonTripValues[0].tripValues[0].sRouteId[0].routeDestinationName)"
            startFromLabel.text     = "Starts from \(commonTripValues[0].tripValues[0].sRouteId[0].routeSourceName)"
            agencyName.text         = "\(commonTripValues[0].tripValues[0].sRouteId[0].agency)"
            
            startingMarker.backgroundColor      = UIColor.black
            startingMarker.layer.cornerRadius   = 5
            
            setHeaderLabelValues()
            FetchTripDetailAPICall()
        }
    }
    
    
    
    
    func moveTableView(_ gestureRecognizer: UIPanGestureRecognizer){
        // This function is not in use but it contain pan gesture for moving table view so saving this for future refrence
        if (gestureRecognizer.state == .began || gestureRecognizer.state == .changed) {
            
            let translation             = gestureRecognizer.translation(in: self.view)
            var doPanGesture            = false
            var panStatus               = false
            
            print("Table View translation \(translation)")
            // note: 'view' is optional and need to be unwrapped
            if gestureRecognizer.state == .began {
                tableOriginalCenter = tripDetailTableView.center
                mapViewCenter       = mapView.center
                
            } else if gestureRecognizer.state == .changed {
                
                let velocity = gestureRecognizer.velocity(in: self.view)
                print("Velocity of tableview pan gesture \(velocity)")
                
                let visibleIndexesOfTableview = tripDetailTableView.indexPathsForVisibleRows
                for index in visibleIndexesOfTableview!{
                
                    if(index.row == 0){
                        doPanGesture = true
                    }
                }
                
                if (velocity.y > 1500 && doPanGesture) {
                    panStatus = true
                    UIView.animate(withDuration: 0.3) {
                        self.tripDetailTableView.center = self.trayDown
                        
                        UIView.animate(withDuration:0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options:[] ,
                                       animations: { () -> Void in
                                        self.tripDetailTableView.center = self.trayDown
                                        self.mapView.frame.size.height  = self.tripDetailTableView.center.y - 250
                        }, completion: nil)
                    }
                } else {
                    UIView.animate(withDuration: 0.3) {
                        panStatus = false
                        self.tripDetailTableView.center = self.trayUp
                        self.mapView.frame.size.height  = 130
                    }
                }
            } else if gestureRecognizer.state == .ended {
                
                let velocity = gestureRecognizer.velocity(in: self.view)
                print("Velocity of tableview pan gesture \(velocity)")
                if velocity.y > 50 {
                    UIView.animate(withDuration: 0.3) {
                        self.tripDetailTableView.center = self.trayDown
                        
                        UIView.animate(withDuration:0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options:[] ,
                                       animations: { () -> Void in
                                        self.tripDetailTableView.center = self.trayDown
                        }, completion: nil)
                    }
                } else {
                    UIView.animate(withDuration: 0.3) {
                        self.tripDetailTableView.center = self.trayUp
                    }
                }
            }
            
        }
        
    }
    
    func setHeaderLabelValues(){
    
        // Setting toolbar label values
        stationNameLabel.text   = "\(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].routeDestinationName)"
        startFromLabel.text     = "Starts from \(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].routeSourceName)"
        agencyName.text         = "\(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].agency)"
        busNumberLabel.text     = ""
    
        if(hopSelected){
            // setting hop station names
            stationNameLabel.text   = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeDestinationName)"
            startFromLabel.text     = "Starts from \(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeSourceName)"
            
            agencyName.text     = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].agency)"
            busNumberLabel.text = "(\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeShortName))"
            busNumberLabel.text = ""
        }
        
        if(hop2Selected){
            // Setting hope stations names
            agencyIdValueSelected = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].legAgency[0].agencyId
            routeIdValueSelected  = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeId
            
            stationNameLabel.text = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeDestinationName)"
            
            startFromLabel.text   = "Starts from \(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeSourceName)"
            
            agencyName.text       = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].agency)"
            busNumberLabel.text   = ""
        }
    }
    
    func activityIndicator() {
        // loader
        indicator.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func FetchTripDetailAPICall(){
        // Fetching particualr trip detail api call
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
        
        if(hopSelected){
            agencyIdValueSelected = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].legAgency[0].agencyId
            routeIdValueSelected  = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeId
        }
        
        if(hop2Selected){
            agencyIdValueSelected = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].legAgency[0].agencyId
            routeIdValueSelected  = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeId
        }
        
        let fetchTripDetail = FetchTripDetailAPI()      // api call
        fetchTripDetail.makeAPICall(fromViewController: self, jsonString: tripDetailJSONBody(), cityId: cityIdFromData, modeId: timetableModeId, agencyId: agencyIdValueSelected, routeId: routeIdValueSelected)
    }
    
    func FetchBusTripDetailAPI(){
        // Fetching particualr bus trip detail api call
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
        
        stationNameLabel.text     = "\(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].routeDestinationName)"
        startFromLabel.text       = "Starts from \(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].routeSourceName)"
        
        agencyName.text           = "\(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].agency)"
        busNumberLabel.text       = "(\(commonTripValues[tripDetailIndex!].tripValues[0].sRouteId[0].routeShortName))"
        if(busSegmentSelectedIndex == 1){
            
        }else{
            busNumberLabel.text = ""
        }
        
        if(hopSelected){
            // if hop1 is selcted
            agencyIdValueSelected = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].legAgency[0].agencyId
            routeIdValueSelected  = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeId
            stationNameLabel.text = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeDestinationName)"
            
            startFromLabel.text   = "Starts from \(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeSourceName)"
            
            agencyName.text     = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].agency)"
            busNumberLabel.text = "(\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteId[0].routeShortName))"
           
            if(busSegmentSelectedIndex == 1){
                
            }else{
                busNumberLabel.text = ""
            }
            
        }
        
        if(hop2Selected){
            // if hop2 is selected
            agencyIdValueSelected  = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].legAgency[0].agencyId
            routeIdValueSelected  = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeId
            
            stationNameLabel.text = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeDestinationName)"
            
            startFromLabel.text   = "Starts from \(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeSourceName)"
            
            agencyName.text       = "\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].agency)"
            busNumberLabel.text   = "(\(commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteId[0].routeShortName))"
          
            if(busSegmentSelectedIndex == 1){
                
            }else{
                busNumberLabel.text = ""
            }
        }

        
        let busTripDetailAPI = FetchBusTripDetailAPICall()  // api call
        busTripDetailAPI.makeAPICall(fromViewController: self, jsonString: tripDetailBusJSONBody(), agencyId: agencyIdValueSelected, routeId: routeIdValueSelected)
    }
    
    func FetchBusNumberMapAPICall(){
        // Api call for bus number trip detail
        busNumberLabel.text = ""
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
    
        let fetchBusNumberMap = FetchBusNumberMapApiCall()     // api call
        fetchBusNumberMap.makeAPICall(fromViewController: self, agencyId: agencyId, routeId: routeId, tripId: tripId)
    }
    
    func BusNumberMapAPIResponse(response : JSON){
        // Response of FetchBusNumberMapApiCall
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        processBusTripDetail(response: response)
    }
    
    func BusTripDetailResponse(response : JSON){
        // Response of FetchBusTripDetailAPICall
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        print("Got the bus trip result \(response)")
        processBusTripDetail(response: response)
    }
    
    func FetchTripDetailResponse(response : JSON){
        // Response of FetchTripDetailAPI
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        processTripDetailJSON(response: response)
    }
    
    func tripDetailJSONBody() -> String{
        // JSON request body for FetchTripDetailAPI
        var findTimeFromList = ""
        var calendarId       = ""
        
        calendarId           = commonTripValues[tripDetailIndex!].tripValues[0].calendarID
        findTimeFromList     = commonTripValues[tripDetailIndex!].tripValues[0].sRouteStartTimeStr

        if(hopSelected){
            findTimeFromList = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteStartTimeStr
            calendarId       = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].calendarID
        }
        
        if(hop2Selected){
            findTimeFromList = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteStartTimeStr
            calendarId       = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].calendarID
        }

        
        let findTime            = findTimeFromList.components(separatedBy: " ")
        let tripStartTimeArray  = findTime[1].components(separatedBy: ":")
        let hour                = tripStartTimeArray[0]
        let minutes             = tripStartTimeArray[1]
        let requestedTime       = "\(hour)\(minutes)00"
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(calendarId, forKey: "calendar_id")
        para.setValue(requestedTime, forKey: "trip_start_time")
        
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("timetable trips detail json string = \(jsonString)") // json string
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return ""
        }
    }
    
    func tripDetailBusJSONBody() -> String{
        // JSON body request for FetchBusTripDetailAPICall
        var calendarId       = ""
        var findTimeFromList = ""
        if(hopSelected){
            calendarId       = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].calendarID
            findTimeFromList = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].sRouteStartTimeStr
        }else if(hop2Selected){
            calendarId       = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].calendarID
            findTimeFromList = commonTripValues[tripDetailIndex!].tripValues[0].nextEarliestTripLegModel[0].nextEarliestTripLegModel[0].sRouteStartTimeStr
        }else{
            calendarId       = commonTripValues[tripDetailIndex!].tripValues[0].calendarID
            findTimeFromList = commonTripValues[tripDetailIndex!].tripValues[0].sRouteStartTimeStr
        }
        let findTime            = findTimeFromList.components(separatedBy: " ")
        
        let tripStartTimeArray  = findTime[1].components(separatedBy: ":")
        let hour                = tripStartTimeArray[0]
        let minutes             = tripStartTimeArray[1]
        let requestedTime       = "\(hour)\(minutes)00"
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(calendarId, forKey: "calendar_id")
        para.setValue(requestedTime, forKey: "trip_start_time")
        
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("timetable trips detail bus json string = \(jsonString)") // json string
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return ""
        }
    }

    func processTripDetailJSON(response : JSON){
        // processing json response of FetchTripDetailResponse and storing into model
        if(String(describing : response["statusmessage"]) == "TRIP_NOT_FOUND"){
            uiFun.showAlert(title: "", message: "Ooops...Trip Not Found", logMessage: "Trip Not Found", fromController: self)
            return
        }
        timetableTripDetailValuesList.removeAll()
        let tripDetailModel = TripDetailModel()
        
        tripDetailModel.startTime   = String(describing: response["starttime"])
        tripDetailModel.endTime     = String(describing: response["endtime"])
        
        let stopJSON = response["stop"]
        
        var i = 0
        while (i < stopJSON.count) {
            
            let stopModel = TripStop()
            
            stopModel.id                = String(describing: stopJSON.arrayValue[i]["id"])
            stopModel.name              = String(describing: stopJSON.arrayValue[i]["name"])
            stopModel.arrivaltime       = String(describing: stopJSON.arrayValue[i]["arrivaltime"])
            stopModel.departuretime     = String(describing: stopJSON.arrayValue[i]["departuretime"])
            stopModel.lon               = String(describing: stopJSON.arrayValue[i]["lon"])
            stopModel.lat               = String(describing: stopJSON.arrayValue[i]["lat"])
            stopModel.platformno        = String(describing: stopJSON.arrayValue[i]["platformno"])
            
            lati = String(describing: stopJSON.arrayValue[i]["lat"])
            long = String(describing: stopJSON.arrayValue[i]["lon"])
            
            let mapLat = String(describing: stopJSON.arrayValue[i]["lat"])
            let mapLon = String(describing: stopJSON.arrayValue[i]["lon"])
            
            mapPath.add(CLLocationCoordinate2DMake(Double(mapLat)!, Double(mapLon)!))
            
            tripDetailModel.stops.append(stopModel)
            timetableTripDetailValuesList.append(stopModel) // saving reposne into list
            i += 1
        }
        findStartingDestinationIndex()
        initMap()
    }
    
    func findStartingDestinationIndex(){
        // finding user starting and destination station index from stop list of agency
        var destinationTripIndex : Int?
        var startingTripIndex    : Int?
        
        if(hopeCount == 2){
            if(hopSelected){
                startingTripIndex     = findIndex(stationName: hopeStationName1)
                destinationTripIndex  = findIndex(stationName: hopeStationName2)
            }else if(hop2Selected){
                
                startingTripIndex     = findIndex(stationName: hopeStationName2)
                destinationTripIndex  = findIndex(stationName: destinationStationName)
                
            }else{
                startingTripIndex     = findIndex(stationName: startingStationName)
                destinationTripIndex  = findIndex(stationName: hopeStationName1)
            }
        }
        
        if(hopeCount == 1){
            // hop count is 1
            if(hopSelected){
                startingTripIndex     = findIndex(stationName: hopeStationName1)
                destinationTripIndex  = findIndex(stationName: destinationStationName)
           
            }else{
                startingTripIndex     = findIndex(stationName: startingStationName)
                destinationTripIndex  = findIndex(stationName: hopeStationName1)
            }
        }
        
        if(hopeCount == 0){
            // no hop
            startingTripIndex     = findIndex(stationName: startingStationName)
            destinationTripIndex  = findIndex(stationName: destinationStationName)
        }
        
        if(startingTripIndex! > destinationTripIndex!){
            // Blue cirlce index
            blueGreaterIndex = startingTripIndex!
            blueLoweIndex    = destinationTripIndex!
       
        }else{
            // Blue cirlce index
            blueGreaterIndex = destinationTripIndex!
            blueLoweIndex   = startingTripIndex!
        }
        
        print("Starting and ending index are api response \(startingTripIndex!) \(destinationTripIndex!)")
        tripDetailTableView.reloadData()
    }
    
    func processBusTripDetail(response : JSON){
        // Processing bus trip json response into model
        if(String(describing : response["statusmessage"]) == "TRIP_NOT_FOUND"){
            uiFun.showAlert(title: "", message: "Ooops...Trip Not Found", logMessage: "Trip Not Found", fromController: self)
            return
        }
        
        let busTripDetailModel          = BusTripDetailModel()
        
        busTripDetailModel.starttime    = String(describing: response["starttime"])
        busTripDetailModel.endtime      = String(describing: response["endtime"])
        
        let stopDetailJSON = response["stop"]
        print("Bus Stop Detail Count \(stopDetailJSON.count)")
        
        var i = 0
        
        while(i < stopDetailJSON.count){
            
            let busStopDetail           = BusStopsDetail()
            
            busStopDetail.id            = String(describing: stopDetailJSON.arrayValue[i]["id"])
            busStopDetail.name          = String(describing: stopDetailJSON.arrayValue[i]["name"])
            busStopDetail.arrivaltime   = String(describing: stopDetailJSON.arrayValue[i]["arrivaltime"])
            busStopDetail.departuretime = String(describing: stopDetailJSON.arrayValue[i]["departuretime"])
            busStopDetail.lon           = String(describing: stopDetailJSON.arrayValue[i]["lon"])
            busStopDetail.lat           = String(describing: stopDetailJSON.arrayValue[i]["lat"])
            busStopDetail.platformno    = String(describing: stopDetailJSON.arrayValue[i]["platformno"])
            
            lati = String(describing: stopDetailJSON.arrayValue[i]["lat"])
            long = String(describing: stopDetailJSON.arrayValue[i]["lon"])

            let mapLat = String(describing: stopDetailJSON.arrayValue[i]["lat"])
            let mapLon = String(describing: stopDetailJSON.arrayValue[i]["lon"])
            
            mapPath.add(CLLocationCoordinate2DMake(Double(mapLat)!, Double(mapLon)!))
            
            busTripDetailModel.busStops.append(busStopDetail)
            busTripStopListDetail.append(busStopDetail)
            
            i += 1
        }
        
        busTripDetailList.append(busTripDetailModel)        // saving repsonse into the list
        tripDetailTableView.reloadData()
        initMap()
    }
    
    func findIndex(stationName : String) -> Int{
        // fining index of particular station
        var index :Int?
        var comparingStationName = ""
        
        // check for bus agency from A to B
        if(travelModeSelectedIndex == 1 && busSegmentSelectedIndex == 1){
            var splitStationName = stationName.components(separatedBy: "-")
            comparingStationName = splitStationName[0]
        }else{
            comparingStationName = stationName
        }
        
        var i = 0
        while(i < timetableTripDetailValuesList.count){
            
            let loopingStationName = timetableTripDetailValuesList[i].name
            if(loopingStationName == comparingStationName){
                index = i
                break
            }
            i += 1
        }
        if(index == nil){
            return 0
        }
        return index!
    }
    
    func findBusIndex(stationName : String) -> Int{
        // finding bus stop station index
        var index :Int?
        var i = 0
        while(i < busTripStopListDetail.count){
            
            let loopingStationName = busTripStopListDetail[i].name
          
            if(loopingStationName == stationName){
                index = i
                busBlueIndex = index
                break
            }
            i += 1
        }
        return index!
    }
}
