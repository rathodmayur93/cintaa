//
//  RenewBusPassNewViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 27/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class RenewBusPassNewViewController: UIViewController {

    @IBOutlet var passStatus                : UILabel!
    @IBOutlet var passStatusDesc            : UILabel!
    
    @IBOutlet var passDetailView            : UIView!
    @IBOutlet var monthlyPassView           : UIView!
    @IBOutlet var quartelyPassView          : UIView!
    
    @IBOutlet var name                      : UILabel!
    @IBOutlet var validity                  : UILabel!
    @IBOutlet var passTypeBEST              : UILabel!
    @IBOutlet var passCategory              : UILabel!
    @IBOutlet var concession                : UILabel!
    @IBOutlet var passNumber                : UILabel!
    
    @IBOutlet var monthlyLabel              : UILabel!
    @IBOutlet var monthlyValidity           : UILabel!
    @IBOutlet var monthlyPrice              : UILabel!
    
    @IBOutlet var quarterlyLabel            : UILabel!
    @IBOutlet var quarterlyValidity         : UILabel!
    @IBOutlet var quarterlyPrice            : UILabel!
    
    @IBOutlet var backButton                : UIButton!
    var passDuration                        = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        uiFun.createCleverTapEvent(message: "BUS PASS RECHARGE DURATION SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        /************** SHADOW ***********/
        
        passDetailView.dropShadow()
        monthlyPassView.dropShadow()
        quartelyPassView.dropShadow()
        
        /************ SETTING VALUES ***********/
        passStatus.text        = fetchingPassStatus()
        passNumber.text        = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].companyIdentifier
        name.text              = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].user[0].name
        validity.text          = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].expiryDate
        passTypeBEST.text      = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].itemSubType
        concession.text        = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].concession
        monthlyLabel.text      = bestPassResponseListCurrent[0].bestData[0].actionDetails[0].validity
        
        // Clevertap Events
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Pass Category \(bestPassResponseListCurrent[0].bestData[0].currentDetails[0].passCategory)")
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Concession type \(bestPassResponseListCurrent[0].bestData[0].currentDetails[0].concession)")
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Agency \(bestPassResponseListCurrent[0].bestData[0].currentDetails[0].itemSubType)")
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Pass Number \(bestPassResponseListCurrent[0].bestData[0].currentDetails[0].companyIdentifier)")
        
        passCategory.text   = bestPassResponseListCurrent[0].bestData[0].actionDetails[0].passCategory
        
        // Monthly pass validity date
        monthlyValidity.text   = "\(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[0].activationDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0]) - \(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[0].expiryDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0])"
        
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Recharge duration \(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[0].activationDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0]) - \(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[0].expiryDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0])")
        
        // Monthly Pass Price
        monthlyPrice.text      = "₹ \(bestPassResponseListAction[0].bestData[0].actionDetails[0].pricingDetails[0].totalAmount)"
        
        
        if(passDurationType == "Q"){
            
            quarterlyLabel.text = bestPassResponseListCurrent[0].bestData[0].actionDetails[1].validity // Quarterly Pass Validity
            quarterlyPrice.text = "₹ \(bestPassResponseListCurrent[0].bestData[0].actionDetails[1].pricingDetails[0].totalAmount)" //Quarterly Pass Price
            
            // Quarterly Pass Expiry Date
            quarterlyValidity.text = "\(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[1].activationDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0]) - \(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[1].expiryDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0])"
            
            uiFun.createCleverTapEvent(message: "Request Pass Recharge Recharge duration \(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[1].activationDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0]) - \(formattedDateFromString(dateString: bestPassResponseListCurrent[0].bestData[0].actionDetails[1].expiryDate, withFormat: "MMM dd, yyyy").components(separatedBy: ",")[0])")
        }else{
            // If only monthly pass availbale hide quarterly pass
            quartelyPassView.isHidden = true
        }
        print("Pass Duration is \(passDurationType)")
        
        rechargeTapFunction()
    }
    
    func rechargeTapFunction(){
    
        // Monthly View Tap Function
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(monthlyPassTapFunctino))
        tapGesture.numberOfTapsRequired = 1
        monthlyPassView.isUserInteractionEnabled = true
        monthlyPassView.addGestureRecognizer(tapGesture)
        
        // Quarterly View Tap Function
        let tapGestureQuarterly = UITapGestureRecognizer(target: self, action: #selector(quarterlyPassTapFunction))
        tapGesture.numberOfTapsRequired = 1
        quartelyPassView.isUserInteractionEnabled = true
        quartelyPassView.addGestureRecognizer(tapGestureQuarterly)
        
    }
    
    func monthlyPassTapFunctino(){
        
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Pass type Monthly")
        rechargeAmount    = bestPassResponseListAction[0].bestData[0].actionDetails[0].pricingDetails[0].totalAmount // Fetch Monthly Amount
        passType          = 0
        passDuration      = bestPassResponseListAction[0].bestData[0].actionDetails[0].validity // Fetch Validity
        let profileStatus = uiFun.readData(key: "ProfileCreation")
        
        if(profileStatus == "done"){
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }else{
            profileBackNavigation = "RenewBusPassNewViewController"
            uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
        }
    }
    
    func quarterlyPassTapFunction(){
    
        uiFun.createCleverTapEvent(message: "Request Pass Recharge Pass type Quarterly")
        rechargeAmount    = bestPassResponseListAction[0].bestData[0].actionDetails[1].pricingDetails[0].totalAmount // Fetch Quarterly Amount
        passType          = 1
        passDuration      = bestPassResponseListAction[0].bestData[0].actionDetails[1].validity // Fetch Validity
        
        let profileStatus = uiFun.readData(key: "ProfileCreation")
        
        if(profileStatus == "done"){
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
        }
    }
   
    @IBAction func backButtonAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "RenewBusPassViewController", fromController: self)
    }
    
    func fetchingPassStatus() -> String{
    
        // Fetching Pass Status Whether It is Active, Expired or Expiring Soon.
        let date1           = Date()
        let calendar1       = Calendar.current
        let components1     = calendar1.dateComponents([.year, .month, .day], from: date1)
        
        let currentMonth    = components1.month!
        let currentDay      = components1.day!
        
        let passExpiryDate  = bestPassResponseListCurrent[0].bestData[0].currentDetails[0].expiryDate
        
        let expiryMonth     = Int(passExpiryDate.components(separatedBy: "-")[1])!
        let expiryDate      = Int(passExpiryDate.components(separatedBy: "-")[2])!
        
        if(currentMonth == expiryMonth){
            if(currentDay > expiryDate){
                passStatus.textColor = UIColor.red
                return "Expired"
          
            }else if(currentDay <= expiryDate){
                let remainingDays = expiryDate - currentDay
               
                if(remainingDays <= 7){
                    passStatus.textColor = UIColor.orange
                    return "Expiring Soon"
               
                }else{
                    passStatus.textColor = UIColor.green
                    return "Active"
                }
            }
       
        }else if(currentMonth > expiryMonth){
            passStatus.textColor = UIColor.red
            return "Expired"
        
        }else{
            passStatus.textColor = UIColor.red
            return "Expired"
        }
        
        return ""
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String {
        // Convert Date to String in "yyyy/MM/dd" format
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy/MM/dd"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            print("Validity \(outputFormatter.string(from: date))")
            return outputFormatter.string(from: date)
            
        }
        
        return ""
    }
    
}

extension UIView {
    
    func dropShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.layer.shadowRadius = 1
    }
}

