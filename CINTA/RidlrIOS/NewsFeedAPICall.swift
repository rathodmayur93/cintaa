//
//  NewsFeedAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 12/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit


class NewsFeedAPICall {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    func makeAPICall(fromViewController : NewsFeedViewController, firstPostOn : String, lastPostOn : String, sinceId : String){
        
        
        print("News Feed First Post on \(firstPostOn)")
        print("News Feed Last post on \(lastPostOn)")
        let url = "\(constant.TIMELINE_URL)GetTimeLinesForLocation"     // Fetching timeline api call
        
        // Parameters for api call
        let params : [String:AnyObject] = ["strIP":"" as AnyObject,
                                           "strCustKey":constant.CUST_KEY as AnyObject,
                                           "passkey":constant.PASS_KEY as AnyObject,
                                           "addparams":"FirstPostedOn|\(firstPostOn),LastPostedOn|\(lastPostOn)" as AnyObject,
                                           "accessType":constant.ACCESS_TYPE as AnyObject,
                                           "strGroupId":constant.GROUP_ID as AnyObject,
                                           "userid":"-111" as AnyObject,
                                           "strSinceId":sinceId as AnyObject,
                                           "intNoOfRecords":"10" as AnyObject,
                                           "strLocation":"Mumbai" as AnyObject,
                                           "strViewType":"" as AnyObject,
                                           "dbLatitude" : uiFun.readData(key: "lat") as AnyObject,
                                           "updType" : "All" as AnyObject,
                                           "dbLongitude" : uiFun.readData(key: "long") as AnyObject,
                                           "updSource" : "All" as AnyObject, ]
        
        print("News Feed url \(url)")
        
        // GET request
        util.sendRequest(url: url, parameters: params, completionHandler: { (result, error) in
        
            if let jsonResponse = result{
                
                fromViewController.responseNewsFeed(response: jsonResponse)
                print("Successfully News Feed Response Got.... \(jsonResponse) ")
                return
                
            }else {
                
                print("News Feed Response Failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
}
