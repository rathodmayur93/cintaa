//
//  RouteSearchResultAPIViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 01/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class RouteSearchResultAPIViewController {

    var util        = Utillity()
    var constant    = Constants()
    var uiFun       = UiUtillity()
    var jsonString  = ""
    
    func makeAPICall(fromViewController : RouteSearchResult){
    
        let url = constant.ROUTE_SEARCH_PLANNER_URL
        
        if(doRecentSearch && !timeChange){
            jsonString = recentSearchJSON
            print("Recent Search JSON \(jsonString)")
        }else{
            jsonString = RouteSearchJSONString()!
        }
        
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: false) { result, error in
            
            if let jsonResponse = result{
                
                fromViewController.routeSearchAPIResponse(josnResult: jsonResponse)
                print("Successfully Route seatch API Call Repsonse \(jsonResponse) ")
                return
                
            }else {
                
                print("Route search api call Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    func RouteSearchJSONString() -> String?{
        
        var arrival = ""
        
        let date = NSDate()
        let calendar = NSCalendar.current
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        let seconds = calendar.component(.second, from: date as Date)
        
        let date1 = Date()
        let components1 = calendar.dateComponents([.year, .month, .day], from: date1)
        
        let year =  components1.year!
        let month = components1.month!
        let day = components1.day!
        
        if(timeChange){
            arrival = plannerChangeDateTime
        }else{
            let actualMonth   = uiFun.getTwoDigitTime(time: String(describing: month))
            let actualDay     = uiFun.getTwoDigitTime(time: String(describing: day))
            let actualHour    = uiFun.getTwoDigitTime(time: String(describing: hour))
            let actualMinutes = uiFun.getTwoDigitTime(time: String(describing: minutes))
            let actualSeconds = uiFun.getTwoDigitTime(time: String(describing: seconds))
            
            arrival = "\(year)\(actualMonth)\(actualDay)\(actualHour)\(actualMinutes)\(actualSeconds)"
        }
        
        let localTransportArray = NSMutableArray()
        localTransportArray.add("WALK_ONLY")
        localTransportArray.add("AUTO_AND_CAB")
        
        let modeArray = NSMutableArray()
        modeArray.add("2")
        modeArray.add("3")
        modeArray.add("1")
        modeArray.add("0")
        
        let locationArray = NSMutableArray()
        
        let latArray = NSMutableArray()
        if plannerStartingStationPlaceId == "null" {
            latArray.add(plannerStartingLat)
            latArray.add(plannerStartingLong)
        }
        
        
        let longArray = NSMutableArray()
        if plannerDestinationPlaceId == "null" {
            longArray.add(plannerDestinationLat)
            longArray.add(plannerDestinationLong)
        }
        
        
        locationArray.add(latArray)
        locationArray.add(longArray)
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue("null", forKey: "arrivalTime")
        para.setValue(cityIdFromData, forKey: "cityId")
        para.setValue(arrival, forKey: "departureTime")
        para.setValue(plannerDestinationSubLocality, forKey: "destinationPlace")
        para.setValue(plannerDestinationPlaceId, forKey: "destinationPlaceId")
        para.setValue("456833d82fe7d822", forKey: "deviceId")
        para.setValue("LT", forKey: "preference")
        para.setValue("3", forKey: "resultPerLocalTransport")
        para.setValue(plannerStartingSubLocality, forKey: "sourcePlace")
        para.setValue(plannerStartingStationPlaceId, forKey: "sourcePlaceId")
        para.setValue("-9", forKey: "userId")
        
        para.setValue(localTransportArray, forKey: "localTransport")
        para.setValue(modeArray, forKey: "mode")
        para.setValue(locationArray, forKey: "points")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string route search = \(jsonString)")
            uiFun.writeToDatabase(entityValue: jsonString, entityName: "RecentSearch", key: "plannerRecentSearchJSON")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

    
}
