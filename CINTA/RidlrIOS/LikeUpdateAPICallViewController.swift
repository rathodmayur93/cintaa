//
//  LikeUpdateAPICallViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 16/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreLocation

class LikeUpdateAPICallViewController:UIViewController, CLLocationManagerDelegate {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    var encodedPassKey = ""
    var lat : Double = Double()
    var long: Double = Double()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation: CLLocation = locations[0]
        
        lat     = userLocation.coordinate.latitude
        long    = userLocation.coordinate.longitude
    }
    
    func makeAPICall(fromViewController : NewsFeedViewController, updateId : String){
        
        let uuid = uiFun.readData(key: "UUID")
        let utf8str = uuid.data(using: String.Encoding.utf8)
        if let base64Encoded = utf8str?.base64EncodedString()  {
        
            encodedPassKey = base64Encoded
        }
        
        
        
        let url = "\(constant.TIMELINE_URL)UpdateTimelineActivities"    // api call url
        
        // Parameters for api call
        let params : [String:AnyObject] = ["strActivity":"like" as AnyObject,
                                           "strParentUpdateId":newsFeedList[likeIndex].updateId as AnyObject,
                                           "strSourceUpdateId":"traffapp" as AnyObject,
                                           "strTextUpdate":newsFeedList[likeIndex].textUpdate as AnyObject,
                                           "lngFromUserId":newsFeedList[likeIndex].fromUser as AnyObject,
                                           "strProfilePicURL":newsFeedList[likeIndex].profileImg as AnyObject,
                                           "strTwitterHandleOrTraffUsername":newsFeedList[likeIndex].username as AnyObject,
                                           "strLocation":"" as AnyObject,
                                           "dbLatitude":"19.136326" as AnyObject,
                                           "dbLongitude":"72.827660" as AnyObject,
                                           "updType":"" as AnyObject,
                                           "strMediaLinks" : newsFeedList[likeIndex].mediaLink as AnyObject,
                                           "strEmoticons" : "" as AnyObject,
                                           "strGroupId" : "g2apps" as AnyObject,
                                           "strCustKey":"f29a3c79212611e3aeb927d5ba248633" as AnyObject,
                                           "strIP":"" as AnyObject,
                                           "passkey" : encodedPassKey as AnyObject,
                                           "addparams":"" as AnyObject ]
        
    
        print("News Feed url \(url)")
        
        // GET req
        util.sendRequest(url: url, parameters: params, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.likeTimelinePostResponse(response: jsonResponse)
                print("Successfully News Feed Response Got.... \(jsonResponse) ")
                return
                
            }else {
                
                print("News Feed Response Failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }


}
