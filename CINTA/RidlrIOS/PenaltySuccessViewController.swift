//
//  PenaltySuccessViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 27/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PenaltySuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
    }
    
    @IBAction func continueAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
}
