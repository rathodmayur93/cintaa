//
//  StoreValueReceiptViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 15/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import Darwin
import BigInt
import SwiftyJSON

var isFromRechargePass = false
class StoreValueReceiptViewController: UIViewController {

    @IBOutlet var view1             : UIView!
    @IBOutlet var view2             : UIView!
    @IBOutlet var passNumberLabel   : UILabel!
    @IBOutlet var passExpiry        : UILabel!
    @IBOutlet var passBalance       : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        view1.layer.borderWidth = 1.0
        view2.layer.borderWidth = 1.0
        
        view1.layer.borderColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_50).cgColor
        view2.layer.borderColor = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_50).cgColor
        
        passNumberLabel.text    = uiFun.readData(key: "metroStoreValueMasterQRCode")
        passBalance.text        = uiFun.readData(key: "storeValuePassBalance")
        
        let timestampDate                 = NSDate(timeIntervalSince1970: Double(uiFun.readData(key: "storeValueExpiry"))!/1000)
        let dayTimePeriodFormatter        = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
        passExpiry.text                   = "\(dayTimePeriodFormatter.string(from: timestampDate as Date))"

    }
    
    func createBookingNo() -> String?{
        
        let timestamp                = Int(NSDate().timeIntervalSince1970 * 1000)
        
        let userId                  = uiFun.readData(key: "userId")
        let maxAllowedCharacter     = 10
        let maxAllowedBookingNo     = 16
        let userIdCharacters        = userId.characters.count
        let allowedRandom           = maxAllowedCharacter - userIdCharacters
        let random                  = drand48()
        let randomGeneration        = UInt64(round(random * pow(10, Double(allowedRandom)) * 9) + pow(10, Double(allowedRandom)))
        let mergedChar              = "\(randomGeneration)\(userId)\(timestamp)"
        print(mergedChar)
        let mergedCharINT           = BigInt(mergedChar)
        let base36                  = mergedCharINT?.toBase(b: 36)
        let leftCharcter            = maxAllowedBookingNo - (base36?.characters.count)!
        print("Booking no charcter left \(leftCharcter)")
        
        return base36!
    }
    
    func gotResponseFromTripStart(response : JSON){
        
        uiFun.hideIndicatorLoader()
        print("JSON Response Of Trip Pass Start \(response)")
        if(String(describing: response["code"]) == "5000"){
            
            let processResponseToDatabase   = MetroJSONResponses()
            let tripPassResponse            = processResponseToDatabase.processMetroJSONResponse(response: response)
            
            uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
        }else{
            let errorJSON       = response["error"]
            uiFun.showAlert(title: "", message: String(describing : errorJSON["errorCodeText"]), logMessage: "Start Trip Pass Error", fromController: self)
        }
        
    }
    
    @IBAction func startTripAction(_ sender: Any) {
        
        uiFun.showIndicatorLoader()
        let bookingNo       = uiFun.readData(key: "storeValueBookingId")
        let compantRefid    = uiFun.readData(key: "metroStoreValueMasterQRCode")
        let itemId          = createBookingNo()
        
        let tripStartAPI    = TripStartAPI()
        tripStartAPI.makeAPICall(bookingNo: bookingNo, companyItemId: compantRefid, companyReferenceId: itemId!, fromViewController: self)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    @IBAction func rechargePassAction(_ sender: Any) {
        isFromRechargePass = true
        uiFun.navigateToScreem(identifierName: "StoreValueRechargeScreenViewController", fromController: self)
    }
    
    
}
extension Decimal {
    var doubleValue:Double {
        return NSDecimalNumber(decimal:self).doubleValue
    }
}

extension BigInt {
    func toBase(b:BigInt) -> String {
        let dict = [10:"A", 11:"B", 12:"C", 13:"D",14:"E",15:"F", 16:"G", 17:"H", 18:"I", 19:"J", 20:"K", 21:"L",22:"M", 23:"N", 24:"O", 25:"P", 26:"Q",27:"R", 28:"S", 29:"T", 30:"U", 31:"V", 32:"W", 33:"X", 34:"Y", 35:"Z"]
        var num = self.abs
        var str = ""
        repeat {
            let remainder   = num % b.abs
            if let l = dict[Int(remainder.toIntMax())] {
                str = l + str
            }
            else {
                str = String(num % b.abs) + str
                print("Base64 \(str)")
            }
            
            num = num/b.abs
        }
            while num > 0
        return self < 0 ? "-\(str)" : str
        
    }
}
