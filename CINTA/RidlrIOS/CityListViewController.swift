//
//  CityListViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 26/10/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import CoreData

class CityListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var toolbarView: UIView!
    var uiFun       = UiUtillity()
    var ridlrColors = RidlrColors()
    
    @IBOutlet var backButton: UIButton!
    
    var window: UIWindow?
        
    var city = ["TOP CITIES", "Bangalore", "Chennai", "Delhi", "Mumbai", "Pune", "Other Major Cities", "Ahmedabad", "Bellary", "Bhavnagar", "Bhopal", "Bhubaneswar", "Chandigarh", "Hyderabad", "Indore", "Jaipur", "Kanpur", "Kolhapur", "Kolkata", "Lucknow", "Ludhiana", "Nagpur", "Nashik", "Rajkot", "Vadodara", "Vijaywada"] // Array Of Cities
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return city.count // Count Of Rows In Table
    }
        
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        let cell                = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell") // Assinging Cell
        cell.selectionStyle     = UITableViewCellSelectionStyle.blue // When User Clicks On Any City, It Will Highlighted As Blue
        cell.textLabel?.text    = city[indexPath.row]   // Assigning City Name To Each Row In Table From Array
        
        if indexPath.row == 0 || indexPath.row == 6{
            cell.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.greyColor) // Separating Top Cities And Other Major Cities
            cell.selectionStyle  = UITableViewCellSelectionStyle.none
        }
        
        let cityName = uiFun.readFromDatabase(entityName: "Users", key: "city") // Reading City From Core Data
        if(cityName != ""){
        
            if(cityName == city[indexPath.row]){
                cell.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlr_brand_color_light)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(city[indexPath.row])
        
        if(cityParserList.count != 0){
            cityParserList.removeAll()
        }
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let selectedCity = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
        selectedCity.setValue(city[indexPath.row], forKey: "city") // Inserting City Value In Core Data
        
        uiFun.writeData(value: city[indexPath.row], key: "city")
        uiFun.writeToDatabase(entityValue: city[indexPath.row], entityName: "Users", key: "city") // Inserting City Value In Core Data
        do {
            try context.save()
            print("Saved Successfully")
        }
        catch {
            print("There is an error")
        }
        
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        toolbarView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        uiFun.writeData(value: "Nope", key: "TimelineLogin")
        
        if(changeCityFrom == "navigation"){
            backButton.isHidden = false
        }else{
            backButton.isHidden = true
        }
        
        uiFun.createCleverTapEvent(message: "City ListView Screen")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
}
