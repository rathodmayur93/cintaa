//
//  TripPassModel.swift
//  RidlrIOS
//
//  Created by Mayur on 27/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class TripPassModel {

    var amount              = ""
    var paymnetMethod       = ""
    var paymentGateway      = ""
    var originStation       = ""
    var destinationStation  = ""
    var expiryDate          = ""
    var masterQRCode        = ""
    var trips               = ""
    var bookingId           = ""
    var itemSubType         = ""
    var balance             = ""
}
