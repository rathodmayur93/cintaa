//
//  MetroRechargeSuccessViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 19/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class MetroRechargeSuccessViewController: UIViewController {

    @IBOutlet var successIV                 : UIImageView!
    
    @IBOutlet var successLabel              : UILabel!
    @IBOutlet var successDescriptionLabel   : UITextView!
    
    @IBOutlet var bookingIdLabel            : UILabel!
    @IBOutlet var bookingId                 : UILabel!
    @IBOutlet var rechargeAmountLabel       : UILabel!
    @IBOutlet var rechargeAmountTextLabel   : UILabel!
    
    @IBOutlet var cardNumber                : UILabel!
    @IBOutlet var timeLabel                 : UILabel!
    @IBOutlet var paymentMode               : UILabel!
    @IBOutlet var bookingIdValueLabel       : UILabel!
    
    @IBOutlet var rechargeView              : UIView!
    @IBOutlet var containerView             : UIView!
    @IBOutlet var pageControl               : UIPageControl!
   
    @IBOutlet var whatsNextView             : UIView!
    let uiFun = UiUtillity()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        uiFun.createCleverTapEvent(message: "Metro Recharge Successful Screen")
        
        let metroCardNumber = uiFun.readData(key: "bestPassNumber") // Fetching Card Number From permanent data storage
        cardNumber.text = metroCardNumber
    
        containerView.isHidden = false  // Tutorial Screen is hidden
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year    = components.year
        let month   = components.month
        let day     = components.day
        
        timeLabel.text = "\(day!)-\(month!)-\(year!)" 
        
        // This check defines whether the user came from payment screen or from profile booking history page
        if(showBookingReceipt){
            
            // User came from profile booking history page
            if(bookingList[receiptIndex].companyStatus == "SUCCESS"){
                
                bookingId.text           = bookingList[receiptIndex].bookingId // fetching booking id from bookingList
                rechargeAmountLabel.text = "₹ \(bookingList[receiptIndex].totalAmount)"
                paymentMode.text         = "Paid via \(bookingList[receiptIndex].paidVia)"
                cardNumber.text          = bookingList[receiptIndex].companyItemId
                print("Booking Payment \(bookingList[receiptIndex].paidVia)")
                
                let timestampDate                 = NSDate(timeIntervalSince1970: Double(bookingList[receiptIndex].timestamp)!/1000)
                let dayTimePeriodFormatter        = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
                // Converting timestamp to date format
                let dateString                    = dayTimePeriodFormatter.string(from: timestampDate as Date)
                timeLabel.text                    = dateString
                uiFun.createCleverTapEvent(message: "Metro Recharge Amount \(bookingList[receiptIndex].totalAmount)")
            }else{
                // If recharge status is fail
                successIV.image          = UIImage(named: "ic_fail") // Changing Success image to fail
                successLabel.text        = "Recharge Failed"
                containerView.isHidden   = true                      // Hide tutorial screen
                whatsNextView.isHidden   = true                      // Hide Whats new label
                cardNumber.text          = bookingList[receiptIndex].companyItemId //  Recharge Card Number
                paymentMode.text         = "Paid via \(bookingList[receiptIndex].paidVia)"
                
                let timestampDate                 = NSDate(timeIntervalSince1970: Double(bookingList[receiptIndex].timestamp)!/1000)
                let dayTimePeriodFormatter        = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
                // Converting timestamp to date format
                let dateString                    = dayTimePeriodFormatter.string(from: timestampDate as Date)
                timeLabel.text                    = dateString
            }
        }else{
            
            // If user is from payment page this block of code gets execute
            print("Metro Recharge Statu \(paymentResponseListWebview[0].status)")
            if(paymentResponseListWebview[0].status == "OK" || paymentResponseListWebview[0].status == "SUCCESS"){
            
                // Recharged successful
                bookingId.text           = paymentResponseListWebview[0].data[0].paymentDetails[0].bookingId
                rechargeAmountLabel.text = "₹ \(paymentResponseListWebview[0].data[0].paymentDetails[0].amount)"
                paymentMode.text         = "Paid via \(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentMode) (\(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentGateway))"
                successLabel.text        = "Recharge Successful"
                cardNumber.text          = paymentResponseListWebview[0].data[0].paymentDetails[0].cardNumber
                uiFun.createCleverTapEvent(message: "Metro Recharge Amount \(paymentResponseListWebview[0].data[0].paymentDetails[0].amount)")
            }else{
            
                // Recharged fail
                successIV.image = UIImage(named: "ic_fail")
                successLabel.text = "Recharge Failed"
                bookingId.text           = paymentResponseListWebview[0].data[0].paymentDetails[0].bookingId
                rechargeAmountLabel.text = "₹ \(paymentResponseListWebview[0].data[0].paymentDetails[0].amount)"
                paymentMode.text         = "Paid via \(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentMode) (\(paymentResponseListWebview[0].data[0].paymentDetails[0].paymentGateway))"
             
                uiFun.createCleverTapEvent(message: "Metro Recharge Amount \(paymentResponseListWebview[0].data[0].paymentDetails[0].amount)")
                
                containerView.isHidden = true
                whatsNextView.isHidden = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeButtonTapped(_ sender: AnyObject) {
        
        // Checks When User clicks on back button where to navigates
        if(showBookingReceipt){
            // Navigate to Booking History Page
            showBookingReceipt = false
            uiFun.navigateToScreem(identifierName: "BookingsViewController", fromController: self)
        }else{
            // Navigate to homepage
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        }
        
    }
  
}
