//
//  TimetablePageViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 01/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class TimetablePageViewController: UIPageViewController {
    
    var uiFun = UiUtillity()
    var ridlrColors = RidlrColors()
    
    func getTimeTable() -> TimeTableViewController{
    
        return storyboard!.instantiateViewController(withIdentifier: "PageContentTrainViewController") as! TimeTableViewController
    }
   /*       UNCOMMENT THIS TO SEE PageViewController
    func getBusTimeTable() -> BusTimeTableViewController{
    
        return storyboard!.instantiateViewController(withIdentifier: "BusTimeTableViewController") as! BusTimeTableViewController
    }
    */

    override func viewDidLoad() {
        super.viewDidLoad()

        
        view.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)

        setViewControllers([getTimeTable()], direction: .forward, animated: false, completion: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*      UNCOMMENT THIS TO SEE PageViewController
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        if viewController.isKind(of: BusTimeTableViewController.self){
            
            return getTimeTable()
        }else{
            return nil
        }

    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        if viewController.isKind(of: TimeTableViewController.self){
        
            return getBusTimeTable()
        }else{
            return nil
        }
    }
    */
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 3
    }

    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}
