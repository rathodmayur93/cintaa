//
//  CityParser.swift
//  RidlrIOS
//
//  Created by Mayur on 10/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var cityParserList : [CityParserModel] = []
var cityIdFromData = ""
class CityParser {

    
    let uiFun           = UiUtillity()
    
    func demoReadFromCSV(cityName : String){
        
        
        let queueName = DispatchQueue(label: "data.city.processing")
        // creating thread to run in background
        queueName.async {
            let csvContent = self.uiFun.readFromCSV(fileName: "splashCityData")
            let csvNewLineContent = csvContent.components(separatedBy: .newlines)
            let delimiter = ","
            var inCity = true
            var inMode = false
            
            var lines:[String] = csvNewLineContent
            
            print("SplashCity Data Line Count is \(lines.count)")
            
            let cityParser      = CityModel()
            let cityParserModel = CityParserModel()
            
            var count = 0
            for line in lines {
                
                
                    var getCityData = line.components(separatedBy: delimiter)
                        
                    if(getCityData[0] != ""){
                        // fetching city details
                        if(getCityData[0] == "city" && getCityData[2] == cityName){
                            
                            cityParser.cityId       = getCityData[1]
                            cityIdFromData          = getCityData[1]
                            cityParser.cityName     = getCityData[2]
                            cityParser.minLat       = getCityData[3]
                            cityParser.maxLat       = getCityData[4]
                            cityParser.minLong      = getCityData[5]
                            cityParser.maxLong      = getCityData[6]
                            cityParser.centerLat    = getCityData[7]
                            cityParser.centerLong   = getCityData[8]
                            cityParser.cityBound    = getCityData[9]
                            
                            inCity = true
                            
                        }else if(getCityData[0] == "city"){
                         
                            inCity = false
                            
                        }else if(getCityData[0] == "mode" && inCity){
                            
                            cityParser.mode.append(self.getModeObject(getCityData: getCityData))
                            inMode = true
                            count += 1
                        
                        }else if(getCityData[0] == "mode" && !inCity){
                         
                            inMode = false
                        
                        }else if(inCity && inMode){
                            
                            cityParser.mode[count - 1].agencies.append(self.getAgenciesObject(getCityData: getCityData))
                            cityParserModel.cityModel.append(cityParser)
                            cityParserList.append(cityParserModel)
                        }
                    }
                }
            
            }
        }
    
    func getModeObject(getCityData : [String]) -> Mode{
        // return the mode of the city
        let mode = Mode()
        
        mode.modeId         = getCityData[1]
        mode.modeName       = getCityData[2]
        mode.modePriority   = getCityData[3]
        
        return mode
    }

    func getAgenciesObject(getCityData : [String]) -> Agency{
        // return the agencies of mode
        let agencies = Agency()
        
        agencies.agencyId           = getCityData[0]
        agencies.agencyLongName     = getCityData[1]
        agencies.agencyShortName    = getCityData[2]
    
        if(getCityData[3] != ""){
            agencies.agencyColor = getCityData[3]
        }
        
        return agencies
    }
    
    func getModeId(modeName : String) -> String{
        // return the mode id
        var i = 0
        while(i < cityParserList[0].cityModel[0].mode.count){
        
            if(cityParserList[0].cityModel[0].mode[i].modeName == modeName){
                print("Returning Mode Value \(cityParserList[0].cityModel[0].mode[i].modeId)")
                return cityParserList[0].cityModel[0].mode[i].modeId
            }
            i += 1
        }
        return ""
    }
    
    func getAgencies(modeName : String) -> [Agency]{
        // return the agencies of mode
        var i = 0
        while(i < cityParserList[0].cityModel[0].mode.count){
        
            if(cityParserList[0].cityModel[0].mode[i].modeName == modeName){
                return cityParserList[0].cityModel[0].mode[i].agencies
            }
            i += 1
        }
        return []
    }
    
    
    
}
