//
//  mysectrust.h
//  RidlrIOS
//
//  Created by Mayur on 04/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

#ifndef mysectrust_h
#define mysectrust_h

#include <stdio.h>
#include <Security/Security.h>

SecTrustRef changeHostForTrust(SecTrustRef trust);

#endif /* mysectrust_h */
