//
//  NewsFeedTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 12/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

protocol MyCellDelegate {
    func btnCloseTapped(cell: NewsFeedTableViewCell)
    func btnLikeTapped(cell : NewsFeedTableViewCell)
    func btnShareTapped(cell : NewsFeedTableViewCell)
}

class NewsFeedTableViewCell: UITableViewCell {

    @IBOutlet var userProfileIV     : UIImageView!
    @IBOutlet var usernameLabel     : UILabel!
    @IBOutlet var timeLabel         : UILabel!
    
    @IBOutlet var emotionIV         : UIImageView!
    @IBOutlet var emotionLabel      : UILabel!
    
    @IBOutlet var newsFeedContent   : UITextView!
    
    @IBOutlet var foundUsefulLabel  : UILabel!
    @IBOutlet var repliesLabel      : UILabel!
    
    @IBOutlet var likeBT            : UIButton!
    @IBOutlet var replyBT           : UIButton!
    @IBOutlet var shareBT           : UIButton!
    @IBOutlet var timelineIV        : UIImageView!
    @IBOutlet var deleteBT          : UIButton!
    
    var delegate: MyCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       userProfileIV.layer.cornerRadius = userProfileIV.frame.size.width / 2;
       //userProfileIV.clipsToBounds = true;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        if let _ = delegate {
            delegate?.btnCloseTapped(cell: self)
        }
    }
    
    @IBAction func likeButtonAction(_ sender: Any) {
        if let _ = delegate {
            delegate?.btnLikeTapped(cell: self)
        }
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        if let _ = delegate {
            delegate?.btnShareTapped(cell: self)
        }
    }
}


