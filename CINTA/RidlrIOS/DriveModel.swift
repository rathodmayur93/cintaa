//
//  DriveModel.swift
//  RidlrIOS
//
//  Created by Mayur on 07/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class DriveModel {

    var distance            = ""
    var roadProperties      = [RoadProperties]()
    var route               = ""
    var summary             = ""
    var tte                 = ""
    var destination         = ""
    var destinationCoords   = ""
    var source              = ""
    var sourceCoords        = ""
}

class RoadProperties{

    var color               = ""
    var legend              = ""
    var roadAreaName        = ""
    var speedKPH            = ""
    var time                = ""
}
