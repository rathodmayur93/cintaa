//
//  TrafficNearYouViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 03/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON
import GooglePlaces
import CoreLocation
import KCFloatingActionButton

var showPlanner         = false
var fromTrafficPage     = false
var redirectingPage     = ""
class TrafficNearYouViewController: UIViewController, GMSMapViewDelegate, UITextFieldDelegate, CLLocationManagerDelegate, UIAlertViewDelegate {

    let screenSize : CGRect         = UIScreen.main.bounds
    let uiFun                       = UiUtillity()
    let ridlrColors                 = RidlrColors()
    var mapView : GMSMapView        = GMSMapView()
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    var searchBar                   : UISearchBar?
    var tableDataSource             : GMSAutocompleteTableDataSource?
    var searchController            : UISearchController?
    var locationManager             = CLLocationManager()
    var indicator                   = UIActivityIndicatorView()
    var alert: UIAlertView          = UIAlertView()
    
    var minLat : Double             = Double()
    var maxLat : Double             = Double()
    var minLong: Double             = Double()
    var maxLong: Double             = Double()
    
    var task : URLSessionTask       = URLSessionTask()
    var timer : Timer               = Timer()
    
    @IBOutlet var trafficView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate  = self
        
        activityIndicator()
        uiFun.createCleverTapEvent(message: "TRAFFIC NEAR BY YOU SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            print("Authorized")
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            let alertController = UIAlertController (title: "Location Setting", message: "Please provide location access so we can show you traffic around you.", preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                        redirectingPage = "TrafficNearYouViewController"
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (UIAlertAction) in
                self.uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            })
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
            
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Fetching user location
        print("Location Manger Updating Location")
        let userLocation: CLLocation = locations[0]
        let latitude                 = userLocation.coordinate.latitude
        let longitude                = userLocation.coordinate.longitude
        
        uiFun.writeData(value: String(describing :latitude), key: "lat")
        uiFun.writeData(value: String(describing :longitude), key: "long")
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = false
        
        initMap()
        setUI()
        mapView.delegate = self
    }
    
    func activityIndicator() {
        // Show loader
        indicator.frame                         = CGRect(x: 0, y: 0, width: 60, height: 60)
        indicator.activityIndicatorViewStyle    = UIActivityIndicatorViewStyle.gray
        indicator.center                        = self.view.center
        self.view.addSubview(indicator)
    }
    
    func initMap(){
    
        let lati                        = uiFun.readData(key: "lat") // Fetching user latitude from parmanent storage
        let long                        = uiFun.readData(key: "long") //Fetching user longitude from parmanent storage
        
        // settting camera postino and lat long for map
        let camera                      = GMSCameraPosition.camera(withLatitude: Double(lati)!, longitude: Double(long)!, zoom: 15.0)
        mapView                         = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled     = true
        view                            = mapView
        
        mapView.settings.zoomGestures   = true
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        
        marker.position     = CLLocationCoordinate2D(latitude: Double(lati)!, longitude: Double(long)!) // postion of marker
        marker.map          = mapView
        
        let region          = GMSVisibleRegion.init()
        
        // FInding visible region on map
        print("GMS REgion lat long \(region.farLeft.latitude) \(region.farLeft.longitude) ")
        print("GMS REgion lat long \(region.farRight.latitude) \(region.farRight.longitude) ")
        print("GMS REgion lat long \(region.nearLeft.latitude) \(region.nearLeft.longitude) ")
        print("GMS REgion lat long \(region.nearRight.latitude) \(region.nearRight.longitude) ")
        
        let path            = GMSMutablePath()
        path.add(CLLocationCoordinate2DMake(Double(lati)!, Double(long)!))
        
        let bounds          = GMSCoordinateBounds(path: path)
        print("Path Region is \(bounds.northEast.latitude) \(bounds.northEast.longitude) \(bounds.southWest.latitude) \(bounds.southWest.longitude)")
        
        // Bounds of the visible region
        let gmsBounds       = GMSCoordinateBounds.init(region: mapView.projection.visibleRegion())
        
        print("gmsBounds are \(gmsBounds.northEast.latitude) \(gmsBounds.northEast.longitude)")
        print("gmsBounds are \(gmsBounds.southWest.latitude) \(gmsBounds.southWest.longitude)")
    }
    
    public func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
    
        // Setting map and its properties
        print("End od map dragging ....")
        
        // Start loader
        indicator.startAnimating()
        indicator.backgroundColor = UIColor.clear
        var time                  = 0
        
        timer.invalidate()  // Invalidate the timer or stop the timer
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in   // Start the timer
        mapView.setMinZoom(13.0, maxZoom: 17.0)     // Min and max zoom of map
            
            time += 1
            print("Traffic Near you Timer \(time)")
            
            if(time == 1){
                timer.invalidate()            
                print("Map Four Cordinates \(self.mapView.bounds)")
                
                // bound of the visible map region
                let bounds = mapView.projection.visibleRegion()
            
                print("Bounds of map end \(bounds.farLeft.latitude) \(bounds.farLeft.longitude) \(bounds.farRight.latitude) \(bounds.farRight.longitude) \(bounds.nearLeft.latitude) \(bounds.nearLeft.longitude) \(bounds.nearRight.latitude) \(bounds.nearRight.longitude)")
            
                self.indicator.startAnimating()
                self.indicator.backgroundColor = UIColor.clear
            
                let zoomLevel = position.zoom
                print("map zoom level \(zoomLevel)")
            
                // If zoom level is between this range make an api call
                if(Int(zoomLevel) < 19 || Int(zoomLevel) > 15){
                    
                    self.minLat  = bounds.nearLeft.latitude
                    self.maxLat  = bounds.farRight.latitude
                    self.minLong = bounds.nearLeft.longitude
                    self.maxLong = bounds.farRight.longitude
                    
                    self.mapDataAPICall()
                    
                }
            }
        }
        
    }
    
    
    
    func setUI(){
    
        /*************  SEARCH BUTTON ON TOP ****************/
        
        let searchButton                = UIButton(type: .system)
        searchButton.frame              = CGRect(x: 8, y: 24, width: (screenSize.width - 16), height: 36)
        searchButton.backgroundColor    = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        searchButton.setTitle("Select Location", for: .normal)
        searchButton.setTitleColor(UIColor.black, for: .normal)
        searchButton.addTarget(self, action: #selector(searchButtonAction), for: .touchUpInside)
        searchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        searchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 48, 0, 0)
        self.view.addSubview(searchButton)
        
        /*************  BACK BUTTON ON TOP ****************/
        
        let backButton                  = UIButton(type: .system)
        backButton.frame                = CGRect(x: 16, y: 24, width: 36, height: 36)
        let buttonImage                 = UIImage(named: "ic_indicator_go_back_holo")
        backButton.setImage(buttonImage, for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        self.view.addSubview(backButton)
        
        /**************** BOTTOM BAR CONGESTION & SMOOTH *****************/
        
        
        
        /******************* ROUTE SEARCH FAB *******************/
        
        let fab = KCFloatingActionButton()
        fab.buttonColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        fab.addItem("Route Search", icon: UIImage(named: "ic_indicator_plan")!, handler: { item in
            fromTrafficPage = true
            self.uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
            fab.close()
        })
        fab.addItem("Compose", icon: UIImage(named: "ic_icon_edit_grey")!, handler: { item in
            fromTrafficPage = true
            self.uiFun.navigateToScreem(identifierName: "ComposeViewController", fromController: self)
            fab.close()
        })
        
        self.view.addSubview(fab)
    }
    
    func alertIndicator(){
    
        // Loader in the alert box Not in use but if we need this in future just call this function wherever u want
        alert = UIAlertView(title: "Please Wait...", message: "", delegate: nil, cancelButtonTitle: "Cancel");
       
        alert.delegate  = self
        
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 50, y: 4, width: 48, height: 48)) as UIActivityIndicatorView
        loadingIndicator.center = self.view.center
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert.setValue(loadingIndicator, forKey: "accessoryView")
        loadingIndicator.startAnimating()
        
        alert.show();
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
       
        // Button of the laoder in alert box
        let buttonTitle = alertView.buttonTitle(at: buttonIndex)
        print("\(buttonTitle) pressed")
       
        if buttonTitle == "Go Back" {
            showPlanner = true
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            KCFABManager.defaultInstance().hide()
        }else if(buttonTitle == "Cancel"){
            task.cancel()
        }
    }
    
    func routeSearchTapped(){
        // When user click on Floatin action button of route search will navigate to FindARouteViewController Screen
        KCFABManager.defaultInstance().hide()
        uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
    }
    
    func searchButtonAction(sender: UIButton!) {
        // When user click on search text field to search any location on map
        print("Button tapped")
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func backButtonAction(sender: UIButton){
        // When user clicks on back button
        print("Back button tapped")
        showPlanner = true
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        KCFABManager.defaultInstance().hide()
        
        mapView.isMyLocationEnabled     = false
        locationManager.stopUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
    }
    
    func mapDataAPICall(){
    
        // Fetching traffic data api call
        let mapDataApi = MapDataAPI()
        mapDataApi.makeAPICall(fromViewController: self, zoomLevel: 15, minLat: minLat, minLong: minLong, maxLat: maxLat, maxLong: maxLong)
        
        let mapDataUrl = "http://ridlr.in/callWebService_Get.php?webMethodName=MapDataAsJSonWap&parameters=bound1:\(minLat),\(minLong),\(maxLat),\(maxLong)~bound2:0,0,0,0~zoom:4"
        
        let url = NSURL(string: mapDataUrl)!
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "GET"
        
        task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error != nil{
                print("Certificate Error -> \(error!)")
                if(error.debugDescription.contains("timed out")){
                    self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                }else if(error.debugDescription.contains("connection was lost")){
                    self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                }else if(error.debugDescription.contains("appears to be offline")){
                    self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                }
                return
            }else{
                
                if let urlContent = data{
                    
                    do {
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        print("Yo Yo \(jsonResult)")
                        self.responseOfMapData(response: JSON(jsonResult))
                    }
                    catch{
                        print("Failed To Convert JSON")
                    }
                }
            }
            
        }
        
        task.resume()
    }
    
    func responseOfMapData(response : JSON){
    
        
        // response of map data
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        
        print("MAP DATA Response Got it")
            
            let rc = response["rc"]
            var i = 0
            
            while i < rc.count{
                
                let data = rc.arrayValue[i]["data"]
                var j = 0
                let path = GMSMutablePath()
                
                while j < data.count{
                    
                    //fetching lat long from the api response
                    let lati = Double(String(describing : data.arrayValue[j]["lt"]))
                    let long = Double(String(describing : data.arrayValue[j]["ln"]))
                    
                    path.add(CLLocationCoordinate2D(latitude: lati!, longitude: long!)) // adding data into Google mutable path
                    j += 1
                }
            
                // Creating polylines on the map
                let polyline = GMSPolyline(path: path)
                
                // Color of polyline
                polyline.strokeColor = self.uiFun.hexStringToUIColor(hex: String(describing: rc.arrayValue[i]["color"]))
                polyline.strokeWidth = 3.0      // width of the polyline
                polyline.map         = self.mapView // adding polylines on map
                let solidRed         = GMSStrokeStyle.solidColor(self.uiFun.hexStringToUIColor(hex: String(describing: rc.arrayValue[i]["color"])))
                polyline.spans       = [GMSStyleSpan(style: solidRed)]
                polyline.geodesic    = true
                i += 1
                UIApplication.shared.endBackgroundTask(self.backgroundTask)
            }
        alert.dismiss(withClickedButtonIndex: 0, animated: true)
        }
    }

extension TrafficNearYouViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection or user search.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place Lat Long: \(place.coordinate)")
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 13.0)
        mapView.animate(to: camera)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
