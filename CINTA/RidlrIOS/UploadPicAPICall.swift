//
//  UploadPicAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 15/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class UploadPicAPICall{
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    var encodedPassKey = ""
    
    func makeAPICall(fromViewController : ComposeViewController, loginName : String, imageName : String, base64String : String, lat : String, long : String){
        
        
        let headers             = ["Content-Type": "application/x-www-form-urlencoded","Cache-Control": "no-cache"]
        
        let postData            = NSMutableData(data: "loginname=Mayur".data(using: String.Encoding.utf8)!)
        postData.append("&latlng=\(lat),\(long)".data(using: String.Encoding.utf8)!)
        postData.append("&passkey=NGQyYWE5YjI1OTNmZDM3MA==".data(using: String.Encoding.utf8)!)
        postData.append("&imageName=mayur.png".data(using: String.Encoding.utf8)!)
        postData.append("&custKey=f29a3c79212611e3aeb927d5ba248633".data(using: String.Encoding.utf8)!)
        postData.append("&groupId=g2apps".data(using: String.Encoding.utf8)!)
        postData.append("&accessType=device".data(using: String.Encoding.utf8)!)
        postData.append("&userid=-111".data(using: String.Encoding.utf8)!)
        postData.append("&sBase64InputStream=\(base64String)".data(using: String.Encoding.utf8)!)
        
        let postDataString      = "loginname=Mayur&latlng=\(lat),\(long)&passkey=NGQyYWE5YjI1OTNmZDM3MA==&imageName=\(imageName).png&custKey=f29a3c79212611e3aeb927d5ba248633&groupId=g2apps&accessType=device&userid=-111&sBase64InputStream=\(base64String)"
        let postDataStringData  = postDataString.data(using: String.Encoding.utf8)
        
        print("latlng \(lat) \(long) passkey \(constant.PASS_KEY) custkey \(constant.CUST_KEY) base64 \(base64String)")
        
        let request             = NSMutableURLRequest(url: NSURL(string: "https://api.saarath.com/MobileAppsWS/MobileAppWS.asmx/UploadProfilePicture")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 60.0)
        request.httpMethod              = "POST"
        request.allHTTPHeaderFields     = headers
        request.httpBody                = postDataStringData! as Data
        
        let session  = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
                
                if let urlContent = data{
                    
                    do {
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        print("Yo Yo \(jsonResult)")
                        DispatchQueue.main.sync(execute: {
                            fromViewController.responseFromUploadPicApi(jsonResponse: JSON(jsonResult))

                        })
                    }
                    catch{
                        print("Failed To Convert JSON")
                    }
                }

            }
        })
        
        dataTask.resume()
    }
    
    func uploadPicJSONBody(loginName : String, imageName : String, base64String : String, encodedPassKey : String, lat : String, long : String) -> String?{
        
        // Upload pic json string
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(loginName, forKey: "loginname")
        para.setValue("\(lat),\(long)", forKey: "latlng")
        para.setValue(imageName, forKey: "imageName")
        para.setValue(constant.CUST_KEY, forKey: "custKey")
        para.setValue("g2apps", forKey: "groupId")
        para.setValue(constant.ACCESS_TYPE, forKey: "accessType")
        para.setValue("-111", forKey: "userid")
        para.setValue(base64String, forKey: "sBase64InputStream")
        para.setValue(encodedPassKey, forKey: "passkey")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("uploading timeline pic json string = \(jsonString)") // Return json string for api call
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

}
