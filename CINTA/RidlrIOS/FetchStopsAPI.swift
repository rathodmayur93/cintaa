//
//  FetchStopsAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 22/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchStopsAPI{

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TimetableStationViewController, cityId : String, modeId : String){

        let url = "\(constant.TIMETABLE_STAGING_BASE_URL)list/stop/cityid/\(cityIdFromData)/modeid/\(modeId)/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Fetch Stops URL \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                print("Reached Success Response")
                fromViewController.FetchStopAPIResponse(response: jsonResponse)
                print("Successfully Timetable Stops fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable Stops fetching failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }

    
}
