//
//  MetroParserModel.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MetroParserModel{

    var metroStops = [MetroStops]()
}

class MetroStops{

    var stopId      = ""
    var stopName    = ""
    var stopLat     = ""
    var stopLong    = ""
}
