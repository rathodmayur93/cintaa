//
//  RidlrPointsViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 17/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class RidlrPointsViewController: UIViewController {

    @IBOutlet var rcashLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        rcashLabel.text = rcashBalance      
        // Do any additional setup after loading the view.
        uiFun.createCleverTapEvent(message: "User Profile Rcash Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
