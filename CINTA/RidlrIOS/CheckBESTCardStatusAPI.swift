//
//  CheckBESTCardStatusAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 11/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CheckBESTCardStatusAPI {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : RenewBusPassViewController, passNumber : String, from : String){
        
        let randomNum:UInt32            = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String      = String(randomNum)
        let timeStamp                   = Int(NSDate().timeIntervalSince1970)
        
        let url = "\(constant.STAGING_BASE_URL)/inventory/item/?random=\(randomNumString)&company=BEST&identifier=\(passNumber)&item=PASS&current_time=\(timeStamp)" // API Url for checking pass status
        
        print("Payment Option Fetcing Balance URL \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                if(from == "normal"){
                    fromViewController.BESTCardStatusResponse(response: jsonResponse)
                }else if(from == "action"){
                    fromViewController.buttonClickResponse(response: jsonResponse)
                }else if(from == "lastRecharge"){
                    fromViewController.lastPassRechargeResponse(response: jsonResponse)
                }
                
                print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
}
