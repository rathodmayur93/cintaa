//
//  NewsFeedViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 11/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import KCFloatingActionButton

var newsFeedIndex       : Int!
var newsFeedList        = [NewsFeedModel]()
var mediaLinkToLoad     = ""
var likeIndex           = 0

class NewsFeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MyCellDelegate {

    let uiFun = UiUtillity()
    let ridlrColors = RidlrColors()
    
    @IBOutlet var newsFeedTableView : UITableView!
    @IBOutlet var backButton        : UIButton!
    @IBOutlet var timelineIV        : UIImageView!
    @IBOutlet var deleteButton      : UIButton!
    
    var refreshControl              : UIRefreshControl!
    
    var difference : Date           = Date()
    var v : UIView!
    var noOfRecord                  = 10
    var lastPostOn                  = ""
    var todayDate                   = ""
    var lastPostIndex : Int         = Int()
    var sinceId                     = ""
    var firstSinceId                = ""
    var lastSinceId                 = ""
    var firstPostTime               = ""
    var lastPostTime                = ""
    var indexOfNewsFeed : Int       = Int()
    var cellHeight                  = 197.0
    var cellbackgroundHeight        = 205
    
     override func viewDidLoad() {
        super.viewDidLoad()

        if(newsFeedList.count == 0){
            
            let newsFeedAPI = NewsFeedAPICall() // Fetch News Feed API call
            newsFeedAPI.makeAPICall(fromViewController: self, firstPostOn: "", lastPostOn: "", sinceId: "")
        }
        createFAB()
        
        newsFeedTableView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        newsFeedTableView.separatorStyle  = UITableViewCellSeparatorStyle.none
        
        /************* PULL TO REFRESH ***************/
        refreshControl                  = UIRefreshControl()
        refreshControl.attributedTitle  = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: UIControlEvents.valueChanged)
        
        newsFeedTableView.addSubview(refreshControl)
        newsFeedTableView.estimatedRowHeight = CGFloat(cellHeight)
        
        let todaysDate:NSDate           = NSDate()
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat        = "yyyy-MM-dd HH:mm:ss.SSS"
        todayDate                       = dateFormatter.string(from: todaysDate as Date)
    
        print("today time is \(todaysDate)")
        uiFun.createCleverTapEvent(message: "NEWS FEED SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(newsFeedList.count == 0){
            uiFun.showIndicatorLoader()
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
    
        print("news feed count \(newsFeedList.count)")
        lastPostIndex = newsFeedList.count - 1
        print("last post index \(lastPostIndex)")
        return newsFeedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = newsFeedTableView.dequeueReusableCell(withIdentifier: "Cell") as! NewsFeedTableViewCell
       
        if(newsFeedList.count != 0){
        cell.emotionLabel.text      = ""
        cell.foundUsefulLabel.text  = newsFeedList[indexPath.row].likesCount        // Set like count value from api response
        cell.newsFeedContent.text   = newsFeedList[indexPath.row].textUpdate        // Set news feed content from api response
        cell.repliesLabel.text      = newsFeedList[indexPath.row].repliesCount      // Set reply count value from api response
        cell.delegate               = self
        
            // Checks whether the post is of the user or of some other user
            if(newsFeedList[indexPath.row].isMyOwnPost){
                cell.deleteBT.isHidden  = false
            }else{
                cell.deleteBT.isHidden  = true
            }
        
        if(newsFeedList[indexPath.row].modifiedDate != "null"){
            // Last modified date of the post if this date is present than show this date on post
            let stringToDate = uiFun.convertStringToDate(dateString: newsFeedList[indexPath.row].modifiedDate)
            let timeInterval = difference.offset(from: stringToDate)
            print("Date difference is \(timeInterval)")
            
            cell.timeLabel.text = timeInterval

        }else{
            // Date of the post
            let stringToDate = uiFun.convertStringToDate(dateString: newsFeedList[indexPath.row].postedOn)
            let timeInterval = difference.offset(from: stringToDate)
            print("Date difference is \(timeInterval)")
            cell.timeLabel.text = timeInterval
        }
        
        if(newsFeedList[indexPath.row].mediaLink != "null"){
        
            // Attached image or video url of the post
            let timelineIVUrl = newsFeedList[indexPath.row].mediaLink
            uiFun.loadImageFromURL(url: timelineIVUrl) { (result, error) in
                
                if result != nil{
                    // Setting media from url
                    if let mediaLinkData            = UIImage(data: result!){
                        cell.timelineIV.image       = mediaLinkData
                        cell.timelineIV.tag         = indexPath.row
                        self.cellHeight             = 286.0
                        self.cellbackgroundHeight   = 290
                        print("Media Link Set Successfully")
                        return
                    }
                }else {
                    cell.timelineIV.isHidden = true
                    print("Image Set Failed....Try Harder Mayur ")
                    
                    return
                }
            }
        }else{
            cellHeight = 197.0
            cellbackgroundHeight = 205
            cell.timelineIV.isHidden = true
            print("Media Link Null")
        }
        
        
        cell.usernameLabel.text = newsFeedList[indexPath.row].username
        
        if(newsFeedList[indexPath.row].profileImg != ""){
            
            let imageUrl = newsFeedList[indexPath.row].profileImg
            
            uiFun.loadImageFromURL(url: imageUrl) { (result, error) in
            
            if result != nil{
                if let imageDataFromURl = UIImage(data: result!){
                    cell.userProfileIV.image = imageDataFromURl
                    cell.userProfileIV.layer.cornerRadius = cell.userProfileIV.frame.size.width/2
                    cell.userProfileIV.clipsToBounds = true
                    return
                }
            }else {
                cell.userProfileIV.image = UIImage(named: "ic_indicator_default_user_active")
                print("Image Set Failed....Try Harder Mayur ")
                return
                }
            }
        }
        
        /********************* LIKE, REPLY, DELETE and SHARE Action Buttons ***********************/
        indexOfNewsFeed = indexPath.row
            
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(timelineImageViewTapped(sender:)))
        tapGesture.numberOfTapsRequired = 1
        cell.timelineIV.isUserInteractionEnabled = true
        cell.timelineIV.addGestureRecognizer(tapGesture)

        
        if(indexPath.row == newsFeedList.count-1){
        
            let newsFeedAPI = NewsFeedAPICall()
            newsFeedAPI.makeAPICall(fromViewController: self, firstPostOn: todayDate, lastPostOn: lastPostOn, sinceId: sinceId)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // When user clicks on the row
        newsFeedIndex = indexPath.row
        KCFABManager.defaultInstance().hide()
        print("Index Path of ImageView \(indexPath.row)")
        uiFun.navigateToScreem(identifierName: "NewsFeedDetailViewController", fromController: self)
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return true
    }
    /******************* LAYOUT OF CELL ***********************/
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension    // Set the height of each row depending on the content
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // Creating Cell Layout
        cell.contentView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.newsFeed_bg_color)
        let cellHeightFrame              = tableView.rectForRow(at: indexPath)
        let whiteRoundedView : UIView    = UIView(frame: CGRect(x: 0, y:8, width: Int(self.view.frame.width), height: Int(cellHeightFrame.size.height)))
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        // back button tap action
        showPlanner = true
        KCFABManager.defaultInstance().hide()
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
    func createFAB(){
        // Floating action button using third party library
        KCFABManager.defaultInstance().getButton().buttonColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        KCFABManager.defaultInstance().getButton().buttonImage = UIImage(named: "ic_indicator_compose_report")
       
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(composeButtonTapped))
        KCFABManager.defaultInstance().getButton().addGestureRecognizer(tapGesture)
        KCFABManager.defaultInstance().show()
    }
    
    func pullToRefresh(){
    
        newsFeedList.removeAll()        // Reload data
        
        let newsFeedAPI = NewsFeedAPICall()
        newsFeedAPI.makeAPICall(fromViewController: self, firstPostOn: "", lastPostOn: "", sinceId: "")
        
    }
    
    func composeButtonTapped(){
        // When user click on compose timeline where user can post
        KCFABManager.defaultInstance().hide()
        uiFun.navigateToScreem(identifierName: "ComposeViewController", fromController: self)
    }
    
    func btnCloseTapped(cell: NewsFeedTableViewCell){
        
        // Delete Post from the timeline
        let indexPath   = newsFeedTableView.indexPath(for: cell)
        print("Index Path of Delete Button is \(indexPath?.row)")
        
        uiFun.showIndicatorLoader()
        let updateId    = newsFeedList[(indexPath?.row)!].updateId
        let userId      = newsFeedList[(indexPath?.row)!].fromUser
        print("User and Update Id is \(updateId) & \(userId)")

        newsFeedList.remove(at: (indexPath?.row)!)
        newsFeedTableView.deleteRows(at: [indexPath!], with: .left)

        let deleteAPICall   = DeleteAPICall()
        deleteAPICall.makeAPICall(fromViewController: self, updateId: updateId, userId: userId)
    }
    
    func btnLikeTapped(cell: NewsFeedTableViewCell) {
       
        // When user taps on the like button
        let indexPath       = newsFeedTableView.indexPath(for: cell)
        likeIndex           = (indexPath?.row)!
        
        let updateId        = newsFeedList[(indexPath?.row)!].updateId
        
        let likeAPICall     = LikeUpdateAPICallViewController()
        likeAPICall.makeAPICall(fromViewController: self, updateId: updateId)
        
        let timelineLogin   = uiFun.readData(key: "TimelineLogin")
        
        // Check whether user is login or not for post anything on news feed
        if(timelineLogin == "Nope"){
            guestUserLogin()
        }
        
        print("Like Button Tapped")
    }
    
    func btnShareTapped(cell: NewsFeedTableViewCell) {
       
        // When user clicks on the share button
        let indexPath        = newsFeedTableView.indexPath(for: cell)
        let shareIndex       = (indexPath?.row)!
        print("Share BUtton Tapped")
       
        let textToShare     = newsFeedList[shareIndex].textUpdate
        
        let objectsToShare = [textToShare]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func timelineImageViewTapped(sender: UITapGestureRecognizer){
       
        let tapLocation = sender.location(in: newsFeedTableView)
        
        //using the tapLocation, we retrieve the corresponding indexPath
        let indexPath   = newsFeedTableView.indexPathForRow(at: tapLocation)
        print("Selected TableView Image IndexPath is \(indexPath?.row)")
        
        mediaLinkToLoad = newsFeedList[(indexPath?.row)!].mediaLink
        KCFABManager.defaultInstance().hide()
        uiFun.navigateToScreem(identifierName: "ImageViewController", fromController: self)
        
    }
    func guestUserLogin(){
        // If user is not login than will show alert box where user will enter his name
        var alertController:UIAlertController?
        alertController = UIAlertController(title: "User Detail",
                                            message: "You are required to create a guest name before posting a message",
                                            preferredStyle: .alert)
        
        alertController!.addTextField(
            configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Enter Username"
        })
        
        let action = UIAlertAction(title: "Ok",
                                   style: UIAlertActionStyle.default,
                                   handler: {[weak self]
                                    (paramAction:UIAlertAction!) in
                                    if let textFields = alertController?.textFields{
                                        let theTextFields = textFields as [UITextField]
                                        let enteredText = theTextFields[0].text
                                        // Storig data into parmanent storage
                                        self?.uiFun.writeData(value: enteredText!, key: "Username")
                                        self?.uiFun.writeData(value: "Done", key: "TimelineLogin")
                                        self?.composeButtonTapped()
                                        
                                    }
            })
        alertController?.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default,handler: nil))
        
        alertController?.addAction(action)
        self.present(alertController!,
                                   animated: true,
                                   completion: nil)
    }
    
    func deleteAPIResponse(response : JSON){
        // Deleted the post from the timeline
        uiFun.hideIndicatorLoader()
        if(Int(String(describing: response["code"])) == 200){
        
        }
    }
    
    func likeTimelinePostResponse(response : JSON){
        // When user click on like button api response
        if(Int(String(describing: response["code"])) == 200){
            let indexPath = NSIndexPath(row: likeIndex, section: 0) as NSIndexPath
            let cell      = newsFeedTableView.cellForRow(at: indexPath as IndexPath) as! NewsFeedTableViewCell
            let focusLikeImage = UIImage(named: "ic_indicator_feed_mark_useful_focus")
            cell.likeBT.setImage(focusLikeImage, for: .normal)
        }
    }
    
    func responseNewsFeed(response:JSON){
        
        // Response of news feed api & storing response into model
        uiFun.hideIndicatorLoader()
        if(Int(String(describing: response["code"])) == 200){
            // API response success
            let value = response["value"]["Event"]
            
            var i = 0
            while i < value.count{
            
                let newsFeedObj = NewsFeedModel()
                
                newsFeedObj.alertMessageSent    = String(describing: value.arrayValue[i]["AlertMessageSent"])
                newsFeedObj.sourceUpdateId      = String(describing: value.arrayValue[i]["SourceUpdateId"])
                newsFeedObj.modifiedDate        = String(describing: value.arrayValue[i]["ModifiedDate"])
                newsFeedObj.updateId            = String(describing: value.arrayValue[i]["UpdateId"])
                newsFeedObj.source              = String(describing: value.arrayValue[i]["Source"])
                newsFeedObj.location            = String(describing: value.arrayValue[i]["Location"])
                newsFeedObj.likesCount          = String(describing: value.arrayValue[i]["LikesCount"])
                newsFeedObj.textUpdate          = String(describing: value.arrayValue[i]["TextUpdate"])
                newsFeedObj.username            = String(describing: value.arrayValue[i]["Username"])
                newsFeedObj.updateType          = String(describing: value.arrayValue[i]["UpdateType"])
                newsFeedObj.postedOn            = String(describing: value.arrayValue[i]["PostedOn"])
                newsFeedObj.repliesCount        = String(describing: value.arrayValue[i]["RepliesCount"])
                newsFeedObj.fromUser            = String(describing: value.arrayValue[i]["FromUser"])
                newsFeedObj.profileImg          = String(describing: value.arrayValue[i]["ProfileImg"])
                newsFeedObj.isMyOwnPost         = value.arrayValue[i]["isMyOwnPost"].boolValue
                
                let mediaLinkArray  = value.arrayValue[i]["MediaLinks"]
                print("Timeline Media Link JSON \(mediaLinkArray)")
                newsFeedObj.mediaLink           = String(describing: mediaLinkArray[0])
                print("Timelilne Media Link URL \(newsFeedObj.mediaLink)")
                
                let likeArray = value.arrayValue[i]["Likes"]
                
                if(String(describing: likeArray) != "null"){
                
                    print("Like array \(likeArray)")
                    if(likeArray.count > 0){
                        
                        var i = 0
                        
                            while i < likeArray.count{
                                newsFeedObj.likeArrayFromUser = String(describing: likeArray.arrayValue[i]["FromUser"])
                                newsFeedObj.likeArrayUsername = String(describing: likeArray.arrayValue[i]["Username"])
                                newsFeedObj.likeArrayActivityTime = String(describing: likeArray.arrayValue[i]["ActivityTime"])
                                i += 1
                            }
                        }
                    }
                
                    newsFeedList.append(newsFeedObj)
                    i += 1
                
                }
            
                    newsFeedTableView.reloadData()
                    lastPostOn      = newsFeedList[lastPostIndex].postedOn
                    sinceId         = newsFeedList[lastPostIndex].updateId
                    firstSinceId    = newsFeedList[0].updateId              // first post id
                    firstPostTime   = newsFeedList[0].postedOn              // first post time
                    lastPostTime    = newsFeedList[9].postedOn              // last post time
                    lastSinceId     = newsFeedList[lastPostIndex].updateId  // last post id
                    refreshControl.endRefreshing()
        }else{
            
        }
        
    }
}
