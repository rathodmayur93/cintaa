//
//  BusTripListAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BusTripListAPI{
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TImeTableTimingViewController, agencyId : String, routeId : String, stopId : String, date:String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/trip/agencyid/\(agencyId)/routeid/\(routeId)/stopid/\(stopId)/date/\(date)/time/000000/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Fetch Stops URL \(url)")
        
        util.GetAPICall(apiUrl: url, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.FetchBusTripList(response: jsonResponse)
                print("Successfully Timetable BUS Trips fetched.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable BUS Trip fetching failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }
}
