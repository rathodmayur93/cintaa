//
//  CitrusSaveCardAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 09/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CitrusSaveCardAPI: UIViewController {
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    func makeAPICall(fromViewController : AddMoneyViewController, token : String, cvv : String, amount : String){
        
        let url = "\(constant.STAGING_BASE_URL)/payment/addMoney"
        print("Citrus save card payment gateway url \(url)")
        
        let jsonRequstBody = saveCardPaymentJSONString(token: token, cvv: cvv, amount: amount)
        
        util.APICall(apiUrl: url, jsonString: jsonRequstBody!, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.saveCardPaymentResponse(response: jsonResponse)
                print("Successfully Citrus Save Card Payment Gateway Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Citrus Save Card Payment Gateway Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    func saveCardPaymentJSONString(token : String, cvv : String, amount : String) -> String?{
        
        let para2:NSMutableDictionary = NSMutableDictionary()
        para2.setValue(token, forKey: "token")
        para2.setValue(cvv, forKey: "cvv")
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue("CITRUSPAY", forKey: "paymentGateway")
        para1.setValue(amount, forKey: "amount")
        para1.setValue("SAVED_CARD", forKey: "paymentMode")
        para1.setValue(para2, forKey: "details")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(para1, forKey: "paymentDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json array string ultimate = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

}
