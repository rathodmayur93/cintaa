//
//  CitrusPayemntWebViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 06/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import WebKit
import JavaScriptCore
import SwiftyJSON

var fromCitrusWebView           = false
var paymentResponseListWebview  = [PaymentReesponseModel]()
var addMoneyStatusOfTransaction = ""


class CitrusPayemntWebViewController: UIViewController, WKScriptMessageHandler, UIWebViewDelegate {

    var uiFun                                       = UiUtillity()
    var context                 : JSContext         = JSContext()
    
    @IBOutlet var webView1      : UIWebView!
    var webView123              : WKWebView?
    var userContentController   : WKUserContentController?
    //var bridge = SwiftJavascriptBridge.bridge()

    
    override func viewDidLoad() {
        super.viewDidLoad()
         print("Entered Citrus WebView")
        //webView.delegate = self
        let url = NSURL (string: redirectUrl!)
        let requestObj = NSURLRequest(url: url! as URL);
        //webView?.load(requestObj as URLRequest)
        webView1.delegate = self
        webView1.loadRequest(requestObj as URLRequest)
        clearCookies()
        
        uiFun.createCleverTapEvent(message: "CITRUS WEBVIEW SCREEN")
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage){
        print("Javascript Response \(message.body)")
        
        if(message.name == "callbackHandler") {
            print("JavaScript is sending a message \(message.body)")
        }
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        uiFun.showIndicatorLoader()
        if(request.mainDocumentURL?.query != nil){
            print("New Try Of Javascript \(request.description)")
        }
        return true
    }
    
    func parseResponse(data : Data){
        print("JSINterface response \(data)")
    }
    
    func postResponse(data : String){
        print("JSInterface Part 2 \(data)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
         uiFun.showIndicatorLoader()
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView){

        uiFun.hideIndicatorLoader()
        
        let paymentJSON = webView.stringByEvaluatingJavaScript(from: "getJsonResponse()")
        print("JSINterface response 123 \(paymentJSON)")
        
        if(paymentJSON != ""){
            let nativeJSON   = convertToDictionary(text: paymentJSON!)!
            
            let actualJSON   = JSON(nativeJSON)
            print("Status Of Yola \(String(describing : actualJSON["status"]))")
            processJSONResponse(response: nativeJSON)
            
            if(String(describing : nativeJSON["status"]!) == "OK"){
                if(isFromProfileWallet){
                    uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
                    return
                }
                    fromCitrusWebView = true
                    let agencyName              = uiFun.readData(key: "ticketingAgency")
                
                switch agencyName {
                case "Metro":
                    uiFun.navigateToScreem(identifierName: "MetroRechargeSuccessViewController", fromController: self)
                    break
                case "Best":
                    uiFun.navigateToScreem(identifierName: "BESTPassRechargeSuccessViewController", fromController: self)
                    break
                case "MetroToken":
                    uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
                    break
                case "metroTrip":
                    uiFun.navigateToScreem(identifierName: "TripPassReceiptViewController", fromController: self)
                    break
                case "Penalty":
                    uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
                    break
                case "STORE_VALUE_PASS":
                    uiFun.navigateToScreem(identifierName: "StoreValueReceiptViewController", fromController: self)
                    break
                default:
                    break
                }
              }
            }else{
                print("Failed")
            }
        
        if((String(describing: webView.request?.url).range(of: "SUCCESSFUL")) != nil){
            
            addMoneyStatusOfTransaction = "SUCCESSFUL"
            if(isFromProfileWallet){
                print("Add Money Crash Report 1")
                
                uiFun.navigateToScreem(identifierName: "AddMoneyStatusViewController", fromController: self)
            }else{
                print("Add Money Crash Report 1.1")
                fromCitrusWebView = true
                uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
            }
        }
        
        if((String(describing: webView.request?.url).range(of: "session_expired")) != nil){
            let alert = UIAlertController(title: "Error", message: "Oops..Look like session expired. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { action in
                print("Add Money Crash Report 2")
                
                if(isFromProfileWallet){
                    self.uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
                }else{
                    self.uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
                }
                
                fromCitrusWebView = false
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        if((String(describing: webView.request?.url).range(of: "failed")) != nil){
            
            addMoneyStatusOfTransaction      = "FAILED"
            let alert = UIAlertController(title: "Error", message: "Oops..Look like transaction failed. Please try again.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: { action in
                if(isFromProfileWallet){
                    self.uiFun.navigateToScreem(identifierName: "AddMoneyStatusViewController", fromController: self)
                }else{
                    self.uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
                }
                fromCitrusWebView = false
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
        if((String(describing: webView.request?.url).range(of: "return")) != nil){
            
            let agencyName = uiFun.readData(key: "ticketingAgency")
            
            if(isFromProfileWallet){
                print("Add Money Crash Report 3")
                uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
            }
            if(agencyName == "Metro"){
                print("Add Money Crash Report 3.1")
                uiFun.navigateToScreem(identifierName: "MetroRechargeSuccessViewController", fromController: self)
            }else if(agencyName == "Best"){
                print("Add Money Crash Report 3.2")
               uiFun.navigateToScreem(identifierName: "BESTPassRechargeSuccessViewController", fromController: self)
            }
        }
    }
    
    
    func processJSONResponse(response : [String : Any]){
    
        let metroTokenResponse  = MetroJSONResponses()
        metroTokenResponse.processMetroJSONResponse(response: JSON(response))
        //uiFun.navigateToScreem(identifierName: "TokenQRCodeViewController", fromController: self)
        
        paymentResponseListWebview.removeAll()
        print("JSON Response Test \(response)")
        let jsonResponse        = JSON(response)
        
        if(jsonResponse["code"] != 0){
        
            print("JSON RESPONSE ADDING INTO LIST")
            var paymentDetailJSON           = jsonResponse["data"]
            
            if(paymentDetailJSON["bookingDetails"].exists()){
            
                let paymentResponse             = PaymentReesponseModel()
            
                let paymentData                 = PaymentDataWebView()
                
                paymentData.isPaymentFinal      = String(describing : paymentDetailJSON["isPaymentFinal"])
                paymentData.isPGFlow            = String(describing : paymentDetailJSON["isPGFlow"])
                paymentData.order_no            = String(describing : paymentDetailJSON["order_no"])
            
                let paymentJSON                 = paymentDetailJSON["paymentDetails"]
                let paymentDetails              = PaymentDetailsResponseWebView()
            
                paymentDetails.paymentGateway   = String(describing : paymentJSON["paymentGateway"])
                paymentDetails.paymentMode      = String(describing : paymentJSON["paymentMode"])
                paymentDetails.responseType     = String(describing : paymentJSON["responseType"])
                paymentDetails.message          = String(describing : paymentJSON["message"])
                paymentDetails.paymentBookingId = String(describing : paymentJSON["bookingId"])
                paymentDetails.pgTransactionId  = String(describing : paymentJSON["pgTransactionId"])
                paymentDetails.status           = String(describing : paymentJSON["status"])
            
                let bookingPaymentDetailJSON    = paymentDetailJSON["bookingDetails"].arrayValue[0]
            
                paymentDetails.cardNumber       = String(describing : bookingPaymentDetailJSON["bookingItemsList"].arrayValue[0]["companyItemId"])
            
                if(jsonResponse["code"] == 5000){
                    if(bookingPaymentDetailJSON["bookingPaymentDetailsList"].count == 1){
                        paymentDetails.paymentMethod    = String(describing : bookingPaymentDetailJSON["bookingPaymentDetailsList"].arrayValue[0]["paymentMethod"])
                    }else{
                        paymentDetails.paymentMethod    = "\(String(describing : bookingPaymentDetailJSON["bookingPaymentDetailsList"].arrayValue[0]["paymentMethod"])), \(String(describing :   bookingPaymentDetailJSON["bookingPaymentDetailsList"].arrayValue[1]["paymentGateway"])) "
                    }
                }
                paymentDetails.bookingId        = String(describing : bookingPaymentDetailJSON["bookingId"])
                paymentDetails.amount           = String(describing : bookingPaymentDetailJSON["totalAmount"])
                paymentResponse.status          = String(describing : jsonResponse["status"])
                paymentResponse.code            = String(describing : jsonResponse["code"])

            
                paymentData.paymentDetails.append(paymentDetails)
                paymentResponse.data.append(paymentData)
            
                paymentResponseListWebview.append(paymentResponse)
            }else if(jsonResponse["code"] == 5000){
                
                if(isFromProfileWallet){
                    print("Add Money Crash Report 3")
                    uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
                }else{
                    
                }
            }
            //print("Booking id webview \(String(describing : bookingResponse.arrayValue[0]["bookingId"]))")
        }
       // paymentDetailsResponse.amount = paymentDetailsResponse["amount"]
    }
    
    func clearCookies(){

        let cookieJar = HTTPCookieStorage.shared
        
        for cookie in cookieJar.cookies! {
            // print(cookie.name+"="+cookie.value)
            cookieJar.deleteCookie(cookie)
        }
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: AnyObject) {
        if(isFromProfileWallet){
            isFromProfileWallet = false
            uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }
    }
}
