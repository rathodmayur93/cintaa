//
//  DriveTableViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 07/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class DriveTableViewCell: UITableViewCell {

    @IBOutlet var startingTextLabel: UILabel!
    @IBOutlet var trafficStatus: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var routeNameLabel: UILabel!
    @IBOutlet var averageSpeedLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
