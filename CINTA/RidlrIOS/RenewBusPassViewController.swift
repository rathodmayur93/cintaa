//
//  RenewBusPassViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 04/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var busMetroPassNumber              = ""
var bestPassResponseListCurrent     = [BESTModel]()
var bestPassResponseListAction      = [BESTModel]()
var passDurationType                    = ""

class RenewBusPassViewController: UIViewController {
    
    @IBOutlet var passNumberTF          : UITextField!
    @IBOutlet var submitTapped          : UIButton!
    @IBOutlet var demoPassIV            : UIImageView!
    
    
    let uiFun                                               = UiUtillity()
    let ridlrColors                                         = RidlrColors()
    
    let lastPassStatus                  : UITextView        = UITextView()
    let lastPassNumberLabel             : UILabel           = UILabel()
    let lastPassMonthlyRechargeButton   : UIButton          = UIButton()
    let lastPassQuterlyRechargeButton   : UIButton          = UIButton()
    let quickRenewView                  : UIView            = UIView()
    let quickRenewLabel                 : UILabel           = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("/***** ENTER BUS PASS NUMBER SCREEN *******/")
        setUI()
        uiFun.createCleverTapEvent(message: "ENTER BUS PASS NUMBER SCREEN")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
        quickRenewalOfPass()
        passNumberTF.keyboardType       = UIKeyboardType.numberPad
        submitTapped.backgroundColor    = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
        submitTapped.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.ridlrTextColor), for: .normal)
        // When user enters the pass number "checkPassNumber" this method gets executed
        passNumberTF.addTarget(self, action: #selector(RenewBusPassViewController.checkPassNumber), for: UIControlEvents.editingChanged)
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    func quickRenewalOfPass(){
        
        // This method shows last recharged card for quick renewal of pass
        demoPassIV.isHidden = true
        
        
        if UserDefaults.standard.object(forKey: "lastBestPassNumber") != nil{
            // last pass recharge pass number exist
            
            let lastPassNumber = uiFun.readData(key: "lastBestPassNumber") // Fetching last recharged pass number
            print("Last Pass Number is \(lastPassNumber)")
            
            if(lastPassNumber != ""){
                
                // API call to check the status of pass  whether it's active and expiry date
                let bestCardStatus = CheckBESTCardStatusAPI()
                bestCardStatus.makeAPICall(fromViewController: self, passNumber: lastPassNumber, from: "lastRecharge")
                
                // Creating ""Last Pass Status" Label and giving it's properties
                quickRenewLabel.frame       = CGRect(x: 16, y: 300, width: Int(self.view.frame.width - 32), height: 18)
                quickRenewLabel.text        = "Last Pass Status"
                quickRenewLabel.textColor   = UIColor.black
                quickRenewLabel.font        = UIFont.boldSystemFont(ofSize: 14.0)
                self.view.addSubview(quickRenewLabel)
                
                // Creating quickRenewView frame which contains the pass number and its status
                quickRenewView.frame            = CGRect(x: 16, y: 324, width: self.view.frame.width - 32, height: 90)
                quickRenewView.backgroundColor  = UIColor.white
                
                //Creating Last recharged pass number label
                lastPassNumberLabel.frame           = CGRect(x: 16, y: 8, width: Int(self.view.frame.width - 32), height: 18)
                lastPassNumberLabel.text            = "Pass Number : \(lastPassNumber)"
                lastPassNumberLabel.textColor       = UIColor.black
                lastPassNumberLabel.font            = UIFont.boldSystemFont(ofSize: 14.0)
                quickRenewView.addSubview(lastPassNumberLabel)

                //Creating Last recharge pass status label
                lastPassStatus.frame        = CGRect(x: 0, y: 34, width: Int(self.view.frame.width - 32), height: 40)
                lastPassStatus.text         = ""
                lastPassStatus.textColor    = UIColor.black
                lastPassStatus.font         = UIFont.systemFont(ofSize: 12)
                quickRenewView.addSubview(lastPassStatus)
                
                self.view.addSubview(quickRenewView)
                
            }
        }else{
            // If last recharged pass number doesnt exist will hide whole UI of Last Recharged Pass
            quickRenewView.isHidden = true
            quickRenewLabel.isHidden = true
            demoPassIV.isHidden = false

        }
        
    }
    
    @IBAction func submitButtonAction(_ sender: AnyObject) {
        
        view.endEditing(true) // hide the keyboard when user clicks on the submit button
        uiFun.createCleverTapEvent(message: "Pass Recharge Validation Pass Number \(passNumberTF.text!)")
        uiFun.createCleverTapEvent(message: "Pass Recharge Validation Page main flow")
        
        // Check for pass number whether user have entered the valid pass number or not
        if(passNumberTF.text == "" || passNumberTF.text?.characters.count != 14){
    
            // Show Alert
            let alertController = UIAlertController(title: "Error", message:
                "Please Enter 14 Digit Pass Number", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            print("Bus Pass Number Validation Error")
        }else{
        
            // Make api call to check whether entered pass number is active or expired
            uiFun.showIndicatorLoader()
            let bestCardStatus = CheckBESTCardStatusAPI()
            bestCardStatus.makeAPICall(fromViewController: self, passNumber: passNumberTF.text!, from: "action")
        }
    }
    
    func checkPassNumber(){
        // When user enters the pass number, after entering every single character this method gets invoked to check following conditions
        if((passNumberTF.text?.characters.count)! > 14){
            passNumberTF.deleteBackward() // If user enters the 14 characters pass number after that he cant enter any character
        }
        if(passNumberTF.text?.characters.count == 14){
            
            // After entering 14 charchter pass number submit button background color changes to blue from gray
            submitTapped.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
            submitTapped.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor), for: .normal)
            
            let bestCardStatus = CheckBESTCardStatusAPI() // Check whether pass number is valid or not
            bestCardStatus.makeAPICall(fromViewController: self, passNumber: passNumberTF.text!, from: "normal")
            
        }else{
            submitTapped.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.lighGreyColor)
            
        }
        
    }
    
    func BESTCardStatusResponse(response : JSON){
    
        // Response from quick renewal of pass
        print("BEST RESPONSE \(response)")
        
        uiFun.hideIndicatorLoader()
        let jsonError = response["error"] // fetching error from response
        print("BEST Error \(jsonError)")
        
        if(String(describing : jsonError["errorCodeText"]) != "null"){
            // Show error in alert
            uiFun.showAlert(title: "", message: String(describing : jsonError["errorCodeText"]), logMessage: "BEST Pass Error", fromController: self)
        }else if(String(describing : response["code"]) != "5000"){
            // If errorCodeText is not there just parse the respones to showExceptionDialog
            uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
        }
    }
    
    func buttonClickResponse(response : JSON){
        
        // response from submit button
        print("BEST RESPONSE \(response)")
        let jsonError = response["error"]
        print("BEST Error \(jsonError)")
        uiFun.hideIndicatorLoader()
        
        if(String(describing : jsonError["errorCodeText"]) != "null"){
            // Error in response
            uiFun.createCleverTapEvent(message: "Pass Recharge Validation failed \(String(describing : jsonError["errorCodeText"]))")
            
           
            let error = String(describing : jsonError["errorCodeText"])
            let displayError = error.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            uiFun.showAlert(title: "Notification", message: displayError, logMessage: "BEST Enter Pass Number error", fromController: self)
        }else if(String(describing : response["code"]) == "5000"){
        
            // Success in response
            uiFun.createCleverTapEvent(message: "Pass Recharge Validation Success")
            processBESTResponse(jsonResponse: response) // Store response into respective model
            let defaults        = UserDefaults.standard
            // Set pass number in last recharged pass number
            defaults.setValue(String(describing: passNumberTF.text), forKey: "bestPassNumber")
            
            busMetroPassNumber  = String(describing: passNumberTF.text) // Assigning pass number to global variable
            uiFun.navigateToScreem(identifierName: "RenewBusPassNewViewController", fromController: self)
        }else{
            
            uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
        }
    }
    
    func lastPassRechargeResponse(response : JSON){
    
        // Response from quick renewal of pass

        print("Last Pass Recharge Response \(response)")
        let jsonError = response["error"] // Fetching error from response
        print("BEST Error \(jsonError)")
        uiFun.hideIndicatorLoader()
        
        if(String(describing : jsonError["errorCodeText"]) != "null"){
            // Error in response show this in alert box
            let error = String(describing : jsonError["errorCodeText"]) // Fetching error text from response
            let displayError = error.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            lastPassStatus.text = displayError
            lastPassMonthlyRechargeButton.isHidden = true
            lastPassQuterlyRechargeButton.isHidden = true
        
        }else if(String(describing : response["code"]) == "5000"){
            // Success in response hide the quick renew view and show the demo pass image
            quickRenewView.isHidden     = true
            quickRenewLabel.isHidden    = true
            demoPassIV.isHidden         = false
            
        }else{
            // If there is no error code text parse response to "showExceptionDialog" method
            uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
        }
    }
    
    func processBESTResponse(jsonResponse : JSON){
    
        // Storing api response to respective model
        bestPassResponseListCurrent.removeAll() // Remove the previous values from the List
        bestPassResponseListAction.removeAll()
        uiFun.hideIndicatorLoader()
        
        let bestData            = BestData()    // Model
        let bestModel           = BESTModel()   // Model
        bestModel.status        = String(describing: jsonResponse["status"]) // Fetching status of response
        bestModel.code          = String(describing: jsonResponse["code"])   // Fetching status code of response
        
        let jsonData            = jsonResponse["data"]          // Fetching data from the api response
        
        let currentDetail       = jsonData["currentDetails"]    //  Response Json array 1
        let actionDetails       = jsonData["actionDetails"]     //  Response Json array 2
        
        let currentDetailItem   = currentDetail["items"]
        let actionDetailItem    = actionDetails["items"]
        
        var detailItem          = JSON("")
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        
        uiFun.createCleverTapEvent(message: "Pass Recharge Validation Recharge attempt date \(result)")
        
        var a                   = 0
        while(a < jsonData.count){
            print("JSon data Item count \(jsonData.count)")
            var i = 0
            if(a == 0){
                detailItem = currentDetailItem //  Current pass detail is stored in detail item
            }else if(a == 1){
                detailItem = actionDetailItem  // Monthly and Quarterly Pass Data is stored in detail list
            }
        
            while(i < detailItem.count){
                print("Detail Item count \(detailItem.count)")
                let items           = Items()           // Model
                let bestUser        = BestUser()        // Model
                let bestPriceDetail = PricingDetails()  // Model
                let bestSurcharge   = Surcharge()       // Model
                let routeDetail     = RouteDetails()    // Model
                
                var surcharge       = JSON("")
                var userDetail      = detailItem.arrayValue[i]["user"]
                var pricingDetail   = detailItem.arrayValue[i]["pricingDetails"]
                var routeDetails    = detailItem.arrayValue[i]["routeDetails"]
                
                if(a == 0){
                     surcharge      = pricingDetail["surcharge"]
                }else if(a == 1){
                
                    surcharge       = pricingDetail["surcharge"].arrayValue[0]
                }
                // Storing values into respective model variables
                
                items.itemType              =       String(describing: detailItem.arrayValue[i]["itemType"])
                items.issuerIdentifier      =       String(describing: detailItem.arrayValue[i]["issuerIdentifier"])
                items.activeRenewalDate     =       String(describing: detailItem.arrayValue[i]["activeRenewalDate"])
                items.itemSubType           =       String(describing: detailItem.arrayValue[i]["itemSubType"])
                items.txnRefNo              =       String(describing: detailItem.arrayValue[i]["txnRefNo"])
                items.discount              =       String(describing: detailItem.arrayValue[i]["discount"])
                items.activationDate        =       String(describing: detailItem.arrayValue[i]["activationDate"])
                items.activationCriteria    =       String(describing: detailItem.arrayValue[i]["activationCriteria"])
                items.passCategory          =       String(describing: detailItem.arrayValue[i]["passCategory"])
                items.concession            =       String(describing: detailItem.arrayValue[i]["concession"])
                items.companyIdentifier     =       String(describing: detailItem.arrayValue[i]["companyIdentifier"])
                items.itemAction            =       String(describing: detailItem.arrayValue[i]["itemAction"])
                items.itemAmount            =       String(describing: detailItem.arrayValue[i]["itemAmount"])
                items.validity              =       String(describing: detailItem.arrayValue[i]["validity"])
                items.afterMigration        =       String(describing: detailItem.arrayValue[i]["afterMigration"])
                items.expiryDate            =       String(describing: detailItem.arrayValue[i]["expiryDate"])
                items.issuerTag             =       String(describing: detailItem.arrayValue[i]["issuerTag"])
                items.routeDetailText       =       String(describing: detailItem.arrayValue[i]["routeDetails"])
                bestUser.dob                =       String(describing: userDetail["dob"])
                bestUser.userImage          =       String(describing: userDetail["userImage"])
                bestUser.gender             =       String(describing: userDetail["gender"])
                bestUser.name               =       String(describing: userDetail["name"])
                bestUser.type               =       String(describing: userDetail["type"])
                
                if(String(describing: detailItem.arrayValue[i]["routeDetails"]) != "null"){
                    routeDetail.destinationId   = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["destinationId"]))!
                    routeDetail.routeNo         = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["routeNo"]))!
                    routeDetail.destinationName = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["destinationName"]))!
                    routeDetail.nextRoute       = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["nextRoute"]))!
                    routeDetail.routeId         = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["routeId"]))!
                    routeDetail.sourceStageName = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["sourceStageName"]))!
                    routeDetail.sourceAreaId    = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["sourceAreaId"]))!
                    routeDetail.sourceName      = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["sourceName"]))!
                    routeDetail.prevRoute       = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["prevRoute"]))!
                    routeDetail.sourceId        = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["sourceId"]))!
                    routeDetail.destinationStageName = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["destinationStageName"]))!
                    routeDetail.routeName       = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["routeName"]))!
                    routeDetail.destinationStageId      = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["destinationStageId"]))!
                    routeDetail.destinationAreaId       = uiFun.nullToNil(value: String(describing: routeDetails.arrayValue[0]["destinationAreaId"]))!
                }
                
                bestPriceDetail.discount    =       String(describing: pricingDetail["discount"])
                bestPriceDetail.concession  =       String(describing: pricingDetail["concession"])
                bestPriceDetail.totalAmount =       String(describing: pricingDetail["totalAmount"]) // Rs 100 is for testing purpose revert this on production
                
                bestPriceDetail.baseAmount  =       String(describing: pricingDetail["baseAmount"])
                bestPriceDetail.tax         =       String(describing: pricingDetail["tax"])

                bestSurcharge.amount        =       String(describing: surcharge["amount"])
                bestSurcharge.code          =       String(describing: surcharge["code"])
                bestSurcharge.appliedBy     =       String(describing: surcharge["appliedBy"])
                bestSurcharge.applyLevel    =       String(describing: surcharge["applyLevel"])
                bestSurcharge.applyValue    =       String(describing: surcharge["applyValue"])
                bestSurcharge.applyType     =       String(describing: surcharge["applyType"])
                bestSurcharge.name          =       String(describing: surcharge["name"])
                
                bestPriceDetail.surcharge.append(bestSurcharge)
                items.pricingDetails.append(bestPriceDetail)
                items.user.append(bestUser)
                items.routeDetails.append(routeDetail)
                
                uiFun.createCleverTapEvent(message: "Pass Recharge Validation Expiry Date \(String(describing: detailItem.arrayValue[0]["expiryDate"]))")
                
                if(a == 0){
                    bestData.currentDetails.append(items)
                    bestModel.bestData.append(bestData)
                    bestPassResponseListCurrent.append(bestModel) // Adding model values into the bestPassResponseListCurrent list
                }else if(a == 1){
                
                    bestData.actionDetails.append(items)
                    bestModel.bestData.append(bestData)
                    bestPassResponseListAction.append(bestModel) //// Adding model values into the bestPassResponseListAction list
                    
                    if(detailItem.count == 2){
                        passDurationType = "Q"
                        print("Pass Duration Is Quarterly")
                    }else if(detailItem.count == 1){
                        passDurationType = "M"
                        print("Pass Duration Is Monthly")
                    }
                }
                i += 1
            }
            a += 1
        }
    }
}
