//
//  BusStopFromAtoBModel.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BusStopFromAtoBModel{
    
    var _index  = ""
    var _type   = ""
    var _id     = ""
    var _score  = ""
    
    var source = [SourceBusStops]()
}

class SourceBusStops{

    var l_agency_id         = ""
    var l_custom_stop_id    = ""
    var l_stop_id           = ""
    var l_stop_name         = ""
    var l_stop_lat          = ""
    var l_stop_lon          = ""
    var l_stop_code         = ""
    var l_stop_area         = ""
    var l_stop_name_index   = ""
    
}
