//
//  AddWalletViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 18/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var addingWhichWallet = ""
class AddWalletViewController: UIViewController {

    @IBOutlet var emailTF       : UITextField!
    @IBOutlet var mobileTF      : UITextField!
    
    @IBOutlet var walletLogo    : UIImageView!
    @IBOutlet var walletLabel   : UILabel!
    
    @IBOutlet var proceedBT     : UIButton!
    let constant                = Constants()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
    
        switch addingWhichWallet {
        case "CITRUSPAY":
            walletLogo.image = UIImage(named: "ic_indicator_citrus")
            walletLabel.text = "Citrus Cash"
        case "MOBIKWIK":
            walletLogo.image = UIImage(named: "ic_indicator_mobikwik")
            walletLabel.text = "Mobikwik Wallet"
        case "FREECHARGE":
            walletLogo.image = UIImage(named: "ic_indicator_freecharge")
            walletLabel.text = "Freecharge Wallet"
        default:
            break
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        if(isFromProfileWallet){
            uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }
    }
    
    @IBAction func proceedBTAction(_ sender: Any) {
        
        if(isValidEmail(testStr: emailTF.text!)){
            // Check for validation of mobile number
            if(validateMobile(value: mobileTF.text!)){
                
                otpWallet = addingWhichWallet
                authMethodAPICall()
                
            }else{
                uiFun.showAlert(title: "Notification", message: "Invalid Mobile Number", logMessage: "Add Wallet() Invalid Mobile Number", fromController: self)
            }
        }else{
            uiFun.showAlert(title: "Notification", message: "Invalid Email Address", logMessage: "Add Wallet() Invalid Email Address", fromController: self)
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // Validating Email Address
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validateMobile(value: String) -> Bool {
        // Validating mobile number
        if(value.characters.count != 10){
            return false
        }else{
            return true
        }
    }
    
    func authMethodAPICall(){
        
        // Profile creation without any wallet otp will be received from Respective Wallet
        profileType = "wallet"
        uiFun.showIndicatorLoader()
        
        do {
            // Creating json string for wallet profile creation
            let jsonString = checkEmailAndNumber(email: String(describing: emailTF.text!), mobile: String(describing: mobileTF.text!), payment: addingWhichWallet)
            print("JOSN BODY REQUEST \(jsonString)")
            
            // create post request
            let url = NSURL(string: "\(constant.STAGING_BASE_URL)/payment/auth_methods")! // API Call For Reqeusting OTP
            print("Auth Method URL Payment \(url)")
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonString?.data(using: String.Encoding.utf8, allowLossyConversion: true)
            print("BODY REQUEST" + String(describing: request.httpBody))
            
            
            let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                if error != nil{
                    print("Certificate Error -> \(error!)")
                    uiFun.hideIndicatorLoader()
                    if(error.debugDescription.contains("timed out")){
                        uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                    }else if(error.debugDescription.contains("connection was lost")){
                        uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                    }else if(error.debugDescription.contains("appears to be offline")){
                        uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                    }
                    
                    return
                }else{
                    
                    if let urlContent = data{
                        
                        do {
                            
                            let jsonResult      = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                            
                            let jsonResponse    = JSON(jsonResult)
                            print("JSON RESPONSE \(jsonResponse)")
                            
                            if(JSON(jsonResult["status"]!) == "OK"){
                                
                                uiFun.hideIndicatorLoader()
                                
                                print("JSON OBJECT VALUE \(jsonResult["data"]!!)")
                                
                                let jsonData = JSON(jsonResult["data"]!!)
                                
                                // Setting data into the parmanent storage
                                let defaults = UserDefaults.standard
                                defaults.set(String(describing: jsonData["pgPreAuthData"]), forKey: "pgPreAuthData")
                                defaults.set(String(describing: self.emailTF.text!), forKey: "userEmailId")
                                defaults.set(String(describing: self.mobileTF.text!), forKey: "userMobileNo")
                                
                                
                                print("Jugaad of  mobile \(jsonData["mobile"])")
                                
                                if let description1 = ((jsonResult["data"] as? NSArray)?[0] as? NSDictionary)?["mobile"] as? String{
                                    print("JSON Response Mobile \(description1)")
                                }
                                
                                /**************** NAVIGATE TO OTP SCREEN**************/
                                
                                uiFun.createCleverTapEvent(message: "Authentication method Citrus:OTP")
                                uiFun.navigateToScreem(identifierName: "ProfileOTPViewController", fromController: self)
                                
                                if(JSON(jsonResult["status"]!) == "OK"){
                                    print("authMethodAPICall Success")
                                }
                            }else {
                                
                                let jsonError = JSON(jsonResult["error"]!!)
                                uiFun.hideIndicatorLoader()
                                
                                
                                if(String(describing: jsonError["errorCodeText"]) == "" || String(describing: jsonError["errorCodeText"]) == "null"){
                                    // Fetching errorCodeText and if its null parse response to "showExceptionDialog" method
                                    uiFun.showExceptionDialog(jsonResponse: jsonResponse, fromViewController: self)
                                }else{
                                    // Show erroCodeText into alert box
                                   uiFun.showAlert(title: "Error", message: String(describing: jsonError["errorCodeText"]), logMessage: "authMethodAPICall()", fromController: self)
                                }
                            }
                            
                        }
                        catch{
                            // Failed To Convert JSON
                            uiFun.hideIndicatorLoader()
                            print("Failed To Convert JSON")
                            uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "authMethodAPICall()", fromController: self)
                        }
                        
                    }
                }
            }
            
            task.resume()
            
        }
        
    }
    
    func checkEmailAndNumber(email: String, mobile: String, payment:String) -> String?{
        // Creating json string for wallet otp request
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(email, forKey: "walletEmail")
        para.setValue(mobile, forKey: "walletMobile")
        para.setValue(payment, forKey: "paymentGateway")
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
}
