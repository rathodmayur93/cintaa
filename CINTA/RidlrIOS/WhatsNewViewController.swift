//
//  WhatsNewViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 01/06/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class WhatsNewViewController: UIViewController {
   
    @IBOutlet var letGoAction: UIButton!
    var delegate: WhatsNewDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func letsGoButtonAction(_ sender: Any) {
      //  ViewController.hideContainerView()
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callForAlert"), object: nil)
    }
    @IBAction func closeButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callForAlert"), object: nil)
    }
    
}

protocol WhatsNewDelegate {
    func buttonPressed(sender : AnyObject)
}
