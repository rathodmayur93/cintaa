//
//  MetroStoredValueViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 15/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import GMStepper

class MetroStoredValueViewController: UIViewController {

    @IBOutlet var stepper           : GMStepper!
    @IBOutlet var registrationFee   : UILabel!
    @IBOutlet var passPriceLabel    : UILabel!
    @IBOutlet var totalAmountLabel  : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        stepper.labelFont = UIFont(name: "AvenirNext-Bold", size: 18.0)!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func stepperAction(_ sender: Any) {
        
        passPriceLabel.text     = "₹ \(stepper.value)"
        totalAmountLabel.text   = "₹ \(stepper.value)"
    }
    
    @IBAction func confirmDetails(_ sender: Any) {
        
        let profileStatus       = uiFun.readData(key: "ProfileCreation")
        rechargeAmount          = "\(Int(stepper.value))"
        uiFun.writeData(value: "STORE_VALUE_PASS", key: "ticketingAgency")
        
        if(profileStatus == "done"){
            uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
        }else{
            uiFun.createCleverTapEvent(message: "Profile creation touchpoints Metro Store Value Pass")
            profileBackNavigation = "MetroHomeScreenViewController"
            uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    
}
