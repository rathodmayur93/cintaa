//
//  BookingsViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 20/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var bookingList         = [BookingModelViewController]()
var showBookingReceipt  = false
var receiptIndex        = 0
class BookingsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var bookingTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FetchBookingDetailAPICall(page: "1")
        bookingList.removeAll()
        uiFun.createCleverTapEvent(message: "BOOKING HISTORY LIST VIEW SCREEN")
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // bookingList.count is the number of rows in table
        return bookingList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell                        = bookingTableView.dequeueReusableCell(withIdentifier: "Cell") as! BookingTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.blue
        
        cell.bookingAmount.text         = "₹ \(bookingList[indexPath.row].totalAmount)"         // Recharge Amount
        cell.bookingCardNumber.text     = "Card Id: \(bookingList[indexPath.row].companyItemId)" // Card number
        
        // booking type is identified from the company
        if(bookingList[indexPath.row].company == "RELIANCE_METRO"){
            cell.bookingType.text       = "Metro Card"
            
        }else{
            cell.bookingType.text       =  "BEST \(bookingList[indexPath.row].itemType)"
        }
        
        let epocTime        = TimeInterval(Double(bookingList[indexPath.row].bookingDateTime)!) / 1000
        let date            = NSDate(timeIntervalSince1970: epocTime)
        print("Converted TImestamp \(String(describing: date))")
        
        // converting epoc time into "yyyy-MM-dd HH:mm" date string
        let dayTimePeriodFormatter              = DateFormatter()
        dayTimePeriodFormatter.timeZone         = NSTimeZone.local
        dayTimePeriodFormatter.dateFormat       = "yyyy-MM-dd HH:mm"
        let dateString                          = dayTimePeriodFormatter.string(from: date as Date)
        cell.bookingTime.text                   = dateString
        
        // Fetching booking status
        if(bookingList[indexPath.row].companyStatus == "SUCCESS"){
            
            cell.bookingStatus.textColor         = uiFun.hexStringToUIColor(hex: ridlrColors.success_color)
            cell.bookingStatus.text              = "SUCCESS"
        }else{
            cell.bookingStatus.text              = "FAIL"
            cell.bookingStatus.textColor         = uiFun.hexStringToUIColor(hex: ridlrColors.error_color)
        }
        
        uiFun.hideIndicatorLoader()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        showBookingReceipt = true
        receiptIndex       = indexPath.row
        
        // Table view row Click or Tap Fuctino
        if(bookingList[indexPath.row].company == "RELIANCE_METRO"){
            // if company is "RELIANCE_METRO" will navigate to "MetroRechargeSuccessViewController"
            uiFun.navigateToScreem(identifierName: "MetroRechargeSuccessViewController", fromController: self)
        }
        else{
            // if company is "RELIANCE_METRO" will navigate to "BESTPassRechargeSuccessViewController"
            uiFun.navigateToScreem(identifierName: "BESTPassRechargeSuccessViewController", fromController: self)
        }
    }
    
    func FetchBookingDetailAPICall(page : String){
    
        uiFun.showIndicatorLoader()
        // Fetching booking history api call
        let constant = Constants()
        let util     = Utillity()
        
        let url = "\(constant.STAGING_BASE_URL)/booking"    // API Url
        print("News Feed url \(url)")
        let params : [String:AnyObject] = ["page":page as AnyObject,
                                           "size":"20" as AnyObject]
        util.sendRequest(url: url, parameters:params , completionHandler: { (result, error) in
            if let jsonResponse = result{
                
                uiFun.hideIndicatorLoader()
                print("Successfully All Booking Response Got.... \(jsonResponse) ")
                /****************** API RESPONSE *******************/
                
                if(String(describing:jsonResponse["code"]) == "5000"){
                    // API response Success
                    uiFun.hideIndicatorLoader()
                     self.processJSONResult(response: jsonResponse)
                }else{
                    // If there is no errorCodeText parse json respose to "showExceptionDialog" method
                    uiFun.hideIndicatorLoader()
                    uiFun.showExceptionDialog(jsonResponse: jsonResponse, fromViewController: self)
                }
                return
            }
        })
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        // back button tap action
        uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
    }
    
    func processJSONResult(response : JSON){
        // Store booking history json response into the model
        
        uiFun.hideIndicatorLoader()
        var i    = 0
        let data = response["data"]
        
        while(i < data.count){
        
                let bookingItemsList            = data.arrayValue[i]["bookingItemsList"]
            
            if(bookingItemsList.count != 0 ){
                if(String(describing: bookingItemsList.arrayValue[0]["itemType"]) == "PASS" || String(describing: data.arrayValue[i]["company"]) == "RELIANCE_METRO"){
                    let bookingModel = BookingModelViewController()
            
                    bookingModel.bookingId          = String(describing: data.arrayValue[i]["bookingId"])
                    bookingModel.companyStatus      = String(describing: data.arrayValue[i]["companyStatus"])
                    bookingModel.bookingDateTime    = String(describing: data.arrayValue[i]["bookingDateTime"])
                    bookingModel.totalAmount        = String(describing: data.arrayValue[i]["totalAmount"])
                    bookingModel.company            = String(describing: data.arrayValue[i]["company"])
                    
                    bookingModel.companyItemId      = String(describing: bookingItemsList.arrayValue[0]["companyItemId"])
                    bookingModel.itemType           = String(describing: bookingItemsList.arrayValue[0]["itemType"])
                    bookingModel.itemSubtype        = String(describing: bookingItemsList.arrayValue[0]["itemSubType"])
                    
                    let bookingPaymentDetailsListJSON = data.arrayValue[i]["bookingPaymentDetailsList"]
                    bookingModel.timestamp            = String(describing: data.arrayValue[i]["bookingDateTime"])
                
                    if(bookingPaymentDetailsListJSON.count > 0){
                        bookingModel.paidVia              = String(describing : bookingPaymentDetailsListJSON.arrayValue[0]["paymentMethod"])
                        print("Payment Method Booking \(String(describing : bookingPaymentDetailsListJSON.arrayValue[0]["paymentMethod"]))")
                    }else{
                        bookingModel.paidVia              = ""
                    }
                
                    print("Booking Payment Array Count \(bookingPaymentDetailsListJSON.count)")
                    print("Booking Payment Details \(bookingPaymentDetailsListJSON)")
                
                    bookingList.append(bookingModel)
                }
            }
                i += 1
            
        }
        bookingTableView.reloadData()       // Reload table view so new data will reflect in cell
        uiFun.hideIndicatorLoader()
    }
}
