//
//  MonoTripParser.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var monoTripsDataList = [MonoTripModel]()
class MonoTripParser {

    let uiFun = UiUtillity()
    
    func parseMonoTripsCSVToList(){
        
        let queueName = DispatchQueue(label: "trip.mono.processing")
        
        queueName.async {
            
            let csvContent = self.uiFun.readFromCSV(fileName: "MUM Mono_trips")
            let csvNewLineContent = csvContent.components(separatedBy: .newlines)
            let delimiter = ","
            
            let lines:[String] = csvNewLineContent
            let monoTripModel = MonoTripModel()
            
            for line in lines {
                
                var inStopId            = false
                var inPattern           = false
                var inTrips             = false
                
                var stopValuePattern    = 0
                var monoData            = line.components(separatedBy: delimiter)
                
                if(monoData[0] != ""){
                    
                    let monoTripParseModel = MonoTripBaseModel()
                    monoTripParseModel.tripId = monoData[0]
                    
                    var i = 0
                    while(i < monoData.count){
                        
                        if(monoData[i] == "stopid" ){
                            
                            inStopId = true
                        }else if(monoData[i] == "patterns"){
                            
                            inStopId = false
                            inPattern = true
                        }else if(monoData[i] == "trips"){
                            
                            inPattern = false
                            inTrips = true
                        }else if(inStopId){
                            
                            let monoStopId         = MonoStopsId()
                            monoStopId.stopId      = monoData[i]
                            monoTripParseModel.stopModel.append(monoStopId)
                            
                        }else if(inPattern){
                            
                            let currentString = String(describing: monoData[i])
                            if(currentString.range(of: ":0") != nil){
                                stopValuePattern = 0
                            }
                                
                                let monoPatternModel   = MonoPatternModel()
                                monoPatternModel.stopInterval = monoData[i]
                                monoTripParseModel.stopModel[stopValuePattern].pattern.append(monoPatternModel)
                                stopValuePattern += 1
                            
                            
                            
                        }else if(inTrips){
                            
                            let monoTimingModel    = MonoTimingModel()
                            let tripRawData = monoData[i]
                            var tripData = tripRawData.components(separatedBy: "|")
                            
                            if(tripData.count > 1){
                                
                                monoTimingModel.startingTime = tripData[0]
                                monoTimingModel.calendarId   = tripData[2]
                                
                                monoTripParseModel.monoTiming.append(monoTimingModel)
                            }
                            
                        }
                        i += 1
                    }
                    monoTripModel.monoBaseModel.append(monoTripParseModel)
                    monoTripsDataList.append(monoTripModel)
                }
            }
        }
    }
}
