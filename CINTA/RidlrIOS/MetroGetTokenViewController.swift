//
//  MetroGetTokenViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 05/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import DLRadioButton
import GMStepper

var metroTokenJourneyType           = "SINGLE_JOURNEY"        // either single or return
var isOriginStationSelected         = false     // is starting location selected

var tokenOriginStationName          = "Origin Station"              // origin station name
var tokenDestinationStationName     = "Destination Station"        // destination station name

var tokenOriginStationAgencyId : Int!            // starting station agency id
var tokenDestinationAgencyId   : Int!            // destination station agency id

var tokenNumberOfPassengers    = 1

class MetroGetTokenViewController: UIViewController {

    
    @IBOutlet var singleJourneyRB   : DLRadioButton!
    @IBOutlet var returnJourneyRB   : DLRadioButton!
    
    @IBOutlet var originStationLabel: UILabel!
    @IBOutlet var destinationLabel  : UILabel!
    @IBOutlet var totalAmountLabel  : UILabel!
    
    @IBOutlet var confirmBT         : UIButton!
    @IBOutlet var passengerStepper  : GMStepper!
    
    var totalFare = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
        
        if(metroTokenJourneyType == "SINGLE_JOURNEY"){
            singleJourneyRB.isSelected  = true
            uiFun.writeData(value: "SINGLE_JOURNEY", key: "fetchingJourneyType")
        }else if(metroTokenJourneyType == "RETURN_JOURNEY"){
            returnJourneyRB.isSelected  = true
            uiFun.writeData(value: "RETURN_JOURNEY", key: "fetchingJourneyType")
        }else {
            singleJourneyRB.isSelected = true
            metroTokenJourneyType      = "SINGLE_JOURNEY"
            uiFun.writeData(value: "SINGLE_JOURNEY", key: "fetchingJourneyType")
            
        }
    
        // Origin Station Click
        let tapGesture                                  = UITapGestureRecognizer(target: self, action: #selector(originStationAction))
        tapGesture.numberOfTapsRequired                 = 1
        originStationLabel.isUserInteractionEnabled     = true
        originStationLabel.addGestureRecognizer(tapGesture)
        
        // destination Station Click
        let tapGesture1                             = UITapGestureRecognizer(target: self, action: #selector(destinationStationAction))
        tapGesture1.numberOfTapsRequired            = 1
        destinationLabel.isUserInteractionEnabled   = true
        destinationLabel.addGestureRecognizer(tapGesture1)
        
        // check for setting station name
        originStationLabel.text = tokenOriginStationName
        destinationLabel.text   = tokenDestinationStationName
        
        // change the text color when station gets selected
        if(tokenOriginStationName != "Origin Station"){
            originStationLabel.textColor    = UIColor.black
        }
         // change the text color when station gets selected
        if(tokenDestinationStationName != "Destination Station"){
            destinationLabel.textColor      = UIColor.black
        }
        
        // change the confirm detail button background color
        if(tokenOriginStationName != "Origin Station" && tokenDestinationStationName != "Destination Station"){
            confirmBT.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        }
        
        // Check for source and destination station must be different
        if(tokenOriginStationName == tokenDestinationStationName){
            if(isOriginStationSelected){
                originStationLabel.text         = "Origin Station"
                originStationLabel.textColor    = UIColor.gray
                tokenOriginStationName          = ""
            }else{
                destinationLabel.text       = "Destination Station"
                destinationLabel.textColor  = UIColor.gray
                tokenDestinationStationName = ""
            }
            
            uiFun.showAlert(title: "", message: "Source and destination can not be same", logMessage: "Same source and destination for metro token", fromController: self)
            
            return
        }
        
        getFareOfJourney()
    }
    
    // Fetch fare of journey based on type of journey and passesngers
    func getFareOfJourney(){
    
        if(tokenOriginStationName != "Origin Station" && tokenDestinationStationName != "Destination Station"){
           
            let fetchFare           = MetroFetchTokenFare()
            let fare                = fetchFare.getTokenFare(source: tokenOriginStationAgencyId , destination: tokenDestinationAgencyId)
            totalAmountLabel.text   = "₹ \(fare * Int(passengerStepper.value))"
            totalFare               = "\(fare * Int(passengerStepper.value))"
            tokenNumberOfPassengers = Int(passengerStepper.value)
            print("Metro Token Fare \(fare) \(passengerStepper.value)")
            
        }
    }
    
    // When user clicks on the origin station label
    func originStationAction(){
        uiFun.navigateToScreem(identifierName: "MetroStationsViewController", fromController: self)
        isOriginStationSelected = true
    }
    
    //When user clicks on the destination station label
    func destinationStationAction(){
        uiFun.navigateToScreem(identifierName: "MetroStationsViewController", fromController: self)
        isOriginStationSelected = false
    }
    
    func resetStations(){
        // this will reset starting and destination station
        tokenOriginStationName      = "Origin Station"
        tokenDestinationStationName = "Destination Station"
        metroTokenJourneyType       = "SINGLE_JOURNEY"
        uiFun.writeData(value: "SINGLE_JOURNEY", key: "fetchingJourneyType")
       
    }
    
    /************* RADIO BUTTON ACTIONS ***************/
    @IBAction func singleJourneyAction(_ sender: Any) {
        returnJourneyRB.isSelected = false
        metroTokenJourneyType      = "SINGLE_JOURNEY"
        getFareOfJourney()
        uiFun.writeData(value: "SINGLE_JOURNEY", key: "fetchingJourneyType")
    }
    
    @IBAction func returnJourneyAction(_ sender: Any) {
        singleJourneyRB.isSelected = false
        metroTokenJourneyType      = "RETURN_JOURNEY"
        getFareOfJourney()
        uiFun.writeData(value: "RETURN_JOURNEY", key: "fetchingJourneyType")
    }
    
    //Confirm Button Action
    @IBAction func confirmBTActino(_ sender: Any) {
        if(tokenOriginStationName != "Origin Station" && tokenDestinationStationName != "Destination Station"){
            
            let profileStatus   = uiFun.readData(key: "ProfileCreation") //  Fetching data from permanent storage regarding profile status
            tokenFlow           = "normalFlow"
            rechargeAmount      = totalFare
            uiFun.writeData(value: "MetroToken", key: "ticketingAgency")
            
            if(profileStatus == "done"){
                uiFun.navigateToScreem(identifierName: "PaymentViewController", fromController: self)
            }else{
                uiFun.createCleverTapEvent(message: "Profile creation touchpoints Metro Token")
                profileBackNavigation = "MetroHomeScreenViewController"
                uiFun.navigateToScreem(identifierName: "WelcomeToProfileViewController", fromController: self)
            }
            
       
        }else{
          
            
            uiFun.showAlert(title: "", message: "Please selected origin and destination stations", logMessage: "metro token confirm button", fromController: self)
        }
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
         resetStations()
         uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    //stepper action
    @IBAction func StepperActino(_ sender: Any) {
        if(tokenOriginStationName != "Origin Station" && tokenDestinationStationName != "Destination Station"){
            getFareOfJourney()
        }
    }
    
    
    
}
