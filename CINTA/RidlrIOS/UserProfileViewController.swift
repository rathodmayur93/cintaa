//
//  UserProfileViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 17/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreData

var rcashBalance            = ""
var citrusWalletBalance     = ""
var profileSelectedWallet   = ""
class UserProfileViewController: UIViewController, UIGestureRecognizerDelegate {

    
    @IBOutlet var backButton                : UIButton!
    @IBOutlet var editButton                : UIButton!
    @IBOutlet var changeCityButton          : UIButton!
    @IBOutlet var logOutButton: UIButton!
    
    @IBOutlet var userMobileNumberLabel     : UILabel!
    @IBOutlet var userEmailIdLabel          : UILabel!
    @IBOutlet var rCashLabel                : UILabel!
    @IBOutlet var citrusWalletBalanceLabel  : UILabel!
    
    @IBOutlet var ridlrCashView             : UIView!
    @IBOutlet var myWalletView              : UIView!
    @IBOutlet var citrusWalletView          : UIView!
    @IBOutlet var myBookingView             : UIView!
    var isCitrusConfigured                  : Bool!
    
    /************* FREECHARHGE VARIABLES **************/
    @IBOutlet var freechargeView            : UIView!
    @IBOutlet var freechargeLabel           : UILabel!
    @IBOutlet var freechargeBalanceLabel    : UILabel!
    var isFreechargeConfigured              : Bool!
    
    /************* MOBIKWIK VARIABLES **************/
    @IBOutlet var mobikwikView              : UIView!
    @IBOutlet var mobikwikLabel             : UILabel!
    @IBOutlet var mobikwikBalanceLabel      : UILabel!
    var isMobikwikConfigured                : Bool!
    
    var mobikwikWalletBalance                     : Double?
    var freechargeWalletBalance                   : Double?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        uiFun.showIndicatorLoader()
        // Fetching Wallet Detail, balance and Rcash api call
        let apiCall = UserFetchWalletBalanceAPICall()
        apiCall.makeAPICall(fromViewController: self)
        
        // Rcash label tap action
        let rcash: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToRcashScreen))
        ridlrCashView.addGestureRecognizer(rcash)
        ridlrCashView.isUserInteractionEnabled = true
        rcash.delegate = self
        
        // Booking history label tap action
        let bookingDetail: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateBookingScreen))
        myBookingView.addGestureRecognizer(bookingDetail)
        myBookingView.isUserInteractionEnabled = true
        bookingDetail.delegate = self
        
        // Citrus Wallet label tap action
        let citrusWallet: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToWalletScreen))
        citrusWalletView.addGestureRecognizer(citrusWallet)
        citrusWalletView.isUserInteractionEnabled = true
        citrusWallet.delegate = self
        
        // Mobikwik label tap action
        let mobikwikWallet: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToWalletScreenMobikwik))
        mobikwikView.addGestureRecognizer(mobikwikWallet)
        mobikwikView.isUserInteractionEnabled = true
        mobikwikWallet.delegate = self
        
        // Freecharge label tap action
        let freechargeWallet: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateToWalletScreenFreecharge))
        freechargeView.addGestureRecognizer(freechargeWallet)
        freechargeView.isUserInteractionEnabled = true
        freechargeWallet.delegate = self
        
        uiFun.createCleverTapEvent(message: "USER PROFILE SCREEN WITH RCASH & WALLET")
        profileSelectedWallet = "CITRUS"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //uiFun.showIndicatorLoader()
    }
    
    func setUI(){
        logOutButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        
        // Blur effect on background
        let blurEffect = UIBlurEffect(style: .extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        self.view.insertSubview(blurEffectView, at: 0)
        
        // Fetching user email id and mobile Number from parmanent storage
        let userEmail       = uiFun.readFromDatabase(entityName: "Users", key: "userEmail")
        let mobileNumber    = uiFun.readFromDatabase(entityName: "Users", key: "mobileNumber")
        print("User email id and mobile number \(userEmail) & \(mobileNumber)")
        
        userEmailIdLabel.text           = userEmail
        userMobileNumberLabel.text      = mobileNumber
    }
    
    func gotResponseFromPaymentAPI(response : JSON){
        
        // API response of fetching wallet balnace, rcash balance
        uiFun.hideIndicatorLoader()
        
        print("No Internet Error \(response)")
        if(response["status"] == "OK" || response["status"] == "PHE"){
            // API response success
            let jsonData = response["data"]
            
            /************** CITRUS WALLET CHECK *******************/
            let jsonPaymentOption = jsonData["paymentOptions"].arrayValue[0]
            isCitrusConfigured    = NSString(string: String(describing: jsonPaymentOption["configured"])).boolValue
         
            if(isCitrusConfigured!){
                if(jsonPaymentOption["details"] != nil){
                    citrusWalletBalanceLabel.text = String(describing: jsonPaymentOption["details"]["balance"]) // Citrus wallet balance
                    citrusWalletBalance           = String(describing: jsonPaymentOption["details"]["balance"])
                    isCitrusConfigured            = true
                }else{
                    // Citrus wallet is not added or configured
                    uiFun.hideIndicatorLoader()
                    citrusWalletBalanceLabel.text = "Currently Unavailable"
                    
                }
            }else{
                uiFun.hideIndicatorLoader()
                citrusWalletBalanceLabel.text = "Add Wallet"
                
            }
        
            /************ MOBIKWIK WALLET CHECK ************/
            
            let jsonPaymentOptionMobi = jsonData["paymentOptions"].arrayValue[6]
            isMobikwikConfigured      = NSString(string: String(describing: jsonPaymentOptionMobi["configured"])).boolValue
            
            if(isMobikwikConfigured!){
                if(jsonPaymentOptionMobi["details"] != nil){
                
                    let mobikwikBalance         = String(describing: jsonPaymentOptionMobi["details"]["balance"]) // mobikwik Balance
                    print("jsonPaymentOption \(mobikwikBalance)")
                
                    mobikwikBalanceLabel.text   = "\(mobikwikBalance)"
                    citrusWalletBalance         = "\(mobikwikBalance)"
                    mobikwikWalletBalance       = Double(mobikwikBalance)
                    print("Mobikwik balance is equal to \(mobikwikWalletBalance!)")
                }else{
                    // Mobikwik wallet is not added or configured
                    uiFun.hideIndicatorLoader()
                    mobikwikBalanceLabel.text        = "Currently Unavailable"
                    mobikwikWalletBalance            =  0.0
                    //uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
                }
            }else{
                uiFun.hideIndicatorLoader()
                mobikwikBalanceLabel.text        = "Add Wallet"
                mobikwikWalletBalance            =  0.0
            }
            
            /************ FREECHARGE WALLET CHECK ************/
            
            let jsonPaymentOptionFree = jsonData["paymentOptions"].arrayValue[5]
            isFreechargeConfigured      = NSString(string: String(describing: jsonPaymentOptionFree["configured"])).boolValue
            
            if(isFreechargeConfigured!){
                if(jsonPaymentOptionFree["details"] != nil){
                
                    let freechargeBalance = String(describing: jsonPaymentOptionFree["details"]["balance"]) // Freecharge Wallet balance
                    print("jsonPaymentOption \(freechargeBalance)")
                
                    freechargeBalanceLabel.text    = "\(freechargeBalance)"
                    citrusWalletBalance            = "\(freechargeBalance)"
                    freechargeWalletBalance        = Double(freechargeBalance)
                    isFreechargeConfigured         = true
                    print("Freecharge balance is equal to \(freechargeWalletBalance!)")
                }else{
                    // Freecharge wallet is not added or configured
                    uiFun.hideIndicatorLoader()
                    freechargeBalanceLabel.text     = "Currenly Unavailable"
                    freechargeWalletBalance         = 0.0
                    //uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
                }
            }else{
                uiFun.hideIndicatorLoader()
                freechargeBalanceLabel.text     = "Add Wallet"
                freechargeWalletBalance         = 0.0
            }
            /****************** RCASH WALLET *******************/
            let ridlrCash = jsonData["paymentOptions"].arrayValue[7]
            if(ridlrCash["details"] != nil){
                let rcash       = ridlrCash["details"]["balance"]   // Availabel Rcash or ridlr cash
                rCashLabel.text = String(describing: rcash)
                rcashBalance    = String(describing: rcash)
            }else{
                // unable to fetch wallet balance
                uiFun.hideIndicatorLoader()
                rCashLabel.text = "Unable To Fetch"
            }
        }else{
            
            uiFun.hideIndicatorLoader()
            // If there is no errorCodeText parse response to "showExceptionDialog" fucntion
            uiFun.showExceptionDialog(jsonResponse: response, fromViewController: self)
        }
    }
    
    func navigateToRcashScreen(){
        // navigation of Rcash Screen
        uiFun.navigateToScreem(identifierName: "RidlrPointsViewController", fromController: self)
    }
    
    func navigateBookingScreen(){
        // Navigation of booking history screen
        uiFun.createCleverTapEvent(message: "Tap on My Bookings")
        uiFun.navigateToScreem(identifierName: "BookingsViewController", fromController: self)
    }
    
    func navigateToWalletScreen(){
        // navigation of wallet screen
        uiFun.createCleverTapEvent(message: "Tap on My Wallet Citrus")
        citrusWalletBalance = citrusWalletBalanceLabel.text!
        if(isCitrusConfigured!){
            // if citrus is configured or added it will redirect to AddMOneyViewCOntroller screen
            uiFun.navigateToScreem(identifierName: "AddMoneyIntoWalletViewController", fromController: self)
        }else{
            addingWhichWallet = "CITRUSPAY"
            isFromProfileWallet = true
            // if selected citrus is not configured or added it will redirect to CreateProfile screen
            uiFun.navigateToScreem(identifierName: "AddWalletViewController", fromController: self)
        }
    }
    
    func navigateToWalletScreenMobikwik(){
        
        uiFun.createCleverTapEvent(message: "Tap on My Wallet Mobikwik")
        profileSelectedWallet = "MOBIKWIK"
        citrusWalletBalance   = mobikwikBalanceLabel.text!
        if(isMobikwikConfigured!){
            // if mobikwik is configured or added it will redirect to AddMOneyViewCOntroller screen
            uiFun.navigateToScreem(identifierName: "AddMoneyIntoWalletViewController", fromController: self)
        }else{
            // if selected mobikwik is not configured or added it will redirect to CreateProfile screen
            addingWhichWallet   = "MOBIKWIK"
            isFromProfileWallet = true
            uiFun.navigateToScreem(identifierName: "AddWalletViewController", fromController: self)
        }
    }
    
    func navigateToWalletScreenFreecharge(){
        uiFun.createCleverTapEvent(message: "Tap on My Wallet Freecharge")
        profileSelectedWallet = "FREECHARGE"
        citrusWalletBalance   = freechargeBalanceLabel.text!
        if(isFreechargeConfigured!){
            // if selected freecharge is configured or added it will redirect to CreateProfile screen
            uiFun.navigateToScreem(identifierName: "AddMoneyIntoWalletViewController", fromController: self)
        }else{
            // if selected freecharge is not configured or added it will redirect to CreateProfile screen
            addingWhichWallet   = "FREECHARGE"
            isFromProfileWallet = true
            uiFun.navigateToScreem(identifierName: "AddWalletViewController", fromController: self)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        // back button tap action
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
    @IBAction func editButtonTapped(_ sender: Any) {
        // Edit profile where user can update his email id
        uiFun.createCleverTapEvent(message: "Taps on edit profile")
        uiFun.navigateToScreem(identifierName: "EditProfileViewController", fromController: self)
    }
    
    @IBAction func changeCityButtonTapped(_ sender: Any) {
        // Change city button where user can change his city and it will show list of city
        uiFun.navigateToScreem(identifierName: "CityListViewController", fromController: self)
    }
    
    @IBAction func logoutButtonTapped(_ sender: Any) {
        // On logout user will logout from its profile and ridlr-token will be set to blank
        
        uiFun.writeData(value: "", key: "ridlrToken")
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)

        trainStopList.removeAll()
        metroStopList1.removeAll()
        monoStopList1.removeAll()
        
        UserDefaults.standard.set(true, forKey: "justAnotherKey1")
        UserDefaults.standard.set(true, forKey: "justAnotherKey2")
        
        for key in UserDefaults.standard.dictionaryRepresentation().keys {
            UserDefaults.standard.removeObject(forKey: key.description)
        }
        print(UserDefaults.standard.dictionaryRepresentation().keys.count)
        uiFun.writeData(value: "NotDone", key: "ProfileCreation")
    }
    
    func deleteAllData(entity: String)
    {
        // This function will clear all the core data values this function is not in use but may be in future will need this
        let appDelegate         = UIApplication.shared.delegate as! AppDelegate
        let managedContext      = appDelegate.persistentContainer.viewContext
        let fetchRequest        = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    
}







