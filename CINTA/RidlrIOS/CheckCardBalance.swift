//
//  CheckCardBalance.swift
//  RidlrIOS
//
//  Created by Mayur on 09/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CheckCardBalance {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : MetroCardViewController, cardNumber : String){
        
        let url = "\(constant.BASE_URL)mumbai_metro/api/metro/card/\(cardNumber)" // URL for getting balance of metro card
        print("Card Fetch Balance payment gateway url \(url)")
        
        util.GetAPICall(apiUrl: url){result, error in
            // Executing the api call and response will come in result and if there is any error while calling api it will come in error
            if let jsonResponse = result{
                // API Call Successful
                fromViewController.cardBalanceAPIResponse(response: jsonResponse) // Parsing response to respective view controller method
                print("Fetch Card Balance \(jsonResponse) ")
                return
                
            }else {
                // Error in api Call
                print("Fetch Card Balance Failed....Try Harder Mayur ")
                return
            }
            
        }
    }
}
