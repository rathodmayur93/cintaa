//
//  TripStartAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 04/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class TripStartAPI {
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(bookingNo : String, companyItemId : String, companyReferenceId  :String, fromViewController : TripPassReceiptViewController){
        
        let randomNum:UInt32            = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String      = String(randomNum)
        let timeStamp                   = Int(NSDate().timeIntervalSince1970)
        
        
        let url = "\(constant.METRO_TOKEN)/trip/start"
        
        print("Metro Token Penalty Items \(url)")
        
        let jsonString      = createJSONBody(bookingNo: bookingNo, companyItemId: companyItemId, companyReferenceId:companyReferenceId, itemSubType: "TRIP_PASS")
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.gotResponseFromTripStart(response: jsonResponse)
                print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
            }else {
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
        }
        
    }
    
    func makeAPICall(bookingNo : String, companyItemId : String, companyReferenceId  :String, fromViewController : StoreValueReceiptViewController){
        
        let randomNum:UInt32            = arc4random_uniform(10000000) // range is 0 to 9999999
        let randomNumString:String      = String(randomNum)
        let timeStamp                   = Int(NSDate().timeIntervalSince1970)
        
        
        let url = "\(constant.METRO_TOKEN)/trip/start"
        
        print("Metro Token Penalty Items \(url)")
        
        let jsonString      = createJSONBody(bookingNo: bookingNo, companyItemId: companyItemId, companyReferenceId: companyReferenceId, itemSubType: "STORE_VALUE_PASS")
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: true){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.gotResponseFromTripStart(response: jsonResponse)
                print("Successfully PaymentRechargeAPICall Done.... \(jsonResponse) ")
                return
            }else {
                print("Successfully PaymentRechargeAPICall Failed....Try Harder Mayur ")
                return
            }
        }
        
    }
    
    
    func createJSONBody(bookingNo : String, companyItemId : String, companyReferenceId : String, itemSubType : String) -> String{
    
        let para        = NSMutableDictionary()
        let timestamp   = Int(NSDate().timeIntervalSince1970 * 1000)
        
        para.setValue(bookingNo, forKey: "bookingNo")
        para.setValue("RELIANCE_METRO", forKey: "company")
        para.setValue(companyItemId, forKey: "companyItemId")
        para.setValue(companyReferenceId, forKey: "companyReferenceId")
        para.setValue("\(timestamp)", forKey: "issueTime")
        para.setValue(itemSubType, forKey: "itemSubType")
        para.setValue("PASS", forKey: "itemType")
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            print("json array string metro json request body = \(jsonString)") // JSON string for metro payment
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return ""
        }
    }
}
