//
//  TokenQRCodeViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 06/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import QRCode
import Security
import CryptoSwift

var isTokenAvailable = false
var tokenSubType     = ""
var tokenIdentifier  = ""

class TokenQRCodeViewController: UIViewController {

    @IBOutlet var tokenQRCodeIV     : UIImageView!
    @IBOutlet var originStationName : UILabel!
    @IBOutlet var destinationName   : UILabel!
    @IBOutlet var amountLabel       : UILabel!
    @IBOutlet var dateTimeLabel     : UILabel!
    @IBOutlet var expiryDateLabel   : UILabel!
    @IBOutlet var tokenIndicator    : UILabel!
    @IBOutlet var tripId            : UILabel!
    @IBOutlet var referenceId       : UILabel!
    @IBOutlet var headingLabel      : UILabel!
    
    
    @IBOutlet var rightButton       : UIButton!
    @IBOutlet var leftButton        : UIButton!
    
    @IBOutlet var journeySegment    : UISegmentedControl!
    
    var currentTokenIndex           = 1
    var totalNumberOfToken          = 0
    var qrCode                      : QRCode!
    var tokenJourneyIndex           = 0
    var typeOfToken                 = ""
    
    var bookingItemList             = [MetroTokenJSONModel]()
    var metroTokenList              = [MetroTokenJSONModel]()
    
    var outward_journey             = [NewMetroTokenModel]()
    var return_journey              = [NewMetroTokenModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func journeySegmentAction(_ sender: Any) {
        
      switch journeySegment.selectedSegmentIndex {
        case 0:
            tokenJourneyIndex       = 0
        case 1:
            tokenJourneyIndex       = 1
        default:
            break
        }
        
        showQRCode(currentIndex: 1)
    }
    
    func setUI(){
    
        if(metroTokenJourneyType == "TRIP_PASS" || metroTokenJourneyType == "STORE_VALUE_PASS"){
            
        }else if(paymentResponseListWebview.count != 0){
        
            if(paymentResponseListWebview[0].data[0].bookingDetails.count == 1
                && paymentResponseListWebview[0].data[0].bookingDetails[0].qrCode.count == 1){
                
                rightButton.isHidden    = true
                leftButton.isHidden     = true
                tokenIndicator.isHidden = true
            }
            
        }
        //singleJourney(currentIndex: currentTokenIndex)
        
        if(metroTokenJourneyType != "RETURN_JOURNEY"){
            journeySegment.isHidden = true // hide segment control for single journey
        }else{
            journeySegment.isHidden = false // show segment control for return journey
        }
        
        SetHeaderLabel()
        distinguishToken()
        showQRCode(currentIndex: currentTokenIndex)
    }
    
    func SetHeaderLabel(){
    
        switch metroTokenJourneyType {
        case "RETURN_JOURNEY":
            headingLabel.text = "Return Journey Token"
            break
        case "TRIP_PASS":
            headingLabel.text = "Trip Pass"
            break
        case "STORE_VALUE_PASS":
            headingLabel.text = "Store Value Pass"
            break
        default:
            headingLabel.text   = "Single Journey Token"
            break
        }
    }
    
    func distinguishToken(){
        
        bookingItemList     = DBManager.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
        metroTokenList      = DBManagerToken.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
        print("Metro Token Journey Type Is \(metroTokenJourneyType))")
       // tokenSubType        = String(describing :bookingItemList[0].itemSubType)
        
        
        for i in 0..<metroTokenList.count{
            
            typeOfToken = String(describing: metroTokenList[i].newMetroToken[0].type)
            print("Metro Token Type Is \(typeOfToken)")
            
            switch typeOfToken {
            case "OUTWARD":
                outward_journey.append(getMetroTokenList(index: i))
                break
            case "RETURN" :
                return_journey.append(getMetroTokenList(index: i))
                break
            case "PENALTY" :
                outward_journey.append(getMetroTokenList(index: i))
                break
            case "TRIP_PASS":
                outward_journey.append(getMetroTokenList(index: i))
                break
            case "STORE_VALUE":
                outward_journey.append(getMetroTokenList(index: i))
            default:
                break
            }
        }
    }
    
    func getMetroTokenList(index : Int) -> NewMetroTokenModel{
        
        let metroToken                  = NewMetroTokenModel()
        metroToken.qrCode               = String(describing: metroTokenList[index].newMetroToken[0].qrCode)
        metroToken.expiry               = String(describing: metroTokenList[index].newMetroToken[0].expiry)
        metroToken.qrCodeId             = String(describing: metroTokenList[index].newMetroToken[0].qrCodeId)
        metroToken.status               = String(describing: metroTokenList[index].newMetroToken[0].status)
        metroToken.type                 = String(describing: metroTokenList[index].newMetroToken[0].type)
        
        metroToken.sourceName           =  String(describing: metroTokenList[index].newMetroToken[0].sourceName)
        metroToken.destinationName      =  String(describing: metroTokenList[index].newMetroToken[0].destinationName)
        metroToken.sourceId             =  String(describing: metroTokenList[index].newMetroToken[0].sourceId)
        metroToken.destinationId        =  String(describing: metroTokenList[index].newMetroToken[0].destinationId)
        return metroToken
    }
    
    func showQRCode(currentIndex : Int){
    
        var qrCodeString            = ""
        var encryptedExtraString    = ""
        
        if(outward_journey.count == 0 && return_journey.count > 0){
            journeySegment.isHidden = true
            tokenJourneyIndex = 1
        }
        
        if(tokenJourneyIndex == 0){
            qrCodeString            = String(describing: outward_journey[currentIndex - 1].qrCode)
            tokenIdentifier         = String(describing: outward_journey[currentIndex - 1].qrCodeId)
            
            var expiry              = metroTokenList[0].expiryDate
            let currentDate         = bookingItemList[0].activationDate
            
            if(typeOfToken == "TRIP_PASS" || typeOfToken == "STORE_VALUE"){
                
                expiry                      = bookingItemList[0].expiryDate
                var qrCodeExtraString       = "\(expiry)|\(currentDate)"
                
                let paddingChar     = 0;
                let size            = 16;
                let x               = qrCodeExtraString.characters.count % size;
                let padLength       = size - x;
                
                for i in 0..<padLength
                {
                    qrCodeExtraString += "\(paddingChar)"
                    print("Padded String for qrCode \(qrCodeExtraString)")
                }
                
                encryptedExtraString        = qrCodeExtraString.aesEncrypt(key: "0123456789abcdef", iv: "fedcba9876543210")
                print("Extra QR Code String \(qrCodeExtraString) and ecrypted string is \(encryptedExtraString)")
            }
            
            uiFun.writeData(value: String(describing: outward_journey[currentIndex - 1].sourceId), key: "penalyuSourceId")
            originStationName.text  = String(describing: outward_journey[currentIndex - 1].sourceName)
            destinationName.text    = String(describing: outward_journey[currentIndex - 1].destinationName)
            
            tripId.text             = tokenIdentifier
            referenceId.text        = bookingItemList[currentIndex - 1].companyRefId
            
            qrCodeString            = "\(qrCodeString + encryptedExtraString)"
            print("QR Code visible to user is \(qrCodeString)")
            
            tokenIndicator.text  = "\(currentIndex)/\(outward_journey.count)"
            totalNumberOfToken   = outward_journey.count
            
        }else if(tokenJourneyIndex == 1){
            
            qrCodeString    = String(describing: return_journey[currentIndex - 1].qrCode)
            tokenIdentifier = String(describing: return_journey[currentIndex - 1].qrCodeId)
            print("QR Code visible to user is \(qrCodeString)")
            
            uiFun.writeData(value: bookingItemList[currentIndex - 1].routeDetail[0].sourceId, key: "penalyuSourceId")
            originStationName.text  = bookingItemList[currentIndex - 1].routeDetail[0].destinationName
            destinationName.text    = bookingItemList[currentIndex - 1].routeDetail[0].sourceName
            
            tripId.text             = tokenIdentifier
            referenceId.text        = bookingItemList[0].companyRefId
            
            tokenIndicator.text  = "\(currentIndex)/\(return_journey.count)"
            totalNumberOfToken   = return_journey.count
        }
        
        qrCode                            = QRCode(qrCodeString)
        print("Encrypted QRCode String is \(qrCodeString)")
        tokenQRCodeIV.image               = qrCode.image // setting token into imageview
    
        // setting ui label text
        //originStationName.text            = String(describing : bookingItemList[0].routeDetail[0].sourceName)
        //destinationName.text              = String(describing : bookingItemList[0].routeDetail[0].destinationName)
        amountLabel.text                  = String(describing : bookingItemList[0].fare[0].totalAmount)
        
        // converting timestamp into string date format
        let date                          = Date()
        let timestampDate                 = NSDate(timeIntervalSince1970: Double(String(describing: metroTokenList[0].newMetroToken[0].expiry))!/1000)
        let dayTimePeriodFormatter        = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
        let currentDate                   = dayTimePeriodFormatter.string(from: date)
        let dateString                    = currentDate
        dateTimeLabel.text                = dateString
        
        expiryDateLabel.text                = "*Token valid till \(dayTimePeriodFormatter.string(from: timestampDate as Date)), 01:10 a.m."
    }
    
    func singleJourney(currentIndex : Int){
        // Fetching QRCode string and storing it into database
        let metroTokenString = ""//uiFun.readData(key: "QRCodeString")
        
        if(metroTokenString != ""){
            // showing active metro token offline
            print("Offline Token")
            qrCode               = QRCode(metroTokenString)
            tokenQRCodeIV.image  = qrCode.image // setting token into imageview
            
            originStationName.text            = uiFun.readData(key: "tokenOriginStation")
            destinationName.text              = uiFun.readData(key: "tokenDestinationStation")
            amountLabel.text                  = uiFun.readData(key: "tokenAmount")
            
            let timestamp                     = uiFun.readData(key: "tokenTime")
            let timestampDate                 = NSDate(timeIntervalSince1970: Double(timestamp)!/1000)
            let dayTimePeriodFormatter        = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
            let dateString                    = dayTimePeriodFormatter.string(from: timestampDate as Date)
            dateTimeLabel.text                = dateString
            
            
            
        }else{
            
            var qrCodeString        = ""
            var timestamp           = ""
            
            print("API call Token")
            switch tokenFlow {
              case "normalFlow":
                let bookingDatabaseList       = DBManager.shared.fetchBookingItems(itemSubType: metroTokenJourneyType)
                qrCodeString                  = paymentResponseListWebview[0].data[0].bookingDetails[currentIndex - 1].qrCode[0].qrCode
                timestamp                     = paymentResponseListWebview[0].data[0].bookingDetails[currentIndex - 1].qrCode[0].expiry
                break
            case "inventoryFlow":
                qrCodeString                  = paymentResponseListWebview[0].data[0].bookingDetails[0].qrCode[currentIndex - 1].qrCode
                timestamp                     = paymentResponseListWebview[0].data[0].bookingDetails[0].qrCode[currentIndex - 1].expiry
            default:
                break
            }
            
            qrCode                            = QRCode(qrCodeString)
            tokenQRCodeIV.image               = qrCode.image // setting token into imageview
            uiFun.writeData(value: qrCodeString, key: "QRCodeString")
        
            // setting ui label text
            originStationName.text            = tokenOriginStationName
            destinationName.text              = tokenDestinationStationName
            amountLabel.text                  = rechargeAmount
        
            // converting timestamp into string date format
            let date                          = Date()
            let timestampDate                 = NSDate(timeIntervalSince1970: Double(timestamp)!/1000)
            let dayTimePeriodFormatter        = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
            let currentDate                   = dayTimePeriodFormatter.string(from: date)
            let dateString                    = currentDate
            dateTimeLabel.text                = dateString
        
            expiryDateLabel.text                = "*Token valid till \(dayTimePeriodFormatter.string(from: timestampDate as Date)), 01:10 a.m."
            
            uiFun.writeData(value: timestamp, key: "tokenTime") //// storing timestamp into parmanent storage
           // storingData()
        }
    }
    
    func returnJourney(currentIndex : Int){
        
        print("API call Token")
        let qrCodeString                  = paymentResponseListWebview[0].data[0].bookingDetails[currentTokenIndex - 1].qrCode[1].qrCode
        qrCode                            = QRCode(qrCodeString)
        tokenQRCodeIV.image               = qrCode.image // setting token into imageview
        uiFun.writeData(value: qrCodeString, key: "QRCodeString")
        
        // setting ui label text
        originStationName.text            = tokenDestinationStationName
        destinationName.text              = tokenOriginStationName
        amountLabel.text                  = rechargeAmount
        
        // converting timestamp into string date format
        let date                          = Date()
        let timestamp                     = paymentResponseListWebview[0].data[0].bookingDetails[currentTokenIndex - 1].qrCode[1].expiry
        let timestampDate                 = NSDate(timeIntervalSince1970: Double(timestamp)!/1000)
        let dayTimePeriodFormatter        = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"
        let currentDate                   = dayTimePeriodFormatter.string(from: date)
        let dateString                    = currentDate
        dateTimeLabel.text                = dateString
        
        expiryDateLabel.text                = "*Token valid till \(dayTimePeriodFormatter.string(from: timestampDate as Date)), 01:10a.m."
        
        uiFun.writeData(value: timestamp, key: "tokenTime") //// storing timestamp into parmanent storage
    }

    
    func storingData(){
        
        // storing data into parmanent storage for offline usage
        uiFun.writeData(value: tokenOriginStationName, key: "tokenOriginStation")
        uiFun.writeData(value: tokenDestinationStationName, key: "tokenDestinationStation")
        uiFun.writeData(value: rechargeAmount, key: "tokenAmount")
        uiFun.writeData(value: metroTokenJourneyType, key: "journeyType")
        uiFun.writeData(value: paymentResponseListWebview[0].data[0].bookingDetails[0].qrCode[0].qrCodeId, key: "tokenIdentifier")
        
        // company reference id shouldn't be blank since we need it for inventory items api call
        if(paymentResponseListWebview[0].data[0].bookingDetails[0].companyReferenceId != ""){
            uiFun.writeData(value: paymentResponseListWebview[0].data[0].bookingDetails[0].companyReferenceId, key: "companyReferenceId")
        }
        
        //print("Metro token value \(uiFun.readFromDatabase(entityName: "Users", key: "metroToken"))")
        isTokenAvailable     = true
        
        // finding the total number of token
        totalNumberOfToken   = paymentResponseListWebview[0].data[0].bookingDetails.count
        
        
        // deciding number of token from where user navigated to token screen
        switch tokenFlow {
            case "inventoryFlow":
                totalNumberOfToken   = paymentResponseListWebview[0].data[0].bookingDetails[0].qrCode.count
                break
            case "normalFlow":
                totalNumberOfToken   = paymentResponseListWebview[0].data[0].bookingDetails.count
                break
            default:
                break
            }
        
        tokenIndicator.text  = "\(currentTokenIndex)/\(totalNumberOfToken)"
        
        print("total number of token present is \(totalNumberOfToken)")
    }
    
    
    @IBAction func rightBTAction(_ sender: Any) {
        
        currentTokenIndex += 1      // will display next token when user clicks on right butt
       
        // check for index out of range
        if(currentTokenIndex <= totalNumberOfToken && currentTokenIndex >= 0){
            
            // showing qr code index
            tokenIndicator.text  = "\(currentTokenIndex)/\(totalNumberOfToken)"
          
            if(tokenJourneyIndex == 0){
                // calling method to show next single journey family token
                showQRCode(currentIndex: currentTokenIndex)
            }else{
                 // calling method to show for return journey family token
                showQRCode(currentIndex: currentTokenIndex)
            }
        }else if(currentTokenIndex == totalNumberOfToken + 1){
            currentTokenIndex -= 1
        }
        
    }
    
    @IBAction func leftBTAction(_ sender: Any) {
        currentTokenIndex -= 1      // will display the previous token when user clicks on left button
      
        // check for index out of range
        if(currentTokenIndex <= totalNumberOfToken && currentTokenIndex >= 1){
            
            // showing qr code index
            tokenIndicator.text  = "\(currentTokenIndex)/\(totalNumberOfToken)"
            
            if(tokenJourneyIndex == 0){
                // calling method to show next single journey family token
                showQRCode(currentIndex: currentTokenIndex)
            }else{
                // calling method to show for return journey family token
                showQRCode(currentIndex: currentTokenIndex)
            }
        }else if(currentTokenIndex == 0){
            currentTokenIndex += 1
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "MetroHomeScreenViewController", fromController: self)
    }
    
    @IBAction func penalyAction(_ sender: Any) {
        
        openActionSheet()
    }
    
    func openActionSheet(){
    
        let penaltyMenu      = UIAlertController(title: nil, message: "Penalty Type", preferredStyle: .actionSheet)
        
        let exitAction       = UIAlertAction(title: "Exit Issue", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            uiFun.navigateToScreem(identifierName: "PenaltyIssueViewController", fromController: self)
            print("File Deleted")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        penaltyMenu.addAction(exitAction)
        penaltyMenu.addAction(cancelAction)
        
        self.present(penaltyMenu, animated: true, completion: nil)
    }
    
    
    func mcryptEncryption(qrCodeString : String) -> String?{
    
        let key             = "0123456789abcdef"
        let iv              = "fedcba9876543210"
        
        let keyData: NSData! = (key as NSString).data(using: String.Encoding.utf8.rawValue) as NSData!
        let keyBytes         = keyData.bytes
        let keyLength        = size_t(kCCKeySizeAES256)
        
        let plainData     = (qrCodeString as NSString).data(using: String.Encoding.utf8.rawValue) as NSData!
        let dataLength    = UInt((plainData?.length)!)
        let dataBytes     = plainData?.bytes
        
        let bufferData    = NSMutableData(length: Int(dataLength) + kCCBlockSizeAES128)
        let bufferPointer = bufferData?.mutableBytes
        let bufferLength  = size_t((bufferData?.length)!)
        
        let operation: CCOperation = UInt32(kCCEncrypt)
        let algoritm:  CCAlgorithm = UInt32(kCCAlgorithmAES128)
        let options = ccNoPadding
        
        let ivData: NSData! = (iv as NSString).data(using: String.Encoding.utf8.rawValue) as NSData!
        let ivPointer = ivData.bytes
        
        var numBytesEncrypted   = 0
        
        var cryptStatus = CCCrypt(operation, algoritm, CCOptions(options), keyBytes, keyLength, ivPointer, dataBytes, Int(dataLength), bufferPointer, bufferLength, &numBytesEncrypted)
        
        bufferData?.length = Int(numBytesEncrypted)
        let base64cryptString = bufferData?.base64EncodedString(options: .lineLength64Characters)
        print("mcrypt \(base64cryptString!)")
        
        return base64cryptString
    
    }
    
    
}
extension String{

    func aesEncrypt(key: String, iv: String) -> String {
        var k : Int32!
        self.withCString {
            ( bytes : (UnsafePointer<CChar>) ) -> Void in
            k = atoi(bytes)
            print("k is \(k)")
        }
        
        let data                = self.data(using: .utf8)!
        let buf                 = [UInt8](data)
        let encrypted           = try! AES(key: key, iv: iv, blockMode: .CBC, padding: NoPadding()).encrypt(buf)
        let encryptedData       = Data(encrypted)
        
//        var charArray: [Character]    = [Character](repeating: nil, count: buf.capacity)
//        for i in 0..<buf.count{
//            
//            charArray[2 * i] = HEX_CHARS[(buf[i])]
//            
//        }
        
        print("bytesToHex \(encryptedData.hexEncodedString())")
        return encryptedData.hexEncodedString()
    }
}
extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
