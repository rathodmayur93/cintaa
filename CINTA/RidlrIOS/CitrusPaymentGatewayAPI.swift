//
//  CitrusPaymentGatewayAPI.swift
//  RidlrIOS
//
//  Created by Mayur on 07/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class CitrusPaymentGatewayAPI {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()

    func makeAPICall(fromViewController : AddMoneyViewController, value : String, cvv : String, expiryDate : String, cardNumber : String, scheme : String, cardType : String){
        
        let url = "\(constant.CITRUS_PG)"
        print("Citrus payment gateway url \(url)")
        
        let jsonRequstBody = citrusPGJSONString(value: value, cvv: cvv, expiryDate: expiryDate, cardNumber: cardNumber, scheme: scheme, cardType: cardType)
        
        util.APICall(apiUrl: constant.CITRUS_PG, jsonString: jsonRequstBody!, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.citrusPGResponse(response: jsonResponse)
                print("Successfully Citrus Payment Gateway Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Citrus Payment Gateway Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
    
    func citrusPGJSONString(value : String, cvv : String, expiryDate : String, cardNumber : String, scheme : String, cardType : String) -> String?{
        
        let amount:NSMutableDictionary = NSMutableDictionary()
        amount.setValue("INR", forKey: "currency")
        amount.setValue(value, forKey: "value")
        
        let paymentMode:NSMutableDictionary = NSMutableDictionary()
        paymentMode.setValue(cvv, forKey: "cvv")
        paymentMode.setValue(expiryDate, forKey: "expiry")
        paymentMode.setValue("", forKey: "holder")
        paymentMode.setValue(cardNumber, forKey: "number")
        paymentMode.setValue(scheme, forKey: "scheme")
        paymentMode.setValue(cardType, forKey: "type")
        
        let paymentToken:NSMutableDictionary = NSMutableDictionary()
        paymentToken.setValue(paymentMode, forKey: "paymentMode")
        paymentToken.setValue("paymentOptionToken", forKey: "type")
        
        let address:NSMutableDictionary = NSMutableDictionary()
        address.setValue("Mumbai", forKey: "city")
        address.setValue("India", forKey: "country")
        address.setValue("Maharashtra", forKey: "state")
        address.setValue("streetone", forKey: "street1")
        address.setValue("streettwo", forKey: "street2")
        address.setValue("400052", forKey: "zip")
        
        let userDetail:NSMutableDictionary = NSMutableDictionary()
        userDetail.setValue(address, forKey: "address")
        userDetail.setValue(uiFun.readFromDatabase(entityName: "Users", key: "userEmail"), forKey: "email")
        userDetail.setValue("Tester", forKey: "firstName")
        userDetail.setValue("Citrus", forKey: "lastName")
        userDetail.setValue(uiFun.readFromDatabase(entityName: "Users", key: "mobileNumber"), forKey: "mobileNo")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(value, forKey: "amount")
        para.setValue(uiFun.readData(key: "merchantAccessKey"), forKey: "merchantAccessKey")
        para.setValue(uiFun.readData(key: "merchantTxnId"), forKey: "merchantTxnId")
        para.setValue(uiFun.readData(key: "notifyUrl"), forKey: "notifyUrl")
        para.setValue(paymentToken, forKey: "paymentToken")
        para.setValue("MSDKG", forKey: "requestOrigin")
        para.setValue(uiFun.readData(key: "requestSignature"), forKey: "requestSignature")
        para.setValue(uiFun.readData(key: "returnUrl"), forKey: "returnUrl")
        para.setValue(userDetail, forKey: "userDetails")
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("citrus add money json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }

}
