//
//  TImeTableTimingViewCell.swift
//  RidlrIOS
//
//  Created by Mayur on 23/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit

class TImeTableTimingViewCell: UITableViewCell {

    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var destionStationNameLabel: UILabel!
    @IBOutlet var startsFromStationLabel: UILabel!
    @IBOutlet var agencyName: UILabel!
    let hopeView : UIView = UIView()
    
    /************** HOP 1 **************/
    
    @IBOutlet var hope1View: UIView!
    @IBOutlet var hopeNumberLabel: UILabel!
    @IBOutlet var changeAtHopeNameLabel: UILabel!
    @IBOutlet var hopeTime: UILabel!
    @IBOutlet var hopeStationName: UILabel!
    @IBOutlet var hopeStartsFrom: UILabel!
    @IBOutlet var hopeAgency: UILabel!
    
    /************** HOP 2 **************/
    
    @IBOutlet var hope2View: UIView!
    @IBOutlet var hopeNumberLabel2: UILabel!
    @IBOutlet var changeAtHopeNameLabel2: UILabel!
    @IBOutlet var hopeTime2: UILabel!
    @IBOutlet var hopeStationName2: UILabel!
    @IBOutlet var hopeStartsFrom2: UILabel!
    @IBOutlet var hopeAgency2: UILabel!
    
    @IBOutlet var currentCellBand: UIView!
    var noOfHops        = 0
    var isHopAvailable  = false
    var hopeNameValue   = "Dadar"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(hope1Selected))
        tapGesture1.numberOfTapsRequired = 1
        hope1View.addGestureRecognizer(tapGesture1)
        hope1View.isUserInteractionEnabled = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func hope1Selected(){
    
        guard let indexPath = indexPath else { return }
        print("Tableview Cell \(indexPath.row) ")
    }
}





















