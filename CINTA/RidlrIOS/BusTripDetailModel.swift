//
//  BusTripDetailModel.swift
//  RidlrIOS
//
//  Created by Mayur on 09/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class BusTripDetailModel{
    
    var starttime = ""
    var endtime = ""
    
    var busStops = [BusStopsDetail]()
}

class BusStopsDetail{

    var id              = ""
    var name            = ""
    var arrivaltime     = ""
    var departuretime   = ""
    var lon             = ""
    var lat             = ""
    var platformno      = ""
}
