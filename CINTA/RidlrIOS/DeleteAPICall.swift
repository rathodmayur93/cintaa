//
//  DeleteAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 06/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class DeleteAPICall {

    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    var encodedPassKey = ""
    
    func makeAPICall(fromViewController : NewsFeedViewController, updateId : String, userId : String){
        
        let uuid                = uiFun.readData(key: "UUID")
        let utf8str             = uuid.data(using: String.Encoding.utf8)
        if let base64Encoded    = utf8str?.base64EncodedString()  {
            encodedPassKey      = base64Encoded
        }
        
        let url                 = "\(constant.TIMELINE_URL)DeleteUpdate"    // API url for deleting post
        
        // Parameters for api call
        let params : [String:AnyObject] = ["strUpdateId":updateId as AnyObject,
                                           "lngUserId":userId as AnyObject,
                                           "strGroupId":"g2apps" as AnyObject,
                                           "strIP":"" as AnyObject,
                                           "strCustKey":"f29a3c79212611e3aeb927d5ba248633" as AnyObject,
                                           "passkey" : encodedPassKey as AnyObject,
                                           "addparams":"" as AnyObject ]
        
        print("News Feed url \(url)")
        
        util.sendRequest(url: url, parameters: params, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.deleteAPIResponse(response: jsonResponse)
                print("Successfully News Feed Delete API Response Got.... \(jsonResponse) ")
                return
                
            }else {
                
                print("News Feed Response Delete API Failed....Try Harder Mayur ")
                return
            }
            
        })
        
    }

}
