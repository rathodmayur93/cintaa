//
//  MetroJSONResponses.swift
//  RidlrIOS
//
//  Created by Mayur on 28/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class MetroJSONResponses: UIViewController {

    var metroTokenResponseList  = [MetroTokenJSONModel]()
    let agencyName              = uiFun.readData(key: "ticketingAgency")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func processMetroJSONResponse(response : JSON) -> [MetroTokenJSONModel]{
        
        let responseCode    = String(describing: response["code"])
        if(responseCode == "5000"){
        let dataJSON            = response["data"]
        let bookingDetailJSON : JSON!
        
        // if agency is metroTrip then in response it doesn't contain bookingDetail JSON Object
        if(agencyName == "metroTrip" || agencyName == "STORE_VALUE_PASS"){
          
            bookingDetailJSON   = dataJSON
      
        }else{
            
            bookingDetailJSON   = dataJSON["bookingDetails"]
            print("Metro Trip Count \(bookingDetailJSON.count)")
        }
        
        for i in 0..<bookingDetailJSON.count{
     
            var bookingItemListJSON : JSON
            // if agency is metroTrip then in response it doesn't contain JSON array
            if(agencyName == "metroTrip" || agencyName == "STORE_VALUE_PASS"){
            
                bookingItemListJSON = bookingDetailJSON["bookingItemsList"]
                if(i > 0){
                    break
                }
            
            }else{
            
                bookingItemListJSON = bookingDetailJSON.arrayValue[i]["bookingItemsList"]
            }
            
            
            for j in 0..<bookingItemListJSON.count{
            
                let tokenJSON           = bookingItemListJSON.arrayValue[j]["tokens"]
                let metroTokenJSONModel = MetroTokenJSONModel()
                
                // Storing booking item list response into response
                metroTokenJSONModel.itemType            = String(describing: bookingItemListJSON.arrayValue[j]["itemType"])
                metroTokenJSONModel.companyItemId       = String(describing: bookingItemListJSON.arrayValue[j]["companyItemId"])
                metroTokenJSONModel.bookingItemId       = String(describing: bookingItemListJSON.arrayValue[j]["bookingItemId"])
                metroTokenJSONModel.activeRenewalDate   = String(describing: bookingItemListJSON.arrayValue[j]["activeRenewalDate"])
                metroTokenJSONModel.itemSubType         = String(describing: bookingItemListJSON.arrayValue[j]["itemSubType"])
                metroTokenJSONModel.issuerIdentifier    = String(describing: bookingItemListJSON.arrayValue[j]["issuerIdentifier"])
                metroTokenJSONModel.userType            = String(describing: bookingItemListJSON.arrayValue[j]["userType"])
                metroTokenJSONModel.userDob             = String(describing: bookingItemListJSON.arrayValue[j]["userDob"])
                metroTokenJSONModel.activationDate      = String(describing: bookingItemListJSON.arrayValue[j]["activationDate"])
                metroTokenJSONModel.validityCriteria    = String(describing: bookingItemListJSON.arrayValue[j]["validityCriteria"])
                metroTokenJSONModel.validity            = String(describing: bookingItemListJSON.arrayValue[j]["validity"])
                metroTokenJSONModel.userName            = String(describing: bookingItemListJSON.arrayValue[j]["userName"])
                metroTokenJSONModel.expiryDate          = String(describing: bookingItemListJSON.arrayValue[j]["expiryDate"])
                metroTokenJSONModel.issuerTag           = String(describing: bookingItemListJSON.arrayValue[j]["issuerTag"])
                
                metroTokenJSONModel.companyRefId        = String(describing: bookingItemListJSON.arrayValue[j]["itemAttribute"]["masterQrCodeId"])
                
                metroTokenJSONModel.refTxnId            = String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["refTxnId"])
                metroTokenJSONModel.trips               = String(describing: bookingItemListJSON.arrayValue[j]["itemAttribute"]["trips"])
                
                /************ Storing Data To SharedPref or In Local Storage *********/
//                uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["masterQrCodeId"]), key: "companyReferenceId")
                print("Master QR Code Id \(String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["masterQrCodeId"]))")
                
                let agencyName              = uiFun.readData(key: "ticketingAgency")
                
                switch agencyName {
                case "MetroToken":
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["masterQrCodeId"]), key: "companyReferenceId")
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["itemSubType"]), key: "journeyType")
                    break
                case "metroTrip":
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[j]["itemAttribute"]["trips"]), key: "tripPassRemainingTrip")
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["masterQrCodeId"]), key: "tripCompanyReferenceId")
                    break
                case "STORE_VALUE_PASS":
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[j]["itemAttribute"]["balance"]), key: "balance")
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["masterQrCodeId"]), key: "storeValueCompanyReferenceId")
                    break
                case "Penalty":
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["itemAttribute"]["masterQrCodeId"]), key: "penaltyCompanyReferenceId")
                    break
                default:
                    break
                }
                
                
                // Storing token array into model
                for k in 0..<tokenJSON.count{
                
                    let newMetroTokenModel      = NewMetroTokenModel()
                    
                    newMetroTokenModel.status   = String(describing: tokenJSON.arrayValue[k]["status"])
                    newMetroTokenModel.qrCodeId = String(describing: tokenJSON.arrayValue[k]["qrCodeId"])
                    newMetroTokenModel.type     = String(describing: tokenJSON.arrayValue[k]["type"])
                    newMetroTokenModel.qrCode   = String(describing: tokenJSON.arrayValue[k]["qrCode"])
                    newMetroTokenModel.expiry   = String(describing: tokenJSON.arrayValue[k]["expiry"])
    
                    metroTokenJSONModel.newMetroToken.append(newMetroTokenModel)
                }
                
                // storing trip and store value pass qrCOdeId for status api reference
                let itemSubType = String(describing: bookingItemListJSON.arrayValue[j]["itemSubType"])
                if(itemSubType == "TRIP_PASS"){
                    uiFun.writeData(value: String(describing: tokenJSON.arrayValue[0]["qrCodeId"]), key: "tripPassSlaveQRCode")
                }else if(itemSubType == "STORE_VALUE_PASS"){
                    uiFun.writeData(value: String(describing: tokenJSON.arrayValue[0]["qrCodeId"]), key: "storeValueSlaveQRCode")
                }
             
                let routeJSON           = bookingItemListJSON.arrayValue[j]["bookingItemRoutesList"]
                
                for l in 0..<routeJSON.count{
                
                    let route = Route()
                
                    route.routeId               = String(describing: routeJSON.arrayValue[l]["routeId"])
                    route.routeName             = String(describing: routeJSON.arrayValue[l]["routeName"])
                    route.sourceStageName       = String(describing: routeJSON.arrayValue[l]["sourceStageName"])
                    route.destinationName       = String(describing: routeJSON.arrayValue[l]["destinationName"])
                    route.destinationStageId    = String(describing: routeJSON.arrayValue[l]["destinationStageId"])
                    route.bookingItemRouteId    = String(describing: routeJSON.arrayValue[l]["bookingItemRouteId"])
                    route.sourceId              = String(describing: routeJSON.arrayValue[l]["sourceId"])
                    route.sourceStageId         = String(describing: routeJSON.arrayValue[l]["sourceStageId"])
                    route.destinationId         = String(describing: routeJSON.arrayValue[l]["destinationId"])
                    route.sourceName            = String(describing: routeJSON.arrayValue[l]["sourceName"])
                    route.destinationStageName  = String(describing: routeJSON.arrayValue[l]["destinationStageName"])
                    
                    metroTokenJSONModel.routeDetail.append(route)
                }
                
                let fareDetail = Fare()
                
                fareDetail.baseAmount           = String(describing: bookingItemListJSON.arrayValue[j]["baseAmount"])
                fareDetail.totalAmount          = String(describing: bookingItemListJSON.arrayValue[j]["totalAmount"])
                fareDetail.totalSurcharge       = String(describing: bookingItemListJSON.arrayValue[j]["totalSurcharge"])
                fareDetail.totalTax             = String(describing: bookingItemListJSON.arrayValue[j]["totalTax"])
                fareDetail.totalDiscount        = String(describing: bookingItemListJSON.arrayValue[j]["totalDiscount"])
                fareDetail.totalConcession      = String(describing: bookingItemListJSON.arrayValue[j]["totalConcession"])
                
                metroTokenJSONModel.fare.append(fareDetail)
                metroTokenResponseList.append(metroTokenJSONModel)
                
            }
        }
        
        if(agencyName == "Penalty"){
        
            let oldQRCodeId         = metroTokenResponseList[0].refTxnId
            let newQRCodeId         = metroTokenResponseList[0].newMetroToken[0].qrCodeId
            let qrCode              = metroTokenResponseList[0].newMetroToken[0].qrCode
            let status              = metroTokenResponseList[0].newMetroToken[0].status
            let type                = metroTokenResponseList[0].newMetroToken[0].type
            let expiry              = metroTokenResponseList[0].newMetroToken[0].expiry
            
            let source              = metroTokenResponseList[0].routeDetail[0].sourceName
            let destination         = metroTokenResponseList[0].routeDetail[0].destinationName
            let sourceId            = metroTokenResponseList[0].routeDetail[0].sourceId
            let destinationId       = metroTokenResponseList[0].routeDetail[0].destinationId
            let itemSubType         = metroTokenResponseList[0].itemSubType
            
            DBManagerToken.shared.updateTokenData(withID: oldQRCodeId, newQRcodeId: newQRCodeId, qrCode: qrCode, status: status, type: type, expiry: expiry, source: source, destination: destination, sourceId: sourceId, destinationId: destinationId,itemSubType: itemSubType)
           
//            DBManager.shared.updateBookingData(withID: metroTokenResponseList[0].companyRefId, source: source, destination: destination, sourceId: sourceId, destinationId: destinationId, activationDate: metroTokenResponseList[0].activationDate, expiryDate: metroTokenResponseList[0].expiryDate, totalAmount: metroTokenResponseList[0].fare[0].totalAmount, itemSubType: metroTokenResponseList[0].itemSubType)
            
            
            let dbManagerData   = DBManagerToken.shared.fetchBookingItems(itemSubType: "SINGLE_JOURNEY")
            
//            //let bookingId = DBManagerToken.shared.fetchBookingId(qrCodeId: oldQRCodeId)
//            print("Booking id is \(bookingId)")
//            print("Updated QR Code into database successfully ")
//            
//            let sourceStation       = metroTokenResponseList[0].routeDetail[0].sourceName
//            let destinationStation  = metroTokenResponseList[0].routeDetail[0].destinationName
//            let activationDatae     = metroTokenResponseList[0].activationDate
//            let expiryDate          = metroTokenResponseList[0].expiryDate
//            let totalAmount         = metroTokenResponseList[0].fare[0].totalAmount
//            let itemSubType         = metroTokenResponseList[0].itemSubType
//            
//            DBManager.shared.updateBookingData(withID: bookingId, source: sourceStation, destination: destinationStation, sourceId: sourceId, destinationId: destinationId, activationDate: activationDatae, expiryDate: expiryDate, totalAmount: totalAmount, itemSubType: itemSubType)
            
        
        }else{
        
            // Inserting response values into the bookingItemList Database
            DBManager.shared.insertBookingDataData(bookingItemList: metroTokenResponseList)
        
            // Inserting response values into the tokenData Database
            DBManagerToken.shared.insertTokenData(bookingItemList: metroTokenResponseList)
        }
        
        
        return metroTokenResponseList
        }else{
            uiFun.showAlert(title: "", message: "We are facing some issue. Please Try Again later.", logMessage: "Card Token Response Fail", fromController: self)
            
            return metroTokenResponseList
        }
    }

}
