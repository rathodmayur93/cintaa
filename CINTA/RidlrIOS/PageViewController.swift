//
//  PageViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 24/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource {

    var firstPage   = PageItemController()
    var secondPage  = PageItemControllerTwo()
    
    var pageControl : UIPageControl = UIPageControl()
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        // Tutorial Screen parent class, contains the names of view controller which its gonna display
        return [self.newPageNameViewController(pageName: "PageItemController"), // First Tutorial Screen View Controller
                self.newPageNameViewController(pageName: "PageItemControllerTwo")] // Second Tutorial Screen View Controller
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        
        // Intializing page view controller
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        // Page Control Appearance and its colors
        pageControl = UIPageControl.appearance(whenContainedInInstancesOf: [PageViewController.self])
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.black

    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Page Control Appearance and its colors
        pageControl = UIPageControl.appearance(whenContainedInInstancesOf: [PageViewController.self])
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.black
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func newPageNameViewController(pageName: String) -> UIViewController {
        // Return the ViewContoller From StoryBoard
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(pageName)")
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1 // Return the index of previous view controller
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1     // return the index of the next view controller
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    

    public func presentationCount(for pageViewController: UIPageViewController) -> Int{
        // Total number of view controller count
        return orderedViewControllers.count
    }
    
    
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int{
    
        // Shows the first view controller
        guard let firstViewController = viewControllers?.first,
            let firstViewControllerIndex = orderedViewControllers.index(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
}
