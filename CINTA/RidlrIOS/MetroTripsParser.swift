//
//  MetroTripsParser.swift
//  RidlrIOS
//
//  Created by Mayur on 18/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var metroTripsDataList = [MetroTripModel]()
class MetroTripsParser{

    let uiFun = UiUtillity()
    
    func parseMetroTripsCSVToList(){
        
        let queueName = DispatchQueue(label: "trip.metro.processing")
        
        queueName.async {
            
            let csvContent = self.uiFun.readFromCSV(fileName: "MUM Metro_trips")
            let csvNewLineContent = csvContent.components(separatedBy: .newlines)
            let delimiter = ","
            
            let lines:[String] = csvNewLineContent
            
            
            let metroTripModel = MetroTripModel()
            
            for line in lines {
            
                var inStopId            = false
                var inPattern           = false
                var inTrips             = false
                
                var stopValuePattern = 0
                var metroData           = line.components(separatedBy: delimiter)
                
                if(metroData[0] != ""){
                    
                    let metroTripParseModel = MetroTripBaseModel()
                    metroTripParseModel.tripId = metroData[0]
                    
                    var i = 0
                    while(i < metroData.count){
                    
                        if(metroData[i] == "stopid" ){
                            
                            inStopId = true
                        }else if(metroData[i] == "patterns"){
                            
                            inStopId = false
                            inPattern = true
                        }else if(metroData[i] == "trips"){
                        
                            inPattern = false
                            inTrips = true
                        }else if(inStopId){
                            
                            let metroStopId         = MetroStopsId()
                            metroStopId.stopId      = metroData[i]
                            metroTripParseModel.stopModel.append(metroStopId)
                            
                        }else if(inPattern){
                        
                            
                            let metroPatternModel   = MetroPatternModel()
                            metroPatternModel.stopInterval = metroData[i]
                            metroTripParseModel.stopModel[stopValuePattern].pattern.append(metroPatternModel)
                            stopValuePattern += 1
                            
                        }else if(inTrips){
                        
                            let metroTimingModel    = MetroTimingModel()
                            let tripRawData = metroData[i]
                            var tripData = tripRawData.components(separatedBy: "|")
                            
                            if(tripData.count > 1){
                                
                                metroTimingModel.startingTime = tripData[0]
                                metroTimingModel.calendarId   = tripData[2]
                            
                                metroTripParseModel.metroTiming.append(metroTimingModel)
                            }
                            
                        }
                        i += 1
                    }
                    metroTripModel.metroBaseModel.append(metroTripParseModel)
                    metroTripsDataList.append(metroTripModel)
                }
            }
        }
    }
}
