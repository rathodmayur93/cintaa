//
//  AddMoneyStatusViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 25/07/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class AddMoneyStatusViewController: UIViewController {

    @IBOutlet var toolbarHeading        : UILabel!
    @IBOutlet var statusImageView       : UIImageView!
    @IBOutlet var addMoneyStatus        : UILabel!
    @IBOutlet var addMoneyDesc          : UILabel!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI(){
        
        toolbarHeading.text = "Pay By \(paymentType)"
        if(addMoneyStatusOfTransaction == "FAILED"){
            statusImageView.image   = UIImage(named: "ic_indicator_cancel")
            addMoneyDesc.text       = "Add money failed. Please try again"
            addMoneyStatus.text     = "Add money failed"
        }else{
            addMoneyDesc.text = "\(amountToAddValue) has been added successfullly into your wallet."
        }
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
    }
    
}
