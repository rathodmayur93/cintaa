//
//  FetchBusRouteDetailAPICall.swift
//  RidlrIOS
//
//  Created by Mayur on 10/02/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchBusRouteDetailAPICall{
    
    var util = Utillity()
    var constant = Constants()
    var uiFun = UiUtillity()
    
    
    func makeAPICall(fromViewController : TImeTableTimingViewController, jsonString : String, cityId : String, modeId : String){
        
        let url = "\(constant.TIMETABLE_BASE_URL)list/trip/schedule/cityid/\(cityIdFromData)/modeid/\(modeId)/userid/-9/deviceid/90fcd69416b44ac6"
        
        print("Timetable Trips Detail Fetching URL \(url)")
        
        util.APICall(apiUrl: url, jsonString: jsonString, requestType: "POST", header: false){result, error in
            
            if let jsonResponse = result{
                
                fromViewController.FetchBusRouteTripResponse(response: jsonResponse)
                print("Successfully Timetable Bus Trips Details Done.... \(jsonResponse) ")
                return
                
            }else {
                
                print("Timetable Trips Bus Details Failed....Try Harder Mayur ")
                return
            }
            
        }
        
    }
}
