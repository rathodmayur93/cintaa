//
//  PaymentReesponseModel.swift
//  RidlrIOS
//
//  Created by Mayur on 21/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PaymentReesponseModel : NSObject{

    var code        = ""
    var status      = ""
    var data        = [PaymentDataWebView]()

}

class PaymentDataWebView{

    var order_no        = ""
    var isPaymentFinal  = ""
    var isPGFlow        = ""
    var paymentDetails  = [PaymentDetailsResponseWebView]()
    var bookingDetails  = [BookingDetailResponse]()
}

class PaymentDetailsResponseWebView{

    var paymentGateway    = ""
    var paymentMode       = ""
    var responseType      = ""
    var message           = ""
    var amount            = ""
    var paymentBookingId  = ""
    var pgTransactionId   = ""
    var status            = ""
    var bookingIdWebView  = ""
    var paymentMethod     = ""
    var bookingId         = ""
    var cardNumber        = ""
    var transactionStatus = ""
}

class BookingDetailResponse{

    var bookingId               = ""
    var paymentStatus           = ""
    var paymentStatusMessage    = ""
    var bookingDateTime         = ""
    var unpaidAmount            = ""
    var itemSubType             = ""
    var companyReferenceId      = ""

    var qrCode                  = [QRCodeToken]()
}

class QRCodeToken {

    var tokenStatus             = ""
    var qrCodeId                = ""
    var type                    = ""
    var qrCode                  = ""
    var expiry                  = ""
    
//    init(tokenStatus : String, qrCodeId : String, type : String, qrCode : String, expiry : String) {
//    
//        self.tokenStatus    = tokenStatus
//        self.qrCodeId       = qrCodeId
//        self.type           = type
//        self.qrCode         = qrCode
//        self.expiry         = expiry
//    }
//    
//    required convenience init?(coder decoder : NSCoder){
//        
//        let tokenStatus   = decoder.decodeObject(forKey: "tokenStatus") as? String
//        let qrCodeId      = decoder.decodeObject(forKey: "qrCodeId") as? String
//        let type          = decoder.decodeObject(forKey: "type") as? String
//        let qrCode        = decoder.decodeObject(forKey: "qrCode") as? String
//        let expiry        = decoder.decodeObject(forKey: "expiry") as? String
//        
//        self.init(tokenStatus : tokenStatus!, qrCodeId : qrCodeId!, type : type!, qrCode : qrCode!, expiry : expiry!)
//    }
//    
//    func encode(with coder: NSCoder) {
//        
//        coder.encode(self.tokenStatus, forKey: "tokenStatus")
//        coder.encode(self.qrCodeId, forKey: "qrCodeId")
//        coder.encode(self.type, forKey: "type")
//        coder.encode(self.qrCode, forKey: "qrCode")
//        coder.encode(self.expiry, forKey: "expiry")
//    }
}

class QRCodeOffline : NSObject, NSCoding{

    var tokenStatus             = ""
    var qrCodeId                = ""
    var type                    = ""
    var qrCode                  = ""
    var expiry                  = ""
    
    init(tokenStatus : String, qrCodeId : String, type : String, qrCode : String, expiry : String) {
        
        self.tokenStatus    = tokenStatus
        self.qrCodeId       = qrCodeId
        self.type           = type
        self.qrCode         = qrCode
        self.expiry         = expiry
    }
    
    required convenience init?(coder decoder : NSCoder){
        
        let tokenStatus   = decoder.decodeObject(forKey: "tokenStatus") as? String
        let qrCodeId      = decoder.decodeObject(forKey: "qrCodeId") as? String
        let type          = decoder.decodeObject(forKey: "type") as? String
        let qrCode        = decoder.decodeObject(forKey: "qrCode") as? String
        let expiry        = decoder.decodeObject(forKey: "expiry") as? String
        
        self.init(tokenStatus : tokenStatus!, qrCodeId : qrCodeId!, type : type!, qrCode : qrCode!, expiry : expiry!)
    }
    
    func encode(with coder: NSCoder) {
        
        coder.encode(self.tokenStatus, forKey: "tokenStatus")
        coder.encode(self.qrCodeId, forKey: "qrCodeId")
        coder.encode(self.type, forKey: "type")
        coder.encode(self.qrCode, forKey: "qrCode")
        coder.encode(self.expiry, forKey: "expiry")
    }
}
