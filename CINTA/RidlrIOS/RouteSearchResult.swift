//
//  RouteSearchResult.swift
//  RidlrIOS
//
//  Created by Mayur on 29/12/16.
//  Copyright © 2016 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var routeSearchResultList   = [RouteSearchModel]()
var walkOnlyList            = [WalkOnly]()
var autoCabList             = [WalkOnly]()
var routeList               = [WalkOnly]()
var driveList               = [DriveModel]()

var plannerTripIndex = 0
var fromWhichScreen  = ""

var plannerSelectedSegmentIndex = 0

class RouteSearchResult: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let uiFun = UiUtillity()
    let ridlrColors = RidlrColors()
    
    @IBOutlet var backButton                : UIButton!
    @IBOutlet var nowButton                 : UIButton!
    @IBOutlet var changeButton              : UIButton!
    
    @IBOutlet var startingStationLabel      : UILabel!
    @IBOutlet var destinationLabel          : UILabel!
    @IBOutlet var timeLabel                 : UILabel!
    
    @IBOutlet var cabLabel                  : UILabel!
    @IBOutlet var walkLabel                 : UILabel!
    @IBOutlet var driveLabel                : UILabel!
    
    @IBOutlet var tabIndicatorView          : UIView!
    
    @IBOutlet var tableView                 : UITableView!
    @IBOutlet var routeSegment              : UISegmentedControl!
    
    var legsList                            = [Legs]()
    var j                                   = 0
    var legsCount                           = 0
    var routeMode                           = "walkOnly"
    var shouldClearTable                    = false
    
    var hour                                : Int?
    var minutes                             : Int?
    var isTableCleared                      = false
    var shouldClearWalkData                 = false
    
    var shouldIntializeCellContent          = true
    var shouldAddToCellContentView          = true
    
    let spinningActivityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let container: UIView                                  = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setting tableview style to none and backgroud color to gray
        tableView.separatorStyle            = UITableViewCellSeparatorStyle.none
        tableView.backgroundColor           = uiFun.hexStringToUIColor(hex: ridlrColors.light_grey_50)
        
        routeSegment.selectedSegmentIndex   = 0
        
        startingStationLabel.text           = myLocation
        destinationLabel.text               = plannerDestinationStation
        
        if(timeChange){
            // If user have change the time
            timeChangeEvent()
            walkOnlyList.removeAll()
            autoCabList.removeAll()
            routeList.removeAll()
            
            // API call for route search
            let apiCall = RouteSearchResultAPIViewController()
            apiCall.makeAPICall(fromViewController: self)
            timeChange = false
        }else if(routeList.count == 0){
            // when user taps on now button
            nowButtonTappedEvent()
        }
        nowChangeButtonBackground() // when user taps on change button
        
        print("List Values are Walk oNly \(walkOnlyList) Auto Cab List \(autoCabList)")
        uiFun.createCleverTapEvent(message: "FIND A ROUTE SEARCH RESULT SCREEN")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func routeSegmentAction(_ sender: AnyObject) {
        
        switch routeSegment.selectedSegmentIndex {
            
        // Different segment selection either auto_cab or walk or drive
        case 0:
            // walk only mode
            routeMode                       = "walkOnly"
            plannerSelectedSegmentIndex     = 0
            shouldClearTable                = true
            shouldClearWalkData             = true
            var refreshRowIndex             = 0
            
            while(refreshRowIndex < 3){
                
                let indexPath = IndexPath(item: refreshRowIndex, section: 0)
                if let visibleIndexPaths = tableView.indexPathsForVisibleRows?.index(of: indexPath as IndexPath) {
                    if visibleIndexPaths != NSNotFound {
    
                    }
                }
                refreshRowIndex += 1
            }
            tableView.reloadData()
            j = 0
           break;
        case 1:
            // autoAndCab mode
            routeMode = "autoAndCab"
            j = 0
            plannerSelectedSegmentIndex     = 1
            var refreshRowIndex             = 0
            
            while(refreshRowIndex < 3){
                let indexPath = IndexPath(item: refreshRowIndex, section: 0)
                if let visibleIndexPaths = tableView.indexPathsForVisibleRows?.index(of: indexPath as IndexPath) {
                    if visibleIndexPaths != NSNotFound {
                    }
                }
                refreshRowIndex += 1
            }
            
            tableView.reloadData()
            isTableCleared              = false
            shouldClearTable            = false
            shouldIntializeCellContent  = false
            shouldAddToCellContentView  = true
            break
        case 2:
            // Drive mode
            routeMode = "drive"
            j = 0
            plannerSelectedSegmentIndex     = 2
            driveList.removeAll()
            uiFun.showIndicatorLoader()
            let driveAPICall                = RouteSearchDriveAPI()
            driveAPICall.makeAPICall(fromViewController: self)
            isTableCleared                  = false
            shouldClearTable                = false
            shouldIntializeCellContent      = false
            print("Drive Selected")
            break
        default:
            break
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // Tableview no of rows
        if(plannerSelectedSegmentIndex == 2){
            return driveList.count
        }else{
            return 3
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
     
        print("Number of time s table view getting loop throught \(indexPath.row)")
        // Initialzing cell
        let cell =  tableView.dequeueReusableCell(withIdentifier: "Cell") as! CabAutoTableViewCell
        print("Cell Value is \(cell)")
        
        // Assigning current selected mode list
        if(routeMode == "walkOnly"){
            routeList = walkOnlyList
        }else if(routeMode == "autoAndCab"){
            routeList = autoCabList
        }
        
        if(routeList.count == 0){
            return cell
        }
        
            var j = 0
            if(routeMode != "drive"){
                j                           = indexPath.row
                cell.durationLabel.isHidden = false
            }
            var i = 0
            print("Route List Count \(routeList.count) && \(j)")
            
            let cellView : UIView   = UIView()
            cellView.frame          = CGRect(x: 8, y: 8, width: Int(cell.frame.size.width), height: 36)
            
            var newXCord            = 0
            let newWidth            = 24
        
            while (i < routeList[j].legs.count){
                // fetchig the duration of leg
                let duration   = routeList[j].legs[i].duration
                
                print("Number of time Value of i \(i) and j is \(j)")
                
                cell.durationLabel.text = "\(uiFun.convertTimestampToHourMinute(arrivalTime: routeList[indexPath.row].starttimestamp)) - \(uiFun.convertTimestampToHourMinute(arrivalTime: routeList[indexPath.row].endtimestamp))"
                
                    let mode1Image : UIImageView    = UIImageView()
                    let mode1Label : UILabel        = UILabel()
                
            if(routeMode == "walkOnly"){
                
                print("Travel Mode Table View \(routeList[indexPath.row].legs[i].mode)")
                // assigning the tag to each view and remove it from superview, when segment gets change
                if(timeChange || shouldClearWalkData){
                    var m = 0
                    
                    while(m < walkOnlyList[j].legs.count){
                        
                        cell.contentView.viewWithTag(203)?.removeFromSuperview()
                        
                        cell.contentView.viewWithTag(221)?.removeFromSuperview()
                        cell.contentView.viewWithTag(222)?.removeFromSuperview()
                        
                        cell.contentView.viewWithTag(320)?.removeFromSuperview()
                        cell.contentView.viewWithTag(321)?.removeFromSuperview()
                        cell.contentView.viewWithTag(322)?.removeFromSuperview()
                        m += 1
                    }
                }
                    // creating mode image and mode label text
                    mode1Image.frame    = CGRect(x: newXCord , y: 0, width: newWidth, height: 24)
                    mode1Image.tag      = 101
                    cellView.addSubview(mode1Image)
                
                    mode1Label.frame    = CGRect(x: (newXCord + newWidth), y: 4, width: 128, height: 24)
                    mode1Label.tag      = 102
                    mode1Label.font     = UIFont.boldSystemFont(ofSize: 12.0)
                
                if(routeList[j].legs[i].mode == "WALK"){
                    // if leg mode is walk assign image and label text
                    mode1Image.image    = UIImage(named: "ic_indicator_mode_walk_holo")
                    mode1Label.text     = "\(uiFun.convertTimestampToMinutes(arrivalTime: duration))"
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "RAIL"){
                    // if leg mode is rail assign image and label text
                    mode1Image.image = UIImage(named: "ic_indicator_mode_train_holo")
                    mode1Label.text = routeList[j].legs[i].routeLongName
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "BUS"){
                    // if leg mode is bus assign image and label text
                    mode1Image.image = UIImage(named: "ic_indicator_mode_bus_holo")
                    mode1Label.text = routeList[j].legs[i].routeShortName
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "AUTO"){
                    // if leg mode is auto assign image and label text
                    mode1Image.image = UIImage(named: "ic_indicator_mode_rickshaw_holo")
                    mode1Label.text = routeList[j].legs[i].routeLongName
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "METRO"){
                    // if leg mode is metro assign image and label text
                    mode1Image.image = UIImage(named: "ic_indicator_mode_metro_holo")
                    mode1Label.text = routeList[j].legs[i].routeLongName
                    mode1Label.sizeToFit()
                }
                
                }else if(routeMode == "drive"){
                    // routeMode is drive
                    cell.durationLabel.isHidden                         = true
                    cell.contentView.viewWithTag(103)?.isHidden         = true
                    if(!isTableCleared){
                        var m = 0
                        
                        while(m < walkOnlyList[j].legs.count){
                            cell.contentView.viewWithTag(103)?.removeFromSuperview()
                            cell.contentView.viewWithTag(203)?.removeFromSuperview()
                          
                            cell.contentView.viewWithTag(221)?.removeFromSuperview()
                            cell.contentView.viewWithTag(222)?.removeFromSuperview()
                            
                            cell.contentView.viewWithTag(101)?.removeFromSuperview()
                            cell.contentView.viewWithTag(102)?.removeFromSuperview()
                            
                            m += 1
                        }
                    }
                
                    // recommended route properties and its creation
                    let recommendedRouteLabel : UILabel = UILabel()
                    recommendedRouteLabel.frame         = CGRect(x: 8, y: 8, width: self.view.frame.size.width, height: 24)
                    recommendedRouteLabel.text          = "Recommended Route"
                    recommendedRouteLabel.textColor     = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
                    recommendedRouteLabel.font          = UIFont.systemFont(ofSize: 12.0)
                    recommendedRouteLabel.tag           = 320
                    cell.contentView.addSubview(recommendedRouteLabel)
                
                    // routeName Label properties and its creation
                    let routeNameLabel : UILabel        = UILabel()
                    routeNameLabel.frame                = CGRect(x: 8, y: 32, width: self.view.frame.size.width, height: 24)
                    routeNameLabel.text                 = driveList[indexPath.row].summary
                    routeNameLabel.textColor            = uiFun.hexStringToUIColor(hex: ridlrColors.color_gray_90)
                    routeNameLabel.font                 = UIFont.systemFont(ofSize: 16.0)
                    routeNameLabel.tag                  = 321
                    cell.contentView.addSubview(routeNameLabel)
                    routeNameLabel.sizeToFit()
                
                    // timelabel properties and its creation
                    let timeLabel : UILabel             = UILabel()
                    timeLabel.frame                     = CGRect(x: 8, y: 56, width: self.view.frame.size.width, height: 24)
                    timeLabel.text                      = "\(driveList[indexPath.row].tte) ( \(driveList[indexPath.row].distance) )"
                    timeLabel.font                      = UIFont.boldSystemFont(ofSize: 14.0)
                    timeLabel.textColor                 = UIColor.black
                    timeLabel.tag                       = 322
                    cell.contentView.addSubview(timeLabel)
                    
                }else{
                
                    // auto and cab mode (almost same as walkOnly)
                    print("Travel Mode Table View \(routeList[indexPath.row].legs[i].mode)")
                    print("Value of i and j is \(i) & \(j)")
   
                    if(!isTableCleared){
                        var m = 0
                    while(m < autoCabList[j].legs.count){
                        cell.contentView.viewWithTag(101)?.removeFromSuperview()
                        cell.contentView.viewWithTag(102)?.removeFromSuperview()
                        cell.contentView.viewWithTag(103)?.removeFromSuperview()
                        
                        cell.contentView.viewWithTag(320)?.removeFromSuperview()
                        cell.contentView.viewWithTag(321)?.removeFromSuperview()
                        cell.contentView.viewWithTag(322)?.removeFromSuperview()
                        m += 1
                    }
                    
                }
                    cell.durationLabel.text = "\(uiFun.convertTimestampToHourMinute(arrivalTime: routeList[indexPath.row].starttimestamp)) - \(uiFun.convertTimestampToHourMinute(arrivalTime: routeList[indexPath.row].endtimestamp)) "

                    mode1Image.frame = CGRect(x: newXCord , y: 0, width: newWidth, height: 24)
                    mode1Image.tag   = 221
                    mode1Image.image = UIImage(named: "ic_indicator_mode_mono_holo")
                    cellView.addSubview(mode1Image)
                
                    mode1Label.text = "\(uiFun.convertTimestampToMinutes(arrivalTime: duration))"
                    mode1Label.tag  = 222
                    mode1Label.frame = CGRect(x: (newXCord + newWidth), y: 4, width: 128, height: 24)
                    mode1Label.font = UIFont.boldSystemFont(ofSize: 12.0)
                
                if(routeList[j].legs[i].mode == "WALK"){
                    
                    mode1Image.image = UIImage(named: "ic_indicator_mode_walk_holo")
                    mode1Label.text = "\((duration)) mins"
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "RAIL"){
                    
                    mode1Image.image = UIImage(named: "ic_indicator_mode_train_holo")
                    mode1Label.text = routeList[j].legs[i].routeLongName
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "BUS"){
                    
                    mode1Image.image = UIImage(named: "ic_indicator_mode_bus_holo")
                    mode1Label.text = routeList[j].legs[i].routeShortName
                    mode1Label.sizeToFit()
                }
                if(routeList[j].legs[i].mode == "AUTO"){
                    
                    mode1Image.image = UIImage(named: "ic_indicator_mode_rickshaw_holo")
                    mode1Label.text = "\(routeList[j].legs[i].distance) KM"
                    print("Hello at \(j) and \(i)")

                    mode1Label.sizeToFit()
                }
                
            }
            if(routeMode != "drive"){
                // route mode is drive
                
                let arrowImage : UIImageView = UIImageView()
                arrowImage.frame = CGRect(x: (Int(mode1Label.frame.origin.x + mode1Label.frame.size.width)) , y: 0, width: 24, height: 24)
                if(routeMode == "walkOnly"){
                    arrowImage.tag      = 103
                }else{
                    arrowImage.tag      = 203
                }
                arrowImage.image = UIImage(named: "ic_indicator_mode_seprator_holo")
                
                // Horizontal scroll creation
                let horizontalScroll : UIScrollView = UIScrollView()
                horizontalScroll.frame              = CGRect(x: 4, y: 42, width: self.view.frame.size.width, height: 36)
                horizontalScroll.contentSize        = CGSize(width: (150 * routeList[j].legs.count), height: 36)
                
                horizontalScroll.showsHorizontalScrollIndicator = true
                horizontalScroll.alwaysBounceVertical           = false
                horizontalScroll.bounces                        = true
                horizontalScroll.isScrollEnabled                = true
                horizontalScroll.alwaysBounceHorizontal         = true
                horizontalScroll.bouncesZoom                    = true
                horizontalScroll.delaysContentTouches           = true
                horizontalScroll.canCancelContentTouches        = true
                
                cellView.addSubview(mode1Image)
                cellView.addSubview(mode1Label)
                print("Horizontal Scroll containing label \(mode1Label.text)")
                cellView.addSubview(arrowImage)
                horizontalScroll.addSubview(cellView)
                cell.contentView.addSubview(horizontalScroll)
                
                newXCord = Int(arrowImage.frame.origin.x) + Int(arrowImage.frame.size.width) + 8
            }
                i += 1
            }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        plannerTripIndex = indexPath.row
        // when user clicks on any one row
        if(plannerSelectedSegmentIndex == 2){
            uiFun.navigateToScreem(identifierName: "DriveDetailViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "RouteSearchResultDetailViewController", fromController: self)
        }
    }
    
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            // creating layout of row
            cell.contentView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.newsFeed_bg_color)
            let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y:8, width: self.view.frame.width, height: 170))
            whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
            cell.contentView.addSubview(whiteRoundedView)
            cell.contentView.sendSubview(toBack: whiteRoundedView)
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92.0
    }
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        // when user taps on close action button
        navigateToPlannerResult = false
        myLocation                = "My Location"
        plannerDestinationStation = "Destination"
        
        walkOnlyList.removeAll()
        autoCabList.removeAll()
        routeMode.removeAll()
        uiFun.navigateToScreem(identifierName: "FindARouteViewController", fromController: self)
    }
    @IBAction func nowButtonTappedEvent(_ sender: Any) {
        // now button tap action
        nowButtonTappedEvent()
    }
    
    @IBAction func changeButtonTappedEvent(_ sender: Any) {
        // change button action
        fromWhichScreen = "planner"
        uiFun.navigateToScreem(identifierName: "DateTimePickerViewController", fromController: self)
    }
    
    func nowChangeButtonBackground(){
        
        // now button background
        nowButton.layer.cornerRadius = 5
        nowButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        nowButton.layer.borderWidth = 1
        nowButton.layer.borderColor = UIColor.white.cgColor
        nowButton.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor), for: .normal)
        
        // change button background
        changeButton.layer.cornerRadius = 5
        changeButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        changeButton.layer.borderWidth = 1
        changeButton.layer.borderColor = UIColor.white.cgColor
        changeButton.setTitleColor(UIColor.white, for: .normal)
    }
    
    func nowButtonTappedEvent(){
        // when user taps on now button event
        let date            = NSDate()
        let calendar        = NSCalendar.current
        
        var minuteString    = ""
        
        hour                = calendar.component(.hour, from: date as Date)
        minutes             = calendar.component(.minute  , from: date as Date)
        
        // fetching current time
        if(String(describing : hour!).characters.count == 1){
            hour = Int("0\(hour!)")
        }
        if(String(describing : minutes!).characters.count == 1){
            minuteString = "0\(minutes!)"
        }else{
            minuteString = "\(minutes!)"
        }
        
        // if hour is grater than 12
        if(hour! > 12){
            let actualHour = Int(hour!) - 12
            timeLabel.text = "Today, \(actualHour):\(minuteString) PM"
        }else{
            timeLabel.text = "Today, \(hour!):\(minuteString) AM"
        }
        
        if(routeList.count == 0 ){
            let apiCall = RouteSearchResultAPIViewController()
            apiCall.makeAPICall(fromViewController: self)
        }
    }
    
    func timeChangeEvent(){
        // when user change the time from time picker
        if(timeChange){
            
            var timeChangeMinutesString = ""
            print("Time has been change in TimeTableTimingViewcontroller")
            
            // now button background
            nowButton.layer.cornerRadius = 5
            nowButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
            nowButton.layer.borderWidth = 1
            nowButton.layer.borderColor = UIColor.white.cgColor
            nowButton.setTitleColor(UIColor.white, for: .normal)
            
            // change button background
            changeButton.layer.cornerRadius = 5
            changeButton.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
            changeButton.layer.borderWidth = 1
            changeButton.layer.borderColor = UIColor.white.cgColor
            changeButton.setTitleColor(uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor), for: .normal)
            
            // if minute character is one
            if(String(describing : timeChangeMinute!).characters.count == 1){
                timeChangeMinutesString = "0\(timeChangeMinute!)"
            }else{
                timeChangeMinutesString = "\(timeChangeMinute!)"
            }
            timeLabel.text = "\(timeChangeDate), \(timingLabelValue!):\(timeChangeMinutesString) \(timeChangeMode!)"
        }
    }
    
    
    func routeSearchDriveAPIResponse(jsonResult : JSON){
        // processing Drive api response
        self.uiFun.hideIndicatorLoader()
        if(String(describing: jsonResult["codedesc"]) == "Success"){
        
            let valueJSON                       =   jsonResult["value"]
            let skinnyRouteJSON                 =   valueJSON["skinnyRoutes"]
            
            var j = 0
            while(j < skinnyRouteJSON.count){
            
                let driveJSON                   =   skinnyRouteJSON.arrayValue[j]
                
                let driveModel  = DriveModel()
                driveModel.distance             =  String(describing: driveJSON["distance"])
                driveModel.route                =  String(describing: driveJSON["route"])
                driveModel.summary              =  String(describing: driveJSON["summary"])
                driveModel.tte                  =  String(describing: driveJSON["tte"])
                
                driveModel.destination          =  String(describing: valueJSON["destination"])
                driveModel.destinationCoords    =  String(describing: valueJSON["destinationCoords"])
                driveModel.source               =  String(describing: valueJSON["source"])
                driveModel.sourceCoords         =  String(describing: valueJSON["sourceCoords"])
                
            
                let roadPropertiesJSON          = driveJSON["roadProperties"]
                var i = 0
                while (i < roadPropertiesJSON.count){
            
                    let roadProperties          = RoadProperties()
                
                    roadProperties.color            = String(describing: roadPropertiesJSON.arrayValue[i]["color"])
                    roadProperties.legend           = String(describing: roadPropertiesJSON.arrayValue[i]["legend"])
                    roadProperties.roadAreaName     = String(describing: roadPropertiesJSON.arrayValue[i]["roadAreaName"])
                    roadProperties.speedKPH         = String(describing: roadPropertiesJSON.arrayValue[i]["speedKPH"])
                    roadProperties.time             = String(describing: roadPropertiesJSON.arrayValue[i]["time"])
                
                    driveModel.roadProperties.append(roadProperties)
                    i += 1
            }
                driveList.append(driveModel)
                j += 1
        }
            tableView.reloadData()
            var refreshRowIndex = 0
            while(refreshRowIndex < skinnyRouteJSON.count){
                let indexPath = IndexPath(item: refreshRowIndex, section: 0)
                if let visibleIndexPaths = tableView.indexPathsForVisibleRows?.index(of: indexPath as IndexPath) {
                    if visibleIndexPaths != NSNotFound {
                    }
                }
                refreshRowIndex += 1
            }
        
    }else{
            routeSegment.selectedSegmentIndex = 0
            uiFun.showAlert(title: "", message: String(describing: jsonResult["value"]), logMessage: "Failed In Fetching Drive Data", fromController: self)
            
        }
    }
        
        
    
    func routeSearchAPIResponse(josnResult : JSON){
        // processing route search api response
        if(josnResult["statusmessage"] == "NO_ROUTE_FOUND_IN_OTP"){
            // show alert
            uiFun.showAlert(title: "Notification", message: "Ooops..!! Looks like there is not route for this source and destination", logMessage: "Planner no route found", fromController: self)
            return
        }
        
        var i = 0
        
        let planJsonAraay   = josnResult["plans"]
        print("Route Search Plan JSON Array Count \(planJsonAraay.count)")
        var requireMode     = ""
        
        while(i < planJsonAraay.count){
            
            var j = 0
            
            if (i == 0){
                requireMode = "walkOnly"
            }else if(i == 1){
                requireMode = "autoAndCab"
            }
            let walkOnlyArray = planJsonAraay.arrayValue[i]
            let walkOnlyObject = walkOnlyArray[requireMode]
            
            print("Route Search Walk Only Object Count \(walkOnlyObject.count)")
            
            while(j < walkOnlyObject.count){
                var k = 0
                
                let walkOnlyValues              = WalkOnly()
                walkOnlyValues.duration         = String(describing: walkOnlyObject.arrayValue[j]["duration"])
                walkOnlyValues.starttimestamp   = String(describing: walkOnlyObject.arrayValue[j]["starttimestamp"])
                walkOnlyValues.endtimestamp     = String(describing: walkOnlyObject.arrayValue[j]["endtimestamp"])
                walkOnlyValues.localTransport   = String(describing: walkOnlyObject.arrayValue[j]["localTransport"])
                walkOnlyValues.walkDuration     = String(describing: walkOnlyObject.arrayValue[j]["walkDuration"])
                
                let legsJSONArray = walkOnlyObject.arrayValue[j]["legs"]
                print("Legs JSON Array \(legsJSONArray.count) \(legsJSONArray)")
                legsCount = legsJSONArray.count
                
                while (k < legsJSONArray.count){
                    
                    var o = 0
                    let legsValues = Legs()
                    
                    legsValues.mode             = String(describing: legsJSONArray.arrayValue[k]["mode"])
                    legsValues.starttimestamp   = String(describing: legsJSONArray.arrayValue[k]["starttimestamp"])
                    legsValues.endtimestamp     = String(describing: legsJSONArray.arrayValue[k]["endtimestamp"])
                    legsValues.duration         = String(describing: legsJSONArray.arrayValue[k]["duration"])
                    legsValues.routeShortName   = String(describing: legsJSONArray.arrayValue[k]["routeShortName"])
                    legsValues.routeLongName    = String(describing: legsJSONArray.arrayValue[k]["routeLongName"])
                    legsValues.agencyId         = String(describing: legsJSONArray.arrayValue[k]["agencyId"])
                    legsValues.routeid          = String(describing: legsJSONArray.arrayValue[k]["routeid"])
                    legsValues.distance         = String(describing: legsJSONArray.arrayValue[k]["distance"])
                    
                    let walkInstruction     = legsJSONArray.arrayValue[k]["walkinstructions"]
                    var w = 0
                    
                    while(w < walkInstruction.count){
                        legsValues.walkinstructions.append(String(describing : walkInstruction.arrayValue[w]))
                        w += 1
                    }
                    
                    
                    let legGeometryArray = legsJSONArray.arrayValue[k]["legGeometry"]
                    print("Route Search Leg Geometry JSON \(legGeometryArray.count) \(legGeometryArray)")
                    
                    let legGeometryObject  = LegGeometry()
                    
                    legGeometryObject.length = String(describing: legGeometryArray["length"])
                    legGeometryObject.points = String(describing: legGeometryArray["points"])
                    
                    legsValues.legGeometry.append(legGeometryObject)
                    
                    let startingPointArray = legsJSONArray.arrayValue[k]["startpoint"]
                    print("Route Search Starting Point JSON Array \(startingPointArray)")
                    
                    if(startingPointArray != "null"){
                        
                        let startingPointObject = StartingPoint()
                        
                        startingPointObject.name = String(describing: startingPointArray["name"])
                        startingPointObject.lat = String(describing: startingPointArray["lat"])
                        startingPointObject.lon = String(describing: startingPointArray["lon"])
                        startingPointObject.stopid = String(describing: startingPointArray["stopid"])
                        
                        legsValues.startingPoint.append(startingPointObject)
                        
                    }
                    
                    let endPointArray = legsJSONArray.arrayValue[k]["endpoint"]
                    print("Route Search End Point JSON Array \(endPointArray.count) \(endPointArray)")
                    
                    if(endPointArray != "null"){
                        let endPointObejct = EndPoint()
                        
                        endPointObejct.name = String(describing: endPointArray["name"])
                        endPointObejct.lat = String(describing: endPointArray["lat"])
                        endPointObejct.lon = String(describing: endPointArray["lon"])
                        endPointObejct.stopid = String(describing: endPointArray["stopid"])
                        
                        legsValues.endPoint.append(endPointObejct)
                    }
                    
                    let intermediateStopsArray = legsJSONArray.arrayValue[k]["intermediatestops"]
                    print("Route Search intermediateStop JSON Array \(intermediateStopsArray.count) \(intermediateStopsArray)")
                    
                    if(intermediateStopsArray != "null"){
                        while (o < intermediateStopsArray.count){
                            
                            let intermediateStopsObject  = IntermediateStops()
                            
                            intermediateStopsObject.lat = String(describing: intermediateStopsArray.arrayValue[o]["lat"])
                            intermediateStopsObject.name = String(describing: intermediateStopsArray.arrayValue[o]["name"])
                            intermediateStopsObject.lon = String(describing: intermediateStopsArray.arrayValue[o]["lon"])
                            
                            legsValues.intermediatestops.append(intermediateStopsObject)
                            o += 1
                        }
                    }
                    
                    walkOnlyValues.legs.append(legsValues)
                    legsList.append(legsValues)
                    
                    k += 1
                }
                
                if(i == 0){
                    walkOnlyList.append(walkOnlyValues) // storing json response of walk into walkOnlyLIst
                }else if(i == 1){
                    autoCabList.append(walkOnlyValues)  // storing json response of autoCab into autoCabList
                }
            
                print("Route Search walkOnlyObjectValue \(walkOnlyList)")
                j += 1
            }
            
            i += 1
        }
        
        print("Number of times i am getting response \(i)")
        tableView.reloadData()
    }
}
