//
//  ProfileOTPViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 09/11/16.
//  Copyright © 2016 mayur. All rights reserved.
//
import UIKit
import SwiftyJSON

class ProfileOTPViewController: UIViewController {
  
    var uiFun       = UiUtillity()
    var ridlrColors = RidlrColors()
    var constant    = Constants()
    var timer       = Timer()
    
    @IBOutlet var otpTF             : UITextField!
    @IBOutlet var proceedBT         : UIButton!
    @IBOutlet var loader            : UIActivityIndicatorView!
    @IBOutlet var sendingOTPLabel   : UILabel!
    @IBOutlet var resendOTPLabel    : UILabel!
    @IBOutlet var resendBT: UIButton!
    
    var agencyName          = ""
    var bookingItemListJSON : JSON!
    var tokenArrayJSON      : JSON!
    
    var time = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("/***** PROFILE OTP SCREEN *******/")
        proceedBT.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        proceedBT.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        
        otpTF.keyboardType = UIKeyboardType.numberPad
        loader.isHidden = true
        
        otpTF.addTarget(self, action: #selector(checkOTPLength), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
        uiFun.createCleverTapEvent(message: "PROFILE OTP SCREEN")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Intializing timer for resendng otp
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(resendOTP), userInfo: nil, repeats: true)
        resendBT.isHidden = true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Hide keyboard when user clicks outside the keyboard
        self.view.endEditing(true)
    }
    
    @IBAction func proceedBTTapped(_ sender: AnyObject) {
        uiFun.showIndicatorLoader()
        
        self.view.endEditing(true)
        if(profileType == "wallet"){
            // Authenticating OTP for wallets
            doAuth()
            
        }else{
            // Authenticating OTP for without wallet
            doAuthWithoutWallet()
        }
    }
    
    @IBAction func resendOTPAction(_ sender: Any) {
        // Resending OTP Button Action
        
        time = 10
        // Start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(resendOTP), userInfo: nil, repeats: true)
        resendBT.isHidden = true
        uiFun.showIndicatorLoader()
        
        // Resending api call
        let resendOTPApi = ResendOtpApiCall()
        resendOTPApi.makeAPICall(fromViewController: self, emailId: otpEmailId, paymentGateway: otpWallet, walletEmail: otpEmailId, walletMobile: otpMobile)
    }
    
    func checkOTPLength(){
        // OTP max length is 6, it restric user to enter 6 character
        if((otpTF.text?.characters.count)! > 6){
            otpTF.deleteBackward()
        }
    }
    
    func resendOTP(){
    
        // Resend otp api call timer will start from 10 to 1
        time -= 1
        uiFun.hideIndicatorLoader()
        sendingOTPLabel.text = "Sending OTP in \(time) seconds"
        if(time == 0){
            timer.invalidate()
            resendBT.isHidden = false
            sendingOTPLabel.text = "Didn't Receive OTP?"
        }
        print("Timer of resending otp \(time)")
        
    }
    
    
    func resendOTPResponse(jsonResult : JSON){
    
        // Resend OTP api call response
        uiFun.hideIndicatorLoader()
        print("Successfully Resending otp")
        if(String(describing : jsonResult["status"]) == "OK"){
        
            // Suceess response
            self.loader.stopAnimating()
            self.uiFun.hideIndicatorLoader()
            
            print("JSON OBJECT VALUE \(jsonResult["data"])")
            
            let jsonData = jsonResult["data"]
            
            let defaults = UserDefaults.standard
            defaults.set(String(describing: jsonData["pgPreAuthData"]), forKey: "pgPreAuthData")
            defaults.set(String(describing: jsonData["email"]), forKey: "userEmailId")
            defaults.set(String(describing: jsonData["mobile"]), forKey: "userMobileNo")
            
            uiFun.writeData(value: String(describing: jsonData["pgPreAuthData"]), key: "pgPreAuthData")
            uiFun.writeData(value: String(describing: jsonData["email"]), key: "userEmailId")
            uiFun.writeData(value: String(describing: jsonData["mobile"]), key: "userMobileNo")
            
            print("Jugaad of  mobile \(jsonData["mobile"])")
            
            if let description1 = ((jsonResult["data"] as? NSArray)?[0] as? NSDictionary)?["mobile"] as? String{
                print("JSON Response Mobile \(description1)")
            }
        }else{
            uiFun.showExceptionDialog(jsonResponse: jsonResult, fromViewController: self)
        }
    }
    
    func doAuthWithoutWallet(){
    
        // Make api call for creating user profile without any wallet
        let util = Utillity()
        
        let url = "\(constant.STAGING_BASE_URL)/user/authenticate"
        util.APICall(apiUrl: url, jsonString: checkOTPWithoutWallet(otp: String(otpTF.text!))!, requestType: "POST", header: false) { (result, error) in
            print("Successfuly validate user profile otp")
            let jsonResponse    = result!
            if(String(describing : jsonResponse["code"]) == "5000"){
                self.uiFun.createCleverTapEvent(message: "Authentication method by user Ridlr: Manual OTP")
                
                /******** PROFILE CREATED SAVE RIDLR-TOKEN AND CUSTOMER ID ************/
                let jsonData = jsonResponse["data"]
                
                let defaults = UserDefaults.standard
                // Storing ridlr-token into parmanent storage
                defaults.setValue(String(describing: jsonData["ridlrToken"]), forKey: "ridlrToken")
                // Storing user id into parmanent storage
                defaults.setValue(String(describing: jsonData["customerId"]), forKey: "userId")
                
                print("Ridlr Token \(String(describing: jsonData["ridlrToken"]))")
                print("User Profile Auth Method Data \(jsonData)")
                
                self.makeUserAPICall()
                
//                if(fromUserProfile){
//                // If user have created profile from navigation drawer or menu drawer will redirect to profile screen after profie creation
//                    self.uiFun.hideIndicatorLoader()
//                    self.uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
//                    fromUserProfile = false
//                }
                
                
            }else{
                self.uiFun.hideIndicatorLoader()
                let jsonError = jsonResponse["error"]
                // Fetching errorCodeText from json response
                if(jsonError["errorCodeText"] == "" || jsonError["errorCodeText"] == "null"){
                    // if errorCodeText is null parse error to "showExceptionDialog" method
                    self.uiFun.showExceptionDialog(jsonResponse: jsonResponse, fromViewController: self)
                }else{
                    // SHow error into alert box
                    self.uiFun.showAlert(title: "", message: String(describing : jsonError["errorCodeText"]), logMessage: "doAuthWithoutWallet()", fromController: self)
                }
            }
        }
    }
    
    func doAuth(){
        // Wallet OTP Authentication Method
        do {
            if let jsonString = checkOTP(otp: String(otpTF.text!)){
                print("JSON BODY REQUEST \(jsonString)")
                // create post request
                let url = NSURL(string: "\(constant.STAGING_BASE_URL)/payment/authenticate")!
                print("Payment Authenticate URL \(url)")
                let request = NSMutableURLRequest(url: url as URL)
                request.httpMethod = "POST"
                
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpBody = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true)
                print("BODY REQUEST" + String(describing: request.httpBody))
                
                
                let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                    if error != nil{
                        print("Certificate Error -> \(error!)")
                        
                        self.uiFun.hideIndicatorLoader()
                        if(error.debugDescription.contains("timed out")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("connection was lost")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("appears to be offline")){
                            self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                        }

                        return
                    }else{
                        
                        if let urlContent = data{
                            
                            do {
                                
                                let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                                
                                let jsonResponse = JSON(jsonResult)
                                print("Profile OTP Verification JSON Response \(jsonResult)")
                                
                                if(jsonResponse["status"] == "OK"){
                                    
                                    self.uiFun.hideIndicatorLoader()
                                    
                                    self.uiFun.createCleverTapEvent(message: "Authentication method by user Citrus : Manual OTP")
                                    self.uiFun.createCleverTapEvent(message: "Wallet validation response Success")
                                    
                                    print("JSON RESPONSE doAuth \(jsonResult)")
                                    print("JSON OBJECT VALUE doAuth \(jsonResult["data"]!!)")
                                    
                                    let jsonData = JSON(jsonResult["data"]!!)
                                    
                                    let defaults = UserDefaults.standard
                                    // Storing pgAuthData into parmanent stroage
                                    defaults.setValue(String(describing: jsonData["pgAuthData"]), forKey: "pgAuthData")
                                    
                                    self.CreateUserProfile()
                                    
                                    /**************** NAVIGATE TO OTP SCREEN**************/
                                    self.uiFun.hideIndicatorLoader()
                                    
                                    
                                }else if (String(describing : jsonResponse["code"]) != "5000"){
                                    self.uiFun.hideIndicatorLoader()
                                    let jsonError       = jsonResponse["error"]
                                    let errorText       = String(describing: jsonError["errorCodeText"])
                                    
                                    self.uiFun.showAlert(title: "", message: errorText, logMessage: "Profile Creating Error Text", fromController: self)
                                    self.loader.stopAnimating()
                                    self.uiFun.createCleverTapEvent(message: "Wallet validation response Failed: \(jsonResponse)")

                                }else{
                                    self.uiFun.hideIndicatorLoader()
                                    self.loader.stopAnimating()
                                    self.uiFun.hideIndicatorLoader()
                                }
                            }
                            catch{
                                print("Failed To Convert JSON")
                                self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "doAuth()", fromController: self)
                            }
                        }
                    }
                }
                
                task.resume()
                
            }
        }
    }
    
    func checkOTP(otp: String) -> String?{
        // OTP validation json string with wallet
        let defaults                    = UserDefaults.standard
        let pgPreAuthData               = uiFun.readData(key: "pgPreAuthData")//defaults.string(forKey: "pgPreAuthData")
        let userEmail                   = uiFun.readData(key : "userEmailId")
        
        let para:NSMutableDictionary    = NSMutableDictionary()
        para.setValue(otpWallet, forKey: "paymentGateway")
        para.setValue(otp, forKey: "otp")
        para.setValue("", forKey: "password")
        para.setValue(userEmail, forKey: "email")
        para.setValue(pgPreAuthData, forKey: "pgPreAuthData")

        let jsonData: NSData
        do{
            jsonData        = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString  = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            print("json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    func checkOTPWithoutWallet(otp : String) -> String?{
    
        // OTP Validation without wallet json string
        let authData : NSMutableDictionary = NSMutableDictionary()
        authData.setValue(otp, forKey: "data")
        authData.setValue("OTP", forKey: "method")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(otpEmailId, forKey: "contactEmail")
        para.setValue(otpMobile, forKey: "contactNumber")
        para.setValue(otpEmailId, forKey: "deviceEmail")
        para.setValue(authData, forKey: "authData")
        
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            print("json string = \(jsonString)")
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    func CreateUserProfile(){
        // Creating user profile api call
        uiFun.hideIndicatorLoader()
        do {
            if let jsonString       = CreateUserProfileData(otp: String(otpTF.text!)){
                print("JSON BODY Create User Profile REQUEST \(jsonString)")
                
                // create post request
                let url             = NSURL(string: "\(constant.STAGING_BASE_URL)/user/authenticate")!
                print("User Authenicate URK \(url)")
                let request         = NSMutableURLRequest(url: url as URL)
                request.httpMethod  = "POST"
                
                // insert json data to the request
                request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                request.httpBody    = jsonString.data(using: String.Encoding.utf8, allowLossyConversion: true) // setting api call json body
                print("BODY REQUEST" + String(describing: request.httpBody))
                
                
                let task = URLSession.shared.dataTask(with: request as URLRequest){ data, response, error in
                    if error != nil{
                        // API Calling error
                        print("Certificate Error -> \(error!)")
                        self.uiFun.hideIndicatorLoader()
                        if(error.debugDescription.contains("timed out")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("connection was lost")){
                            self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                        }else if(error.debugDescription.contains("appears to be offline")){
                            self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                        }
                        return
                    }else{
                        
                        if let urlContent = data{
                            
                            do {
                                // API Response
                                let jsonResult      = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                                
                                let jsonResponse    = JSON(jsonResult)
                                print("Creating User Profile Response \(jsonResult)")
                                
                                if(jsonResponse["status"] == "OK"){
                                    // API Response Success
                                    self.uiFun.createCleverTapEvent(message: "User Profile created successfully Success")
                                    print("JSON RESPONSE CreateUserProfile \(jsonResult)")
                                    print("JSON OBJECT VALUE CreateUserProfile \(jsonResult["data"]!!)")
                                    
                                    let jsonData    = JSON(jsonResult["data"]!!)
                                    
                                    let defaults    = UserDefaults.standard
                                    // Storing ridlr-token into parmanent storage
                                    defaults.setValue(String(describing: jsonData["ridlrToken"]), forKey: "ridlrToken")
                                    // Storing user id into parmanent storage
                                    defaults.setValue(String(describing: jsonData["customerId"]), forKey: "userId")
                                    
                                    print("Ridlr Token \(String(describing: jsonData["ridlrToken"]))")
                                    print("User Profile Auth Method Data \(jsonData)")
                                    
                                    self.makeUserAPICall()
                                    defaults.setValue("done", forKey: "ProfileCreation")
                                    
                                    
                                }else{
                                    
                                    OperationQueue.main.addOperation {
                                        
                                        // Show API response error in alert box
                                        let jsonError = JSON(jsonResult["error"]!!)

                                        self.uiFun.createCleverTapEvent(message: "User Profile created successfully Failed \(jsonResponse)")
                                        self.uiFun.showAlert(title: "Error", message: String(describing: jsonError["errorCodeText"]), logMessage: "Profile Validation OTP Error", fromController: self)
                                    }
                                }
                                
                            }
                            catch{
                                // Default error if resposne failed to convert into json
                                print("Failed To Convert JSON")
                                self.uiFun.showAlert(title: "", message: RidlrError.JSON_FAIL.rawValue, logMessage: "CreateUserProfile()", fromController: self)
                            }
                        }
                    }
                }
                
                task.resume()
            }
        }
    }
    
    func CreateUserProfileData(otp: String) -> String?{
        // Creating User profile json string
        let userEmailId : String!
        userEmailId = uiFun.readData(key: "userEmailId")
        
        let userMobileNo : String!
        userMobileNo = uiFun.readData(key: "userMobileNo")

        let defaults = UserDefaults.standard
        let pgAuthData = defaults.string(forKey: "pgAuthData")
        
        let para1:NSMutableDictionary = NSMutableDictionary()
        para1.setValue(otpWallet, forKey: "paymentGateway")
        para1.setValue(pgAuthData, forKey: "pgAuthData")
        
        let para2:NSMutableDictionary = NSMutableDictionary()
        para2.setValue("OTP", forKey: "method")
        para2.setValue(otp, forKey: "data")
        
        let para:NSMutableDictionary = NSMutableDictionary()
        para.setValue(userEmailId!   , forKey: "contactEmail")
        para.setValue(userMobileNo!  , forKey: "contactNumber")
        para.setValue(userEmailId!   , forKey: "deviceEmail")
        para.setValue(para2, forKey: "authData")
        para.setValue(para1, forKey: "walletInfo")
        
        
        // let jsonError: NSError?
        let jsonData: NSData
        do{
            jsonData = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            print("json array string creating user profile = \(jsonString)") // Create user profile json string
            return jsonString
            
        } catch _ {
            print ("UH OOO")
            return nil
        }
    }
    
    // MARK:Fetching User Detail
    
    func makeUserAPICall(){
        
        uiFun.showIndicatorLoader()
        let userAPI = UserAPICall()
        userAPI.makeAPICall(fromViewController: self)
    }
    
    
    func gotUserAPIResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        print("Got the user api response \(response)")
        
        let data        = response["data"]
        
        uiFun.writeData(value: String(describing: data["contactEmail"]), key: "userEmail")
        uiFun.writeData(value: String(describing: data["contactNumber"]), key: "mobileNumber")
        
        let customCards = data["customerCards"]
        uiFun.writeData(value: "done", key: "ProfileCreation")
        
        if (customCards.count == 0){
            
            let agencyName          = uiFun.readData(key: "ticketingAgency")
            if(fromUserProfile){
                uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
            }else if(agencyName == "metroTrip"){
                uiFun.navigateToScreem(identifierName: "MetroTripViewController", fromController: self)
            }else if(agencyName == "STORE_VALUE_PASS"){
                uiFun.navigateToScreem(identifierName: "StoreValueRechargeScreenViewController", fromController: self)
            }else{
                uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
            }
        }
        
        for i in 0..<customCards.count{
        
            let typeOfCard          = String(describing : customCards.arrayValue[i]["type"])
            let bookingJSON         = customCards.arrayValue[i]["booking"]
            
            if(String(describing : bookingJSON) != "null"){
                agencyName              = uiFun.readData(key: "ticketingAgency")
                bookingItemListJSON     = bookingJSON["bookingItemsList"]
                tokenArrayJSON          = bookingItemListJSON.arrayValue[0]["tokens"]
            }
            
            switch typeOfCard {
            case "TRIP_PASS":
                print("User have trip pass at index \(i)")
//                let processJSONResponse = MetroJSONResponses()
//                let processedResponse   = processJSONResponse.processMetroJSONResponse(response: bookingJSON)
            
                if(bookingItemListJSON != nil){
                    
                    let tokenJSON           = bookingItemListJSON.arrayValue[0]["tokens"]
                    let itemAttJSON         = bookingItemListJSON.arrayValue[0]["itemAttribute"]
                    let routeJSON           = bookingItemListJSON.arrayValue[0]["bookingItemRoutesList"]
                    
                    uiFun.writeData(value: "yes", key: "metroTripStatus")
                    uiFun.writeData(value: String(describing: bookingJSON ["bookingId"]), key: "tripBookingId")
                    uiFun.writeData(value: String(describing: itemAttJSON["masterQrCodeId"]), key: "tripMasterQRCode")
                    uiFun.writeData(value: String(describing: routeJSON.arrayValue[0]["sourceName"]), key: "tripPassSourceName")
                    uiFun.writeData(value: String(describing: routeJSON.arrayValue[0]["destinationName"]), key: "tripPassDestinationeName")
                    uiFun.writeData(value: String(describing: itemAttJSON["trips"]), key: "tripPassRemainingTrip")
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["expiryDate"]), key: "tripPassExpiry")
                    
                    if(tokenArrayJSON.count > 0){
                        uiFun.writeData(value: String(describing : tokenArrayJSON.arrayValue[0]["qrCodeId"]), key: "tripPassSlaveQRCode")
                        processResponseToModel(response: response, index: i)
                    }
                }
                break
                
            case "STORE_VALUE_PASS":
                print("User have store value pass")
//                let processJSONResponse = MetroJSONResponses()
//                let processedResponse   = processJSONResponse.processMetroJSONResponse(response: bookingJSON)
                
                if (bookingItemListJSON != nil){
                 
                    let tokenJSON           = bookingItemListJSON.arrayValue[0]["tokens"]
                    let itemAttJSON         = bookingItemListJSON.arrayValue[0]["itemAttribute"]
                    let routeJSON           = bookingItemListJSON.arrayValue[0]["bookingItemRoutesList"]
                    let tokenArray          = bookingItemListJSON.arrayValue[0]["tokens"]
                    
                    uiFun.writeData(value: "yes", key: "metroStoreValuePass")
                    uiFun.writeData(value: String(describing: itemAttJSON["masterQrCodeId"]), key: "metroStoreValueMasterQRCode")
                    uiFun.writeData(value: String(describing: bookingJSON["bookingId"]), key: "storeValueBookingId")
                    uiFun.writeData(value: String(describing: itemAttJSON["balance"]), key: "storeValuePassBalance")
                    uiFun.writeData(value: String(describing: bookingItemListJSON.arrayValue[0]["expiryDate"]), key: "storeValueExpiry")
                    
                    if(tokenArrayJSON.count > 0){
                        uiFun.writeData(value: String(describing : tokenArrayJSON.arrayValue[0]["qrCodeId"]), key: "storeValueSlaveQRCode")
                        processResponseToModel(response: response, index: i)
                    }
                }
                break
                
            default:
                //If user have created profile from any agency will redirect to payment screen after profie creation
                break
            }
        }
        
        if(fromUserProfile){
            uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
        }else if(agencyName == "metroTrip"){
            uiFun.navigateToScreem(identifierName: "TripPassReceiptViewController", fromController: self)
        }else if(agencyName == "STORE_VALUE_PASS"){
            uiFun.navigateToScreem(identifierName: "StoreValueReceiptViewController", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "MetroGetTokenViewController", fromController: self)
        }
    }
    
    
    func processResponseToModel(response : JSON, index : Int){
    
        let data                        = response["data"]
        let customCardsArray            = data["customerCards"]
        
        let bookingsJSON                = customCardsArray.arrayValue[index]["booking"]
        let bookingItemListJSON         = bookingsJSON["bookingItemsList"]
        
        let tokensJSON                  = bookingItemListJSON.arrayValue[0]["tokens"]
        let bookingItemRoutesList       = bookingItemListJSON.arrayValue[0]["bookingItemRoutesList"]
        let itemAttJSON                 = bookingItemListJSON.arrayValue[0]["itemAttribute"]
        
        let tokenModel                  = MetroTokenJSONModel()
        var tokenResponseList           = [MetroTokenJSONModel]()
        
        if(tokensJSON.count > 0){
        
            let metroTokenModal         = NewMetroTokenModel()
            metroTokenModal.status              = String(describing: tokensJSON.arrayValue[0]["status"])
            metroTokenModal.qrCodeId            = String(describing: tokensJSON.arrayValue[0]["qrCodeId"])
            metroTokenModal.type                = String(describing: tokensJSON.arrayValue[0]["type"])
            metroTokenModal.qrCode              = String(describing: tokensJSON.arrayValue[0]["qrCode"])
            metroTokenModal.expiry              = String(describing: tokensJSON.arrayValue[0]["expiry"])
            
            metroTokenModal.routeId             = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            metroTokenModal.routeName           = String(describing: bookingItemRoutesList.arrayValue[0]["routeName"])
            metroTokenModal.sourceStageName     = String(describing: bookingItemRoutesList.arrayValue[0]["sourceStageName"])
            metroTokenModal.destinationName     = String(describing: bookingItemRoutesList.arrayValue[0]["destinationName"])
            metroTokenModal.destinationStageId  = String(describing: bookingItemRoutesList.arrayValue[0]["destinationStageId"])
            metroTokenModal.bookingItemRouteId  = String(describing: bookingItemRoutesList.arrayValue[0]["bookingItemRouteId"])
            metroTokenModal.routeId             = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            metroTokenModal.routeId             = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            metroTokenModal.routeId             = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            metroTokenModal.routeId             = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            
            let route                 = Route()
            route.routeId                       = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            route.routeName                     = String(describing: bookingItemRoutesList.arrayValue[0]["routeName"])
            route.sourceStageName               = String(describing: bookingItemRoutesList.arrayValue[0]["sourceStageName"])
            route.destinationName               = String(describing: bookingItemRoutesList.arrayValue[0]["destinationName"])
            route.destinationStageId            = String(describing: bookingItemRoutesList.arrayValue[0]["destinationStageId"])
            route.bookingItemRouteId            = String(describing: bookingItemRoutesList.arrayValue[0]["bookingItemRouteId"])
            route.routeId                       = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            route.routeId                       = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            route.routeId                       = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            route.routeId                       = String(describing: bookingItemRoutesList.arrayValue[0]["routeId"])
            
            let fare                 = Fare()
            fare.baseAmount                     = String(describing: bookingsJSON["baseAmount"])
            fare.totalAmount                    = String(describing: bookingsJSON["totalAmount"])
            fare.totalSurcharge                 = String(describing: bookingsJSON["totalSurcharge"])
            fare.totalTax                       = String(describing: bookingsJSON["totalTax"])
            fare.totalDiscount                  = String(describing: bookingsJSON["totalDiscount"])
            
            tokenModel.bookingNo                = String(describing: bookingsJSON["bookingId"])
            tokenModel.itemType                 = String(describing: bookingItemListJSON.arrayValue[0]["itemType"])
            tokenModel.itemSubType              = String(describing: bookingItemListJSON.arrayValue[0]["itemSubType"])
            tokenModel.expiryDate               = String(describing: bookingItemListJSON.arrayValue[0]["expiryDate"])
            tokenModel.activationDate           = String(describing: bookingItemListJSON.arrayValue[0]["activationDate"])
            tokenModel.activeRenewalDate        = String(describing: bookingItemListJSON.arrayValue[0]["activeRenewalDate"])
            tokenModel.validityCriteria         = String(describing: bookingItemListJSON.arrayValue[0]["validityCriteria"])
            tokenModel.issuerIdentifier         = String(describing: bookingItemListJSON.arrayValue[0]["issuerIdentifier"])
            tokenModel.issuerTag                = String(describing: bookingItemListJSON.arrayValue[0]["issuerTag"])
            tokenModel.companyItemId            = String(describing: bookingItemListJSON.arrayValue[0]["companyItemId"])
            tokenModel.bookingItemId            = String(describing: bookingItemListJSON.arrayValue[0]["bookingItemId"])
            
            tokenModel.companyRefId             = String(describing : itemAttJSON["masterQrCodeId"])
            tokenModel.refTxnId                 = String(describing : itemAttJSON["refTxnId"])
            tokenModel.trips                    = String(describing : itemAttJSON["trips"])
            tokenModel.entryScanTime            = String(describing : itemAttJSON["entryScanTime"])
            
            tokenModel.fare.append(fare)
            tokenModel.routeDetail.append(route)
            tokenModel.newMetroToken.append(metroTokenModal)
            tokenResponseList.append(tokenModel)
            
            DBManager.shared.insertBookingDataData(bookingItemList: tokenResponseList)
            DBManagerToken.shared.insertTokenData(bookingItemList: tokenResponseList)
            
        }
    }
}
