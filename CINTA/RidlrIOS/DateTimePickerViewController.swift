//
//  DateTimePickerViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 25/01/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var timeChange              = false
var timeChangehour          : Int?
var timeChangeMinute        : Int?
var timeChangeMode          : String?
var timingLabelValue        : String?
var timeChangeDate          = ""
var plannerChangeDateTime   = ""
class DateTimePickerViewController: UIViewController {

    @IBOutlet var datePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker                  = UIDatePicker()
        datePicker.datePickerMode   = .dateAndTime  // Setting picker view in date time format
    
        datePicker.addTarget(self, action: Selector(("datePickerChanged:")), for: UIControlEvents.valueChanged)
        uiFun.createCleverTapEvent(message: "CHANGE DATE & TIME SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func datePickerAction(_ sender: Any) {
        
        let dateFormatter       = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let strDate             = dateFormatter.string(from: (sender as AnyObject).date) // Gives the current date
        
        let currentDateTime     = strDate.components(separatedBy: ",")
        let selectedDate        = currentDateTime[0]
        let selectedTime        = currentDateTime[1]
        
        
        let arrivalDateArray    = selectedDate.components(separatedBy: "/") // Fetchign Year & Month
        let year                = "20\(arrivalDateArray[2])"
        var month               = arrivalDateArray[0]
        
        if(month.characters.count == 1){
            month = "0\(arrivalDateArray[0])"
        }
        
        var date  = arrivalDateArray[1]
        
        if(date.characters.count == 1){
            date = "0\(arrivalDateArray[1])"
        }
        
        // When user select time from the picker view
        let selectedTimeArray   = selectedTime.components(separatedBy: " ")
        let time                = selectedTimeArray[1]
        let timeValue           = selectedTimeArray[2]
        
        let timeArray           = time.components(separatedBy: ":")
        var hour                = timeArray[0]
        var minutes             = timeArray[1]
        let seconds             = "00"
        timeChangeMode          = timeValue
        
        // if timevalue is PM add 12 into current time
        timingLabelValue = hour
        if(timeValue == "PM"){
            hour                = "\(Int(hour)! + 12)"
            timeChangehour      = Int(hour)
        }
        timeChangehour = Int(hour)
        
        // If hour charcter is one than add 0 before hour
        if(hour.characters.count == 1){
            hour                = "0\(hour)"
        }
        
        // If minute charcter is one than add 0 before hour
        if(minutes.characters.count == 1){
            minutes = "0\(minutes)"
            timeChangeMinute    = Int(minutes)
        }else{
            timeChangeMinute    = Int(minutes)
        }
        
        // Changed date
        timeChangeDate          = "\(month)/ \(date)/ \(year)"
        print("Changed Time is \(date) \(month) \(year)")
        let arrivalDate         = year + month + date
        let arrivalTime         = "\(hour):\(minutes):\(seconds)"
        
        timeChangeValue         = "\(arrivalDate) \(arrivalTime)"
        plannerChangeDateTime   = arrivalDate + hour + minutes + seconds
        
        print("Date picker date \(arrivalDate + arrivalTime)")
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        // When user press done button on top right corner
        timeChange = true
        if(fromWhichScreen == "planner"){
            uiFun.navigateToScreem(identifierName: "RouteSearchResult", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
        }
        
        

    }
    @IBAction func closeButtonAction(_ sender: Any) {
        
        // When user press close button on top left corner
        timeChange = false
        if(fromWhichScreen == "planner"){
            routeList.removeAll()
            uiFun.navigateToScreem(identifierName: "RouteSearchResult", fromController: self)
        }else{
            uiFun.navigateToScreem(identifierName: "TImeTableTimingViewController", fromController: self)
        }
    }
    
}
