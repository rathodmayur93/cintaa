//
//  DriveDetailViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 07/03/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps

class DriveDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, GMSMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet var recommendedView       : UIView!
    @IBOutlet var routeName             : UILabel!
    @IBOutlet var routeDuration         : UILabel!
    @IBOutlet var shareButton           : UIButton!
    
    var mapView : GMSMapView            = GMSMapView()
    
    var minLat  : Double                = Double()
    var maxLat  : Double                = Double()
    var minLong : Double                = Double()
    var maxLong : Double                = Double()
    
    var task    : URLSessionTask        = URLSessionTask()
    var timer   : Timer                 = Timer()
    
    var polyline                        = GMSPolyline()
    var animationPolyline               = GMSPolyline()
    var path                            = GMSPath()
    var animationPath                   = GMSMutablePath()
   
    var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    @IBOutlet var trafficDataTableVIew: UITableView!
    
    let uiFun = UiUtillity()
    let ridlrColors = RidlrColors()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        initMap()
        mapView.delegate    = self
        
        uiFun.createCleverTapEvent(message: "FIND A ROUTE DRIVE DETAIL RESULT SCREEN")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func setUI(){
    
        routeName.text                      = driveList[plannerTripIndex].summary
        routeDuration.text                  = "\(driveList[plannerTripIndex].tte) ( \(driveList[plannerTripIndex].distance) )"
        
        // craeting pan gesture for recommended routes
        let panGesture  = UIPanGestureRecognizer(target: self, action: #selector(moveRecommendeView(_:)))
        panGesture.maximumNumberOfTouches = 1
        recommendedView.addGestureRecognizer(panGesture)
        
        // craeting pan gesture for recommended routes
        let panGestureTable  = UIPanGestureRecognizer(target: self, action: #selector(moveRecommendeView(_:)))
        panGestureTable.maximumNumberOfTouches = 1
        trafficDataTableVIew.addGestureRecognizer(panGestureTable)
        
        panGesture.delegate         = self
        panGestureTable.delegate    = self
        
        mapDataAPICall()    // map traffic data api call
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool{
        return true
    }
    
    func moveRecommendeView(_ gestureRecognizer: UIPanGestureRecognizer){
    
        // when user pan the table view table view will cover whole screen
        if (gestureRecognizer.state == .began || gestureRecognizer.state == .changed) {
            
            let translation             = gestureRecognizer.translation(in: view)
            
            print("Table View translation \(translation)")
            // note: 'view' is optional and need to be unwrapped
            if gestureRecognizer.state == .began {
                
            } else if gestureRecognizer.state == .changed {
                
                let velocity = gestureRecognizer.velocity(in: view)
                print("Velocity of tableview pan gesture \(velocity)")
                
                // changing the height and y point of recommended routes table
                gestureRecognizer.setTranslation(CGPoint.zero, in: view)
                recommendedView.center         = CGPoint(x: (gestureRecognizer.view?.center.x)!, y: (gestureRecognizer.view?.center.y)! + translation.y)
                
                if(velocity.y > 300){
                    // pan sensitivity depends on its velocity
                    UIView.animate(withDuration:1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options:[] ,
                                   animations: { () -> Void in
                                    self.recommendedView.center.y = 428
                    }, completion: nil)
                }else{
                
                    UIView.animate(withDuration:1.0, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 3, options:[] ,
                                   animations: { () -> Void in
                                    self.recommendedView.center.y = 94
                    }, completion: nil)
                }
                
                
            }
        }

    }
   
    @IBAction func backButtonAction(_ sender: Any) {
        // when user clicks on the back button
        uiFun.navigateToScreem(identifierName: "RouteSearchResult", fromController: self)
    }
    
    func initMap(){
        // initializing map
        let sourceCord      = driveList[plannerTripIndex].sourceCoords
        let destinationCord = driveList[plannerTripIndex].destinationCoords
        let latlong         = sourceCord.components(separatedBy: ",")
        let destiLatLong    = destinationCord.components(separatedBy: ",")
        mapView.settings.zoomGestures = true
        
        // setting the camera of the map
        let camera      = GMSCameraPosition.camera(withLatitude: Double(latlong[0])!, longitude: Double(latlong[1])!, zoom: 15.0)
        mapView.frame   = CGRect(x: 0, y: 66, width: self.view.frame.width, height: 294)
        mapView         = GMSMapView.map(withFrame: mapView.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        
        // creating source marker for map
        let marker      = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(latlong[0])!, longitude: Double(latlong[1])!)
        marker.icon     = UIImage(named: "ic_map_flag_start_complete")
        marker.map = mapView
        
        // creating destination marker for map
        let markerEnd       = GMSMarker()
        markerEnd.position  = CLLocationCoordinate2D(latitude: Double(destiLatLong[0])!, longitude: Double(destiLatLong[1])!)
        markerEnd.icon      = UIImage(named: "ic_map_flag_end_complete")
        markerEnd.map       = mapView
        
        let path = GMSMutablePath()
        
       // path.add(CLLocationCoordinate2D(latitude: Double(latlong[0])!, longitude: Double(latlong[1])!))
       // path.add(CLLocationCoordinate2D(latitude: Double(destiLatLong[0])!, longitude: Double(destiLatLong[1])!))
        
        // creating the polylines
        let polyline = GMSPolyline(path: path)
        
        polyline.strokeColor = UIColor.black
        polyline.strokeWidth = 3.0
        let solidRed         = GMSStrokeStyle.solidColor(UIColor.black)
        polyline.spans       = [GMSStyleSpan(style: solidRed)]
        polyline.geodesic    = true
        polyline.map         = self.mapView

        self.view.addSubview(mapView)
        self.view.bringSubview(toFront: recommendedView)
        
        callApiForDrivingMode(sourceLat:  Double(latlong[0])!, sourceLong: Double(latlong[1])!, destinationLat: Double(destiLatLong[0])!, destinationLong: Double(destiLatLong[1])!)
    }
    
    // api call to get polyline path of public road
    func callApiForDrivingMode(sourceLat : Double, sourceLong : Double, destinationLat : Double, destinationLong : Double){
    
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLat),\(sourceLong)&destination=\(destinationLat),\(destinationLong)&sensor=false&mode=driving")!
       print("Google Direction API call \(url)")
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print("Geocoding error")
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        print("JSON Response Of Direction API \(json)")
                        let jsonData            = JSON(json)
                        let routes              = jsonData["routes"]
                        let overview_polyline   = routes.arrayValue[0]["overview_polyline"]
                        let polyString          = String(describing: overview_polyline["points"])
                        print("PolyString is \(polyString)")
                        //Call this method to draw path on map
                        self.showPath(polyStr: polyString)
                    }
                    
                }catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    // draw polylines on map public road
    func showPath(polyStr :String){
        
        // showing polyline bet
        path                    = GMSPath(fromEncodedPath: polyStr)!
        polyline                = GMSPolyline(path: path)
        polyline.strokeColor    = UIColor.black
        polyline.strokeWidth    = 3.0
        let solidRed            = GMSStrokeStyle.solidColor(UIColor.black)
        polyline.spans          = [GMSStyleSpan(style: solidRed)]
        polyline.geodesic       = true
        polyline.map            = mapView // Your map view
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(animatePolylinePath), userInfo: nil, repeats: true)
        
    }
    
    func animatePolylinePath() {
        print("Animating Polylines")
        var i : UInt = 0
        if (i < path.count()) {
            
            self.animationPath.add(self.path.coordinate(at: i))
            self.animationPolyline.path         = self.animationPath
            self.animationPolyline.strokeColor  = UIColor.lightGray
            self.animationPolyline.strokeWidth  = 3.0
            self.animationPolyline.map          = self.mapView
            i += 1
        }
        else {
            i = 0
            self.animationPath          = GMSMutablePath()
            self.animationPolyline.map  = nil
        }
    }
    
    
     public func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        print("End Of map dragging")
        
        // when user drag the map view after that when maps get ideal
        var time                        = 0
        timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            mapView.setMinZoom(13.0, maxZoom: 17.0)
            time += 1
            print("Traffic Near you Timer \(time)")
            if(time == 2){
                timer.invalidate()
                
                print("Map Four Cordinates \(self.mapView.bounds)")
                
                let bounds = mapView.projection.visibleRegion()
                
                print("Bounds of map end \(bounds.farLeft.latitude) \(bounds.farLeft.longitude) \(bounds.farRight.latitude) \(bounds.farRight.longitude) \(bounds.nearLeft.latitude) \(bounds.nearLeft.longitude) \(bounds.nearRight.latitude) \(bounds.nearRight.longitude)")
                
                let zoomLevel = position.zoom
                print("map zoom level \(zoomLevel)")
                if(Int(zoomLevel) > 19 || Int(zoomLevel) < 15){
                    mapView.animate(toZoom: 15.0)
                }
                
                if(Int(zoomLevel) < 19 || Int(zoomLevel) > 15){
                    self.minLat  = bounds.nearLeft.latitude
                    self.maxLat  = bounds.farRight.latitude
                    self.minLong = bounds.nearLeft.longitude
                    self.maxLong = bounds.farRight.longitude
                    self.mapDataAPICall()
                    
                }
            }
        }
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        // number of rows in table view
        return driveList[plannerTripIndex].roadProperties.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        // initializing the cell
        let cell =  tableView.dequeueReusableCell(withIdentifier: "Cell") as! DriveTableViewCell
        
        cell.routeNameLabel.text    =  driveList[plannerTripIndex].roadProperties[indexPath.row].roadAreaName
        cell.durationLabel.text     =  driveList[plannerTripIndex].roadProperties[indexPath.row].time
        cell.trafficStatus.text     =  driveList[plannerTripIndex].roadProperties[indexPath.row].legend
        cell.averageSpeedLabel.text =  "Average speed \(driveList[plannerTripIndex].roadProperties[indexPath.row].speedKPH) \(driveList[plannerTripIndex].roadProperties[indexPath.row].roadAreaName)"
        
        let trafficString           = driveList[plannerTripIndex].roadProperties[indexPath.row].legend
        let firstChar               = trafficString.index(trafficString.startIndex, offsetBy: 0)
        cell.startingTextLabel.text = String(describing: trafficString[firstChar])
        
        cell.startingTextLabel.layer.cornerRadius    = (cell.startingTextLabel.frame.size.width)/2
        cell.startingTextLabel.layer.borderWidth     = 1.0
        cell.startingTextLabel.textColor             = UIColor.black
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // creating the layout of the row in tableview
        cell.contentView.backgroundColor = uiFun.hexStringToUIColor(hex: ridlrColors.newsFeed_bg_color)
        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y:8, width: self.view.frame.width, height: 170))
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
        
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        
    }
    
    /******************* MAP TRAFFIC DATA API AND IT'S RESPONSE ******************///
    
    func mapDataAPICall(){
        // api call for fetching the traffic data
        let mapDataUrl = "http://ridlr.in/callWebService_Get.php?webMethodName=MapDataAsJSonWap&parameters=bound1:\(minLat),\(minLong),\(maxLat),\(maxLong)~bound2:0,0,0,0~zoom:4"
        
        let url = NSURL(string: mapDataUrl)!
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "GET"
        
        task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            
            if error != nil{
                print("Certificate Error -> \(error!)")
                self.uiFun.hideIndicatorLoader()
                if(error.debugDescription.contains("timed out")){
                    self.uiFun.showAlert(title: "Notification", message: "Oops..Request timed out", logMessage: "Create Profile", fromController: self)
                }else if(error.debugDescription.contains("connection was lost")){
                    self.uiFun.showAlert(title: "Notification", message: "Oops.. Looks like connection was lost", logMessage: "Create Profile", fromController: self)
                }else if(error.debugDescription.contains("appears to be offline")){
                    self.uiFun.showAlert(title: "Notification", message: "Internet connection appears to be offline", logMessage: "Create Profile", fromController: self)
                }
                return
            }else{
                
                if let urlContent = data{
                    
                    do {
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        print("Yo Yo \(jsonResult)")
                        self.responseOfMapData(response: JSON(jsonResult))
                        
                    }
                    catch{
                        print("Failed To Convert JSON")
                        
                    }
                }
            }
            
        }
        
        task.resume()
    }
    
    func responseOfMapData(response : JSON){
        // response of traffic map data api call
        print("MAP DATA Response Got it")
                
        let rc = response["rc"]
        var i = 0
        
        while i < rc.count{
            
            let data = rc.arrayValue[i]["data"]
            var j = 0
            let path = GMSMutablePath()
            
            while j < data.count{
                
                let lati = Double(String(describing : data.arrayValue[j]["lt"]))
                let long = Double(String(describing : data.arrayValue[j]["ln"]))
                path.add(CLLocationCoordinate2D(latitude: lati!, longitude: long!))
                j += 1
            }
            
            let polyline = GMSPolyline(path: path)
            
            polyline.strokeColor = uiFun.hexStringToUIColor(hex: String(describing: rc.arrayValue[i]["color"]))
            //print("Colors of polyline \(rc.arrayValue[i]["color"])")
            polyline.strokeWidth = 3.0
            polyline.map = self.mapView
            let solidRed = GMSStrokeStyle.solidColor(uiFun.hexStringToUIColor(hex: String(describing: rc.arrayValue[i]["color"])))
            polyline.spans = [GMSStyleSpan(style: solidRed)]
            polyline.geodesic = true
            i += 1
            UIApplication.shared.endBackgroundTask(self.backgroundTask)
        }
    }
}
