//
//  AddMoneyIntoWalletViewController.swift
//  RidlrIOS
//
//  Created by Mayur on 03/04/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var isFromProfileWallet = false;
class AddMoneyIntoWalletViewController: UIViewController {

  
    @IBOutlet var emailLabel            : UILabel!
    @IBOutlet var balanceLabel          : UILabel!
    @IBOutlet var toolbarHeadingLabel   : UILabel!
    
    @IBOutlet var TwoButton             : UIButton!
    @IBOutlet var FIveButton            : UIButton!
    @IBOutlet var TenButton             : UIButton!
    
    @IBOutlet var enterAmountTF         : UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        isFromProfileWallet         = true
        let userEmail               = uiFun.readFromDatabase(entityName: "Users", key: "userEmail")
        emailLabel.text             = userEmail
        enterAmountTF.keyboardType  = .numberPad
        
        balanceLabel.text           = citrusWalletBalance
        uiFun.createCleverTapEvent(message: "USER PROFILE ADD MONEY SCREEN")
        
        toolbarHeadingLabel.text    = "\(profileSelectedWallet) WALLET"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func TwoButtonAction(_ sender: Any) {
        // Add 200Rs into selected wallet
        amountToAddValue = "200.0"
        addMoneyFunction(amount: amountToAddValue)
    }
    
    @IBAction func FiveButtonAction(_ sender: Any) {
        // Add 200Rs into selected wallet
        amountToAddValue = "500.0"
        addMoneyFunction(amount: amountToAddValue)
    }
   
    @IBAction func TenActionButton(_ sender: Any) {
        // Add 200Rs into selected wallet
        amountToAddValue = "1000.0"
        addMoneyFunction(amount: amountToAddValue)
    }
    
    @IBAction func addMoneyActionButton(_ sender: Any) {
        // Check for whether user have entered the amount
        if(enterAmountTF.text != ""){
            amountToAddValue = "\(enterAmountTF.text!).0"
            addMoneyFunction(amount: amountToAddValue)
        }else{
            // SHow alert box
            uiFun.showAlert(title: "", message: "Please Enter Amount To Add Money Into Citrus Wallet", logMessage: "", fromController: self)
        }
    }
    
    
    func addMoneyFunction(amount : String){
        // When user clicks on the add money button
        switch profileSelectedWallet {
        case "CITRUS":
            // Will redirect to citrus add money view controller
            uiFun.navigateToScreem(identifierName: "AddMoneyViewController", fromController: self)
            break
        case "MOBIKWIK":
            // makes an api call to add money into mobikwik
            uiFun.showIndicatorLoader()
            let mobiAddMoneyApiCall = MobiFreeWalletAPICall()
            mobiAddMoneyApiCall.makeAPICallProfile(fromViewController: self, amount: amountToAddValue, paymentGateway: "MOBIKWIK")
            break
        case "FREECHARGE":
            // makes an api call to add money into freecharge
            uiFun.showIndicatorLoader()
            let mobiAddMoneyApiCall = MobiFreeWalletAPICall()
            mobiAddMoneyApiCall.makeAPICallProfile(fromViewController: self, amount: amountToAddValue, paymentGateway: "FREECHARGE")
            break
        default:
            break
        }
        
        
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        // back button tap action
        isFromProfileWallet = false
        uiFun.navigateToScreem(identifierName: "UserProfileViewController", fromController: self)
    }
    
    func MobiFreeWalletResponse(response : JSON){
       // Mobikwik and freecharge add money api response
        uiFun.hideIndicatorLoader()
        if(String(describing: response["status"]) == "OK"){
            // API response success
            let jsonData    = response["data"]
            redirectUrl     = String(describing: jsonData["redirectUrl"]) // Redirect url open this url in webview to complete transaction
            uiFun.navigateToScreem(identifierName: "CitrusPayemntWebViewController", fromController: self)
        }else{
            let jsonError = response["error"]
            // Show api response error into the alert box
            uiFun.showAlert(title: "", message: String(describing : jsonError["errorCodeText"]), logMessage: "MobiFreeWalletResponse()", fromController: self)
        }
    }
}
