//
//  FetchUserDetail.swift
//  CINTA
//
//  Created by Mayur on 17/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchUserDetail {
    
    func makeAPICall(fromViewController : ProfileViewController, member_id : String){
        
        let url                         = "\(constant.baseURL)myprofilegetdata.php"
        let params : [String:AnyObject] = ["member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.fetchedUserApiResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
