//
//  SocailWebViewViewController.swift
//  CINTA
//
//  Created by Mayur on 21/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var socialHeader    = ""
var socialURL       = ""
class SocailWebViewViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var toolbarHeader : UILabel!
    @IBOutlet var webView       : UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.delegate    = self
        setUI()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- WebView Delegate Method
    
    public func webViewDidStartLoad(_ webView: UIWebView){
    
        uiFun.showIndicatorLoader()
    }
  
    public func webViewDidFinishLoad(_ webView: UIWebView){
    
        uiFun.hideIndicatorLoader()
    }
    
    // MARK:- UI Function
    func setUI(){
        print("Selected \(socialHeader) & URL is \(socialURL)")
        toolbarHeader.text  = socialHeader
        let url             = URL (string: socialURL)
        let requestObj      = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    }

    // MARK:- Button Action
    @IBAction func backAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
    }
    
}
