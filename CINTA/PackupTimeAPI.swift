//
//  PackupTimeAPI.swift
//  CINTA
//
//  Created by Mayur on 15/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class PackupTimeAPI: NSObject {

    func makeAPICall(fromViewController : NewDiaryEntryViewController, entry_id : String, callin_time : String, callin_location : String, callin_lat : String, callin_lng : String){
        
        let url                         = "\(constant.baseURL)updatePackupTime.php"
        let params : [String:AnyObject] = ["entry_id"        : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "packup_time"     : callin_time.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "packup_location" : callin_location.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "packup_lat"      : callin_lat.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "packup_lng"      : callin_lng.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "packup_entry_type"    : "ManualOnline" as AnyObject]
        
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        CleverTap.sharedInstance().recordEvent("PackUp Time API Call URL \(url)")
        CleverTap.sharedInstance().recordEvent("PackUp Time API Call Params", withProps: params)
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.packUpTimeResponse(response: jsonResponse)
                return
                
            }else {
                print("Diary Entry Failed")
                return
            }
            
        })
        
    }
}
