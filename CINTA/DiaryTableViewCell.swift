//
//  DiaryTableViewCell.swift
//  CINTA
//
//  Created by Mayur on 03/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import RaisePlaceholder
import SearchTextField

class DiaryTableViewCell: UITableViewCell {

    @IBOutlet var autoTextField: SearchTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
