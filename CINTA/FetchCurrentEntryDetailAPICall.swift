//
//  FetchCurrentEntryDetailAPICall.swift
//  CINTA
//
//  Created by Mayur on 10/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchCurrentEntryDetailAPICall {

    func makeAPICall(fromViewController : CreateScheduleEntryViewController, entry_id : String, member_id : String){
        
        let url                         = "\(constant.baseURL)selectInProgressDetails.php"
        let params : [String:AnyObject] = ["entry_id"     : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.currentEventResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
