//
//  UpdateScheduleEntry.swift
//  CINTA
//
//  Created by Mayur on 10/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class UpdateScheduleEntry {
    
    func makeAPICall(fromViewController : CreateScheduleEntryViewController, entry_id : String, member_id : String, ScheduleDate : String, filmName : String, productionHouseName : String, producerName : String, call_time : String){
        
        let url                         = "\(constant.baseURL)updateSchedule.php"
        let params : [String:AnyObject] = ["entry_id"     : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "ScheduleDate" : ScheduleDate.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "filmName"     : filmName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "productionHouseName"     : productionHouseName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "call_time"    : call_time.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "producerName" : producerName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.updteScheduleEntryResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
    
}
