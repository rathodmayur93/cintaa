//
//  UserProfileParser.swift
//  CINTA
//
//  Created by Mayur on 07/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

var userProfileList     = [UserProfileModel]()
class UserProfileParser {
    
    func parseUserProfileJSON(response : JSON){
        
        userProfileList.removeAll()
    
        let profileModel                    = UserProfileModel()
        let nameModal                       = Nameandimage()
        
        
        let nameImageJSON                   = response["Nameandimage"]
        let historyJSON                     = response["HistoryRecords"]
        
        nameModal.name                      = String(describing: nameImageJSON.arrayValue[0]["name"])
        nameModal.gender                    = String(describing: nameImageJSON.arrayValue[0]["gender"])
        nameModal.address                   = String(describing: nameImageJSON.arrayValue[0]["address"])
        nameModal.city                      = String(describing: nameImageJSON.arrayValue[0]["city"])
        nameModal.state                     = String(describing: nameImageJSON.arrayValue[0]["state"])
        nameModal.zip                       = String(describing: nameImageJSON.arrayValue[0]["zip"])
        nameModal.country                   = String(describing: nameImageJSON.arrayValue[0]["country"])
        nameModal.email                     = String(describing: nameImageJSON.arrayValue[0]["email"])
        nameModal.password                  = String(describing: nameImageJSON.arrayValue[0]["password"])
        nameModal.mobile                    = String(describing: nameImageJSON.arrayValue[0]["mobile"])
        nameModal.website                   = String(describing: nameImageJSON.arrayValue[0]["website"])
        nameModal.profile_imgurl            = String(describing: nameImageJSON.arrayValue[0]["profile_imgurl"])
        nameModal.alt_mobile                = String(describing: nameImageJSON.arrayValue[0]["alt_mobile"])
        
        let profile: Dictionary<String, String> = ["name"  : String(describing: nameImageJSON.arrayValue[0]["name"]),
                                                   "gender": String(describing: nameImageJSON.arrayValue[0]["gender"]),
                                                   "city"  : String(describing: nameImageJSON.arrayValue[0]["city"]),
                                                   "email" : String(describing: nameImageJSON.arrayValue[0]["email"]),
                                                   "profile_imgurl" : String(describing: nameImageJSON.arrayValue[0]["profile_imgurl"])]
        CleverTap.sharedInstance().profilePush(profile)
        
        profileModel.nameAndimage.append(nameModal)
        
        userProfileList.append(profileModel)
    }

}
