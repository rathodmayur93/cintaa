//
//  DashboardViewController.swift
//  CINTA
//
//  Created by Mayur on 30/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import Crashlytics

class DashboardViewController: UIViewController {

    @IBOutlet var scheduleEntryView     : UIView!
    @IBOutlet var diaryEntryView        : UIView!
    @IBOutlet var completedShootsView   : UIView!
    let cintaColors                     = Colors()
    var timer                           = Timer()
    var time                            = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        
        //
//        let backgroundService = BackgroundService()
//        backgroundService.fetchRemianingEventsForAPICall()
        
        let pdfGen  = PDFPage()
        pdfGen.generatePDF()
        
        //createGradientLayer()
        
        // Show First Time Loader
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(showLoaderFirstTime){
            showLoaderFirstTime = false
            refreshDatabase()
        }else{
            uiFun.hideIndicatorLoader()
        }
    }
    
    // MARK:- UI Function
    func setUI(){
        
        // Creating gradient background
        var gradientLayer3           = CAGradientLayer()
        gradientLayer3.frame         = scheduleEntryView.bounds
        gradientLayer3.colors        = [uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor, uiFun.hexStringToUIColor(hex: cintaColors.whiteColor).cgColor]
        gradientLayer3.startPoint    = CGPoint(x: 0.0, y: 0.5)
        gradientLayer3.endPoint      = CGPoint(x: 1.0, y: 0.5)
        
        scheduleEntryView.layer.cornerRadius    = 25
        scheduleEntryView.layer.borderWidth     = 1.0
        scheduleEntryView.layer.borderColor     = uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor
        scheduleEntryView.layer.insertSublayer(gradientLayer3, at: 0)
        
        let tapGesture                                = UITapGestureRecognizer(target: self, action: #selector(scheduleEntryTapped))
        tapGesture.numberOfTapsRequired               = 1
        scheduleEntryView.isUserInteractionEnabled = true
        scheduleEntryView.addGestureRecognizer(tapGesture)
        
        
        var gradientLayer1           = CAGradientLayer()
        gradientLayer1.frame         = scheduleEntryView.bounds
        gradientLayer1.colors        = [uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor, uiFun.hexStringToUIColor(hex: cintaColors.whiteColor).cgColor]
        gradientLayer1.startPoint    = CGPoint(x: 0.0, y: 0.5)
        gradientLayer1.endPoint      = CGPoint(x: 1.0, y: 0.5)
        
        self.view.layer.insertSublayer(gradientLayer1, at: 0)
        diaryEntryView.layer.cornerRadius    = 25
        diaryEntryView.layer.borderWidth     = 1.0
        diaryEntryView.layer.borderColor     = uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor
        diaryEntryView.layer.insertSublayer(gradientLayer1, at: 0)
        
        let tapGesture1                                = UITapGestureRecognizer(target: self, action: #selector(diaryEntryTapped))
        tapGesture1.numberOfTapsRequired               = 1
        diaryEntryView.isUserInteractionEnabled = true
        diaryEntryView.addGestureRecognizer(tapGesture1 )
        
        
        var gradientLayer2           = CAGradientLayer()
        gradientLayer2.frame         = scheduleEntryView.bounds
        gradientLayer2.colors        = [uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor, uiFun.hexStringToUIColor(hex: cintaColors.whiteColor).cgColor]
        gradientLayer2.startPoint    = CGPoint(x: 0.0, y: 0.5)
        gradientLayer2.endPoint      = CGPoint(x: 1.0, y: 0.5)
        
        self.view.layer.insertSublayer(gradientLayer2, at: 0)
        completedShootsView.layer.cornerRadius    = 25
        completedShootsView.layer.borderWidth     = 1.0
        completedShootsView.layer.borderColor     = uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor
        completedShootsView.layer.insertSublayer(gradientLayer2, at: 0)
        
        let tapGesture2                                = UITapGestureRecognizer(target: self, action: #selector(completedShootTapped))
        tapGesture2.numberOfTapsRequired               = 1
        completedShootsView.isUserInteractionEnabled = true
        completedShootsView.addGestureRecognizer(tapGesture2)
    }
    
    func createGradientLayer() {
        
        var gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @IBAction func threeDotsActions(_ sender: Any) {
        openActionSheet()
    }
    
    
    
    // MARK:- Button Actions
    func scheduleEntryTapped(){
        
        uiFun.showIndicatorLoader()
        tabBarIndex = 0
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    func diaryEntryTapped(){
        tabBarIndex = 1
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    func completedShootTapped(){
        tabBarIndex = 2
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    @IBAction func refreshButtonAction(_ sender: Any) {
        CleverTap.sharedInstance().recordEvent("Refresh Button Pressed")
        refreshDatabase()
    }
    
    // MARK:- API Call
    func fetchAllCalendarEvents(){
    
        let calendarEvent   = FetchAllEventAPICall()
        calendarEvent.makeAPICall3(fromViewController: self, member_id: uiFun.readData(key: "member_id"))
    }
    
    
    // MARK:- API Responses
    func calendarEventResponse(response : JSON){
        
        // MARK: Clevertap Event
        let props = ["Screen Name": "Dashboard",
                     "All Calendar Event Response": String(describing : response)]
        CleverTap.sharedInstance().recordEvent("Dashoboard", withProps: props)
        
        let fetchBooking         = HistoryRecordDB.shared.fetchBookingItems()
        
        var calendarEventList    = [CalendarEvent]()
        let calendarData         = response["sht_date"]
        calendarEventList.reserveCapacity(calendarData.count)
        
        // Get Current Date
        let date             = Date()
        let formatter        = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result           = formatter.string(from: date)
        var actualArray      = [String : Any]()
        print("Calendar Data Count Is \(calendarData.count) & Database Count Is \(fetchBooking.count)")
        for i in 0 ..< calendarData.count {
            
            let calendarEvents  = CalendarEvent()
            let calendarJSON    = calendarData.arrayValue[i]
            
            // Check for whether entry id exist in current local database or not
            let scanResult = fetchBooking.contains(where: { (shootDate) -> Bool in
                shootDate.entry_id == String(describing: calendarJSON["entry_id"])
            })
            
        if(!scanResult){
            
            print("New Entry \(String(describing: calendarJSON["entry_id"])) & Date Is \(String(describing: calendarJSON["shoot_date"]))")
            
            calendarEvents.entry_id             = String(describing: calendarJSON["entry_id"])
            calendarEvents.film_tv_name         = String(describing: calendarJSON["film_tv_name"])
            calendarEvents.house_name           = String(describing: calendarJSON["house_name"])
            calendarEvents.producer_name        = String(describing: calendarJSON["producer_name"])
            calendarEvents.shoot_date           = String(describing: calendarJSON["shoot_date"])
            
            calendarEvents.schedule_status      = String(describing: calendarJSON["schedule_status"])
            calendarEvents.shift_time           = String(describing: calendarJSON["shift_time"])
            calendarEvents.call_time            = String(describing: calendarJSON["call_time"])
            calendarEvents.shoot_map_location   = String(describing: calendarJSON["shoot_map_location"])
            calendarEvents.lat                  = String(describing: calendarJSON["lat"])
            
            calendarEvents.lng                  = String(describing: calendarJSON["lng"])
            calendarEvents.played_character     = String(describing: calendarJSON["played_character"])
            calendarEvents.rate                 = String(describing: calendarJSON["rate"])
            calendarEvents.due_days             = String(describing: calendarJSON["due_days"])
            calendarEvents.remark               = String(describing: calendarJSON["remark"])
            
            calendarEvents.rate_type            = String(describing: calendarJSON["rate_type"])
            calendarEvents.callin_time          = String(describing: calendarJSON["callin_time"])
            calendarEvents.packup_time          = String(describing: calendarJSON["packup_time"])
            calendarEvents.shoot_location       = String(describing: calendarJSON["shoot_location"])
            
            calendarEventList.append(calendarEvents)
            
            //HistoryRecordDB.shared.updateBookingInDiaryEntry(withID: String(describing: calendarJSON["entry_id"]), calendarEvents: calendarEventList, serviceCall: "1")
            
            
            actualArray.updateValue(uiFun.hexStringToUIColor(hex: cintaColors.calendarEvent), forKey: uiFun.convertDateFormater(date: String(describing: calendarJSON["shoot_date"])))
            //}
        }
        }
        
        // TODO: Database Entries
        let isDatabaseCreated   = HistoryRecordDB.shared.createDatabase()
        
        HistoryRecordDB.shared.insertBookingData(userInfoList: calendarEventList,serviceCall: "1")
        uiFun.hideIndicatorLoader()
        
    }
    
    // MARK:- Common Functions
    
    func goToScreen(){
        
        let fetchBooking    = HistoryRecordDB.shared.fetchBookingItems()
        print("Event Count is \(fetchBooking.count)")
        
        time += 1
        if(time > 5){
            uiFun.hideIndicatorLoader()
            timer.invalidate()
            print("Invalidate Timer")
        }
    }
    
    func openActionSheet(){
        
        let dotMenu      = UIAlertController(title: nil, message: "Select Any Option", preferredStyle: .actionSheet)
        
        let instructionAction       = UIAlertAction(title: "Instructions", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.instructionTapped()
        })
        
        let helpAction       = UIAlertAction(title: "Help Center", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.helpCenterTapped()
        })
        
        let privacyAction       = UIAlertAction(title: "Privacy Policy", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.privacyViewTapped()
        })
        
        let rateAction       = UIAlertAction(title: "Rate Us", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.rateApp()
        })
        
        let shareAction       = UIAlertAction(title: "Share", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let objectsToShare = [constant.shareContent]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        })
        
        let profileAction       = UIAlertAction(title: "My Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.profileTapped()
        })
        
        let mouAction       = UIAlertAction(title: "MOU", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
          self.mouViewTapped()
        })
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

           self.logoutApp()
        })
        
        let exitAction       = UIAlertAction(title: "Exit", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            exit(0)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        dotMenu.addAction(instructionAction)
        dotMenu.addAction(helpAction)
        dotMenu.addAction(privacyAction)
        dotMenu.addAction(rateAction)
        dotMenu.addAction(shareAction)
        dotMenu.addAction(profileAction)
        dotMenu.addAction(mouAction)
        dotMenu.addAction(logoutAction)
        dotMenu.addAction(exitAction)
        dotMenu.addAction(cancelAction)
        
        
        
        self.present(dotMenu, animated: true, completion: nil)
    }
    
    func helpCenterTapped(){
        webViewHeader   = "Help Center"
        loadUrl         = "http://www.cintaa.net/cintaa-help-center/"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func privacyViewTapped(){
        loadUrl         = "http://www.cintaa.net/privacy-policy/"
        webViewHeader   = "Privacy Policy"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func mouViewTapped(){
        loadUrl         = "\(constant.baseURL)mou.pdf"
        webViewHeader   = "MOU"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func rateUsViewTapped(){
        
        webViewHeader   = "Rate Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func instructionTapped(){
        
        loadUrl         = "\(constant.baseURL)instructions.pdf"
        webViewHeader   = "Instruction"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func contactUs(){
        
        loadUrl         = "http://www.cintaa.net/contact-us/"
        webViewHeader   = "Contact Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func profileTapped(){
        uiFun.navigateToScreem(identifierName: "ProfileViewController", fromController: self)
    }
    
    func rateApp() {
        guard let url = URL(string : "https://itunes.apple.com/us/app/cintaa/id1288952015?mt=8") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func logoutApp(){
    
        MemberProfileInfo.shared.dropTable()
        HistoryRecordDB.shared.dropTable()
        AuthorityDatabse.shared.dropTable()
        
        uiFun.writeData(value: "", key: "member_id")
        uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        print("File Logour")

    }
    
    
    func refreshDatabase(){
        
        if(uiFun.isConnectedToNetwork()){
            uiFun.showIndicatorLoader()
            let backgroundService = BackgroundService()
            backgroundService.fetchRemianingEventsForAPICall()
            //logoutApp()
            
            fetchAllCalendarEvents()
            
        }else{
            uiFun.showAlert(title: "Error", message: "You have to online for rescan process ", logMessage: "Rescan Failed", fromController: self)
        }
    }
}
