//
//  ForgotPasswordViewController.swift
//  CINTA
//
//  Created by Mayur on 21/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForgotPasswordViewController: UIViewController {

    @IBOutlet var otpTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Button Actions
    
    @IBAction func verify(_ sender: Any) {
        if(otpTF.text == userOTP){
            uiFun.navigateToScreem(identifierName: "SetNewPasswordViewController", fromController: self)
        }else{
            uiFun.showAlert(title: "Alert", message: "Invalid OTP", logMessage: "Invalid OTP", fromController: self)
        }
    }
    
    @IBAction func resendAction(_ sender: Any) {
        
        
    }
    
    // MARK:- API Call
    
    func resendOTPApiCall(){
        uiFun.showIndicatorLoader()
        let sendOTPAPICall      = SendOTPAPICall()
        sendOTPAPICall.makeAPICallResend(fromViewController: self, memberId: memberId, otp: "AAAAAA", mobile: userMobileNo)
    }
    
    // MARK:- API Response
    
    func resendOTPResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        processResendOTPResponse(response: response)
    }
    
    // MARK:- Process Responses
    
    func processResendOTPResponse(response : JSON){
        
        let status = String(describing : response["success"])
        
        if(status == "1"){
            uiFun.navigateToScreem(identifierName: "SetNewPasswordViewController", fromController: self)
        }
    }
}
