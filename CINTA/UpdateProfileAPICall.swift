//
//  UpdateProfileAPICall.swift
//  CINTA
//
//  Created by Mayur on 21/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class UpdateProfileAPICall {
  
    func makeAPICall(fromViewController : ProfileViewController, member_id : String, name : String, gender : String, address : String, city : String, state : String, country : String, zip : String, email : String, password : String, mobile : String, alt_mobile : String, website : String){
        
        let url                         = "\(constant.baseURL)myprofilegetdata.php"
        let params : [String:AnyObject] = ["member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines)   as AnyObject,
                                           "name"         : name.trimmingCharacters(in: .whitespacesAndNewlines)        as AnyObject,
                                           "gender"       : gender.trimmingCharacters(in: .whitespacesAndNewlines)      as AnyObject,
                                           "address"      : address.trimmingCharacters(in: .whitespacesAndNewlines)     as AnyObject,
                                           "city"         : city.trimmingCharacters(in: .whitespacesAndNewlines)        as AnyObject,
                                           "state"        : state.trimmingCharacters(in: .whitespacesAndNewlines)       as AnyObject,
                                           "country"      : country.trimmingCharacters(in: .whitespacesAndNewlines)     as AnyObject,
                                           "zip"          : zip.trimmingCharacters(in: .whitespacesAndNewlines)         as AnyObject,
                                           "email"        : email.trimmingCharacters(in: .whitespacesAndNewlines)       as AnyObject,
                                           "password"     : password.trimmingCharacters(in: .whitespacesAndNewlines)    as AnyObject,
                                           "mobile"       : zip.trimmingCharacters(in: .whitespacesAndNewlines)         as AnyObject,
                                           "alt_mobile"   : email.trimmingCharacters(in: .whitespacesAndNewlines)       as AnyObject,
                                           "website"      : password.trimmingCharacters(in: .whitespacesAndNewlines)    as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.updateProfileAPIResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
