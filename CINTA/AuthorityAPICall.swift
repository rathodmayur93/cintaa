//
//  AuthorityAPICall.swift
//  CINTA
//
//  Created by Mayur on 15/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class AuthorityAPICall: NSObject {

    func makeAPICall(fromViewController : NewDiaryEntryViewController, entry_id : String, encoded_string : String, img_name : String, authority_sign_date : String, authority_nm : String, authority_number : String, authority_email : String, productionHouse : String, producerName : String, member_id : String, remark : String){
        
        let url                         = "\(constant.baseURL)UpdateAutoritySign.php"
        let params : [String:AnyObject] = ["entry_id"       : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "encoded_string" : encoded_string.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "img_name"       : img_name.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "authority_sign_date" : authority_sign_date.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "authority_nm" : authority_nm.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "authority_number" : authority_number.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           
                                           "authority_email" : authority_email.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "productionHouse" : productionHouse.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "producerName"     : producerName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "member_id"      : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "remark"         : remark.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.packUpTimeResponse(response: jsonResponse)
                return
                
            }else {
                print("Authority Entry Failed")
                return
            }
            
        })
        
    }
}
