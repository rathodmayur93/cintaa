//
//  AuthorityAPICall2.swift
//  CINTA
//
//  Created by Mayur on 19/11/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthorityAPICall2 {

    func makeAPICall(fromViewController : NewDiaryEntryViewController, entry_id : String, encoded_string : String, img_name : String, authority_sign_date : String, authority_nm : String, authority_number : String, authority_email : String, productionHouse : String, producerName : String, member_id : String, remark : String){
        
        let headers                     = ["Content-Type": "application/x-www-form-urlencoded"]
        let postDataString              = "entry_id=\(entry_id)&encoded_string=\(encoded_string)&img_name=\(img_name)&authority_sign_date=\(authority_sign_date)&authority_nm=\(authority_nm)&authority_number=\(authority_number)&authority_email=\(authority_email)&productionHouse=\(productionHouse)&producerName=\(producerName)&member_id=\(member_id)&remark=\(remark)"
        
        print("Authority Params are \(postDataString)")
        CleverTap.sharedInstance().recordEvent("Authority API Call Body \(postDataString)")
        
        let postDataStringData          = postDataString.data(using: String.Encoding.utf8)
        
        let request             = NSMutableURLRequest(url: NSURL(string: "\(constant.baseURL)UpdateAuthoritySignIOS.php")! as URL,
                                                      cachePolicy: .useProtocolCachePolicy,
                                                      timeoutInterval: 60.0)
        request.httpMethod              = "POST"
        request.allHTTPHeaderFields     = headers
        request.httpBody                = postDataStringData! as Data
        
        let session  = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
                uiFun.hideIndicatorLoader()
                if(error.debugDescription.contains("timed out")){
                    uiFun.showAlert(title: "Network Error", message: "Request Timed Out", logMessage: "Req Timed Out", fromController: fromViewController)
                }else if(error.debugDescription.contains("connection was lost")){
                    uiFun.showAlert(title: "Network Error", message: "Network Connection Was Lost. Please Try Again!", logMessage: "connection was lost", fromController: fromViewController)
                }else if(error.debugDescription.contains("appears to be offline")){
                    uiFun.showAlert(title: "Network Error", message: "Network Connection Appears To Be Offline. Please Check!", logMessage: "Network Error", fromController: fromViewController)
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
                
                if let urlContent = data{
                    
                    do {
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        print("Yo Yo \(jsonResult)")
                        DispatchQueue.main.sync(execute: {
                            fromViewController.authorityAPIResponse(response: JSON(jsonResult))
                            
                        })
                    }
                    catch{
                        uiFun.hideIndicatorLoader()
                        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: fromViewController)
                        print("Failed To Convert JSON")
                    }
                }
                
            }
        })
        
        dataTask.resume()
    }
}
