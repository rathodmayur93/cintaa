
//
//  SettingViewController.swift
//  CINTA
//
//  Created by Mayur on 16/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var loadUrl         = ""
var webViewHeader   = ""
class SettingViewController: UIViewController {

    @IBOutlet var userView              : UIView!
    @IBOutlet var profileImageButton    : UIButton!
    @IBOutlet var username              : UILabel!
    @IBOutlet var mobileNumber          : UILabel!
   
    @IBOutlet var helpCenterView        : UIView!
    @IBOutlet var privacyView           : UIView!
    @IBOutlet var mouView               : UIView!
    @IBOutlet var rateUsView            : UIView!
    @IBOutlet var contactUsView         : UIView!
    @IBOutlet var instructionView       : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UI Function
    func setUI(){
        
        //User View Tap
        let tapGesture                              = UITapGestureRecognizer(target: self, action: #selector(userViewTapGesture))
        tapGesture.numberOfTapsRequired             = 1
        userView.isUserInteractionEnabled           = true
        userView.addGestureRecognizer(tapGesture)
        
        //helpCenterView View Tap
        let tapGesture1                                   = UITapGestureRecognizer(target: self, action: #selector(helpCenterTapped))
        tapGesture1.numberOfTapsRequired                  = 1
        helpCenterView.isUserInteractionEnabled           = true
        helpCenterView.addGestureRecognizer(tapGesture1)
        
        //privacyView View Tap
        let tapGesture2                                   = UITapGestureRecognizer(target: self, action: #selector(privacyViewTapped))
        tapGesture2.numberOfTapsRequired                  = 1
        privacyView.isUserInteractionEnabled              = true
        privacyView.addGestureRecognizer(tapGesture2)
        
        //mouView View Tap
        let tapGesture3                                   = UITapGestureRecognizer(target: self, action: #selector(mouViewTapped))
        tapGesture3.numberOfTapsRequired                  = 1
        mouView.isUserInteractionEnabled                  = true
        mouView.addGestureRecognizer(tapGesture3)
        
        //rateUsView View Tap
        let tapGesture4                                   = UITapGestureRecognizer(target: self, action: #selector(helpCenterTapped))
        tapGesture4.numberOfTapsRequired                  = 1
        rateUsView.isUserInteractionEnabled               = true
        rateUsView.addGestureRecognizer(tapGesture4)
        
        //instruction View Tap
        let tapGesture5                                   = UITapGestureRecognizer(target: self, action: #selector(instructionTapped))
        tapGesture5.numberOfTapsRequired                  = 1
        instructionView.isUserInteractionEnabled          = true
        instructionView.addGestureRecognizer(tapGesture5)
        
        //instruction View Tap
        let tapGesture6                                   = UITapGestureRecognizer(target: self, action: #selector(contactUs))
        tapGesture6.numberOfTapsRequired                  = 1
        contactUsView.isUserInteractionEnabled            = true
        contactUsView.addGestureRecognizer(tapGesture6)
        
        
        let memberInfo      = MemberProfileInfo.shared.fetchBookingItems()
        username.text       = memberInfo[0].nameAndimage[0].name
        mobileNumber.text   = memberInfo[0].nameAndimage[0].mobile
    }

    // MARK:- Common Function
    func userViewTapGesture(){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func helpCenterTapped(){
        webViewHeader   = "Help Center"
        loadUrl         = "http://www.cintaa.net/cintaa-help-center/"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func privacyViewTapped(){
        loadUrl         = "http://www.cintaa.net/privacy-policy/"
        webViewHeader   = "Privacy Policy"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func mouViewTapped(){
        loadUrl         = "\(constant.baseURL)mou.pdf"
        webViewHeader   = "MOU"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func rateUsViewTapped(){
        
        webViewHeader   = "Rate Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func instructionTapped(){
    
        loadUrl         = "\(constant.baseURL)instructions.pdf"
        webViewHeader   = "Instruction"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func contactUs(){
    
        loadUrl         = "http://www.cintaa.net/contact-us/"
        webViewHeader   = "Contact Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
}
