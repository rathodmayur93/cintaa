//
//  AuthorityDatabse.swift
//  CINTA
//
//  Created by Mayur on 07/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class AuthorityDatabse: NSObject {

    let field_entry_id                            = "entry_id"
    let field_authority_name                      = "authority_name"
    let field_authority_contact                   = "authority_contact"
    let field_authority_sign                      = "authority_sign"
    let field_authority_email                     = "authority_email"
   
    var pathToDatabase  : String!
    var database        : FMDatabase!
    static let shared   : AuthorityDatabse     = AuthorityDatabse()
    let databaseFileName                      = "cintaAuthority.sqlite"
    let tableName                             = "authorityRecords"

    
    override init() {
        super.init()
        
        let documentsDirectory  = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase          = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createUserInfoTableQuery = "create table if not exists \(tableName) (\(field_entry_id) varchar primary key not null, \(field_authority_name) varchar not null, \(field_authority_contact) varcahr not null, \(field_authority_sign) text not null, \(field_authority_email) varchar)"
                    
                    do {
                        try database.executeUpdate(createUserInfoTableQuery, values: nil)
                        created = true
                        print("Database created succesfully \(tableName)")
                    }
                    catch {
                        
                        print(error.localizedDescription)
                        print("Database creation failed \(tableName)")
                    }
                    
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    
    // MARK:- Open Database
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }else{
                createDatabase()
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        return false
    }
    
    // MARK:- Insert into Database
    func insertBookingData(userInfoList : [AuthorityModel], entry_id : String){
        
        if openDatabase(){
            
                var query = ""
            
                let authority_name              = userInfoList[0].authority_name
                let authority_email             = userInfoList[0].authority_email
                let authority_contact           = userInfoList[0].authority_contact
                let authority_sign              = userInfoList[0].authority_sign
                
                query += "insert into \(tableName) (\(field_entry_id), \(field_authority_name), \(field_authority_contact), \(field_authority_sign), \(field_authority_email)) values ('\(entry_id)', '\(authority_name)', '\(authority_contact)', '\(authority_sign)', '\(authority_email)');"
                
                print("Insert authority details Of \(entry_id)")
            
            
            if !database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }
            
            database.close()
        }
    }
    
    
    // MARK:- Fetch From  Database
    func fetchBookingItems() -> [AuthorityModel]{
        
        var userDetail = [AuthorityModel]()
        
        if openDatabase() {
            let query = "select * from \(tableName)"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let userProfileModel                    = AuthorityModel()
                
                    userProfileModel.authority_name         = results.string(forColumn: field_authority_name)
                    userProfileModel.authority_contact      = results.string(forColumn: field_authority_contact)
                    userProfileModel.authority_sign         = results.string(forColumn: field_authority_sign)
                    userProfileModel.authority_email        = results.string(forColumn: field_authority_email)
                    userProfileModel.entry_id               = results.string(forColumn: field_entry_id)

                    userDetail.append(userProfileModel)
                    print("User Detail Authority \(userDetail) & \(userProfileModel)")
                    
                }
            }
            catch {
                print("Database Error \(error.localizedDescription)")
            }
            
            database.close()
        }
        return userDetail
    }
    
    func fetchAuthorityDetailOfEntryId(entryId : String) -> [AuthorityModel]{
    
        var userDetail = [AuthorityModel]()
        
        if openDatabase() {
            let query = "select * from \(tableName) where \(field_entry_id) = '\(entryId)'"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let userProfileModel                    = AuthorityModel()
                    
                    userProfileModel.authority_name         = results.string(forColumn: field_authority_name)
                    userProfileModel.authority_contact      = results.string(forColumn: field_authority_contact)
                    userProfileModel.authority_sign         = results.string(forColumn: field_authority_sign)
                    userProfileModel.authority_email        = results.string(forColumn: field_authority_email)
                    userProfileModel.entry_id               = results.string(forColumn: field_entry_id)
                    
                    userDetail.append(userProfileModel)
                    print("User Detail Authority \(userDetail) & \(userProfileModel)")
                    
                }
            }
            catch {
                print("Database Error \(error.localizedDescription)")
            }
            
            database.close()
        }
        return userDetail
    }
    
    
    // MARK:- Update Database
    func updateBookingInDiaryEntry(withID entry_id: String, calendarEvents : [AuthorityModel]) {
        if openDatabase() {
            
            let query = "update \(tableName) set \(field_authority_name)=?, \(field_authority_contact)=?, \(field_authority_sign)=?, \(field_authority_email)=? where \(field_entry_id)=?"
            
            do {
                try database.executeUpdate(query, values: [calendarEvents[0].authority_name, calendarEvents[0].authority_contact, calendarEvents[0].authority_sign, calendarEvents[0].authority_email, entry_id])
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
    }
    
    func dropTable(){
        
        if openDatabase() {
            let query = "DELETE FROM \(tableName)"
            print(query)
            do {
                showLoaderFirstTime = true
                uiFun.writeData(value: "", key: "member_id")
                print(database)
                let results = try database.executeUpdate(query, withArgumentsIn: nil)
                database    = nil
                
            }catch{
                print(error.localizedDescription)
            }
        }
    }
}
