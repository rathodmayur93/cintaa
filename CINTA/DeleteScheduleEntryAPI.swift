//
//  DeleteScheduleEntryAPI.swift
//  CINTA
//
//  Created by Mayur on 17/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class DeleteScheduleEntryAPI: NSObject {

    func makeAPICall(fromViewController : CreateScheduleEntryViewController, entry_id : String, member_id : String){
        
        let url                         = "\(constant.baseURL)deleteSchedule.php"
        let params : [String:AnyObject] = ["entry_id"     : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                print("===================================")
                print("Got Delete Response \(jsonResponse)")
                fromViewController.deleteScheduleEntryAPIResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
