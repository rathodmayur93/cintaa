//
//  ContainerEvenDetailViewController.swift
//  CINTA
//
//  Created by Mayur on 25/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class ContainerEvenDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Delegate Mathods Of TableView
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return multiEventList.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        let cell                    = tableView.dequeueReusableCell(withIdentifier: "Cell") as! EvenDetailTableViewCell
        
        cell.tvSerialName.text      = multiEventList[indexPath.row].film_tv_name
        cell.productionHouse.text   = multiEventList[indexPath.row].productioHouse
        cell.producerName.text      = multiEventList[indexPath.row].producerName
        cell.callTime.text          = multiEventList[indexPath.row].call_time
        return cell
    }
    
    
    
    @IBAction func closeButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callForAlert2"), object: nil)
    }
}
