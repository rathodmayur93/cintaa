//
//  MovieListParser.swift
//  CINTA
//
//  Created by Mayur on 26/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var multipleMovieList   = [CalendarEvent]()

class MovieListParser {

    func parsemovieList(index : Int){
        
        let historyRecordsFromDB        = HistoryRecordDB.shared.fetchBookingItems()
        let calendarEvent               = CalendarEvent()
        
        calendarEvent.entry_id              = historyRecordsFromDB[index].entry_id
        calendarEvent.film_tv_name          = historyRecordsFromDB[index].film_tv_name
        calendarEvent.house_name            = historyRecordsFromDB[index].house_name
        calendarEvent.producer_name         = historyRecordsFromDB[index].producer_name
        calendarEvent.shoot_date            = historyRecordsFromDB[index].shoot_date
        calendarEvent.schedule_status       = historyRecordsFromDB[index].schedule_status
        calendarEvent.shift_time            = historyRecordsFromDB[index].shift_time
        calendarEvent.call_time             = historyRecordsFromDB[index].call_time
        calendarEvent.shoot_map_location    = historyRecordsFromDB[index].shoot_map_location
        calendarEvent.lat                   = historyRecordsFromDB[index].lat
        calendarEvent.lng                   = historyRecordsFromDB[index].lng
        
        calendarEvent.played_character      = historyRecordsFromDB[index].played_character
        calendarEvent.rate                  = historyRecordsFromDB[index].rate
        calendarEvent.due_days              = historyRecordsFromDB[index].due_days
        calendarEvent.remark                = historyRecordsFromDB[index].remark
        calendarEvent.rate_type             = historyRecordsFromDB[index].rate_type
        calendarEvent.callin_time           = historyRecordsFromDB[index].callin_time
        calendarEvent.packup_time           = historyRecordsFromDB[index].packup_time
        calendarEvent.shoot_location        = historyRecordsFromDB[index].shoot_location
        calendarEvent.shoot_map_location    = historyRecordsFromDB[index].shoot_map_location
        
        
        multipleMovieList.append(calendarEvent)
    }
    
    func parseMovieListDB(index : Int, date : String){
    
        let historyRecordsFromDB        = HistoryRecordDB.shared.fetchEventOfParticularDate(date: date)
        let calendarEvent               = CalendarEvent()
        
        calendarEvent.entry_id              = historyRecordsFromDB[index].entry_id
        calendarEvent.film_tv_name          = historyRecordsFromDB[index].film_tv_name
        calendarEvent.house_name            = historyRecordsFromDB[index].house_name
        calendarEvent.producer_name         = historyRecordsFromDB[index].producer_name
        calendarEvent.shoot_date            = historyRecordsFromDB[index].shoot_date
        calendarEvent.schedule_status       = historyRecordsFromDB[index].schedule_status
        calendarEvent.shift_time            = historyRecordsFromDB[index].shift_time
        calendarEvent.call_time             = historyRecordsFromDB[index].call_time
        calendarEvent.shoot_map_location    = historyRecordsFromDB[index].shoot_map_location
        calendarEvent.lat                   = historyRecordsFromDB[index].lat
        calendarEvent.lng                   = historyRecordsFromDB[index].lng
        
        calendarEvent.played_character      = historyRecordsFromDB[index].played_character
        calendarEvent.rate                  = historyRecordsFromDB[index].rate
        calendarEvent.due_days              = historyRecordsFromDB[index].due_days
        calendarEvent.remark                = historyRecordsFromDB[index].remark
        calendarEvent.rate_type             = historyRecordsFromDB[index].rate_type
        calendarEvent.callin_time           = historyRecordsFromDB[index].callin_time
        calendarEvent.packup_time           = historyRecordsFromDB[index].packup_time
        calendarEvent.shoot_location        = historyRecordsFromDB[index].shoot_location
        calendarEvent.shoot_map_location    = historyRecordsFromDB[index].shoot_map_location
        
        
        multipleMovieList.append(calendarEvent)
    }
}
