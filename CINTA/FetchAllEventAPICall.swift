//
//  FetchAllEventAPICall.swift
//  CINTA
//
//  Created by Mayur on 23/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class FetchAllEventAPICall: NSObject {

    func makeAPICall(fromViewController : ViewController, member_id : String){
        
        let url                         = "\(constant.baseURL)selectAllCalenderEvents.php"
        let params : [String:AnyObject] = ["member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.calendarEventResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
    
    func makeAPICall2(fromViewController : NewDiaryEntryViewController, member_id : String){
        
        let url                         = "\(constant.baseURL)selectAllCalenderEvents.php"
        let params : [String:AnyObject] = ["member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.calendarEventResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
    
    func makeAPICall3(fromViewController : DashboardViewController, member_id : String){
        
        let url                         = "\(constant.baseURL)selectAllCalenderEvents.php"
        let params : [String:AnyObject] = ["member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.calendarEventResponse(response: jsonResponse)
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
