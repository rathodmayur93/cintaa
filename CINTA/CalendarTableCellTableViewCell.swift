//
//  CalendarTableCellTableViewCell.swift
//  CINTA
//
//  Created by Mayur on 15/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CalendarTableCellTableViewCell: UITableViewCell {

    
    @IBOutlet var colorView             : UIView!
    @IBOutlet var timeView              : UIView!
    @IBOutlet var productionHouseLabel  : UILabel!
    @IBOutlet var dateLabel             : UILabel!
    @IBOutlet var timeLabel             : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        colorView.layer.cornerRadius = colorView.frame.size.width/2
        colorView.backgroundColor   = UIColor.blue
        
        timeView.layer.cornerRadius      = 5.0
        timeView.backgroundColor         = UIColor.blue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
