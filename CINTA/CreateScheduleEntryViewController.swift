//
//  CreateScheduleEntryViewController.swift
//  CINTA
//
//  Created by Mayur on 17/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import RaisePlaceholder
import SwiftyJSON
import SearchTextField

var showDeleteDialog    = false
class CreateScheduleEntryViewController: UIViewController {

    
    @IBOutlet var dateTF            : SearchTextField!
    @IBOutlet var filmTVSerialName  : SearchTextField!
    @IBOutlet var productionHouse   : SearchTextField!
    @IBOutlet var productionName    : SearchTextField!
    @IBOutlet var timeTF            : SearchTextField!
    
    @IBOutlet var createNewBT       : UIButton!
    @IBOutlet var saveBT            : UIButton!
    @IBOutlet var cancelBT          : UIButton!
    
    let datePickerView              : UIDatePicker   = UIDatePicker()
    
    var entry_id                    = ""
    var errorText                   = ""
    var currentTimeStatus           = ""
    
    // MARK: Predictive Search List
    var tvSerialList                    = [String]()
    var productionHouseList             = [String]()
    var producerList                    = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        findOutListValues()
        CleverTap.sharedInstance().recordEvent("Create Schedule Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK:- UI Function
    func setUI(){
    
        saveBT.layer.cornerRadius       = 10.0
        createNewBT.layer.cornerRadius  = 10.0
        dateTF.text                     = scheduleDate
        dateTF.isEnabled                = false
        
        if(editScheduleEntry){
            
            saveBT.setTitle("Save Changes", for: .normal)
            cancelBT.setTitle("Delete Entry", for: .normal)
            
            let selected_entry_id           = multiEventList[eventIndex].entry_id
            let fetch_all_calendar_event    = HistoryRecordDB.shared.fetchBookingItems()
            
            for i in 0..<fetch_all_calendar_event.count{
            
                if(fetch_all_calendar_event[i].entry_id == selected_entry_id){
                    
                    self.filmTVSerialName.text   = fetch_all_calendar_event[i].film_tv_name
                    self.productionHouse.text    = fetch_all_calendar_event[i].house_name
                    self.productionName.text     = fetch_all_calendar_event[i].producer_name
                    self.timeTF.text             = fetch_all_calendar_event[i].call_time
                    entry_id                     = fetch_all_calendar_event[i].entry_id
                    
                    // Hide Delete Entry Button if intime is punched
                    print("Call in time crete schedule entry \(fetch_all_calendar_event[i].callin_time)")
                    if(fetch_all_calendar_event[i].callin_time == "" || fetch_all_calendar_event[i].callin_time == "null" || fetch_all_calendar_event[i].callin_time == "NA"){
                        cancelBT.isHidden   = false
                    }else{
                        cancelBT.isHidden   = true
                    }
                }
            }
        }
    }
    
    // MARK:- Button Actions
    
    @IBAction func saveBTAction(_ sender: Any) {
        
        if(editScheduleEntry){
            EditScheduleEntries()
        }else{
            if(validateEntries()){
                ScheduleEntries()
            }else{
                uiFun.showAlert(title: "Error", message: errorText, logMessage: "Missing Field Create Entry", fromController: self)
            }
        }
    }
    
    @IBAction func createNewAction(_ sender: Any) {
        
        if(validateEntries()){
            
            ScheduleEntries()
            filmTVSerialName.text   = ""
            productionHouse.text    = ""
            productionName.text     = ""
            timeTF.text             = ""
            editScheduleEntry       = false
        }else{
            uiFun.showAlert(title: "Error", message: errorText, logMessage: "Missing Field Create Entry", fromController: self)
        }
    }
    
    
    @IBAction func callTimeAction(_ sender: UITextView) {
        
        datePickerView.datePickerMode   = UIDatePickerMode.time
        sender.inputView                = datePickerView
        timeTF.text                     = "\(getCurrentTime()) \(currentTimeStatus)"
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        if(cancelBT.titleLabel?.text! == "Cancel"){
            uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
        }else if(cancelBT.titleLabel?.text! == "Delete Entry"){
            deleteScheduleAPI()
        }
    }
    
    
    
    
    // MARK:- API Call
    func ScheduleAPICall(dateString : String){
    
        uiFun.showIndicatorLoader()
        
        let scheduleAPICall = CreateScheduleAPI()
        scheduleAPICall.makeAPICall(fromViewController: self, entry_id: "\(uiFun.readData(key: "member_id"))_\(dateString)", member_id: uiFun.readData(key: "member_id"), ScheduleDate: scheduleDate, filmName: filmTVSerialName.text!,productionHouseName: productionHouse.text!, producerName: productionName.text!, callTime: timeTF.text!)
    }
    
    func fetchCurrentEntryDetailAPICall(){
        
        let fetchDetail = FetchCurrentEntryDetailAPICall()
        fetchDetail.makeAPICall(fromViewController: self, entry_id: multiEventList[eventIndex].entry_id, member_id: uiFun.readData(key: "member_id"))
    }
    
    func updateScheduleEntry(){
        
        uiFun.showIndicatorLoader()
        let updateScheduleEntry = UpdateScheduleEntry()
        updateScheduleEntry.makeAPICall(fromViewController: self, entry_id: multiEventList[eventIndex].entry_id, member_id: uiFun.readData(key: "member_id"), ScheduleDate: dateTF.text!, filmName: filmTVSerialName.text!, productionHouseName: productionHouse.text!, producerName: productionName.text!, call_time: timeTF.text!)
    }
    
    func getUserProfileAPICall(){
        
        let userProfileAPI      = UserProfileAPICall()
        userProfileAPI.makeAPICall2(fromViewController: self, memberId: uiFun.readData(key: "member_id"))
    }
    
    func deleteScheduleAPI(){
        uiFun.showIndicatorLoader()
        let deleteEvent     = DeleteScheduleEntryAPI()
        deleteEvent.makeAPICall(fromViewController: self, entry_id: multiEventList[eventIndex].entry_id, member_id: uiFun.readData(key: "member_id"))
    }
    
    // MARK:- API Response
    func scheduleEntryResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        clearAllTextField()
        //getUserProfileAPICall()
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    func userProfileResponse(response : JSON){
        
        uiFun.hideIndicatorLoader()
        processUserProfileResponse(response: response)
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    
    func currentEventResponse(response : JSON){
        
        let inProgressDetail    = response["InProgressDetails"]
        
        filmTVSerialName.text   = String(describing : inProgressDetail.arrayValue[0]["film_tv_name"])
        productionHouse.text    = String(describing : inProgressDetail.arrayValue[0]["house_name"])
        productionName.text     = String(describing : inProgressDetail.arrayValue[0]["producer_name"])
    }
    
    func updteScheduleEntryResponse(response : JSON){
        
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
        uiFun.hideIndicatorLoader()
        clearAllTextField()
    }
    
    func deleteScheduleEntryAPIResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        
        let deleteEntryDB   = HistoryRecordDB.shared.deleteBookingItem(entryId: multiEventList[eventIndex].entry_id)
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
        showDeleteDialog    = true
    }
    // MARK:- Process JSON Responses
    
    func processUserProfileResponse(response : JSON){
        
        fillSelectionColors.removeAll()
        
        print("Gotcha User Profile")
        
        let parseUserProfile        = UserProfileParser()
        parseUserProfile.parseUserProfileJSON(response: response)
        
        var actualArray             = [String : Any]()
        for i in 0..<userProfileList[0].historyRecords.count{
            actualArray.updateValue(uiFun.hexStringToUIColor(hex: cintaColors.calendarEvent), forKey: uiFun.convertDateFormater(date: userProfileList[0].historyRecords[i].shoot_date))
        }
        
        fillSelectionColors = actualArray as! [String : UIColor]
    }
    
    
    // MARK:- Predictive Search Functions
    
    func findOutListValues(){
        
        let calendarEvents  = HistoryRecordDB.shared.fetchBookingItems()
        
        for i in 0..<calendarEvents.count{
            
            // Filter List Of Film/TVSerial
            tvSerialList.append(calendarEvents[i].film_tv_name)
            filmTVSerialName.filterStrings(tvSerialList)
            HandlerOfSelectDropDown2(textField: filmTVSerialName)
            
            // Filter List Of Production House
            productionHouseList.append(calendarEvents[i].house_name)
            productionHouse.filterStrings(productionHouseList)
            HandlerOfSelectDropDown2(textField: productionHouse)
            
            // Filter List Of Producer
            producerList.append(calendarEvents[i].producer_name)
            productionName.filterStrings(producerList)
            HandlerOfSelectDropDown2(textField: productionHouse)
            
        }
    }
    
    func HandlerOfSelectDropDown2(textField : SearchTextField){
        
        // Set theme - Default: light
        textField.theme = SearchTextFieldTheme.lightTheme()
        
        // Define a header - Default: nothing
        let header = UILabel(frame: CGRect(x: 0, y: 0, width: textField.frame.width, height: 30))
        header.backgroundColor              = UIColor.lightGray.withAlphaComponent(0.3)
        header.textAlignment                = .center
        header.font                         = UIFont.systemFont(ofSize: 14)
        header.text                         = "Pick your option"
        textField.resultsListHeader  = header
        
        
        // Modify current theme properties
        textField.theme.font             = UIFont.systemFont(ofSize: 12)
        textField.theme.bgColor          = UIColor.white
        textField.theme.borderColor      = UIColor.white
        textField.theme.separatorColor   = UIColor.white
        textField.theme.cellHeight       = 50
        textField.theme.placeholderColor = UIColor.lightGray
        
        // Max number of results - Default: No limit
        textField.maxNumberOfResults     = 15
        
        // Max results list height - Default: No limit
        textField.maxResultsListHeight   = 200
        
        // Set specific comparision options - Default: .caseInsensitive
        textField.comparisonOptions      = [.caseInsensitive]
        
        // You can force the results list to support RTL languages - Default: false
        textField.forceRightToLeft       = false
        
        // Customize highlight attributes - Default: Bold
        textField.highlightAttributes    = [NSBackgroundColorAttributeName: UIColor.yellow, NSFontAttributeName:UIFont.boldSystemFont(ofSize: 14)]
        
        // Handle item selection - Default behaviour: item title set to the text field
        textField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            textField.text        = item.title
            if(self.filmTVSerialName.isFirstResponder){
                self.fillPartialDetails(selectedText: item.title)
            }
        }
    }
    
    func fillPartialDetails(selectedText : String){
        
        if(filmTVSerialName.isFirstResponder){
            
            let calendarEvents  = HistoryRecordDB.shared.fetchBookingItems()
            for i in 0..<calendarEvents.count{
                
                if(selectedText == calendarEvents[i].film_tv_name){
                    
                    filmTVSerialName.text       = calendarEvents[i].film_tv_name
                    productionHouse.text        = calendarEvents[i].house_name
                    productionName.text         = calendarEvents[i].producer_name
                }
            }
        }
    }
    
    // MARK:- Common Function
    func clearAllTextField(){
    
        filmTVSerialName.text   = ""
        productionName.text     = ""
        productionHouse.text    = ""
        timeTF.text             = ""
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateStyle     = DateFormatter.Style.none
        dateFormatter.timeStyle     = DateFormatter.Style.short
        timeTF.text                 = dateFormatter.string(from: sender.date)
        
    }
    
    func EditScheduleEntries(){
    
        if(uiFun.isConnectedToNetwork()){
            updateScheduleEntry()
            HistoryRecordDB.shared.updateBookingDataScheduleEntry(withID: entry_id, film_tv_name: filmTVSerialName.text!, house_name: productionHouse.text!, producer_name: productionName.text!,call_time: timeTF.text!, serviceCall: "1")
        }else{
            HistoryRecordDB.shared.updateBookingDataScheduleEntry(withID: entry_id, film_tv_name: filmTVSerialName.text!, house_name: productionHouse.text!, producer_name: productionName.text!,call_time: timeTF.text!, serviceCall: "0.1")
        }
    }
    
    func ScheduleEntries(){
    
        let timestamp   = NSDate().timeIntervalSince1970
        let date        = NSDate(timeIntervalSince1970: timestamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        print("Date \(dateString)")

        if(uiFun.isConnectedToNetwork()){
            ScheduleAPICall(dateString: dateString)
            let calendarEventList   = ScheduleEntryCalendarEvent(dateString: dateString)
            HistoryRecordDB.shared.insertBookingData(userInfoList: calendarEventList, serviceCall: "1")
        }else{
            let calendarEventList   = ScheduleEntryCalendarEvent(dateString: dateString)
            HistoryRecordDB.shared.insertBookingData(userInfoList: calendarEventList, serviceCall: "0.1")
        }
    }
    
    func ScheduleEntryCalendarEvent(dateString : String) -> [CalendarEvent]{
    
        var calendarEventList   = [CalendarEvent]()
        let calendarEvent       = CalendarEvent()
        
        calendarEvent.entry_id      = "\(uiFun.readData(key: "member_id"))_\(dateString))"
        calendarEvent.shoot_date    = scheduleDate
        calendarEvent.film_tv_name  = filmTVSerialName.text!
        calendarEvent.house_name    = productionHouse.text!
        calendarEvent.producer_name = productionName.text!
        calendarEvent.call_time     = timeTF.text!
        
        calendarEventList.append(calendarEvent)
        return calendarEventList
    }
    
    func validateEntries() -> Bool{
    
        if(filmTVSerialName.text == ""){
            errorText = "Oops...looks like you forgot to enter Film/TV Serial Name!"
            return false
        }else if(productionName.text == ""){
            errorText = "Oops...looks like you forgot to enter Producer Name!"
            return false
        }else if(productionHouse.text == ""){
            errorText = "Oops...looks like you forgot to enter Productio House Name!"
            return false
        }else if(timeTF.text == ""){
            errorText = "Oops...looks like you forgot to enter Call Time!"
            return false
        }else{
            return true
        }
    }
    
    func getCurrentTime() -> String{
        
        let date         = Date()
        let calendar     = Calendar.current
        var hour         = calendar.component(.hour, from: date)
        var minutes      = calendar.component(.minute, from: date)
        var minuteString = ""
        var hourString   = ""
        
        print("Hour is \(hour)")
        print("Minute is \(minutes)")
        
        if(hour > 12){
            hour                = hour - 12
            hourString          = "\(hour)"
            currentTimeStatus   = "PM"
            
        }else if(hour == 0){
            hour = 12
            hourString  = "12"
        }else if(hour == 12){
            
            hour = 12
            hourString  = "12"
        }else{
            hourString  = "\(hour)"
        }
        
        if (minutes < 10){
            minuteString = "0\(minutes)"
        }else{
            minuteString = "\(minutes)"
        }
        
        if hour < 10 {
            hourString    = "0\(hour)"
        }
        
        return "\(hourString):\(minuteString)"
    }
}
