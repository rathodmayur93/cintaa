//
//  ViewController.swift
//  CINTA
//
//  Created by Mayur on 14/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import Crashlytics

let uiFun               = UiUtillity()
let constant            = Constant()
let util                = Utillity()

var memberId        = ""
var userMobileNo    = ""
var userOTP         = ""

class ViewController: UIViewController {

    
    @IBOutlet var emailTF               : UITextField!
    @IBOutlet var passwordTF            : UITextField!
    @IBOutlet var loginBT               : UIButton!
    @IBOutlet var forgotPasswordLabel   : UIButton!
    
    var mobileNo                    = ""
    var cintaColors                 = Colors()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUI()
        CleverTap.sharedInstance().recordEvent("Login Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        //loginBT.center.y    = 520.0
    }

    func setUI(){
    
        passwordTF.isSecureTextEntry    = true
     
        emailTF.layer.cornerRadius      = 21.0
        emailTF.layer.borderWidth       = 1.0
        emailTF.layer.borderColor       = uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor
        
        passwordTF.layer.cornerRadius      = 21.0
        passwordTF.layer.borderWidth       = 1.0
        passwordTF.layer.borderColor       = uiFun.hexStringToUIColor(hex: cintaColors.strokeColor).cgColor
        
        loginBT.layer.cornerRadius      = 21.0
        
        emailTF.layer.masksToBounds     = true
        passwordTF.layer.masksToBounds  = true
        loginBT.layer.masksToBounds     = true
        
        // Before Entering The Password. -> UI
        passwordTF.isHidden             = true
        forgotPasswordLabel.isHidden    = true
        loginBT.center.y                = 200.0
        loginBT.setTitle("Next", for: .normal)
        
        
        // MARK:- Creating Database
        MemberProfileInfo.shared.createDatabase()
        AuthorityDatabse.shared.createDatabase()
        HistoryRecordDB.shared.createDatabase()
        //MemberProfileInfo.shared.insertBookingDataData(userInfoList: userProfileList)
    }
   
    // Hide Keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // On clicking out side of keyboard the keyboard will get disappear
        self.view.endEditing(true)
        
    }
    
    // MARK:- Button Actions
    
    @IBAction func loginAction(_ sender: Any) {
        uiFun.showIndicatorLoader()
        
        if(uiFun.isConnectedToNetwork()){
            if(loginBT.titleLabel?.text == "Next"){
                MemberAPICall()
            }else if(loginBT.titleLabel?.text == "Sign In"){
                checkPasswordAPICall()
            }
        }else{
            uiFun.showAlert(title: "No Internet", message: "Oops..Look like thre is no internet connection.", logMessage: "no internet", fromController: self)
        }
    }
    
    @IBAction func facebookAction(_ sender: Any) {
        socialHeader    = "Facebook"
        socialURL       = "https://www.facebook.com/cintaamumbai/"
        uiFun.navigateToScreem(identifierName: "SocailWebViewViewController", fromController: self)
    }

    @IBAction func gPlusAction(_ sender: Any) {
        socialHeader    = "YouTube"
        socialURL       = "https://www.youtube.com/c/cintaatv"
        uiFun.navigateToScreem(identifierName: "SocailWebViewViewController", fromController: self)
    }
    
    @IBAction func twitterAction(_ sender: Any) {
        socialHeader    = "Twitter"
        socialURL       = "https://twitter.com/cintaaofficial"
        uiFun.navigateToScreem(identifierName: "SocailWebViewViewController", fromController: self)
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if(emailTF.text! != ""){
            ForgotPassWordAPICall()
        }else{
            uiFun.showAlert(title: "Alert", message: "Please Enter Member Id", logMessage: "Member Id TextField Blank", fromController: self)
        }
    }
   
    /*************************************************************************************************/
    // MARK:- API Calls
    func MemberAPICall(){
        uiFun.showIndicatorLoader()
        let cintaMemberAPICall = CINTAMemberAPICall()
        cintaMemberAPICall.makeAPICall(fromViewController: self, memberId: emailTF.text!)
    
    }
    
    func checkPasswordAPICall(){
    
        let checkPasswordAPI    = CheckPasswordAPICall()
        checkPasswordAPI.makeAPICall(fromViewController: self, memberId: emailTF.text!, password: passwordTF.text!)
    }
    
    func ForgotPassWordAPICall(){

        userOTP                 = randomString(length: 6)
        let sendOTPAPICall      = SendOTPAPICall()
        sendOTPAPICall.makeAPICall(fromViewController: self, memberId: emailTF.text!, otp: userOTP, mobile: mobileNo)
        uiFun.navigateToScreem(identifierName: "ForgotPasswordViewController", fromController: self)
        
    }
    
    func getUserProfileAPICall(){
        
        let userProfileAPI      = UserProfileAPICall()
        userProfileAPI.makeAPICall(fromViewController: self, memberId: emailTF.text!)
    }
    
    func AllCalendarEventAPICall(){
        
        let calendarEvent   = FetchAllEventAPICall()
        calendarEvent.makeAPICall(fromViewController: self, member_id: emailTF.text!)
    }

    /*****************************************************************************************************/
    // MARK:- Responses
    
    func cintaMemberAPIResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        memberId    = emailTF.text!
        processLoginResponse(response: response)
    }
    
    func passwordCheckAPIResponse(response : JSON){
        
        processPasswordCheckResponse(response: response)
        userMobileNo    = mobileNo
    }
    
    func userProfileResponse(response : JSON){
        
        processUserProfileResponse(response: response)
        uiFun.navigateToScreem(identifierName: "DashboardViewController", fromController: self)
    }
    
    func calendarEventResponse(response : JSON){
        
        //processCalendarResponse(response: response)
        uiFun.navigateToScreem(identifierName: "DashboardViewController", fromController: self)
        let queueName   = DispatchQueue(label: "com.cinta.calendarEntries")
        queueName.async {
            self.processCalendarResponse(response: response)
        }
        setUI()
    }
    
    /********************************************************************************************************/
    // MARK:- Process Responses
    func processLoginResponse(response : JSON){
    
        let firstCheckArray     = response["firstCheck"]
        
        if(firstCheckArray.count > 0){
            
            let isFirstLogin        = String(describing: firstCheckArray.arrayValue[0]["isFirstLogin"])
            let account_status      = String(describing: firstCheckArray.arrayValue[0]["account_status"])
            mobileNo                = String(describing: firstCheckArray.arrayValue[0]["mobile"])
            let email               = String(describing: firstCheckArray.arrayValue[0]["email"])
        
            print("Processed Login Response Value \(isFirstLogin) & \(account_status) & \(mobileNo) & \(email)")
        
            switch account_status {
            case "active":
                switch isFirstLogin {
                case "yes":
                    ForgotPassWordAPICall()
                    break
                case "no":
                    loginBT.setTitle("Sign In", for: .normal)
                    passwordTF.isHidden             = false
                    forgotPasswordLabel.isHidden    = false
                    loginBT.center.y                = 520.0
                    break
                default:
                    break
                }
            default:
                uiFun.showAlert(title: "", message: "Account is Deactivated. Contact CINTAA !!", logMessage: "Account is Deactivated", fromController: self)
                break
            }
            
        }else{
            uiFun.showAlert(title: "", message: "Incorrect CINTAA Member ID", logMessage: "Incorrect CINTAA Member ID", fromController: self)
        }
    }
    
    func processPasswordCheckResponse(response : JSON){
    
        let passwordStatus  = String(describing : response["success"])

        if(passwordStatus == "1"){
            
            uiFun.writeData(value: passwordTF.text!, key: "password")
            getUserProfileAPICall()
            //AllCalendarEventAPICall()
            
            Answers.logLogin(withMethod: "Login", success: true, customAttributes: ["member_id" : memberId])
            
            // MARK: Clevertap Events
            let login = ["MemberId": memberId,
                         "Reponse" : String(describing : response)]
            CleverTap.sharedInstance().recordEvent("Login Success", withProps: login)
            
            uiFun.writeData(value: emailTF.text!, key: "member_id")
            uiFun.navigateToScreem(identifierName: "DashboardViewController", fromController: self)
            
        }else{
            uiFun.hideIndicatorLoader()
            uiFun.showAlert(title: "", message: "Oops..Look like you entered wrong password.", logMessage: "Wrong Password", fromController: self)
        }
    }
   
    func processUserProfileResponse(response : JSON){
    
        print("Gotcha User Profile")
        
        let parseUserProfile        = UserProfileParser()
        parseUserProfile.parseUserProfileJSON(response: response)
        
        // TODO: Database Entries
        
        //processEvenInformationToDatabase(response: response)
        //AllCalendarEventAPICall()
    }
    
    func processForgotPasswordReq(response : JSON){
        let status  = String(describing : response["success"])
        
        if(status == "1"){
            uiFun.navigateToScreem(identifierName: "ForgotPasswordViewController", fromController: self)
        }else{
            uiFun.showAlert(title: "Alert", message: "Failed To Send SMS", logMessage: "OTP Serive Failure API Response", fromController: self)
        }
    }
    
    
    func processEvenInformationToDatabase(response : JSON){
        
        let historyJSON                     = response["HistoryRecords"]
        var historyEvenList                 = [CalendarEvent]()
        var actualArray                     = [String : Any]()
        
        for i in 0..<historyJSON.count{
            
            let historyModal                =   CalendarEvent()
            
            historyModal.entry_id           =   String(describing: historyJSON.arrayValue[i]["entry_id"])
            historyModal.shift_time         =   String(describing: historyJSON.arrayValue[i]["shift_time"])
            historyModal.shoot_date         =   String(describing: historyJSON.arrayValue[i]["shoot_date"])
            historyModal.schedule_status    =   String(describing: historyJSON.arrayValue[i]["schedule_status"])
            historyModal.film_tv_name       =   String(describing: historyJSON.arrayValue[i]["film_tv_name"])
            historyModal.shoot_location     =   String(describing: historyJSON.arrayValue[i]["shoot_location"])
            historyModal.call_time          =   String(describing: historyJSON.arrayValue[i]["call_time"])
            historyModal.lat                =   String(describing: historyJSON.arrayValue[i]["lat"])
            historyModal.lng                =   String(describing: historyJSON.arrayValue[i]["lng"])
        
            historyEvenList.append(historyModal)
            
            actualArray.updateValue(uiFun.hexStringToUIColor(hex: cintaColors.calendarEvent), forKey: uiFun.convertDateFormater(date: String(describing: historyJSON.arrayValue[i]["shoot_date"])))
        }
        
        let isDatabaseCreated   = HistoryRecordDB.shared.createDatabase()
        
        if(isDatabaseCreated){
            
            HistoryRecordDB.shared.insertBookingData(userInfoList: historyEvenList,serviceCall: "1")
            fillSelectionColors = actualArray as! [String : UIColor]
            uiFun.hideIndicatorLoader()
        }
        
    }
    
    func processCalendarResponse(response : JSON){
        
        var calendarEventList    = [CalendarEvent]()
        let calendarData         = response["sht_date"]
        calendarEventList.reserveCapacity(calendarData.count)
        
        // Get Current Date
        let date             = Date()
        let formatter        = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result           = formatter.string(from: date)
        var actualArray      = [String : Any]()
        
        for i in 0 ..< calendarData.count {
            
            let calendarEvents  = CalendarEvent()
            
            let calendarJSON                    = calendarData.arrayValue[i]
            
//            // check for current date diary entry
//            if(String(describing: calendarJSON["shoot_date"]) == result){
            
                calendarEvents.entry_id             = String(describing: calendarJSON["entry_id"])
                calendarEvents.film_tv_name         = String(describing: calendarJSON["film_tv_name"])
                calendarEvents.house_name           = String(describing: calendarJSON["house_name"])
                calendarEvents.producer_name        = String(describing: calendarJSON["producer_name"])
                calendarEvents.shoot_date           = String(describing: calendarJSON["shoot_date"])
            
                calendarEvents.schedule_status      = String(describing: calendarJSON["schedule_status"])
                calendarEvents.shift_time           = String(describing: calendarJSON["shift_time"])
                calendarEvents.call_time            = String(describing: calendarJSON["call_time"])
                calendarEvents.shoot_map_location   = String(describing: calendarJSON["shoot_map_location"])
                calendarEvents.lat                  = String(describing: calendarJSON["lat"])
            
                calendarEvents.lng                  = String(describing: calendarJSON["lng"])
                calendarEvents.played_character     = String(describing: calendarJSON["played_character"])
                calendarEvents.rate                 = String(describing: calendarJSON["rate"])
                calendarEvents.due_days             = String(describing: calendarJSON["due_days"])
                calendarEvents.remark               = String(describing: calendarJSON["remark"])
            
                calendarEvents.rate_type            = String(describing: calendarJSON["rate_type"])
                calendarEvents.callin_time          = String(describing: calendarJSON["callin_time"])
                calendarEvents.packup_time          = String(describing: calendarJSON["packup_time"])
                calendarEvents.shoot_location       = String(describing: calendarJSON["shoot_location"])
            
                calendarEventList.append(calendarEvents)
                
                //HistoryRecordDB.shared.updateBookingInDiaryEntry(withID: String(describing: calendarJSON["entry_id"]), calendarEvents: calendarEventList, serviceCall: "1")
                
                
                actualArray.updateValue(uiFun.hexStringToUIColor(hex: cintaColors.calendarEvent), forKey: uiFun.convertDateFormater(date: String(describing: calendarJSON["shoot_date"])))
            //}
        }
        
        // TODO: Database Entries
        let isDatabaseCreated   = HistoryRecordDB.shared.createDatabase()
        
        HistoryRecordDB.shared.insertBookingData(userInfoList: calendarEventList,serviceCall: "1")
        uiFun.hideIndicatorLoader()
        
        
    }
    
    
    
    /********************************************************************************************************************************/
    // MARK:- COMMON FUNCTIONS
    
    func randomOTP(count : Int){
        
        var length              = count
        let letters : NSString  = "A1B2C3D4E5F6G7H8I9J0K1L2M3N4O5P6Q7R8S9T0U1V2W3X4Y5Z6"
        _        = ""
        
        while length != 0 {
            
            let charIndex       = Int(Double(arc4random()) / Double(UInt32.max)) * letters.length
            print(letters.character(at: charIndex))
            length -= 1
            
        }
    }
    
    func randomString(length: Int) -> String {
        
        let letters : NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        print("Random OTP is \(randomString)")
        return randomString
        
    }
    
}


