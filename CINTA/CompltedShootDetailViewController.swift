//
//  CompltedShootDetailViewController.swift
//  CINTA
//
//  Created by Mayur on 19/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class CompltedShootDetailViewController: UIViewController {

    
    @IBOutlet var scrollView        : UIScrollView!
  
    @IBOutlet var film_tv_name      : UILabel!
    @IBOutlet var house_name        : UILabel!
    @IBOutlet var alies_name        : UILabel!
    @IBOutlet var dateOfShoot       : UILabel!
    @IBOutlet var shoot_location    : UILabel!
  
    @IBOutlet var played_character  : UILabel!
    @IBOutlet var shift_time        : UILabel!
    @IBOutlet var call_time         : UILabel!
    @IBOutlet var rate              : UILabel!
    @IBOutlet var due_days          : UILabel!
    
    @IBOutlet var remark            : UILabel!
    @IBOutlet var fees              : UILabel!
    @IBOutlet var call_in_time      : UILabel!
    @IBOutlet var pack_up_time      : UILabel!
    @IBOutlet var authority_name    : UILabel!
    
    @IBOutlet var authority_number  : UILabel!
    @IBOutlet var authority_email   : UILabel!
    @IBOutlet var date_of_sign      : UILabel!
    
    @IBOutlet var signature_image   : UIImageView!
    
    var completedShootDetailList    = [CompletedShootDetailModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: 1600)
    }
    
    // MARK:- UI Function
    func setUI(){
    
        film_tv_name.text       = completedShootDetailList[0].film_tv_name
        house_name.text         = completedShootDetailList[0].house_name
        alies_name.text         = completedShootDetailList[0].producer_name
        dateOfShoot.text        = completedShootDetailList[0].shoot_date
        shoot_location.text     = completedShootDetailList[0].shoot_location
        
        played_character.text   = completedShootDetailList[0].played_character
        shift_time.text         = completedShootDetailList[0].shift_time
        call_time.text          = completedShootDetailList[0].call_time
        rate.text               = completedShootDetailList[0].rate
        due_days.text           = completedShootDetailList[0].due_days
        
        remark.text             = completedShootDetailList[0].remark
        fees.text               = completedShootDetailList[0].fees
        call_in_time.text       = completedShootDetailList[0].callin_time
        pack_up_time.text       = completedShootDetailList[0].packup_time
        authority_name.text     = completedShootDetailList[0].authority_nm
        
        authority_number.text   = completedShootDetailList[0].authority_number
        authority_email.text    = completedShootDetailList[0].house_email
        date_of_sign.text       = completedShootDetailList[0].authority_sign_date
       
        
        uiFun.loadImageFromURL(url: completedShootDetailList[0].authority_sign) { (result, error) in
           
            if result != nil{
                if let mediaLinkData                = UIImage(data: result!){
                    self.signature_image.image      = mediaLinkData

                }
            }else{
                
                print("Image Set Failed....Try Harder Mayur ")
                return
            }
        }
    }
    
    
    // MARK:- API Call
    
    func makeCompletedShootDetailAPICall(){
        
        uiFun.showIndicatorLoader()
    }
    
    
    // MARK:- API Responses
    func completedShootDetailAPIResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        
        let detailShootModel                    = CompletedShootDetailModel()
        
        let completedDetail                     = response["CompletedDetails"]
        let completedDetailObj                  = completedDetail.arrayValue[0]
        
        detailShootModel.film_tv_name           = String(describing: completedDetailObj["film_tv_name"])
        detailShootModel.house_name             = String(describing: completedDetailObj["house_name"])
        detailShootModel.alies_name             = String(describing: completedDetailObj["alies_name"])
        detailShootModel.producer_name          = String(describing: completedDetailObj["producer_name"])
        detailShootModel.shoot_date             = String(describing: completedDetailObj["shoot_date"])
        
        detailShootModel.shoot_location         = String(describing: completedDetailObj["shoot_location"])
        detailShootModel.played_character       = String(describing: completedDetailObj["played_character"])
        detailShootModel.shift_time             = String(describing: completedDetailObj["shift_time"])
        detailShootModel.rate_type              = String(describing: completedDetailObj["rate_type"])
        detailShootModel.rate                   = String(describing: completedDetailObj["rate"])
        
        detailShootModel.due_days               = String(describing: completedDetailObj["due_days"])
        detailShootModel.remark                 = String(describing: completedDetailObj["remark"])
        detailShootModel.callin_time            = String(describing: completedDetailObj["callin_time"])
        detailShootModel.packup_time            = String(describing: completedDetailObj["packup_time"])
        detailShootModel.authority_sign         = String(describing: completedDetailObj["authority_sign"])
        
        detailShootModel.authority_sign_date    = String(describing: completedDetailObj["authority_sign_date"])
        detailShootModel.authority_nm           = String(describing: completedDetailObj["authority_nm"])
        detailShootModel.authority_number       = String(describing: completedDetailObj["authority_number"])
        detailShootModel.call_time              = String(describing: completedDetailObj["call_time"])
        detailShootModel.fees                   = String(describing: completedDetailObj["fees"])
        
        detailShootModel.house_email            = String(describing: completedDetailObj["house_email"])
        
        completedShootDetailList.append(detailShootModel)
        
        setUI()
    }
    
    @IBAction func okAction(_ sender: Any) {
        
        let completedShootVC                = CompleteShootViewController()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DismissContainer"), object: nil)
    }
    
    
}
