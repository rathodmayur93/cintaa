//
//  SignatureViewController.swift
//  CINTA
//
//  Created by Mayur on 15/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import AASignatureView

var signImage   : UIImage?

class SignatureViewController: UIViewController {

    @IBOutlet var signatureView : AASignatureView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Button Actions
    
    @IBAction func clearPadAction(_ sender: Any) {
        signatureView.clear()
    }
    
    @IBAction func saveAction(_ sender: Any) {
        signImage   = signatureView.signature
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callForAlert3"), object: nil)
    }
    
}
