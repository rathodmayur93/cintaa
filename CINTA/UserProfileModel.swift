//
//  UserProfileModel.swift
//  CINTA
//
//  Created by Mayur on 07/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class UserProfileModel {

    var nameAndimage    = [Nameandimage]()
    var historyRecords  = [HistoryRecords]()
}

class Nameandimage{

    var name            = ""
    var gender          = ""
    var address         = ""
    var city            = ""
    var state           = ""
    var zip             = ""
    var country         = ""
    var email           = ""
    var password        = ""
    var mobile          = ""
    var website         = ""
    var profile_imgurl  = ""
    var alt_mobile      = ""
}

class HistoryRecords{

    var entry_id        = ""
    var shoot_date      = ""
    var film_tv_name    = ""
    var shift_time      = ""
    var call_time       = ""
    var shoot_location  = ""
    var lat             = ""
    var lng             = ""
    var schedule_status = ""
    var producerName    = ""
    var productioHouse  = ""
}
