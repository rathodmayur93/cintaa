//
//  MovieListPopUpViewController.swift
//  CINTA
//
//  Created by Mayur on 26/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var diaryMovieIndex = 0
class MovieListPopUpViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return multipleMovieList.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell                = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text    = "\(multipleMovieList[indexPath.row].film_tv_name) - \(multipleMovieList[indexPath.row].call_time)"
        return cell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        diaryMovieIndex = indexPath.row
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callForAlert4"), object: nil)
    }
    
    
    // MARK:- button action
    
}
