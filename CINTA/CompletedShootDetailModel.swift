//
//  CompletedShootDetailModel.swift
//  CINTA
//
//  Created by Mayur on 19/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CompletedShootDetailModel {
    
    var film_tv_name        = ""
    var house_name          = ""
    var alies_name          = ""
    var producer_name       = ""
    var shoot_date          = ""
    var shoot_location      = ""
    var played_character    = ""
    var shift_time          = ""
    var rate_type           = ""
    var rate                = ""
    var due_days            = ""
    var remark              = ""
    var callin_time         = ""
    var packup_time         = ""
    var authority_sign      = ""
    var authority_sign_date = ""
    var authority_nm        = ""
    var authority_number    = ""
    var call_time           = ""
    var fees                = ""
    var house_email         = ""

}
