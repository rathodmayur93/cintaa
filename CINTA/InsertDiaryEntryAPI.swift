//
//  InsertDiaryEntryAPI.swift
//  CINTA
//
//  Created by Mayur on 15/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class InsertDiaryEntryAPI: NSObject {

    
    func makeAPICall(fromViewController : NewDiaryEntryViewController, entry_id : String, member_id : String, shootDate : String, character : String, location : String, serialName : String, shiftTime : String, callTime : String, rateType : String, rate : String, dueDays : String, remark : String, lati : String, lng : String, mapLocation : String, productionHouseName : String, producerName : String){
        
        let url                         = "\(constant.baseURL)DiaryEntryInsert.php"
        let params : [String:AnyObject] = ["entry_id"        : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "member_id"       : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shoot_date"      : shootDate.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "played_character": character.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shoot_location"  : location.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "film_tv_name"    : serialName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shift_time"      : shiftTime.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "call_time"       : callTime.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "rate_type"       : rateType.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "rate"            : rate.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "due_days"        : dueDays.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "remark"          : producerName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "schedule_status" : "pending" as AnyObject,
                                           "lat"             : lati.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "lng"             : lng.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shoot_map_location": mapLocation.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "new_house_name"  : productionHouseName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "new_producer_name" : producerName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        CleverTap.sharedInstance().recordEvent("Diary Entry API Call URL \(url)")
        CleverTap.sharedInstance().recordEvent("Diary Entry API Call Params", withProps: params)
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.diaryEntryResponse(response: jsonResponse)
                return
                
            }else {
                print("Diary Entry Failed")
                return
            }
            
        })
        
    }
    
    
    // For Offline
    func makeAPICall2(fromViewController : BackgroundService, entry_id : String, member_id : String, shootDate : String, character : String, location : String, serialName : String, shiftTime : String, callTime : String, rateType : String, rate : String, dueDays : String, remark : String, lati : String, lng : String, mapLocation : String, productionHouseName : String, producerName : String){
        
        let url                         = "\(constant.baseURL)DiaryEntryInsert.php"
        let params : [String:AnyObject] = ["entry_id"        : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "member_id"       : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shoot_date"      : shootDate.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "played_character": character.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shoot_location"  : location.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "film_tv_name"    : serialName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shift_time"      : shiftTime.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "call_time"       : callTime.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "rate_type"       : rateType.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "rate"            : rate.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "due_days"        : dueDays.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "remark"          : producerName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "schedule_status" : "pending" as AnyObject,
                                           "lat"             : lati.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "lng"             : lng.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "shoot_map_location": mapLocation.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "new_house_name"  : productionHouseName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "new_producer_name" : producerName.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        CleverTap.sharedInstance().recordEvent("Background Service")
        CleverTap.sharedInstance().recordEvent("Diary Entry API Call URL \(url)")
        CleverTap.sharedInstance().recordEvent("Diary Entry API Call Params", withProps: params)
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.diaryEntryResponse(response: jsonResponse)
                return
                
            }else {
                print("Diary Entry Failed")
                return
            }
            
        })
        
    }
}
