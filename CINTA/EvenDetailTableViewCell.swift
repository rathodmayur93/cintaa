//
//  EvenDetailTableViewCell.swift
//  CINTA
//
//  Created by Mayur on 25/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class EvenDetailTableViewCell: UITableViewCell {

    
    @IBOutlet var tvSerialName: UILabel!
    @IBOutlet var callTime: UILabel!
    @IBOutlet var productionHouse: UILabel!
    @IBOutlet var producerName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
