//
//  ContainerViewController.swift
//  CINTA
//
//  Created by Mayur on 10/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var editScheduleEntry   = false
var eventIndex          = 0

class ContainerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView : UITableView!
    @IBOutlet var heading   : UILabel!
    
    @IBOutlet var editEntry : UIButton!
    @IBOutlet var newEntry  : UIButton!
    
    static let shared   : ContainerViewController     = ContainerViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Container Selected Date is \(selectedDate)")
        heading.text    = "Schedule For : \(selectedDate)"
        NotificationCenter.default.addObserver(self, selector: #selector(showHideButton), name: NSNotification.Name(rawValue: "buttonStatus"), object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return multiEventList.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
    
        let cell                = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text    = "\(multiEventList[indexPath.row].film_tv_name) - \(multiEventList[indexPath.row].call_time)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventIndex  = indexPath.row
        if(multiEventList[indexPath.row].schedule_status != "completed"){
            uiFun.navigateToScreem(identifierName: "CreateScheduleEntryViewController", fromController: self)
            editScheduleEntry = true
        }else{
            uiFun.showAlert(title: "Notification", message: "Entry is already completed", logMessage: "Entry is completed", fromController: self)
        }
    }

    // MARK:- Button Actions
    
    @IBAction func editEntryAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "CreateScheduleEntryViewController", fromController: self)
        editScheduleEntry = true
    }
    
    @IBAction func newEntryAction(_ sender: Any) {
        uiFun.navigateToScreem(identifierName: "CreateScheduleEntryViewController", fromController: self)
        editScheduleEntry = false
    }
    
    @IBAction func crossActionButton(_ sender: Any) {
        print("Container Close Button Tapped")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callForAlert"), object: nil)
    }
    

    // MARK:- Common Functinos
    func showHideButton(){
        
    }
}
