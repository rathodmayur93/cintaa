//
//  BackgroundService.swift
//  CINTA
//
//  Created by Mayur on 09/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class BackgroundService:UIViewController {

    var remainingList   = [CalendarEvent]()
    
    
    
    override func viewDidLoad() {
        
    }
    
    
    // MARK:- Fetch Remaining Trips For API Call
    func fetchRemianingEventsForAPICall(){
    
        let queueName = DispatchQueue(label: "data.cintaa.processing")
        queueName.async {
            
        let historyRecords  = HistoryRecordDB.shared.fetchBookingItems()
        
        
        for i in 0..<historyRecords.count{
    
            if(historyRecords[i].serviceCall != "1"){
            
                let userProfileModel                    = CalendarEvent()
                
                userProfileModel.entry_id               = historyRecords[i].entry_id
                userProfileModel.played_character       = historyRecords[i].played_character
                userProfileModel.rate                   = historyRecords[i].rate
                userProfileModel.shoot_map_location     = historyRecords[i].shoot_map_location
                userProfileModel.due_days               = historyRecords[i].due_days
                userProfileModel.producer_name          = historyRecords[i].producer_name
                userProfileModel.packup_time            = historyRecords[i].packup_time
                userProfileModel.call_time              = historyRecords[i].call_time
                userProfileModel.callin_time            = historyRecords[i].callin_time
                userProfileModel.shoot_location         = historyRecords[i].shoot_location
                userProfileModel.lat                    = historyRecords[i].lat
                userProfileModel.shoot_date             = historyRecords[i].shoot_date
                userProfileModel.remark                 = historyRecords[i].remark
                userProfileModel.rate_type              = historyRecords[i].rate_type
                userProfileModel.film_tv_name           = historyRecords[i].film_tv_name
                userProfileModel.house_name             = historyRecords[i].house_name
                userProfileModel.shift_time             = historyRecords[i].shift_time
                userProfileModel.lng                    = historyRecords[i].lng
                userProfileModel.schedule_status        = historyRecords[i].schedule_status
                userProfileModel.serviceCall            = historyRecords[i].serviceCall
                
                self.remainingList.append(userProfileModel)
                
            }
            
                self.APICallForRemainingEvents()
            }
        }
    }
    
    
    func APICallForRemainingEvents(){
        
        /*
        if(remainingList.count == 0){
            
            MemberProfileInfo.shared.dropTable()
            HistoryRecordDB.shared.dropTable()
            AuthorityDatabse.shared.dropTable()
            print("If list is zero")
            DispatchQueue.main.async {
                print("Navigate to ViewController")
                uiFun.writeData(value: "", key: "member_id")
                uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
                print("File Logour")
            }
            return
        }
        */
        
        for i in 0..<remainingList.count{
        
            let seriveCall  = remainingList[i].serviceCall
            
            switch seriveCall {
            case "0.1":
                makeScheduleEntryForOffline(index: i)
                break
            case "0.2":
                diaryEntryAPICall(index: i)
                break
            default:
                break
            }
            
            if(i == remainingList.count){
                
                MemberProfileInfo.shared.dropTable()
                HistoryRecordDB.shared.dropTable()
                AuthorityDatabse.shared.dropTable()
                print("If list is not zero")
                DispatchQueue.main.async {
                    print("Navigate to ViewController")

                    uiFun.writeData(value: "", key: "member_id")
                    uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
                    print("File Logour")
                }
            }
        }
    }
    
    // MARK:- API Call
    func makeScheduleEntryForOffline(index : Int){
        
        let scheduleAPICall = CreateScheduleAPI()
        scheduleAPICall.makeAPICall2(fromViewController: self, entry_id: remainingList[index].entry_id, member_id: uiFun.readData(key: "member_id"), ScheduleDate: remainingList[index].shoot_date, filmName: remainingList[index].film_tv_name, productionHouseName: remainingList[index].house_name, producerName: remainingList[index].producer_name)
        
        HistoryRecordDB.shared.updateBookingEntryForOffline(withID: remainingList[index].entry_id, serviceCall: "1")
    }
    
    func diaryEntryAPICall(index : Int){
        
        uiFun.showIndicatorLoader()
        let diaryApiCall    = InsertDiaryEntryAPI()
        diaryApiCall.makeAPICall2(fromViewController: self, entry_id: remainingList[index].entry_id, member_id: uiFun.readData(key: "member_id"), shootDate: remainingList[index].shoot_date, character: remainingList[index].played_character, location: remainingList[index].shoot_location, serialName: remainingList[index].film_tv_name, shiftTime: remainingList[index].shift_time, callTime: remainingList[index].call_time, rateType: remainingList[index].rate_type, rate: remainingList[index].rate, dueDays: remainingList[index].due_days, remark: remainingList[index].remark, lati: remainingList[index].lat, lng: remainingList[index].lng, mapLocation: remainingList[index].shoot_map_location, productionHouseName: remainingList[index].house_name, producerName: remainingList[index].producer_name)
        
        HistoryRecordDB.shared.updateBookingEntryForOffline(withID: remainingList[index].entry_id, serviceCall: "1")
    }
    
    // MARK:- API Responses
    
    func scheduleEntryResponse(response : JSON){print("Offline Schedule Entry")}
    
    func diaryEntryResponse(response : JSON){print("Offline Diary Entry Done")}
}
