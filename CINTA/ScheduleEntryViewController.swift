//
//  ScheduleEntryViewController.swift
//  CINTA
//
//  Created by Mayur on 15/08/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import iShowcase

var fillSelectionColors   = [String : Any]()
var scheduleDate          = ""

var isMultiEvent          = false
var indexOfMultiEvent     = 0
var tabBarIndex           = 0

let cintaColors           = Colors()
var selectedDate         = ""


class ScheduleEntryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance, UIGestureRecognizerDelegate, iShowcaseDelegate {

    
    @IBOutlet var container                 : UIView!
    @IBOutlet var containerDetail           : UIView!
    @IBOutlet var calendar                  : FSCalendar!
    @IBOutlet var tableView                 : UITableView!
    @IBOutlet var todayDateLabel            : UILabel!
    @IBOutlet var calendarHeightConstraint  : NSLayoutConstraint!
    var showcase                            : iShowcase!
    var historyRecordsFromDB                = [CalendarEvent]()
    var isShowTour2                         = true
    
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    fileprivate lazy var dateFormatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    let fillDefaultColors     = ["2017/10/08": UIColor.purple, "2017/10/06": UIColor.green, "2017/10/18": UIColor.cyan, "2017/10/22": UIColor.yellow, "2017/11/08": UIColor.purple, "2017/11/06": UIColor.green, "2017/11/18": UIColor.cyan, "2017/11/22": UIColor.yellow, "2017/12/08": UIColor.purple, "2017/12/06": UIColor.green, "2017/12/18": UIColor.cyan, "2017/12/22": UIColor.magenta]
    
    let borderDefaultColors   = ["2017/10/08": UIColor.brown, "2017/10/17": UIColor.magenta, "2017/10/21": UIColor.cyan, "2017/10/25": UIColor.black, "2017/11/08": UIColor.brown, "2017/11/17": UIColor.magenta, "2017/11/21": UIColor.cyan, "2017/11/25": UIColor.black, "2017/12/08": UIColor.brown, "2017/12/17": UIColor.magenta, "2017/12/21": UIColor.purple, "2017/12/25": UIColor.black]
    
    let borderSelectionColors = ["2017/10/08": UIColor.red, "2017/10/17": UIColor.purple, "2017/10/21": UIColor.cyan, "2017/10/25": UIColor.magenta, "2017/11/08": UIColor.red, "2017/11/17": UIColor.purple, "2017/11/21": UIColor.cyan, "2017/11/25": UIColor.purple, "2017/12/08": UIColor.red, "2017/12/17": UIColor.purple, "2017/12/21": UIColor.cyan, "2017/12/25": UIColor.magenta]
    
    var datesWithEvent          = [String]()
    var datesWithMultipleEvents = [String]() //["2017-10-08", "2017-10-16", "2017-10-20", "2017-10-28"]
    
    var actualArray             = [String : Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        uiFun.hideIndicatorLoader()
        showcase = iShowcase()
        fetchValuesFromDatabase()  // Fetching values from database
        
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }

        container.isHidden          = true // initially hide container
        containerDetail.isHidden    = true // initially hide container
        calendar.dataSource         = self
        calendar.delegate           = self
        showcase.delegate           = self
        
        self.calendar.select(Date())
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideContainerView), name: NSNotification.Name(rawValue: "callForAlert"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideContainerDetailView), name: NSNotification.Name(rawValue: "callForAlert2"), object: nil)
        
        editScheduleEntry = false   // for avoiding crash when user makes new schedule entry
        
        // For UITest
        self.calendar.accessibilityIdentifier       = "calendar"
        calendar.appearance.subtitleDefaultColor    = UIColor.brown
        
        // set today date
        setCurrentDate()
        
        print("Member Id \(uiFun.readData(key: "memeber_id"))")
        CleverTap.sharedInstance().recordEvent("Showing Calendar")
        
    }
    
    override func willAnimateRotation(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            if let showcase = self.showcase {
                showcase.setNeedsLayout()
            }
        })
    }
    
    deinit {
        print("\(#function)")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.selectedIndex = tabBarIndex
        
        // Show Delete Dialog if any event is deleted
        if(showDeleteDialog){
            showDeleteDialog = false
            uiFun.showAlert(title: "Notification", message: "Schedule Entry Deleted Successfully", logMessage: "Deleting Schedule Entry", fromController: self)
        }

    }

    override func viewWillAppear(_ animated: Bool) {
        if(tabBarIndex == 3){
            self.tabBarController?.selectedIndex = tabBarIndex
        }
        
        if (tabBarIndex == 1){
            self.tabBarController?.selectedIndex = tabBarIndex
        }
        
        if(tabBarIndex == 2){
            self.tabBarController?.selectedIndex = tabBarIndex
        }
        
        fetchValuesFromDatabase()  // Fetching values from database
        
        for i in 0..<historyRecordsFromDB.count{
            datesWithEvent.append(uiFun.converDateFormaterEvent(date: historyRecordsFromDB[i].shoot_date))
        }

    }
    // MARK:- UIGestureRecognizerDelegate
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.calendarHeightConstraint.constant = bounds.height
        self.view.layoutIfNeeded()
        
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.dateFormatter.string(from: date))")
       
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        selectedDate      = self.dateFormatter.string(from: date)
        print("selected dates is \(selectedDates)")
       
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
        
        // Find Out Current Date Event
        findMultipleOccurence(date: self.dateFormatter3.string(from: date)) // find out is there any multiple event on same day
        scheduleDate    = "\(self.dateFormatter3.string(from: date))"
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    func iShowcaseDismissed(_ showcase: iShowcase) {
        
        if(isShowTour2){
            showTour2()
        }
    }
    
    // MARK:- UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return [2,historyRecordsFromDB.count][section]
        
    }
    
    // show schedule entries tableview
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let identifier = ["cell_month", "cell_week"][indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier)!
            return cell
        } else {
            let cell                        = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CalendarTableCellTableViewCell
            cell.colorView.backgroundColor  = UIColor.brown //uiFun.hexStringToUIColor(hex: cintaColors.calendarEvent)
            cell.dateLabel.text             = historyRecordsFromDB[indexPath.row].shoot_date
            cell.productionHouseLabel.text  = historyRecordsFromDB[indexPath.row].film_tv_name
            cell.timeLabel.text             = historyRecordsFromDB[indexPath.row].call_time
            cell.timeLabel.textColor        = UIColor.white
            cell.timeView.backgroundColor   = UIColor.red
            cell.selectionStyle             = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            print("Schedule Entry Deleted")
            historyRecordsFromDB.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            //deleteSchedule(entryId: historyRecordsFromDB[indexPath.row].entry_id)
            
            // Deleting schedule entry from the database
            let deleteEntryDB = HistoryRecordDB.shared.deleteBookingItem(entryId: historyRecordsFromDB[indexPath.row].entry_id)
            
            if(deleteEntryDB){
                uiFun.showAlert(title: "Notification", message: "Schedule Entry Deleted Successfully", logMessage: "Deleting Schedule Entry", fromController: self)
            }else{
                uiFun.showAlert(title: "Notification", message: "Unable to Delete Schedule Entry.. Please try again", logMessage: "Deleting Schedule Entry Failed", fromController: self)
            }
        
        }
    }
    
    
    // MARK:- UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 {
            let scope: FSCalendarScope = (indexPath.row == 0) ? .month : .week
            self.calendar.setScope(scope, animated: true)
        }
        
        findMultipleOccurence(date: historyRecordsFromDB[indexPath.row].shoot_date) // find out is there any multiple event on same day
        scheduleDate    = "\(historyRecordsFromDB[indexPath.row].shoot_date)"
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    
    
    // MARK:- Calendar Appearances
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let dateString = self.dateFormatter2.string(from: date)
        if self.datesWithEvent.contains(dateString) {
            return 1
        }
        if self.datesWithMultipleEvents.contains(dateString) {
            return 3
        }
        return 0
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventColorFor date: Date) -> UIColor? {
        let dateString = self.dateFormatter2.string(from: date)
        if self.datesWithEvent.contains(dateString) {
            return UIColor.purple
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        let key = self.dateFormatter2.string(from: date)
        if self.datesWithMultipleEvents.contains(key) {
            return [UIColor.magenta, appearance.eventDefaultColor, UIColor.black]
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillSelectionColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter1.string(from: date)
        if let color = fillSelectionColors[key] {
            return color as? UIColor
            
        }
        return appearance.selectionColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter1.string(from: date)
        if let color = fillSelectionColors[key] {
            return color as? UIColor
        }
        return nil
    }
    
    /*
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter1.string(from: date)
        if let color = self.borderDefaultColors[key] {
            return color
        }
        return appearance.borderDefaultColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter1.string(from: date)
        if let color = self.borderSelectionColors[key] {
            return color
        }
        return appearance.borderSelectionColor
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderRadiusFor date: Date) -> CGFloat {
        if [8, 17, 21, 25].contains((self.gregorian.component(.day, from: date))) {
            return 0.0
        }
        return 1.0
    }
    */
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:-  Button Actions
    
    @IBAction func tourAction(_ sender: Any) {
        //showTour1()
        openActionSheet()
    }
    
    @IBAction func refreshScanAction(_ sender: Any) {
        
    }
    
    
    
    
    // MARK:- API Call
    
    func deleteSchedule(entryId : String){
//        let deleteEvent     = DeleteScheduleEntryAPI()
//        deleteEvent.makeAPICall(fromViewController: self, entry_id: entryId, member_id: uiFun.readData(key: "member_id"))
    }
    
    
    // MARK:- API Responses
    
    func deleteScheduleEntryAPIResponse(response : JSON){
    
        
    }
    
    // MARK:- Common Function (MultiEvent)
    func findMultipleOccurence(date : String){
        
        multiEventList.removeAll()
        
        var prop = [String: String]()
        
        let date1            = Date()
        let formatter        = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result           = formatter.string(from: date1)
        
        let currentDate = uiFun.convertStringToDate(dateString: result)
        let eventDate   = uiFun.convertStringToDate(dateString: date)
        
        for i in 0..<historyRecordsFromDB.count{
            
            if(historyRecordsFromDB[i].shoot_date == date){
                let historyParser   = HistoryRecordParser()
                historyParser.parseHistoryRecords(index: i)
                print("Multieven Date \(multiEventList[0].shoot_date)")
                prop[historyRecordsFromDB[i].shoot_date] = historyRecordsFromDB[i].film_tv_name
            }
        }
        CleverTap.sharedInstance().recordEvent("Selected Date", withProps: prop)
        
        if (eventDate < currentDate){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "buttonStatus"), object: nil)
            if(multiEventList.count != 0){
                showDetailMultiEventPopUp()
            }
        }else{
            handlingMultiEvent()    // if have mulitple event handle accordingly
        }
        
        
    }
    
    func handlingMultiEvent(){
    
        switch multiEventList.count {
        case 0:
            uiFun.navigateToScreem(identifierName: "CreateScheduleEntryViewController", fromController: self)
            break
        case 1:
            isMultiEvent    = true
            showMultiEventPopUp()
            break
        case 2:
            isMultiEvent    = true
            showMultiEventPopUp()
        default:
            isMultiEvent    = true
            showMultiEventPopUp()
            break
        }
    }
    
    
    func showMultiEventPopUp(){
        
        let childView        = self.childViewControllers.first as! ContainerViewController
        childView.tableView.reloadData()
        container.isHidden   = false
    }
    
    func handlingMultiEventDetail(){
        
        switch multiEventList.count {
        case 0:
            uiFun.navigateToScreem(identifierName: "CreateScheduleEntryViewController", fromController: self)
            break
        case 1:
            isMultiEvent    = true
            showMultiEventPopUp()
            break
        case 2:
            isMultiEvent    = true
            showMultiEventPopUp()
        default:
            isMultiEvent    = true
            showMultiEventPopUp()
            break
        }
    }
    
    func showDetailMultiEventPopUp(){
        
        let childView        = self.childViewControllers.last as! ContainerEvenDetailViewController
        childView.tableView.reloadData()
        containerDetail.isHidden   = false
    }
    
    func hideContainerView(){
        print("Hide Container View")
        container.isHidden   = true
    }
    
    func hideContainerDetailView(){
        print("Hide Container Detail View")
        containerDetail.isHidden    = true
        
    }
    
    func fetchValuesFromDatabase(){
        
        
        historyRecordsFromDB        = HistoryRecordDB.shared.fetchBookingItems()
        
        var actualArray             = [String : Any]()
        for i in 0..<historyRecordsFromDB.count{
            actualArray.updateValue(uiFun.hexStringToUIColor(hex: cintaColors.calendarEvent), forKey: uiFun.convertDateFormater(date: historyRecordsFromDB[i].shoot_date))
        }
        
        fillSelectionColors = actualArray as! [String : UIColor]
        
        calendar.reloadData()
        tableView.reloadData()
    }
    
    func setCurrentDate(){
    
        let date             = Date()
        let formatter        = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        let result           = formatter.string(from: date)
        todayDateLabel.text  = "Today, \(result)"
        
    }
    
    // MARK:- Action Sheet
    func openActionSheet(){
        
        let dotMenu      = UIAlertController(title: nil, message: "Select Any Option", preferredStyle: .actionSheet)
        
        let instructionAction       = UIAlertAction(title: "Instructions", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.instructionTapped()
        })
        
        let helpAction       = UIAlertAction(title: "Help Center", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.helpCenterTapped()
        })
        
        let privacyAction       = UIAlertAction(title: "Privacy Policy", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.privacyViewTapped()
        })
        
        let rateAction       = UIAlertAction(title: "Rate Us", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.rateApp()
        })
        
        let shareAction       = UIAlertAction(title: "Share", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let objectsToShare = [constant.shareContent]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        })
        
        let profileAction       = UIAlertAction(title: "My Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.profileTapped()
        })
        
        let mouAction       = UIAlertAction(title: "MOU", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mouViewTapped()
        })
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            MemberProfileInfo.shared.dropTable()
            HistoryRecordDB.shared.dropTable()
            AuthorityDatabse.shared.dropTable()
            
            uiFun.writeData(value: "", key: "member_id")
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            print("File Logour")
        })
        
        let exitAction       = UIAlertAction(title: "Exit", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            exit(0)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        dotMenu.addAction(instructionAction)
        dotMenu.addAction(helpAction)
        dotMenu.addAction(privacyAction)
        dotMenu.addAction(rateAction)
        dotMenu.addAction(shareAction)
        dotMenu.addAction(profileAction)
        dotMenu.addAction(mouAction)
        dotMenu.addAction(logoutAction)
        dotMenu.addAction(exitAction)
        dotMenu.addAction(cancelAction)
        
        
        self.present(dotMenu, animated: true, completion: nil)
    }
    
    func helpCenterTapped(){
        webViewHeader   = "Help Center"
        loadUrl         = "http://www.cintaa.net/cintaa-help-center/"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func privacyViewTapped(){
        loadUrl         = "http://www.cintaa.net/privacy-policy/"
        webViewHeader   = "Privacy Policy"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func mouViewTapped(){
        loadUrl         = "\(constant.baseURL)mou.pdf"
        webViewHeader   = "MOU"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func rateUsViewTapped(){
        
        webViewHeader   = "Rate Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func instructionTapped(){
        
        loadUrl         = "\(constant.baseURL)instructions.pdf"
        webViewHeader   = "Instruction"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func contactUs(){
        
        loadUrl         = "http://www.cintaa.net/contact-us/"
        webViewHeader   = "Contact Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func profileTapped(){
        uiFun.navigateToScreem(identifierName: "ProfileViewController", fromController: self)
    }
    
    func rateApp() {
        guard let url = URL(string : "https://itunes.apple.com/us/app/cintaa/id1288952015?mt=8") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    // MARK:- Tour Functions
    func showTour1(){
        
        showcase.setupShowcaseForTableView(tableView)
        showcase.titleLabel.text = "Schedule Entry Calendar"
        showcase.detailsLabel.text = "You can create, view or edit your schedule here by simply clicking on particular date."
        showcase.show()
    }
    
    func showTour2(){
    
        print("showing tour 2")
        isShowTour2                 = false
        showcase.setupShowcaseForTableView(tableView, withIndexOfItem: 1, andSectionOfItem: 1)
        showcase.titleLabel.text    = "Scheduled Date"
        showcase.detailsLabel.text  = "You can navigate to particular date schedule, by clicking on the date. You can edit your schedule if and only if in-time is not entered."
        showcase.show()
        
    }
    
    
    
    
}
