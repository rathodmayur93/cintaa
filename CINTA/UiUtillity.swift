//
//  UiUtillity.swift
//  CINTA
//
//  Created by Mayur on 03/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreData
import SwiftyJSON

class UiUtillity {
    
    let spinningActivityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let container: UIView = UIView()
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func showAlert(title: String, message: String, logMessage: String, fromController controller: UIViewController){
        
        OperationQueue.main.addOperation {
            let alertController = UIAlertController(title: "Notification", message:
                message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            controller.present(alertController, animated: true, completion: nil)
            print(logMessage)
        }
    }
    
    func navigateToScreem(identifierName : String, fromController controller: UIViewController){
        
        OperationQueue.main.addOperation {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            //let viewController : UIViewController = (self.navigationController?.viewControllers)! as [UIViewController]
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: identifierName) as UIViewController
            // let navController = UINavigationController(rootViewController: vc)
            //  navController.pushViewController(vc, animated: true)
            // self.present(vc, animated: true, completion: nil)
            controller.present(vc, animated: true, completion: nil)
        }
    }
    
    func writeData(value : String, key: String){
        
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    func readData(key : String) -> String{
        
        let defaults = UserDefaults.standard
        if let value = defaults.string(forKey: key){
            return value
        }else{
            return ""
        }
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func loadImageFromURL(url : String, completionHandler: @escaping (Data?, NSError?) -> Void ){
        
        let imageUrl = URL(string: url)
        if(imageUrl != nil){
            let request = NSMutableURLRequest(url: imageUrl!)
            
            let task1 = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error != nil {
                    print(error!)
                }else{
                    if let urlContent = data{
                        
                        do {
                            DispatchQueue.main.sync(execute: {
                                
                                completionHandler(urlContent, nil)
                            })
                            
                        }
                    }
                }
            }
            
            task1.resume()
        }
    }
    
    @available(iOS 10.0, *)
    func writeToDatabase(entityValue : String, entityName : String, key : String){
        
        let appDelegate =  UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let city = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        city.returnsObjectsAsFaults = false
        do {
            
            let results =  try context.fetch(city)
            if results.count > 1{
                print("Saved Data \(results[0]) \(results[1])")
                for result in results as! [NSManagedObject]{
                    print("Updating Core Data")
                    result.setValue(entityValue, forKey: key)
                }
            }
                
            else{
                let user = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
                user.setValue(entityValue, forKey: key)
                print("Core Data is Empty Inserting Into Core Data Entity Name User")
            }
        }
            
        catch {
            print("There is an error in fetching entity from core data")
        }
    }
    
    @available(iOS 10.0, *)
    func readFromDatabase(entityName : String, key : String) -> String{
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let city = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        city.returnsObjectsAsFaults = false
        var fetchName = ""
        
        do {
            
            let results =  try context.fetch(city)
            if results.count > 1{
                print("Saved Data \(results[0]) \(results[1])")
                for result in results as! [NSManagedObject]{
                    
                    if let name = result.value(forKey: key) as? String{
                        print("Core Data Values \(name) \(key)")
                        fetchName = name
                    }
                }
                return fetchName
            }
                
            else{
                
                print("Entity is not selected yet 2")
                return ""
            }
        }
            
        catch {
            print("There is an error in fetching entity from core data")
            return ""
        }
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    func convertStringToDate(dateString : String) -> Date{
        let dateFormatter           = DateFormatter()
        dateFormatter.dateFormat    = "dd/MM/yyyy"
        let s                       = dateFormatter.date(from: dateString)
        return s!
    }
    
    func convertTimestampToHourMinute(arrivalTime : String) -> String{
        
        let startHour          = arrivalTime.index(arrivalTime.startIndex, offsetBy: 8)
        let endHour            = arrivalTime.index(arrivalTime.endIndex, offsetBy: -4)
        let hourRange          = startHour..<endHour
        
        var hourString         = arrivalTime.substring(with: hourRange)
        
        let startMinute        = arrivalTime.index(arrivalTime.startIndex, offsetBy: 10)
        let endMinute          = arrivalTime.index(arrivalTime.endIndex, offsetBy: -2)
        let minuteRange        = startMinute..<endMinute
        
        let minuteString       = arrivalTime.substring(with: minuteRange)
        
        if(Int(hourString)! > 12){
            hourString = "\((Int(hourString))! - 12)"
            return "\(hourString):\(minuteString) PM"
        }else{
            return "\(hourString):\(minuteString) AM"
        }
        
        
    }
    
    func nullToNil(value : String?) -> String? {
        if (value == "null") {
            return ""
        } else {
            return value
        }
    }
    
    func showIndicatorLoader(){
        
        if(isConnectedToNetwork()){
            let window                      = UIApplication.shared.keyWindow
            container.frame                 = UIScreen.main.bounds
            container.backgroundColor       = UIColor(hue: 0/360, saturation: 0/100, brightness: 0/100, alpha: 0.4)
            
            let loadingView: UIView         = UIView()
            loadingView.frame               = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center              = container.center
            loadingView.backgroundColor     = UIColor.brown //uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
            loadingView.clipsToBounds       = true
            loadingView.layer.cornerRadius  = 40
            
            spinningActivityIndicator.frame                         = CGRect(x: 0, y: 0, width: 40, height: 40)
            spinningActivityIndicator.hidesWhenStopped              = true
            spinningActivityIndicator.activityIndicatorViewStyle    = UIActivityIndicatorViewStyle.whiteLarge
            spinningActivityIndicator.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2)
            loadingView.addSubview(spinningActivityIndicator)
            container.addSubview(loadingView)
            window?.addSubview(container)
            spinningActivityIndicator.startAnimating()
        }
    }
    
    func hideIndicatorLoader(){
        
        spinningActivityIndicator.stopAnimating()
        container.removeFromSuperview()
    }
    
    
    func maskCardNumber(str : String) -> String{
        
        let cardNumber  = str.replacingOccurrences(of: " ", with: "")
        let las4Digits  = cardNumber.substring(from:cardNumber.index(cardNumber.endIndex, offsetBy: -4))
        
        return "XXXXXXXXXXXX\(las4Digits)"
    }
    
    func convertTimestampToMinutes(arrivalTime : String) -> String{
        
        let startHour          = arrivalTime.index(arrivalTime.startIndex, offsetBy: 2)
        let endHour            = arrivalTime.index(arrivalTime.endIndex, offsetBy: -2)
        let hourRange          = startHour..<endHour
        
        let hourString         = arrivalTime.substring(with: hourRange)
        
        return "\(hourString) min"
    }
    
    func handleApiCallError(response : JSON, fromController controller: UIViewController){
        
        // This Part handle the error in api response
        uiFun.hideIndicatorLoader()
        let responseDic =  convertToDictionary(text: String(describing: response)) // Converting response to NSMutableDictionary
        
        if let status  = responseDic?["status"]{
            print("If LEt Unwrapp \(status)")
            
            if(String(describing : status) == "fail"){
                
                if let errorCodeText = responseDic?["errorCodeText"]{
                    // Showing error in alert dialog by fetching erroText from api response
                    showAlert(title: "", message: errorCodeText as! String, logMessage: "cardBalanceAPIResponse()", fromController: controller)
                }
            }
        }
    }
    
    func convertDateFormater(date: String) -> String
    {
        let splitDate               = date.components(separatedBy: "/")
        if Int(splitDate[0])! > 2000 {
            return ""
        }else{
            let dateFormatter           = DateFormatter()
            dateFormatter.dateFormat    = "dd/MM/yyyy"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat    = "yyyy/MM/dd"
            return dateFormatter.string(from: date!)
        }
    }
    
    func converDateFormaterEvent(date : String) -> String{
      
        let splitDate               = date.components(separatedBy: "/")
        if Int(splitDate[0])! > 2000 {
            return ""
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return  dateFormatter.string(from: date!)
        }
    }
    
    func getTwoDigitTime(time : String) -> String{
        
        if(time.characters.count == 1){
            let realTime = "0\(time)"
            return realTime
        }else{
            return time
        }
    }
    
    func showExceptionDialog(jsonResponse : JSON, fromViewController : UIViewController){
        
        uiFun.hideIndicatorLoader()
        let jsonString = String(describing: jsonResponse)
        
        let dict   = uiFun.convertToDictionary(text: jsonString)
        //print("JSON STRING FOR DIC \(dict?["errorCodeText"]!)")
        
        var errorString : String!
        if(jsonResponse["code"] == nil || jsonResponse["code"] == ""){
            errorString = "No Internet Connection"
        }else{
            if(dict?["errorCodeText"] != nil){
                errorString = String(describing :dict?["errorCodeText"]!)
            }else if(dict?["message"] != nil){
                if dict != nil{
                    errorString = String(describing: dict?["message"])
                    print(String(describing: errorString))
                }
                errorString = String(describing :dict?["message"]!)
            }else{
                errorString = "We are facing some error. Be right back"
            }
        }
        
        uiFun.showAlert(title: "", message: errorString, logMessage: errorString, fromController: fromViewController)
    }
    
    func getCurrentDate() -> String{
    
        let date             = Date()
        let formatter        = DateFormatter()
        formatter.dateFormat = "MMM d, yyyy"
        let result           = formatter.string(from: date)
        return result
    }
}
