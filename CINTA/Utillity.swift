//
//  Utillity.swift
//  CINTA
//
//  Created by Mayur on 03/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class Utillity: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendRequest(url: String, parameters: [String : AnyObject], fromViewController : UIViewController,completionHandler: @escaping (JSON?, NSError?) -> Void) {
        
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        if(uiFun.isConnectedToNetwork()){
            let parameterString = parameters.stringFromHttpParameters()
            let requestURL = URL(string:"\(url)?\(parameterString)")!       // api url
            print("API Call URL \(String(describing : requestURL))")
            
            let headers             = ["Content-Type": "application/x-www-form-urlencoded","Cache-Control": "no-cache"]
            
            var request                     = URLRequest(url: requestURL)
            request.httpMethod              = "POST"
            request.httpBody                = parameterString.data(using: .utf8)
            request.allHTTPHeaderFields     = headers
            
            
            if(url.range(of: "updateProfileImageIOS.php") != nil){
                request.setValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            }
            
            let task1 = session.dataTask(with: request) { (data, response, error) in
                // api calling errors
                if error != nil{
                    uiFun.hideIndicatorLoader()
                    if(error.debugDescription.contains("timed out")){
                        uiFun.showAlert(title: "Network Error", message: "Request Timed Out", logMessage: "Req Timed Out", fromController: fromViewController)
                    }else if(error.debugDescription.contains("connection was lost")){
                        uiFun.showAlert(title: "Network Error", message: "Network Connection Was Lost. Please Try Again!", logMessage: "connection was lost", fromController: fromViewController)
                    }else if(error.debugDescription.contains("appears to be offline")){
                        uiFun.showAlert(title: "Network Error", message: "Network Connection Appears To Be Offline. Please Check!", logMessage: "Network Error", fromController: fromViewController)
                    }
                    print("Certificate Error -> \(error!)")
                    return
                }else{
                    // api json response
                    if let urlContent = data{
                        
                        do {
                            
                            print("Upload Pic Response \(urlContent)")
                            let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                            
                            print("==========================================")
                            print("Got The Response \(JSON(jsonResult))")
                            print("==========================================")
                            
                            DispatchQueue.main.sync(execute: {
                                completionHandler(JSON(jsonResult), nil)
                            })
                        }
                        catch{
                            uiFun.hideIndicatorLoader()
                            let jsonString = self.SendCompletionHandler(errorMessage: "Failed")
                            completionHandler(jsonString, nil)
                            print("Failed To Convert JSON")
                        }
                    }
                }
            }
            task1.resume()
        }else{
           // completionHandler(jsonString, nil)
        }
    }
    
    /****************** GET API CALL ***********************/
    func GetAPICall(apiUrl:String,fromViewController : UIViewController, completionHandler: @escaping (JSON?, NSError?) -> Void ){
        
        if(uiFun.isConnectedToNetwork()){
            let url = NSURL(string: apiUrl)!            /// api call
            
            let request = NSMutableURLRequest(url: url as URL)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                
                if error != nil{
                    // api calling errors
                    if(error.debugDescription.contains("timed out")){
                        uiFun.showAlert(title: "Error", message: "Request Timed Out", logMessage: "Request Timed Out", fromController: fromViewController)
                    }else if(error.debugDescription.contains("connection was lost")){
                        uiFun.showAlert(title: "Error", message: "The network connection was lost.", logMessage: "The network connection was lost.", fromController: fromViewController)
                    }else if(error.debugDescription.contains("appears to be offline")){
                        uiFun.showAlert(title: "Error", message: "The Internet connection appears to be offline.", logMessage: "The Internet connection appears to be offline.", fromController: fromViewController)
                    }else if(error.debugDescription.contains("not connect to")){
                        uiFun.showAlert(title: "Error", message: "Could not connect to the server", logMessage: "Could not connect to the server", fromController: fromViewController)

                    }
                    
                    print("Certificate Error -> \(error!)")
                    return
                }else{
                    
                    if let urlContent = data{
                        
                        do {
                            // api response
                            let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                            
                            print("Yo Yo \(jsonResult)")
                            DispatchQueue.main.sync(execute: {
                                print("Completion Of Timetable Stops")
                                completionHandler(JSON(jsonResult), nil)
                                
                            })
                            
                        }
                        catch{
                            uiFun.hideIndicatorLoader()
                            print("Failed To Convert JSON")
                            let jsonString = self.SendCompletionHandler(errorMessage: "Failed")
                            completionHandler(jsonString, nil)
                        }
                    }
                }
                
            }
            task.resume()
        }else{
            
            // if there is no json response
            let jsonString = SendCompletionHandler(errorMessage: "No Internet Connection")
            completionHandler(jsonString, nil)
        }
        
    }
    
    
    // Error JSON Creation Function
    func SendCompletionHandler(errorMessage : String) -> JSON{
        // if some error occured it will send the json response with error
        var errorText                           = errorMessage
        
        let para      : NSMutableDictionary     = NSMutableDictionary()
        let errorDic  : NSMutableDictionary     = NSMutableDictionary()
        
        errorDic.setValue(errorText, forKey: "errorCodeText")
        
        para.setValue("fail", forKey: "status")
        para.setValue(errorText, forKey: "errorCodeText")
        
        let jsonData: NSData
        do{
            jsonData       = try JSONSerialization.data(withJSONObject: para, options: JSONSerialization.WritingOptions()) as NSData
            let jsonString      = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue) as! String
            let jsonParseString = JSON(jsonString)
            print("no internet json string = \(jsonParseString)")
            return jsonParseString
        } catch _ {
            print ("UH OOO")
            
        }
        return nil
    }
}
