//
//  CheckPasswordAPICall.swift
//  CINTA
//
//  Created by Mayur on 06/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CheckPasswordAPICall {

    func makeAPICall(fromViewController : ViewController, memberId : String, password : String){
        
        let url                         = "\(constant.baseURL)selectIdandPw.php"
        let params : [String:AnyObject] = ["member_id" : memberId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "password"  : password.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.passwordCheckAPIResponse(response: jsonResponse)
                return
                
            }else {
                print("CINTA Password Check API Failed")
                return
            }
            
        })
        
    }
}
