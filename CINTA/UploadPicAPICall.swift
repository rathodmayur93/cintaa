//
//  UploadPicAPICall.swift
//  CINTA
//
//  Created by Mayur on 17/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import Alamofire

class UploadPicAPICall: NSObject {

    func makeAPICall(fromViewController : ProfileViewController, member_id : String, encodedString : String){


        let url                         = "\(constant.baseURL)updateProfileImageIOS.php"
        let params : [String:AnyObject] = ["member_id"         : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "encoded_string"    : encodedString.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url)")
        
        
//        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
//            
//            
//            print("Response \(result)")
//            if let jsonResponse = result{
//                
//                return
//                
//            }else {
//                print("Schedule Entry Failed")
//                return
//            }
//            
//        })
        
//        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
//            .responseJSON { response in
//                print(response.request as Any)          // original URL request
//                print(response.response as Any)        // URL response
//                print(response.result.value as Any)    // result of response serialization
//        }
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                fromViewController.uploadUserProfileRersponse(response: jsonResponse)
                print("Image Upload Response","\(jsonResponse)")
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
