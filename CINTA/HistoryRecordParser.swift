//
//  HistoryRecordParser.swift
//  CINTA
//
//  Created by Mayur on 10/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var multiEventList        = [HistoryRecords]()

class HistoryRecordParser {

    func parseHistoryRecords(index : Int){
    
        let historyRecordsFromDB        = HistoryRecordDB.shared.fetchBookingItems()
        let historyRecord               = HistoryRecords()
        
        historyRecord.entry_id          = historyRecordsFromDB[index].entry_id
        historyRecord.shoot_date        = historyRecordsFromDB[index].shoot_date
        historyRecord.film_tv_name      = historyRecordsFromDB[index].film_tv_name
        historyRecord.shift_time        = historyRecordsFromDB[index].shift_time
        historyRecord.call_time         = historyRecordsFromDB[index].call_time
        historyRecord.shoot_location    = historyRecordsFromDB[index].shoot_location
        historyRecord.lat               = historyRecordsFromDB[index].lat
        historyRecord.lng               = historyRecordsFromDB[index].lng
        historyRecord.schedule_status   = historyRecordsFromDB[index].schedule_status
        historyRecord.producerName      = historyRecordsFromDB[index].producer_name
        historyRecord.productioHouse    = historyRecordsFromDB[index].house_name
        
        multiEventList.append(historyRecord)
    }
}
