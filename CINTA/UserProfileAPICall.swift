//
//  UserProfileAPICall.swift
//  CINTA
//
//  Created by Mayur on 07/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class UserProfileAPICall: NSObject {

    func makeAPICall(fromViewController : ViewController, memberId : String){
        
        let url                         = "\(constant.baseURL)MemberProfileInfo.php"
        let params : [String:AnyObject] = ["member_id" : memberId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.userProfileResponse(response: jsonResponse)
                return
                
            }else {
                print("CINTA Send OTP Failed")
                return
            }
            
        })
        
    }
    
    func makeAPICall2(fromViewController : CreateScheduleEntryViewController, memberId : String){
        
        let url                         = "\(constant.baseURL)MemberProfileInfo.php"
        let params : [String:AnyObject] = ["member_id" : memberId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.userProfileResponse(response: jsonResponse)
                return
                
            }else {
                print("CINTA Send OTP Failed")
                return
            }
            
        })
        
    }
}
