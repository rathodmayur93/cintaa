
//
//  ProfileViewController.swift
//  CINTA
//
//  Created by Mayur on 16/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet var scrollView        : UIScrollView!
    @IBOutlet var profileImage      : UIImageView!
    @IBOutlet var welcomeLabel      : UILabel!
    @IBOutlet var mainScrollView    : UIView!
    
    @IBOutlet var userName          : UITextField!
    @IBOutlet var gender            : UITextField!
    @IBOutlet var address           : UITextField!
    @IBOutlet var city              : UITextField!
    @IBOutlet var state             : UITextField!
    @IBOutlet var zipCode           : UITextField!
    @IBOutlet var country           : UITextField!
    @IBOutlet var emailID           : UITextField!
    @IBOutlet var mobile            : UITextField!
    @IBOutlet var alternateContact  : UITextField!
    @IBOutlet var website           : UITextField!
    @IBOutlet var separatorView     : UIView!
    
    var selectImageFrom     = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        FetchProfileDetail()
        disableAllTextField()
        
        CleverTap.sharedInstance().recordEvent("Profile Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentSize  = CGSize(width: self.view.frame.width, height: 700)
    }
    
    // MARK:- UI
    func setUI(){
        
        let button1 = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editAction)) // action:#selector(Class.MethodName) for swift 3
        self.navigationItem.rightBarButtonItem  = button1
        
        // Making Profile Image Round
        profileImage.layer.borderWidth         = 1.0
        profileImage.layer.masksToBounds       = false
        profileImage.layer.borderColor         = UIColor.black.cgColor
        profileImage.layer.cornerRadius        = profileImage.frame.size.width/2
        profileImage.clipsToBounds             = true
        
        // Tap Gesture Of Profile Image
        let tapGesture                                = UITapGestureRecognizer(target: self, action: #selector(showBottomSheet))
        tapGesture.numberOfTapsRequired               = 1
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGesture)
        
        // Drop Shadow 
        separatorView.dropShadow()
    }
    
    func updateUserDataUI(response : JSON){
        
        let myProfile   = response["Myprofiledata"]
        
        welcomeLabel.text       = "Welcome, \(String(describing : myProfile.arrayValue[0]["name"]))"
        userName.text           = String(describing : myProfile.arrayValue[0]["name"])
        gender.text             = String(describing : myProfile.arrayValue[0]["gender"])
        address.text            = String(describing : myProfile.arrayValue[0]["address"])
        city.text               = String(describing : myProfile.arrayValue[0]["city"])
        state.text              = String(describing : myProfile.arrayValue[0]["state"])
        zipCode.text            = String(describing : myProfile.arrayValue[0]["zip"])
        country.text            = String(describing : myProfile.arrayValue[0]["country"])
        emailID.text            = String(describing : myProfile.arrayValue[0]["email"])
        mobile.text             = String(describing : myProfile.arrayValue[0]["mobile"])
        website.text            = String(describing : myProfile.arrayValue[0]["website"])
        alternateContact.text   = String(describing : myProfile.arrayValue[0]["alt_mobile"])
        
        let profileImageURL     = String(describing : myProfile.arrayValue[0]["profile_imgurl"])
        print("Profile imgage url is \(profileImageURL)")
        // Loading imag from url
        if(profileImageURL != ""){
            uiFun.loadImageFromURL(url: profileImageURL, completionHandler: { (result, error) in
                
                if result != nil{
                    if let mediaLinkData            = UIImage(data: result!){
                        self.profileImage.image     = mediaLinkData
                        
                    }
                }else{
                    
                    print("Image Set Failed....Try Harder Mayur ")
                    return
                }
            })
        }
    }
    
    // MARK:- Button Actions
    
    @IBAction func submitAction(_ sender: Any) {
        UpdateProfileInfoAPICall()
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        
        editAction()
    }
    
    @IBAction func overflowAction(_ sender: Any) {
        openActionSheet()
    }
    
    
    
    // MARK:- ImagePicker Protocol
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        self.dismiss(animated: true, completion: { () -> Void in
            // Selected image data
            let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
            if(self.selectImageFrom == "gallery"){
                let imageURL          = info[UIImagePickerControllerReferenceURL] as! NSURL // Selected Image path
                print("User profile image \(imageURL)")
                let imageName         = imageURL.lastPathComponent          // Selected image name
                print("User progile \(String(describing: imageName))")
            }
            
            let compressImageData           = chosenImage.resized(withPercentage: 0.1)    // compressing image data
            let imageData                   = UIImagePNGRepresentation(compressImageData!)//(compressImageData!, 1.0)//(chosenImage, 0.0)
            self.profileImage.contentMode   = .scaleAspectFit                             // Scalling image
            self.profileImage.image         = chosenImage
            
            print("Profile Image data \(String(describing: imageData))")
            let strBase64             = imageData?.base64EncodedString() // Converting image into base64
            let base64Encoded         = strBase64?.data(using: .utf8, allowLossyConversion: false)  // encoding base64
            let base64httpEncoding    = strBase64?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            
            self.UploadPicAPI(base64Encoded: (strBase64?.replacingOccurrences(of: "+", with: "%2B"))!)
            print("===============================================")
           // print("Only Base64 \(strBase64)")
            print("===============================================")
        })
        
    }
    
    // MARK:- API Call
    
    func fetchUserDetailAPICall(){
        uiFun.showIndicatorLoader()
        let fetchUsers  = FetchUserDetail()
        fetchUsers.makeAPICall(fromViewController: self, member_id: uiFun.readData(key: "member_id"))
    }
    
    func UploadPicAPI(base64Encoded : String){
        
        let uploadPic   = UploadPicAPICall2()
        uploadPic.makeAPICall(fromViewController: self, member_id: uiFun.readData(key: "member_id"), encodedString: base64Encoded)
    }
    
    func DeleteProfilePic(){
        
        let deleteDp    = DeleteProfilePicAPI()
        deleteDp.makeAPICall(fromViewController: self, member_id: uiFun.readData(key: "member_id"))
    }
    
    func UpdateProfileInfoAPICall(){
        
        uiFun.showIndicatorLoader()
        let updateProfile   = UpdateProfileAPICall()
        updateProfile.makeAPICall(fromViewController: self, member_id: uiFun.readData(key: "member_id"), name: userName.text!, gender: gender.text!, address: address.text!, city: city.text!, state: state.text!, country: country.text!, zip: zipCode.text!, email: emailID.text!, password: uiFun.readData(key: "password"), mobile: mobile.text!, alt_mobile: alternateContact.text!, website: website.text!)
    }
    
    
    // MARK:- API Response
    func fetchedUserApiResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        updateUserDataUI(response: response)
    }
    
    func uploadUserProfileRersponse(response : JSON){
        
    }
    
    func deleteProfilePicResponse(response : JSON){
    
    }
    
    func updateProfileAPIResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        uiFun.showAlert(title: "Notification", message:"Profile Updated Successfully" , logMessage: "Profile Updated", fromController: self)
    }
    
    // MARK:- Common Function
    func editAction(){
    
        userName.isEnabled          = true
        gender.isEnabled            = true
        address.isEnabled           = true
        city.isEnabled              = true
        state.isEnabled             = true
        zipCode.isEnabled           = true
        country.isEnabled           = true
        emailID.isEnabled           = true
        mobile.isEnabled            = true
        website.isEnabled           = true
        alternateContact.isEnabled  = true
    }
    
    func showBottomSheet(){
    
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.deleteProfilePic()
            
        })
        
        let saveAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openCameraButton()
        })
        
        let choosePhoto = UIAlertAction(title: "Choose Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.fromGallery()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(choosePhoto)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    // Upload Photo from gallery
    func fromGallery(){
        // Uploading pic from gallery
        selectImage(from: UIImagePickerControllerSourceType.photoLibrary)
        selectImageFrom = "gallery"
    }
    
    func selectImage(from source: UIImagePickerControllerSourceType){
        // Selected image source type
        let imagePickerController           = UIImagePickerController()
        imagePickerController.delegate      = self
        imagePickerController.sourceType    = source
        imagePickerController.allowsEditing = false
        
        self.present(imagePickerController, animated: true, completion: nil)
    }
    
    // MARK:- Action Sheet Functions
    func openActionSheet(){
        
        let dotMenu      = UIAlertController(title: nil, message: "Select Any Option", preferredStyle: .actionSheet)
        
        let instructionAction       = UIAlertAction(title: "Instructions", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.instructionTapped()
        })
        
        let helpAction       = UIAlertAction(title: "Help Center", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.helpCenterTapped()
        })
        
        let privacyAction       = UIAlertAction(title: "Privacy Policy", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.privacyViewTapped()
        })
        
        let rateAction       = UIAlertAction(title: "Rate Us", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.rateApp()
        })
        
        let shareAction       = UIAlertAction(title: "Share", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let objectsToShare = [constant.shareContent]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        })
        
        let profileAction       = UIAlertAction(title: "My Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.profileTapped()
        })
        
        let mouAction       = UIAlertAction(title: "MOU", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mouViewTapped()
        })
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            MemberProfileInfo.shared.dropTable()
            HistoryRecordDB.shared.dropTable()
            AuthorityDatabse.shared.dropTable()
            
            uiFun.writeData(value: "", key: "member_id")
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            print("File Logour")
        })
        
        let exitAction       = UIAlertAction(title: "Exit", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            exit(0)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        dotMenu.addAction(instructionAction)
        dotMenu.addAction(helpAction)
        dotMenu.addAction(privacyAction)
        dotMenu.addAction(rateAction)
        dotMenu.addAction(shareAction)
        dotMenu.addAction(profileAction)
        dotMenu.addAction(mouAction)
        dotMenu.addAction(logoutAction)
        dotMenu.addAction(exitAction)
        dotMenu.addAction(cancelAction)
        
        
        self.present(dotMenu, animated: true, completion: nil)
    }
    
    func helpCenterTapped(){
        webViewHeader   = "Help Center"
        loadUrl         = "http://www.cintaa.net/cintaa-help-center/"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func privacyViewTapped(){
        loadUrl         = "http://www.cintaa.net/privacy-policy/"
        webViewHeader   = "Privacy Policy"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func mouViewTapped(){
        loadUrl         = "\(constant.baseURL)mou.pdf"
        webViewHeader   = "MOU"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func rateUsViewTapped(){
        
        webViewHeader   = "Rate Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func instructionTapped(){
        
        loadUrl         = "\(constant.baseURL)instructions.pdf"
        webViewHeader   = "Instruction"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func contactUs(){
        
        loadUrl         = "http://www.cintaa.net/contact-us/"
        webViewHeader   = "Contact Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func profileTapped(){
        uiFun.navigateToScreem(identifierName: "ProfileViewController", fromController: self)
    }
    
    func rateApp() {
        guard let url = URL(string : "https://itunes.apple.com/us/app/cintaa/id1288952015?mt=8") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    // Upload Photo from camera
    func openCameraButton() {
        // Upload pic from the camera
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker             = UIImagePickerController()
            imagePicker.delegate        = self
            imagePicker.sourceType      = UIImagePickerControllerSourceType.camera;
            selectImageFrom             = "camera"
            imagePicker.allowsEditing   = false
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            // If iPhone doesnt have camera :-(
            uiFun.showAlert(title: "No Camera", message: "This device doesn't have camera", logMessage: "NO camera in device", fromController: self)
        }
    }
    
    func deleteProfilePic(){
        profileImage.image  = UIImage(named: "man")
        DeleteProfilePic()
    }
    
    func disableAllTextField(){
    
        userName.isEnabled          = false
        gender.isEnabled            = false
        address.isEnabled           = false
        city.isEnabled              = false
        state.isEnabled             = false
        zipCode.isEnabled           = false
        country.isEnabled           = false
        emailID.isEnabled           = false
        mobile.isEnabled            = false
        website.isEnabled           = false
        alternateContact.isEnabled  = false
        
    }
    
    
    func FetchProfileDetail(){
    
        if(uiFun.isConnectedToNetwork()){
            fetchUserDetailAPICall()
        }else{
            FillProfileDetail()
        }
    }
    
    func FillProfileDetail(){
        
        let userProfileInfo     = MemberProfileInfo.shared.fetchBookingItems()
        
        userName.text           = userProfileInfo[0].nameAndimage[0].name
        gender.text             = userProfileInfo[0].nameAndimage[0].gender
        address.text            = userProfileInfo[0].nameAndimage[0].address
        city.text               = userProfileInfo[0].nameAndimage[0].city
        state.text              = userProfileInfo[0].nameAndimage[0].state
        zipCode.text            = userProfileInfo[0].nameAndimage[0].zip
        country.text            = userProfileInfo[0].nameAndimage[0].country
        emailID.text            = userProfileInfo[0].nameAndimage[0].email
        mobile.text             = userProfileInfo[0].nameAndimage[0].mobile
        website.text            = userProfileInfo[0].nameAndimage[0].website
        alternateContact.text   = userProfileInfo[0].nameAndimage[0].alt_mobile
        
        let profileImageURL     = userProfileInfo[0].nameAndimage[0].profile_imgurl
        
        // Loading imag from url
        if(profileImageURL != ""){
            uiFun.loadImageFromURL(url: profileImageURL, completionHandler: { (result, error) in
                
                if result != nil{
                    if let mediaLinkData            = UIImage(data: result!){
                        self.profileImage.image     = mediaLinkData
                    }
                }else{
                    print("Image Set Failed....Try Harder Mayur ")
                    return
                }
            })
        }
    }
}


extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize  = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize  = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    func resized(toHeight height: CGFloat) -> UIImage? {
        let canvasSize  = CGSize(width: CGFloat(ceil(height/size.width * size.height)), height: height)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
