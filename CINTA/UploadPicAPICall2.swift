//
//  UploadPicAPICall2.swift
//  CINTA
//
//  Created by Mayur on 19/11/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class UploadPicAPICall2 {

    func makeAPICall(fromViewController : ProfileViewController, member_id : String, encodedString : String){
    
        let headers             = ["Content-Type": "application/x-www-form-urlencoded"]
        print("Member id \(member_id) & encoded String \(encodedString)")
        let postDataString      = "member_id=T11&encoded_string=\(encodedString)"
        let postDataStringData  = postDataString.data(using: String.Encoding.utf8)
        
        let request             = NSMutableURLRequest(url: NSURL(string: "http://web2101.cftclients.com/app_api/updateProfileImageIOS.php")! as URL,
                                                      cachePolicy: .useProtocolCachePolicy,
                                                      timeoutInterval: 60.0)
        request.httpMethod              = "POST"
        request.allHTTPHeaderFields     = headers
        request.httpBody                = postDataStringData! as Data
        
        let session  = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
                uiFun.hideIndicatorLoader()
                if(error.debugDescription.contains("timed out")){
                    uiFun.showAlert(title: "Network Error", message: "Request Timed Out", logMessage: "Req Timed Out", fromController: fromViewController)
                }else if(error.debugDescription.contains("connection was lost")){
                    uiFun.showAlert(title: "Network Error", message: "Network Connection Was Lost. Please Try Again!", logMessage: "connection was lost", fromController: fromViewController)
                }else if(error.debugDescription.contains("appears to be offline")){
                    uiFun.showAlert(title: "Network Error", message: "Network Connection Appears To Be Offline. Please Check!", logMessage: "Network Error", fromController: fromViewController)
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
                
                if let urlContent = data{
                    
                    do {
                        
                        let jsonResult = try JSONSerialization.jsonObject(with: urlContent, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        
                        print("Yo Yo \(jsonResult)")
                        DispatchQueue.main.sync(execute: {
                            fromViewController.uploadUserProfileRersponse(response: JSON(jsonResult))
                            
                        })
                    }
                    catch{
                        print("Failed To Convert JSON")
                    }
                }
                
            }
        })
        
        dataTask.resume()
    }
}
