//
//  CallInTimeAPI.swift
//  CINTA
//
//  Created by Mayur on 15/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CallInTimeAPI: NSObject {

    func makeAPICall(fromViewController : NewDiaryEntryViewController, entry_id : String, callin_time : String, callin_location : String, callin_lat : String, callin_lng : String){
        
        let url                         = "\(constant.baseURL)updateCallinTime.php"
        let params : [String:AnyObject] = ["entry_id"        : entry_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "callin_time"     : callin_time.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "callin_location" : callin_location.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "callin_lat"      : callin_lat.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "callin_lng"      : callin_lng.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "callin_entry_type"    : "Automatic" as AnyObject]
        
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        CleverTap.sharedInstance().recordEvent("InTime API Call URL \(url)")
        CleverTap.sharedInstance().recordEvent("InTime API Call Params", withProps: params)
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.callInTimeResponse(response: jsonResponse)
                return
                
            }else {
                print("Calling Time Entry Failed")
                return
            }
            
        })
        
    }
}
