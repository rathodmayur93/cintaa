//
//  ChangePasswordAPICall.swift
//  CINTA
//
//  Created by Mayur on 22/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class ChangePasswordAPICall: NSObject {

    func makeAPICall(fromViewController : SetNewPasswordViewController, memberId : String, password : String){
        
        let url                         = "\(constant.baseURL)changepassword.php"
        let params : [String:AnyObject] = ["member_id" : memberId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "password" : password.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.changePasswordAPIResponse(response: jsonResponse)
                return
                
            }else {
                print("CINTA Change Password Failed")
                return
            }
            
        })
        
    }
}
