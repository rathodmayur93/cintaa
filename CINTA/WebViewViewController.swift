//
//  WebViewViewController.swift
//  CINTA
//
//  Created by Mayur on 21/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var toolbarHeading    : UILabel!
    @IBOutlet var webView           : UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        webView.delegate    = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Webview Delegate Function
    public func webViewDidStartLoad(_ webView: UIWebView){
        uiFun.showIndicatorLoader()
    }
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
        uiFun.hideIndicatorLoader()
    }
    
    
    
    // MARK:- UI Function
    func setUI(){
        
        toolbarHeading.text = webViewHeader
        let url             = URL (string: loadUrl)
        let requestObj      = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        tabBarIndex = 3
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
}
