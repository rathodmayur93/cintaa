//
//  Colors.swift
//  CINTA
//
//  Created by Mayur on 30/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class Colors {
    
    var whiteColor          = "#fffdee"
    var bgColor             = "#e6c398"
    var strokeColor         = "#e6c398"
    var backgroundColor     = "#fffbdb"
    var calendarEvent       = "#ff3f80"
    
    var todayDateColor      = "#795c41"
}
