//
//  ShiftTimeAPICalll.swift
//  CINTA
//
//  Created by Mayur on 17/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class ShiftTimeAPICalll: NSObject {

    func makeAPICall(fromViewController : NewDiaryEntryViewController){
        
        let url                         = "\(constant.baseURL)selectShiftTime.php"
        print("==============================================")
        print("API Call URL \(url)")
        
        CleverTap.sharedInstance().recordEvent("Shift Time API Call \(url)")
        
        
        // MAke API Call
        util.GetAPICall(apiUrl: url, fromViewController: fromViewController) { (result, error) in
            
            if let jsonResponse = result{
                fromViewController.shiftTimeAPIResponse(response: jsonResponse)
            }else{
                print("Shift Time API Call Fail")
            }
        }
    }
}
