//
//  CompleteShootViewController.swift
//  CINTA
//
//  Created by Mayur on 16/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON
import iShowcase

var detailShootEntryId          = ""
var completedShootDetailList    = [CompletedShootDetailModel]()

class CompleteShootViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, iShowcaseDelegate {

    
    @IBOutlet var segementControl   : UISegmentedControl!
    @IBOutlet var searchTF          : UITextField!
    @IBOutlet var tableView         : UITableView!
    @IBOutlet var container         : UIView!
    
    let datePickerView              : UIDatePicker   = UIDatePicker()
    var completedShootList          = [CompletedShootModel]()
    var filterShootList             = [CompletedShootModel]()
    var showcase                    = iShowcase()
    
    var childView                   = CompltedShootDetailViewController()
    
    var isShowingTour2              = true
    var isShowingTour3              = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBarIndex = 0
        setUI()
        
        childView           = self.childViewControllers.last as! CompltedShootDetailViewController
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideContainerView), name: NSNotification.Name(rawValue:"DismissContainer"), object: nil)
        
        showcase.delegate   = self
        
        CleverTap.sharedInstance().recordEvent("Completed Shoot Screen")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UI
    func setUI(){
        
        completedShootAPICall()
        searchTF.layer.borderColor  = UIColor.black.cgColor
        searchTF.layer.borderWidth  = 1.0
        container.isHidden          = true
    }
    
    // MARK:- Table View Protocols
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return filterShootList.count + 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell        = tableView.dequeueReusableCell(withIdentifier: "cell") as! CompletedShootTableViewCell
        if(indexPath.row == 0){
            
            cell.date.text              = "Date"
            cell.date.font              = UIFont.boldSystemFont(ofSize: cell.date.font.pointSize)
            
            cell.productionHouse.text   = "Production House"
            cell.productionHouse.font   = UIFont.boldSystemFont(ofSize: cell.date.font.pointSize)
            
            cell.location.text          = "Location"
            cell.location.font          = UIFont.boldSystemFont(ofSize: cell.date.font.pointSize)
            
            cell.shiftTime.text         = "Shift Time"
            cell.shiftTime.font         = UIFont.boldSystemFont(ofSize: cell.date.font.pointSize)
        
        }else{
                
            cell.date.text              = filterShootList[indexPath.row - 1].shoot_date
            cell.productionHouse.text   = filterShootList[indexPath.row - 1].house_name
            cell.location.text          = filterShootList[indexPath.row - 1].shoot_location
            cell.shiftTime.text         = filterShootList[indexPath.row - 1].shift_time
            print(indexPath.row)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.row != 0){
            detailShootEntryId  = filterShootList[indexPath.row - 1].entry_id
            makeCompletedShootDetailAPICall()
        }
        
    }
    
    func iShowcaseDismissed(_ showcase: iShowcase) {
        
        if(isShowingTour2){
            showTour2()
        }else if(isShowingTour3){
            showTour3()
        }
    }
    
    // MARK:- Actions
    
    @IBAction func segmentAction(_ sender: Any) {
        
        switch segementControl.selectedSegmentIndex {
        case 0:
            searchTF.placeholder = "Search by Date"
            searchTF.text        = ""
            break
        case 1:
            searchTF.placeholder = "Search by Prod. House"
            searchTF.text        = ""
            self.view.endEditing(true)
            break
        case 2:
            searchTF.placeholder = "Search by Film/TV Serial"
            searchTF.text        = ""
            self.view.endEditing(true)
            break
        default:
            searchTF.placeholder = "Search by Date"
            break
        }
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        
        if(segementControl.selectedSegmentIndex == 1){
            
        }
    }
    
    @IBAction func textFieldAction(_ sender: UITextField) {
        
        if(segementControl.selectedSegmentIndex == 0){
            
            datePickerView.datePickerMode   = UIDatePickerMode.date
            sender.inputView                = datePickerView
            datePickerView.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
        }else{
            sender.inputView        = nil
        }
    }
    
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        print(sender.text!)
        
        if(sender.text! == ""){
            filterShootList = completedShootList
        }else{
            
            switch segementControl.selectedSegmentIndex {
            case 0:
                break
            case 1:
                filterProductionHouse(text: sender.text!)
                break
            case 2:
                filterFilmTVSerial(text: sender.text!)
                break
            default:
                break
            }
        }
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter       = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        searchTF.text           = dateFormatter.string(from: sender.date)
        
    }
    
    @IBAction func tourAction(_ sender: Any) {
        
        //showTour1()
        
        isShowingTour2      = true
        isShowingTour3      = true
        
        self.openActionSheet()
    }
    
    
    // MARK:- API Call
    
    func completedShootAPICall(){
        
        if(uiFun.isConnectedToNetwork()){
            uiFun.showIndicatorLoader()
            let completedShootAPI   = CompletedScheduleAPICall()
            completedShootAPI.makeAPICall(fromViewController: self, member_id: uiFun.readData(key: "member_id"))
        }else{
            uiFun.showAlert(title: "No Internet", message: "Oops..Look like there is no internet connection", logMessage: "no internet", fromController: self)
        }
    }
    
    func makeCompletedShootDetailAPICall(){
        
        uiFun.showIndicatorLoader()
        let detailShoot     = CompletedShootDetailAPICall()
        detailShoot.makeAPICall(fromViewController: self, entryId: detailShootEntryId)
    }
    
    // MARK:- API Responses
    
    func completedShootAPIResponse(response : JSON){
    
        uiFun.hideIndicatorLoader()
        let completedEntryJSON  = response["CompletedEntryTable"]
        print("Completed Shoot Response Count \(completedEntryJSON.count)")
        var prop = [String : String]()
        
        for i in 0..<completedEntryJSON.count{
            
            let completedShootModel             = CompletedShootModel()
            
            completedShootModel.alies_name      = String(describing: completedEntryJSON.arrayValue[i]["alies_name"])
            completedShootModel.shift_time      = String(describing: completedEntryJSON.arrayValue[i]["shift_time"])
            completedShootModel.shoot_date      = String(describing: completedEntryJSON.arrayValue[i]["shoot_date"])
            completedShootModel.entry_id        = String(describing: completedEntryJSON.arrayValue[i]["entry_id"])
            completedShootModel.shoot_location  = String(describing: completedEntryJSON.arrayValue[i]["shoot_location"])
            completedShootModel.film_tv_name    = String(describing: completedEntryJSON.arrayValue[i]["film_tv_name"])
            completedShootModel.house_name      = String(describing: completedEntryJSON.arrayValue[i]["house_name"])
            //print("Completd Shoot \(String(describing: completedEntryJSON.arrayValue[i]["film_tv_name"]))")
            
            prop[String(describing: completedEntryJSON.arrayValue[i]["shoot_date"])] = String(describing: completedEntryJSON.arrayValue[i]["film_tv_name"])
            
            completedShootList.append(completedShootModel)
            
            filterShootList = completedShootList
        }
        
        CleverTap.sharedInstance().recordEvent("Completed Shoot Response", withProps: prop)
        
        tableView.reloadData()
    }
    
    func completedShootDetailAPIResponse(response : JSON){
        
        uiFun.hideIndicatorLoader()
        
        completedShootDetailList.removeAll()
        
        let detailShootModel                    = CompletedShootDetailModel()
        
        let completedDetail                     = response["CompletedDetails"]
        let completedDetailObj                  = completedDetail.arrayValue[0]
        
        detailShootModel.film_tv_name           = String(describing: completedDetailObj["film_tv_name"])
        detailShootModel.house_name             = String(describing: completedDetailObj["house_name"])
        detailShootModel.alies_name             = String(describing: completedDetailObj["alies_name"])
        detailShootModel.producer_name          = String(describing: completedDetailObj["producer_name"])
        detailShootModel.shoot_date             = String(describing: completedDetailObj["shoot_date"])
        
        detailShootModel.shoot_location         = String(describing: completedDetailObj["shoot_location"])
        detailShootModel.played_character       = String(describing: completedDetailObj["played_character"])
        detailShootModel.shift_time             = String(describing: completedDetailObj["shift_time"])
        detailShootModel.rate_type              = String(describing: completedDetailObj["rate_type"])
        detailShootModel.rate                   = String(describing: completedDetailObj["rate"])
        
        detailShootModel.due_days               = String(describing: completedDetailObj["due_days"])
        detailShootModel.remark                 = String(describing: completedDetailObj["remark"])
        detailShootModel.callin_time            = String(describing: completedDetailObj["callin_time"])
        detailShootModel.packup_time            = String(describing: completedDetailObj["packup_time"])
        detailShootModel.authority_sign         = String(describing: completedDetailObj["authority_sign"])
        
        detailShootModel.authority_sign_date    = String(describing: completedDetailObj["authority_sign_date"])
        detailShootModel.authority_nm           = String(describing: completedDetailObj["authority_nm"])
        detailShootModel.authority_number       = String(describing: completedDetailObj["authority_number"])
        detailShootModel.call_time              = String(describing: completedDetailObj["call_time"])
        detailShootModel.fees                   = String(describing: completedDetailObj["fees"])
        
        detailShootModel.house_email            = String(describing: completedDetailObj["house_email"])
        
        completedShootDetailList.append(detailShootModel)
        
        CleverTap.sharedInstance().recordEvent("Completed Shoot Event Response \(response)")
        
        // Scroll to top 
        let offset = CGPoint(x: -childView.scrollView.contentInset.left, y: -childView.scrollView.contentInset.top)
        childView.scrollView.setContentOffset(offset, animated: true)
        
        container.isHidden  = false
        
        showDetailOfShoot()
    }
    
    // MARK:- Filtering Functions
    
    func filterProductionHouse(text : String){
        
        filterShootList = completedShootList.filter {$0.house_name.localizedCaseInsensitiveContains(text)}
        tableView.reloadData()
    }
    
    func filterFilmTVSerial(text : String){
    
        filterShootList = completedShootList.filter {$0.film_tv_name.localizedCaseInsensitiveContains(text)}
        tableView.reloadData()
    }
    
    // MARK:- Setting Response To Container View
    func showDetailOfShoot(){
    
        
        
        childView.film_tv_name.text       = completedShootDetailList[0].film_tv_name
        childView.house_name.text         = completedShootDetailList[0].house_name
        childView.alies_name.text         = completedShootDetailList[0].producer_name
        childView.dateOfShoot.text        = completedShootDetailList[0].shoot_date
        childView.shoot_location.text     = completedShootDetailList[0].shoot_location
        
        childView.played_character.text   = completedShootDetailList[0].played_character
        childView.shift_time.text         = completedShootDetailList[0].shift_time
        childView.call_time.text          = completedShootDetailList[0].call_time
        childView.rate.text               = completedShootDetailList[0].rate
        childView.due_days.text           = completedShootDetailList[0].due_days
        
        childView.remark.text             = completedShootDetailList[0].remark
        childView.fees.text               = completedShootDetailList[0].fees
        childView.call_in_time.text       = completedShootDetailList[0].callin_time
        childView.pack_up_time.text       = completedShootDetailList[0].packup_time
        childView.authority_name.text     = completedShootDetailList[0].authority_nm
        
        childView.authority_number.text   = completedShootDetailList[0].authority_number
        childView.authority_email.text    = completedShootDetailList[0].house_email
        childView.date_of_sign.text       = completedShootDetailList[0].authority_sign_date
        
        
        uiFun.loadImageFromURL(url: completedShootDetailList[0].authority_sign) { (result, error) in
            
            if result != nil{
                if let mediaLinkData                = UIImage(data: result!){
                    self.childView.signature_image.image = mediaLinkData
                    
                }
            }else{
                
                print("Image Set Failed....Try Harder Mayur ")
                return
            }
        }
        
    }
    
    func hideContainerView(){
    
        container.isHidden = true
    }
    
    // MARK:- Action Sheet
    func openActionSheet(){
        
        let dotMenu      = UIAlertController(title: nil, message: "Select Any Option", preferredStyle: .actionSheet)
        
        let instructionAction       = UIAlertAction(title: "Instructions", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.instructionTapped()
        })
        
        let helpAction       = UIAlertAction(title: "Help Center", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.helpCenterTapped()
        })
        
        let privacyAction       = UIAlertAction(title: "Privacy Policy", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.privacyViewTapped()
        })
        
        let rateAction       = UIAlertAction(title: "Rate Us", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.rateApp()
        })
        
        let shareAction       = UIAlertAction(title: "Share", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let objectsToShare = [constant.shareContent]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        })
        
        let profileAction       = UIAlertAction(title: "My Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.profileTapped()
        })
        
        let mouAction       = UIAlertAction(title: "MOU", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mouViewTapped()
        })
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            MemberProfileInfo.shared.dropTable()
            HistoryRecordDB.shared.dropTable()
            AuthorityDatabse.shared.dropTable()
            
            uiFun.writeData(value: "", key: "member_id")
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            print("File Logour")
        })
        
        let exitAction       = UIAlertAction(title: "Exit", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            exit(0)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        dotMenu.addAction(instructionAction)
        dotMenu.addAction(helpAction)
        dotMenu.addAction(privacyAction)
        dotMenu.addAction(rateAction)
        dotMenu.addAction(shareAction)
        dotMenu.addAction(profileAction)
        dotMenu.addAction(mouAction)
        dotMenu.addAction(logoutAction)
        dotMenu.addAction(exitAction)
        dotMenu.addAction(cancelAction)
        
        
        self.present(dotMenu, animated: true, completion: nil)
    }
    
    func helpCenterTapped(){
        webViewHeader   = "Help Center"
        loadUrl         = "http://www.cintaa.net/cintaa-help-center/"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func privacyViewTapped(){
        loadUrl         = "http://www.cintaa.net/privacy-policy/"
        webViewHeader   = "Privacy Policy"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func mouViewTapped(){
        loadUrl         = "\(constant.baseURL)mou.pdf"
        webViewHeader   = "MOU"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func rateUsViewTapped(){
        
        webViewHeader   = "Rate Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func instructionTapped(){
        
        loadUrl         = "\(constant.baseURL)instructions.pdf"
        webViewHeader   = "Instruction"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func contactUs(){
        
        loadUrl         = "http://www.cintaa.net/contact-us/"
        webViewHeader   = "Contact Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func profileTapped(){
        uiFun.navigateToScreem(identifierName: "ProfileViewController", fromController: self)
    }
    
    func rateApp() {
        guard let url = URL(string : "https://itunes.apple.com/us/app/cintaa/id1288952015?mt=8") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    // MARK:- Tour Functions
    func showTour1(){
    
        showcase.setupShowcaseForLocation(tableView.frame)
        showcase.titleLabel.text    = "Completed Shoot"
        showcase.detailsLabel.text  = "Here is list of completed shoot due today's date. You can retrieve whole details of your previous shoot by just clicking on the item"
        showcase.show()
    }
    
    func showTour2(){
    
        isShowingTour2              = false
        showcase.setupShowcaseForLocation(segementControl.frame)
        showcase.titleLabel.text    = "Completed Shoot"
        showcase.detailsLabel.text  = "You can filter completed shoots as date wise, production house wise and as Film/Serial Wise from here"
        showcase.show()
    }
    
    func showTour3(){
    
        isShowingTour3              = false
        showcase.setupShowcaseForTableView(tableView, withIndexOfItem: 2, andSectionOfItem: 0)
        showcase.titleLabel.text    = "Completed Shoot"
        showcase.detailsLabel.text  = "Shoot item can be view like this"
        showcase.show()
    }
    
}
