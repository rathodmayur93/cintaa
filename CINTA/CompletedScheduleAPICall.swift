//
//  CompletedScheduleAPICall.swift
//  CINTA
//
//  Created by Mayur on 16/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CompletedScheduleAPICall: NSObject {

    
    func makeAPICall(fromViewController : CompleteShootViewController, member_id : String){
        
        let url                         = "\(constant.baseURL)completedscheduleentry.php"
        let params : [String:AnyObject] = ["member_id"    : member_id.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        CleverTap.sharedInstance().recordEvent("Completed Shoot API Call URL \(url)")
        CleverTap.sharedInstance().recordEvent("Completed Shoot API Call Params", withProps: params)

        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.completedShootAPIResponse(response: jsonResponse)
                print("Completed Shoot Response \(jsonResponse)")
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
