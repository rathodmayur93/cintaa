//
//  SetNewPasswordViewController.swift
//  CINTA
//
//  Created by Mayur on 22/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SwiftyJSON

class SetNewPasswordViewController: UIViewController {

    @IBOutlet var informationLabel      : UILabel!
    
    @IBOutlet var newPasswordTF         : UITextField!
    @IBOutlet var confirmNewPasswordTF  : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        informationLabel.text = "Please Set New Password For User Name: \(uiFun.readData(key: "member_id"))"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Button Actions
    
    @IBAction func saveAction(_ sender: Any) {
        
        if(newPasswordTF.text! == confirmNewPasswordTF.text!){
            SetNewPasswordAPI()
        }else{
            uiFun.showAlert(title: "Alert", message: "Password Mismatch. Please Type Again", logMessage: "Password Mismatch", fromController: self)
        }
    }
    
    
    // MARK:- API Call
    func SetNewPasswordAPI(){
        uiFun.showIndicatorLoader()
        let changePassAPI   = ChangePasswordAPICall()
        changePassAPI.makeAPICall(fromViewController: self, memberId: memberId, password: newPasswordTF.text!)
    }
    
    // MARK:- API Response
    func changePasswordAPIResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        processChangePasswordResponse(response: response)
    }
    
    //MARK:- Process Responses
    func processChangePasswordResponse(response : JSON){
        
        let status = String(describing : response["success"])
        if(status == "1"){
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
        }else{
            uiFun.showAlert(title: "Alert", message: "Failed to set new password. Please Try Again", logMessage: "Change Password Failed", fromController: self)
        }
    }
}
