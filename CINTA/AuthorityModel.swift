//
//  AuthorityModel.swift
//  CINTA
//
//  Created by Mayur on 07/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class AuthorityModel {
    
    var entry_id            = ""
    var authority_name      = ""
    var authority_contact   = ""
    var authority_sign      = ""
    var authority_email     = ""

}
