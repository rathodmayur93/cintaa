//
//  SplashViewController.swift
//  CINTA
//
//  Created by Mayur on 23/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

var showLoaderFirstTime : Bool!
class SplashViewController: UIViewController {

    @IBOutlet var cintaLogo     : UIImageView!
    @IBOutlet var welcomeLabel  : UILabel!
    var timer                   = Timer()
    var time                    = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(SplashViewController.goToScreen), userInfo: nil, repeats: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- Animation
    func imageFadeAnimation(){
        
        UIView.transition(with: self.cintaLogo, duration: 1.0, options: .transitionCurlDown, animations: {
            self.cintaLogo.image = UIImage(named: "app_icon")
        }, completion: nil)
    }
    
    
    //  MARK:- Common Functions
    func goToScreen(){
        DispatchQueue.main.async {
            self.time += 1
            if(self.time == 3){
                uiFun.hideIndicatorLoader()
                self.timer.invalidate()
                let member_id   = uiFun.readData(key: "member_id")
                
                if(member_id != ""){
                    showLoaderFirstTime = false
                    uiFun.navigateToScreem(identifierName: "DashboardViewController", fromController: self)
                }else{
                    showLoaderFirstTime = true
                    uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
                }
            }
        }
    }
}
