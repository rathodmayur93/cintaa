//
//  NewDiaryEntryViewController.swift
//  CINTA
//
//  Created by Mayur on 12/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import SearchTextField
import SwiftyJSON
import iShowcase

class NewDiaryEntryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, iShowcaseDelegate, UITextViewDelegate {
    
    // MARK:- SearchTextField
    @IBOutlet var dateTF                : SearchTextField!
    @IBOutlet var televisionTF          : SearchTextField!
    @IBOutlet var productionHouseTF     : SearchTextField!
    @IBOutlet var producerName          : SearchTextField!
    @IBOutlet var LocationTF            : SearchTextField!
    @IBOutlet var addressOfShootTF      : SearchTextField!
    @IBOutlet var rolePlayTF            : SearchTextField!
    @IBOutlet var shiftTiming           : SearchTextField!
    @IBOutlet var callTimeTF            : SearchTextField!
    @IBOutlet var RupeeTF               : SearchTextField!
    @IBOutlet var transactionAfterTF    : SearchTextField!
    @IBOutlet var rateType              : UITextField!
    
    // MARK:- Buttons
    @IBOutlet var saveButton            : UIButton!
    @IBOutlet var inTimeGetBT           : UIButton!
    @IBOutlet var inTimeSubmitBT        : UIButton!
    @IBOutlet var packupTimeGetBT       : UIButton!
    @IBOutlet var packupTimeSubmitBT    : UIButton!
    
    
    // MARK:- TextField
    @IBOutlet var inTime                : UITextField!
    @IBOutlet var packUpTime            : UITextField!
    @IBOutlet var remarks               : UITextView!
    @IBOutlet var authorityName         : SearchTextField!
    @IBOutlet var authorityContact      : SearchTextField!
    @IBOutlet var authoritySign         : SearchTextField!
    @IBOutlet var productionEmail       : SearchTextField!
    @IBOutlet var detailView            : UIView!
    @IBOutlet var conveynceCharges      : SearchTextField!
   
    // MARK:- Views
    @IBOutlet var scrollView            : UIScrollView!
    @IBOutlet var containerView         : UIView!
    @IBOutlet var signLabel             : UILabel!
    @IBOutlet var singView              : UIImageView!
    @IBOutlet var signMainView          : UIView!
    @IBOutlet var mainViewOfScroll      : UIView!
    
    @IBOutlet var dateView              : UIView!
    @IBOutlet var filmNameView          : UIView!
    @IBOutlet var houseView             : UIView!
    @IBOutlet var producerNameView      : UIView!
    @IBOutlet var locationMapView       : UIView!
    @IBOutlet var addressView           : UIView!
    @IBOutlet var playedCharacterView   : UIView!
    @IBOutlet var shiftTimeView         : UIView!
    @IBOutlet var callTime              : UIView!
    @IBOutlet var rateView              : UIView!
    @IBOutlet var dueDaysView           : UIView!
    @IBOutlet var container             : UIView!
    @IBOutlet var conveyaneCharge       : UIView!
    @IBOutlet var authorityOnlyView     : UIView!
    
    // MARK:- Variables
    var tvSerialList                    = [String]()
    var productionHouseList             = [String]()
    var producerList                    = [String]()
    var calendarEventList               = [CalendarEvent]()
    var shiftTimingList                 = [String]()
    var rateTypeList                    = [String]()
    
    // MARK:- Authority Lists
    var authName                        = [String]()
    var authContact                     = [String]()
    var prodHouseDetail                 = [String]()
    
    var diaryEntryDate                  = ""
    var entryId                         = ""
    var selectedIndex                   = 0
    var lat                             = ""
    var lng                             = ""
    var currentTimeStatus               = "AM"
    
    let datePickerView  :UIDatePicker   = UIDatePicker()
    let customPickerView                = UIPickerView()
    
    var showAuthorityView               = true
    
    var histroryRecords                 = [CalendarEvent]()
    var showcase                        : iShowcase!
    
    // MARK:- Segments
    @IBOutlet var inTimeType            : UISegmentedControl!
    @IBOutlet var packupTimeType        : UISegmentedControl!
    

    // MARK:- Boolean Variable For Tour
    var isShowDate                      = true
    var isNameOfFilm                    = true
    var isProductionHouse               = true
    var isProducerName                  = true
    var isLocationShoot                 = true
    var isAddressShoot                  = true
    var isNameOfCharacter               = true
    var isShifTime                      = true
    var isCallTime                      = true
    var isRate                          = true
    var isDueDays                       = true
    
    var moveView : CGFloat              = 40.0
    var moveDetailView : CGFloat        = 48.0
    
    var strBase64                       = ""
    
    
    fileprivate lazy var dateFormatter3: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    // MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUI()
        tabBarIndex         = 0
        NotificationCenter.default.addObserver(self, selector: #selector(hideSignturePad), name: NSNotification.Name(rawValue: "callForAlert3"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideContainerView), name: NSNotification.Name(rawValue: "callForAlert4"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateLocation), name:NSNotification.Name(rawValue: "locationUpdate"), object: nil)
        
        showcase.delegate   = self
        remarks.delegate    = self
        
        // Fetching and showing data
        LocationTF.text = userSelectedLocation
        
        findOutListValues()
        fetchingMultiMovieList()
        
        CleverTap.sharedInstance().recordEvent("Diary Entry Screen")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        
        if(showAuthorityView){
            
            scrollView.contentSize  = CGSize(width: self.view.frame.width, height: 700)        // change it to 700
        }else{
            scrollView.contentSize  = CGSize(width: self.view.frame.width, height: 1370)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // Fetch All Calendar Event API Call
        if(tabBarIndex != 2){
           // AllCalendarEventAPICall()
        }
        //uiFun.showIndicatorLoader()
        //hidePackUpView()
        
        findOutListValues()
        fetchingMultiMovieList()
        
        DispatchQueue.main.async {
            print("Scrolling to top")
            let point = CGPoint(x: 0, y: 0) // 200 or any value you like.
            self.scrollView.contentOffset = point

        }
    }
    
    // MARK:- PickerView Delegate Methods
    public func textFieldDidBeginEditing(_ textField: UITextField){
        //remarks.text    = ""
    }
    
    @available(iOS 2.0, *)
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if(shiftTiming.isFirstResponder){
            return shiftTimingList.count
        }else{
            return rateTypeList.count
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        if(shiftTiming.isFirstResponder){
            return shiftTimingList[row]
        }else{
            return rateTypeList[row]
        }
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if(shiftTiming.isFirstResponder){
            shiftTiming.text          = shiftTimingList[row]
        }else{
            rateType.text             = rateTypeList[row]
        }
    }
    
    //MARK:- Showcase Delegate Method
    func iShowcaseDismissed(_ showcase: iShowcase) {
        
        if(isNameOfFilm){
            showTour2()
        }else if (isProductionHouse){
            showTour3()
        }else if (isProducerName){
            showTour4()
        }else if (isLocationShoot){
            showTour5()
        }else if (isAddressShoot){
            showTour6()
        }else if (isNameOfCharacter){
            showTour7()
        }else if (isShifTime){
            showTour8()
        }else if (isCallTime){
            showTour9()
        }else if (isRate){
            showTour10()
        }else if(isDueDays){
            showTour11()
        }
    }
    
    // MARK:- UI Functions
    func setUI(){
        
        showcase                = iShowcase()
        // Fetch Values From Database
        histroryRecords         = HistoryRecordDB.shared.fetchBookingItems()
        
        showAuthorityView       = true
        dateTF.isEnabled        = false
        containerView.isHidden  = true
        detailView.isHidden     = showAuthorityView
        container.isHidden      = true
        
        
        // Delegate
        customPickerView.delegate   = self
        shiftTiming.inputView       = customPickerView
        rateType.inputView          = customPickerView
        
        //Shift Timing API Call
        shiftTimeAPICall()
        
        let currentDate     = getCurrentDate()
        dateTF.text         = currentDate
        
        remarks.text        = "Remark (If Any)"
        
        // rate type list
        rateTypeList.removeAll()
        rateTypeList.append("Per Day")
        rateTypeList.append("Per Project")
        
        // MARK:- Signature View Tap Gesture
        let tapGesture                                = UITapGestureRecognizer(target: self, action: #selector(signatureViewTap))
        tapGesture.numberOfTapsRequired               = 1
        signMainView.isUserInteractionEnabled = true
        signMainView.addGestureRecognizer(tapGesture)
        
        // MARK: Remark Tap Gesture
//        let tapGestureRemark                           = UITapGestureRecognizer(target: self, action: #selector(remarkTapAction))
//        tapGestureRemark.numberOfTapsRequired               = 1
//        remarks.isUserInteractionEnabled = true
//        remarks.addGestureRecognizer(tapGestureRemark)
        
//        datePickerView.addTarget(self, action: #selector(datePickerValueChangedInTime(sender:)), for: UIControlEvents.valueChanged)
    
        remarks.toolbarPlaceholder      = "Remarks"
        remarks.layer.borderColor       = UIColor.black.cgColor
        remarks.layer.borderWidth       = 1.0
        
        if(userSelectedLocation != ""){
            LocationTF.text = userSelectedLocation
        }
        
        conveynceCharges.isEnabled  = false
        
    }
    
    
    // MARK:- Button Actions
    @IBAction func submitAction(_ sender: Any) {
        MakeDiryEntry()
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        tabBarIndex =   0
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    
    @IBAction func getTimeAction(_ sender: Any) {
        inTime.text             = getCurrentTime()
        showAuthorityView       = false
        //scrollView.contentSize  = CGSize(width: self.view.frame.width, height: 1370)
    }
    
    @IBAction func getPackUpTime(_ sender: Any) {
        if(!checkPackupTime()){
            uiFun.showAlert(title: "", message: "Pack Up Can't Be Earlier Than In Time", logMessage: "Packup time issue", fromController: self)
        }
        packUpTime.text     = getCurrentTime()
    }
    
    @IBAction func submitTimeAction(_ sender: Any) {
        
        if(inTime.text != ""){
            callInTimeAPICall()
            
        }else{
            uiFun.showAlert(title: "Error", message: "Please Click Get Time button", logMessage: "Diary Entry Error", fromController: self)
        }
    }
    
    @IBAction func packUpTimeAction(_ sender: Any) {
        
        if(packUpTime.text != ""){
            if(checkPackupTime()){
                packUpTimeAPICall()
            }
        }else{
            uiFun.showAlert(title: "Error", message: "Please Click Get Time button", logMessage: "Diary Entry Error", fromController: self)
        }
    }
    
    
    @IBAction func submitSecondFormAction(_ sender: Any) {
        encodeImageData()
        if(validatingAuthorityDetail()){
            uiFun.showIndicatorLoader()
            MakeAuthorityEntries()
        }
    }
    
    @IBAction func locationTextFieldAction(_ sender: Any) {
        
        navigateToSelectLocation()
        /*
        if(uiFun.isConnectedToNetwork()){
            
        }else{
            uiFun.showAlert(title: "Error", message: "No Internet Connection", logMessage: "no Internet", fromController: self)
        }
         */
    }
    
    @IBAction func callTimeTFAction(_ sender: UITextField) {
        
        
        datePickerView.datePickerMode   = UIDatePickerMode.time
        sender.inputView                = datePickerView
        callTimeTF.text                 = "\(getCurrentTime()) \(currentTimeStatus)"
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func shiftTimeTFAction(_ sender: UITextField) {
        shiftTiming.filterStrings(shiftTimingList)
        
    }
    
    @IBAction func tourButtonAction(_ sender: Any) {
        
        isShowDate                      = true
        isNameOfFilm                    = true
        isProductionHouse               = true
        isProducerName                  = true
        isLocationShoot                 = true
        isAddressShoot                  = true
        isNameOfCharacter               = true
        isShifTime                      = true
        isCallTime                      = true
        isRate                          = true
        isDueDays                       = true
        
        //showTour1()
        openActionSheet()
    }
    
    @IBAction func intTimTFAction2(_ sender: UITextField) {
        
        showAuthorityView               = false
        datePickerView.datePickerMode   = UIDatePickerMode.time
        sender.inputView                = datePickerView
        inTime.text                     = getCurrentTime()
        datePickerView.addTarget(self, action: #selector(datePickerValueChangedInTime(sender:)), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func packUpTimeTFAction(_ sender: UITextField) {
        
        showAuthorityView               = false
        datePickerView.datePickerMode   = UIDatePickerMode.time
        sender.inputView                = datePickerView
        packUpTime.text                 = getCurrentTime()
        datePickerView.addTarget(self, action: #selector(datePickerValueChangedPackUpTime(sender:)), for: UIControlEvents.valueChanged)
    }
    
    
    
    
    // MARK:- Validations
    
    func validateDiaryEntry() -> Bool{
    
        if(televisionTF.text == ""){
            showErrorAlert(text: "Please Enter Film/TV Serial Name")
            return false
        }else if(productionHouseTF.text == ""){
            showErrorAlert(text: "Please Enter Production House Name")
            return false
        }else if(addressOfShootTF.text == ""){
            showErrorAlert(text: "Please Enter Address Of Shoot")
            return false
        }else if(rolePlayTF.text == ""){
            showErrorAlert(text: "Please Enter Character To Play")
            return false
        }else if(shiftTiming.text == ""){
            showErrorAlert(text: "Please Select Shift Timing")
            return false
        }else if(callTimeTF.text == ""){
            showErrorAlert(text: "Please Select Call Timing")
            return false
        }else if(transactionAfterTF.text == ""){
            showErrorAlert(text: "Please Enter Payment After How Many Days")
            return false
        }else{
            return true
        }
    }
    
    func validatingAuthorityDetail() -> Bool{
        
        if(inTime.text == ""){
            showErrorAlert(text: "Please Enter In Time")
            return false
        }else if(packUpTime.text == ""){
            showErrorAlert(text: "Please Enter Packup Time")
            return false
        }else if(authorityName.text == ""){
            showErrorAlert(text: "Please Enter Authority Name")
            return false
        }else if(authorityContact.text == ""){
            showErrorAlert(text: "Please Enter Authority Contact")
            return false
        }else if(productionEmail.text == ""){
            showErrorAlert(text: "Please Enter Production Mail")
            return false
        }else if(strBase64 == ""){
            showErrorAlert(text: "Please Take Signature Of Authority")
            return false
        }else{
            return true
        }
    }
    
    func showErrorAlert(text : String){
        
        uiFun.showAlert(title: "Error", message: text, logMessage: "Validation Error", fromController: self)
    }
    
    // MARK:- API Calls
    func diaryEntryAPICall(){
    
        uiFun.showIndicatorLoader()
        
        if(entryId == ""){
        
            let timestamp   = NSDate().timeIntervalSince1970
            let date        = NSDate(timeIntervalSince1970: timestamp)
            
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            print("Date \(dateString)")
            
            entryId = "\(uiFun.readData(key: "member_id"))_\(dateString)"
        }
        
        let diaryApiCall    = InsertDiaryEntryAPI()
        diaryApiCall.makeAPICall(fromViewController: self, entry_id: entryId, member_id: uiFun.readData(key: "member_id"), shootDate: dateTF.text!, character: rolePlayTF.text!, location: addressOfShootTF.text!, serialName: televisionTF.text!, shiftTime: shiftTiming.text!, callTime: callTimeTF.text!, rateType: rateType.text!, rate: RupeeTF.text!, dueDays: transactionAfterTF.text!, remark: remarks.text, lati: "", lng: "", mapLocation: LocationTF.text!, productionHouseName: productionHouseTF.text!, producerName: producerName.text!)
    }
    
    func callInTimeAPICall(){
        
        var timeType    = ""
//        if(inTimeType.selectedSegmentIndex == 0){
//            timeType = "AM"
//        }else{
//            timeType = "PM"
//        }
        HistoryRecordDB.shared.updateBookingEntryForInTime(withID: entryId, inTime: inTime.text!)
        let insertCallTime  = CallInTimeAPI()
        insertCallTime.makeAPICall(fromViewController: self, entry_id: entryId, callin_time: "\(inTime.text!) \(timeType)", callin_location: LocationTF.text!, callin_lat: lat, callin_lng: lng)
        uiFun.showAlert(title: "Notification", message: "In time recorded successfully", logMessage: "In time success", fromController: self)
    }
    
    func packUpTimeAPICall(){
        
        var timeType    = ""
        if(packupTimeType.selectedSegmentIndex == 0){
            timeType = "AM"
        }else{
            timeType = "PM"
        }
        
        HistoryRecordDB.shared.updateBookingEntryForPackupTime(withID: entryId, packupTime: packUpTime.text!)
        let packUpTimeAPI   = PackupTimeAPI()
        packUpTimeAPI.makeAPICall(fromViewController: self, entry_id: entryId, callin_time: "\(packUpTime.text!) \(timeType)", callin_location: LocationTF.text!, callin_lat: lat, callin_lng: lng)
        uiFun.showAlert(title: "Notification", message: "Pack up time recorded successfully", logMessage: "Pack up time success", fromController: self)
    }
    
    func authorityAPICall(){
        
        let authorityAPI    = AuthorityAPICall2()
        authorityAPI.makeAPICall(fromViewController: self, entry_id: entryId, encoded_string: encodeImageData(), img_name: createImageName(), authority_sign_date: "\(getCurrentDate()), \(getCurrentTime())", authority_nm: authorityName.text!, authority_number: authorityContact.text!, authority_email:productionEmail.text!, productionHouse: productionHouseTF.text!, producerName: producerName.text!, member_id: uiFun.readData(key: "member_id"), remark: remarks.text)
    }
    
    func shiftTimeAPICall(){
        let shiftTime   = ShiftTimeAPICalll()
        shiftTime.makeAPICall(fromViewController: self)
    }
    
    func AllCalendarEventAPICall(){
        uiFun.showIndicatorLoader()
        let calendarEvent   = FetchAllEventAPICall()
        calendarEvent.makeAPICall2(fromViewController: self, member_id: uiFun.readData(key: "member_id"))
    }
    
    // MARK:- API Responses
    func diaryEntryResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        hidePackUpView()
    }
    
    func callInTimeResponse(response : JSON){
        inTimeGetBT.isHidden    = true
        inTimeSubmitBT.isHidden = true
        showPackUpTimeView()
        inTime.isEnabled        = false
        inTimeType.isEnabled    = false
        //hideIntimeView()
        
    }
    
    func packUpTimeResponse(response : JSON){
        packupTimeGetBT.isHidden    = true
        packupTimeSubmitBT.isHidden = true
        packUpTime.isEnabled        = false
        packupTimeType.isEnabled    = false
    }
    
    func authorityAPIResponse(response : JSON){
        HistoryRecordDB.shared.updateEventStatus(withID: entryId)
        uiFun.hideIndicatorLoader()
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
    }
    
    func shiftTimeAPIResponse(response : JSON){
        shiftTimingList.removeAll()
        let data        = response["shift_time"]
        
        for i in 0..<data.count{
            
            let startTime   = String(describing: data.arrayValue[i]["shift_Start"])
            let endTime     = String(describing: data.arrayValue[i]["shift_end"])
            shiftTimingList.append("\(startTime)")
        }
        customPickerView.reloadAllComponents()
//        shiftTiming.text    = "7"
//        shiftTiming.filterStrings(shiftTimingList)
//        HandlerOfSelectDropDown2(textField: shiftTiming)
    }
    
    func calendarEventResponse(response : JSON){
        uiFun.hideIndicatorLoader()
        processCalendarResponse(response: response)
    }
    
    
    // MARK:- Process Response
    
    func processCalendarResponse(response : JSON){
        
        calendarEventList.removeAll()
        let calendarData    = response["sht_date"]
        
        for i in 0..<calendarData.count{
        
            let calendarEvents  = CalendarEvent()
            
            calendarEvents.entry_id             = String(describing: calendarData.arrayValue[i]["entry_id"])
            calendarEvents.film_tv_name         = String(describing: calendarData.arrayValue[i]["film_tv_name"])
            calendarEvents.house_name           = String(describing: calendarData.arrayValue[i]["house_name"])
            calendarEvents.producer_name        = String(describing: calendarData.arrayValue[i]["producer_name"])
            calendarEvents.shoot_date           = String(describing: calendarData.arrayValue[i]["shoot_date"])
            
            calendarEvents.schedule_status      = String(describing: calendarData.arrayValue[i]["schedule_status"])
            calendarEvents.shift_time           = String(describing: calendarData.arrayValue[i]["shift_time"])
            calendarEvents.call_time            = String(describing: calendarData.arrayValue[i]["call_time"])
            calendarEvents.shoot_map_location   = String(describing: calendarData.arrayValue[i]["shoot_map_location"])
            calendarEvents.lat                  = String(describing: calendarData.arrayValue[i]["lat"])
            
            calendarEvents.lng                  = String(describing: calendarData.arrayValue[i]["lng"])
            calendarEvents.played_character     = String(describing: calendarData.arrayValue[i]["played_character"])
            calendarEvents.rate                 = String(describing: calendarData.arrayValue[i]["rate"])
            calendarEvents.due_days             = String(describing: calendarData.arrayValue[i]["due_days"])
            calendarEvents.remark               = String(describing: calendarData.arrayValue[i]["remark"])
            
            calendarEvents.rate_type            = String(describing: calendarData.arrayValue[i]["rate_type"])
            calendarEvents.callin_time          = String(describing: calendarData.arrayValue[i]["callin_time"])
            calendarEvents.packup_time          = String(describing: calendarData.arrayValue[i]["packup_time"])
            calendarEvents.shoot_location       = String(describing: calendarData.arrayValue[i]["shoot_location"])
            
            calendarEventList.append(calendarEvents)
        }
    }
    
    
    
    // MARK:- Time Picker Function
    
   
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateStyle     = DateFormatter.Style.none
        dateFormatter.timeStyle     = DateFormatter.Style.short
        callTimeTF.text             = dateFormatter.string(from: sender.date)
        
    }
    
    func datePickerValueChangedInTime(sender:UIDatePicker) {
        
        let dateFormatter           = DateFormatter()
        dateFormatter.dateStyle     = DateFormatter.Style.none
        dateFormatter.timeStyle     = DateFormatter.Style.short
        
        let selectedTime             = dateFormatter.string(from: sender.date)
        let onlyTime                 = selectedTime.components(separatedBy: " ")
        
        if(inTime.isFirstResponder){
            if(selectedTime.contains("AM")){
                inTimeType.selectedSegmentIndex = 0
                inTime.text                     = onlyTime[0]
            }else{
                inTimeType.selectedSegmentIndex = 1
                inTime.text                     = onlyTime[0]
            }
        }
    }
    
    func datePickerValueChangedPackUpTime(sender:UIDatePicker) {
        
        let dateFormatter1           = DateFormatter()
        dateFormatter1.dateStyle     = DateFormatter.Style.none
        dateFormatter1.timeStyle     = DateFormatter.Style.short
        //packUpTime.text              = dateFormatter1.string(from: sender.date)
        
        let selectedTime             = dateFormatter1.string(from: sender.date)
        let onlyTime                 = selectedTime.components(separatedBy: " ")
        packUpTime.text              = onlyTime[0]
        
        if(selectedTime.contains("AM")){
            packupTimeType.selectedSegmentIndex = 0
        }else{
            packupTimeType.selectedSegmentIndex = 1
        }
    }
    
    // MARK:- Common Functions
    /**************** Common Functions *******************/
 
    func getCurrentDate() -> String{
    
        let date        = Date()
        let formatter   = DateFormatter()
        
        formatter.dateFormat    = "dd/MM/yyyy"
        let result              = formatter.string(from: date)
        
        return result
    }
    
    func getCurrentTime() -> String{
    
        let date         = Date()
        let calendar     = Calendar.current
        var hour         = calendar.component(.hour, from: date)
        var minutes      = calendar.component(.minute, from: date)
        var minuteString = ""
        var hourString   = ""
        
        print("Hour is \(hour)")
        print("Minute is \(minutes)")
        
        if(hour > 12){
            inTimeType.selectedSegmentIndex     = 1
            packupTimeType.selectedSegmentIndex = 1
            hour                = hour - 12
            hourString          = "\(hour)"
            currentTimeStatus   = "PM"
            
        }else if(hour == 0){
            hour = 12
            inTimeType.selectedSegmentIndex      = 0
            packupTimeType.selectedSegmentIndex  = 0
            hourString  = "12"
        }else if(hour == 12){
            
            hour = 12
            inTimeType.selectedSegmentIndex      = 1
            packupTimeType.selectedSegmentIndex  = 1
            hourString  = "12"
        }else{
            inTimeType.selectedSegmentIndex     = 0
            packupTimeType.selectedSegmentIndex = 0
            hourString  = "\(hour)"
            
        }
        
        if (minutes < 10){
            minuteString = "0\(minutes)"
        }else{
            minuteString = "\(minutes)"
        }
        
        if hour < 10 {
            hourString    = "0\(hour)"
        }
        
        return "\(hourString):\(minuteString)"
    }
    
    func fillAllDetail(index : Int){
    
        entryId                 = multipleMovieList[index].entry_id
        dateTF.text             = multipleMovieList[index].shoot_date
        let played_character    = multipleMovieList[index].played_character
        let rate                = multipleMovieList[index].rate
        let shoot_map_location  = multipleMovieList[index].shoot_map_location
        let due_days            = multipleMovieList[index].due_days
        let producer_name       = multipleMovieList[index].producer_name
        
        let packup_time         = multipleMovieList[index].packup_time
        let call_time           = multipleMovieList[index].call_time
        let callin_time         = multipleMovieList[index].callin_time
        let shoot_location      = multipleMovieList[index].shoot_location
        let lat                 = multipleMovieList[index].lat
        
        let shoot_date          = multipleMovieList[index].shoot_date
        let remark              = multipleMovieList[index].remark
        let entry_id            = multipleMovieList[index].entry_id
        let rate_type           = multipleMovieList[index].rate_type
        let film_tv_name        = multipleMovieList[index].film_tv_name
        
        let house_name          = multipleMovieList[index].house_name
        let shift_time          = multipleMovieList[index].shift_time
        let lng                 = multipleMovieList[index].lng
        let schedule_status     = multipleMovieList[index].schedule_status

        
        allDetailSetText(textField: televisionTF, text: film_tv_name)
        allDetailSetText(textField: productionHouseTF, text: house_name)
        allDetailSetText(textField: producerName, text: producer_name)
        allDetailSetText(textField: LocationTF, text: shoot_map_location)
        allDetailSetText(textField: addressOfShootTF, text: shoot_location)
        
        allDetailSetText(textField: rolePlayTF, text: played_character)
        allDetailSetText(textField: shiftTiming, text: shift_time)
        allDetailSetText(textField: callTimeTF, text: call_time)
        allDetailSetText(textField: RupeeTF, text: rate)
        allDetailSetText(textField: transactionAfterTF, text: due_days)
        
        allDetailSetText2(textField: inTime, text: callin_time)
        allDetailSetText2(textField: packUpTime, text: packup_time)
        
        //allDetailSetText2(textField: rateType, text: rate_type)
        allDetailSetText2(textField: inTime, text: callin_time)
        allDetailSetText2(textField: packUpTime, text: packup_time)
        
        print("Remark Value Is \(remark)")
        if(remark == "null" || remark.trimmingCharacters(in: .whitespaces) == ""){
            remarks.text    = "Remark"
        }else{
            remarks.text    = remark
        }
        
        if(rate_type == "null" || rate_type == "NA" || rate_type == ""){
            rateType.text   = "Per Day"
        }
        
        print("Calling Time \(callin_time) && Packup Time \(packup_time)")
        if(callin_time == "" || callin_time == "NA" || callin_time == "null"){
            inTimeGetBT.isHidden    = false
            inTimeSubmitBT.isHidden = false
            inTime.isEnabled        = true
            hidePackUpView()
            inTime.isEnabled        = true
            inTimeType.isEnabled    = true

        }else{
            
            inTimeGetBT.isHidden    = true
            inTimeSubmitBT.isHidden = true
            inTime.isEnabled        = false
            showPackUpTimeView()
            inTime.isEnabled        = false
            inTimeType.isEnabled    = false
            
            if(callin_time.contains("AM")){
                inTimeType.selectedSegmentIndex = 0
            }else{
                inTimeType.selectedSegmentIndex = 1
            }
        }
        
        if(packup_time == "" || packup_time == "NA" || packup_time == "null"){
            packUpTime.isEnabled        = true
            packupTimeType.isEnabled    = true
            
        }else{
            showPackUpTimeView()
            packupTimeGetBT.isHidden    = true
            packupTimeSubmitBT.isHidden = true
            packUpTime.isEnabled        = false
            packupTimeType.isEnabled    = false
            
            if(packup_time.contains("AM")){
                packupTimeType.selectedSegmentIndex = 0
            }else{
                packupTimeType.selectedSegmentIndex = 1
            }
        }
        
        print("rate is \(rate)")
        
        if(rate == "" || rate == "null" || rate == "NA"){
            showAuthorityView   = true
            detailView.isHidden = true
            saveButton.isHidden = false
            
        }else{
            showAuthorityView   = false
            detailView.isHidden = false
            hideSavebutton()
        }
    }
    
    func FillAuthorityData(){
        
        let authorityDetail = AuthorityDatabse.shared.fetchBookingItems()
        
        for i in 0..<authorityDetail.count{
        
            if(authorityDetail[i].entry_id == entryId){
                
                authorityName.text      = authorityDetail[i].authority_name
                authorityContact.text   = authorityDetail[i].authority_contact
                productionEmail.text    = authorityDetail[i].authority_email
                
                
                print("Image Data \(authorityDetail[0].authority_sign)")
                if let decodedData = Data(base64Encoded: authorityDetail[i].authority_sign, options: []) {
                   
                    let image       = UIImage(data: decodedData)
                    singView.image  = image!
                }
            }
        }
    }
    
    func allDetailSetText(textField : SearchTextField, text : String){
        
        if(text != "null"){
            textField.text  = text
        }else{
            textField.text  = ""
        }
    }
    
    func allDetailSetText2(textField : UITextField, text : String){
    
        if(text == "null" || text == "NA" || text == ""){
            textField.text  = ""
        }else{
            textField.text  = text
        }
    }
    
    func createImageName() -> String{
    
        let date        = Date()
        let formatter   = DateFormatter()
        
        formatter.dateFormat    = "ddMMyyyyHHmmssSSS"
        let result              = formatter.string(from: date)
        let imageName           = "\(uiFun.readData(key: "member_id")+result)"
        return  imageName
    }
    
    func signatureViewTap(){
        
        authorityName.resignFirstResponder()
        authorityContact.resignFirstResponder()
        
        containerView.isHidden            = false
        scrollView.isScrollEnabled        = false
    }
    
    func navigateToSelectLocation(){
        
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SelectLocationViewController") as! SelectLocationViewController
        
        self.addChildViewController(popvc)
        
        popvc.view.frame = self.view.frame
        
        self.view.addSubview(popvc.view)
        
        popvc.didMove(toParentViewController: self)
    }
    
    func hideSignturePad(){
        
        containerView.isHidden       = true
        scrollView.isScrollEnabled   = true
        signLabel.isHidden           = true
        singView.image               = signImage
    }
    
    func encodeImageData() -> String{
        
        let imageData             = UIImagePNGRepresentation(signImage!)
        strBase64                 = (imageData?.base64EncodedString())!            // Converting image into base64
        let base64Encoded         = strBase64.addingPercentEncodingForURLQueryValue()  // encoding base64
        
        let base64httpEncoding    = base64Encoded?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        print(base64Encoded!)
        return (base64Encoded!.replacingOccurrences(of: "+", with: "%2B"))
    }
    
    func findOutListValues(){
        
        let calendarEvents  = HistoryRecordDB.shared.fetchBookingItems()
        
        for i in 0..<calendarEvents.count{
            
            // Filter List Of Film/TVSerial
            tvSerialList.append(calendarEvents[i].film_tv_name)
            televisionTF.filterStrings(tvSerialList)
            HandlerOfSelectDropDown2(textField: televisionTF)
            
            // Filter List Of Production House
            productionHouseList.append(calendarEvents[i].house_name)
            productionHouseTF.filterStrings(productionHouseList)
            HandlerOfSelectDropDown2(textField: productionHouseTF)
            
            // Filter List Of Producer
            producerList.append(calendarEvents[i].producer_name)
            producerName.filterStrings(producerList)
            HandlerOfSelectDropDown2(textField: producerName)
        }
        
        let authorityDetails    = AuthorityDatabse.shared.fetchBookingItems()
        
        for i in 0..<authorityDetails.count{
        
            print("Authority Values \(authorityDetails[i].authority_name)")
            // Filter Authority Name Details
            authName.append(authorityDetails[i].authority_name)
            authorityName.filterStrings(authName)
            HandlerOfSelectDropDown2(textField: authorityName)
            
            authContact.append(authorityDetails[i].authority_contact)
            authorityContact.filterStrings(authContact)
            HandlerOfSelectDropDown2(textField: authorityContact)
            
            prodHouseDetail.append(authorityDetails[i].authority_email)
            productionEmail.filterStrings(prodHouseDetail)
            HandlerOfSelectDropDown2(textField: productionEmail)
            
        }
    }
    
    func HandlerOfSelectDropDown2(textField : SearchTextField){
        
            // Set theme - Default: light
            textField.theme = SearchTextFieldTheme.lightTheme()
            
            // Define a header - Default: nothing
            let header = UILabel(frame: CGRect(x: 0, y: 0, width: textField.frame.width, height: 30))
            header.backgroundColor              = UIColor.lightGray.withAlphaComponent(0.3)
            header.textAlignment                = .center
            header.font                         = UIFont.systemFont(ofSize: 14)
            header.text                         = "Pick your option"
            textField.resultsListHeader  = header
            
            
            // Modify current theme properties
            textField.theme.font             = UIFont.systemFont(ofSize: 12)
            textField.theme.bgColor          = UIColor.white
            textField.theme.borderColor      = UIColor.white
            textField.theme.separatorColor   = UIColor.white
            textField.theme.cellHeight       = 50
            textField.theme.placeholderColor = UIColor.lightGray
            
            // Max number of results - Default: No limit
            textField.maxNumberOfResults     = 15
            
            // Max results list height - Default: No limit
            textField.maxResultsListHeight   = 200
            
            // Set specific comparision options - Default: .caseInsensitive
            textField.comparisonOptions      = [.caseInsensitive]
            
            // You can force the results list to support RTL languages - Default: false
            textField.forceRightToLeft       = false
            
            // Customize highlight attributes - Default: Bold
            textField.highlightAttributes    = [NSBackgroundColorAttributeName: UIColor.yellow, NSFontAttributeName:UIFont.boldSystemFont(ofSize: 14)]
            
            // Handle item selection - Default behaviour: item title set to the text field
            textField.itemSelectionHandler = { filteredResults, itemPosition in
                // Just in case you need the item position
                let item = filteredResults[itemPosition]
                print("Item at position \(itemPosition): \(item.title)")
                
                // Do whatever you want with the picked item
                textField.text        = item.title
                if(self.televisionTF.isFirstResponder){
                    self.fillPartialDetails(selectedText: item.title)
                }else if(self.authorityName.isFirstResponder == true){
                    self.fillPartialDetails2(selectedText: item.title)
                }
            }
    }
    
    func fillPartialDetails(selectedText : String){
        
        if(televisionTF.isFirstResponder){
            
            let calendarEvents  = HistoryRecordDB.shared.fetchBookingItems()
            for i in 0..<calendarEvents.count{
                
                if(selectedText == calendarEvents[i].film_tv_name){
                    
                    televisionTF.text       = calendarEvents[i].film_tv_name
                    productionHouseTF.text  = calendarEvents[i].house_name
                    producerName.text       = calendarEvents[i].producer_name
                    callTimeTF.text         = calendarEvents[i].call_time
                }
            }
        }
    }
    
    // MARK: Authority Predictive Search Functions
    
    func HandlerOfSelectDropDown3(textField : SearchTextField){
        
        // Set theme - Default: light
        textField.theme = SearchTextFieldTheme.lightTheme()
        
        // Define a header - Default: nothing
        let header = UILabel(frame: CGRect(x: 0, y: 0, width: textField.frame.width, height: 30))
        header.backgroundColor              = UIColor.lightGray.withAlphaComponent(0.3)
        header.textAlignment                = .center
        header.font                         = UIFont.systemFont(ofSize: 14)
        header.text                         = "Pick your option"
        textField.resultsListHeader  = header
        
        
        // Modify current theme properties
        textField.theme.font             = UIFont.systemFont(ofSize: 12)
        textField.theme.bgColor          = UIColor.white
        textField.theme.borderColor      = UIColor.white
        textField.theme.separatorColor   = UIColor.white
        textField.theme.cellHeight       = 50
        textField.theme.placeholderColor = UIColor.lightGray
        
        // Max number of results - Default: No limit
        textField.maxNumberOfResults     = 15
        
        // Max results list height - Default: No limit
        textField.maxResultsListHeight   = 200
        
        // Set specific comparision options - Default: .caseInsensitive
        textField.comparisonOptions      = [.caseInsensitive]
        
        // You can force the results list to support RTL languages - Default: false
        textField.forceRightToLeft       = false
        
        // Customize highlight attributes - Default: Bold
        textField.highlightAttributes    = [NSBackgroundColorAttributeName: UIColor.yellow, NSFontAttributeName:UIFont.boldSystemFont(ofSize: 14)]
        
        // Handle item selection - Default behaviour: item title set to the text field
        textField.itemSelectionHandler = { filteredResults, itemPosition in
            // Just in case you need the item position
            let item = filteredResults[itemPosition]
            print("Item at position \(itemPosition): \(item.title)")
            
            // Do whatever you want with the picked item
            textField.text        = item.title
            if(self.televisionTF.isFirstResponder){
                self.fillPartialDetails2(selectedText: item.title)
            }else if(self.authorityName.isFirstResponder){
                self.fillPartialDetails2(selectedText: item.title)
            }
        }
    }
    
    func fillPartialDetails2(selectedText : String){
        
        if(authorityName.isFirstResponder){
            
            let authorityDetails  = AuthorityDatabse.shared.fetchBookingItems()
            for i in 0..<authorityDetails.count{
                
                if(selectedText == authorityDetails[i].authority_name){
                    
                    authorityName.text          = authorityDetails[i].authority_name
                    authorityContact.text       = authorityDetails[i].authority_contact
                    productionEmail.text        = authorityDetails[i].authority_email
                }
            }
        }
    }
    
    func hideContainerView(){
    
        container.isHidden = true
        fillAllDetail(index: diaryMovieIndex)
        FillAuthorityData()
    }
    
    func updateLocation(){
        
       LocationTF.text  = userSelectedLocation
    }
    
    func fetchingMultiMovieList(){
    
        findMultipleOccurence(date: getCurrentDate())
        var prop = [String : String]()
        
        //        for i in 0..<histroryRecords.count{
        //
        //            if (histroryRecords[i].shoot_date == currentDate){
        //                print("Diary Entry Present")
        //                diaryEntryDate          = currentDate
        //                fillAllDetail(index: i)
        //                selectedIndex           = i
        //                showAuthorityView       = false
        //                detailView.isHidden     = showAuthorityView
        //                entryId                 = histroryRecords[i].entry_id
        //                FillAuthorityData()
        //            }
        //        }
        
        let multipleEventsFromDB    = HistoryRecordDB.shared.fetchEventOfParticularDate(date: getCurrentDate())
        
        for i in 0..<multipleEventsFromDB.count{
            
            
            if(multipleEventsFromDB[i].schedule_status != "completed"){
                
                selectedIndex               = i
                
                televisionTF.text           = multipleEventsFromDB[i].film_tv_name
                productionHouseTF.text      = multipleEventsFromDB[i].house_name
                producerName.text           = multipleEventsFromDB[i].producer_name
                LocationTF.text             = multipleEventsFromDB[i].shoot_map_location
                addressOfShootTF.text       = multipleEventsFromDB[i].shoot_location
                rolePlayTF.text             = multipleEventsFromDB[i].played_character
                shiftTiming.text            = multipleEventsFromDB[i].shift_time
                callTimeTF.text             = multipleEventsFromDB[i].call_time
                RupeeTF.text                = multipleEventsFromDB[i].rate
                transactionAfterTF.text     = multipleEventsFromDB[i].due_days
                rateType.text               = multipleEventsFromDB[i].rate_type
                entryId                     = multipleEventsFromDB[i].entry_id
                
                lat                         = multipleEventsFromDB[i].lat
                lng                         = multipleEventsFromDB[i].lng
                
                prop[multipleEventsFromDB[i].shoot_date]    = multipleEventsFromDB[i].film_tv_name
                
                print("Rate Type Is \(rateType.text!)")
                fillAllDetail(index: 0)
                FillAuthorityData()
            }
        }
        CleverTap.sharedInstance().recordEvent("Diary Entry Multiple Movie List", withProps: prop)
    }
    
    func moveAllViews(){
    
        DispatchQueue.main.async {
            self.packUpTime.frame.origin.y           = self.packUpTime.frame.origin.y - self.moveView
            self.packupTimeType.frame.origin.y       = self.packupTimeType.frame.origin.y - self.moveView
            
            self.packupTimeGetBT.frame.origin.y      = self.packupTimeGetBT.frame.origin.y - self.moveView
            self.packupTimeSubmitBT.frame.origin.y   = self.packupTimeSubmitBT.frame.origin.y - self.moveView
            
            self.authorityOnlyView.frame.origin.y    = self.authorityOnlyView.frame.origin.y - self.moveView
            self.remarks.frame.origin.y              = self.remarks.frame.origin.y - self.moveView
            
            self.authorityName.frame.origin.y        = self.authorityName.frame.origin.y - self.moveView
            self.authorityContact.frame.origin.y     = self.authorityContact.frame.origin.y - self.moveView
            self.signMainView.frame.origin.y         = self.signMainView.frame.origin.y - self.moveView
            self.authorityContact.frame.origin.y     = self.authorityContact.frame.origin.y - self.moveView
            
            self.moveView = 0
        }
    }
    
    
    func hidePackUpView(){
    
        packUpTime.isHidden         = true
        packupTimeType.isHidden     = true
        packupTimeGetBT.isHidden    = true
        packupTimeSubmitBT.isHidden = true
    }
    
    func showPackUpTimeView(){
        
        packUpTime.isHidden         = false
        packupTimeType.isHidden     = false
        packupTimeGetBT.isHidden    = false
        packupTimeSubmitBT.isHidden = false
    }
    
    func moveAuthorityViews(){
    
        self.authorityOnlyView.frame.origin.y    = self.authorityOnlyView.frame.origin.y - 48
        self.remarks.frame.origin.y              = self.remarks.frame.origin.y - 48
        
        self.authorityName.frame.origin.y        = self.authorityName.frame.origin.y - 48
        self.authorityContact.frame.origin.y     = self.authorityContact.frame.origin.y - 48
        self.signMainView.frame.origin.y         = self.signMainView.frame.origin.y - 48
        self.authorityContact.frame.origin.y     = self.authorityContact.frame.origin.y - 48
        
    }
    
    func hideIntimeView(){
        
        inTime.isHidden         = true
        inTimeType.isHidden     = true
        inTimeGetBT.isHidden    = true
        inTimeSubmitBT.isHidden = true
    }
    
    func showInTimeView(){
        
        inTime.isHidden         = false
        inTimeType.isHidden     = false
        inTimeGetBT.isHidden    = false
        inTimeSubmitBT.isHidden = false
        
    }
    
    func remarkTapAction(){
        //remarks.text    = ""
    }
    
    func checkPackupTime() -> Bool{
    
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        
        let date1    = formatter.date(from: result)
        var date2   = formatter.date(from: dateTF.text!)
        
        if(date2! < date1!){
            return true
        }else if(date2 == date1){
            print("Date is same")
            let inTimeArray = inTime.text?.components(separatedBy: ":")
            let hour1       = inTimeArray?[0]
            let minutes1    = inTimeArray?[1]
            
            let packupTimeArray = getCurrentTime().components(separatedBy: ":")
            let hour2       = packupTimeArray[0]
            let minute2     = packupTimeArray[1]
            
            if(hour1! < hour2){
                return true
            }else if( hour1 == hour2){
                if(minutes1! < minute2){
                    return true
                }else{
                    return false
                }
            }else{
                return true
            }
        }else{
            return true
        }
        return true
    }
    
    
    // Movie List Container Functions
    
    // MARK:- (MultiEvent) Common Function
    func findMultipleOccurence(date : String){
        
        multipleMovieList.removeAll()
        
        let date1            = Date()
        let formatter        = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result           = formatter.string(from: date1)
        let yesterDate       = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterDay        = formatter.string(from: yesterDate!)
        
        var historyRecordsFromDB    = HistoryRecordDB.shared.fetchBookingItems()
        let multiEventListFromDB    = HistoryRecordDB.shared.fetchEventOfParticularDate(date: getCurrentDate())
        let previousDatEventFromDB  = HistoryRecordDB.shared.fetchEventOfParticularDate(date: yesterDay)
        
        
        for i in 0..<multiEventListFromDB.count{
            
            print("Movie List Values \(multiEventListFromDB[i].film_tv_name)")
            if(multiEventListFromDB[i].schedule_status != "completed"){
                let movieList   = MovieListParser()
                movieList.parseMovieListDB(index: i, date: getCurrentDate())
                print("movieList Date \(multipleMovieList[0].shoot_date)")
            }
        }
        
        for i in 0..<previousDatEventFromDB.count{
        
            if (previousDatEventFromDB[i].shoot_date == yesterDay && previousDatEventFromDB[i].packup_time == "" && previousDatEventFromDB[i].callin_time != ""){
                
                let movieList   = MovieListParser()
                movieList.parseMovieListDB(index: i, date: yesterDay)
                print("previous movieList Date \(multipleMovieList[0].shoot_date)")
            }
        }
        
        handlingMultiEvent()// if have mulitple event handle accordingly
    }
    
    func handlingMultiEvent(){
        
        switch multipleMovieList.count {
        case 0:
            container.isHidden = true
            break
        case 1:
            container.isHidden = true
        default:
            isMultiEvent    = true
            showMultiEventPopUp()
            break
        }
        
        if(userSelectedLocation != ""){
            container.isHidden = true
        }
    }
    
    func showMultiEventPopUp(){
        
        let childView        = self.childViewControllers.last as! MovieListPopUpViewController
        childView.tableView.reloadData()
        container.isHidden   = false
        uiFun.hideIndicatorLoader()
    }
    
    // MARK:- Diary Entry Functions
    func MakeDiryEntry(){
    
        if(validateDiaryEntry()){
            detailView.isHidden     = false
            scrollView.contentSize  = CGSize(width: self.view.frame.width, height: 1370)
            hideSavebutton()
            hidePackUpView()
            showPackUpTimeView()
            scrollView.setContentOffset(CGPoint(x: 0, y: 500), animated: true)
            
            if(uiFun.isConnectedToNetwork()){
                diaryEntryAPICall()
                DiaryEntryToDatabase(serviceCall: "1")
            }else{
                DiaryEntryToDatabase(serviceCall: "0.2")
            }
            
            var rate    = Int(RupeeTF.text!)
            if(rate! < 5000){
                conveyaneCharge.isHidden    = false
            }else{
                conveyaneCharge.isHidden    = true
                moveDetailView              = 64.0
            }
        }
    }
    
    func DiaryEntryToDatabase(serviceCall : String){
    
        histroryRecords         = HistoryRecordDB.shared.fetchBookingItems()
        let checkStatusOfEntry  = histroryRecords.contains { (CalendarEvent) -> Bool in
            if (CalendarEvent.entry_id == entryId){
                return true
            }else{
                return false
            }
        }
        
        if(checkStatusOfEntry){
            HistoryRecordDB.shared.updateBookingInDiaryEntry(withID: entryId, calendarEvents: CreateCalendarEventList(), serviceCall: serviceCall)
        }else{
            HistoryRecordDB.shared.insertBookingData(userInfoList: CreateCalendarEventList(), serviceCall: serviceCall)
        }
    }
    
    func CreateCalendarEventList() -> [CalendarEvent]{
    
        
        let timestamp   = NSDate().timeIntervalSince1970
        let date        = NSDate(timeIntervalSince1970: timestamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        var calendarEventList   = [CalendarEvent]()
        let calendarEvent       = CalendarEvent()
        
        calendarEvent.entry_id              = "\(uiFun.readData(key: "member_id"))_\(dateString)"
        calendarEvent.film_tv_name          = televisionTF.text!
        calendarEvent.house_name            = productionHouseTF.text!
        calendarEvent.producer_name         = producerName.text!
        calendarEvent.shoot_map_location    = LocationTF.text!
        calendarEvent.shoot_location        = addressOfShootTF.text!
        calendarEvent.played_character      = rolePlayTF.text!
        calendarEvent.shift_time            = shiftTiming.text!
        calendarEvent.call_time             = callTimeTF.text!
        calendarEvent.rate                  = RupeeTF.text!
        calendarEvent.rate_type             = rateType.text!
        calendarEvent.due_days              = transactionAfterTF.text!
        calendarEvent.shoot_date            = getCurrentDate()
        
        calendarEventList.append(calendarEvent)
        return calendarEventList
    }
    
    
    // MARK:- Authority Data Functions
    func MakeAuthorityEntries(){
    
        if(validateAuthData()){
            if(uiFun.isConnectedToNetwork()){
                authorityAPICall()
                AuthorityDatabaseEntries()
            }else{
                uiFun.hideIndicatorLoader()
                AuthorityDatabaseEntries()
            }
        }
    }
    
    func AuthorityDatabaseEntries(){
    
        let authorityDetails = AuthorityDatabse.shared.fetchBookingItems()
        print("Auhtority Detail Count \(authorityDetails.count)")
        
        if(authorityDetails.count == 0){
            AuthorityDatabse.shared.insertBookingData(userInfoList: CreateAuthorityData(), entry_id: entryId)
        }
        
        for i in 0..<authorityDetails.count{
            
            if(authorityDetails[i].entry_id == entryId){
                AuthorityDatabse.shared.updateBookingInDiaryEntry(withID: entryId, calendarEvents: CreateAuthorityData())
            }else{
                AuthorityDatabse.shared.insertBookingData(userInfoList: CreateAuthorityData(), entry_id: entryId)
            }
        }
    }
    
    func validateAuthData() -> Bool{
    
        if(authorityName.text == ""){
            showErrorAlert(text: "Please Enter Authority Name")
            return false
        }else if(authorityContact.text == ""){
            showErrorAlert(text: "Please Enter Authority Contact")
            return false
        }else{
            return true
        }
    }
    
    func CreateAuthorityData() -> [AuthorityModel]{
    
        var authorityDetailList     = [AuthorityModel]()
        
        let authData                = AuthorityModel()
        
        authData.authority_name     = authorityName.text!
        authData.authority_contact  = authorityContact.text!
        authData.authority_email    = productionEmail.text!
        authData.authority_sign     = encodeImageData()
        
        authorityDetailList.append(authData)
        return authorityDetailList
    }
    
    func hideSavebutton(){
    
        DispatchQueue.main.async {
            self.saveButton.isHidden           = true
            self.detailView.frame.origin.y     = self.detailView.frame.origin.y - self.moveDetailView
            self.moveDetailView                = 0
        }
        
    }
    
    // MARK:- Tour Functions
    
    func showTour1(){
        
            showcase.setupShowcaseForView(dateView)
            showcase.titleLabel.text = "Shooting Date"
            showcase.detailsLabel.text = "Current Date shows the shooting date"
            showcase.show()
    }
    
    func showTour2(){
        
        isNameOfFilm    = false
        showcase.setupShowcaseForView(filmNameView)
        showcase.titleLabel.text = "Film/Serial Name"
        showcase.detailsLabel.text = "Enter film name or tv serial name here"
        showcase.show()
    }
    
    func showTour3(){
        
        isProductionHouse = false
        showcase.setupShowcaseForView(houseView)
        showcase.titleLabel.text = "Production House"
        showcase.detailsLabel.text = "Here is the production house name"
        showcase.show()
    }
    
    func showTour4(){
        
        isProducerName  = false
        showcase.setupShowcaseForView(producerNameView)
        showcase.titleLabel.text = "Producer Name"
        showcase.detailsLabel.text = "Enter Producer Name Here"
        showcase.show()
    }
    
    func showTour5(){
    
        isLocationShoot = false
        showcase.setupShowcaseForView(locationMapView)
        showcase.titleLabel.text = "Location Of Shoot"
        showcase.detailsLabel.text = "Select the location of the shoot"
        showcase.show()
    }
    
    func showTour6(){
        
        isAddressShoot  = false
        showcase.setupShowcaseForView(addressView)
        showcase.titleLabel.text = "Address Of Shoot"
        showcase.detailsLabel.text = "Enter Address Of Shoot"
        showcase.show()
    }
    
    func showTour7(){
        
        isNameOfCharacter   = false
        showcase.setupShowcaseForView(playedCharacterView)
        showcase.titleLabel.text = "Played Character"
        showcase.detailsLabel.text = "Enter Character Name you are going to play"
        showcase.show()
    }
    
    func showTour8(){
        
        isShifTime      = false
        showcase.setupShowcaseForView(shiftTimeView)
        showcase.titleLabel.text = "Shift Timing"
        showcase.detailsLabel.text = "Select your shift from the drop down list by clicking here"
        showcase.show()
    }
    
    func showTour9(){
        
        isCallTime      = false
        showcase.setupShowcaseForView(callTime)
        showcase.titleLabel.text = "Call Time"
        showcase.detailsLabel.text = "Set Call Time"
        showcase.show()
    }
    
    func showTour10(){
        
        isRate       = false
        showcase.setupShowcaseForView(rateView)
        showcase.titleLabel.text = "Rate"
        showcase.detailsLabel.text = "Enter your rate as per day or single serial."
        showcase.show()
    }
    
    func showTour11(){
        
        isDueDays       = false
        showcase.setupShowcaseForView(dueDaysView)
        showcase.titleLabel.text = "Due Days"
        showcase.detailsLabel.text = "Enter payment due after days."
        showcase.show()
    }
    
    // MARK:- Action Sheet
    func openActionSheet(){
        
        let dotMenu      = UIAlertController(title: nil, message: "Select Any Option", preferredStyle: .actionSheet)
        
        let instructionAction       = UIAlertAction(title: "Instructions", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.instructionTapped()
        })
        
        let helpAction       = UIAlertAction(title: "Help Center", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.helpCenterTapped()
        })
        
        let privacyAction       = UIAlertAction(title: "Privacy Policy", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.privacyViewTapped()
        })
        
        let rateAction       = UIAlertAction(title: "Rate Us", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.rateApp()
        })
        
        let shareAction       = UIAlertAction(title: "Share", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let objectsToShare = [constant.shareContent]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        })
        
        let profileAction       = UIAlertAction(title: "My Profile", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.profileTapped()
        })
        
        let mouAction       = UIAlertAction(title: "MOU", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mouViewTapped()
        })
        
        let logoutAction = UIAlertAction(title: "Logout", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            MemberProfileInfo.shared.dropTable()
            HistoryRecordDB.shared.dropTable()
            AuthorityDatabse.shared.dropTable()
            
            uiFun.writeData(value: "", key: "member_id")
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            
            uiFun.navigateToScreem(identifierName: "ViewController", fromController: self)
            print("File Logour")
        })
        
        let exitAction       = UIAlertAction(title: "Exit", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            exit(0)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        dotMenu.addAction(instructionAction)
        dotMenu.addAction(helpAction)
        dotMenu.addAction(privacyAction)
        dotMenu.addAction(rateAction)
        dotMenu.addAction(shareAction)
        dotMenu.addAction(profileAction)
        dotMenu.addAction(mouAction)
        dotMenu.addAction(logoutAction)
        dotMenu.addAction(exitAction)
        dotMenu.addAction(cancelAction)
        
        
        self.present(dotMenu, animated: true, completion: nil)
    }
    
    func helpCenterTapped(){
        webViewHeader   = "Help Center"
        loadUrl         = "http://www.cintaa.net/cintaa-help-center/"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func privacyViewTapped(){
        loadUrl         = "http://www.cintaa.net/privacy-policy/"
        webViewHeader   = "Privacy Policy"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func mouViewTapped(){
        loadUrl         = "\(constant.baseURL)mou.pdf"
        webViewHeader   = "MOU"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func rateUsViewTapped(){
        
        webViewHeader   = "Rate Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func instructionTapped(){
        
        loadUrl         = "\(constant.baseURL)instructions.pdf"
        webViewHeader   = "Instruction"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func contactUs(){
        
        loadUrl         = "http://www.cintaa.net/contact-us/"
        webViewHeader   = "Contact Us"
        uiFun.navigateToScreem(identifierName: "WebViewViewController", fromController: self)
    }
    
    func profileTapped(){
        uiFun.navigateToScreem(identifierName: "ProfileViewController", fromController: self)
    }
    
    func rateApp() {
        guard let url = URL(string : "https://itunes.apple.com/us/app/cintaa/id1288952015?mt=8") else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
}


extension UIImage {
    
    /*
     @brief decode image base64
     */
    static func decodeBase64(toImage strEncodeData: String!) -> UIImage {
        
        if let decData = Data(base64Encoded: strEncodeData, options: .ignoreUnknownCharacters), strEncodeData.characters.count > 0 {
            return UIImage(data: decData)!
        }
        return UIImage()
    }
}
