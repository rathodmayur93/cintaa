//
//  CompletedShootDetailAPICall.swift
//  CINTA
//
//  Created by Mayur on 19/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CompletedShootDetailAPICall: NSObject {

    func makeAPICall(fromViewController : CompleteShootViewController, entryId : String){
        
        let url                         = "\(constant.baseURL)completedscheduledetails.php"
        let params : [String:AnyObject] = ["member_id"    : uiFun.readData(key: "member_id").trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "entry_id"     : entryId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        CleverTap.sharedInstance().recordEvent("Completed Shoot Detail API Call URL \(url)")
        CleverTap.sharedInstance().recordEvent("Completed Shoot Detail API Call Params", withProps: params)
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.completedShootDetailAPIResponse(response: jsonResponse)
                print("Completed Shoot Response \(jsonResponse)")
                return
                
            }else {
                print("Schedule Entry Failed")
                return
            }
            
        })
        
    }
}
