//
//  HistoryRecordDB.swift
//  CINTA
//
//  Created by Mayur on 03/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class HistoryRecordDB: NSObject {

    let field_entry_id                      = "entry_id"
    let field_played_character              = "played_character"
    let field_rate                          = "rate"
    let field_shoot_map_location            = "shoot_map_location"
    let field_due_days                      = "due_days"
    let field_producer_name                 = "producer_name"
    let field_packup_time                   = "packup_time"
    let field_call_time                     = "call_time"
    let field_callin_time                   = "callin_time"
    let field_shoot_location                = "shoot_location"
    let field_lat                           = "lat"
    let field_shoot_date                    = "shoot_date"
    let field_remark                        = "remark"
    let field_rate_type                     = "rate_type"
    let field_film_tv_name                  = "film_tv_name"
    let field_house_name                    = "house_name"
    let field_shift_time                    = "shift_time"
    let field_lng                           = "lng"
    let field_schedule_status               = "schedule_status"
    let field_service_call                  = "service_call"  // 0.1 means need to make api call for schedule entry & 0.2 means need to make an api call for diary entry and 1 means no need to make an api call
    
    
    
    var pathToDatabase  : String!
    var database        : FMDatabase!
    var databaseQueue   : FMDatabaseQueue!
    static let shared   : HistoryRecordDB     = HistoryRecordDB()
    let databaseFileName                      = "cintaHistory.sqlite"
    let tableName                             = "historyRecords"
    
    
    
    override init() {
        super.init()
        
        let documentsDirectory  = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase          = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    // MARK:- Create Database
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createUserInfoTableQuery = "create table if not exists \(tableName) (\(field_entry_id) varchar primary key not null, \(field_played_character) varchar not null, \(field_rate) varcahr not null, \(field_shoot_map_location) varchar not null, \(field_due_days) varchar, \(field_producer_name) varchar not null,\(field_packup_time) varchar not null, \(field_call_time) varchar not null,\(field_callin_time) varchar not null, \(field_shoot_location) varchar not null, \(field_lat) varcahr not null, \(field_shoot_date) varchar not null, \(field_remark) varchar, \(field_rate_type) varchar not null,\(field_film_tv_name) varchar not null, \(field_house_name) varchar not null,\(field_shift_time) varchar not null, \(field_lng) varchar not null,\(field_schedule_status) varchar not null, \(field_service_call) varchar not null)"
                    
                    do {
                        try database.executeUpdate(createUserInfoTableQuery, values: nil)
                        created = true
                        print("Database created succesfully \(tableName)")
                    }
                    catch {

                        print(error.localizedDescription)
                        print("Database creation failed \(tableName)")
                    }
                    
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    // MARK:- Open Database
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
                databaseQueue   = FMDatabaseQueue(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        return false
    }
    
    // MARK:- Insert into Database
    func insertBookingData(userInfoList : [CalendarEvent], serviceCall : String){
        
        if openDatabase(){
            
            var query = ""
            
            for i in 0..<userInfoList.count
            {
                let entry_id                    = userInfoList[i].entry_id
              
                let played_character            = userInfoList[i].played_character
                let rate                        = userInfoList[i].rate
                let shoot_map_location          = userInfoList[i].shoot_map_location
                
                let due_days                    = userInfoList[i].due_days
                let producer_name               = userInfoList[i].producer_name
                let packup_time                 = userInfoList[i].packup_time
                let call_time                   = userInfoList[i].call_time
                let callin_time                 = userInfoList[i].callin_time
                
                let shoot_location              = userInfoList[i].shoot_location
                let lat                         = userInfoList[i].lat
                let shoot_date                  = userInfoList[i].shoot_date
                
                let remark                      = userInfoList[i].remark
                let rate_type                   = userInfoList[i].rate_type
                let film_tv_name                = userInfoList[i].film_tv_name
                let house_name                  = userInfoList[i].house_name
                let shift_time                  = userInfoList[i].shift_time
                
                let lng                         = userInfoList[i].lng
                let schedule_status             = userInfoList[i].schedule_status
                
                query += "insert into \(tableName) (\(field_entry_id), \(field_played_character), \(field_rate), \(field_shoot_map_location), \(field_due_days), \(field_producer_name), \(field_packup_time), \(field_call_time), \(field_callin_time), \(field_shoot_location), \(field_lat), \(field_shoot_date), \(field_remark), \(field_rate_type), \(field_film_tv_name), \(field_house_name), \(field_shift_time), \(field_lng), \(field_schedule_status), \(field_service_call)) values ('\(entry_id)', '\(played_character)', '\(rate)', '\(shoot_map_location)', '\(due_days)', '\(producer_name)', '\(packup_time)', '\(call_time)', '\(callin_time)', '\(shoot_location)', '\(lat)', '\(shoot_date)', '\(remark)', '\(rate_type)', '\(film_tv_name)', '\(house_name)', '\(shift_time)', '\(lng)', '\(schedule_status)', '\(serviceCall)');"
                
                print("Insert historyRecords Of \(entry_id) & \(call_time)")
            }
            
            if !database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }
            
            database.close()
        }
    }
    
    
    func insertBookingQueue(userInfoList : [CalendarEvent], serviceCall : String){
    
        
    }
    
    // MARK:- Fetch From  Database
    func fetchBookingItems() -> [CalendarEvent]{
        
        var userDetail = [CalendarEvent]()
        
        if openDatabase() {
            let query = "select * from \(tableName)"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let userProfileModel                    = CalendarEvent()
                    
                    userProfileModel.entry_id               = results.string(forColumn: field_entry_id)
                    userProfileModel.played_character       = results.string(forColumn: field_played_character)
                    userProfileModel.rate                   = results.string(forColumn: field_rate)
                    userProfileModel.shoot_map_location     = results.string(forColumn: field_shoot_map_location)
                    userProfileModel.due_days               = results.string(forColumn: field_due_days)
                    userProfileModel.producer_name          = results.string(forColumn: field_producer_name)
                    userProfileModel.packup_time            = results.string(forColumn: field_packup_time)
                    userProfileModel.call_time              = results.string(forColumn: field_call_time)
                    userProfileModel.callin_time            = results.string(forColumn: field_callin_time)
                    userProfileModel.shoot_location         = results.string(forColumn: field_shoot_location)
                    userProfileModel.lat                    = results.string(forColumn: field_lat)
                    userProfileModel.shoot_date             = results.string(forColumn: field_shoot_date)
                    userProfileModel.remark                 = results.string(forColumn: field_remark)
                    userProfileModel.rate_type              = results.string(forColumn: field_rate_type)
                    userProfileModel.film_tv_name           = results.string(forColumn: field_film_tv_name)
                    userProfileModel.house_name             = results.string(forColumn: field_house_name)
                    userProfileModel.shift_time             = results.string(forColumn: field_shift_time)
                    userProfileModel.lng                    = results.string(forColumn: field_lng)
                    userProfileModel.schedule_status        = results.string(forColumn: field_schedule_status)
                    userProfileModel.serviceCall            = results.string(forColumn: field_service_call)
    
                    userDetail.append(userProfileModel)
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return userDetail
    }
    
    func fetchEventOfParticularDate(date : String) -> [CalendarEvent]{
    
        var userDetail = [CalendarEvent]()
        
        if openDatabase() {
            let query = "select * from \(tableName) where \(field_shoot_date)='\(date)'"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let userProfileModel                    = CalendarEvent()
                    
                    userProfileModel.entry_id               = results.string(forColumn: field_entry_id)
                    userProfileModel.played_character       = results.string(forColumn: field_played_character)
                    userProfileModel.rate                   = results.string(forColumn: field_rate)
                    userProfileModel.shoot_map_location     = results.string(forColumn: field_shoot_map_location)
                    userProfileModel.due_days               = results.string(forColumn: field_due_days)
                    userProfileModel.producer_name          = results.string(forColumn: field_producer_name)
                    userProfileModel.packup_time            = results.string(forColumn: field_packup_time)
                    userProfileModel.call_time              = results.string(forColumn: field_call_time)
                    userProfileModel.callin_time            = results.string(forColumn: field_callin_time)
                    userProfileModel.shoot_location         = results.string(forColumn: field_shoot_location)
                    userProfileModel.lat                    = results.string(forColumn: field_lat)
                    userProfileModel.shoot_date             = results.string(forColumn: field_shoot_date)
                    userProfileModel.remark                 = results.string(forColumn: field_remark)
                    userProfileModel.rate_type              = results.string(forColumn: field_rate_type)
                    userProfileModel.film_tv_name           = results.string(forColumn: field_film_tv_name)
                    userProfileModel.house_name             = results.string(forColumn: field_house_name)
                    userProfileModel.shift_time             = results.string(forColumn: field_shift_time)
                    userProfileModel.lng                    = results.string(forColumn: field_lng)
                    userProfileModel.schedule_status        = results.string(forColumn: field_schedule_status)
                    userProfileModel.serviceCall            = results.string(forColumn: field_service_call)
                    
                    userDetail.append(userProfileModel)
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return userDetail
    }
    
    func fetchBookingItemForCompletedShoot(entryId : String) -> [CalendarEvent]{
    
        var userDetail = [CalendarEvent]()
        
        if openDatabase() {
            let query = "select * from tokenData where \(field_entry_id) = '\(entryId)'"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let userProfileModel                    = CalendarEvent()
                    
                    userProfileModel.entry_id               = results.string(forColumn: field_entry_id)
                    userProfileModel.played_character       = results.string(forColumn: field_played_character)
                    userProfileModel.rate                   = results.string(forColumn: field_rate)
                    userProfileModel.shoot_map_location     = results.string(forColumn: field_shoot_map_location)
                    userProfileModel.due_days               = results.string(forColumn: field_due_days)
                    userProfileModel.producer_name          = results.string(forColumn: field_producer_name)
                    userProfileModel.packup_time            = results.string(forColumn: field_packup_time)
                    userProfileModel.call_time              = results.string(forColumn: field_call_time)
                    userProfileModel.callin_time            = results.string(forColumn: field_callin_time)
                    userProfileModel.shoot_location         = results.string(forColumn: field_shoot_location)
                    userProfileModel.lat                    = results.string(forColumn: field_lat)
                    userProfileModel.shoot_date             = results.string(forColumn: field_shoot_date)
                    userProfileModel.remark                 = results.string(forColumn: field_remark)
                    userProfileModel.rate_type              = results.string(forColumn: field_rate_type)
                    userProfileModel.film_tv_name           = results.string(forColumn: field_film_tv_name)
                    userProfileModel.house_name             = results.string(forColumn: field_house_name)
                    userProfileModel.shift_time             = results.string(forColumn: field_shift_time)
                    userProfileModel.lng                    = results.string(forColumn: field_lng)
                    userProfileModel.schedule_status        = results.string(forColumn: field_schedule_status)
                    userProfileModel.serviceCall            = results.string(forColumn: field_service_call)
                    
                    userDetail.append(userProfileModel)
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return userDetail
        
    }
    
    // MARK:- Update Database
    func updateBookingDataScheduleEntry(withID entry_id: String, film_tv_name: String, house_name: String, producer_name : String, call_time: String, serviceCall : String) {
        if openDatabase() {
            
            let query = "update \(tableName) set \(field_film_tv_name)=?, \(field_house_name)=?, \(field_producer_name)=?,  \(field_call_time)=?, \(field_service_call)=? where \(field_entry_id)=?"
            
            do {
                try database.executeUpdate(query, values: [film_tv_name, house_name, producer_name, call_time, serviceCall, entry_id])
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
    }
    
    func updateBookingInDiaryEntry(withID entry_id: String, calendarEvents : [CalendarEvent], serviceCall : String) {
        if openDatabase() {
            
            let query = "update \(tableName) set \(field_film_tv_name)=?, \(field_house_name)=?, \(field_producer_name)=?, \(field_shoot_map_location)=?, \(field_shoot_location)=?, \(field_played_character)=?, \(field_shift_time)=?, \(field_call_time)=?, \(field_rate)=?, \(field_due_days)=?, \(field_service_call)=? where \(field_entry_id)=?"
            
            do {
                try database.executeUpdate(query, values: [calendarEvents[0].film_tv_name, calendarEvents[0].house_name, calendarEvents[0].producer_name, calendarEvents[0].shoot_map_location, calendarEvents[0].shoot_location,calendarEvents[0].played_character, calendarEvents[0].shift_time, calendarEvents[0].call_time, calendarEvents[0].rate, calendarEvents[0].due_days, serviceCall, entry_id])
                
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
    }
    
    func updateBookingEntryForOffline(withID entryId : String, serviceCall : String){
    
        if openDatabase() {
            
            let query = "update \(tableName) set \(field_service_call)=? where \(field_entry_id)=?"
            
            do {
                try database.executeUpdate(query, values: [serviceCall ,entryId])
                
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        
    }
    
    func updateBookingEntryForInTime(withID entryId : String, inTime : String){
        
        if openDatabase() {
            let query = "update \(tableName) set \(field_callin_time)=?  where \(field_entry_id)=?"
            do
            {
                try database.executeUpdate(query, values: [inTime, entryId])
            }
            catch
            {
                print(error.localizedDescription)
            }
            database.close()
        }
    }
    
    func updateBookingEntryForPackupTime(withID entryId : String, packupTime : String){
        
        if openDatabase() {
            
            let query = "update \(tableName) set \(field_packup_time)=?  where \(field_entry_id)=?"
            do
            {
                try database.executeUpdate(query, values: [packupTime, entryId])
            }
            catch
            {
                print(error.localizedDescription)
            }
            database.close()
        }
    }
    
    func updateEventStatus(withID entryId : String){
    
        if openDatabase() {
            
            let query = "update \(tableName) set \(field_schedule_status)=?  where \(field_entry_id)=?"
            do
            {
                try database.executeUpdate(query, values: ["completed", entryId])
            }
            catch
            {
                print(error.localizedDescription)
            }
            database.close()
        }
    }
    
    
    
    func deleteBookingItem(entryId : String) -> Bool{
        
        var deleted = false
        
        if openDatabase() {
            let query = "delete from \(tableName) where \(field_entry_id) = '\(entryId)'"
            
            database.executeStatements(query)
            deleted = true
            database.close()
        }
        
        return deleted
    }
    
    func dropTable(){
        
        if openDatabase() {
            let query = "DELETE FROM \(tableName)"
            print(query)
        
            do {
                print(database)
                let results = try database.executeUpdate(query, withArgumentsIn: nil)
                database    = nil
            }catch{
                print(error.localizedDescription)
            }
        }
    }
}
