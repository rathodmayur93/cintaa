//
//  MemberProfileInfo.swift
//  CINTA
//
//  Created by Mayur on 03/10/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class MemberProfileInfo: NSObject {

    let field_mobile                = "mobilemobile"
    let field_state                 = "state"
    let field_email                 = "email"
    let field_profile_imgurl        = "profile_imgurl"
    let field_zip                   = "zip"
    let field_address               = "address"
    let field_password              = "password"
    let field_city                  = "city"
    let field_alt_mobile            = "alt_mobile"
    let field_website               = "website"
    let field_country               = "country"
    let field_name                  = "name"
    let field_gender                = "gender"
    
   
    var pathToDatabase  : String!
    var database        : FMDatabase!
    static let shared   : MemberProfileInfo     = MemberProfileInfo()
    let databaseFileName                = "cinta.sqlite"
    let TABLE_NAME                      = "memberInfo"
    
    
    
    override init() {
        super.init()
        
        let documentsDirectory  = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString) as String
        pathToDatabase          = documentsDirectory.appending("/\(databaseFileName)")
    }
    
    
    func createDatabase() -> Bool {
        var created = false
        
        if !FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase!)
            
            if database != nil {
                // Open the database.
                if database.open() {
                    let createUserInfoTableQuery = "create table if not exists \(TABLE_NAME) (\(field_mobile) varchar primary key not null, \(field_state) varchar not null, \(field_email) varcahr not null, \(field_profile_imgurl) varchar not null, \(field_zip) varchar, \(field_address) varchar not null,\(field_password) varchar not null,\(field_city) varchar not null,\(field_alt_mobile) varchar not null,\(field_website) varchar not null, \(field_country) varchar not null, \(field_name) varchar not null, \(field_gender) varchar not null)"
                    
                    do {
                        try database.executeUpdate(createUserInfoTableQuery, values: nil)
                        created = true
                        
                    }
                    catch {
                        print("Could not create table.")
                        print(error.localizedDescription)
                    }
                    
                    database.close()
                }
                else {
                    print("Could not open the database.")
                }
            }
        }
        
        return created
    }
    
    func openDatabase() -> Bool {
        if database == nil {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                database = FMDatabase(path: pathToDatabase)
            }
        }
        
        if database != nil {
            if database.open() {
                return true
            }
        }
        return false
    }
    
    func insertBookingDataData(userInfoList : [UserProfileModel]){
        
        if openDatabase(){
            
            var query = ""
            
            for i in 0..<userInfoList[0].nameAndimage.count{
                
                let state           = userInfoList[0].nameAndimage[0].state
                let email           = userInfoList[0].nameAndimage[0].email
                let profile_imgurl  = userInfoList[0].nameAndimage[0].profile_imgurl
                let zip             = userInfoList[0].nameAndimage[0].zip
                
                let mobile          = userInfoList[0].nameAndimage[0].mobile
                let address         = userInfoList[0].nameAndimage[0].address
                let password        = userInfoList[0].nameAndimage[0].password
                let city            = userInfoList[0].nameAndimage[0].city
                let alt_mobile      = userInfoList[0].nameAndimage[0].alt_mobile
                
                let website         = userInfoList[0].nameAndimage[0].website
                let country         = userInfoList[0].nameAndimage[0].country
                let name            = userInfoList[0].nameAndimage[0].name
                let gender          = userInfoList[0].nameAndimage[0].gender
                
                query += "insert into \(TABLE_NAME) (\(field_state), \(field_email), \(field_profile_imgurl), \(field_zip), \(field_mobile), \(field_address), \(field_password), \(field_city), \(field_alt_mobile), \(field_website), \(field_country), \(field_name), \(field_gender)) values ('\(state)', '\(email)', '\(profile_imgurl)', '\(zip)', '\(mobile)', '\(address)', '\(password)', '\(city)', '\(alt_mobile)', '\(website)', '\(country)', '\(name)', '\(gender)');"
               
                print("Insert userInfo Of \(email)")
            }
            
            if !database.executeStatements(query) {
                print("Failed to insert initial data into the database.")
                print(database.lastError(), database.lastErrorMessage())
            }
            
            database.close()
        }
    }
    
    func fetchBookingItems() -> [UserProfileModel]{
        
        var userDetail = [UserProfileModel]()
        
        if openDatabase() {
            let query = "select * from \(TABLE_NAME)"
            
            do {
                print(database)
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    let userProfileModel                    = UserProfileModel()
                    let nameImage                           = Nameandimage()
                    
                    nameImage.state                   = results.string(forColumn: field_state)
                    nameImage.email                   = results.string(forColumn: field_email)
                    nameImage.profile_imgurl          = results.string(forColumn: field_profile_imgurl)
                    nameImage.zip                     = results.string(forColumn: field_zip)
                    nameImage.mobile                  = results.string(forColumn: field_mobile)
                    
                    nameImage.address                 = results.string(forColumn: field_address)
                    nameImage.password                = results.string(forColumn: field_password)
                    nameImage.city                    = results.string(forColumn: field_city)
                    nameImage.alt_mobile              = results.string(forColumn: field_alt_mobile)
                    nameImage.website                 = results.string(forColumn: field_website)
                    
                    nameImage.country                 = results.string(forColumn: field_country)
                    nameImage.name                    = results.string(forColumn: field_name)
                    nameImage.gender                  = results.string(forColumn: field_gender)
                    
                    userProfileModel.nameAndimage.append(nameImage)
                    userDetail.append(userProfileModel)
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
            database.close()
        }
        return userDetail
    }
    
    func dropTable(){
    
        if openDatabase(){
            let query = "DELETE FROM \(TABLE_NAME)"
            print(query)
        
            do {
                print(database)
                let results = try database.executeUpdate(query, withArgumentsIn: nil)
                database    = nil
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
}
