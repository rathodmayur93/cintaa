//
//  CompletedShootTableViewCell.swift
//  CINTA
//
//  Created by Mayur on 16/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CompletedShootTableViewCell: UITableViewCell {

    @IBOutlet var date              : UILabel!
    @IBOutlet var productionHouse   : UILabel!
    @IBOutlet var location          : UILabel!
    @IBOutlet var shiftTime         : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
