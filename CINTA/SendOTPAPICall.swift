//
//  SendOTPAPICall.swift
//  CINTA
//
//  Created by Mayur on 06/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class SendOTPAPICall {

    func makeAPICall(fromViewController : ViewController, memberId : String, otp : String, mobile : String){
        
        let url                         = "\(constant.baseURL)cintaaSendSMS.php"
        let params : [String:AnyObject] = ["member_id" : memberId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "userOTP"   : otp.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "phone"     : mobile.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.passwordCheckAPIResponse(response: jsonResponse)
                return
                
            }else {
                print("CINTA Send OTP Failed")
                return
            }
            
        })
        
    }
    
    
    func makeAPICallResend(fromViewController : ForgotPasswordViewController, memberId : String, otp : String, mobile : String){
        
        let url                         = "\(constant.baseURL)cintaaSendSMS.php"
        let params : [String:AnyObject] = ["member_id" : memberId.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "userOTP"   : otp.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject,
                                           "phone"     : mobile.trimmingCharacters(in: .whitespacesAndNewlines) as AnyObject]
        print("==============================================")
        print("API Call URL \(url) & Parameters are \(params)")
        
        
        // Making an API Call
        util.sendRequest(url: url, parameters: params, fromViewController: fromViewController, completionHandler: { (result, error) in
            
            if let jsonResponse = result{
                
                fromViewController.resendOTPResponse(response: jsonResponse)
                return
                
            }else {
                print("CINTA Send OTP Failed")
                return
            }
            
        })
        
    }
}
