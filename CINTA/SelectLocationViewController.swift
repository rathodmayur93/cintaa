//
//  SelectLocationViewController.swift
//  CINTA
//
//  Created by Mayur on 17/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

var userSelectedLocation    = ""

class SelectLocationViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {

    var resultsViewController       : GMSAutocompleteResultsViewController?
    var searchController            : UISearchController?
    var backButtonView              : UIButton?
    
    let geocoder                                             = GMSGeocoder()
    var locationManager                                      = CLLocationManager()
    var mapView                     : GMSMapView             = GMSMapView()
    var lat                         : CLLocationDegrees      = CLLocationDegrees()
    var long                        : CLLocationDegrees      = CLLocationDegrees()
    var marker                      : GMSMarker              = GMSMarker()
    var camera                      : GMSCameraPosition      = GMSCameraPosition()
    var userPosition                : CLLocationCoordinate2D = CLLocationCoordinate2D()
    let screenSize : CGRect                                  = UIScreen.main.bounds
    
    @IBOutlet var myLocation        : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        locationManager.delegate    = self
        showAnimate()
        
        self.view.endEditing(true)
        self.view.resignFirstResponder()
        
        CleverTap.sharedInstance().recordEvent("Select Location From Map")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Hide Keyboard When View Controller Appears
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    // MARK:- UI Components
    func setUI(){
        
        /*************  SEARCH BUTTON ON TOP ****************/
        resultsViewController                   = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate         = self
        
        searchController                        = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater  = resultsViewController
        
        // craeting search bar
        let subView = UIView(frame: CGRect(x: 0, y: 20.0, width: (self.view.frame.width), height: 45.0))
        navigationController?.navigationBar.isTranslucent       = false
        searchController?.hidesNavigationBarDuringPresentation  = false
        
        // creating back button
        backButtonView?.frame = CGRect(x: 8, y: 8, width: 36, height: 36)
        let btnImage          = UIImage(named: "back")
        backButtonView?.setImage(btnImage, for: .normal)
        
        // This makes the view area include the nav bar even though it is opaque.
        // Adjust the view placement down.
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .top
        
        subView.addSubview((searchController?.searchBar)!)
        view.addSubview(subView)
        
        searchController?.searchBar.barTintColor    = UIColor.brown//uiFun.hexStringToUIColor(hex: ridlrColors.ridlrBrandColor)
        searchController?.searchBar.tintColor       = UIColor.white//uiFun.hexStringToUIColor(hex: ridlrColors.whiteColor)
        searchController?.searchBar.placeholder     = "Enter a location in Mumbai"
        searchController?.searchBar.barStyle        = UIBarStyle.default
        
        if(searchController?.searchBar.isSearchResultsButtonSelected)!{
            print("Search bar is selected...!!!")
        }
        
        // requesting for location
        locationManager.delegate            = self
        locationManager.desiredAccuracy     = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    
    func searchBar(){
        
        
        /*************  SEARCH BUTTON ON TOP ****************/
        
        let searchButton                = UIButton(type: .system)
        searchButton.frame              = CGRect(x: 8, y: 100, width: (screenSize.width - 16), height: 36)
        searchButton.backgroundColor    = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        searchButton.setTitle("Select Location", for: .normal)
        searchButton.setTitleColor(UIColor.black, for: .normal)
        searchButton.addTarget(self, action: #selector(searchButtonAction), for: .touchUpInside)
        searchButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        searchButton.titleEdgeInsets = UIEdgeInsetsMake(0, 48, 0, 0)
        searchButton.resignFirstResponder()
        self.view.addSubview(searchButton)
        
        /*************  BACK BUTTON ON TOP ****************/
        
        let backButton                  = UIButton(type: .system)
        backButton.frame                = CGRect(x: 16, y: 24, width: 36, height: 36)
        let buttonImage                 = UIImage(named: "ic_indicator_go_back_holo")
        backButton.setImage(buttonImage, for: .normal)
        backButton.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)
        backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        self.view.addSubview(backButton)
        
        setUI()
    }
    
    // MARK:- Location Functions
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // fetching location response
        let userLocation: CLLocation = locations[0]
        
        let latitude                 = userLocation.coordinate.latitude
        let longitude                = userLocation.coordinate.longitude
        
        
        // Storing data into the parmanent storage
        uiFun.writeData(value: String(describing :latitude), key: "lat")
        uiFun.writeData(value: String(describing :longitude), key: "long")
        
        camera  = GMSCameraPosition.camera(withLatitude: Double(uiFun.readData(key: "lat"))!, longitude: Double(uiFun.readData(key: "long"))!, zoom: 13.0)
        
        // google reverse geo-coding json response
        if(uiFun.isConnectedToNetwork()){
            geocoder.reverseGeocodeCoordinate(userLocation.coordinate) { (response, error) in
                let jsonResponse            = response?.results()
                self.myLocation.text        = jsonResponse?[0].lines?[0]    // User current location address
                userSelectedLocation        = (jsonResponse?[0].lines?[0])!
            }
        }else{
            self.myLocation.text            = "\(latitude),\(longitude)"
            userSelectedLocation            = "\(latitude),\(longitude)"
        }
        
        self.locationManager.stopUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = false
        
        initMap()
        mapView.delegate    = self
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            print("Authorized")
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            let alertController = UIAlertController (title: "Location Setting", message: "Please provide location access.", preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (UIAlertAction) in
                uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
            })
        
            
            present(alertController, animated: true, completion: nil)
            
            break
        }
    }
    
    // MARK:- Google Map Functions
    func initMap(){
        
        print("Location from splash to route search \(uiFun.readData(key: "lat"))")
        
        // set the map camera
        let mapViewFrame            = CGRect(x: 0, y: 64, width: self.view.frame.width, height: self.view.frame.height - 300)
        mapView                     = GMSMapView.map(withFrame: mapViewFrame, camera: camera)
        mapView.isMyLocationEnabled = true
        self.view.addSubview(mapView)
        
        // Creates a marker in the center of the map.
        let position = CLLocationCoordinate2D(latitude: Double(uiFun.readData(key: "lat"))!, longitude: Double(uiFun.readData(key: "long"))!)
        marker                  = GMSMarker(position: position)
        marker.icon             = UIImage(named: "droplocation_icon")
        marker.map              = self.mapView
        
        //reverse geo-coding
        geocoder.reverseGeocodeCoordinate(userPosition) { (response, error) in
            print("Response of Google Map is \(response) \(self.userPosition)")
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition){
        // When map camera changes this method will get invoked
        marker.position = mapView.camera.target
        print("Lcoation Changed")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        // When dragging of the map is complete this method will get invoked
        
        geocoder.reverseGeocodeCoordinate(position.target) { (response, error) in
            // GOogle reverse geo-coding using lat long
            let jsonResponse = response?.results()
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                self.myLocation.text = lines.joined(separator: "\n")
                userSelectedLocation = lines.joined(separator: "\n")
            }
        }
    }
    
    // MARK:- Button Actions
    
    @IBAction func okAction(_ sender: Any) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "locationUpdate"), object: nil)
        userSelectedLocation    = myLocation.text!
        //uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
        removeAnimate()
        
    }
    
    
    // MARK:- Common Function
    func searchButtonAction(sender: UIButton!) {
        // When user click on search text field to search any location on map
        print("Button tapped")
        let autocompleteController      = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    func backButtonAction(sender: UIButton){
        // When user clicks on back button
        print("Back button tapped")
        uiFun.navigateToScreem(identifierName: "MainViewController", fromController: self)
        
        mapView.isMyLocationEnabled     = false
        locationManager.stopUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParentViewController: nil)
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        })
    }
    
}

// MARK:- Extensions
extension SelectLocationViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection or user search.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place Lat Long: \(place.coordinate)")
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 13.0)
        mapView.animate(to: camera)
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension SelectLocationViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        print("Place Sublocality : \(place.placeID)")
        
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 13.0)
        mapView.animate(to: camera)
        
        
        geocoder.reverseGeocodeCoordinate(place.coordinate) { (response, error) in
            if let address = response?.firstResult() {
                let lines = address.lines! as [String]
                self.myLocation.text = lines.joined(separator: "\n")
                userSelectedLocation = lines.joined(separator: "\n")
                
            }
        }
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
