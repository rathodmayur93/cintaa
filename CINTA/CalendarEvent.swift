//
//  CalendarEvent.swift
//  CINTA
//
//  Created by Mayur on 23/09/17.
//  Copyright © 2017 mayur. All rights reserved.
//

import UIKit

class CalendarEvent {
    
    var entry_id                = ""
    var film_tv_name            = ""
    var house_name              = ""
    var producer_name           = ""
    var shoot_date              = ""
    var schedule_status         = ""
    var shift_time              = ""
    var call_time               = ""
    var shoot_map_location      = ""
    var lat                     = ""
    var lng                     = ""
    var played_character        = ""
    var rate                    = ""
    var due_days                = ""
    var remark                  = ""
    var rate_type               = ""
    var callin_time             = ""
    var packup_time             = ""
    var shoot_location          = ""
    var serviceCall             = ""
}
